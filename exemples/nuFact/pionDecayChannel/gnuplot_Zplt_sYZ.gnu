#plot for [i=1:1000] 'data'.i.'.txt' using 1:2 title 'Flow '.i

set title "Plotted from file Zplt "

set key maxcol 1
set key t r 

#set logscale y 

set xtics mirror
set ytics mirror

set size ratio 1

set xlabel 's  [m]'
set ylabel 'Y  [m]'

cm2m = 0.01
kG2T= 0.1
MeV2eV = 1e6
am = 938.27203
c = 2.99792458e8

set xrange []
set yrange []

plot for [i=1:100] \
   'zgoubi.plt' u ($19== i ? $14 *cm2m : 1/0):($10 *cm2m) w p ps .4 notit

     set terminal postscript eps color  enh  
       set output "gnuplot_Zplt_sY.eps"  
       replot  
       set terminal X11  
       unset output  

 
pause 1

set ylabel 'Z  [m]'

plot for [i=1:100] \
   'zgoubi.plt' u ($19== i ? $14 *cm2m : 1/0):($12 *cm2m) w p ps .4 notit

     set terminal postscript eps color  enh  
       set output "gnuplot_Zplt_sZ.eps"  
       replot  
       set terminal X11  
       unset output  

 
pause 1
exit

