call, "mad.lattice"

initial : beta0,  betx=8.783,bety=22.874


!----------------------------
! Will set chromaticity to zero
sexF =  2.321411E-01        ! => B_r = 2.321411E-01*r^2/2 = 0.0011607055 T at r=10cm
sexD = -3.673753E-01  ! sexD== K2=B''/Brho=B2/Brho and B_r=B2 r^2/2 => B_r=Brho sexD r^2/2=-3.673753E-01= -0.0018368765 T at r=10cm
set, bde[k2] , sexD
set, bdend[k2] , 0.
set, bda[k2] , sexD
set, bfa[k2] , sexF
set, bfand[k2] , 0.
!----------------------------

use, superperiod
print, #E
twiss,  save

!------ HARMINization of the stuff ----------
beam, particle=muon, energy=20, &
ex=1.5e-4, ey=1.5e-4

harmon

hchromaticity
hvary, name=bfa[k2], step=0.001
hvary, name=bda[k2], step=0.001
htune, qx'=0, qy'=0

!--

HFUNCTIONS

!--

hresonance, ORDER=3
hweight, qx''=1., qy''=1.,  qx'''=10.,  qy'''=1.

ENDHARM
!------ end HARMINization of the stuff -------



stop

