call, "mad.lattice"

initial : beta0,  betx=8.783,bety=22.874

setplot, lwidth=3, xsize=28, ysize=20, &
rscale=1.5, sscale=1.5, ascale=1.5, lscale=1.5

use, arc10
print,#E
savebeta, label=betaAtArcS, place=#S 
savebeta, label=betaAtArcE, place=#E 
twiss,  save

!plot, spline=T, haxis=s, vaxis1=betx,bety, vaxis2=dx

use, arc16
print, #E
twiss,   save


use, arcToEnd
print, #E
savebeta, label=betaAtStart, place=#E 
twiss,  beta=betaAtArcS, save

use, superperiod
print, #E
!SURVEY, TAPE='SURVEY_RING'
twiss,  beta=betaAtStart, save


plot, spline=T, haxis=s, vaxis1=betx, vaxis2=bety
plot, spline=T, haxis=s, vaxis1=dx

stop


!plot, spline=T, haxis=s, vaxis1=dx !, vaxis2=dy


use, soleno
print, full
twiss,    beta=betaAtArcS, save
! MATRIX
match, line=soleno
rmatrix, range=#S/#E, rm(1,5)=0
endmatch


use, superperiod
print, #E
twiss,  save

!plot, spline=T, haxis=s, vaxis1=betx,bety, vaxis2=dx

use, arc16
print, #E
twiss,   save

use, begToArc10
print, #E
! MATRIX
match, line=begToArc10
rmatrix, range=#S/#E, rm(1,5)=0
endmatch
twiss, beta0=initial, save

!plot, spline=T, haxis=s, vaxis1=betx,bety, vaxis2=dx

use, arcToEnd
print, #E
! MATRIX
match, line=begToArc10
rmatrix, range=#S/#E, rm(1,5)=0
endmatch
twiss, beta=betaAtArcS, save

use, arcToSole
print, #E
! MATRIX
match, line=begToArc10
rmatrix, range=#S/#E, rm(1,5)=0
endmatch
savebeta, label=betaAtSoleS, place=#E
twiss, beta0=betaAtArcE, save

use, sole
print, #E
savebeta, label=betaAtSoleE, place=#E
twiss, beta=betaAtSoleS, save
! MATRIX
match, line=sole
rmatrix, range=#S/#E, rm(1,5)=0
endmatch

use, soleStraight
print, #E
twiss, save

plot, spline=T, haxis=s, vaxis1=betx,bety



use, soleToArc
print, #E
twiss, beta=betaAtSoleE, save

use, arc16
print, full
survey, tape="surveyArc"
twiss,   save

use, arcToEnd
print, #E
twiss,  beta=betaAtArcE, save


use, collimRFTune
print, #E
twiss,  beta=betaAtArcE, save


!plot,  haxis=s, vaxis1=betx, bety, spline  
!!!,hmin = 0, hmax = 125



use, superperiod
print, #E
twiss,  save

plot, spline=T, haxis=s, vaxis1=betx,bety, vaxis2=dx

stop 



