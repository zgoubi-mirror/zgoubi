

set key c t

set xtics nomirror; set xlabel "turn"; set ytics nomirror; set ylabel "V [Volt]"; set y2tics; set y2label "phase [rad]"

plot "Ramp18GeV2.csv" u 1:4 w l lt 3 dt 1 lw 2 lc 1 tit "V",  "Ramp18GeV2.csv" u 1:5 axes x1y2 w l lw 1 lc 2 tit "phase"

 set terminal postscript eps blacktext color enh size 9.3cm,6cm "Times-Roman" 12
 set output "gnuplot_RFRamp.eps"
 replot
 set terminal X11
 unset output

pause 1
exit
