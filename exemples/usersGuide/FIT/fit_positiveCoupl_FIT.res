MATCHING A SYMMETRIC QUADRUPOLE TRIPLET, INCLUDING COUPLED QUADS
'OBJET'                                                                                                      1
2501.73            RIGIDITY (kG.cm)
5                  11 PARTICLES GENERATED FOR USE OF MATRIX
.01  .01  .01   .01  0. .0001
0.  0.  0.   0.  0.  1.
'ESL   '                                                                                                     2
200.
'QUADRUPO'    3                                                                                              3
0
40.  15.  -7.
0.  0.
6  .1122 6.2671 -1.4982 3.5882 -2.1209 1.723
0.   0.
6  .1122 6.2671 -1.4982 3.5882 -2.1209 1.723
5.
1 0. 0. 0.
'ESL'                                                                                                        4
30.
'QUADRUPO'     5                                                                                             5
0
40.  15.  3.
0.  0.
6  .1122 6.2671 -1.4982 3.5882 -2.1209 1.723
0.   0.
6  .1122 6.2671 -1.4982 3.5882 -2.1209 1.723
5.
1 0. 0. 0.
'ESL'                                                                                                        6
30.
'QUADRUPO'      7                                                                                            7
0
40.  15.  -7.
0.  0.
6  .1122 6.2671 -1.4982 3.5882 -2.1209 1.723
0.   0.
6  .1122 6.2671 -1.4982 3.5882 -2.1209 1.723
5.
1 0. 0. 0.
'ESL'                                                                                                        8
200.
'MATRIX'                                                                                                     9
1  0
'FIT'             ! Vary b in quads for FIT of R12 and R34                                                  10
2 nofinal          ! # of variables; variables will save in zgoubi.FITVALS.out
3  12 7.012  .2    ! Symmetric triplet => quads #1 and #3 are coupled
5  12  0    2.     ! Parmtr #12 of elements #3, 5 and 7 is the field value
2  1.E-5  200     ! # of constraints; penalty; max. nmbr of itereations
1  1 2 8 16.6 1. 0 ! Cnstrnt #1 : R12=16.6 after last drift (lmnt #8)
1  3 4 8 -.88 1. 0 ! Cnstrnt #2 : R34=-.88 after last drift
'MATRIX'                                                                                                    11
1  0
'END'                                                                                                       12

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =       2501.730 kG*cm

                                         CALCUL  DES  TRAJECTOIRES

                              OBJET  (5)  FORME  DE     13 POINTS 



                                 Y (cm)        T (mrd)       Z (cm)        P (mrd)       S (cm)        dp/p 
                   Sampling :    1.0000E-02    1.0000E-02    1.0000E-02    1.0000E-02    0.0000E+00    1.0000E-04
Reference trajectory #    1 :    0.0000E+00    0.0000E+00    0.0000E+00    0.0000E+00    0.0000E+00     1.000    

************************************************************************************************************************************
      2  Keyword, label(s) :  ESL         '                                                                            IPASS= 1


                              Drift,  length =   200.00000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  2.0000000E+02  0.00000E+00

 Cumulative length of optical axis =    2.00000000     m   ;  Time  (for reference rigidity & particle) =   1.068463E-08 s 

************************************************************************************************************************************
      3  Keyword, label(s) :  QUADRUPO    3                                                                            IPASS= 1


      -----  QUADRUPOLE  : 
                Length  of  element  =    40.000000      cm
                Bore  radius      RO =    15.000      cm                              B/RO^IM/Brho (strength)
               B-QUADRUPOLE  = -7.0000000E+00 kG   (i.e.,  -7.0000000E+00 * SCAL)    -1.8653758E+00

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   7.5000E+00

                    Integration step :   5.000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           40.000     0.000     0.000     0.000     0.000            1
  A    1  1.0000     0.010     0.000     0.000     0.000           40.000     0.012     0.000     0.000     0.000            2
  A    1  1.0000    -0.010     0.000     0.000     0.000           40.000    -0.012    -0.000     0.000     0.000            3
  A    1  1.0000     0.000     0.010     0.000     0.000           40.000     0.003     0.000     0.000     0.000            4
  A    1  1.0000     0.000    -0.010     0.000     0.000           40.000    -0.003    -0.000     0.000     0.000            5
  A    1  1.0000     0.000     0.000     0.010     0.000           40.000     0.000     0.000     0.009    -0.000            6
  A    1  1.0000     0.000     0.000    -0.010     0.000           40.000     0.000     0.000    -0.009     0.000            7
  A    1  1.0000     0.000     0.000     0.000     0.010           40.000     0.000     0.000     0.002    -0.000            8
  A    1  1.0000     0.000     0.000     0.000    -0.010           40.000     0.000     0.000    -0.002     0.000            9
  A    1  1.0001     0.000     0.000     0.000     0.000           40.000     0.000     0.000     0.000     0.000           10
  A    1  0.9999     0.000     0.000     0.000     0.000           40.000     0.000     0.000     0.000     0.000           11
  A    1  1.0000     0.000     0.000     0.000     0.000           40.000     0.000     0.000     0.000     0.000           12
  A    1  1.0000     0.000     0.000     0.000     0.000           40.000     0.000     0.000     0.000     0.000           13

     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    2.40000000     m ;  Time  (for ref. rigidity & particle) =   1.282155E-08 s 

************************************************************************************************************************************
      4  Keyword, label(s) :  ESL                                                                                      IPASS= 1


                              Drift,  length =    30.00000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  2.7000000E+02  1.33426E-03

 Cumulative length of optical axis =    2.70000000     m   ;  Time  (for reference rigidity & particle) =   1.442425E-08 s 

************************************************************************************************************************************
      5  Keyword, label(s) :  QUADRUPO    5                                                                            IPASS= 1


      -----  QUADRUPOLE  : 
                Length  of  element  =    40.000000      cm
                Bore  radius      RO =    15.000      cm                              B/RO^IM/Brho (strength)
               B-QUADRUPOLE  =  3.0000000E+00 kG   (i.e.,   3.0000000E+00 * SCAL)     7.9944678E-01

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   7.5000E+00

                    Integration step :   5.000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           40.000     0.000     0.000     0.000     0.000            1
  A    1  1.0000     0.010     0.000     0.000     0.000           40.000     0.016     0.000     0.000     0.000            2
  A    1  1.0000    -0.010     0.000     0.000     0.000           40.000    -0.016    -0.000     0.000     0.000            3
  A    1  1.0000     0.000     0.010     0.000     0.000           40.000     0.004     0.000     0.000     0.000            4
  A    1  1.0000     0.000    -0.010     0.000     0.000           40.000    -0.004    -0.000     0.000     0.000            5
  A    1  1.0000     0.000     0.000     0.010     0.000           40.000     0.000     0.000     0.004    -0.000            6
  A    1  1.0000     0.000     0.000    -0.010     0.000           40.000     0.000     0.000    -0.004     0.000            7
  A    1  1.0000     0.000     0.000     0.000     0.010           40.000     0.000     0.000     0.002     0.000            8
  A    1  1.0000     0.000     0.000     0.000    -0.010           40.000     0.000     0.000    -0.002    -0.000            9
  A    1  1.0001     0.000     0.000     0.000     0.000           40.000     0.000     0.000     0.000     0.000           10
  A    1  0.9999     0.000     0.000     0.000     0.000           40.000     0.000     0.000     0.000     0.000           11
  A    1  1.0000     0.000     0.000     0.000     0.000           40.000     0.000     0.000     0.000     0.000           12
  A    1  1.0000     0.000     0.000     0.000     0.000           40.000     0.000     0.000     0.000     0.000           13

     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    3.10000000     m ;  Time  (for ref. rigidity & particle) =   1.656117E-08 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  ESL                                                                                      IPASS= 1


                              Drift,  length =    30.00000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  3.4000000E+02  2.66851E-03

 Cumulative length of optical axis =    3.40000000     m   ;  Time  (for reference rigidity & particle) =   1.816387E-08 s 

************************************************************************************************************************************
      7  Keyword, label(s) :  QUADRUPO    7                                                                            IPASS= 1


      -----  QUADRUPOLE  : 
                Length  of  element  =    40.000000      cm
                Bore  radius      RO =    15.000      cm                              B/RO^IM/Brho (strength)
               B-QUADRUPOLE  = -7.0000000E+00 kG   (i.e.,  -7.0000000E+00 * SCAL)    -1.8653758E+00

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   7.5000E+00

                    Integration step :   5.000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           40.000     0.000     0.000     0.000     0.000            1
  A    1  1.0000     0.010     0.000     0.000     0.000           40.000     0.021     0.000     0.000     0.000            2
  A    1  1.0000    -0.010     0.000     0.000     0.000           40.000    -0.021    -0.000     0.000     0.000            3
  A    1  1.0000     0.000     0.010     0.000     0.000           40.000     0.006     0.000     0.000     0.000            4
  A    1  1.0000     0.000    -0.010     0.000     0.000           40.000    -0.006    -0.000     0.000     0.000            5
  A    1  1.0000     0.000     0.000     0.010     0.000           40.000     0.000     0.000    -0.000    -0.000            6
  A    1  1.0000     0.000     0.000    -0.010     0.000           40.000     0.000     0.000     0.000     0.000            7
  A    1  1.0000     0.000     0.000     0.000     0.010           40.000     0.000     0.000     0.002    -0.000            8
  A    1  1.0000     0.000     0.000     0.000    -0.010           40.000     0.000     0.000    -0.002     0.000            9
  A    1  1.0001     0.000     0.000     0.000     0.000           40.000     0.000     0.000     0.000     0.000           10
  A    1  0.9999     0.000     0.000     0.000     0.000           40.000     0.000     0.000     0.000     0.000           11
  A    1  1.0000     0.000     0.000     0.000     0.000           40.000     0.000     0.000     0.000     0.000           12
  A    1  1.0000     0.000     0.000     0.000     0.000           40.000     0.000     0.000     0.000     0.000           13

     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    3.80000000     m ;  Time  (for ref. rigidity & particle) =   2.030079E-08 s 

************************************************************************************************************************************
      8  Keyword, label(s) :  ESL                                                                                      IPASS= 1


                              Drift,  length =   200.00000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  5.8000000E+02  4.00277E-03

 Cumulative length of optical axis =    5.80000000     m   ;  Time  (for reference rigidity & particle) =   3.098542E-08 s 

************************************************************************************************************************************
      9  Keyword, label(s) :  MATRIX                                                                                   IPASS= 1


  Reference, before change of frame (particle #  1  - D-1,Y,T,Z,s,time)  : 
   0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00   5.80000000E+02   4.00276914E-03

           Frame for MATRIX calculation moved by :
            XC =    0.000 cm , YC =    0.000 cm ,   A =  0.00000 deg  ( = 0.000000 rad )


  Reference, after change of frame (particle #  1  - D-1,Y,T,Z,s,time)  : 
   0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00   5.80000000E+02   4.00276914E-03

  Reference particle (#     1), path length :   580.00000     cm  relative momentum :    1.00000    


                  TRANSFER  MATRIX  ORDRE  1  (MKSA units)

           5.43427         17.0254         0.00000         0.00000         0.00000         0.00000    
           1.67580         5.43425         0.00000         0.00000         0.00000         0.00000    
           0.00000         0.00000        -1.27003       -0.974288         0.00000         0.00000    
           0.00000         0.00000       -0.629171        -1.27004         0.00000         0.00000    
           0.00000         0.00000         0.00000         0.00000         1.00000         0.00000    
           0.00000         0.00000         0.00000         0.00000         0.00000         1.00000    

          DetY-1 =      -0.0000157172,    DetZ-1 =      -0.0000158156

          R12=0 at   -3.133     m,        R34=0 at  -0.7671     m

      First order symplectic conditions (expected values = 0) :
        -1.5717E-05   -1.5816E-05     0.000         0.000         0.000         0.000    


                                        ----------------------------------------
                                        -  EDWARDS AND TENG`S PARAMETRIZATION  -
                                        ----------------------------------------


      COUPLING PARAMETERS:
                           - r:     0.00000000
                           - C:
                                   0.00000000      0.00000000
                                   0.00000000      0.00000000

                                                                              MODE 1               MODE 2

      FRACTIONAL PART OF THE BETATRON TUNES IN THE DECOUPLED FRAME:        0.00000000           0.00000000

      EDWARDS-TENG`S PARAMETERS:
                                - ALPHA:                                    0.00000000           0.00000000
                                - BETA:                                     0.00000000           0.00000000
                                - GAMMA:                                    0.00000000           0.00000000

      HAMILTONIAN PERTURBATION PARAMETERS:

                           - DISTANCE FROM THE NEAREST DIFFERENCE LINEAR RESONANCE:     0.00000000
                           - COUPLING STRENGTH OF THE DIFFERENCE LINEAR RESONANCE:      0.00000000

                           - DISTANCE FROM THE NEAREST SUM LINEAR RESONANCE:            0.00000000
                           - UNPERTURBED HORIZONTAL TUNE:                               0.00000000
                           - UNPERTURBED VERTICAL TUNE:                                 0.00000000

      P MATRIX :

    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00

************************************************************************************************************************************
     10  Keyword, label(s) :  FIT                                                                                      IPASS= 1

     Pgm main. FITING=.T., FIT procedure launched.

           variable #            1       IR =            3 ,   ok.
           variable #            1       IP =           12 ,   ok.
           variable  element 3,  prmtr # 12 : coupled  with  element 7,  prmtr # 12
           variable #            1       XC.=            7 ,   ok.
           variable #            1       .XC=           12 ,   ok.
           variable #            2       IR =            5 ,   ok.
           variable #            2       IP =           12 ,   ok.
           constraint #            1       IR =            8 ,   ok.
           constraint #            2       IR =            8 ,   ok.

                    FIT  variables  and  constraints  in  good  order,  FIT  will proceed. 
 STATUS OF VARIABLES  (Iteration #   201 /    200 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   3   1    12   -8.40       -7.00      -6.9999863      -5.60      1.192-265  QUADRUPO   3                    -                   
   7   1    12   -8.40       -7.00      -7.0000137      -5.60      1.192-265
   5   2    12   -3.00        3.25       3.2504129       9.00      5.108-265  QUADRUPO   5                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-05)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  1   1   2     8    1.660000E+01    1.000E+00    1.659934E+01    4.41E-02 ESL        -                    -                    0
  1   3   4     8   -8.800000E-01    1.000E+00   -8.830948E-01    9.56E-01 ESL        -                    -                    0
 Fit reached penalty value   1.0019E-05



   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     11  Keyword, label(s) :  MATRIX                                                                                   IPASS= 1


  Reference, before change of frame (particle #  1  - D-1,Y,T,Z,s,time)  : 
   0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00   5.80000000E+02   4.00276914E-03

           Frame for MATRIX calculation moved by :
            XC =    0.000 cm , YC =    0.000 cm ,   A =  0.00000 deg  ( = 0.000000 rad )


  Reference, after change of frame (particle #  1  - D-1,Y,T,Z,s,time)  : 
   0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00   5.80000000E+02   4.00276914E-03

  Reference particle (#     1), path length :   580.00000     cm  relative momentum :    1.00000    


                  TRANSFER  MATRIX  ORDRE  1  (MKSA units)

           5.27308         16.5993         0.00000         0.00000         0.00000         0.00000    
           1.61484         5.27306         0.00000         0.00000         0.00000         0.00000    
           0.00000         0.00000        -1.24479       -0.883095         0.00000         0.00000    
           0.00000         0.00000       -0.622298        -1.24481         0.00000         0.00000    
           0.00000         0.00000         0.00000         0.00000         1.00000         0.00000    
           0.00000         0.00000         0.00000         0.00000         0.00000         1.00000    

          DetY-1 =      -0.0000159654,    DetZ-1 =      -0.0000160474

          R12=0 at   -3.148     m,        R34=0 at  -0.7094     m

      First order symplectic conditions (expected values = 0) :
        -1.5965E-05   -1.6047E-05     0.000         0.000         0.000         0.000    


                                        ----------------------------------------
                                        -  EDWARDS AND TENG`S PARAMETRIZATION  -
                                        ----------------------------------------


      COUPLING PARAMETERS:
                           - r:     0.00000000
                           - C:
                                   0.00000000      0.00000000
                                   0.00000000      0.00000000

                                                                              MODE 1               MODE 2

      FRACTIONAL PART OF THE BETATRON TUNES IN THE DECOUPLED FRAME:        0.00000000           0.00000000

      EDWARDS-TENG`S PARAMETERS:
                                - ALPHA:                                    0.00000000           0.00000000
                                - BETA:                                     0.00000000           0.00000000
                                - GAMMA:                                    0.00000000           0.00000000

      HAMILTONIAN PERTURBATION PARAMETERS:

                           - DISTANCE FROM THE NEAREST DIFFERENCE LINEAR RESONANCE:     0.00000000
                           - COUPLING STRENGTH OF THE DIFFERENCE LINEAR RESONANCE:      0.00000000

                           - DISTANCE FROM THE NEAREST SUM LINEAR RESONANCE:            0.00000000
                           - UNPERTURBED HORIZONTAL TUNE:                               0.00000000
                           - UNPERTURBED VERTICAL TUNE:                                 0.00000000

      P MATRIX :

    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00

************************************************************************************************************************************
     12  Keyword, label(s) :  END                                                                                      IPASS= 1

                            13 particles have been launched
                     Made  it  to  the  end :     13

************************************************************************************************************************************
 Pgm zgoubi : Execution ended normally, upon keyword END or FIN
   
 File in:   zgoubi.dat
 File out:  zgoubi.res

  Zgoubi, author's dvlpmnt version.
  Job  started  on  14-02-2022,  at  10:09:19 
  JOB  ENDED  ON    14-02-2022,  AT  10:09:20 

   CPU time, total :     1.2168740000000000     

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
