MATCHING QUADRUPOLE TRIPLET, INCLUDING COUPLED DRIFTS
'OBJET'                                                                                                      1
2501.73            RIGIDITY (kG.cm)
5                  11 PARTICLES GENERATED FOR USE OF MATRIX
.01  .01  .01   .01  0. .0001
0.  0.  0.   0.  0.  1.
'ESL   '                                                                                                     2
200.
'QUADRUPO'    3                                                                                              3
0
40.  15.  -6.
0.  0.
6  .1122 6.2671 -1.4982 3.5882 -2.1209 1.723
0.   0.
6  .1122 6.2671 -1.4982 3.5882 -2.1209 1.723
5.
1 0. 0. 0.
'ESL'                                                                                                        4
30.
'QUADRUPO'     5                                                                                             5
0
40.  15.  3.
0.  0.
6  .1122 6.2671 -1.4982 3.5882 -2.1209 1.723
0.   0.
6  .1122 6.2671 -1.4982 3.5882 -2.1209 1.723
5.
1 0. 0. 0.
'ESL'                                                                                                        6
30.
'QUADRUPO'      7                                                                                            7
0
40.  15.  -8.
0.  0.
6  .1122 6.2671 -1.4982 3.5882 -2.1209 1.723
0.   0.
6  .1122 6.2671 -1.4982 3.5882 -2.1209 1.723
5.
1 0. 0. 0.
'ESL'                                                                                                        8
200.
'MATRIX'                                                                                                     9
1  0
'FIT'             ! Vary first and last straight sections                                                   10
2 nofinal         ! # of variables; variables will save in zgoubi.FITVALS.out
2  1 -8.1  .5     ! Coupled: sum of drifts stays constant
5  12  0   2.     ! Parmtr #12 of elements #3 is the field value
2  1.E-5 200       ! # of constraints; penalty; max. nmbr of iterations
1  1 2 8  17. 1. 0 ! Cnstrnt #1 : R12=17. after last drift (lmnt #8)
1  3 4 8 -.8  1. 0 ! Cnstrnt #2 : R34=-.8 after last drift
'MATRIX'                                                                                                    11
1  0
'END'                                                                                                       12

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =       2501.730 kG*cm

                                         CALCUL  DES  TRAJECTOIRES

                              OBJET  (5)  FORME  DE     13 POINTS 



                                 Y (cm)        T (mrd)       Z (cm)        P (mrd)       S (cm)        dp/p 
                   Sampling :    1.0000E-02    1.0000E-02    1.0000E-02    1.0000E-02    0.0000E+00    1.0000E-04
Reference trajectory #    1 :    0.0000E+00    0.0000E+00    0.0000E+00    0.0000E+00    0.0000E+00     1.000    

************************************************************************************************************************************
      2  Keyword, label(s) :  ESL         '                                                                            IPASS= 1


                              Drift,  length =   200.00000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  2.0000000E+02  0.00000E+00

 Cumulative length of optical axis =    2.00000000     m   ;  Time  (for reference rigidity & particle) =   1.068463E-08 s 

************************************************************************************************************************************
      3  Keyword, label(s) :  QUADRUPO    3                                                                            IPASS= 1


      -----  QUADRUPOLE  : 
                Length  of  element  =    40.000000      cm
                Bore  radius      RO =    15.000      cm                              B/RO^IM/Brho (strength)
               B-QUADRUPOLE  = -6.0000000E+00 kG   (i.e.,  -6.0000000E+00 * SCAL)    -1.5988936E+00

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   7.5000E+00

                    Integration step :   5.000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           40.000     0.000     0.000     0.000     0.000            1
  A    1  1.0000     0.010     0.000     0.000     0.000           40.000     0.011     0.000     0.000     0.000            2
  A    1  1.0000    -0.010     0.000     0.000     0.000           40.000    -0.011    -0.000     0.000     0.000            3
  A    1  1.0000     0.000     0.010     0.000     0.000           40.000     0.003     0.000     0.000     0.000            4
  A    1  1.0000     0.000    -0.010     0.000     0.000           40.000    -0.003    -0.000     0.000     0.000            5
  A    1  1.0000     0.000     0.000     0.010     0.000           40.000     0.000     0.000     0.009    -0.000            6
  A    1  1.0000     0.000     0.000    -0.010     0.000           40.000     0.000     0.000    -0.009     0.000            7
  A    1  1.0000     0.000     0.000     0.000     0.010           40.000     0.000     0.000     0.002    -0.000            8
  A    1  1.0000     0.000     0.000     0.000    -0.010           40.000     0.000     0.000    -0.002     0.000            9
  A    1  1.0001     0.000     0.000     0.000     0.000           40.000     0.000     0.000     0.000     0.000           10
  A    1  0.9999     0.000     0.000     0.000     0.000           40.000     0.000     0.000     0.000     0.000           11
  A    1  1.0000     0.000     0.000     0.000     0.000           40.000     0.000     0.000     0.000     0.000           12
  A    1  1.0000     0.000     0.000     0.000     0.000           40.000     0.000     0.000     0.000     0.000           13

     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    2.40000000     m ;  Time  (for ref. rigidity & particle) =   1.282155E-08 s 

************************************************************************************************************************************
      4  Keyword, label(s) :  ESL                                                                                      IPASS= 1


                              Drift,  length =    30.00000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  2.7000000E+02  1.33426E-03

 Cumulative length of optical axis =    2.70000000     m   ;  Time  (for reference rigidity & particle) =   1.442425E-08 s 

************************************************************************************************************************************
      5  Keyword, label(s) :  QUADRUPO    5                                                                            IPASS= 1


      -----  QUADRUPOLE  : 
                Length  of  element  =    40.000000      cm
                Bore  radius      RO =    15.000      cm                              B/RO^IM/Brho (strength)
               B-QUADRUPOLE  =  3.0000000E+00 kG   (i.e.,   3.0000000E+00 * SCAL)     7.9944678E-01

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   7.5000E+00

                    Integration step :   5.000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           40.000     0.000     0.000     0.000     0.000            1
  A    1  1.0000     0.010     0.000     0.000     0.000           40.000     0.015     0.000     0.000     0.000            2
  A    1  1.0000    -0.010     0.000     0.000     0.000           40.000    -0.015    -0.000     0.000     0.000            3
  A    1  1.0000     0.000     0.010     0.000     0.000           40.000     0.004     0.000     0.000     0.000            4
  A    1  1.0000     0.000    -0.010     0.000     0.000           40.000    -0.004    -0.000     0.000     0.000            5
  A    1  1.0000     0.000     0.000     0.010     0.000           40.000     0.000     0.000     0.005    -0.000            6
  A    1  1.0000     0.000     0.000    -0.010     0.000           40.000     0.000     0.000    -0.005     0.000            7
  A    1  1.0000     0.000     0.000     0.000     0.010           40.000     0.000     0.000     0.002     0.000            8
  A    1  1.0000     0.000     0.000     0.000    -0.010           40.000     0.000     0.000    -0.002    -0.000            9
  A    1  1.0001     0.000     0.000     0.000     0.000           40.000     0.000     0.000     0.000     0.000           10
  A    1  0.9999     0.000     0.000     0.000     0.000           40.000     0.000     0.000     0.000     0.000           11
  A    1  1.0000     0.000     0.000     0.000     0.000           40.000     0.000     0.000     0.000     0.000           12
  A    1  1.0000     0.000     0.000     0.000     0.000           40.000     0.000     0.000     0.000     0.000           13

     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    3.10000000     m ;  Time  (for ref. rigidity & particle) =   1.656117E-08 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  ESL                                                                                      IPASS= 1


                              Drift,  length =    30.00000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  3.4000000E+02  2.66851E-03

 Cumulative length of optical axis =    3.40000000     m   ;  Time  (for reference rigidity & particle) =   1.816387E-08 s 

************************************************************************************************************************************
      7  Keyword, label(s) :  QUADRUPO    7                                                                            IPASS= 1


      -----  QUADRUPOLE  : 
                Length  of  element  =    40.000000      cm
                Bore  radius      RO =    15.000      cm                              B/RO^IM/Brho (strength)
               B-QUADRUPOLE  = -8.0000000E+00 kG   (i.e.,  -8.0000000E+00 * SCAL)    -2.1318581E+00

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   7.5000E+00

                    Integration step :   5.000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           40.000     0.000     0.000     0.000     0.000            1
  A    1  1.0000     0.010     0.000     0.000     0.000           40.000     0.019     0.000     0.000     0.000            2
  A    1  1.0000    -0.010     0.000     0.000     0.000           40.000    -0.019    -0.000     0.000     0.000            3
  A    1  1.0000     0.000     0.010     0.000     0.000           40.000     0.006     0.000     0.000     0.000            4
  A    1  1.0000     0.000    -0.010     0.000     0.000           40.000    -0.006    -0.000     0.000     0.000            5
  A    1  1.0000     0.000     0.000     0.010     0.000           40.000     0.000     0.000     0.001    -0.000            6
  A    1  1.0000     0.000     0.000    -0.010     0.000           40.000     0.000     0.000    -0.001     0.000            7
  A    1  1.0000     0.000     0.000     0.000     0.010           40.000     0.000     0.000     0.002    -0.000            8
  A    1  1.0000     0.000     0.000     0.000    -0.010           40.000     0.000     0.000    -0.002     0.000            9
  A    1  1.0001     0.000     0.000     0.000     0.000           40.000     0.000     0.000     0.000     0.000           10
  A    1  0.9999     0.000     0.000     0.000     0.000           40.000     0.000     0.000     0.000     0.000           11
  A    1  1.0000     0.000     0.000     0.000     0.000           40.000     0.000     0.000     0.000     0.000           12
  A    1  1.0000     0.000     0.000     0.000     0.000           40.000     0.000     0.000     0.000     0.000           13

     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    3.80000000     m ;  Time  (for ref. rigidity & particle) =   2.030079E-08 s 

************************************************************************************************************************************
      8  Keyword, label(s) :  ESL                                                                                      IPASS= 1


                              Drift,  length =   200.00000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  5.8000000E+02  4.00277E-03

 Cumulative length of optical axis =    5.80000000     m   ;  Time  (for reference rigidity & particle) =   3.098542E-08 s 

************************************************************************************************************************************
      9  Keyword, label(s) :  MATRIX                                                                                   IPASS= 1


  Reference, before change of frame (particle #  1  - D-1,Y,T,Z,s,time)  : 
   0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00   5.80000000E+02   4.00276914E-03

           Frame for MATRIX calculation moved by :
            XC =    0.000 cm , YC =    0.000 cm ,   A =  0.00000 deg  ( = 0.000000 rad )


  Reference, after change of frame (particle #  1  - D-1,Y,T,Z,s,time)  : 
   0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00   5.80000000E+02   4.00276914E-03

  Reference particle (#     1), path length :   580.00000     cm  relative momentum :    1.00000    


                  TRANSFER  MATRIX  ORDRE  1  (MKSA units)

           5.25705         16.9606         0.00000         0.00000         0.00000         0.00000    
           1.66150         5.55066         0.00000         0.00000         0.00000         0.00000    
           0.00000         0.00000        -1.15003        -1.03949         0.00000         0.00000    
           0.00000         0.00000       -0.643559        -1.45123         0.00000         0.00000    
           0.00000         0.00000         0.00000         0.00000         1.00000         0.00000    
           0.00000         0.00000         0.00000         0.00000         0.00000         1.00000    

          DetY-1 =      -0.0000160143,    DetZ-1 =      -0.0000161106

          R12=0 at   -3.056     m,        R34=0 at  -0.7163     m

      First order symplectic conditions (expected values = 0) :
        -1.6014E-05   -1.6111E-05     0.000         0.000         0.000         0.000    


                                        ----------------------------------------
                                        -  EDWARDS AND TENG`S PARAMETRIZATION  -
                                        ----------------------------------------


      COUPLING PARAMETERS:
                           - r:     0.00000000
                           - C:
                                   0.00000000      0.00000000
                                   0.00000000      0.00000000

                                                                              MODE 1               MODE 2

      FRACTIONAL PART OF THE BETATRON TUNES IN THE DECOUPLED FRAME:        0.00000000           0.00000000

      EDWARDS-TENG`S PARAMETERS:
                                - ALPHA:                                    0.00000000           0.00000000
                                - BETA:                                     0.00000000           0.00000000
                                - GAMMA:                                    0.00000000           0.00000000

      HAMILTONIAN PERTURBATION PARAMETERS:

                           - DISTANCE FROM THE NEAREST DIFFERENCE LINEAR RESONANCE:     0.00000000
                           - COUPLING STRENGTH OF THE DIFFERENCE LINEAR RESONANCE:      0.00000000

                           - DISTANCE FROM THE NEAREST SUM LINEAR RESONANCE:            0.00000000
                           - UNPERTURBED HORIZONTAL TUNE:                               0.00000000
                           - UNPERTURBED VERTICAL TUNE:                                 0.00000000

      P MATRIX :

    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00

************************************************************************************************************************************
     10  Keyword, label(s) :  FIT                                                                                      IPASS= 1

     Pgm main. FITING=.T., FIT procedure launched.

           variable #            1       IR =            2 ,   ok.
           variable #            1       IP =            1 ,   ok.
           variable  element 2,  prmtr # 1 : coupled  with  element 8,  prmtr # 1
           variable #            1       XC.=           -8 ,   ok.
           variable #            1       .XC=            1 ,   ok.
           variable #            2       IR =            5 ,   ok.
           variable #            2       IP =           12 ,   ok.
           constraint #            1       IR =            8 ,   ok.
           constraint #            2       IR =            8 ,   ok.

                    FIT  variables  and  constraints  in  good  order,  FIT  will proceed. 
 STATUS OF VARIABLES  (Iteration #   201 /    200 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1     1    100.        176.       176.06859       300.      9.145E-04  ESL        '                    -                   
   8   1     1    100.        176.       176.06859       300.      9.145E-04
   5   2    12   -3.00        1.25       1.2516872       9.00      5.487E-05  QUADRUPO   5                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-05)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  1   1   2     8    1.700000E+01    1.000E+00    1.695758E+01    4.38E-02 ESL        -                    -                    0
  1   3   4     8   -8.000000E-01    1.000E+00   -9.981689E-01    9.56E-01 ESL        -                    -                    0
 Fit reached penalty value   4.1070E-02



   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     11  Keyword, label(s) :  MATRIX                                                                                   IPASS= 1


  Reference, before change of frame (particle #  1  - D-1,Y,T,Z,s,time)  : 
   0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00   5.32137174E+02   4.00276914E-03

           Frame for MATRIX calculation moved by :
            XC =    0.000 cm , YC =    0.000 cm ,   A =  0.00000 deg  ( = 0.000000 rad )


  Reference, after change of frame (particle #  1  - D-1,Y,T,Z,s,time)  : 
   0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00   5.32137174E+02   4.00276914E-03

  Reference particle (#     1), path length :   532.13717     cm  relative momentum :    1.00000    


                  TRANSFER  MATRIX  ORDRE  1  (MKSA units)

           5.88401         16.9576         0.00000         0.00000         0.00000         0.00000    
           2.09183         6.19855         0.00000         0.00000         0.00000         0.00000    
           0.00000         0.00000        -1.16561       -0.998169         0.00000         0.00000    
           0.00000         0.00000       -0.689892        -1.44869         0.00000         0.00000    
           0.00000         0.00000         0.00000         0.00000         1.00000         0.00000    
           0.00000         0.00000         0.00000         0.00000         0.00000         1.00000    

          DetY-1 =      -0.0000147905,    DetZ-1 =      -0.0000150101

          R12=0 at   -2.736     m,        R34=0 at  -0.6890     m

      First order symplectic conditions (expected values = 0) :
        -1.4790E-05   -1.5010E-05     0.000         0.000         0.000         0.000    


                                        ----------------------------------------
                                        -  EDWARDS AND TENG`S PARAMETRIZATION  -
                                        ----------------------------------------


      COUPLING PARAMETERS:
                           - r:     0.00000000
                           - C:
                                   0.00000000      0.00000000
                                   0.00000000      0.00000000

                                                                              MODE 1               MODE 2

      FRACTIONAL PART OF THE BETATRON TUNES IN THE DECOUPLED FRAME:        0.00000000           0.00000000

      EDWARDS-TENG`S PARAMETERS:
                                - ALPHA:                                    0.00000000           0.00000000
                                - BETA:                                     0.00000000           0.00000000
                                - GAMMA:                                    0.00000000           0.00000000

      HAMILTONIAN PERTURBATION PARAMETERS:

                           - DISTANCE FROM THE NEAREST DIFFERENCE LINEAR RESONANCE:     0.00000000
                           - COUPLING STRENGTH OF THE DIFFERENCE LINEAR RESONANCE:      0.00000000

                           - DISTANCE FROM THE NEAREST SUM LINEAR RESONANCE:            0.00000000
                           - UNPERTURBED HORIZONTAL TUNE:                               0.00000000
                           - UNPERTURBED VERTICAL TUNE:                                 0.00000000

      P MATRIX :

    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00

************************************************************************************************************************************
     12  Keyword, label(s) :  END                                                                                      IPASS= 1

                            13 particles have been launched
                     Made  it  to  the  end :     13

************************************************************************************************************************************
 Pgm zgoubi : Execution ended normally, upon keyword END or FIN
   
 File in:   zgoubi.dat
 File out:  zgoubi.res

  Zgoubi, author's dvlpmnt version.
  Job  started  on  14-02-2022,  at  10:08:12 
  JOB  ENDED  ON    14-02-2022,  AT  10:08:13 

   CPU time, total :     1.3988520000000000     

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
