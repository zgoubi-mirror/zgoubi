Translated from TRANSPORT file
'OBJET'                                                                                                      1
667.128190           ! reference rigidity (kG.cm) = *****************  ,  G.gamma =   3393668241.6026
5
   1.000000E-03   1.000000E-02   1.000000E-03   1.000000E-02   1.000000E-03  1.000000E-04
   0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00  1.00000000E+00
 
'PARTICUL'                                                                                                   2
MUON+
!'OPTIONS'                                                                                                    2
!0 1
!.plt 2
!'FAISCEAU'                                                                                                   2
 
'DRIFT'                                                                                                      3
   0.000000
 
'MARKER'  AK81_S                                                                                             4
 
'CHANGREF'  4.00                                                                                             5
YS   0.00000000 XS -24.00000000
'DIPOLE'    4.00                                                                                             6
2  .Dipole
35.7713304      85.52925518                                  ! AT, RM
   15.6744246    7.7976277    0.0000000    0.0000000    0.0000000     ! ACNT,  HNORM, indices
8.0000   -1.                                     ! lambda      face 1
4 .1455 2.2670 -.6395 1.1558 0. 0. 0.
0.0000000     20.0969058107   1.E6 -1.E6 1.E6 1.E6            ! omga+,  theta
0.0000   -1.                                     ! lambda      face 2
4 .1455 2.2670 -.6395 1.1558 0. 0. 0.
-20.0969058       0.0000000   1.E6 -1.E6 1.E6 1.E6            ! omga-,  theta
0.  0.                     face 3
4 .1455 2.2670 -.6395 1.1558 0. 0. 0.
0. 0.   1.E6 -1.E6 1.E6 1.E6  0.
2   10.
.3
2    88.8327276    -0.2735703    85.5292552     0.0000000             ! KPOS, RE, TE, RS, TS
'CHANGREF'  4.00                                                                                             7
XS  -0.00000000
'CHANGREF'  2.00                                                                                             8
XS  -0.00000000
'DIPOLE'    2.00                                                                                             9
2  .Dipole
35.7713304      85.52925518                                  ! AT, RM
    0.0000000    7.7976277    0.0000000    0.0000000    0.0000000     ! ACNT,  HNORM, indices
0.0000   -1.                                     ! lambda      face 1
4 .1455 2.2670 -.6395 1.1558 0. 0. 0.
0.0000000       0.0000000   1.E6 -1.E6 1.E6 1.E6            ! omga+,  theta
8.0000   -1.                                     ! lambda      face 2
4 .1455 2.2670 -.6395 1.1558 0. 0. 0.
-20.0969058      -20.0969058107   1.E6 -1.E6 1.E6 1.E6            ! omga-,  theta
0.  0.                     face 3
4 .1455 2.2670 -.6395 1.1558 0. 0. 0.
0. 0.   1.E6 -1.E6 1.E6 1.E6  0.
2  10.
.3
2    85.5292552    -0.0000000    88.8327276     0.2735703             ! KPOS, RE, TE, RS, TS
'CHANGREF'  2.00                                                                                            10
XS -24.00000000 YS   0.00000000
 
'MARKER'  AK81_E                                                                                            11
 
!             This FIT: change field in two half dipoles (equal values) so to get expected deviaiton.
 ! 'FIT2'                                                                                                      12
 ! 1                                         ! Vary field in first half of dipole, coupled (difference
 ! 6 5 -9.5 .8                                           ! maintained constant) to field in 2nd half.
 ! 3
 ! 2 3 1 7    0. 0.1  0                                                         ! Nul angle at exit of bend.
 ! 3 1 2 #End 0. 1.0  0                                                      ! Nul posiiton at exit of bend.
 ! 3 1 3 #End 0. 0.1  0                                                         ! Nul angle at exit of bend.
'FAISCEAU'                                                                                                  13
 
'MATRIX'                                                                                                    14
1  0
'FAISCEAU'                                                                                                  15
 
'END'
