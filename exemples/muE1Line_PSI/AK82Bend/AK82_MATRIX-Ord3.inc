Translated from TRANSPORT file
'OBJET'                                                                                                      1
667.128190           ! reference rigidity (kG.cm) = *****************  ,  G.gamma =   3393668241.6026
5
   1.000000E-03   1.000000E-02   1.000000E-03   1.000000E-02   1.000000E-03  1.000000E-04
   0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00  1.00000000E+00
 
'PARTICUL'                                                                                                   2
MUON+
!'OPTIONS'                                                                                                    2
!0 1
!.plt 2
 
'DRIFT'                                                                                                      3
   0.000000
 
'MARKER'  AK82_S                                                                                             4
 
'CHANGREF'  4.00                                                                                             5
YS  -0.00000000 XS -24.00000000
'DIPOLE'    4.00                                                                                             6
2  .Dipole
14.8075127     188.63866672                                  ! AT, RM
    7.2506368    3.5353457    0.0000000    0.0000000    0.0000000     ! ACNT,  HNORM, indices
8.0000   -1.                                     ! lambda      face 1
4 .1455 2.2670 -.6395 1.1558 0. 0. 0.
0.0000000      7.0000000   1.E6 -1.E6 1.E6 1.E6            ! omga+,  theta
0.0000   -1.                                     ! lambda      face 2
4 .1455 2.2670 -.6395 1.1558 0. 0. 0.
-7.5568759      -0.0000000   1.E6 -1.E6 1.E6 1.E6            ! omga-,  theta
0.  0.                     face 3
4 .1455 2.2670 -.6395 1.1558 0. 0. 0.
0. 0.   1.E6 -1.E6 1.E6 1.E6  0.
2   64
.3
2   190.1592664    -0.1265475   188.6386667     0.0000000             ! KPOS, RE, TE, RS, TS
'CHANGREF'  4.00                                                                                             7
XS  -0.00000000
'CHANGREF'  3.00                                                                                             8
XS  -0.00000000
'DIPOLE'    3.00                                                                                             9
2  .Dipole
14.8075127     188.63866672                                  ! AT, RM
    0.0000000    3.5353457    0.0000000    0.0000000    0.0000000     ! ACNT,  HNORM, indices
0.0000   -1.                                     ! lambda      face 1
4 .1455 2.2670 -.6395 1.1558 0. 0. 0.
0.0000000       0.0000000   1.E6 -1.E6 1.E6 1.E6            ! omga+,  theta
8.0000   -1.                                     ! lambda      face 2
4 .1455 2.2670 -.6395 1.1558 0. 0. 0.
-7.5568759      -7.0000000   1.E6 -1.E6 1.E6 1.E6            ! omga-,  theta
0.  0.                     face 3
4 .1455 2.2670 -.6395 1.1558 0. 0. 0.
0. 0.   1.E6 -1.E6 1.E6 1.E6  0.
2   64
.3
2   188.6386667    -0.0000000   190.1592664     0.1265475             ! KPOS, RE, TE, RS, TS
'CHANGREF'  3.00                                                                                            10
XS -24.00000000 YS   0.00000000
 
'MARKER'  AK82_E                                                                                            11
 
!             This FIT: change field in two half dipoles (equal values) so to get expected deviaiton.
 ! 'FIT2'                                                                                                      12
 ! 1                                         ! Vary field in first half of dipole, coupled (difference
 ! 6 5 -9.5 3                                           ! maintained constant) to field in 2nd half.
 ! 3
 ! 3 1 3 7  0. .1 0                                                         ! Nul angle at exit of bend.
 ! 3 1 2 10 0. 1. 0                                                         ! Nul angle at exit of bend.
 ! 3 1 3 10 0. .1 0                                                         ! Nul angle at exit of bend.
'FAISCEAU'                                                                                                  13
 
'DRIFT'                                                                                                     14
   0.000000
'MATRIX'                                                                                                    15
1  0
'FAISCEAU'                                                                                                  16
 
'END'
