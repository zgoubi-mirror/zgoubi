Test signs, with wedges.
! ourne dans un sens...
'OBJET'                                                                                                      1
1000.
2
3 1
0. 0. 0. 0. 0. 1. 'a'
0. 0. 0. 0. 0. 1. 'b'
0. 0. 0. 0. 0. 1. 'c'
1 1 1
'PARTICUL'                                                                                                   2
PROTON
'SPNTRK'                                                                                                     3
4
1 0 0
0 1 0
0 0 1
 
 
'BEND'                                                                                                       4
0
247.30039  0.  1.57776
20. 8. 0.04276056667                           ! wedge is +2.45deg
4 .2401  1.8639  -.5572  .3904 0. 0. 0.
20. 8. 0.04276056667                           ! wedge is +2.45deg
4 .2401  1.8639  -.5572  .3904 0. 0. 0.
#30|10|30   bend\
3 0. 0. -.1963495408
 
'FAISCEAU'                                                                                                   5
'SPNPRT'                                                                                                     6
 
! ... tourne dans l'autre.
'OBJET'                                                                                                      7
1000.
2
3 1
0. 0. 0. 0. 0. 1. 'a'
0. 0. 0. 0. 0. 1. 'b'
0. 0. 0. 0. 0. 1. 'c'
1 1 1
'PARTICUL'                                                                                                   8
PROTON
'SPNTRK'                                                                                                     9
4
1 0 0
0 1 0
0 0 1
 
 
'YMY'                                                                                                       10
'BEND'                                                                                                      11
0
247.30039  0.  1.57776
20. 8. 0.04276056667                           ! wedge is +2.45deg
4 .2401  1.8639  -.5572  .3904 0. 0. 0.
20. 8. 0.04276056667                           ! wedge is +2.45deg
4 .2401  1.8639  -.5572  .3904 0. 0. 0.
#30|10|30   bend\
3 0. 0. -.1963495408
'YMY'                                                                                                       12
 
'FAISCEAU'                                                                                                  13
'SPNPRT'                                                                                                    14
 
'END'                                                                                                       15

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =       1000.000 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 1

     Particle  properties :
     PROTON
                     Mass          =    938.272        MeV/c2
                     Charge        =   1.602176E-19    C     
                     G  factor     =    1.79285              
                     COM life-time =   1.000000E+99    s     

              Reference  data :
                    mag. rigidity (kG.cm)   :   1000.0000      =p/q, such that dev.=B*L/rigidity
                    mass (MeV/c2)           :   938.27208    
                    momentum (MeV/c)        :   299.79246    
                    energy, total (MeV)     :   985.00255    
                    energy, kinetic (MeV)   :   46.730465    
                    beta = v/c              :  0.3043570386    
                    gamma                   :   1.049804813    
                    beta*gamma              :  0.3195154838    
                    G*gamma                 :   1.882139776    
                    electric rigidity (MeV) :   91.24394470    =T[eV]*(gamma+1)/gamma, such that dev.=E*L/rigidity
  
 I, AMQ(1,I), AMQ(2,I)/QE, P/Pref, v/c, time, s :
  
     1   9.38272081E+02  1.00000000E+00  1.00000000E+00  3.04357039E-01  0.00000000E+00  0.00000000E+00
     2   9.38272081E+02  1.00000000E+00  1.00000000E+00  3.04357039E-01  0.00000000E+00  0.00000000E+00
     3   9.38272081E+02  1.00000000E+00  1.00000000E+00  3.04357039E-01  0.00000000E+00  0.00000000E+00

************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 1


                Spin  tracking  requested.


                          Particle  mass          =    938.2721     MeV/c2
                          Gyromagnetic  factor  G =    1.792847    

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 may be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO    =      1000.000 kG*cm
                               beta    =    0.30435704
                               gamma   =   1.04980481
                               gamma*G =   1.8821397761


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  BEND                                                                                     IPASS= 1


      +++++        BEND  : 

                Length    =   2.473004E+02 cm
                Arc length    =   2.488966E+02 cm
                Deviation    =   2.250000E+01 deg.,    3.926991E-01 rad
                GAP   =   0.000000E+00 cm
                Gradient   =   0.000000E+00 kG/cm
                Grad-prime   =   0.000000E+00 kG/cm^2

                Field  =  1.5777600E+00  kG   (i.e.,   1.5777600E+00 * SCAL)
                Reference curvature radius (Brho/B) =   6.3380996E+02 cm
                Skew  angle  =   0.000000E+00  rad

               Entrance  face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

               Exit      face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

     CHXC - KPOS =  3 :  automatic positioning of element, 
        XCE, YCE, ALE =   0.000000000       0.000000000     -0.1963495408     cm/cm/rad

                    Integration steps inside entrance|body|exit region : 
                                      1.33       20.7       1.33     (cm)
                                    30            10      30

  A    1  1.0000     0.000     0.000     0.000     0.000          287.300     0.003    -0.196     0.000     0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000          287.300     0.003    -0.196     0.000     0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000          287.300     0.003    -0.196     0.000     0.000            3

     QUASEX - KPOS =  3 :  Automatic  positionning  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle = -0.19634954     RAD


 Cumulative length of optical axis =    2.48896601     m ;  Time  (for ref. rigidity & particle) =   2.727815E-08 s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

a  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.003    0.017    0.000    0.000   2.488852E+02     1
               Time of flight (mus) :  2.72769002E-02 mass (MeV/c2) :   938.272    
b  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.003    0.017    0.000    0.000   2.488852E+02     2
               Time of flight (mus) :  2.72769002E-02 mass (MeV/c2) :   938.272    
c  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.003    0.017    0.000    0.000   2.488852E+02     3
               Time of flight (mus) :  2.72769002E-02 mass (MeV/c2) :   938.272    

************************************************************************************************************************************
      6  Keyword, label(s) :  SPNPRT                                                                                   IPASS= 1



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.470899   0.021833   0.333333   0.577350   1.882140  28.230253  19.961803


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   |Si,Sf|   (Z,Sf_yz) (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_yz : projection of Sf on YZ plane)

 a  1  1.000000  0.000000  0.000000  1.000000     0.739098 -0.673598  0.000000  1.000000      1.0498   42.345   90.000   90.000    1
 b  1  0.000000  1.000000  0.000000  1.000000     0.673598  0.739098  0.000000  1.000000      1.0498   42.345   90.000   90.000    2
 c  1  0.000000  0.000000  1.000000  1.000000     0.000000  0.000000  1.000000  1.000000      1.0498    0.000   45.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  7.3910E-01  7.3910E-01 -6.7360E-01 -6.7360E-01  0.0000E+00  0.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.04980E+00     1   1
  6.7360E-01  6.7360E-01  7.3910E-01  7.3910E-01  0.0000E+00  0.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.04980E+00     2   1
  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.04980E+00     3   1

************************************************************************************************************************************
      7  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =       1000.000 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      8  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 1


************************************************************************************************************************************
      9  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 1


                Spin  tracking  requested.


                          Particle  mass          =    938.2721     MeV/c2
                          Gyromagnetic  factor  G =    1.792847    

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 may be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO    =      1000.000 kG*cm
                               beta    =    0.30435704
                               gamma   =   1.04980481
                               gamma*G =   1.8821397761


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
     10  Keyword, label(s) :  YMY                                                                                      IPASS= 1


                                        *****************   YMOINSY   ******************

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -0.000000E+00 -0.000000E+00 -0.000000E+00 -0.000000E+00   0.0000000E+00   0.00000E+00
TRAJ #1 SX, SY, SZ, |S| :  1    1.000000E+00   0.000000E+00   0.000000E+00   1.000000E+00

************************************************************************************************************************************
     11  Keyword, label(s) :  BEND                                                                                     IPASS= 1


      +++++        BEND  : 

                Length    =   2.473004E+02 cm
                Arc length    =   2.488966E+02 cm
                Deviation    =   2.250000E+01 deg.,    3.926991E-01 rad
                GAP   =   0.000000E+00 cm
                Gradient   =   0.000000E+00 kG/cm
                Grad-prime   =   0.000000E+00 kG/cm^2

                Field  =  1.5777600E+00  kG   (i.e.,   1.5777600E+00 * SCAL)
                Reference curvature radius (Brho/B) =   6.3380996E+02 cm
                Skew  angle  =   0.000000E+00  rad

               Entrance  face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

               Exit      face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

     CHXC - KPOS =  3 :  automatic positioning of element, 
        XCE, YCE, ALE =   0.000000000       0.000000000     -0.1963495408     cm/cm/rad

                    Integration steps inside entrance|body|exit region : 
                                      1.33       20.7       1.33     (cm)
                                    30            10      30

  A    1  1.0000     0.000     0.000     0.000     0.000          287.300     0.003    -0.196     0.000     0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000          287.300     0.003    -0.196     0.000     0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000          287.300     0.003    -0.196     0.000     0.000            3

     QUASEX - KPOS =  3 :  Automatic  positionning  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle = -0.19634954     RAD


 Cumulative length of optical axis =    4.97793202     m ;  Time  (for ref. rigidity & particle) =   5.455630E-08 s 

************************************************************************************************************************************
     12  Keyword, label(s) :  YMY                                                                                      IPASS= 1


                                        *****************   YMOINSY   ******************

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -2.841830E-03 -1.673144E-02 -0.000000E+00 -0.000000E+00   2.4888520E+02   2.72769E-02
TRAJ #1 SX, SY, SZ, |S| :  1    7.390978E-01   6.735981E-01  -8.249198E-17   1.000000E+00

************************************************************************************************************************************
     13  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #     12)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

a  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000   -0.003   -0.017   -0.000   -0.000   2.488852E+02     1
               Time of flight (mus) :  2.72769002E-02 mass (MeV/c2) :   938.272    
b  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000   -0.003   -0.017   -0.000   -0.000   2.488852E+02     2
               Time of flight (mus) :  2.72769002E-02 mass (MeV/c2) :   938.272    
c  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000   -0.003   -0.017   -0.000   -0.000   2.488852E+02     3
               Time of flight (mus) :  2.72769002E-02 mass (MeV/c2) :   938.272    

************************************************************************************************************************************
     14  Keyword, label(s) :  SPNPRT                                                                                   IPASS= 1



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.021833   0.470899   0.333333   0.577350   1.882140  28.230253  19.961803


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   |Si,Sf|   (Z,Sf_yz) (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_yz : projection of Sf on YZ plane)

 a  1  1.000000  0.000000  0.000000  1.000000     0.739098  0.673598 -0.000000  1.000000      1.0498   42.345   90.000   90.000    1
 b  1  0.000000  1.000000  0.000000  1.000000    -0.673598  0.739098 -0.000000  1.000000      1.0498   42.345   90.000   90.000    2
 c  1  0.000000  0.000000  1.000000  1.000000    -0.000000  0.000000  1.000000  1.000000      1.0498    0.000   45.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  7.3910E-01  7.3910E-01 -6.7360E-01  6.7360E-01 -8.2492E-17  0.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.04980E+00     1   1
 -6.7360E-01  6.7360E-01  7.3910E-01  7.3910E-01 -2.1298E-16  0.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.04980E+00     2   1
 -8.2492E-17  0.0000E+00  0.0000E+00  2.1298E-16  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.04980E+00     3   1

************************************************************************************************************************************
     15  Keyword, label(s) :  END                                                                                      IPASS= 1


                             3 particles have been launched
                     Made  it  to  the  end :      3

************************************************************************************************************************************
 Pgm zgoubi : Execution ended normally, upon keyword END or FIN
   
 File in:   BEND_B+B-.dat
 File out:  zgoubi.res

  Zgoubi, author's dvlpmnt version.
  Job  started  on  24-11-2020,  at  10:35:03 
  JOB  ENDED  ON    24-11-2020,  AT  10:35:03 

   CPU time, total :     2.9829999999999995E-003
