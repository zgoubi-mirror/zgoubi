

set xlabel "step size [cm]"  font 'roman, 20'
set ylabel " x [10^{-9}m],  x' [10^{-6} rad]"   font 'roman, 20'
set y2label "{/Symbol d q}_s/{/Symbol q}_s"   font 'roman, 24'

set k c l  font "roman, 18"

set xtics mirror
set ytics nomirror
set y2tics

set logscale x
set logscale x2

stpi = 0.01
stpf = 10.
nrblt = 40
dstp = (stpf-stpi)/(nrblt-1)
nbtraj = 3

ttaInf = -30.

cm2nm = 1e7
rd2urd = 1e6

set yrange [0.3:1.7]

plot   \
  "zgoubi.fai" u ($38>1 && $26==1 ? stpi + dstp*$0/nbtraj :1/0):($10 *cm2nm) axes x1y1 w p pt 5 ps .7 lc rgb "red" tit "x (left axis)" ,\
  "zgoubi.fai" u ($38>1 && $26==1 ? stpi + dstp*$0/nbtraj :1/0):($11 *rd2urd) axes x1y1 w lp pt 6 ps .7 lc rgb "blue" tit "x' (left axis)" ,\
  "zgoubi.fai" u ($38>1 && $26==1 ? stpi + dstp*$0/nbtraj :1/0):(-(atan($21/$20)/(4.*atan(1.))*180. -ttaInf)/ttaInf) axes x1y2 w lp pt 7 ps .7 lc rgb "green" tit "spin-R (right axis)" 


# "zgoubi.fai" u (stpi + dstp*$0):($11) axes x1y2 w p pt 5 ps 1 lc rgb "red" tit "x'" 

 set terminal postscript eps blacktext color enh    # size 8.3cm,4cm "Times-Roman" 12 
 set output "gnuplot_scanTraj.eps" 
 replot 
 set terminal X11 
 unset output 

pause .2
quit
