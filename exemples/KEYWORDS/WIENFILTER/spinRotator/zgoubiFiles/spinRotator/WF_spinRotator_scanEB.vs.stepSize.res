Wien filter used as spin rotator. After E. Wang, EIC pCDR, BNL, 2019.
 'OBJET'                                                                                                      1
2.3114795386518345           ! Rigidity of a 350 keV electron.
2
3  1                         ! 3 electrons, reason: see SPNTRK below.
0.  0. 0. 0. 0. 1. 'o'
0.  0. 0. 0. 0. 1. 'o'
0.  0. 0. 0. 0. 1. 'o'
1 1 1
 'PARTICUL'                                                                                                   2
0.510998946  1.602176487D-19 1.159652181D-3 0. 0.
 
 'SPNTRK'                     ! Allows chceking rotation of all 3 spin components.                            3
4                             ! (they are computed independently by zgoubi)
1. 0. 0.
0. 1. 0.
0. 0. 1.
 
 'MARKER'  #S_WF_tuned        ! Needed by 'INCLUDE'                                                           4
 'WIENFILT'                                                                                                   5
0
0.5  -984079.312413395  0.004078507588806  1
0. 0. 0.     ! 20. 5. 5.      ! Hard-edge entrance face.
0.2401  1.8639  -0.5572  0.3904 0. 0.
0.2401  1.8639  -0.5572  0.3904 0. 0.
0. 0. 0.     ! 20. 5. 5.      ! Hard-edge exit face.
0.2401  1.8639  -0.5572  0.3904 0. 0.
0.2401  1.8639  -0.5572  0.3904 0. 0.
.1
1. 0. 0. 0.
 'MARKER'  #E_WF_tuned        ! Needed by 'INCLUDE'                                                           6
 
 'FIT2'                                                                                                       7
2   nofinal
5 11 0 1.05
5 12 0 1.05
6 1E-15
3 1 2 #End 0. 1. 0
3 1 3 #End 0. 1. 0
10.2 1 1 #End 0.523598775598299 1. 0
10 1 4 #End 1. 1. 0
10 2 4 #End 1. 1. 0
10 3 4 #End 1. 1. 0
 
 'FAISCEAU'     ! Get some trajectory and some                                                                8
 'SPNPRT' MATRIX  PRINT ! spin outcomes, including spin rotation matrix.                                      9
 
 'REBELOTE'                                                                                                  10
40 0.1 0 1
1
WIENFILT 80 0.001:5.
 
! For just 4 values of step size:
! 'REBELOTE'                                                                                                  10
!4 0.1 0 1
!1
!WIENFILT 80 0.001 .01 .1 1.
 
 'SYSTEM'                                                                                                    11
2
gnuplot <./gnuplot_scanEB.gnu
okular ./gnuplot_scanEB.eps &
 
 'END'                                                                                                       12

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 1

     Particle  properties :
     Particle name unknown.
                     Mass          =   0.510999        MeV/c2
                     Charge        =   1.602176E-19    C     
                     G  factor     =   1.159652E-03          

              Reference  data :
                    mag. rigidity (kG.cm)   :   2.3114795      =p/q, such that dev.=B*L/rigidity
                    mass (MeV/c2)           :  0.51099895    
                    momentum (MeV/c)        :  0.69296413    
                    energy, total (MeV)     :  0.86099896    
                    energy, kinetic (MeV)   :  0.35000002    
                    beta = v/c              :  0.8048373616    
                    gamma                   :   1.684932950    
                    beta*gamma              :   1.356096990    
                    G*gamma                 :  1.9539361703E-03
                    electric rigidity (MeV) :  0.5577234241    =T[eV]*(gamma+1)/gamma, such that dev.=E*L/rigidity
  
 I, AMQ(1,I), AMQ(2,I)/QE, P/Pref, v/c, time, s :
  
     1   5.10998946E-01  1.00000000E+00  1.00000000E+00  8.04837362E-01  0.00000000E+00  0.00000000E+00
     2   5.10998946E-01  1.00000000E+00  1.00000000E+00  8.04837362E-01  0.00000000E+00  0.00000000E+00
     3   5.10998946E-01  1.00000000E+00  1.00000000E+00  8.04837362E-01  0.00000000E+00  0.00000000E+00

************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 1


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 may be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO   =         2.311 KG*CM
                               BETA   =    0.804837
                               GAMMA*G =   0.001954


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WF_tuned                                                                  IPASS= 1


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 1

Entrance hard edge is to be implemented
Exit hard edge is to be implemented

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.84079E+05 V/m
                               B                       =  4.07851E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =   1.0000     C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80484    

                FACE  D'ENTREE
                DX =   0.000,  LAMBDA_E, LAMBDA_B =   0.000  0.000 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )

                FACE  DE  SORTIE
                DX =   0.000,  LAMBDA_E, LAMBDA_B =   0.000  0.000 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )

  ***  Warning : hard-edge fringe model entails vertical wedge focusing simulated with
                  first order kick  ***

                    Integration step :  0.1000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           50.000    -0.000    -0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           50.000    -0.000    -0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           50.000    -0.000    -0.000    -0.000    -0.000            3

     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.0000000     RAD


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WF_tuned                                                                  IPASS= 1


************************************************************************************************************************************
      7  Keyword, label(s) :  FIT2                                                                                     IPASS= 1

     FIT procedure launched.

           variable #            1       IR =            5 ,   ok.
           variable #            1       IP =           11 ,   ok.
           variable #            2       IR =            5 ,   ok.
           variable #            2       IP =           12 ,   ok.
           constraint #            1       IR =            6 ,   ok.
           constraint #            1       I  =            1 ,   ok.
           constraint #            2       IR =            6 ,   ok.
           constraint #            2       I  =            1 ,   ok.
           constraint #            3       IR =            6 ,   ok.
           constraint #            4       IR =            6 ,   ok.
           constraint #            5       IR =            6 ,   ok.
           constraint #            6       IR =            6 ,   ok.

                    FIT  variables  and  constraints  in  good  order,  FIT  will proceed. 

 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -2.017E+06  -9.841E+05  -982939.37      4.920E+04  0.111      WIENFILT   -                    -                   
   5   2    12  -2.039E-04   4.079E-03  4.07378306E-03  8.361E-03  4.585E-10  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00   -5.419835E-10    Infinity MARKER     #E_WF_tuned          -                    0
  3   1   3     6    0.000000E+00    1.000E+00   -2.118186E-08    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   3   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 Fit reached penalty value   5.4575E-16

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00  -5.419835E-12  -2.118186E-11        3        0    0.000      (Y,T)         1
   0.0000E+00   0.0000E+00   1.0000E+00  -1.348959E-17  -5.395836E-17        3        3    1.000      (Z,P)         1
   0.0000E+00   0.0000E+00   1.0000E+00   2.072245E-03   3.500000E-01        3        3    1.000      (t,K)         1

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   6.525304E-55   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  SPNPRT      MATRIX                PRINT                                                  IPASS= 1



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455342   0.122008   0.333333   0.577350   0.001954  20.000000  14.142136


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.866025 -0.500000 -0.000000  1.000000      1.6849   30.000  -90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500000  0.866025  0.000000  1.000000      1.6849   30.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   -0.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6603E-01  8.6603E-01 -5.0000E-01 -5.0000E-01 -2.4297E-17 -2.4297E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      1   1
  5.0000E-01  5.0000E-01  8.6603E-01  8.6603E-01  3.4144E-18  3.4144E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      2   1
  1.9342E-17  1.9342E-17 -1.5084E-17 -1.5084E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      3   1


                  Spin transfer matrix, momentum group # 1 :

          0.866025        0.500000        1.934226E-17
         -0.500000        0.866025       -1.508421E-17
         -2.429733E-17    3.414397E-18     1.00000    

     Trace =       2.7320507977,    ;   spin precession acos((trace-1)/2) =      30.0000005637 deg
     Rotation axis :   ( 0.0000,  0.0000, -1.0000)   ->   angle to (X,Y) plane,  angle to X axis :   -90.0000,    67.0282  degree
     Spin  tune  Qs =     8.3333E-02

************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 1


 Pgm rebel. At pass #    1/  41.  In element #    5,  parameter # 80  changed to    1.00000000E-03   (was    1.00000000E-01)

                                -----  REBELOTE  -----

     End of pass #        1 through the optical structure 

                     Total of          3 particles have been launched

     Multiple pass, 
          from element #     1 : OBJET     /label1=                    /label2=                    
                             to  REBELOTE  /label1=                    /label2=                    
     ending at pass #      41 at element #    10 : REBELOTE  /label1=                    /label2=                    


     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -2.017E+06  -9.829E+05  -982939.51      4.920E+04  3.704E-02  WIENFILT   -                    -                   
   5   2    12  -2.039E-04   4.074E-03  4.07378365E-03  8.361E-03  1.538E-10  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    4.536320E-10    Infinity MARKER     #E_WF_tuned          -                    0
  3   1   3     6    0.000000E+00    1.000E+00    1.774225E-08    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   3   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 Fit reached penalty value   3.4817E-16

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 2

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.536320E-12   1.774225E-11        3        3    1.000      (Y,T)         2
   0.0000E+00   0.0000E+00   1.0000E+00  -1.348959E-17  -5.395836E-17        3        0    0.000      (Z,P)         2
   0.0000E+00   0.0000E+00   1.0000E+00   2.072245E-03   3.500000E-01        3        3    1.000      (t,K)         2

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   3.798227E-65

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  SPNPRT      MATRIX                PRINT                                                  IPASS= 2



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455342   0.122008   0.333333   0.577350   0.001954  20.000000  14.142136


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.866025 -0.500000 -0.000000  1.000000      1.6849   30.000  -90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500000  0.866025  0.000000  1.000000      1.6849   30.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   -0.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6603E-01  8.6603E-01 -5.0000E-01 -5.0000E-01 -2.4297E-17 -2.4297E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      1   1
  5.0000E-01  5.0000E-01  8.6603E-01  8.6603E-01  3.3928E-18  3.4144E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      2   1
  1.9342E-17  1.9345E-17 -1.5086E-17 -1.5084E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      3   1


                  Spin transfer matrix, momentum group # 1 :

          0.866025        0.500000        1.934522E-17
         -0.500000        0.866025       -1.508635E-17
         -2.429667E-17    3.392777E-18     1.00000    

     Trace =       2.7320508018,    ;   spin precession acos((trace-1)/2) =      30.0000003300 deg
     Rotation axis :   ( 0.0000,  0.0000, -1.0000)   ->   angle to (X,Y) plane,  angle to X axis :   -90.0000,    67.0509  degree
     Spin  tune  Qs =     8.3333E-02

************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 2


 Pgm rebel. At pass #    2/  41.  In element #    5,  parameter # 80  changed to    1.29179487E-01   (was    1.00000000E-03)

                                -----  REBELOTE  -----

     End of pass #        2 through the optical structure 

                     Total of          6 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -2.017E+06  -9.829E+05  -982939.24      4.920E+04  0.113      WIENFILT   -                    -                   
   5   2    12  -2.039E-04   4.074E-03  4.07378252E-03  8.361E-03  4.665E-10  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00   -2.724334E-10    Infinity MARKER     #E_WF_tuned          -                    0
  3   1   3     6    0.000000E+00    1.000E+00   -1.064712E-08    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   3   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 Fit reached penalty value   1.4626E-16

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 3

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00  -2.724334E-12  -1.064712E-11        3        3    1.000      (Y,T)         3
   0.0000E+00   0.0000E+00   1.0000E+00  -1.348959E-17  -5.395835E-17        3        3    1.000      (Z,P)         3
   0.0000E+00   0.0000E+00   1.0000E+00   2.072245E-03   3.500000E-01        3        3    1.000      (t,K)         3

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  SPNPRT      MATRIX                PRINT                                                  IPASS= 3



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455342   0.122008   0.333333   0.577350   0.001954  20.000000  14.142135


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.866025 -0.500000 -0.000000  1.000000      1.6849   30.000  -90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500000  0.866025  0.000000  1.000000      1.6849   30.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   -0.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6603E-01  8.6603E-01 -5.0000E-01 -5.0000E-01 -2.4298E-17 -2.4297E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      1   1
  5.0000E-01  5.0000E-01  8.6603E-01  8.6603E-01  3.3928E-18  3.4208E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      2   1
  1.9341E-17  1.9345E-17 -1.5086E-17 -1.5084E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      3   1


                  Spin transfer matrix, momentum group # 1 :

          0.866025        0.500000        1.934139E-17
         -0.500000        0.866025       -1.508357E-17
         -2.429752E-17    3.420765E-18     1.00000    

     Trace =       2.7320508133,    ;   spin precession acos((trace-1)/2) =      29.9999996717 deg
     Rotation axis :   ( 0.0000,  0.0000, -1.0000)   ->   angle to (X,Y) plane,  angle to X axis :   -90.0000,    67.0215  degree
     Spin  tune  Qs =     8.3333E-02

************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 3


 Pgm rebel. At pass #    3/  41.  In element #    5,  parameter # 80  changed to    2.57358974E-01   (was    1.29179487E-01)

                                -----  REBELOTE  -----

     End of pass #        3 through the optical structure 

                     Total of          9 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -2.017E+06  -9.829E+05  -982938.49      4.920E+04  0.125      WIENFILT   -                    -                   
   5   2    12  -2.039E-04   4.074E-03  4.07377941E-03  8.361E-03  5.180E-10  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00   -2.549024E-10    Infinity MARKER     #E_WF_tuned          -                    0
  3   1   3     6    0.000000E+00    1.000E+00   -9.961389E-09    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   3   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 Fit reached penalty value   2.0916E-16

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 4

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00  -2.549024E-12  -9.961389E-12        3        0    0.000      (Y,T)         4
   0.0000E+00   0.0000E+00   1.0000E+00  -1.348958E-17  -5.395831E-17        3        0    0.000      (Z,P)         4
   0.0000E+00   0.0000E+00   1.0000E+00   2.072245E-03   3.500000E-01        3        0    0.000      (t,K)         4

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.631326E-55   0.000000E+00   0.000000E+00   2.489206E-60
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   2.489206E-60   0.000000E+00   0.000000E+00   3.798227E-65

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  SPNPRT      MATRIX                PRINT                                                  IPASS= 4



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455342   0.122008   0.333333   0.577350   0.001954  20.000000  14.142135


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.866025 -0.500000 -0.000000  1.000000      1.6849   30.000  -90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500000  0.866025  0.000000  1.000000      1.6849   30.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   -0.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6603E-01  8.6603E-01 -5.0000E-01 -5.0000E-01 -2.4298E-17 -2.4297E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      1   1
  5.0000E-01  5.0000E-01  8.6603E-01  8.6603E-01  3.3928E-18  3.4487E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      2   1
  1.9338E-17  1.9345E-17 -1.5086E-17 -1.5081E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      3   1


                  Spin transfer matrix, momentum group # 1 :

          0.866025        0.500000        1.933756E-17
         -0.500000        0.866025       -1.508080E-17
         -2.429836E-17    3.448695E-18     1.00000    

     Trace =       2.7320508181,    ;   spin precession acos((trace-1)/2) =      29.9999993995 deg
     Rotation axis :   ( 0.0000,  0.0000, -1.0000)   ->   angle to (X,Y) plane,  angle to X axis :   -90.0000,    66.9921  degree
     Spin  tune  Qs =     8.3333E-02

************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 4


 Pgm rebel. At pass #    4/  41.  In element #    5,  parameter # 80  changed to    3.85538462E-01   (was    2.57358974E-01)

                                -----  REBELOTE  -----

     End of pass #        4 through the optical structure 

                     Total of         12 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -2.017E+06  -9.829E+05  -982937.29      4.920E+04  0.112      WIENFILT   -                    -                   
   5   2    12  -2.039E-04   4.074E-03  4.07377445E-03  8.361E-03  4.658E-10  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    7.634618E-10    Infinity MARKER     #E_WF_tuned          -                    0
  3   1   3     6    0.000000E+00    1.000E+00    2.983371E-08    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   3   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 Fit reached penalty value   9.3305E-16

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 5

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   7.634618E-12   2.983371E-11        3        3    1.000      (Y,T)         5
   0.0000E+00   0.0000E+00   1.0000E+00  -1.348956E-17  -5.395824E-17        3        0    0.000      (Z,P)         5
   0.0000E+00   0.0000E+00   1.0000E+00   2.072245E-03   3.500000E-01        3        3    1.000      (t,K)         5

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   3.798227E-65

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  SPNPRT      MATRIX                PRINT                                                  IPASS= 5



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455342   0.122008   0.333333   0.577350   0.001954  20.000000  14.142136


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.866025 -0.500000 -0.000000  1.000000      1.6849   30.000  -90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500000  0.866025  0.000000  1.000000      1.6849   30.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   -0.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6603E-01  8.6603E-01 -5.0000E-01 -5.0000E-01 -2.4299E-17 -2.4297E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      1   1
  5.0000E-01  5.0000E-01  8.6603E-01  8.6603E-01  3.3928E-18  3.4766E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      2   1
  1.9334E-17  1.9345E-17 -1.5086E-17 -1.5078E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      3   1


                  Spin transfer matrix, momentum group # 1 :

          0.866025        0.500000        1.933373E-17
         -0.500000        0.866025       -1.507803E-17
         -2.429921E-17    3.476595E-18     1.00000    

     Trace =       2.7320508011,    ;   spin precession acos((trace-1)/2) =      30.0000003732 deg
     Rotation axis :   ( 0.0000,  0.0000, -1.0000)   ->   angle to (X,Y) plane,  angle to X axis :   -90.0000,    66.9627  degree
     Spin  tune  Qs =     8.3333E-02

************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 5


 Pgm rebel. At pass #    5/  41.  In element #    5,  parameter # 80  changed to    5.13717949E-01   (was    3.85538462E-01)

                                -----  REBELOTE  -----

     End of pass #        5 through the optical structure 

                     Total of         15 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -2.017E+06  -9.829E+05  -982935.55      4.920E+04  9.275E-02  WIENFILT   -                    -                   
   5   2    12  -2.039E-04   4.074E-03  4.07376724E-03  8.361E-03  3.849E-10  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00   -7.498283E-10    Infinity MARKER     #E_WF_tuned          -                    0
  3   1   3     6    0.000000E+00    1.000E+00   -2.929915E-08    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    Infinity MARKER     #E_WF_tuned          -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    Infinity MARKER     #E_WF_tuned          -                    0
 10   3   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 Fit reached penalty value   8.7077E-16

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 6

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00  -7.498283E-12  -2.929915E-11        3        3    1.000      (Y,T)         6
   0.0000E+00   0.0000E+00   1.0000E+00  -1.348954E-17  -5.395815E-17        3        3    1.000      (Z,P)         6
   0.0000E+00   0.0000E+00   1.0000E+00   2.072245E-03   3.500000E-01        3        0    0.000      (t,K)         6

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  SPNPRT      MATRIX                PRINT                                                  IPASS= 6



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455342   0.122008   0.333333   0.577350   0.001954  20.000000  14.142136


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.866025 -0.500000 -0.000000  1.000000      1.6849   30.000  -90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500000  0.866025  0.000000  1.000000      1.6849   30.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   -0.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6603E-01  8.6603E-01 -5.0000E-01 -5.0000E-01 -2.4300E-17 -2.4297E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      1   1
  5.0000E-01  5.0000E-01  8.6603E-01  8.6603E-01  3.3928E-18  3.5045E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      2   1
  1.9330E-17  1.9345E-17 -1.5086E-17 -1.5075E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      3   1


                  Spin transfer matrix, momentum group # 1 :

          0.866025        0.500000        1.932990E-17
         -0.500000        0.866025       -1.507525E-17
         -2.430004E-17    3.504454E-18     1.00000    

     Trace =       2.7320508110,    ;   spin precession acos((trace-1)/2) =      29.9999998035 deg
     Rotation axis :   ( 0.0000,  0.0000, -1.0000)   ->   angle to (X,Y) plane,  angle to X axis :   -90.0000,    66.9334  degree
     Spin  tune  Qs =     8.3333E-02

************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 6


 Pgm rebel. At pass #    6/  41.  In element #    5,  parameter # 80  changed to    6.41897436E-01   (was    5.13717949E-01)

                                -----  REBELOTE  -----

     End of pass #        6 through the optical structure 

                     Total of         18 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -2.017E+06  -9.829E+05  -982933.37      4.920E+04  0.121      WIENFILT   -                    -                   
   5   2    12  -2.039E-04   4.074E-03  4.07375821E-03  8.361E-03  5.008E-10  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00   -4.368889E-10    Infinity MARKER     #E_WF_tuned          -                    0
  3   1   3     6    0.000000E+00    1.000E+00   -1.707013E-08    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   3   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 Fit reached penalty value   5.7250E-16

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 7

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00  -4.368889E-12  -1.707013E-11        3        3    1.000      (Y,T)         7
   0.0000E+00   0.0000E+00   1.0000E+00  -1.348951E-17  -5.395803E-17        3        3    1.000      (Z,P)         7
   0.0000E+00   0.0000E+00   1.0000E+00   2.072245E-03   3.500000E-01        3        0    0.000      (t,K)         7

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  SPNPRT      MATRIX                PRINT                                                  IPASS= 7



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455342   0.122008   0.333333   0.577350   0.001954  20.000001  14.142136


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.866025 -0.500000 -0.000000  1.000000      1.6849   30.000  -90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500000  0.866025  0.000000  1.000000      1.6849   30.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   -0.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6603E-01  8.6603E-01 -5.0000E-01 -5.0000E-01 -2.4301E-17 -2.4297E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      1   1
  5.0000E-01  5.0000E-01  8.6603E-01  8.6603E-01  3.3928E-18  3.5326E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      2   1
  1.9326E-17  1.9345E-17 -1.5086E-17 -1.5072E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      3   1


                  Spin transfer matrix, momentum group # 1 :

          0.866025        0.500000        1.932601E-17
         -0.500000        0.866025       -1.507243E-17
         -2.430091E-17    3.532559E-18     1.00000    

     Trace =       2.7320507908,    ;   spin precession acos((trace-1)/2) =      30.0000009603 deg
     Rotation axis :   ( 0.0000,  0.0000, -1.0000)   ->   angle to (X,Y) plane,  angle to X axis :   -90.0000,    66.9038  degree
     Spin  tune  Qs =     8.3333E-02

************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 7


 Pgm rebel. At pass #    7/  41.  In element #    5,  parameter # 80  changed to    7.70076923E-01   (was    6.41897436E-01)

                                -----  REBELOTE  -----

     End of pass #        7 through the optical structure 

                     Total of         21 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -2.017E+06  -9.829E+05  -982930.64      4.920E+04  0.107      WIENFILT   -                    -                   
   5   2    12  -2.039E-04   4.074E-03  4.07374689E-03  8.361E-03  4.454E-10  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00   -4.476942E-10    Infinity MARKER     #E_WF_tuned          -                    0
  3   1   3     6    0.000000E+00    1.000E+00   -1.749120E-08    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    Infinity MARKER     #E_WF_tuned          -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    Infinity MARKER     #E_WF_tuned          -                    0
 10   3   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 Fit reached penalty value   3.3862E-16

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 8

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00  -4.476942E-12  -1.749120E-11        3        3    1.000      (Y,T)         8
   0.0000E+00   0.0000E+00   1.0000E+00  -1.348947E-17  -5.395788E-17        3        3    1.000      (Z,P)         8
   0.0000E+00   0.0000E+00   1.0000E+00   2.072245E-03   3.500000E-01        3        3    1.000      (t,K)         8

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  SPNPRT      MATRIX                PRINT                                                  IPASS= 8



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455342   0.122008   0.333333   0.577350   0.001954  20.000000  14.142136


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.866025 -0.500000 -0.000000  1.000000      1.6849   30.000  -90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500000  0.866025  0.000000  1.000000      1.6849   30.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   -0.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6603E-01  8.6603E-01 -5.0000E-01 -5.0000E-01 -2.4302E-17 -2.4297E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      1   1
  5.0000E-01  5.0000E-01  8.6603E-01  8.6603E-01  3.3928E-18  3.5606E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      2   1
  1.9322E-17  1.9345E-17 -1.5086E-17 -1.5070E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      3   1


                  Spin transfer matrix, momentum group # 1 :

          0.866025        0.500000        1.932212E-17
         -0.500000        0.866025       -1.506962E-17
         -2.430176E-17    3.560561E-18     1.00000    

     Trace =       2.7320508019,    ;   spin precession acos((trace-1)/2) =      30.0000003265 deg
     Rotation axis :   ( 0.0000,  0.0000, -1.0000)   ->   angle to (X,Y) plane,  angle to X axis :   -90.0000,    66.8744  degree
     Spin  tune  Qs =     8.3333E-02

************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 8


 Pgm rebel. At pass #    8/  41.  In element #    5,  parameter # 80  changed to    8.98256410E-01   (was    7.70076923E-01)

                                -----  REBELOTE  -----

     End of pass #        8 through the optical structure 

                     Total of         24 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -2.017E+06  -9.829E+05  -982927.49      4.920E+04  0.104      WIENFILT   -                    -                   
   5   2    12  -2.039E-04   4.074E-03  4.07373385E-03  8.361E-03  4.310E-10  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00   -2.875034E-10    Infinity MARKER     #E_WF_tuned          -                    0
  3   1   3     6    0.000000E+00    1.000E+00   -1.123200E-08    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   3   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 Fit reached penalty value   1.3056E-16

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 9

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00  -2.875034E-12  -1.123200E-11        3        3    1.000      (Y,T)         9
   0.0000E+00   0.0000E+00   1.0000E+00  -1.348943E-17  -5.395770E-17        3        0    0.000      (Z,P)         9
   0.0000E+00   0.0000E+00   1.0000E+00   2.072245E-03   3.500000E-01        3        3    1.000      (t,K)         9

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   2.373892E-66   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  SPNPRT      MATRIX                PRINT                                                  IPASS= 9



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455342   0.122008   0.333333   0.577350   0.001954  20.000000  14.142136


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.866025 -0.500000 -0.000000  1.000000      1.6849   30.000  -90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500000  0.866025  0.000000  1.000000      1.6849   30.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   -0.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6603E-01  8.6603E-01 -5.0000E-01 -5.0000E-01 -2.4303E-17 -2.4297E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      1   1
  5.0000E-01  5.0000E-01  8.6603E-01  8.6603E-01  3.3928E-18  3.5878E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      2   1
  1.9318E-17  1.9345E-17 -1.5086E-17 -1.5067E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      3   1


                  Spin transfer matrix, momentum group # 1 :

          0.866025        0.500000        1.931840E-17
         -0.500000        0.866025       -1.506694E-17
         -2.430254E-17    3.587823E-18     1.00000    

     Trace =       2.7320508055,    ;   spin precession acos((trace-1)/2) =      30.0000001191 deg
     Rotation axis :   ( 0.0000,  0.0000, -1.0000)   ->   angle to (X,Y) plane,  angle to X axis :   -90.0000,    66.8457  degree
     Spin  tune  Qs =     8.3333E-02

************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 9


 Pgm rebel. At pass #    9/  41.  In element #    5,  parameter # 80  changed to    1.02643590E+00   (was    8.98256410E-01)

                                -----  REBELOTE  -----

     End of pass #        9 through the optical structure 

                     Total of         27 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -2.017E+06  -9.829E+05  -982923.85      4.920E+04  6.316E-02  WIENFILT   -                    -                   
   5   2    12  -2.039E-04   4.074E-03  4.07371874E-03  8.361E-03  2.622E-10  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    4.207425E-10    Infinity MARKER     #E_WF_tuned          -                    0
  3   1   3     6    0.000000E+00    1.000E+00    1.643627E-08    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   3   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 Fit reached penalty value   3.9675E-16

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 10

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.207425E-12   1.643627E-11        3        3    1.000      (Y,T)        10
   0.0000E+00   0.0000E+00   1.0000E+00  -1.348938E-17  -5.395750E-17        3        3    1.000      (Z,P)        10
   0.0000E+00   0.0000E+00   1.0000E+00   2.072245E-03   3.500000E-01        3        0    0.000      (t,K)        10

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  SPNPRT      MATRIX                PRINT                                                  IPASS= 10



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455342   0.122008   0.333333   0.577350   0.001954  20.000000  14.142136


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.866025 -0.500000 -0.000000  1.000000      1.6849   30.000  -90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500000  0.866025  0.000000  1.000000      1.6849   30.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   -0.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6603E-01  8.6603E-01 -5.0000E-01 -5.0000E-01 -2.4303E-17 -2.4297E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      1   1
  5.0000E-01  5.0000E-01  8.6603E-01  8.6603E-01  3.3928E-18  3.6156E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      2   1
  1.9315E-17  1.9345E-17 -1.5086E-17 -1.5064E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      3   1


                  Spin transfer matrix, momentum group # 1 :

          0.866025        0.500000        1.931455E-17
         -0.500000        0.866025       -1.506415E-17
         -2.430338E-17    3.615643E-18     1.00000    

     Trace =       2.7320507963,    ;   spin precession acos((trace-1)/2) =      30.0000006442 deg
     Rotation axis :   ( 0.0000,  0.0000, -1.0000)   ->   angle to (X,Y) plane,  angle to X axis :   -90.0000,    66.8165  degree
     Spin  tune  Qs =     8.3333E-02

************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 10


 Pgm rebel. At pass #   10/  41.  In element #    5,  parameter # 80  changed to    1.15461538E+00   (was    1.02643590E+00)

                                -----  REBELOTE  -----

     End of pass #       10 through the optical structure 

                     Total of         30 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -2.017E+06  -9.829E+05  -982919.71      4.920E+04  9.442E-02  WIENFILT   -                    -                   
   5   2    12  -2.039E-04   4.074E-03  4.07370158E-03  8.361E-03  3.915E-10  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00   -2.135300E-10    Infinity MARKER     #E_WF_tuned          -                    0
  3   1   3     6    0.000000E+00    1.000E+00   -8.341088E-09    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   3   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 Fit reached penalty value   9.9474E-16

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 11

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00  -2.135300E-12  -8.341088E-12        3        3    1.000      (Y,T)        11
   0.0000E+00   0.0000E+00   1.0000E+00  -1.348932E-17  -5.395728E-17        3        0    0.000      (Z,P)        11
   0.0000E+00   0.0000E+00   1.0000E+00   2.072245E-03   3.500000E-01        3        3    1.000      (t,K)        11

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   2.373892E-66   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  SPNPRT      MATRIX                PRINT                                                  IPASS= 11



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455342   0.122008   0.333333   0.577350   0.001954  20.000001  14.142136


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.866025 -0.500000 -0.000000  1.000000      1.6849   30.000  -90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500000  0.866025  0.000000  1.000000      1.6849   30.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   -0.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6603E-01  8.6603E-01 -5.0000E-01 -5.0000E-01 -2.4304E-17 -2.4297E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      1   1
  5.0000E-01  5.0000E-01  8.6603E-01  8.6603E-01  3.3928E-18  3.6433E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      2   1
  1.9311E-17  1.9345E-17 -1.5086E-17 -1.5061E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      3   1


                  Spin transfer matrix, momentum group # 1 :

          0.866025        0.500000        1.931072E-17
         -0.500000        0.866025       -1.506139E-17
         -2.430420E-17    3.643299E-18     1.00000    

     Trace =       2.7320507772,    ;   spin precession acos((trace-1)/2) =      30.0000017427 deg
     Rotation axis :   ( 0.0000,  0.0000, -1.0000)   ->   angle to (X,Y) plane,  angle to X axis :   -90.0000,    66.7874  degree
     Spin  tune  Qs =     8.3333E-02

************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 11


 Pgm rebel. At pass #   11/  41.  In element #    5,  parameter # 80  changed to    1.28279487E+00   (was    1.15461538E+00)

                                -----  REBELOTE  -----

     End of pass #       11 through the optical structure 

                     Total of         33 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -2.017E+06  -9.829E+05  -982914.85      4.920E+04  6.066E-02  WIENFILT   -                    -                   
   5   2    12  -2.039E-04   4.074E-03  4.07368146E-03  8.361E-03  2.515E-10  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00   -2.596666E-10    Infinity MARKER     #E_WF_tuned          -                    0
  3   1   3     6    0.000000E+00    1.000E+00   -1.014251E-08    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   3   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 Fit reached penalty value   1.7208E-16

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 12

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00  -2.596666E-12  -1.014251E-11        3        0    0.000      (Y,T)        12
   0.0000E+00   0.0000E+00   1.0000E+00  -1.348925E-17  -5.395701E-17        3        0    0.000      (Z,P)        12
   0.0000E+00   0.0000E+00   1.0000E+00   2.072245E-03   3.500000E-01        3        3    1.000      (t,K)        12

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   2.610122E-54   0.000000E+00   9.956824E-60
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   9.956824E-60   0.000000E+00   3.798227E-65

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  SPNPRT      MATRIX                PRINT                                                  IPASS= 12



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455342   0.122008   0.333333   0.577350   0.001954  20.000000  14.142135


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.866025 -0.500000 -0.000000  1.000000      1.6849   30.000  -90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500000  0.866025  0.000000  1.000000      1.6849   30.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   -0.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6603E-01  8.6603E-01 -5.0000E-01 -5.0000E-01 -2.4305E-17 -2.4297E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      1   1
  5.0000E-01  5.0000E-01  8.6603E-01  8.6603E-01  3.3928E-18  3.6726E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      2   1
  1.9306E-17  1.9345E-17 -1.5086E-17 -1.5058E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      3   1


                  Spin transfer matrix, momentum group # 1 :

          0.866025        0.500000        1.930649E-17
         -0.500000        0.866025       -1.505832E-17
         -2.430518E-17    3.672613E-18     1.00000    

     Trace =       2.7320508159,    ;   spin precession acos((trace-1)/2) =      29.9999995236 deg
     Rotation axis :   ( 0.0000,  0.0000, -1.0000)   ->   angle to (X,Y) plane,  angle to X axis :   -90.0000,    66.7568  degree
     Spin  tune  Qs =     8.3333E-02

************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 12


 Pgm rebel. At pass #   12/  41.  In element #    5,  parameter # 80  changed to    1.41097436E+00   (was    1.28279487E+00)

                                -----  REBELOTE  -----

     End of pass #       12 through the optical structure 

                     Total of         36 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -2.017E+06  -9.829E+05  -982909.97      4.920E+04  0.163      WIENFILT   -                    -                   
   5   2    12  -2.039E-04   4.074E-03  4.07366120E-03  8.361E-03  6.773E-10  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    5.092284E-10    Infinity MARKER     #E_WF_tuned          -                    0
  3   1   3     6    0.000000E+00    1.000E+00    1.988946E-08    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    Infinity MARKER     #E_WF_tuned          -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    Infinity MARKER     #E_WF_tuned          -                    0
 10   3   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 Fit reached penalty value   3.9613E-16

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 13

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   5.092284E-12   1.988946E-11        3        0    0.000      (Y,T)        13
   0.0000E+00   0.0000E+00   1.0000E+00  -1.348919E-17  -5.395674E-17        3        0    0.000      (Z,P)        13
   0.0000E+00   0.0000E+00   1.0000E+00   2.072245E-03   3.500000E-01        3        3    1.000      (t,K)        13

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   1.044049E-53   0.000000E+00  -1.991365E-59
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00  -1.991365E-59   0.000000E+00   3.798227E-65

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  SPNPRT      MATRIX                PRINT                                                  IPASS= 13



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455342   0.122008   0.333333   0.577350   0.001954  20.000000  14.142136


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.866025 -0.500000 -0.000000  1.000000      1.6849   30.000  -90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500000  0.866025  0.000000  1.000000      1.6849   30.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   -0.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6603E-01  8.6603E-01 -5.0000E-01 -5.0000E-01 -2.4306E-17 -2.4297E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      1   1
  5.0000E-01  5.0000E-01  8.6603E-01  8.6603E-01  3.3928E-18  3.6982E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      2   1
  1.9303E-17  1.9345E-17 -1.5086E-17 -1.5056E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      3   1


                  Spin transfer matrix, momentum group # 1 :

          0.866025        0.500000        1.930312E-17
         -0.500000        0.866025       -1.505591E-17
         -2.430581E-17    3.698232E-18     1.00000    

     Trace =       2.7320508081,    ;   spin precession acos((trace-1)/2) =      29.9999999699 deg
     Rotation axis :   ( 0.0000,  0.0000, -1.0000)   ->   angle to (X,Y) plane,  angle to X axis :   -90.0000,    66.7297  degree
     Spin  tune  Qs =     8.3333E-02

************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 13


 Pgm rebel. At pass #   13/  41.  In element #    5,  parameter # 80  changed to    1.53915385E+00   (was    1.41097436E+00)

                                -----  REBELOTE  -----

     End of pass #       13 through the optical structure 

                     Total of         39 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -2.017E+06  -9.829E+05  -982904.38      4.920E+04  0.132      WIENFILT   -                    -                   
   5   2    12  -2.039E-04   4.074E-03  4.07363807E-03  8.361E-03  5.488E-10  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00   -1.329964E-10    Infinity MARKER     #E_WF_tuned          -                    0
  3   1   3     6    0.000000E+00    1.000E+00   -5.194266E-09    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   3   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 Fit reached penalty value   1.9560E-16

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 14

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00  -1.329964E-12  -5.194266E-12        3        3    1.000      (Y,T)        14
   0.0000E+00   0.0000E+00   1.0000E+00  -1.348911E-17  -5.395644E-17        3        0    0.000      (Z,P)        14
   0.0000E+00   0.0000E+00   1.0000E+00   2.072245E-03   3.500000E-01        3        0    0.000      (t,K)        14

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   3.798227E-65

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  SPNPRT      MATRIX                PRINT                                                  IPASS= 14



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455342   0.122008   0.333333   0.577350   0.001954  20.000000  14.142135


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.866025 -0.500000 -0.000000  1.000000      1.6849   30.000  -90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500000  0.866025  0.000000  1.000000      1.6849   30.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   -0.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6603E-01  8.6603E-01 -5.0000E-01 -5.0000E-01 -2.4307E-17 -2.4297E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      1   1
  5.0000E-01  5.0000E-01  8.6603E-01  8.6603E-01  3.3928E-18  3.7257E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      2   1
  1.9299E-17  1.9345E-17 -1.5086E-17 -1.5053E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      3   1


                  Spin transfer matrix, momentum group # 1 :

          0.866025        0.500000        1.929930E-17
         -0.500000        0.866025       -1.505316E-17
         -2.430662E-17    3.725711E-18     1.00000    

     Trace =       2.7320508206,    ;   spin precession acos((trace-1)/2) =      29.9999992560 deg
     Rotation axis :   ( 0.0000,  0.0000, -1.0000)   ->   angle to (X,Y) plane,  angle to X axis :   -90.0000,    66.7009  degree
     Spin  tune  Qs =     8.3333E-02

************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 14


 Pgm rebel. At pass #   14/  41.  In element #    5,  parameter # 80  changed to    1.66733333E+00   (was    1.53915385E+00)

                                -----  REBELOTE  -----

     End of pass #       14 through the optical structure 

                     Total of         42 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -2.017E+06  -9.829E+05  -982897.86      4.920E+04  0.296      WIENFILT   -                    -                   
   5   2    12  -2.039E-04   4.074E-03  4.07361103E-03  8.361E-03  1.225E-09  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00   -6.967906E-10    Infinity MARKER     #E_WF_tuned          -                    0
  3   1   3     6    0.000000E+00    1.000E+00   -2.721100E-08    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    Infinity MARKER     #E_WF_tuned          -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    Infinity MARKER     #E_WF_tuned          -                    0
 10   3   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 Fit reached penalty value   8.1810E-16

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 15

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00  -6.967906E-12  -2.721100E-11        3        0    0.000      (Y,T)        15
   0.0000E+00   0.0000E+00   1.0000E+00  -1.348902E-17  -5.395608E-17        3        3    1.000      (Z,P)        15
   0.0000E+00   0.0000E+00   1.0000E+00   2.072245E-03   3.500000E-01        3        3    1.000      (t,K)        15

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   6.525304E-55   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  SPNPRT      MATRIX                PRINT                                                  IPASS= 15



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455342   0.122008   0.333333   0.577350   0.001954  20.000000  14.142135


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.866025 -0.500000 -0.000000  1.000000      1.6849   30.000  -90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500000  0.866025  0.000000  1.000000      1.6849   30.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   -0.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6603E-01  8.6603E-01 -5.0000E-01 -5.0000E-01 -2.4308E-17 -2.4297E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      1   1
  5.0000E-01  5.0000E-01  8.6603E-01  8.6603E-01  3.3928E-18  3.7567E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      2   1
  1.9295E-17  1.9345E-17 -1.5086E-17 -1.5050E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      3   1


                  Spin transfer matrix, momentum group # 1 :

          0.866025        0.500000        1.929466E-17
         -0.500000        0.866025       -1.504978E-17
         -2.430776E-17    3.756683E-18     1.00000    

     Trace =       2.7320508164,    ;   spin precession acos((trace-1)/2) =      29.9999994967 deg
     Rotation axis :   ( 0.0000,  0.0000, -1.0000)   ->   angle to (X,Y) plane,  angle to X axis :   -90.0000,    66.6686  degree
     Spin  tune  Qs =     8.3333E-02

************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 15


 Pgm rebel. At pass #   15/  41.  In element #    5,  parameter # 80  changed to    1.79551282E+00   (was    1.66733333E+00)

                                -----  REBELOTE  -----

     End of pass #       15 through the optical structure 

                     Total of         45 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -2.017E+06  -9.829E+05  -982891.57      4.920E+04  0.238      WIENFILT   -                    -                   
   5   2    12  -2.039E-04   4.074E-03  4.07358496E-03  8.361E-03  9.856E-10  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    1.710345E-10    Infinity MARKER     #E_WF_tuned          -                    0
  3   1   3     6    0.000000E+00    1.000E+00    6.678878E-09    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   3   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 Fit reached penalty value   4.4911E-16

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 16

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   1.710345E-12   6.678878E-12        3        3    1.000      (Y,T)        16
   0.0000E+00   0.0000E+00   1.0000E+00  -1.348893E-17  -5.395573E-17        3        0    0.000      (Z,P)        16
   0.0000E+00   0.0000E+00   1.0000E+00   2.072245E-03   3.500000E-01        3        0    0.000      (t,K)        16

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   2.373892E-66   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  SPNPRT      MATRIX                PRINT                                                  IPASS= 16



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455342   0.122008   0.333333   0.577350   0.001954  19.999999  14.142135


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.866025 -0.500000 -0.000000  1.000000      1.6849   30.000  -90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500000  0.866025  0.000000  1.000000      1.6849   30.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   -0.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6603E-01  8.6603E-01 -5.0000E-01 -5.0000E-01 -2.4308E-17 -2.4297E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      1   1
  5.0000E-01  5.0000E-01  8.6603E-01  8.6603E-01  3.3928E-18  3.7827E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      2   1
  1.9291E-17  1.9345E-17 -1.5086E-17 -1.5047E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      3   1


                  Spin transfer matrix, momentum group # 1 :

          0.866025        0.500000        1.929117E-17
         -0.500000        0.866025       -1.504728E-17
         -2.430843E-17    3.782681E-18     1.00000    

     Trace =       2.7320508277,    ;   spin precession acos((trace-1)/2) =      29.9999988477 deg
     Rotation axis :   ( 0.0000,  0.0000, -1.0000)   ->   angle to (X,Y) plane,  angle to X axis :   -90.0000,    66.6413  degree
     Spin  tune  Qs =     8.3333E-02

************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 16


 Pgm rebel. At pass #   16/  41.  In element #    5,  parameter # 80  changed to    1.92369231E+00   (was    1.79551282E+00)

                                -----  REBELOTE  -----

     End of pass #       16 through the optical structure 

                     Total of         48 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -2.017E+06  -9.829E+05  -982884.07      4.920E+04  6.665E-02  WIENFILT   -                    -                   
   5   2    12  -2.039E-04   4.074E-03  4.07355386E-03  8.361E-03  2.763E-10  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    1.960742E-10    Infinity MARKER     #E_WF_tuned          -                    0
  3   1   3     6    0.000000E+00    1.000E+00    7.656096E-09    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   3   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 Fit reached penalty value   1.5606E-16

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 17

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   1.960742E-12   7.656096E-12        3        3    1.000      (Y,T)        17
   0.0000E+00   0.0000E+00   1.0000E+00  -1.348883E-17  -5.395532E-17        3        3    1.000      (Z,P)        17
   0.0000E+00   0.0000E+00   1.0000E+00   2.072245E-03   3.500000E-01        3        0    0.000      (t,K)        17

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  SPNPRT      MATRIX                PRINT                                                  IPASS= 17



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455342   0.122008   0.333333   0.577350   0.001954  20.000000  14.142135


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.866025 -0.500000 -0.000000  1.000000      1.6849   30.000  -90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500000  0.866025  0.000000  1.000000      1.6849   30.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   -0.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6603E-01  8.6603E-01 -5.0000E-01 -5.0000E-01 -2.4309E-17 -2.4297E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      1   1
  5.0000E-01  5.0000E-01  8.6603E-01  8.6603E-01  3.3928E-18  3.8127E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      2   1
  1.9287E-17  1.9345E-17 -1.5086E-17 -1.5044E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      3   1


                  Spin transfer matrix, momentum group # 1 :

          0.866025        0.500000        1.928672E-17
         -0.500000        0.866025       -1.504406E-17
         -2.430948E-17    3.812741E-18     1.00000    

     Trace =       2.7320508174,    ;   spin precession acos((trace-1)/2) =      29.9999994345 deg
     Rotation axis :   ( 0.0000,  0.0000, -1.0000)   ->   angle to (X,Y) plane,  angle to X axis :   -90.0000,    66.6099  degree
     Spin  tune  Qs =     8.3333E-02

************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 17


 Pgm rebel. At pass #   17/  41.  In element #    5,  parameter # 80  changed to    2.05187179E+00   (was    1.92369231E+00)

                                -----  REBELOTE  -----

     End of pass #       17 through the optical structure 

                     Total of         51 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -2.017E+06  -9.829E+05  -982877.27      4.920E+04  9.748E-02  WIENFILT   -                    -                   
   5   2    12  -2.039E-04   4.074E-03  4.07352568E-03  8.361E-03  4.038E-10  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    6.571457E-10    Infinity MARKER     #E_WF_tuned          -                    0
  3   1   3     6    0.000000E+00    1.000E+00    2.565911E-08    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   3   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 Fit reached penalty value   9.0337E-16

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 18

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   6.571457E-12   2.565911E-11        3        0    0.000      (Y,T)        18
   0.0000E+00   0.0000E+00   1.0000E+00  -1.348874E-17  -5.395495E-17        3        3    1.000      (Z,P)        18
   0.0000E+00   0.0000E+00   1.0000E+00   2.072245E-03   3.500000E-01        3        0    0.000      (t,K)        18

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   1.044049E-53   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  SPNPRT      MATRIX                PRINT                                                  IPASS= 18



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455342   0.122008   0.333333   0.577350   0.001954  20.000001  14.142136


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.866025 -0.500000 -0.000000  1.000000      1.6849   30.000  -90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500000  0.866025  0.000000  1.000000      1.6849   30.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   -0.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6603E-01  8.6603E-01 -5.0000E-01 -5.0000E-01 -2.4310E-17 -2.4297E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      1   1
  5.0000E-01  5.0000E-01  8.6603E-01  8.6603E-01  3.3928E-18  3.8358E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      2   1
  1.9284E-17  1.9345E-17 -1.5086E-17 -1.5042E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      3   1


                  Spin transfer matrix, momentum group # 1 :

          0.866025        0.500000        1.928391E-17
         -0.500000        0.866025       -1.504208E-17
         -2.430986E-17    3.835751E-18     1.00000    

     Trace =       2.7320507919,    ;   spin precession acos((trace-1)/2) =      30.0000008960 deg
     Rotation axis :   ( 0.0000,  0.0000, -1.0000)   ->   angle to (X,Y) plane,  angle to X axis :   -90.0000,    66.5855  degree
     Spin  tune  Qs =     8.3333E-02

************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 18


 Pgm rebel. At pass #   18/  41.  In element #    5,  parameter # 80  changed to    2.18005128E+00   (was    2.05187179E+00)

                                -----  REBELOTE  -----

     End of pass #       18 through the optical structure 

                     Total of         54 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -2.017E+06  -9.829E+05  -982868.65      4.920E+04  6.730E-02  WIENFILT   -                    -                   
   5   2    12  -2.039E-04   4.074E-03  4.07348998E-03  8.361E-03  2.788E-10  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    2.577286E-10    Infinity MARKER     #E_WF_tuned          -                    0
  3   1   3     6    0.000000E+00    1.000E+00    1.006230E-08    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   3   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 Fit reached penalty value   1.0287E-16

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 19

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   2.577286E-12   1.006230E-11        3        0    0.000      (Y,T)        19
   0.0000E+00   0.0000E+00   1.0000E+00  -1.348862E-17  -5.395447E-17        3        3    1.000      (Z,P)        19
   0.0000E+00   0.0000E+00   1.0000E+00   2.072245E-03   3.500000E-01        3        0    0.000      (t,K)        19

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   2.610122E-54   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  SPNPRT      MATRIX                PRINT                                                  IPASS= 19



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455342   0.122008   0.333333   0.577350   0.001954  20.000000  14.142136


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.866025 -0.500000 -0.000000  1.000000      1.6849   30.000  -90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500000  0.866025  0.000000  1.000000      1.6849   30.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   -0.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6603E-01  8.6603E-01 -5.0000E-01 -5.0000E-01 -2.4311E-17 -2.4297E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      1   1
  5.0000E-01  5.0000E-01  8.6603E-01  8.6603E-01  3.3928E-18  3.8674E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      2   1
  1.9279E-17  1.9345E-17 -1.5086E-17 -1.5039E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      3   1


                  Spin transfer matrix, momentum group # 1 :

          0.866025        0.500000        1.927906E-17
         -0.500000        0.866025       -1.503855E-17
         -2.431108E-17    3.867445E-18     1.00000    

     Trace =       2.7320508063,    ;   spin precession acos((trace-1)/2) =      30.0000000715 deg
     Rotation axis :   ( 0.0000,  0.0000, -1.0000)   ->   angle to (X,Y) plane,  angle to X axis :   -90.0000,    66.5526  degree
     Spin  tune  Qs =     8.3333E-02

************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 19


 Pgm rebel. At pass #   19/  41.  In element #    5,  parameter # 80  changed to    2.30823077E+00   (was    2.18005128E+00)

                                -----  REBELOTE  -----

     End of pass #       19 through the optical structure 

                     Total of         57 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -2.017E+06  -9.829E+05  -982860.99      4.920E+04  0.144      WIENFILT   -                    -                   
   5   2    12  -2.039E-04   4.073E-03  4.07345821E-03  8.361E-03  5.963E-10  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    9.853135E-11    Infinity MARKER     #E_WF_tuned          -                    0
  3   1   3     6    0.000000E+00    1.000E+00    3.846820E-09    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235987E-01    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   3   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 Fit reached penalty value   6.9502E-16

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 20

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   9.853135E-13   3.846820E-12        3        3    1.000      (Y,T)        20
   0.0000E+00   0.0000E+00   1.0000E+00  -1.348851E-17  -5.395405E-17        3        3    1.000      (Z,P)        20
   0.0000E+00   0.0000E+00   1.0000E+00   2.072245E-03   3.500000E-01        3        0    0.000      (t,K)        20

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  SPNPRT      MATRIX                PRINT                                                  IPASS= 20



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455342   0.122008   0.333333   0.577350   0.001954  19.999999  14.142135


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.866025 -0.500000 -0.000000  1.000000      1.6849   30.000  -90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500000  0.866025  0.000000  1.000000      1.6849   30.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   -0.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6603E-01  8.6603E-01 -5.0000E-01 -5.0000E-01 -2.4311E-17 -2.4297E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      1   1
  5.0000E-01  5.0000E-01  8.6603E-01  8.6603E-01  3.3928E-18  3.8907E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      2   1
  1.9276E-17  1.9345E-17 -1.5086E-17 -1.5037E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      3   1


                  Spin transfer matrix, momentum group # 1 :

          0.866025        0.500000        1.927617E-17
         -0.500000        0.866025       -1.503651E-17
         -2.431149E-17    3.890706E-18     1.00000    

     Trace =       2.7320508336,    ;   spin precession acos((trace-1)/2) =      29.9999985057 deg
     Rotation axis :   ( 0.0000,  0.0000, -1.0000)   ->   angle to (X,Y) plane,  angle to X axis :   -90.0000,    66.5279  degree
     Spin  tune  Qs =     8.3333E-02

************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 20


 Pgm rebel. At pass #   20/  41.  In element #    5,  parameter # 80  changed to    2.43641026E+00   (was    2.30823077E+00)

                                -----  REBELOTE  -----

     End of pass #       20 through the optical structure 

                     Total of         60 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -2.017E+06  -9.829E+05  -982852.19      4.920E+04  0.102      WIENFILT   -                    -                   
   5   2    12  -2.039E-04   4.073E-03  4.07342175E-03  8.361E-03  4.221E-10  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    6.859648E-10    Infinity MARKER     #E_WF_tuned          -                    0
  3   1   3     6    0.000000E+00    1.000E+00    2.677979E-08    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   3   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 Fit reached penalty value   7.3118E-16

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 21

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   6.859648E-12   2.677979E-11        3        3    1.000      (Y,T)        21
   0.0000E+00   0.0000E+00   1.0000E+00  -1.348839E-17  -5.395357E-17        3        0    0.000      (Z,P)        21
   0.0000E+00   0.0000E+00   1.0000E+00   2.072245E-03   3.500000E-01        3        0    0.000      (t,K)        21

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   3.798227E-65

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  SPNPRT      MATRIX                PRINT                                                  IPASS= 21



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455342   0.122008   0.333333   0.577350   0.001954  20.000000  14.142136


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.866025 -0.500000 -0.000000  1.000000      1.6849   30.000  -90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500000  0.866025  0.000000  1.000000      1.6849   30.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   -0.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6603E-01  8.6603E-01 -5.0000E-01 -5.0000E-01 -2.4312E-17 -2.4297E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      1   1
  5.0000E-01  5.0000E-01  8.6603E-01  8.6603E-01  3.3928E-18  3.9172E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      2   1
  1.9273E-17  1.9345E-17 -1.5086E-17 -1.5034E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      3   1


                  Spin transfer matrix, momentum group # 1 :

          0.866025        0.500000        1.927252E-17
         -0.500000        0.866025       -1.503390E-17
         -2.431221E-17    3.917209E-18     1.00000    

     Trace =       2.7320508113,    ;   spin precession acos((trace-1)/2) =      29.9999997890 deg
     Rotation axis :   ( 0.0000,  0.0000, -1.0000)   ->   angle to (X,Y) plane,  angle to X axis :   -90.0000,    66.5001  degree
     Spin  tune  Qs =     8.3333E-02

************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 21


 Pgm rebel. At pass #   21/  41.  In element #    5,  parameter # 80  changed to    2.56458974E+00   (was    2.43641026E+00)

                                -----  REBELOTE  -----

     End of pass #       21 through the optical structure 

                     Total of         63 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -2.017E+06  -9.829E+05  -982842.85      4.920E+04  6.148E-02  WIENFILT   -                    -                   
   5   2    12  -2.039E-04   4.073E-03  4.07338302E-03  8.361E-03  2.549E-10  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    5.334459E-10    Infinity MARKER     #E_WF_tuned          -                    0
  3   1   3     6    0.000000E+00    1.000E+00    2.082433E-08    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    Infinity MARKER     #E_WF_tuned          -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    Infinity MARKER     #E_WF_tuned          -                    0
 10   3   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 Fit reached penalty value   4.5565E-16

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 22

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   5.334459E-12   2.082433E-11        3        3    1.000      (Y,T)        22
   0.0000E+00   0.0000E+00   1.0000E+00  -1.348826E-17  -5.395306E-17        3        3    1.000      (Z,P)        22
   0.0000E+00   0.0000E+00   1.0000E+00   2.072245E-03   3.500000E-01        3        0    0.000      (t,K)        22

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  SPNPRT      MATRIX                PRINT                                                  IPASS= 22



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455342   0.122008   0.333333   0.577350   0.001954  20.000000  14.142136


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.866025 -0.500000 -0.000000  1.000000      1.6849   30.000  -90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500000  0.866025  0.000000  1.000000      1.6849   30.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   -0.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6603E-01  8.6603E-01 -5.0000E-01 -5.0000E-01 -2.4313E-17 -2.4297E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      1   1
  5.0000E-01  5.0000E-01  8.6603E-01  8.6603E-01  3.3928E-18  3.9444E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      2   1
  1.9269E-17  1.9345E-17 -1.5086E-17 -1.5031E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      3   1


                  Spin transfer matrix, momentum group # 1 :

          0.866025        0.500000        1.926870E-17
         -0.500000        0.866025       -1.503115E-17
         -2.431299E-17    3.944384E-18     1.00000    

     Trace =       2.7320508029,    ;   spin precession acos((trace-1)/2) =      30.0000002670 deg
     Rotation axis :   ( 0.0000,  0.0000, -1.0000)   ->   angle to (X,Y) plane,  angle to X axis :   -90.0000,    66.4716  degree
     Spin  tune  Qs =     8.3333E-02

************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 22


 Pgm rebel. At pass #   22/  41.  In element #    5,  parameter # 80  changed to    2.69276923E+00   (was    2.56458974E+00)

                                -----  REBELOTE  -----

     End of pass #       22 through the optical structure 

                     Total of         66 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -2.017E+06  -9.828E+05  -982833.12      4.920E+04  0.115      WIENFILT   -                    -                   
   5   2    12  -2.039E-04   4.073E-03  4.07334274E-03  8.361E-03  4.750E-10  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    4.276292E-10    Infinity MARKER     #E_WF_tuned          -                    0
  3   1   3     6    0.000000E+00    1.000E+00    1.669250E-08    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    Infinity MARKER     #E_WF_tuned          -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    Infinity MARKER     #E_WF_tuned          -                    0
 10   3   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 Fit reached penalty value   4.6984E-16

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 23

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.276292E-12   1.669250E-11        3        3    1.000      (Y,T)        23
   0.0000E+00   0.0000E+00   1.0000E+00  -1.348813E-17  -5.395252E-17        3        3    1.000      (Z,P)        23
   0.0000E+00   0.0000E+00   1.0000E+00   2.072245E-03   3.500000E-01        3        0    0.000      (t,K)        23

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  SPNPRT      MATRIX                PRINT                                                  IPASS= 23



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455342   0.122008   0.333333   0.577350   0.001954  20.000001  14.142136


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.866025 -0.500000 -0.000000  1.000000      1.6849   30.000  -90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500000  0.866025  0.000000  1.000000      1.6849   30.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   -0.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6603E-01  8.6603E-01 -5.0000E-01 -5.0000E-01 -2.4314E-17 -2.4297E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      1   1
  5.0000E-01  5.0000E-01  8.6603E-01  8.6603E-01  3.3928E-18  3.9717E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      2   1
  1.9265E-17  1.9345E-17 -1.5086E-17 -1.5028E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      3   1


                  Spin transfer matrix, momentum group # 1 :

          0.866025        0.500000        1.926483E-17
         -0.500000        0.866025       -1.502837E-17
         -2.431380E-17    3.971723E-18     1.00000    

     Trace =       2.7320507937,    ;   spin precession acos((trace-1)/2) =      30.0000007919 deg
     Rotation axis :   ( 0.0000,  0.0000, -1.0000)   ->   angle to (X,Y) plane,  angle to X axis :   -90.0000,    66.4430  degree
     Spin  tune  Qs =     8.3333E-02

************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 23


 Pgm rebel. At pass #   23/  41.  In element #    5,  parameter # 80  changed to    2.82094872E+00   (was    2.69276923E+00)

                                -----  REBELOTE  -----

     End of pass #       23 through the optical structure 

                     Total of         69 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -2.017E+06  -9.828E+05  -982822.57      4.920E+04  0.115      WIENFILT   -                    -                   
   5   2    12  -2.039E-04   4.073E-03  4.07329901E-03  8.361E-03  4.772E-10  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    6.568621E-10    Infinity MARKER     #E_WF_tuned          -                    0
  3   1   3     6    0.000000E+00    1.000E+00    2.563857E-08    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   3   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 Fit reached penalty value   6.5840E-16

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 24

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   6.568621E-12   2.563857E-11        3        3    1.000      (Y,T)        24
   0.0000E+00   0.0000E+00   1.0000E+00  -1.348799E-17  -5.395194E-17        3        0    0.000      (Z,P)        24
   0.0000E+00   0.0000E+00   1.0000E+00   2.072245E-03   3.500000E-01        3        0    0.000      (t,K)        24

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   3.798227E-65

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  SPNPRT      MATRIX                PRINT                                                  IPASS= 24



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455342   0.122008   0.333333   0.577350   0.001954  20.000000  14.142136


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.866025 -0.500000 -0.000000  1.000000      1.6849   30.000  -90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500000  0.866025  0.000000  1.000000      1.6849   30.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   -0.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6603E-01  8.6603E-01 -5.0000E-01 -5.0000E-01 -2.4315E-17 -2.4297E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      1   1
  5.0000E-01  5.0000E-01  8.6603E-01  8.6603E-01  3.3928E-18  4.0008E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      2   1
  1.9261E-17  1.9345E-17 -1.5086E-17 -1.5025E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      3   1


                  Spin transfer matrix, momentum group # 1 :

          0.866025        0.500000        1.926055E-17
         -0.500000        0.866025       -1.502528E-17
         -2.431477E-17    4.000791E-18     1.00000    

     Trace =       2.7320508068,    ;   spin precession acos((trace-1)/2) =      30.0000000456 deg
     Rotation axis :   ( 0.0000,  0.0000, -1.0000)   ->   angle to (X,Y) plane,  angle to X axis :   -90.0000,    66.4127  degree
     Spin  tune  Qs =     8.3333E-02

************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 24


 Pgm rebel. At pass #   24/  41.  In element #    5,  parameter # 80  changed to    2.94912821E+00   (was    2.82094872E+00)

                                -----  REBELOTE  -----

     End of pass #       24 through the optical structure 

                     Total of         72 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -2.017E+06  -9.828E+05  -982809.80      4.920E+04  0.113      WIENFILT   -                    -                   
   5   2    12  -2.039E-04   4.073E-03  4.07324606E-03  8.361E-03  4.672E-10  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    1.805238E-10    Infinity MARKER     #E_WF_tuned          -                    0
  3   1   3     6    0.000000E+00    1.000E+00    7.045265E-09    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   3   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 Fit reached penalty value   3.5616E-16

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 25

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   1.805238E-12   7.045265E-12        3        0    0.000      (Y,T)        25
   0.0000E+00   0.0000E+00   1.0000E+00  -1.348781E-17  -5.395124E-17        3        3    1.000      (Z,P)        25
   0.0000E+00   0.0000E+00   1.0000E+00   2.072245E-03   3.500000E-01        3        0    0.000      (t,K)        25

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   4.078315E-56   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  SPNPRT      MATRIX                PRINT                                                  IPASS= 25



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455342   0.122008   0.333333   0.577350   0.001954  19.999999  14.142135


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.866025 -0.500000 -0.000000  1.000000      1.6849   30.000  -90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500000  0.866025  0.000000  1.000000      1.6849   30.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   -0.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6603E-01  8.6603E-01 -5.0000E-01 -5.0000E-01 -2.4316E-17 -2.4297E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      1   1
  5.0000E-01  5.0000E-01  8.6603E-01  8.6603E-01  3.3928E-18  4.0352E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      2   1
  1.9255E-17  1.9345E-17 -1.5086E-17 -1.5021E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      3   1


                  Spin transfer matrix, momentum group # 1 :

          0.866025        0.500000        1.925502E-17
         -0.500000        0.866025       -1.502125E-17
         -2.431623E-17    4.035189E-18     1.00000    

     Trace =       2.7320508251,    ;   spin precession acos((trace-1)/2) =      29.9999989969 deg
     Rotation axis :   ( 0.0000,  0.0000, -1.0000)   ->   angle to (X,Y) plane,  angle to X axis :   -90.0000,    66.3772  degree
     Spin  tune  Qs =     8.3333E-02

************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 25


 Pgm rebel. At pass #   25/  41.  In element #    5,  parameter # 80  changed to    3.07730769E+00   (was    2.94912821E+00)

                                -----  REBELOTE  -----

     End of pass #       25 through the optical structure 

                     Total of         75 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -2.017E+06  -9.828E+05  -982799.64      4.920E+04  5.922E-02  WIENFILT   -                    -                   
   5   2    12  -2.039E-04   4.073E-03  4.07320395E-03  8.361E-03  2.452E-10  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    4.575378E-10    Infinity MARKER     #E_WF_tuned          -                    0
  3   1   3     6    0.000000E+00    1.000E+00    1.785640E-08    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   3   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 Fit reached penalty value   3.3641E-16

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 26

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.575378E-12   1.785640E-11        3        3    1.000      (Y,T)        26
   0.0000E+00   0.0000E+00   1.0000E+00  -1.348767E-17  -5.395068E-17        3        0    0.000      (Z,P)        26
   0.0000E+00   0.0000E+00   1.0000E+00   2.072245E-03   3.500000E-01        3        3    1.000      (t,K)        26

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   3.798227E-65

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  SPNPRT      MATRIX                PRINT                                                  IPASS= 26



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455342   0.122008   0.333333   0.577350   0.001954  20.000000  14.142136


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.866025 -0.500000 -0.000000  1.000000      1.6849   30.000  -90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500000  0.866025  0.000000  1.000000      1.6849   30.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   -0.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6603E-01  8.6603E-01 -5.0000E-01 -5.0000E-01 -2.4316E-17 -2.4297E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      1   1
  5.0000E-01  5.0000E-01  8.6603E-01  8.6603E-01  3.3928E-18  4.0559E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      2   1
  1.9253E-17  1.9345E-17 -1.5086E-17 -1.5020E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      3   1


                  Spin transfer matrix, momentum group # 1 :

          0.866025        0.500000        1.925267E-17
         -0.500000        0.866025       -1.501964E-17
         -2.431639E-17    4.055876E-18     1.00000    

     Trace =       2.7320508034,    ;   spin precession acos((trace-1)/2) =      30.0000002387 deg
     Rotation axis :   ( 0.0000,  0.0000, -1.0000)   ->   angle to (X,Y) plane,  angle to X axis :   -90.0000,    66.3551  degree
     Spin  tune  Qs =     8.3333E-02

************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 26


 Pgm rebel. At pass #   26/  41.  In element #    5,  parameter # 80  changed to    3.20548718E+00   (was    3.07730769E+00)

                                -----  REBELOTE  -----

     End of pass #       26 through the optical structure 

                     Total of         78 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -2.017E+06  -9.828E+05  -982789.29      4.920E+04  5.705E-02  WIENFILT   -                    -                   
   5   2    12  -2.039E-04   4.073E-03  4.07316108E-03  8.361E-03  2.366E-10  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00   -5.765894E-11    Infinity MARKER     #E_WF_tuned          -                    0
  3   1   3     6    0.000000E+00    1.000E+00   -2.250177E-09    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    Infinity MARKER     #E_WF_tuned          -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    Infinity MARKER     #E_WF_tuned          -                    0
 10   3   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 Fit reached penalty value   2.1730E-16

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 27

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00  -5.765894E-13  -2.250177E-12        3        3    1.000      (Y,T)        27
   0.0000E+00   0.0000E+00   1.0000E+00  -1.348753E-17  -5.395012E-17        3        0    0.000      (Z,P)        27
   0.0000E+00   0.0000E+00   1.0000E+00   2.072245E-03   3.500000E-01        3        0    0.000      (t,K)        27

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   3.798227E-65

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  SPNPRT      MATRIX                PRINT                                                  IPASS= 27



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455342   0.122008   0.333333   0.577350   0.001954  19.999999  14.142135


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.866025 -0.500000 -0.000000  1.000000      1.6849   30.000  -90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500000  0.866025  0.000000  1.000000      1.6849   30.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   -0.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6603E-01  8.6603E-01 -5.0000E-01 -5.0000E-01 -2.4317E-17 -2.4297E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      1   1
  5.0000E-01  5.0000E-01  8.6603E-01  8.6603E-01  3.3928E-18  4.0803E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      2   1
  1.9249E-17  1.9345E-17 -1.5086E-17 -1.5017E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      3   1


                  Spin transfer matrix, momentum group # 1 :

          0.866025        0.500000        1.924946E-17
         -0.500000        0.866025       -1.501733E-17
         -2.431694E-17    4.080282E-18     1.00000    

     Trace =       2.7320508221,    ;   spin precession acos((trace-1)/2) =      29.9999991653 deg
     Rotation axis :   ( 0.0000,  0.0000, -1.0000)   ->   angle to (X,Y) plane,  angle to X axis :   -90.0000,    66.3294  degree
     Spin  tune  Qs =     8.3333E-02

************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 27


 Pgm rebel. At pass #   27/  41.  In element #    5,  parameter # 80  changed to    3.33366667E+00   (was    3.20548718E+00)

                                -----  REBELOTE  -----

     End of pass #       27 through the optical structure 

                     Total of         81 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -2.017E+06  -9.828E+05  -982773.04      4.920E+04  0.112      WIENFILT   -                    -                   
   5   2    12  -2.039E-04   4.073E-03  4.07309371E-03  8.361E-03  4.641E-10  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00   -6.344227E-10    Infinity MARKER     #E_WF_tuned          -                    0
  3   1   3     6    0.000000E+00    1.000E+00   -2.475386E-08    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   3   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 Fit reached penalty value   9.8875E-16

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 28

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00  -6.344227E-12  -2.475386E-11        3        3    1.000      (Y,T)        28
   0.0000E+00   0.0000E+00   1.0000E+00  -1.348731E-17  -5.394922E-17        3        3    1.000      (Z,P)        28
   0.0000E+00   0.0000E+00   1.0000E+00   2.072245E-03   3.500000E-01        3        0    0.000      (t,K)        28

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  SPNPRT      MATRIX                PRINT                                                  IPASS= 28



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455342   0.122008   0.333333   0.577350   0.001954  20.000001  14.142136


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.866025 -0.500000 -0.000000  1.000000      1.6849   30.000  -90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500000  0.866025  0.000000  1.000000      1.6849   30.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   -0.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6603E-01  8.6603E-01 -5.0000E-01 -5.0000E-01 -2.4319E-17 -2.4297E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      1   1
  5.0000E-01  5.0000E-01  8.6603E-01  8.6603E-01  3.3928E-18  4.1212E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      2   1
  1.9242E-17  1.9345E-17 -1.5086E-17 -1.5012E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      3   1


                  Spin transfer matrix, momentum group # 1 :

          0.866025        0.500000        1.924236E-17
         -0.500000        0.866025       -1.501214E-17
         -2.431902E-17    4.121240E-18     1.00000    

     Trace =       2.7320507882,    ;   spin precession acos((trace-1)/2) =      30.0000011104 deg
     Rotation axis :   ( 0.0000,  0.0000, -1.0000)   ->   angle to (X,Y) plane,  angle to X axis :   -90.0000,    66.2876  degree
     Spin  tune  Qs =     8.3333E-02

************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 28


 Pgm rebel. At pass #   28/  41.  In element #    5,  parameter # 80  changed to    3.46184615E+00   (was    3.33366667E+00)

                                -----  REBELOTE  -----

     End of pass #       28 through the optical structure 

                     Total of         84 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -2.017E+06  -9.828E+05  -982764.27      4.920E+04  7.229E-02  WIENFILT   -                    -                   
   5   2    12  -2.039E-04   4.073E-03  4.07305736E-03  8.361E-03  2.995E-10  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    7.141484E-10    Infinity MARKER     #E_WF_tuned          -                    0
  3   1   3     6    0.000000E+00    1.000E+00    2.786720E-08    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235987E-01    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   3   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 Fit reached penalty value   3.1523E-16

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 29

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   7.141484E-12   2.786720E-11        3        0    0.000      (Y,T)        29
   0.0000E+00   0.0000E+00   1.0000E+00  -1.348719E-17  -5.394874E-17        3        3    1.000      (Z,P)        29
   0.0000E+00   0.0000E+00   1.0000E+00   2.072245E-03   3.500000E-01        3        0    0.000      (t,K)        29

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   1.044049E-53   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  SPNPRT      MATRIX                PRINT                                                  IPASS= 29



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455342   0.122008   0.333333   0.577350   0.001954  19.999999  14.142135


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.866025 -0.500000 -0.000000  1.000000      1.6849   30.000  -90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500000  0.866025  0.000000  1.000000      1.6849   30.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   -0.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6603E-01  8.6603E-01 -5.0000E-01 -5.0000E-01 -2.4319E-17 -2.4297E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      1   1
  5.0000E-01  5.0000E-01  8.6603E-01  8.6603E-01  3.3928E-18  4.1338E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      2   1
  1.9242E-17  1.9345E-17 -1.5086E-17 -1.5012E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      3   1


                  Spin transfer matrix, momentum group # 1 :

          0.866025        0.500000        1.924190E-17
         -0.500000        0.866025       -1.501192E-17
         -2.431843E-17    4.133782E-18     1.00000    

     Trace =       2.7320508464,    ;   spin precession acos((trace-1)/2) =      29.9999977726 deg
     Rotation axis :   ( 0.0000,  0.0000, -1.0000)   ->   angle to (X,Y) plane,  angle to X axis :   -90.0000,    66.2735  degree
     Spin  tune  Qs =     8.3333E-02

************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 29


 Pgm rebel. At pass #   29/  41.  In element #    5,  parameter # 80  changed to    3.59002564E+00   (was    3.46184615E+00)

                                -----  REBELOTE  -----

     End of pass #       29 through the optical structure 

                     Total of         87 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -2.017E+06  -9.828E+05  -982748.13      4.920E+04  9.915E-02  WIENFILT   -                    -                   
   5   2    12  -2.039E-04   4.073E-03  4.07299050E-03  8.361E-03  4.108E-10  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    4.752353E-10    Infinity MARKER     #E_WF_tuned          -                    0
  3   1   3     6    0.000000E+00    1.000E+00    1.854098E-08    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   3   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 Fit reached penalty value   6.3756E-16

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 30

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.752353E-12   1.854098E-11        3        3    1.000      (Y,T)        30
   0.0000E+00   0.0000E+00   1.0000E+00  -1.348696E-17  -5.394786E-17        3        0    0.000      (Z,P)        30
   0.0000E+00   0.0000E+00   1.0000E+00   2.072245E-03   3.500000E-01        3        3    1.000      (t,K)        30

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   3.798227E-65

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  SPNPRT      MATRIX                PRINT                                                  IPASS= 30



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455342   0.122008   0.333333   0.577350   0.001954  19.999999  14.142135


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.866025 -0.500000 -0.000000  1.000000      1.6849   30.000  -90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500000  0.866025  0.000000  1.000000      1.6849   30.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   -0.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6603E-01  8.6603E-01 -5.0000E-01 -5.0000E-01 -2.4320E-17 -2.4297E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      1   1
  5.0000E-01  5.0000E-01  8.6603E-01  8.6603E-01  3.3928E-18  4.1729E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      2   1
  1.9235E-17  1.9345E-17 -1.5086E-17 -1.5007E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      3   1


                  Spin transfer matrix, momentum group # 1 :

          0.866025        0.500000        1.923521E-17
         -0.500000        0.866025       -1.500702E-17
         -2.432036E-17    4.172910E-18     1.00000    

     Trace =       2.7320508247,    ;   spin precession acos((trace-1)/2) =      29.9999990183 deg
     Rotation axis :   ( 0.0000,  0.0000, -1.0000)   ->   angle to (X,Y) plane,  angle to X axis :   -90.0000,    66.2334  degree
     Spin  tune  Qs =     8.3333E-02

************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 30


 Pgm rebel. At pass #   30/  41.  In element #    5,  parameter # 80  changed to    3.71820513E+00   (was    3.59002564E+00)

                                -----  REBELOTE  -----

     End of pass #       30 through the optical structure 

                     Total of         90 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -2.017E+06  -9.827E+05  -982737.86      4.920E+04  0.117      WIENFILT   -                    -                   
   5   2    12  -2.039E-04   4.073E-03  4.07294791E-03  8.361E-03  4.851E-10  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00   -3.063480E-10    Infinity MARKER     #E_WF_tuned          -                    0
  3   1   3     6    0.000000E+00    1.000E+00   -1.195282E-08    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   3   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 Fit reached penalty value   2.5958E-16

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 31

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00  -3.063480E-12  -1.195282E-11        3        3    1.000      (Y,T)        31
   0.0000E+00   0.0000E+00   1.0000E+00  -1.348682E-17  -5.394729E-17        3        0    0.000      (Z,P)        31
   0.0000E+00   0.0000E+00   1.0000E+00   2.072245E-03   3.500000E-01        3        3    1.000      (t,K)        31

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   3.798227E-65

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  SPNPRT      MATRIX                PRINT                                                  IPASS= 31



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455342   0.122008   0.333333   0.577350   0.001954  20.000000  14.142136


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.866025 -0.500000 -0.000000  1.000000      1.6849   30.000  -90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500000  0.866025  0.000000  1.000000      1.6849   30.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   -0.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6603E-01  8.6603E-01 -5.0000E-01 -5.0000E-01 -2.4320E-17 -2.4297E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      1   1
  5.0000E-01  5.0000E-01  8.6603E-01  8.6603E-01  3.3928E-18  4.1875E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      2   1
  1.9234E-17  1.9345E-17 -1.5086E-17 -1.5006E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      3   1


                  Spin transfer matrix, momentum group # 1 :

          0.866025        0.500000        1.923426E-17
         -0.500000        0.866025       -1.500645E-17
         -2.431996E-17    4.187476E-18     1.00000    

     Trace =       2.7320507968,    ;   spin precession acos((trace-1)/2) =      30.0000006187 deg
     Rotation axis :   ( 0.0000,  0.0000, -1.0000)   ->   angle to (X,Y) plane,  angle to X axis :   -90.0000,    66.2174  degree
     Spin  tune  Qs =     8.3333E-02

************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 31


 Pgm rebel. At pass #   31/  41.  In element #    5,  parameter # 80  changed to    3.84638462E+00   (was    3.71820513E+00)

                                -----  REBELOTE  -----

     End of pass #       31 through the optical structure 

                     Total of         93 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -2.017E+06  -9.827E+05  -982717.78      4.920E+04  7.736E-02  WIENFILT   -                    -                   
   5   2    12  -2.039E-04   4.073E-03  4.07286470E-03  8.361E-03  3.206E-10  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    5.507274E-10    Infinity MARKER     #E_WF_tuned          -                    0
  3   1   3     6    0.000000E+00    1.000E+00    2.148227E-08    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   3   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 Fit reached penalty value   9.7136E-16

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 32

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   5.507274E-12   2.148227E-11        3        0    0.000      (Y,T)        32
   0.0000E+00   0.0000E+00   1.0000E+00  -1.348655E-17  -5.394619E-17        3        0    0.000      (Z,P)        32
   0.0000E+00   0.0000E+00   1.0000E+00   2.072245E-03   3.500000E-01        3        3    1.000      (t,K)        32

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   6.525304E-55   0.000000E+00  -1.244603E-60   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  -1.244603E-60   0.000000E+00   2.373892E-66   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  SPNPRT      MATRIX                PRINT                                                  IPASS= 32



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455342   0.122008   0.333333   0.577350   0.001954  19.999999  14.142135


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.866025 -0.500000 -0.000000  1.000000      1.6849   30.000  -90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500000  0.866025  0.000000  1.000000      1.6849   30.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   -0.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6603E-01  8.6603E-01 -5.0000E-01 -5.0000E-01 -2.4323E-17 -2.4297E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      1   1
  5.0000E-01  5.0000E-01  8.6603E-01  8.6603E-01  3.3928E-18  4.2335E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      2   1
  1.9226E-17  1.9345E-17 -1.5086E-17 -1.5000E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      3   1


                  Spin transfer matrix, momentum group # 1 :

          0.866025        0.500000        1.922594E-17
         -0.500000        0.866025       -1.500034E-17
         -2.432252E-17    4.233500E-18     1.00000    

     Trace =       2.7320508301,    ;   spin precession acos((trace-1)/2) =      29.9999987066 deg
     Rotation axis :   ( 0.0000,  0.0000, -1.0000)   ->   angle to (X,Y) plane,  angle to X axis :   -90.0000,    66.1706  degree
     Spin  tune  Qs =     8.3333E-02

************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 32


 Pgm rebel. At pass #   32/  41.  In element #    5,  parameter # 80  changed to    3.97456410E+00   (was    3.84638462E+00)

                                -----  REBELOTE  -----

     End of pass #       32 through the optical structure 

                     Total of         96 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -2.017E+06  -9.827E+05  -982709.98      4.920E+04  0.171      WIENFILT   -                    -                   
   5   2    12  -2.039E-04   4.073E-03  4.07283238E-03  8.361E-03  7.072E-10  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00   -6.110765E-10    Infinity MARKER     #E_WF_tuned          -                    0
  3   1   3     6    0.000000E+00    1.000E+00   -2.383965E-08    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   3   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 Fit reached penalty value   8.4032E-16

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 33

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00  -6.110765E-12  -2.383965E-11        3        3    1.000      (Y,T)        33
   0.0000E+00   0.0000E+00   1.0000E+00  -1.348644E-17  -5.394576E-17        3        0    0.000      (Z,P)        33
   0.0000E+00   0.0000E+00   1.0000E+00   2.072245E-03   3.500000E-01        3        3    1.000      (t,K)        33

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   2.373892E-66   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  SPNPRT      MATRIX                PRINT                                                  IPASS= 33



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455342   0.122008   0.333333   0.577350   0.001954  19.999999  14.142135


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.866025 -0.500000 -0.000000  1.000000      1.6849   30.000  -90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500000  0.866025  0.000000  1.000000      1.6849   30.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   -0.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6603E-01  8.6603E-01 -5.0000E-01 -5.0000E-01 -2.4323E-17 -2.4297E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      1   1
  5.0000E-01  5.0000E-01  8.6603E-01  8.6603E-01  3.3928E-18  4.2414E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      2   1
  1.9226E-17  1.9345E-17 -1.5086E-17 -1.5000E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      3   1


                  Spin transfer matrix, momentum group # 1 :

          0.866025        0.500000        1.922653E-17
         -0.500000        0.866025       -1.500089E-17
         -2.432153E-17    4.241400E-18     1.00000    

     Trace =       2.7320508240,    ;   spin precession acos((trace-1)/2) =      29.9999990557 deg
     Rotation axis :   ( 0.0000,  0.0000, -1.0000)   ->   angle to (X,Y) plane,  angle to X axis :   -90.0000,    66.1611  degree
     Spin  tune  Qs =     8.3333E-02

************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 33


 Pgm rebel. At pass #   33/  41.  In element #    5,  parameter # 80  changed to    4.10274359E+00   (was    3.97456410E+00)

                                -----  REBELOTE  -----

     End of pass #       33 through the optical structure 

                     Total of         99 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -2.017E+06  -9.827E+05  -982690.98      4.920E+04  8.057E-02  WIENFILT   -                    -                   
   5   2    12  -2.039E-04   4.073E-03  4.07275360E-03  8.361E-03  3.340E-10  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00   -2.616566E-10    Infinity MARKER     #E_WF_tuned          -                    0
  3   1   3     6    0.000000E+00    1.000E+00   -1.020648E-08    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    Infinity MARKER     #E_WF_tuned          -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    Infinity MARKER     #E_WF_tuned          -                    0
 10   3   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 Fit reached penalty value   1.5453E-16

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 34

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00  -2.616566E-12  -1.020648E-11        3        3    1.000      (Y,T)        34
   0.0000E+00   0.0000E+00   1.0000E+00  -1.348618E-17  -5.394472E-17        3        0    0.000      (Z,P)        34
   0.0000E+00   0.0000E+00   1.0000E+00   2.072245E-03   3.500000E-01        3        0    0.000      (t,K)        34

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   2.373892E-66   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  SPNPRT      MATRIX                PRINT                                                  IPASS= 34



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455342   0.122008   0.333333   0.577350   0.001954  20.000000  14.142135


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.866025 -0.500000 -0.000000  1.000000      1.6849   30.000  -90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500000  0.866025  0.000000  1.000000      1.6849   30.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   -0.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6603E-01  8.6603E-01 -5.0000E-01 -5.0000E-01 -2.4323E-17 -2.4297E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      1   1
  5.0000E-01  5.0000E-01  8.6603E-01  8.6603E-01  3.3928E-18  4.2761E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      2   1
  1.9221E-17  1.9345E-17 -1.5086E-17 -1.4997E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      3   1


                  Spin transfer matrix, momentum group # 1 :

          0.866025        0.500000        1.922083E-17
         -0.500000        0.866025       -1.499679E-17
         -2.432298E-17    4.276125E-18     1.00000    

     Trace =       2.7320508147,    ;   spin precession acos((trace-1)/2) =      29.9999995937 deg
     Rotation axis :   ( 0.0000,  0.0000, -1.0000)   ->   angle to (X,Y) plane,  angle to X axis :   -90.0000,    66.1253  degree
     Spin  tune  Qs =     8.3333E-02

************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 34


 Pgm rebel. At pass #   34/  41.  In element #    5,  parameter # 80  changed to    4.23092308E+00   (was    4.10274359E+00)

                                -----  REBELOTE  -----

     End of pass #       34 through the optical structure 

                     Total of        102 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -2.017E+06  -9.827E+05  -982677.36      4.920E+04  0.104      WIENFILT   -                    -                   
   5   2    12  -2.039E-04   4.073E-03  4.07269718E-03  8.361E-03  4.308E-10  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    4.404201E-11    Infinity MARKER     #E_WF_tuned          -                    0
  3   1   3     6    0.000000E+00    1.000E+00    1.717859E-09    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   3   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 Fit reached penalty value   3.0494E-16

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 35

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.404201E-13   1.717859E-12        3        3    1.000      (Y,T)        35
   0.0000E+00   0.0000E+00   1.0000E+00  -1.348599E-17  -5.394397E-17        3        3    1.000      (Z,P)        35
   0.0000E+00   0.0000E+00   1.0000E+00   2.072245E-03   3.500000E-01        3        3    1.000      (t,K)        35

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  SPNPRT      MATRIX                PRINT                                                  IPASS= 35



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455342   0.122008   0.333333   0.577350   0.001954  19.999999  14.142135


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.866025 -0.500000 -0.000000  1.000000      1.6849   30.000  -90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500000  0.866025  0.000000  1.000000      1.6849   30.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   -0.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6603E-01  8.6603E-01 -5.0000E-01 -5.0000E-01 -2.4324E-17 -2.4297E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      1   1
  5.0000E-01  5.0000E-01  8.6603E-01  8.6603E-01  3.3928E-18  4.3038E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      2   1
  1.9217E-17  1.9345E-17 -1.5086E-17 -1.4994E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      3   1


                  Spin transfer matrix, momentum group # 1 :

          0.866025        0.500000        1.921678E-17
         -0.500000        0.866025       -1.499383E-17
         -2.432390E-17    4.303826E-18     1.00000    

     Trace =       2.7320508249,    ;   spin precession acos((trace-1)/2) =      29.9999990043 deg
     Rotation axis :   ( 0.0000,  0.0000, -1.0000)   ->   angle to (X,Y) plane,  angle to X axis :   -90.0000,    66.0966  degree
     Spin  tune  Qs =     8.3333E-02

************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 35


 Pgm rebel. At pass #   35/  41.  In element #    5,  parameter # 80  changed to    4.35910256E+00   (was    4.23092308E+00)

                                -----  REBELOTE  -----

     End of pass #       35 through the optical structure 

                     Total of        105 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -2.017E+06  -9.827E+05  -982663.82      4.920E+04  0.136      WIENFILT   -                    -                   
   5   2    12  -2.039E-04   4.073E-03  4.07264104E-03  8.361E-03  5.622E-10  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    3.677361E-10    Infinity MARKER     #E_WF_tuned          -                    0
  3   1   3     6    0.000000E+00    1.000E+00    1.434403E-08    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   3   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 Fit reached penalty value   2.7988E-16

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 36

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   3.677361E-12   1.434403E-11        3        3    1.000      (Y,T)        36
   0.0000E+00   0.0000E+00   1.0000E+00  -1.348581E-17  -5.394323E-17        3        0    0.000      (Z,P)        36
   0.0000E+00   0.0000E+00   1.0000E+00   2.072245E-03   3.500000E-01        3        3    1.000      (t,K)        36

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   2.373892E-66   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  SPNPRT      MATRIX                PRINT                                                  IPASS= 36



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455342   0.122008   0.333333   0.577350   0.001954  20.000000  14.142135


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.866025 -0.500000 -0.000000  1.000000      1.6849   30.000  -90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500000  0.866025  0.000000  1.000000      1.6849   30.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   -0.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6603E-01  8.6603E-01 -5.0000E-01 -5.0000E-01 -2.4324E-17 -2.4297E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      1   1
  5.0000E-01  5.0000E-01  8.6603E-01  8.6603E-01  3.3928E-18  4.3209E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      2   1
  1.9215E-17  1.9345E-17 -1.5086E-17 -1.4993E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      3   1


                  Spin transfer matrix, momentum group # 1 :

          0.866025        0.500000        1.921520E-17
         -0.500000        0.866025       -1.499279E-17
         -2.432372E-17    4.320910E-18     1.00000    

     Trace =       2.7320508162,    ;   spin precession acos((trace-1)/2) =      29.9999995071 deg
     Rotation axis :   ( 0.0000,  0.0000, -1.0000)   ->   angle to (X,Y) plane,  angle to X axis :   -90.0000,    66.0781  degree
     Spin  tune  Qs =     8.3333E-02

************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 36


 Pgm rebel. At pass #   36/  41.  In element #    5,  parameter # 80  changed to    4.48728205E+00   (was    4.35910256E+00)

                                -----  REBELOTE  -----

     End of pass #       36 through the optical structure 

                     Total of        108 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -2.017E+06  -9.827E+05  -982641.52      4.920E+04  0.117      WIENFILT   -                    -                   
   5   2    12  -2.039E-04   4.073E-03  4.07254864E-03  8.361E-03  4.866E-10  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    5.163698E-10    Infinity MARKER     #E_WF_tuned          -                    0
  3   1   3     6    0.000000E+00    1.000E+00    2.013784E-08    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   3   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 Fit reached penalty value   4.1912E-16

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 37

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   5.163698E-12   2.013784E-11        3        0    0.000      (Y,T)        37
   0.0000E+00   0.0000E+00   1.0000E+00  -1.348550E-17  -5.394201E-17        3        0    0.000      (Z,P)        37
   0.0000E+00   0.0000E+00   1.0000E+00   2.072245E-03   3.500000E-01        3        0    0.000      (t,K)        37

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   1.044049E-53  -4.978412E-60   1.991365E-59
   0.000000E+00  -4.978412E-60   2.373892E-66  -9.495568E-66
   0.000000E+00   1.991365E-59  -9.495568E-66   3.798227E-65

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  SPNPRT      MATRIX                PRINT                                                  IPASS= 37



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455342   0.122008   0.333333   0.577350   0.001954  20.000000  14.142136


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.866025 -0.500000 -0.000000  1.000000      1.6849   30.000  -90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500000  0.866025  0.000000  1.000000      1.6849   30.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   -0.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6603E-01  8.6603E-01 -5.0000E-01 -5.0000E-01 -2.4326E-17 -2.4297E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      1   1
  5.0000E-01  5.0000E-01  8.6603E-01  8.6603E-01  3.3928E-18  4.3608E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      2   1
  1.9208E-17  1.9345E-17 -1.5086E-17 -1.4988E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      3   1


                  Spin transfer matrix, momentum group # 1 :

          0.866025        0.500000        1.920825E-17
         -0.500000        0.866025       -1.498776E-17
         -2.432566E-17    4.360843E-18     1.00000    

     Trace =       2.7320508112,    ;   spin precession acos((trace-1)/2) =      29.9999997909 deg
     Rotation axis :   ( 0.0000,  0.0000, -1.0000)   ->   angle to (X,Y) plane,  angle to X axis :   -90.0000,    66.0373  degree
     Spin  tune  Qs =     8.3333E-02

************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 37


 Pgm rebel. At pass #   37/  41.  In element #    5,  parameter # 80  changed to    4.61546154E+00   (was    4.48728205E+00)

                                -----  REBELOTE  -----

     End of pass #       37 through the optical structure 

                     Total of        111 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -2.017E+06  -9.826E+05  -982627.79      4.920E+04  0.191      WIENFILT   -                    -                   
   5   2    12  -2.039E-04   4.073E-03  4.07249173E-03  8.361E-03  7.906E-10  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00   -1.942161E-10    Infinity MARKER     #E_WF_tuned          -                    0
  3   1   3     6    0.000000E+00    1.000E+00   -7.573788E-09    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   3   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 Fit reached penalty value   1.6845E-16

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 38

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00  -1.942161E-12  -7.573788E-12        3        3    1.000      (Y,T)        38
   0.0000E+00   0.0000E+00   1.0000E+00  -1.348531E-17  -5.394125E-17        3        3    1.000      (Z,P)        38
   0.0000E+00   0.0000E+00   1.0000E+00   2.072245E-03   3.500000E-01        3        3    1.000      (t,K)        38

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  SPNPRT      MATRIX                PRINT                                                  IPASS= 38



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455342   0.122008   0.333333   0.577350   0.001954  20.000000  14.142136


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.866025 -0.500000 -0.000000  1.000000      1.6849   30.000  -90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500000  0.866025  0.000000  1.000000      1.6849   30.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   -0.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6603E-01  8.6603E-01 -5.0000E-01 -5.0000E-01 -2.4326E-17 -2.4297E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      1   1
  5.0000E-01  5.0000E-01  8.6603E-01  8.6603E-01  3.3928E-18  4.3865E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      2   1
  1.9205E-17  1.9345E-17 -1.5086E-17 -1.4985E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      3   1


                  Spin transfer matrix, momentum group # 1 :

          0.866025        0.500000        1.920464E-17
         -0.500000        0.866025       -1.498511E-17
         -2.432642E-17    4.386548E-18     1.00000    

     Trace =       2.7320507970,    ;   spin precession acos((trace-1)/2) =      30.0000006038 deg
     Rotation axis :   ( 0.0000,  0.0000, -1.0000)   ->   angle to (X,Y) plane,  angle to X axis :   -90.0000,    66.0106  degree
     Spin  tune  Qs =     8.3333E-02

************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 38


 Pgm rebel. At pass #   38/  41.  In element #    5,  parameter # 80  changed to    4.74364103E+00   (was    4.61546154E+00)

                                -----  REBELOTE  -----

     End of pass #       38 through the optical structure 

                     Total of        114 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -2.017E+06  -9.826E+05  -982614.48      4.920E+04  7.932E-02  WIENFILT   -                    -                   
   5   2    12  -2.039E-04   4.072E-03  4.07243655E-03  8.361E-03  3.286E-10  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    1.716269E-10    Infinity MARKER     #E_WF_tuned          -                    0
  3   1   3     6    0.000000E+00    1.000E+00    6.693391E-09    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    Infinity MARKER     #E_WF_tuned          -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    Infinity MARKER     #E_WF_tuned          -                    0
 10   3   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 Fit reached penalty value   4.2999E-16

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 39

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   1.716269E-12   6.693391E-12        3        3    1.000      (Y,T)        39
   0.0000E+00   0.0000E+00   1.0000E+00  -1.348513E-17  -5.394052E-17        3        0    0.000      (Z,P)        39
   0.0000E+00   0.0000E+00   1.0000E+00   2.072245E-03   3.500000E-01        3        0    0.000      (t,K)        39

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   3.798227E-65

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  SPNPRT      MATRIX                PRINT                                                  IPASS= 39



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455342   0.122008   0.333333   0.577350   0.001954  19.999999  14.142135


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.866025 -0.500000 -0.000000  1.000000      1.6849   30.000  -90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500000  0.866025  0.000000  1.000000      1.6849   30.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   -0.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6603E-01  8.6603E-01 -5.0000E-01 -5.0000E-01 -2.4326E-17 -2.4297E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      1   1
  5.0000E-01  5.0000E-01  8.6603E-01  8.6603E-01  3.3928E-18  4.4007E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      2   1
  1.9204E-17  1.9345E-17 -1.5086E-17 -1.4985E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      3   1


                  Spin transfer matrix, momentum group # 1 :

          0.866025        0.500000        1.920372E-17
         -0.500000        0.866025       -1.498455E-17
         -2.432598E-17    4.400714E-18     1.00000    

     Trace =       2.7320508272,    ;   spin precession acos((trace-1)/2) =      29.9999988755 deg
     Rotation axis :   ( 0.0000,  0.0000, -1.0000)   ->   angle to (X,Y) plane,  angle to X axis :   -90.0000,    65.9950  degree
     Spin  tune  Qs =     8.3333E-02

************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 39


 Pgm rebel. At pass #   39/  41.  In element #    5,  parameter # 80  changed to    4.87182051E+00   (was    4.74364103E+00)

                                -----  REBELOTE  -----

     End of pass #       39 through the optical structure 

                     Total of        117 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -2.017E+06  -9.826E+05  -982592.31      4.920E+04  6.779E-02  WIENFILT   -                    -                   
   5   2    12  -2.039E-04   4.072E-03  4.07234470E-03  8.361E-03  2.811E-10  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    2.761591E-10    Infinity MARKER     #E_WF_tuned          -                    0
  3   1   3     6    0.000000E+00    1.000E+00    1.076886E-08    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   3   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 Fit reached penalty value   3.3502E-16

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 40

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   2.761591E-12   1.076886E-11        3        3    1.000      (Y,T)        40
   0.0000E+00   0.0000E+00   1.0000E+00  -1.348483E-17  -5.393930E-17        3        0    0.000      (Z,P)        40
   0.0000E+00   0.0000E+00   1.0000E+00   2.072245E-03   3.500000E-01        3        0    0.000      (t,K)        40

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   3.798227E-65

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  SPNPRT      MATRIX                PRINT                                                  IPASS= 40



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455342   0.122008   0.333333   0.577350   0.001954  20.000001  14.142136


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.866025 -0.500000 -0.000000  1.000000      1.6849   30.000  -90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500000  0.866025  0.000000  1.000000      1.6849   30.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   -0.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6603E-01  8.6603E-01 -5.0000E-01 -5.0000E-01 -2.4327E-17 -2.4297E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      1   1
  5.0000E-01  5.0000E-01  8.6603E-01  8.6603E-01  3.3928E-18  4.4338E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      2   1
  1.9198E-17  1.9345E-17 -1.5086E-17 -1.4981E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      3   1


                  Spin transfer matrix, momentum group # 1 :

          0.866025        0.500000        1.919835E-17
         -0.500000        0.866025       -1.498071E-17
         -2.432726E-17    4.433806E-18     1.00000    

     Trace =       2.7320507928,    ;   spin precession acos((trace-1)/2) =      30.0000008478 deg
     Rotation axis :   ( 0.0000,  0.0000, -1.0000)   ->   angle to (X,Y) plane,  angle to X axis :   -90.0000,    65.9608  degree
     Spin  tune  Qs =     8.3333E-02

************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 40


 Pgm rebel. At pass #   40/  41.  In element #    5,  parameter # 80  changed to    5.00000000E+00   (was    4.87182051E+00)

                                -----  REBELOTE  -----

     End of pass #       40 through the optical structure 

                     Total of        120 particles have been launched


      Next  pass  is  #    41 and  last  pass  through  the  optical  structure


     WRITE statements to zgoubi.res are re-established from now on.

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 41

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 41


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 41


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 may be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO   =         2.311 KG*CM
                               BETA   =    0.804837
                               GAMMA*G =   0.001954


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WF_tuned                                                                  IPASS= 41


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 41

Entrance hard edge is to be implemented
Exit hard edge is to be implemented

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.82592E+05 V/m
                               B                       =  4.07234E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =   1.0000     C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80484    

                FACE  D'ENTREE
                DX =   0.000,  LAMBDA_E, LAMBDA_B =   0.000  0.000 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )

                FACE  DE  SORTIE
                DX =   0.000,  LAMBDA_E, LAMBDA_B =   0.000  0.000 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )

  ***  Warning : hard-edge fringe model entails vertical wedge focusing simulated with
                  first order kick  ***

                    Integration step :   5.000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           50.000     0.000     0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           50.000     0.000     0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           50.000     0.000     0.000    -0.000    -0.000            3

     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.0000000     RAD


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WF_tuned                                                                  IPASS= 41


************************************************************************************************************************************
      7  Keyword, label(s) :  FIT2                                                                                     IPASS= 41


     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 41

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 41


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 41


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 may be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO   =         2.311 KG*CM
                               BETA   =    0.804837
                               GAMMA*G =   0.001954


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WF_tuned                                                                  IPASS= 41


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 41

Entrance hard edge is to be implemented
Exit hard edge is to be implemented

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.82592E+05 V/m
                               B                       =  4.07234E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =   1.0000     C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80484    

                FACE  D'ENTREE
                DX =   0.000,  LAMBDA_E, LAMBDA_B =   0.000  0.000 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )

                FACE  DE  SORTIE
                DX =   0.000,  LAMBDA_E, LAMBDA_B =   0.000  0.000 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )

  ***  Warning : hard-edge fringe model entails vertical wedge focusing simulated with
                  first order kick  ***

                    Integration step :   5.000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           50.000     0.000     0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           50.000     0.000     0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           50.000     0.000     0.000    -0.000    -0.000            3

     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.0000000     RAD


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WF_tuned                                                                  IPASS= 41


************************************************************************************************************************************
      7  Keyword, label(s) :  FIT2                                                                                     IPASS= 41

     FIT procedure launched.

           variable #            1       IR =            5 ,   ok.
           variable #            1       IP =           11 ,   ok.
           variable #            2       IR =            5 ,   ok.
           variable #            2       IP =           12 ,   ok.
           constraint #            1       IR =            6 ,   ok.
           constraint #            1       I  =            1 ,   ok.
           constraint #            2       IR =            6 ,   ok.
           constraint #            2       I  =            1 ,   ok.
           constraint #            3       IR =            6 ,   ok.
           constraint #            4       IR =            6 ,   ok.
           constraint #            5       IR =            6 ,   ok.
           constraint #            6       IR =            6 ,   ok.

                    FIT  variables  and  constraints  in  good  order,  FIT  will proceed. 

 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -2.017E+06  -9.826E+05  -982564.89      4.920E+04  0.119      WIENFILT   -                    -                   
   5   2    12  -2.039E-04   4.072E-03  4.07223106E-03  8.361E-03  4.934E-10  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    2.173089E-10    Infinity MARKER     #E_WF_tuned          -                    0
  3   1   3     6    0.000000E+00    1.000E+00    8.471193E-09    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    Infinity MARKER     #E_WF_tuned          -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 10   3   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WF_tuned          -                    0
 Fit reached penalty value   8.8062E-16

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 41

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000    0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   2.173089E-12   8.471193E-12        3        3    1.000      (Y,T)        41
   0.0000E+00   0.0000E+00   1.0000E+00  -1.348445E-17  -5.393780E-17        3        3    1.000      (Z,P)        41
   0.0000E+00   0.0000E+00   1.0000E+00   2.072245E-03   3.500000E-01        3        3    1.000      (t,K)        41

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  SPNPRT      MATRIX                PRINT                                                  IPASS= 41



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455342   0.122008   0.333333   0.577350   0.001954  20.000001  14.142136


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.866025 -0.500000 -0.000000  1.000000      1.6849   30.000  -90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500000  0.866025  0.000000  1.000000      1.6849   30.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   -0.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6603E-01  8.6603E-01 -5.0000E-01 -5.0000E-01 -2.4330E-17 -2.4297E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      1   1
  5.0000E-01  5.0000E-01  8.6603E-01  8.6603E-01  3.3928E-18  4.4863E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      2   1
  1.9188E-17  1.9345E-17 -1.5086E-17 -1.4973E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      3   1


                  Spin transfer matrix, momentum group # 1 :

          0.866025        0.500000        1.918842E-17
         -0.500000        0.866025       -1.497344E-17
         -2.433045E-17    4.486252E-18     1.00000    

     Trace =       2.7320507791,    ;   spin precession acos((trace-1)/2) =      30.0000016295 deg
     Rotation axis :   ( 0.0000,  0.0000, -1.0000)   ->   angle to (X,Y) plane,  angle to X axis :   -90.0000,    65.9079  degree
     Spin  tune  Qs =     8.3333E-02

************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 41


                         >>>>  End  of  'REBELOTE'  procedure  <<<<

      There  has  been         41  passes  through  the  optical  structure 

                     Total of        123 particles have been launched

************************************************************************************************************************************
     11  Keyword, label(s) :  SYSTEM                                                                                   IPASS= 42

     Number  of  commands :   2,  as  follows : 

 gnuplot <./gnuplot_scanEB.gnu
 okular ./gnuplot_scanEB.eps &

************************************************************************************************************************************
     12  Keyword, label(s) :  END                                                                                      IPASS= 42


************************************************************************************************************************************
 Pgm zgoubi : Execution ended normally, upon keyword END or FIN
   
            ZGOUBI RUN COMPLETED. 

  Zgoubi, author's dvlpmnt version.
  Job  started  on  04-09-2019,  at  01:06:13 
  JOB  ENDED  ON    04-09-2019,  AT  01:11:18 

   CPU time, total :     302.63191300000000     

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
