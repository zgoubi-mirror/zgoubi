Wien filter used as spin rotator. After E. Wang, EIC pCDR, BNL, 2019.
 'OBJET'                                                                                                      1
2.3114795386518345           ! Rigidity of a 350 keV electron.
2
3  1                         ! 3 electrons, reason: see SPNTRK below.
0.  0. 0. 0. 0. 1. 'o'
0.  0. 0. 0. 0. 1. 'o'
0.  0. 0. 0. 0. 1. 'o'
1 1 1
 
 'PARTICUL'                                                                                                   2
0.510998946  1.602176487D-19 1.159652181D-3 0. 0.
 
 'SPNTRK'                      ! Allows chceking rotation of all 3 spin components.                           3
4                             ! (they are computed independently by zgoubi)
1. 0. 0.
0. 1. 0.
0. 0. 1.
 
 'MARKER'  #S_WF_tuned        ! Needed by 'INCLUDE'  ! Include_Start : ./WF_spinRotator_FITparameters.inc     4
 'WIENFILT'                                                                                                   5
20
0.5  -984079.312413395  0.004078507588806  1
0. 0. 0.     ! 20. 5. 5.      ! Hard-edge entrance face.
0.2401  1.8639  -0.5572  0.3904 0. 0.
0.2401  1.8639  -0.5572  0.3904 0. 0.
0. 0. 0.     ! 20. 5. 5.      ! Hard-edge exit face.
0.2401  1.8639  -0.5572  0.3904 0. 0.
0.2401  1.8639  -0.5572  0.3904 0. 0.
.1
1. 0. 0. 0.
 'MARKER'  #E_WF_tuned        ! Needed by 'INCLUDE'  ! Include_End : ./WF_spinRotator_FITparameters.inc       6
 'DRIFT'                                                                                                      7
0.5
 'FAISCEAU'     ! Get some trajectory and some                                                                8
 'SPNPRT'       ! spin outcomes.                                                                              9
 
 'MARKER'  #S_WF_tuned        ! Needed by 'INCLUDE'  ! Include_Start : ./WF_spinRotator_FITparameters.inc    10
 'WIENFILT'                                                                                                  11
20
0.5  -984079.312413395  0.004078507588806  1
0. 0. 0.     ! 20. 5. 5.      ! Hard-edge entrance face.
0.2401  1.8639  -0.5572  0.3904 0. 0.
0.2401  1.8639  -0.5572  0.3904 0. 0.
0. 0. 0.     ! 20. 5. 5.      ! Hard-edge exit face.
0.2401  1.8639  -0.5572  0.3904 0. 0.
0.2401  1.8639  -0.5572  0.3904 0. 0.
.1
1. 0. 0. 0.
 'MARKER'  #E_WF_tuned        ! Needed by 'INCLUDE'  ! Include_End : ./WF_spinRotator_FITparameters.inc      12
 'DRIFT'                                                                                                     13
0.5
 'FAISCEAU'     ! Get some trajectory and some                                                               14
 'SPNPRT'       ! spin outcomes.                                                                             15
 
 'MARKER'  #S_WF_tuned        ! Needed by 'INCLUDE'  ! Include_Start : ./WF_spinRotator_FITparameters.inc    16
 'WIENFILT'                                                                                                  17
20
0.5  -984079.312413395  0.004078507588806  1
0. 0. 0.     ! 20. 5. 5.      ! Hard-edge entrance face.
0.2401  1.8639  -0.5572  0.3904 0. 0.
0.2401  1.8639  -0.5572  0.3904 0. 0.
0. 0. 0.     ! 20. 5. 5.      ! Hard-edge exit face.
0.2401  1.8639  -0.5572  0.3904 0. 0.
0.2401  1.8639  -0.5572  0.3904 0. 0.
.1
1. 0. 0. 0.
 'MARKER'  #E_WF_tuned        ! Needed by 'INCLUDE'  ! Include_End : ./WF_spinRotator_FITparameters.inc      18
 'DRIFT'                                                                                                     19
0.5
 'FAISCEAU'            ! Get some trajectory and some                                                        20
 'SPNPRT'  MATRIX      ! spin outcomes.                                                                      21
 
 'SYSTEM'        ! Get some plots.                                                                           22
2
gnuplot < ./gnuplot_spin.gnu
okular ./gnuplot_spin.eps &
 
 'END'                                                                                                       23

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 1

     Particle  properties :
     Particle name unknown.
                     Mass          =   0.510999        MeV/c2
                     Charge        =   1.602176E-19    C     
                     G  factor     =   1.159652E-03          

              Reference  data :
                    mag. rigidity (kG.cm)   :   2.3114795      =p/q, such that dev.=B*L/rigidity
                    mass (MeV/c2)           :  0.51099895    
                    momentum (MeV/c)        :  0.69296413    
                    energy, total (MeV)     :  0.86099896    
                    energy, kinetic (MeV)   :  0.35000002    
                    beta = v/c              :  0.8048373616    
                    gamma                   :   1.684932950    
                    beta*gamma              :   1.356096990    
                    G*gamma                 :  1.9539361703E-03
                    electric rigidity (MeV) :  0.5577234241    =T[eV]*(gamma+1)/gamma, such that dev.=E*L/rigidity
  
 I, AMQ(1,I), AMQ(2,I)/QE, P/Pref, v/c, time, s :
  
     1   5.10998946E-01  1.00000000E+00  1.00000000E+00  8.04837362E-01  0.00000000E+00  0.00000000E+00
     2   5.10998946E-01  1.00000000E+00  1.00000000E+00  8.04837362E-01  0.00000000E+00  0.00000000E+00
     3   5.10998946E-01  1.00000000E+00  1.00000000E+00  8.04837362E-01  0.00000000E+00  0.00000000E+00

************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 1


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 may be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO   =         2.311 KG*CM
                               BETA   =    0.804837
                               GAMMA*G =   0.001954


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WF_tuned                                                                  IPASS= 1


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 1


                OPEN FILE zgoubi.plt                                                                      
                FOR PRINTING TRAJECTORIES

Entrance hard edge is to be implemented
Exit hard edge is to be implemented

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.84079E+05 V/m
                               B                       =  4.07851E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =   1.0000     C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80484    

                FACE  D'ENTREE
                DX =   0.000,  LAMBDA_E, LAMBDA_B =   0.000  0.000 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )

                FACE  DE  SORTIE
                DX =   0.000,  LAMBDA_E, LAMBDA_B =   0.000  0.000 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )

  ***  Warning : hard-edge fringe model entails vertical wedge focusing simulated with
                  first order kick  ***

                    Integration step :  0.1000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           50.000    -0.000    -0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           50.000    -0.000    -0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           50.000    -0.000    -0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (     1500.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.0000000     RAD


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WF_tuned                                                                  IPASS= 1


************************************************************************************************************************************
      7  Keyword, label(s) :  DRIFT                                                                                    IPASS= 1


                              Drift,  length =     0.50000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1 -3.755693E-09 -2.170116E-07 -8.318288E-06 -1.377534E-15 -5.402093E-14  5.0500000E+01  2.09297E-03
TRAJ #1 SX, SY, SZ, |S| :  1    8.657216E-01  -5.005258E-01  -2.433066E-17   1.000000E+00

 Cumulative length of optical axis =   0.505000000     m   ;  Time  (for reference rigidity & particle) =   2.092968E-09 s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.050000E+01     1
               Time of flight (mus) :  2.09296780E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.050000E+01     2
               Time of flight (mus) :  2.09296780E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   5.050000E+01     3
               Time of flight (mus) :  2.09296780E-03 mass (MeV/c2) :  0.510999    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00  -2.170116E-09  -8.318288E-09        3        3    1.000      (Y,T)         1
   0.0000E+00   0.0000E+00   1.0000E+00  -1.377534E-17  -5.402093E-17        3        3    1.000      (Z,P)         1
   0.0000E+00   0.0000E+00   1.0000E+00   2.092968E-03   3.500000E-01        3        3    1.000      (t,K)         1

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  SPNPRT                                                                                   IPASS= 1



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455416   0.121732   0.333333   0.577350   0.001954  20.023195  14.158537


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.865722 -0.500526 -0.000000  1.000000      1.6849   30.035  -90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500526  0.865722  0.000000  1.000000      1.6849   30.035   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   -0.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6572E-01  8.6572E-01 -5.0053E-01 -5.0053E-01 -2.4331E-17 -2.4331E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      1   1
  5.0053E-01  5.0053E-01  8.6572E-01  8.6572E-01  3.4211E-18  3.4211E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      2   1
  1.9359E-17  1.9359E-17 -1.5118E-17 -1.5118E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      3   1

************************************************************************************************************************************
     10  Keyword, label(s) :  MARKER      #S_WF_tuned                                                                  IPASS= 1


************************************************************************************************************************************
     11  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 1


     zgoubi.plt                                                                      
      already open...
Entrance hard edge is to be implemented
Exit hard edge is to be implemented

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.84079E+05 V/m
                               B                       =  4.07851E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =   1.0000     C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80484    

                FACE  D'ENTREE
                DX =   0.000,  LAMBDA_E, LAMBDA_B =   0.000  0.000 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )

                FACE  DE  SORTIE
                DX =   0.000,  LAMBDA_E, LAMBDA_B =   0.000  0.000 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )

  ***  Warning : hard-edge fringe model entails vertical wedge focusing simulated with
                  first order kick  ***

                    Integration step :  0.1000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           50.000    -0.000    -0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           50.000    -0.000    -0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           50.000    -0.000    -0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (     1500.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.0000000     RAD


 Cumulative length of optical axis =    1.00500000     m ;  Time  (for ref. rigidity & particle) =   4.165213E-09 s 

************************************************************************************************************************************
     12  Keyword, label(s) :  MARKER      #E_WF_tuned                                                                  IPASS= 1


************************************************************************************************************************************
     13  Keyword, label(s) :  DRIFT                                                                                    IPASS= 1


                              Drift,  length =     0.50000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1 -1.401514E-08 -8.056650E-07 -1.440564E-05 -5.483125E-15 -1.080419E-13  1.0100000E+02  4.18594E-03
TRAJ #1 SX, SY, SZ, |S| :  1    4.989478E-01  -8.666320E-01  -6.067520E-17   1.000000E+00

 Cumulative length of optical axis =    1.01000000     m   ;  Time  (for reference rigidity & particle) =   4.185936E-09 s 

************************************************************************************************************************************
     14  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #     13)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   1.010000E+02     1
               Time of flight (mus) :  4.18593560E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   1.010000E+02     2
               Time of flight (mus) :  4.18593560E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   1.010000E+02     3
               Time of flight (mus) :  4.18593560E-03 mass (MeV/c2) :  0.510999    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00  -8.056650E-09  -1.440564E-08        3        3    1.000      (Y,T)         1
   0.0000E+00   0.0000E+00   1.0000E+00  -5.483125E-17  -1.080419E-16        3        0    0.000      (Z,P)         1
   0.0000E+00   0.0000E+00   1.0000E+00   4.185936E-03   3.500000E-01        3        0    0.000      (t,K)         1

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   1.519291E-64

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     15  Keyword, label(s) :  SPNPRT                                                                                   IPASS= 1



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455193  -0.122561   0.333333   0.577350   0.001954  40.046391  28.317075


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.498948 -0.866632 -0.000000  1.000000      1.6849   60.070  -90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.866632  0.498948  0.000000  1.000000      1.6849   60.070   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   -0.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  4.9895E-01  8.6572E-01 -8.6663E-01 -5.0053E-01 -6.0675E-17 -2.4331E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      1   1
  5.0053E-01  8.6663E-01  4.9895E-01  8.6572E-01  3.4211E-18  7.7664E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      2   1
  1.9359E-17  2.3580E-17 -5.6406E-17 -1.5118E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      3   1

************************************************************************************************************************************
     16  Keyword, label(s) :  MARKER      #S_WF_tuned                                                                  IPASS= 1


************************************************************************************************************************************
     17  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 1


     zgoubi.plt                                                                      
      already open...
Entrance hard edge is to be implemented
Exit hard edge is to be implemented

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.84079E+05 V/m
                               B                       =  4.07851E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =   1.0000     C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80484    

                FACE  D'ENTREE
                DX =   0.000,  LAMBDA_E, LAMBDA_B =   0.000  0.000 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )

                FACE  DE  SORTIE
                DX =   0.000,  LAMBDA_E, LAMBDA_B =   0.000  0.000 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )

  ***  Warning : hard-edge fringe model entails vertical wedge focusing simulated with
                  first order kick  ***

                    Integration step :  0.1000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           50.000    -0.000    -0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           50.000    -0.000    -0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           50.000    -0.000    -0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (     1500.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.0000000     RAD


 Cumulative length of optical axis =    1.51000000     m ;  Time  (for ref. rigidity & particle) =   6.258181E-09 s 

************************************************************************************************************************************
     18  Keyword, label(s) :  MARKER      #E_WF_tuned                                                                  IPASS= 1


************************************************************************************************************************************
     19  Keyword, label(s) :  DRIFT                                                                                    IPASS= 1


                              Drift,  length =     0.50000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1 -2.802787E-08 -1.608147E-06 -1.663182E-05 -1.231677E-14 -1.620628E-13  1.5150000E+02  6.27890E-03
TRAJ #1 SX, SY, SZ, |S| :  1   -1.821778E-03  -9.999983E-01  -1.128538E-16   1.000000E+00

 Cumulative length of optical axis =    1.51500000     m   ;  Time  (for reference rigidity & particle) =   6.278903E-09 s 

************************************************************************************************************************************
     20  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #     19)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   1.515000E+02     1
               Time of flight (mus) :  6.27890342E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   1.515000E+02     2
               Time of flight (mus) :  6.27890342E-03 mass (MeV/c2) :  0.510999    
o  1   1.0000     0.000     0.000     0.000     0.000      0.0000   -0.0000   -0.000   -0.000   -0.000   -0.000   1.515000E+02     3
               Time of flight (mus) :  6.27890342E-03 mass (MeV/c2) :  0.510999    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00  -1.608147E-08  -1.663182E-08        3        3    1.000      (Y,T)         1
   0.0000E+00   0.0000E+00   1.0000E+00  -1.231677E-16  -1.620628E-16        3        0    0.000      (Z,P)         1
   0.0000E+00   0.0000E+00   1.0000E+00   6.278903E-03   3.500000E-01        3        0    0.000      (t,K)         1

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   6.077163E-64

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     21  Keyword, label(s) :  SPNPRT      MATRIX                                                                       IPASS= 1



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.332726  -0.333940   0.333333   0.577350   0.001954  60.069587  42.475612


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000    -0.001822 -0.999998 -0.000000  1.000000      1.6849  -90.104  -90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.999998 -0.001822 -0.000000  1.000000      1.6849  -90.104  -90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   -0.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

 -1.8218E-03  8.6572E-01 -1.0000E+00 -5.0053E-01 -1.1285E-16 -2.4331E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      1   1
  5.0053E-01  1.0000E+00 -1.8218E-03  8.6572E-01 -1.7057E-18  7.7664E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      2   1
  1.6078E-18  2.3580E-17 -1.1277E-16 -1.5118E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00      3   1


                  Spin transfer matrix, momentum group # 1 :

         -1.821778E-03    0.999998        1.607808E-18
         -0.999998       -1.821778E-03   -1.127674E-16
         -1.128538E-16   -1.705680E-18     1.00000    

     Trace =       0.9963564447,    ;   spin precession acos((trace-1)/2) =      90.1043802288 deg
     Rotation axis :   ( 0.0000,  0.0000, -1.0000)   ->   angle to (X,Y) plane,  angle to X axis :   -90.0000,    45.8637  degree
     Spin  tune  Qs =     2.5029E-01

************************************************************************************************************************************
     22  Keyword, label(s) :  SYSTEM                                                                                   IPASS= 1

     Number  of  commands :   2,  as  follows : 

 gnuplot < ./gnuplot_spin.gnu
 okular ./gnuplot_spin.eps &

************************************************************************************************************************************
     23  Keyword, label(s) :  END                                                                                      IPASS= 1


                             3 particles have been launched
                     Made  it  to  the  end :      3

************************************************************************************************************************************
 Pgm zgoubi : Execution ended normally, upon keyword END or FIN
   
            ZGOUBI RUN COMPLETED. 

  Zgoubi, author's dvlpmnt version.
  Job  started  on  04-09-2019,  at  01:23:03 
  JOB  ENDED  ON    04-09-2019,  AT  01:23:05 

   CPU time, total :    0.30341899999999999     
