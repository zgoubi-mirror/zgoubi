

set xlabel "step size [cm]"
set ylabel "{/Symbol d}E/E"   font 'roman, 24'
set y2label "{/Symbol d}B/B"   font 'roman, 24'

set k c l

set xtics mirror
set ytics nomirror
set y2tics

set logscale x
set logscale x2

system "rm -f grep.res4E.out"
system "grep '5   1    11 ' zgoubi.res |  cat > grep.res4E.out"
system "rm -f grep.res4B.out"
system "grep '5   2    12 ' zgoubi.res |  cat > grep.res4B.out"

stpi = 0.01
stpf = 10.
nrblt = 40
dstp = (stpf-stpi)/(nrblt-1)
Einf= -982939.51
Binf= 4.07378365E-03


plot   \
  "grep.res4E.out" u (stpi + dstp*$0):(abs(($6-Einf)/Einf)) axes x1y1 w p pt 5 ps 1 lc rgb "red" tit "E" ,\
  "grep.res4B.out" u (stpi + dstp*$0):(abs($6-Binf)/Binf) axes x1y2 w l lw 2 lt 1 lc rgb "blue" tit "B" 

 set terminal postscript eps blacktext color enh    # size 8.3cm,4cm "Times-Roman" 12 
 set output "gnuplot_scanEB.eps" 
 replot 
 set terminal X11 
 unset output 

pause .2
quit
