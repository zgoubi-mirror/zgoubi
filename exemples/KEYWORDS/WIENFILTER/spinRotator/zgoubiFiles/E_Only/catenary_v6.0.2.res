E field only
 'OBJET'                                                                                                      1
2.3114795386518345           ! Rigidity of a 350 keV electron.
2
3  1                         ! 3 electrons, reason: see SPNTRK below.
0.  0. 0. 0. 0. 1. 'o'
0.  0. 0. 0. 0. 1. 'o'
0.  0. 0. 0. 0. 1. 'o'
1 1 1
 
 'PARTICUL'                                                                                                   2
.511 1.602e-19 1.1965e-3 0. 0.
 'SPNTRK'                      ! Allows chceking rotation of all 3 spin components.                           3
4                             ! (they are computed independently by zgoubi)
1. 0. 0.
0. 1. 0.
0. 0. 1.
 
 'WIENFILT'                                                                                                   4
20                    ! Log to zgoubi.plt, every other 10 step.
0.5  -982939.4958 0.    1
0. 0. 0.     ! 20. 5. 5.      ! Hard-edge entrance face.
0.2401  1.8639  -0.5572  0.3904 0. 0.
0.2401  1.8639  -0.5572  0.3904 0. 0.
0. 0. 0.     ! 20. 5. 5.      ! Hard-edge exit face.
0.2401  1.8639  -0.5572  0.3904 0. 0.
0.2401  1.8639  -0.5572  0.3904 0. 0.
.001
1. 0. 0. 0.
 'DRIFT'                                                                                                      5
0.
 'FAISCEAU'     ! Get some trajectory and some                                                                6
 
 'FAISTORE'                   ! In order to be able to get final Y with many digits                           7
zgoubi.fai
1
 
 'SPNPRT'  MATRIX                                                                                             8
 
 'END'                                                                                                        9

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                             

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                          


     Particle  properties :

                     Mass          =   0.511000        MeV/c2
                     Charge        =   1.602000E-19    C     
                     G  factor     =   1.196500E-03          

              Reference  data :
                    mag. rigidity (kG.cm)   :   2.3114795      =p/q, such that dev.=B*L/rigidity
                    mass (MeV/c2)           :  0.51100000    
                    momentum (MeV/c)        :  0.69288780    
                    energy, total (MeV)     :  0.86093815    
                    energy, kinetic (MeV)   :  0.34993815    
                    beta = v/c              :  0.8048055453    
                    gamma                   :   1.684810475    
                    beta*gamma              :   1.355944813    
                    G*gamma                 :  2.0158757329E-03
                    electric rigidity (MeV) :  0.5575785167      =T[eV]*(gamma+1)/gamma, such that dev.=E*L/rigidity
  
 I, AMQ(1,I), AMQ(2,I)/QE, P/Pref, v/c, time :
  
     1   5.11000000E-01  9.99889845E-01  1.00000000E+00  8.04805545E-01  0.00000000E+00
     2   5.11000000E-01  9.99889845E-01  1.00000000E+00  8.04805545E-01  0.00000000E+00
     3   5.11000000E-01  9.99889845E-01  1.00000000E+00  8.04805545E-01  0.00000000E+00

************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                            


                SPIN  TRACKING  REQUESTED  

                          PARTICLE  MASS          =   0.5110000     MeV/c2
                          GYROMAGNETIC  FACTOR  G =   1.1965000E-03

                          INITIAL SPIN CONDITIONS TYPE  4 :
                              All spins entered particle by particle
                              Particles # 1 to      30 may be subjected to spin matching using FIT procedure


                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO   =         2.311 KG*CM
                               BETA   =    0.804806
                               GAMMA*G =   0.002016


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.3333
                               <SY> =     0.3333
                               <SZ> =     0.3333
                               <S>  =     0.5774

************************************************************************************************************************************
      4  Keyword, label(s) :  WIENFILT                          


                OPEN FILE zgoubi.plt                                                                      
                FOR PRINTING TRAJECTORIES

Entrance hard edge is to be implemented
Exit hard edge is to be implemented

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.82939E+05 V/m
                               B                       =   0.0000     T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  0.99989     C
                               Reference  beta         =  0.80481    

                FACE  D'ENTREE
                DX =   0.000,  LAMBDA-E, B =   0.000  0.000 CM

                FACE  DE  SORTIE
                DX =   0.000,  LAMBDA-E, B =   0.000  0.000 CM

  ***  Warning : sharp edge model entails vertical wedge focusing simulated with
                  first order kick  ***

                    Integration step :  1.0000E-03 cm



                CONDITIONS  DE  MAXWELL  (   169245.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072327E-09 s 

************************************************************************************************************************************
      5  Keyword, label(s) :  DRIFT                             


                              Drift,  length =     0.00000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  3.841413E-01  2.297012E+01  7.634288E+02 -1.406514E-15 -4.233620E-14   5.6415343E+01   2.25048E-03
TRAJ #1 SX, SY, SZ, |S| :  1    9.421809E-01   3.351047E-01  -2.051925E-17   1.000000E+00

 Cumulative length of optical axis =   0.500000000     m   ;  Time  (for reference rigidity & particle) =   2.072327E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  FAISCEAU                          

0                                             TRACE DU FAISCEAU

                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(CM)    T(MR)    Z(CM)    P(MR)      S(CM)          D       Y(CM)    T(MR)    Z(CM)    P(MR)      S(CM)

o  1   1.0000     0.000     0.000     0.000     0.000       0.0000    1.3841   22.970  763.429   -0.000   -0.000   5.641534E+01     1
               Time of flight (mus) :  2.25047784E-03 mass (MeV/c2) :  0.511000    
o  1   1.0000     0.000     0.000     0.000     0.000       0.0000    1.3841   22.970  763.429   -0.000   -0.000   5.641534E+01     2
               Time of flight (mus) :  2.25047784E-03 mass (MeV/c2) :  0.511000    
o  1   1.0000     0.000     0.000     0.000     0.000       0.0000    1.3841   22.970  763.429   -0.000   -0.000   5.641534E+01     3
               Time of flight (mus) :  2.25047784E-03 mass (MeV/c2) :  0.511000    


  Beam  characteristics   (EMIT,ALP,BET,XM,XPM,NLIV,NINL,RATIN) : 

    0.000        0.000        0.000       0.2297       0.7634           3       0    0.000    B-Dim     1      1
    0.000        0.000        0.000      -1.4065E-17  -4.2336E-17       3       0    0.000    B-Dim     2      1
    0.000        0.000        0.000       2.2505E-03   0.5757           3       0    0.000    B-Dim     3      1


  Beam  characteristics   SIGMA(4,4) : 

           Ex, Ez =  0.000000E+00  0.000000E+00
           AlpX, BetX =           NaN           NaN
           AlpZ, BetZ =           NaN           NaN

  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00

  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00

  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00

  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00

************************************************************************************************************************************
      7  Keyword, label(s) :  FAISTORE                          


                OPEN FILE zgoubi.fai                                                                      
                FOR PRINTING COORDINATES 

               Print will occur at element[s] labeled : 


************************************************************************************************************************************
      8  Keyword, label(s) :  SPNPRT      MATRIX                



                          Average  over  particles at this pass ;   beam with       3  particles :

                   INITIAL                                           FINAL

        <SX>        <SY>        <SZ>        <S>                   <SX>       <SY>         <SZ>         <S>  <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
    0.333333    0.333333    0.333333    0.577350              0.202359    0.425762    0.333333    0.577350   13.052603    9.229584


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|               SX        SY        SZ        |S|        GAMMA    (Si,Sf)   (Si,Sf_x)
                                                                                                              (deg.)     (deg.)
                                                                                           (Sf_x : projection of Sf on plane x=0)

 o  1  1.000000  0.000000  0.000000  1.000000           0.942181  0.335105 -0.000000  1.000000      2.1266   19.5789   90.0000    1
 o  1  0.000000  1.000000  0.000000  1.000000          -0.335105  0.942181  0.000000  1.000000      2.1266   19.5789    0.0000    2
 o  1  0.000000  0.000000  1.000000  1.000000           0.000000  0.000000  1.000000  1.000000      2.1266    0.0000    0.0000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  9.4218E-01  9.4218E-01  3.3510E-01  3.3510E-01 -2.0519E-17 -2.0519E-17  1.0000E+00  1.0000E+00  1.38414E+00  2.12661E+00      1   1
 -3.3510E-01 -3.3510E-01  9.4218E-01  9.4218E-01  3.5404E-18  3.5404E-18  1.0000E+00  1.0000E+00  1.38414E+00  2.12661E+00      2   1
  2.0519E-17  2.0519E-17  3.5404E-18  3.5404E-18  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.38414E+00  2.12661E+00      3   1

************************************************************************************************************************************
      9  Keyword, label(s) :  END                               


                             3 particles have been launched
                     Made  it  to  the  end :      3

************************************************************************************************************************************

           MAIN PROGRAM : Execution ended upon key  END       

************************************************************************************************************************************
   
            ZGOUBI RUN COMPLETED. 

  Zgoubi version 6.0 16-Jun-2015.
  Job  started  on  09-09-2019,  at  22:05:02 
  JOB  ENDED  ON    09-09-2019,  AT  22:05:03 

   CPU time, total :    0.70268799999999998     
