set xlabel "Turn number" font "roman,22"
set ylabel "log(invariant)" font "roman,22"
set title "DAMPING OF VERTICAL INVARIANT, 18 GeV" font "roman,24"
 
set grid
 
set key right top font "roman,22" spacing 5.

set xtics           font "roman,16"
set ytics  nomirror font "roman,16"

fit_limit=1e-8

bta = 11.33
alf = 1.e-3
gam = (1.+alf**2)/bta

C = 812.8
c = 2.99792458e8

fac = 2.27

leps1(x) = le01 - x /(fac*tau1)
fit leps1(x) 'zgoubi.fai' using ($38):($26==1 ? log10(gam* ($12/100.)**2 +  bta * ($13/1000.)**2) : 1/0) via  le01, tau1
leps2(x) = le02 - x /(fac*tau2)
fit leps2(x) 'zgoubi.fai' using ($38):($26==2 ? log10(gam* ($12/100.)**2 +  bta * ($13/1000.)**2) : 1/0) via  le02, tau2
leps3(x) = le03 - x /(fac*tau3)
fit leps3(x) 'zgoubi.fai' using ($38):($26==3 ? log10(gam* ($12/100.)**2 +  bta * ($13/1000.)**2) : 1/0) via  le03, tau3

 plot [] \
   'zgoubi.fai' using (10*int($38/10) == $38 ? $38 : 1/0):($26 == 1 ? log10(gam* ($12/100.)**2 +  bta * ($13/1000.)**2) : 1/0) w p pt 4 ps 1 title 'zgoubi, eps_y,0~2e-7  m.rad' ,\
   'zgoubi.fai' using (10*int($38/10) == $38 ? $38 : 1/0):($26 == 2 ? log10(gam* ($12/100.)**2 +  bta * ($13/1000.)**2) : 1/0) w p pt 4 ps 1 title 'zgoubi, eps_y,0~2e-9  m.rad' ,\
   'zgoubi.fai' using (10*int($38/10) == $38 ? $38 : 1/0):($26 == 3 ? log10(gam* ($12/100.)**2 +  bta * ($13/1000.)**2) : 1/0) w p pt 4 ps 1 title 'zgoubi, eps_y,0~2e-11 m.rad' ,\
       leps1(x) wi li lt 1 lw 1. title 'match, tau=48.9' ,\
       leps2(x) wi li lt 1 lw 1. title 'match, tau=48.7' ,\
       leps3(x) wi li lt 1 lw 1. title 'match, tau=48.6'


 set terminal postscript eps blacktext color
 set output "gnuplot_inv_y.vs.turn_18GeV.eps"
 replot
 set terminal X11
 set output

show var

pause 40


