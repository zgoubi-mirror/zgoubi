Centering 5 helical orbits in the AGS warm helical snake 3-D OPERA map.
'OBJET'                                                                                                      1
7.2051782956d3      ! Reference rigidity of the problem
2
1 1                 ! A single particle. Initial coordinates :
-2.2  0. 0. 0. 0.  1. 'o'           ! Yo, To, Zo, Po, So, p/po
1
'PARTICUL'         ! proton / data used for spin tracking :                                                  2
938.27203 1.602176487E-19 1.7928474 0 0     ! M, Q, G factor
 
'SPNTRK'                                                                                                     3
4.1                 ! Initial spin is positionned vertical
0. 0. 1.
 
'TOSCA'                                                                                                      4
0  20
1.e1   100. 100. 100.
HEADER_0 wsnake
801 29 29 12.1
b_table55.tab       ! AGS warm snake 3-D OPERA map
0 0 0 0
2
.1
2  0.  .00  0.  0.
 
'FIT'                                                                                                        5
2                   ! Two variables :
1 30 0  [-3,3]          ! Vary initial coordinate Y_0 (horiz. position)
1 31 0  [-3,3]          ! Vary initial coordinate T_0 (horiz. angle)
3    2E-3   199      ! Three constraints (penalty 2E-3 requested) :
3.1 1 2 4 0. 1. 0       ! Y_0=Y after at exit of the magnet
3.1 1 3 4 0. 1. 0       ! T_0=T after at exit of the magnet
7.3 1 2 4 0. 1. 0       ! Y_min+Y_max=0 inside the OPERA field map
 
'SPNPRT'  PRINT    ! Print spin coordinates in zgoubi.SPNPRT.Out                                             6
 
'REBELOTE'         ! Will re-do the FIT for 4 different rigidities                                           7
4  0.1  0 1
1                   ! List of 4 successive values of D in OBJET follows
OBJET  35  1.3872739973  2.1368296674  4.8261190694  11.015241321
 
'END'                                                                                                        8

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =       7205.178 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 1

     Particle  properties :
     Particle name unknown.
                     Mass          =    938.272        MeV/c2
                     Charge        =   1.602176E-19    C     
                     G  factor     =    1.79285              

              Reference  data :
                    mag. rigidity (kG.cm)   :   7205.1783      =p/q, such that dev.=B*L/rigidity
                    mass (MeV/c2)           :   938.27203    
                    momentum (MeV/c)        :   2160.0581    
                    energy, total (MeV)     :   2355.0383    
                    energy, kinetic (MeV)   :   1416.7663    
                    beta = v/c              :  0.9172072069    
                    gamma                   :   2.509973905    
                    beta*gamma              :   2.302166155    
                    G*gamma                 :   4.500000190    
                    electric rigidity (MeV) :   1981.220867    =T[eV]*(gamma+1)/gamma, such that dev.=E*L/rigidity
  
 I, AMQ(1,I), AMQ(2,I)/QE, P/Pref, v/c, time, s :
  
     1   9.38272030E+02  1.00000000E+00  1.00000000E+00  9.17207207E-01  0.00000000E+00  0.00000000E+00

************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 1


                Spin  tracking  requested.


                          Particle  mass          =    938.2720     MeV/c2
                          Gyromagnetic  factor  G =    1.792847    

                          Initial spin conditions type  4 :
                              KSO2=1. Same spin for all particles
                              Particles # 1 to 1 may be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO    =      7205.178 kG*cm
                               beta    =    0.91720721
                               gamma   =   2.50997391
                               gamma*G =   4.5000001900


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        1  PARTICULES :
                               <SX> =     0.000000
                               <SY> =     0.000000
                               <SZ> =     1.000000
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                                                    IPASS= 1


                OPEN FILE zgoubi.plt                                                                      
                FOR PRINTING TRAJECTORIES


     Status of MOD.MOD2 is 12.1 ; NDIM = 3 ; number of field data files used is   1.

           New field map(s) opened, cartesian mesh (MOD.le.19) ; 
           name(s) of map data file(s) : 

          'b_table55.tab'

   ----
   Map file number    1 ( of 1 ) (unit # 25 ) :
     b_table55.tab field map,
     FORMAT type : regular.

 HEADER  (0 lines) : 
  Sbr fmapw/fmapr3 : completed job of reading map.

     Min/max fields found in map (series) read   :  -2.934230E-01  /   3.119730E-01
       @  X,  Y, Z : -1.045E-02 -6.000E-02  6.500E-02              /  1.050E-02  5.500E-02  7.000E-02
     Given normalisation coeffs on field, x, y, z :   1.000000E+01   1.000000E+02   1.000000E+02   1.000000E+02
     this yields min/max normalised fields (kG) : -2.934230E+00                      3.119730E+00
       @  X (cm),  Y (cm), Z (cm) :  -1.04      -6.00       6.50   /   1.05       5.50       7.00    

     Length of element,  XL =  4.000000E+02 cm 
                                               from  XI =   1.985000E+02 cm 
                                               to    XF =   5.985000E+02 cm 

     Nbr of nodes in X = 801;  nbr of nodes in Y =   29
     X-size of mesh =  5.000000E-01 cm ; Y-size =  5.000000E-01 cm
     nber of mesh nodes in Z =   29 ; Step in Z =  0.500000E+00 cm

                     OPTION  DE  CALCUL  :  GRILLE  3-D  A  3*3*3  POINTS , INTERPOLATION  A  L'ORDRE  2

                    Integration step :  0.1000     cm

  A    1  1.0000    -2.200     0.000     0.000     0.000          598.500    -2.182     0.000    -0.055     0.000            1


                CONDITIONS  DE  MAXWELL  (     4004.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                    -3.2200E-12      -1.0450E-13       -1.6092E-14
                                      1.5310E-12       -6.2215E-13
                                     -2.6895E-13       -3.8766E-12
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  2 :  Element  is  mis-aligned  wrt.  the  optical  axis.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.0000000     RAD


 Cumulative length of optical axis =    4.00000000     m ;  Time  (for ref. rigidity & particle) =   1.454695E-08 s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FIT                                                                                      IPASS= 1

     Pgm main. FITING=.T., FIT procedure launched.

           variable #            1       IR =            1 ,   ok.
           variable #            1       IP =           30 ,   ok.
           variable #            2       IR =            1 ,   ok.
           variable #            2       IP =           31 ,   ok.
           constraint #            1       IR =            4 ,   ok.
           constraint #            1       I  =            1 ,   ok.
           constraint #            2       IR =            4 ,   ok.
           constraint #            2       I  =            1 ,   ok.
           constraint #            3       IR =            4 ,   ok.

                    FIT  variables  and  constraints  in  good  order,  FIT  will proceed. 
 STATUS OF VARIABLES  (Iteration #   200 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   1   1    30   -3.00       -1.82      -1.8229718       3.00      1.862-262  OBJET      -                    -                   
   1   2    31   -3.00       4.214E-02  4.21399177E-02   3.00      1.862-262  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-03)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     4    0.000000E+00    1.000E+00    3.167273E-05    1.23E-07 TOSCA      -                    -                    0
  3   1   3     4    0.000000E+00    1.000E+00    8.797328E-02    9.49E-01 TOSCA      -                    -                    0
  7   1   2     4    0.000000E+00    1.000E+00   -2.030356E-02    5.06E-02 TOSCA      -                    -                    0
 Fit reached penalty value   8.1515E-03



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =       7205.178 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 1


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 1


                Spin  tracking  requested.


                          Particle  mass          =    938.2720     MeV/c2
                          Gyromagnetic  factor  G =    1.792847    

                          Initial spin conditions type  4 :
                              KSO2=1. Same spin for all particles
                              Particles # 1 to 1 may be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO    =      7205.178 kG*cm
                               beta    =    0.91720721
                               gamma   =   2.50997391
                               gamma*G =   4.5000001900


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        1  PARTICULES :
                               <SX> =     0.000000
                               <SY> =     0.000000
                               <SZ> =     1.000000
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                                                    IPASS= 1


     zgoubi.plt                                                                      
      already open...

     Status of MOD.MOD2 is 12.1 ; NDIM = 3 ; number of field data files used is   1.

     No  map  file  opened :  was  already  stored.
     Skip  reading  field  map  file :           b_table55.tab
     Restored mesh coordinates for field map #IMAP=   1,  name : b_table55.tab

     Min/max fields found in map (series) read   :  -2.934230E-01  /   3.119730E-01
       @  X,  Y, Z : -1.045E-02 -6.000E-02  6.500E-02              /  1.050E-02  5.500E-02  7.000E-02
     Given normalisation coeffs on field, x, y, z :   1.000000E+01   1.000000E+02   1.000000E+02   1.000000E+02
     this yields min/max normalised fields (kG) : -2.934230E+00                      3.119730E+00
       @  X (cm),  Y (cm), Z (cm) :  -1.04      -6.00       6.50   /   1.05       5.50       7.00    

     Length of element,  XL =  4.000000E+02 cm 
                                               from  XI =   1.985000E+02 cm 
                                               to    XF =   5.985000E+02 cm 

     Nbr of nodes in X = 801;  nbr of nodes in Y =   29
     X-size of mesh =  5.000000E-01 cm ; Y-size =  5.000000E-01 cm
     nber of mesh nodes in Z =   29 ; Step in Z =  0.500000E+00 cm

                     OPTION  DE  CALCUL  :  GRILLE  3-D  A  3*3*3  POINTS , INTERPOLATION  A  L'ORDRE  2

                    Integration step :  0.1000     cm

  A    1  1.0000    -1.823     0.042     0.000     0.000          598.500    -1.823    -0.000    -0.045     0.000            1


                CONDITIONS  DE  MAXWELL  (     4004.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                    -2.6736E-12      -1.0450E-13        7.7656E-15
                                      1.5310E-12       -6.2406E-13
                                     -2.6895E-13       -3.8831E-12
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  2 :  Element  is  mis-aligned  wrt.  the  optical  axis.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.0000000     RAD


 Cumulative length of optical axis =    4.00000000     m ;  Time  (for ref. rigidity & particle) =   1.454695E-08 s 

************************************************************************************************************************************

      5   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      6  Keyword, label(s) :  SPNPRT      PRINT                                                                        IPASS= 1



                         Momentum  group  #1 ; average  over 1 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.000000   0.000000   1.000000   1.000000        -0.000948   0.208319   0.978060   1.000000   4.500000  12.023985   0.000000


                Spin  components  of  each  of  the      1  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   |Si,Sf|   (Z,Sf_yz) (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_yz : projection of Sf on YZ plane)

 o  1  0.000000  0.000000  1.000000  1.000000    -0.000948  0.208319  0.978060  1.000000      2.5100   12.024   45.635   12.024    1



                Min/Max  components  of  each  of  the      1  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

 -9.4780E-04 -9.4780E-04  2.0832E-01  2.0832E-01  9.7806E-01  9.7806E-01  1.0000E+00  1.0000E+00  1.00000E+00  2.50997E+00     1   1

************************************************************************************************************************************
      7  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 1


 Pgm rebel. At pass #    1/   5.  In element #    1,  parameter # 35  changed to    1.38727400E+00   (was    1.00000000E+00)

                                -----  REBELOTE  -----

     End of pass #        1 through the optical structure 

                     Total of          1 particles have been launched

     Multiple pass, 
          from element #     1 : OBJET     /label1=                    /label2=                    
                             to  REBELOTE  /label1=                    /label2=                    
     ending at pass #       5 at element #     7 : REBELOTE  /label1=                    /label2=                    


     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #   200 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   1   1    30   -3.00       -1.31      -1.3064892       3.00      9.315-246  OBJET      -                    -                   
   1   2    31   -3.00       2.509E-02  2.50853810E-02   3.00      9.315-246  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-03)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     4    0.000000E+00    1.000E+00    4.251973E-05    6.36E-07 TOSCA      -                    -                    0
  3   1   3     4    0.000000E+00    1.000E+00    5.291177E-02    9.85E-01 TOSCA      -                    -                    0
  7   1   2     4    0.000000E+00    1.000E+00   -6.418337E-03    1.45E-02 TOSCA      -                    -                    0
 Fit reached penalty value   2.8409E-03



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 2

                          MAGNETIC  RIGIDITY =       7205.178 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 2


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 2


                Spin  tracking  requested.


                          Particle  mass          =    938.2720     MeV/c2
                          Gyromagnetic  factor  G =    1.792847    

                          Initial spin conditions type  4 :
                              KSO2=1. Same spin for all particles
                              Particles # 1 to 1 may be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO    =      7205.178 kG*cm
                               beta    =    0.91720721
                               gamma   =   2.50997391
                               gamma*G =   4.5000001900


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        1  PARTICULES :
                               <SX> =     0.000000
                               <SY> =     0.000000
                               <SZ> =     1.000000
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                                                    IPASS= 2


     zgoubi.plt                                                                      
      already open...

     Status of MOD.MOD2 is 12.1 ; NDIM = 3 ; number of field data files used is   1.

     No  map  file  opened :  was  already  stored.
     Skip  reading  field  map  file :           b_table55.tab
     Restored mesh coordinates for field map #IMAP=   1,  name : b_table55.tab

     Min/max fields found in map (series) read   :  -2.934230E-01  /   3.119730E-01
       @  X,  Y, Z : -1.045E-02 -6.000E-02  6.500E-02              /  1.050E-02  5.500E-02  7.000E-02
     Given normalisation coeffs on field, x, y, z :   1.000000E+01   1.000000E+02   1.000000E+02   1.000000E+02
     this yields min/max normalised fields (kG) : -2.934230E+00                      3.119730E+00
       @  X (cm),  Y (cm), Z (cm) :  -1.04      -6.00       6.50   /   1.05       5.50       7.00    

     Length of element,  XL =  4.000000E+02 cm 
                                               from  XI =   1.985000E+02 cm 
                                               to    XF =   5.985000E+02 cm 

     Nbr of nodes in X = 801;  nbr of nodes in Y =   29
     X-size of mesh =  5.000000E-01 cm ; Y-size =  5.000000E-01 cm
     nber of mesh nodes in Z =   29 ; Step in Z =  0.500000E+00 cm

                     OPTION  DE  CALCUL  :  GRILLE  3-D  A  3*3*3  POINTS , INTERPOLATION  A  L'ORDRE  2

                    Integration step :  0.1000     cm

  A    1  1.3873    -1.306     0.025     0.000     0.000          598.500    -1.307    -0.000    -0.038     0.000            1


                CONDITIONS  DE  MAXWELL  (     4002.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                    -1.4081E-12      -5.6634E-14       -3.0228E-15
                                      9.7427E-13       -4.4164E-13
                                     -1.4333E-13       -2.7470E-12
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  2 :  Element  is  mis-aligned  wrt.  the  optical  axis.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.0000000     RAD


 Cumulative length of optical axis =    4.00000000     m ;  Time  (for ref. rigidity & particle) =   1.454695E-08 s 

************************************************************************************************************************************

      5   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      6  Keyword, label(s) :  SPNPRT      PRINT                                                                        IPASS= 2



                         Momentum  group  #1 ; average  over 1 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.000000   0.000000   1.000000   1.000000        -0.000668   0.196151   0.980574   1.000000   6.000000  11.312014   0.000000


                Spin  components  of  each  of  the      1  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   |Si,Sf|   (Z,Sf_yz) (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_yz : projection of Sf on YZ plane)

 o  1  0.000000  0.000000  1.000000  1.000000    -0.000668  0.196151  0.980574  1.000000      3.3466   11.312   45.562   11.312    1



                Min/Max  components  of  each  of  the      1  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

 -9.4780E-04 -6.6785E-04  1.9615E-01  2.0832E-01  9.7806E-01  9.8057E-01  1.0000E+00  1.0000E+00  1.38727E+00  3.34663E+00     1   1

************************************************************************************************************************************
      7  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 2


 Pgm rebel. At pass #    2/   5.  In element #    1,  parameter # 35  changed to    2.13682967E+00   (was    1.38727400E+00)

                                -----  REBELOTE  -----

     End of pass #        2 through the optical structure 

                     Total of          2 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     5 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   1   1    30   -3.00      -0.826     -0.82648915       3.00      2.000E-02  OBJET      -                    -                   
   1   2    31   -3.00      -3.491E-02 -3.49146190E-02   3.00      2.000E-02  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-03)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     4    0.000000E+00    1.000E+00    1.977033E-02    2.67E-01 TOSCA      -                    -                    0
  3   1   3     4    0.000000E+00    1.000E+00    3.057738E-02    6.40E-01 TOSCA      -                    -                    0
  7   1   2     4    0.000000E+00    1.000E+00    1.165131E-02    9.29E-02 TOSCA      -                    -                    0
 Fit reached penalty value   1.4616E-03



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 3

                          MAGNETIC  RIGIDITY =       7205.178 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 3


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 3


                Spin  tracking  requested.


                          Particle  mass          =    938.2720     MeV/c2
                          Gyromagnetic  factor  G =    1.792847    

                          Initial spin conditions type  4 :
                              KSO2=1. Same spin for all particles
                              Particles # 1 to 1 may be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO    =      7205.178 kG*cm
                               beta    =    0.91720721
                               gamma   =   2.50997391
                               gamma*G =   4.5000001900


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        1  PARTICULES :
                               <SX> =     0.000000
                               <SY> =     0.000000
                               <SZ> =     1.000000
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                                                    IPASS= 3


     zgoubi.plt                                                                      
      already open...

     Status of MOD.MOD2 is 12.1 ; NDIM = 3 ; number of field data files used is   1.

     No  map  file  opened :  was  already  stored.
     Skip  reading  field  map  file :           b_table55.tab
     Restored mesh coordinates for field map #IMAP=   1,  name : b_table55.tab

     Min/max fields found in map (series) read   :  -2.934230E-01  /   3.119730E-01
       @  X,  Y, Z : -1.045E-02 -6.000E-02  6.500E-02              /  1.050E-02  5.500E-02  7.000E-02
     Given normalisation coeffs on field, x, y, z :   1.000000E+01   1.000000E+02   1.000000E+02   1.000000E+02
     this yields min/max normalised fields (kG) : -2.934230E+00                      3.119730E+00
       @  X (cm),  Y (cm), Z (cm) :  -1.04      -6.00       6.50   /   1.05       5.50       7.00    

     Length of element,  XL =  4.000000E+02 cm 
                                               from  XI =   1.985000E+02 cm 
                                               to    XF =   5.985000E+02 cm 

     Nbr of nodes in X = 801;  nbr of nodes in Y =   29
     X-size of mesh =  5.000000E-01 cm ; Y-size =  5.000000E-01 cm
     nber of mesh nodes in Z =   29 ; Step in Z =  0.500000E+00 cm

                     OPTION  DE  CALCUL  :  GRILLE  3-D  A  3*3*3  POINTS , INTERPOLATION  A  L'ORDRE  2

                    Integration step :  0.1000     cm

  A    1  2.1368    -0.826    -0.035     0.000     0.000          598.500    -0.846    -0.000    -0.027     0.000            1


                CONDITIONS  DE  MAXWELL  (     4001.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                    -5.9221E-13      -2.3948E-14        1.9382E-16
                                      6.9905E-13       -2.8736E-13
                                     -1.0721E-13       -1.7891E-12
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  2 :  Element  is  mis-aligned  wrt.  the  optical  axis.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.0000000     RAD


 Cumulative length of optical axis =    4.00000000     m ;  Time  (for ref. rigidity & particle) =   1.454695E-08 s 

************************************************************************************************************************************

      5   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      6  Keyword, label(s) :  SPNPRT      PRINT                                                                        IPASS= 3



                         Momentum  group  #1 ; average  over 1 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.000000   0.000000   1.000000   1.000000        -0.000493   0.188577   0.982058   1.000000   9.000000  10.869780   0.000000


                Spin  components  of  each  of  the      1  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   |Si,Sf|   (Z,Sf_yz) (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_yz : projection of Sf on YZ plane)

 o  1  0.000000  0.000000  1.000000  1.000000    -0.000493  0.188577  0.982058  1.000000      5.0199   10.870   45.519   10.870    1



                Min/Max  components  of  each  of  the      1  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

 -9.4780E-04 -4.9306E-04  1.8858E-01  2.0832E-01  9.7806E-01  9.8206E-01  1.0000E+00  1.0000E+00  2.13683E+00  5.01995E+00     1   1

************************************************************************************************************************************
      7  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 3


 Pgm rebel. At pass #    3/   5.  In element #    1,  parameter # 35  changed to    4.82611907E+00   (was    2.13682967E+00)

                                -----  REBELOTE  -----

     End of pass #        3 through the optical structure 

                     Total of          3 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     4 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   1   1    30   -3.00      -0.386     -0.38648915       3.00      6.667E-03  OBJET      -                    -                   
   1   2    31   -3.00       6.509E-02  6.50853810E-02   3.00      6.667E-03  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-03)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     4    0.000000E+00    1.000E+00    2.372662E-02    7.29E-01 TOSCA      -                    -                    0
  3   1   3     4    0.000000E+00    1.000E+00    1.229049E-02    1.96E-01 TOSCA      -                    -                    0
  7   1   2     4    0.000000E+00    1.000E+00   -7.617293E-03    7.52E-02 TOSCA      -                    -                    0
 Fit reached penalty value   7.7203E-04



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 4

                          MAGNETIC  RIGIDITY =       7205.178 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 4


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 4


                Spin  tracking  requested.


                          Particle  mass          =    938.2720     MeV/c2
                          Gyromagnetic  factor  G =    1.792847    

                          Initial spin conditions type  4 :
                              KSO2=1. Same spin for all particles
                              Particles # 1 to 1 may be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO    =      7205.178 kG*cm
                               beta    =    0.91720721
                               gamma   =   2.50997391
                               gamma*G =   4.5000001900


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        1  PARTICULES :
                               <SX> =     0.000000
                               <SY> =     0.000000
                               <SZ> =     1.000000
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                                                    IPASS= 4


     zgoubi.plt                                                                      
      already open...

     Status of MOD.MOD2 is 12.1 ; NDIM = 3 ; number of field data files used is   1.

     No  map  file  opened :  was  already  stored.
     Skip  reading  field  map  file :           b_table55.tab
     Restored mesh coordinates for field map #IMAP=   1,  name : b_table55.tab

     Min/max fields found in map (series) read   :  -2.934230E-01  /   3.119730E-01
       @  X,  Y, Z : -1.045E-02 -6.000E-02  6.500E-02              /  1.050E-02  5.500E-02  7.000E-02
     Given normalisation coeffs on field, x, y, z :   1.000000E+01   1.000000E+02   1.000000E+02   1.000000E+02
     this yields min/max normalised fields (kG) : -2.934230E+00                      3.119730E+00
       @  X (cm),  Y (cm), Z (cm) :  -1.04      -6.00       6.50   /   1.05       5.50       7.00    

     Length of element,  XL =  4.000000E+02 cm 
                                               from  XI =   1.985000E+02 cm 
                                               to    XF =   5.985000E+02 cm 

     Nbr of nodes in X = 801;  nbr of nodes in Y =   29
     X-size of mesh =  5.000000E-01 cm ; Y-size =  5.000000E-01 cm
     nber of mesh nodes in Z =   29 ; Step in Z =  0.500000E+00 cm

                     OPTION  DE  CALCUL  :  GRILLE  3-D  A  3*3*3  POINTS , INTERPOLATION  A  L'ORDRE  2

                    Integration step :  0.1000     cm

  A    1  4.8261    -0.386     0.065     0.000     0.000          598.500    -0.363     0.000    -0.013    -0.000            1


                CONDITIONS  DE  MAXWELL  (     4001.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                    -1.0697E-13      -5.7188E-15       -6.1631E-16
                                      3.2047E-13       -1.3126E-13
                                     -5.2393E-14       -8.1975E-13
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  2 :  Element  is  mis-aligned  wrt.  the  optical  axis.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.0000000     RAD


 Cumulative length of optical axis =    4.00000000     m ;  Time  (for ref. rigidity & particle) =   1.454695E-08 s 

************************************************************************************************************************************

      5   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      6  Keyword, label(s) :  SPNPRT      PRINT                                                                        IPASS= 4



                         Momentum  group  #1 ; average  over 1 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.000000   0.000000   1.000000   1.000000        -0.000423   0.184148   0.982898   1.000000  20.000001  10.611501   0.000000


                Spin  components  of  each  of  the      1  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   |Si,Sf|   (Z,Sf_yz) (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_yz : projection of Sf on YZ plane)

 o  1  0.000000  0.000000  1.000000  1.000000    -0.000423  0.184148  0.982898  1.000000     11.1554   10.612   45.494   10.612    1



                Min/Max  components  of  each  of  the      1  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

 -9.4780E-04 -4.2267E-04  1.8415E-01  2.0832E-01  9.7806E-01  9.8290E-01  1.0000E+00  1.0000E+00  4.82612E+00  1.11554E+01     1   1

************************************************************************************************************************************
      7  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 4


 Pgm rebel. At pass #    4/   5.  In element #    1,  parameter # 35  changed to    1.10152413E+01   (was    4.82611907E+00)

                                -----  REBELOTE  -----

     End of pass #        4 through the optical structure 

                     Total of          4 particles have been launched


      Next  pass  is  #     5 and  last  pass  through  the  optical  structure


     WRITE statements to zgoubi.res are re-established from now on.

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 5

                          MAGNETIC  RIGIDITY =       7205.178 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 5


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 5


                Spin  tracking  requested.


                          Particle  mass          =    938.2720     MeV/c2
                          Gyromagnetic  factor  G =    1.792847    

                          Initial spin conditions type  4 :
                              KSO2=1. Same spin for all particles
                              Particles # 1 to 1 may be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO    =      7205.178 kG*cm
                               beta    =    0.91720721
                               gamma   =   2.50997391
                               gamma*G =   4.5000001900


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        1  PARTICULES :
                               <SX> =     0.000000
                               <SY> =     0.000000
                               <SZ> =     1.000000
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                                                    IPASS= 5


     zgoubi.plt                                                                      
      already open...

     Status of MOD.MOD2 is 12.1 ; NDIM = 3 ; number of field data files used is   1.

     No  map  file  opened :  was  already  stored.
     Skip  reading  field  map  file :           b_table55.tab
     Restored mesh coordinates for field map #IMAP=   1,  name : b_table55.tab

     Min/max fields found in map (series) read   :  -2.934230E-01  /   3.119730E-01
       @  X,  Y, Z : -1.045E-02 -6.000E-02  6.500E-02              /  1.050E-02  5.500E-02  7.000E-02
     Given normalisation coeffs on field, x, y, z :   1.000000E+01   1.000000E+02   1.000000E+02   1.000000E+02
     this yields min/max normalised fields (kG) : -2.934230E+00                      3.119730E+00
       @  X (cm),  Y (cm), Z (cm) :  -1.04      -6.00       6.50   /   1.05       5.50       7.00    

     Length of element,  XL =  4.000000E+02 cm 
                                               from  XI =   1.985000E+02 cm 
                                               to    XF =   5.985000E+02 cm 

     Nbr of nodes in X = 801;  nbr of nodes in Y =   29
     X-size of mesh =  5.000000E-01 cm ; Y-size =  5.000000E-01 cm
     nber of mesh nodes in Z =   29 ; Step in Z =  0.500000E+00 cm

                     OPTION  DE  CALCUL  :  GRILLE  3-D  A  3*3*3  POINTS , INTERPOLATION  A  L'ORDRE  2

                    Integration step :  0.1000     cm

  A    1 11.0152    -0.386     0.065     0.000     0.000          598.500    -0.361     0.000    -0.006    -0.000            1


                CONDITIONS  DE  MAXWELL  (     4001.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                    -4.6853E-14      -2.5056E-15       -2.4834E-16
                                      1.4041E-13       -5.7860E-14
                                     -2.2955E-14       -3.6148E-13
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  2 :  Element  is  mis-aligned  wrt.  the  optical  axis.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.0000000     RAD


 Cumulative length of optical axis =    4.00000000     m ;  Time  (for ref. rigidity & particle) =   2.909389E-08 s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FIT                                                                                      IPASS= 5


     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 5

                          MAGNETIC  RIGIDITY =       7205.178 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 5


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 5


                Spin  tracking  requested.


                          Particle  mass          =    938.2720     MeV/c2
                          Gyromagnetic  factor  G =    1.792847    

                          Initial spin conditions type  4 :
                              KSO2=1. Same spin for all particles
                              Particles # 1 to 1 may be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO    =      7205.178 kG*cm
                               beta    =    0.91720721
                               gamma   =   2.50997391
                               gamma*G =   4.5000001900


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        1  PARTICULES :
                               <SX> =     0.000000
                               <SY> =     0.000000
                               <SZ> =     1.000000
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                                                    IPASS= 5


     zgoubi.plt                                                                      
      already open...

     Status of MOD.MOD2 is 12.1 ; NDIM = 3 ; number of field data files used is   1.

     No  map  file  opened :  was  already  stored.
     Skip  reading  field  map  file :           b_table55.tab
     Restored mesh coordinates for field map #IMAP=   1,  name : b_table55.tab

     Min/max fields found in map (series) read   :  -2.934230E-01  /   3.119730E-01
       @  X,  Y, Z : -1.045E-02 -6.000E-02  6.500E-02              /  1.050E-02  5.500E-02  7.000E-02
     Given normalisation coeffs on field, x, y, z :   1.000000E+01   1.000000E+02   1.000000E+02   1.000000E+02
     this yields min/max normalised fields (kG) : -2.934230E+00                      3.119730E+00
       @  X (cm),  Y (cm), Z (cm) :  -1.04      -6.00       6.50   /   1.05       5.50       7.00    

     Length of element,  XL =  4.000000E+02 cm 
                                               from  XI =   1.985000E+02 cm 
                                               to    XF =   5.985000E+02 cm 

     Nbr of nodes in X = 801;  nbr of nodes in Y =   29
     X-size of mesh =  5.000000E-01 cm ; Y-size =  5.000000E-01 cm
     nber of mesh nodes in Z =   29 ; Step in Z =  0.500000E+00 cm

                     OPTION  DE  CALCUL  :  GRILLE  3-D  A  3*3*3  POINTS , INTERPOLATION  A  L'ORDRE  2

                    Integration step :  0.1000     cm

  A    1 11.0152    -0.386     0.065     0.000     0.000          598.500    -0.361     0.000    -0.006    -0.000            1


                CONDITIONS  DE  MAXWELL  (     4001.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                    -4.6853E-14      -2.5056E-15       -2.4834E-16
                                      1.4041E-13       -5.7860E-14
                                     -2.2955E-14       -3.6148E-13
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  2 :  Element  is  mis-aligned  wrt.  the  optical  axis.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.0000000     RAD


 Cumulative length of optical axis =    4.00000000     m ;  Time  (for ref. rigidity & particle) =   1.454695E-08 s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FIT                                                                                      IPASS= 5

     Pgm main. FITING=.T., FIT procedure launched.

           variable #            1       IR =            1 ,   ok.
           variable #            1       IP =           30 ,   ok.
           variable #            2       IR =            1 ,   ok.
           variable #            2       IP =           31 ,   ok.
           constraint #            1       IR =            4 ,   ok.
           constraint #            1       I  =            1 ,   ok.
           constraint #            2       IR =            4 ,   ok.
           constraint #            2       I  =            1 ,   ok.
           constraint #            3       IR =            4 ,   ok.

                    FIT  variables  and  constraints  in  good  order,  FIT  will proceed. 
 STATUS OF VARIABLES  (Iteration #     4 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   1   1    30   -3.00      -0.146     -0.14648915       3.00      2.000E-02  OBJET      -                    -                   
   1   2    31   -3.00       5.085E-03  5.08538102E-03   3.00      2.000E-02  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-03)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     4    0.000000E+00    1.000E+00    1.068906E-03    9.03E-04 TOSCA      -                    -                    0
  3   1   3     4    0.000000E+00    1.000E+00    5.145977E-03    2.09E-02 TOSCA      -                    -                    0
  7   1   2     4    0.000000E+00    1.000E+00    3.517070E-02    9.78E-01 TOSCA      -                    -                    0
 Fit reached penalty value   1.2646E-03



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 5

                          MAGNETIC  RIGIDITY =       7205.178 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 5


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 5


                Spin  tracking  requested.


                          Particle  mass          =    938.2720     MeV/c2
                          Gyromagnetic  factor  G =    1.792847    

                          Initial spin conditions type  4 :
                              KSO2=1. Same spin for all particles
                              Particles # 1 to 1 may be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO    =      7205.178 kG*cm
                               beta    =    0.91720721
                               gamma   =   2.50997391
                               gamma*G =   4.5000001900


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        1  PARTICULES :
                               <SX> =     0.000000
                               <SY> =     0.000000
                               <SZ> =     1.000000
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                                                    IPASS= 5


     zgoubi.plt                                                                      
      already open...

     Status of MOD.MOD2 is 12.1 ; NDIM = 3 ; number of field data files used is   1.

     No  map  file  opened :  was  already  stored.
     Skip  reading  field  map  file :           b_table55.tab
     Restored mesh coordinates for field map #IMAP=   1,  name : b_table55.tab

     Min/max fields found in map (series) read   :  -2.934230E-01  /   3.119730E-01
       @  X,  Y, Z : -1.045E-02 -6.000E-02  6.500E-02              /  1.050E-02  5.500E-02  7.000E-02
     Given normalisation coeffs on field, x, y, z :   1.000000E+01   1.000000E+02   1.000000E+02   1.000000E+02
     this yields min/max normalised fields (kG) : -2.934230E+00                      3.119730E+00
       @  X (cm),  Y (cm), Z (cm) :  -1.04      -6.00       6.50   /   1.05       5.50       7.00    

     Length of element,  XL =  4.000000E+02 cm 
                                               from  XI =   1.985000E+02 cm 
                                               to    XF =   5.985000E+02 cm 

     Nbr of nodes in X = 801;  nbr of nodes in Y =   29
     X-size of mesh =  5.000000E-01 cm ; Y-size =  5.000000E-01 cm
     nber of mesh nodes in Z =   29 ; Step in Z =  0.500000E+00 cm

                     OPTION  DE  CALCUL  :  GRILLE  3-D  A  3*3*3  POINTS , INTERPOLATION  A  L'ORDRE  2

                    Integration step :  0.1000     cm

  A    1 11.0152    -0.146     0.005     0.000     0.000          598.500    -0.145    -0.000    -0.006    -0.000            1


                CONDITIONS  DE  MAXWELL  (     4001.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                    -1.7763E-14       2.9406E-18        1.8080E-16
                                      1.1751E-13       -5.7894E-14
                                     -1.8399E-14       -3.6149E-13
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  2 :  Element  is  mis-aligned  wrt.  the  optical  axis.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.0000000     RAD


 Cumulative length of optical axis =    4.00000000     m ;  Time  (for ref. rigidity & particle) =   1.454695E-08 s 

************************************************************************************************************************************

      5   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      6  Keyword, label(s) :  SPNPRT      PRINT                                                                        IPASS= 5



                         Momentum  group  #1 ; average  over 1 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.000000   0.000000   1.000000   1.000000        -0.000395   0.183280   0.983061   1.000000  45.500002  10.560903   0.000000


                Spin  components  of  each  of  the      1  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   |Si,Sf|   (Z,Sf_yz) (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_yz : projection of Sf on YZ plane)

 o  1  0.000000  0.000000  1.000000  1.000000    -0.000395  0.183280  0.983061  1.000000     25.3786   10.561   45.489   10.561    1



                Min/Max  components  of  each  of  the      1  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

 -9.4780E-04 -3.9472E-04  1.8328E-01  2.0832E-01  9.7806E-01  9.8306E-01  1.0000E+00  1.0000E+00  1.10152E+01  2.53786E+01     1   1

************************************************************************************************************************************
      7  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 5


                         >>>>  End  of  'REBELOTE'  procedure  <<<<

      There  has  been          5  passes  through  the  optical  structure 

                     Total of          5 particles have been launched

************************************************************************************************************************************
      8  Keyword, label(s) :  END                                                                                      IPASS= 6


************************************************************************************************************************************
 Pgm zgoubi : Execution ended normally, upon keyword END or FIN
   
 File in:   AGSWarmSnake_mapMinarized.res
 File out:  zgoubi.res

  Zgoubi, author's dvlpmnt version.
  Job  started  on  11-03-2021,  at  05:17:33 
  JOB  ENDED  ON    11-03-2021,  AT  05:17:50 

   CPU time, total :     17.315397999999998     

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
