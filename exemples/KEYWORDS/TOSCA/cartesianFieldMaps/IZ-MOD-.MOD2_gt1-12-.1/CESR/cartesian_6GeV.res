In our lattice it is modeled as a simple Bmad bend with a b1 component.
! The magnet is physically 2.35 m long. I provided a field map 3.5 m long to Bmad
! Expected K=-0.438 m^-2?   Bend is 7.4799110E-02 rad. Arc length 2.3475 m
! The gap at the beam is 4.4 cm.  Rho is 31 m.
!
'OBJET'                                                                                                      1
20.013845711889122E3                  ! p=6GeV/c
5
.01 .001 .01 .001 .0 .00001
-1.6736061 37.399555 0. 0. 0. 1. 'o'
1
 
'TOSCA'                                                                                                      2
0  2
-1.16325667E-03   1. 1. 1.
HEADER_8 CESR CFM. 6GeV/c
261 51 9  12.1
6GeV_Cartesian.table
0 0 0 0
2
2.
2  0.  .0  0.  0.
 
'FIT'                                                                                                        3
2
1 30 0 .1 [-4.,4]
2 10 0 .9
2  1e-20 9999
3.1 1 2 #End 0. 1. 0
3.2 1 3 #End 0. 1. 0
 
'FAISCEAU'                                                                                                   4
'MATRIX'                                                                                                     5
1 0
 
'END'                                                                                                       10

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =      20013.846 kG*cm

                                         CALCUL  DES  TRAJECTOIRES

                              OBJET  (5)  FORME  DE     11 POINTS 



                                Y (cm)         T (mrd)       Z (cm)        P (mrd)       S (cm)        dp/p 
               Sampling :          0.10E-01      0.10E-02      0.10E-01      0.10E-02       0.0          0.1000E-04
  Reference trajectry #      1 :   -1.7           37.           0.0           0.0           0.0           1.000    

************************************************************************************************************************************
      2  Keyword, label(s) :  TOSCA                                                                                    IPASS= 1


                OPEN FILE zgoubi.plt                                                                      
                FOR PRINTING TRAJECTORIES


     Status of MOD.MOD2 is 12.1 ; NDIM = 3 ; number of field data files used is   1.

           New field map(s) opened, cartesian mesh (MOD.le.19) ; 
           name(s) of map data file(s) : 

          '6GeV_Cartesian.table'

   ----
   Map file number    1 ( of 1 ) (unit # 25 ) :
     6GeV_Cartesian.table field map,
     FORMAT type : regular.

 HEADER  (8 lines) : 
      261 9 51 2                                                                                                             
      1 X [CM]                                                                                                               
      2 Y [CM]                                                                                                               
      3 Z [CM]                                                                                                               
      4 BX [1]                                                                                                               
      5 BY [1]                                                                                                               
      6 BZ [1]                                                                                                               
      0                                                                                                                      
R_min (cm), DR (cm), DTTA (deg), DZ (cm) :    2.61000000E+02   9.00000000E+00   5.10000000E+01   2.00000000E+00

  Sbr fmapw/fmapr3 : completed job of reading map.
     Field map scaled by factor  a(1) =   0.000000E+00

     Min/max fields found in map (series) read   :   8.952200E+06  /  -2.744558E+06
       @  X,  Y, Z :  -21.0      -4.75       1.00                  /   2.00      -5.00      -1.00    
     Given normalisation coeffs on field, x, y, z :  -1.163257E-03   1.000000E+00   1.000000E+00   1.000000E+00
     this yields min/max normalised fields (kG) : -1.041371E+04                      3.192626E+03
       @  X (cm),  Y (cm), Z (cm) :  -21.0      -4.75       1.00   /   2.00      -5.00      -1.00    

     Length of element,  XL =  2.600000E+02 cm 
                                               from  XI =  -1.300000E+02 cm 
                                               to    XF =   1.300000E+02 cm 

     Nbr of nodes in X = 261;  nbr of nodes in Y =   51
     X-size of mesh =  1.000000E+00 cm ; Y-size =  2.500000E-01 cm
     nber of mesh nodes in Z =    9 ; Step in Z =  0.250000E+00 cm

                     OPTION  DE  CALCUL  :  GRILLE  3-D  A  3*3*3  POINTS , INTERPOLATION  A  L'ORDRE  2

                    Integration step :   2.000     cm

  A    1  1.0000    -1.674    37.400     0.000     0.000          130.000    -1.673    -0.037     0.000     0.000            1
  A    1  1.0000    -1.664    37.400     0.000     0.000          130.000    -1.643    -0.037     0.000     0.000            2
  A    1  1.0000    -1.684    37.400     0.000     0.000          130.000    -1.703    -0.038     0.000     0.000            3
  A    1  1.0000    -1.674    37.401     0.000     0.000          130.000    -1.673    -0.037     0.000     0.000            4
  A    1  1.0000    -1.674    37.399     0.000     0.000          130.000    -1.674    -0.037     0.000     0.000            5
  A    1  1.0000    -1.674    37.400     0.010     0.000          130.000    -1.673    -0.037    -0.002    -0.000            6
  A    1  1.0000    -1.674    37.400    -0.010     0.000          130.000    -1.673    -0.037     0.002     0.000            7
  A    1  1.0000    -1.674    37.400     0.000     0.001          130.000    -1.673    -0.037     0.000     0.000            8
  A    1  1.0000    -1.674    37.400     0.000    -0.001          130.000    -1.673    -0.037    -0.000     0.000            9
  A    1  1.0000    -1.674    37.400     0.000     0.000          130.000    -1.673    -0.037     0.000     0.000           10
  A    1  1.0000    -1.674    37.400     0.000     0.000          130.000    -1.674    -0.037     0.000     0.000           11


                CONDITIONS  DE  MAXWELL  (     1441.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                    -2.6171E-13       2.8533E-13       -7.3294E-14
                                      1.1960E-11       -1.4216E-11
                                      4.3082E-12       -3.8747E-13
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  2 :  Element  is  mis-aligned  wrt.  the  optical  axis.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.0000000     RAD


 Cumulative length of optical axis =    2.60000000     m ;  Time  (for ref. rigidity & particle) =   8.778068E-09 s 

************************************************************************************************************************************
      3  Keyword, label(s) :  FIT                                                                                      IPASS= 1

     Pgm main. FITING=.T., FIT procedure launched.

           variable #            1       IR =            1 ,   ok.
           variable #            1       IP =           30 ,   ok.
           variable #            2       IR =            2 ,   ok.
           variable #            2       IP =           10 ,   ok.
           constraint #            1       IR =            2 ,   ok.
           constraint #            1       I  =            1 ,   ok.
           constraint #            2       IR =            2 ,   ok.
           constraint #            2       I  =            1 ,   ok.

                    FIT  variables  and  constraints  in  good  order,  FIT  will proceed. 

 STATUS OF VARIABLES  (Iteration #     0 /   9999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   1   1    30   -4.00       -1.67      -1.6736061       4.00       0.00      OBJET      -                    -                   
   2   2    10  -2.210E-03  -1.163E-03 -1.16325666E-03 -1.163E-04   0.00      TOSCA      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-20)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     2    0.000000E+00    1.000E+00    1.851093E-04    9.88E-01 TOSCA      -                    -                    0
  3   1   3     2    0.000000E+00    1.000E+00    2.012095E-05    1.17E-02 TOSCA      -                    -                    0
 Fit reached penalty value   3.4670E-08

************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =      20013.846 kG*cm

                                         CALCUL  DES  TRAJECTOIRES

                              OBJET  (5)  FORME  DE     11 POINTS 



                                Y (cm)         T (mrd)       Z (cm)        P (mrd)       S (cm)        dp/p 
               Sampling :          0.10E-01      0.10E-02      0.10E-01      0.10E-02       0.0          0.1000E-04
  Reference trajectry #      1 :   -1.7           37.           0.0           0.0           0.0           1.000    

************************************************************************************************************************************
      2  Keyword, label(s) :  TOSCA                                                                                    IPASS= 1


     zgoubi.plt                                                                      
      already open...

     Status of MOD.MOD2 is 12.1 ; NDIM = 3 ; number of field data files used is   1.

     No  map  file  opened :  was  already  stored.
     Skip  reading  field  map  file :           6GeV_Cartesian.table
     Restored mesh coordinates for field map #IMAP=   1,  name : 6GeV_Cartesian.table
     Field map scaled by factor  a(1) =   0.000000E+00

     Min/max fields found in map (series) read   :   8.952200E+06  /  -2.744558E+06
       @  X,  Y, Z :  -21.0      -4.75       1.00                  /   2.00      -5.00      -1.00    
     Given normalisation coeffs on field, x, y, z :  -1.163257E-03   1.000000E+00   1.000000E+00   1.000000E+00
     this yields min/max normalised fields (kG) : -1.041371E+04                      3.192626E+03
       @  X (cm),  Y (cm), Z (cm) :  -21.0      -4.75       1.00   /   2.00      -5.00      -1.00    

     Length of element,  XL =  2.600000E+02 cm 
                                               from  XI =  -1.300000E+02 cm 
                                               to    XF =   1.300000E+02 cm 

     Nbr of nodes in X = 261;  nbr of nodes in Y =   51
     X-size of mesh =  1.000000E+00 cm ; Y-size =  2.500000E-01 cm
     nber of mesh nodes in Z =    9 ; Step in Z =  0.250000E+00 cm

                     OPTION  DE  CALCUL  :  GRILLE  3-D  A  3*3*3  POINTS , INTERPOLATION  A  L'ORDRE  2

                    Integration step :   2.000     cm

  A    1  1.0000    -1.674    37.400     0.000     0.000          130.000    -1.673    -0.037     0.000     0.000            1
  A    1  1.0000    -1.664    37.400     0.000     0.000          130.000    -1.643    -0.037     0.000     0.000            2
  A    1  1.0000    -1.684    37.400     0.000     0.000          130.000    -1.703    -0.038     0.000     0.000            3
  A    1  1.0000    -1.674    37.401     0.000     0.000          130.000    -1.673    -0.037     0.000     0.000            4
  A    1  1.0000    -1.674    37.399     0.000     0.000          130.000    -1.674    -0.037     0.000     0.000            5
  A    1  1.0000    -1.674    37.400     0.010     0.000          130.000    -1.673    -0.037    -0.002    -0.000            6
  A    1  1.0000    -1.674    37.400    -0.010     0.000          130.000    -1.673    -0.037     0.002     0.000            7
  A    1  1.0000    -1.674    37.400     0.000     0.001          130.000    -1.673    -0.037     0.000     0.000            8
  A    1  1.0000    -1.674    37.400     0.000    -0.001          130.000    -1.673    -0.037    -0.000     0.000            9
  A    1  1.0000    -1.674    37.400     0.000     0.000          130.000    -1.673    -0.037     0.000     0.000           10
  A    1  1.0000    -1.674    37.400     0.000     0.000          130.000    -1.674    -0.037     0.000     0.000           11


                CONDITIONS  DE  MAXWELL  (     1441.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                    -2.6171E-13       2.8533E-13       -7.3293E-14
                                      1.1960E-11       -1.4216E-11
                                      4.3082E-12       -3.8747E-13
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  2 :  Element  is  mis-aligned  wrt.  the  optical  axis.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.0000000     RAD


 Cumulative length of optical axis =    2.60000000     m ;  Time  (for ref. rigidity & particle) =   8.778068E-09 s 

************************************************************************************************************************************

      3   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                 11 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

O  1   1.0000    -1.674    37.400     0.000     0.000      0.0000    0.0000   -1.673  -37.400    0.000    0.000   2.600713E+02     1
A  1   1.0000    -1.664    37.400     0.000     0.000      0.0000    0.0000   -1.643  -37.216    0.000    0.000   2.600710E+02     2
B  1   1.0000    -1.684    37.400     0.000     0.000      0.0000    0.0000   -1.703  -37.583    0.000    0.000   2.600717E+02     3
C  1   1.0000    -1.674    37.401     0.000     0.000      0.0000    0.0000   -1.673  -37.396    0.000    0.000   2.600713E+02     4
D  1   1.0000    -1.674    37.399     0.000     0.000      0.0000    0.0000   -1.674  -37.403    0.000    0.000   2.600714E+02     5
E  1   1.0000    -1.674    37.400     0.010     0.000      0.0000    0.0000   -1.673  -37.400   -0.002   -0.071   2.600713E+02     6
F  1   1.0000    -1.674    37.400    -0.010     0.000      0.0000    0.0000   -1.673  -37.400    0.002    0.071   2.600713E+02     7
G  1   1.0000    -1.674    37.400     0.000     0.001      0.0000    0.0000   -1.673  -37.400    0.000    0.000   2.600713E+02     8
H  1   1.0000    -1.674    37.400     0.000    -0.001      0.0000    0.0000   -1.673  -37.400   -0.000    0.001   2.600713E+02     9
I  1   1.0000    -1.674    37.400     0.000     0.000      0.0000    0.0000   -1.673  -37.398    0.000    0.000   2.600713E+02    10
J  1   1.0000    -1.674    37.400     0.000     0.000      0.0000   -0.0000   -1.674  -37.401    0.000    0.000   2.600713E+02    11


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   8.7801E-11  -3.5750E+02   5.8340E+02  -1.673418E-02  -3.739956E-02       11        9   0.8182      (Y,T)         1
   5.7120E-11  -1.3838E+01   3.8140E+00   1.315446E-06   4.222948E-07       11        7   0.6364      (Z,P)         1
   3.9082E-10   6.2424E-03   1.9006E-07   8.675046E-03   6.000000E+03       11        7   0.6364      (t,K)         1

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   1.276901E-04
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   7.824623E-05

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   8.327379E-06
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   3.029216E-05

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   4.862519E-09
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   2.558409E-02


  Beam  sigma  matrix  and  determinants : 

   1.630475E-08   9.991227E-09   3.024369E-14   1.091810E-12
   9.991227E-09   6.122472E-09   2.300307E-14   6.854845E-13
   3.024369E-14   2.300307E-14   6.934524E-11   2.515982E-10
   1.091810E-12   6.854845E-13   2.515982E-10   9.176150E-10

      sqrt(det_Y), sqrt(det_Z) :     2.794785E-11    1.818194E-11    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      5  Keyword, label(s) :  MATRIX                                                                                   IPASS= 1


  Reference, before change of frame (part #     1)  : 
   0.00000000E+00  -1.67342101E+00  -3.73995751E+01   1.31591866E-04   4.22309231E-04   2.60071348E+02   8.67504638E-03

           Frame for MATRIX calculation moved by :
            XC =    0.000 cm , YC =   -1.673 cm ,   A = -2.14284 deg  ( =-0.037400 rad )


  Reference, after change of frame (part #     1)  : 
   0.00000000E+00   0.00000000E+00   0.00000000E+00   1.31591866E-04   4.22309231E-04   2.60071348E+02   8.67504638E-03

  Reference particle (#     1), path length :   260.07135     cm  relative momentum :    1.00000    


                  TRANSFER  MATRIX  ORDRE  1  (MKSA units)

           2.99215         4.41314       -1.115395E-04   -2.559419E-05     0.00000        0.125180    
           1.83475         3.04582       -8.113303E-05   -1.142710E-05     0.00000        0.115236    
         -1.557051E-06   -6.629112E-04   -0.194824         1.35421         0.00000        4.579144E-07
          1.740346E-04    2.127880E-04   -0.710411       -0.194822         0.00000        3.712942E-06
          7.777313E-02    0.127277       -3.762176E-06    2.842171E-08     1.00000        2.549797E-03
           0.00000         0.00000         0.00000         0.00000         0.00000         1.00000    

          DetY-1 =       0.0165719392,    DetZ-1 =      -0.0000001668

          R12=0 at   -1.449     m,        R34=0 at    6.951     m

      First order symplectic conditions (expected values = 0) :
         1.6572E-02   -1.6762E-07   -2.2261E-04   -1.3148E-04    3.1033E-06   -4.9407E-04

************************************************************************************************************************************
      6  Keyword, label(s) :  END                                                                                      IPASS= 1


                            11 particles have been launched
                     Made  it  to  the  end :     11

************************************************************************************************************************************
 Pgm zgoubi : Execution ended normally, upon keyword END or FIN
   
 File in:   cartesian_6GeV.dat
 File out:  zgoubi.res

  Zgoubi, author's dvlpmnt version.
  Job  started  on  17-04-2020,  at  06:22:10 
  JOB  ENDED  ON    17-04-2020,  AT  06:22:14 

   CPU time, total :     4.6538129999999995     

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
