ERIT
 'OBJET'                                                                                                      1
4.806447298126e+02
2
2 1
235. 0. 0. 0. 0. 1. 'o'
235. 0. 4. 0. 0. 1. 'o'
1 1
 'OPTIONS'                                                                                                    2
1 1
CONSTY ON
 
 'PARTICUL'                                                                                                   3
PROTON
 
 'TOSCA'                                                                                                      4
0 20
-1.0000e-03 1.0000e+00 1.0000e+00 1.0000e+00
HEADER_8
181 51 93 22.1
fdf8_num2-3_revtheta.table
0 0.0000e+00 0.0000e+00 0.0000e+00
2
.4
2
0.0000e+00 0.0000e+00 0.0000e+00 0.0000e+00
 
 'FAISCEAU'                                                                                                   5
 
 'OBJET'                                                                                                      6
4.806447298126e+02
2
2 1
235. 0. 0. 0. 0. 1. 'o'
235. 0. 4. 0. 0. 1. 'o'
1 1
 'OPTIONS'                                                                                                    7
1 1
CONSTY ON
 
 'PARTICUL'                                                                                                   8
PROTON
 
 'TOSCA'                                                                                                      9
0 20
-1.0000e-03 1.0000e+00 1.0000e+00 1.0000e+00
HEADER_8
181 51 93 22.3 0.33333333 0.33333333 0.33333333
fdf8_num2-3_revtheta.table
fdf8_num2-3_revtheta.table
fdf8_num2-3_revtheta.table
0 0.0000e+00 0.0000e+00 0.0000e+00 0.0000e+00 0.0000e+00 0.0000e+00 0.0000e+00 0.0000e+00 0.0000e+00
2
.4
2
0.0000e+00 0.0000e+00 0.0000e+00 0.0000e+00
 
 'FAISCEAU'                                                                                                  10
 
 'OBJET'                                                                                                     11
4.806447298126e+02
2
2 1
235. 0. 0. 0. 0. 1. 'o'
235. 0. 4. 0. 0. 1. 'o'
1 1
 'OPTIONS'                                                                                                   12
1 1
CONSTY ON
 
 'PARTICUL'                                                                                                  13
PROTON
 
 'TOSCA'                                                                                                     14
0 20
-1.0000e-03 1.0000e+00 1.0000e+00 1.0000e+00
HEADER_8
181 51 1 22.1
fdf8_num2-3_revtheta_2D.table
0 0.0000e+00 0.0000e+00 0.0000e+00 0.0000e+00 0.0000e+00 0.0000e+00 0.0000e+00 0.0000e+00 0.0000e+00
2
.4
2
0.0000e+00 0.0000e+00 0.0000e+00 0.0000e+00
 
 'FAISCEAU'                                                                                                  15
 
 'END'                                                                                                       16

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 

                          MAGNETIC  RIGIDITY =        480.645 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       2 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  OPTIONS                                               

         A list of 1 option(s) is expected.  List and actions taken are as follows :


    Option #  1 : CONSTY ON
     - rays will be forced to constant Y and Z in pgm integr.
     - mid-plane symmetry test in pgm transf (i.e., 'dejaca' procedure) is inhibited.
     - coordinates and field may be checked using IL = 1, 2 OR 7 

************************************************************************************************************************************
      3  Keyword, label(s) :  PARTICUL                                              


     Particle  properties :
     PROTON
                     Mass          =    938.272        MeV/c2
                     Charge        =   1.602176E-19    C     
                     G  factor     =    1.79285              
                     COM life-time =   1.000000E+99    s     

              Reference  data :
                    mag. rigidity (kG.cm)   :   480.64473      =p/q, such that dev.=B*L/rigidity
                    mass (MeV/c2)           :   938.27208    
                    momentum (MeV/c)        :   144.09366    
                    energy, total (MeV)     :   949.27208    
                    energy, kinetic (MeV)   :   10.999999    
                    beta = v/c              :  0.1517938512    
                    gamma                   :   1.011723677    
                    beta*gamma              :  0.1535734334    
                    G*gamma                 :   1.813866114    
                    electric rigidity (MeV) :   21.87253235    =T[eV]*(gamma+1)/gamma, such that dev.=E*L/rigidity
  
 I, AMQ(1,I), AMQ(2,I)/QE, P/Pref, v/c, time, s :
  
     1   9.38272081E+02  1.00000000E+00  1.00000000E+00  1.51793851E-01  0.00000000E+00  0.00000000E+00
     2   9.38272081E+02  1.00000000E+00  1.00000000E+00  1.51793851E-01  0.00000000E+00  0.00000000E+00

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


                OPEN FILE zgoubi.plt                                                                      
                FOR PRINTING TRAJECTORIES


     NDIM =   3 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00

          New field map(s) now used, polar mesh (MOD .ge. 20) ;  name(s) of map data file(s) are : 
          'fdf8_num2-3_revtheta.table'

   ----
   Map file number    1 ( of 1) :

     fdf8_num2-3_revtheta.table map,  FORMAT type : regular.  Field multiplication factor :  1.00000000E+00

 HEADER  (8 lines) : 
       210.  1.  0.25  0.25   ! READ(HDRM,*,ERR=41,END=41) R0/cm, DR/cm, DTTA/deg, DZ/cm ! 51      47       181              
      1 X [LENGU]                                                                                                            
      2 Y [LENGU]                                                                                                            
      3 Z [LENGU]                                                                                                            
      4 BX [FLUXU]                                                                                                           
      5 BY [FLUXU]                                                                                                           
      6 BZ [FLUXU]                                                                                                           
      0                                                                                                                      
 
R_min (cm), DR (cm), DTTA (deg), DZ (cm) :   2.100000E+02  1.000000E+00  2.500000E-01  2.500000E-01



     Field map limits, angle :  min, max, max-min (rad) :  -0.39269908E+00   0.39269908E+00   0.78539816E+00
     Field map limits, radius :  min, max, max-min (cm) :   0.21000000E+03   0.26000000E+03   0.50000000E+02
      Min / max  fields  drawn  from  map  data :   2.13168E+07                 /  -2.20074E+07
       @  X-node, Y-node, z-node :               0.436      211.      8.75      /  0.436      223.      9.25    
     Normalisation  coeff.  BNORM   : -1.000000E-03
     Field  min/max  normalised  :               -21317.        /    22007.    
     Nbre  of  nodes  in  X/Y/Z :  181/  51/  93
     Node  distance  in   X/Y/Z :   4.363323E-03/   1.00000    /  0.250000    

                     Option  for  interpolation :  3-D  grid    3*3*3  points,   interpolation  at  ordre  2

                    Integration step :  0.4000     cm   (i.e.,   1.7021E-03 rad  at mean radius RM =    235.0    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000   235.000     0.000     0.000     0.000            0.393   235.000     0.001     0.000     0.000            1
  A    1  1.0000   235.000     0.000     4.000     0.000            0.393   235.000     0.001     4.000     0.000            2


                CONDITIONS  DE  MAXWELL  (      924.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                    -5.4447E-10       2.1566E-11       -3.2043E-10
                                      2.2747E-11        1.8782E-10
                                     -1.1784E-10       -7.5479E-11
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  2 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000   235.000     0.000     0.000     0.000      0.0000    0.0000  235.000    0.716    0.000    0.000   1.845686E+02     1
               Time of flight (mus) :  4.05585912E-02 mass (MeV/c2) :   938.272    
o  1   1.0000   235.000     0.000     4.000     0.000      0.0000    0.0000  235.000    0.716    4.000    0.000   1.845686E+02     2
               Time of flight (mus) :  4.05585912E-02 mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   3.4618E-23  -1.8813E+05   1.5836E+02   2.350001E+00   7.155756E-04        2        2    1.000      (Y,T)         1
   4.3598E-17  -7.7169E+07   2.8823E+13   2.000000E-02   5.354660E-08        2        2    1.000      (Z,P)         1
   0.0000E+00   0.0000E+00   1.0000E+00   4.055859E-02   1.100000E+01        2        0    0.000      (t,K)         1

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   4.177259E-11
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   4.962572E-08

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   2.000000E-02
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   5.354660E-08

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.744949E-21   2.072994E-18   8.354517E-13   2.236780E-18
   2.072994E-18   2.462712E-15   9.925143E-10   2.657288E-15
   8.354517E-13   9.925143E-10   4.000000E-04   1.070932E-09
   2.236780E-18   2.657288E-15   1.070932E-09   2.867239E-15

      sqrt(det_Y), sqrt(det_Z) :     1.101912E-23    1.387779E-17    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  OBJET                                                 

                          MAGNETIC  RIGIDITY =        480.645 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       2 POINTS 



************************************************************************************************************************************
      7  Keyword, label(s) :  OPTIONS                                               

         A list of 1 option(s) is expected.  List and actions taken are as follows :


    Option #  1 : CONSTY ON
     - rays will be forced to constant Y and Z in pgm integr.
     - mid-plane symmetry test in pgm transf (i.e., 'dejaca' procedure) is inhibited.
     - coordinates and field may be checked using IL = 1, 2 OR 7 

************************************************************************************************************************************
      8  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      9  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   3 ;  Number of data file sets used is   3 ;  Stored in field array # IMAP =    2 ;  
     Value of MOD.MOD2 is 22.3

           3-D map. MOD=22 or 23.   Will sum up 3  field maps, with field coefficient values : 
                 3.333333E-01  3.333333E-01  3.333333E-01

          New field map(s) now used, polar mesh (MOD .ge. 20) ;  name(s) of map data file(s) are : 
          'fdf8_num2-3_revtheta.table'
          'fdf8_num2-3_revtheta.table'
          'fdf8_num2-3_revtheta.table'

   ----
   Map file number    1 ( of 3) :

     fdf8_num2-3_revtheta.table map,  FORMAT type : regular.  Field multiplication factor :  3.33333330E-01

 HEADER  (8 lines) : 
       210.  1.  0.25  0.25   ! READ(HDRM,*,ERR=41,END=41) R0/cm, DR/cm, DTTA/deg, DZ/cm ! 51      47       181              
      1 X [LENGU]                                                                                                            
      2 Y [LENGU]                                                                                                            
      3 Z [LENGU]                                                                                                            
      4 BX [FLUXU]                                                                                                           
      5 BY [FLUXU]                                                                                                           
      6 BZ [FLUXU]                                                                                                           
      0                                                                                                                      
 
R_min (cm), DR (cm), DTTA (deg), DZ (cm) :   2.100000E+02  1.000000E+00  2.500000E-01  2.500000E-01


   ----
   Map file number    2 ( of 3) :

     fdf8_num2-3_revtheta.table map,  FORMAT type : regular.  Field multiplication factor :  3.33333330E-01

 HEADER  (8 lines) : 
       210.  1.  0.25  0.25   ! READ(HDRM,*,ERR=41,END=41) R0/cm, DR/cm, DTTA/deg, DZ/cm ! 51      47       181              
      1 X [LENGU]                                                                                                            
      2 Y [LENGU]                                                                                                            
      3 Z [LENGU]                                                                                                            
      4 BX [FLUXU]                                                                                                           
      5 BY [FLUXU]                                                                                                           
      6 BZ [FLUXU]                                                                                                           
      0                                                                                                                      
 
R_min (cm), DR (cm), DTTA (deg), DZ (cm) :   2.100000E+02  1.000000E+00  2.500000E-01  2.500000E-01


   ----
   Map file number    3 ( of 3) :

     fdf8_num2-3_revtheta.table map,  FORMAT type : regular.  Field multiplication factor :  3.33333330E-01

 HEADER  (8 lines) : 
       210.  1.  0.25  0.25   ! READ(HDRM,*,ERR=41,END=41) R0/cm, DR/cm, DTTA/deg, DZ/cm ! 51      47       181              
      1 X [LENGU]                                                                                                            
      2 Y [LENGU]                                                                                                            
      3 Z [LENGU]                                                                                                            
      4 BX [FLUXU]                                                                                                           
      5 BY [FLUXU]                                                                                                           
      6 BZ [FLUXU]                                                                                                           
      0                                                                                                                      
 
R_min (cm), DR (cm), DTTA (deg), DZ (cm) :   2.100000E+02  1.000000E+00  2.500000E-01  2.500000E-01



     Field map limits, angle :  min, max, max-min (rad) :  -0.39269908E+00   0.39269908E+00   0.78539816E+00
     Field map limits, radius :  min, max, max-min (cm) :   0.21000000E+03   0.26000000E+03   0.50000000E+02
      Min / max  fields  drawn  from  map  data :   2.13168E+07                 /  -2.20074E+07
       @  X-node, Y-node, z-node :               0.436      211.      8.75      /  0.436      223.      9.25    
     Normalisation  coeff.  BNORM   : -1.000000E-03
     Field  min/max  normalised  :               -21317.        /    22007.    
     Nbre  of  nodes  in  X/Y/Z :  181/  51/  93
     Node  distance  in   X/Y/Z :   4.363323E-03/   1.00000    /  0.250000    

                     Option  for  interpolation :  3-D  grid    3*3*3  points,   interpolation  at  ordre  2

                    Integration step :  0.4000     cm   (i.e.,   1.7021E-03 rad  at mean radius RM =    235.0    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000   235.000     0.000     0.000     0.000            0.393   235.000     0.001     0.000     0.000            1
  A    1  1.0000   235.000     0.000     4.000     0.000            0.393   235.000     0.001     4.000     0.000            2


                CONDITIONS  DE  MAXWELL  (      924.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                    -5.4447E-10       2.1566E-11       -3.2043E-10
                                      2.2747E-11        1.8782E-10
                                     -1.1784E-10       -7.5479E-11
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  2 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000   235.000     0.000     0.000     0.000      0.0000    0.0000  235.000    0.716    0.000    0.000   1.845686E+02     1
               Time of flight (mus) :  4.05585912E-02 mass (MeV/c2) :   938.272    
o  1   1.0000   235.000     0.000     4.000     0.000      0.0000    0.0000  235.000    0.716    4.000    0.000   1.845686E+02     2
               Time of flight (mus) :  4.05585912E-02 mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   3.4617E-23  -1.8813E+05   1.5836E+02   2.350001E+00   7.155756E-04        2        2    1.000      (Y,T)         1
          NaN   0.0000E+00   1.0000E+00   2.000000E-02   5.354660E-08        2        0    0.000      (Z,P)         1
   0.0000E+00   0.0000E+00   1.0000E+00   4.055859E-02   1.100000E+01        2        0    0.000      (t,K)         1

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   4.177259E-11
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   4.962572E-08

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =            NaN
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =            NaN

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.744949E-21   2.072994E-18   8.354517E-13   2.236780E-18
   2.072994E-18   2.462712E-15   9.925143E-10   2.657288E-15
   8.354517E-13   9.925143E-10   4.000000E-04   1.070932E-09
   2.236780E-18   2.657288E-15   1.070932E-09   2.867239E-15

      sqrt(det_Y), sqrt(det_Z) :     1.101909E-23             NaN    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  OBJET                                                 

                          MAGNETIC  RIGIDITY =        480.645 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       2 POINTS 



************************************************************************************************************************************
     12  Keyword, label(s) :  OPTIONS                                               

         A list of 1 option(s) is expected.  List and actions taken are as follows :


    Option #  1 : CONSTY ON
     - rays will be forced to constant Y and Z in pgm integr.
     - mid-plane symmetry test in pgm transf (i.e., 'dejaca' procedure) is inhibited.
     - coordinates and field may be checked using IL = 1, 2 OR 7 

************************************************************************************************************************************
     13  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
     14  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    3 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00

          New field map(s) now used, polar mesh (MOD .ge. 20) ;  name(s) of map data file(s) are : 
          'fdf8_num2-3_revtheta_2D.table'

   ----
   Map file number    1 ( of 1) :

     fdf8_num2-3_revtheta_2D.table map,  FORMAT type : regular.  Field multiplication factor :  1.00000000E+00

 HEADER  (8 lines) : 
        210.  1.  0.25  0.25   ! READ(HDRM,*,ERR=41,END=41) R0/cm, DR/cm, DTTA/deg, DZ/cm ! 51      47       181             
       1 X [LENGU]                                                                                                           
       2 Y [LENGU]                                                                                                           
       3 Z [LENGU]                                                                                                           
       4 BX [FLUXU]                                                                                                          
       5 BY [FLUXU]                                                                                                          
       6 BZ [FLUXU]                                                                                                          
       0                                                                                                                     
 
R_min (cm), DR (cm), DTTA (deg), DZ (cm) :   2.100000E+02  1.000000E+00  2.500000E-01  2.500000E-01



     Field map limits, angle :  min, max, max-min (rad) :  -0.39269908E+00   0.39269908E+00   0.78539816E+00
     Field map limits, radius :  min, max, max-min (cm) :   0.21000000E+03   0.26000000E+03   0.50000000E+02
      Min / max  fields  drawn  from  map  data :   9.84483E+06                 /  -8.92181E+06
       @  X-node, Y-node, z-node :               0.227      254.      0.00      /  0.393      254.      0.00    
     Normalisation  coeff.  BNORM   : -1.000000E-03
     Field  min/max  normalised  :               -9844.8        /    8921.8    
     Nbre  of  nodes  in  X/Y/Z :  181/  51/   1
     Node  distance  in   X/Y/Z :   4.363323E-03/   1.00000    /  -11.2500    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :  0.4000     cm   (i.e.,   1.7021E-03 rad  at mean radius RM =    235.0    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000   235.000     0.000     0.000     0.000            0.393   235.000     0.001     0.000     0.000            1
  A    1  1.0000   235.000     0.000     4.000     0.000            0.393   235.000     0.001     4.000    -0.000            2


                CONDITIONS  DE  MAXWELL  (      924.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
     15  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #     14)
                                                  2 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000   235.000     0.000     0.000     0.000      0.0000    0.0000  235.000    0.716    0.000    0.000   1.845686E+02     1
               Time of flight (mus) :  4.05585912E-02 mass (MeV/c2) :   938.272    
o  1   1.0000   235.000     0.000     4.000     0.000      0.0000    0.0000  235.000    0.715    4.000   -0.000   1.845686E+02     2
               Time of flight (mus) :  4.05585912E-02 mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   2.350001E+00   7.154676E-04        2        0    0.000      (Y,T)         1
   0.0000E+00   0.0000E+00   1.0000E+00   2.000000E-02  -5.912713E-08        2        0    0.000      (Z,P)         1
   0.0000E+00   0.0000E+00   1.0000E+00   4.055859E-02   1.100000E+01        2        0    0.000      (t,K)         1

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.419305E-21   2.870414E-18  -9.837287E-13   2.908252E-18
   2.870414E-18   3.405638E-15  -1.167157E-09   3.450532E-15
  -9.837287E-13  -1.167157E-09   4.000000E-04  -1.182543E-09
   2.908252E-18   3.450532E-15  -1.182543E-09   3.496017E-15

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     16  Keyword, label(s) :  END                                                   


                             2 particles have been launched
                     Made  it  to  the  end :      2

************************************************************************************************************************************
 Pgm zgoubi : Execution ended normally, upon keyword END or FIN
   
            ZGOUBI RUN COMPLETED. 

  Zgoubi, author's dvlpmnt version.
  Job  started  on  13-09-2018,  at  16:24:21 
  JOB  ENDED  ON    13-09-2018,  AT  16:24:29 

   CPU time, total :     8.5964499999999990     
