PSI cYCLOTRON.
'OBJET'                                                  ! Definition of initial particle coordinates.       1
1249.382414                                                                                ! Rigidity.
2
7 1             ! A set of 7 particles covering the periodic orbit range of interest R=267 to 351 cm).
2.67042304E+02  -1.50516664E+01   0.00000000E+00   0.00000000E+00   0.00  1.40000001E+00 'o'
2.81258209E+02  -2.66331145E+01   0.00000000E+00   0.00000000E+00   0.00  1.50000001E+00 'o'
2.94829572E+02  -3.87557743E+01   0.00000000E+00   0.00000000E+00   0.00  1.60000001E+00 'o'
3.08680463E+02  -5.17104469E+01   0.00000000E+00   0.00000000E+00   0.00  1.70778172E+00 'R'
3.19924957E+02  -6.27126203E+01   0.00000000E+00   0.00000000E+00   0.00  1.80000001E+00 'o'
3.31557533E+02  -7.45532152E+01   0.00000000E+00   0.00000000E+00   0.00  1.90000001E+00 'o'
3.53201171E+02  -9.77647037E+01   0.00000000E+00   0.00000000E+00   0.00  2.10037893E+00 'o'
1 1 1 1 1 1 1                           ! 7 times 1, for 7 particles (-1 instead, to inhibit tacking).
'PARTICUL'   ! Type of particle. The only interest here is its allowing computation of time of flight,       2
PROTON                               ! otherwsie, zgoubi does not need it: it works with the rigidity.
'CYCLOTRON'                        ! Analytical modeling of the field in a separated sector cyclotron.       3
2
1   45.0  276.  1.0             ! N, AT, RM (reference radius), type of sector (radial, spiral, both).
0. 0. 0.992122800 51.4311902 0. 0. -4.48715507E-01 2.09658166E-03 -4.52609250E-06 3.95913656E-09 -5.68972605E-14 8.48686076E-17 -2.51326976E-19 4.87639705E-22 -1.54248545E-25 1.75499497E-27 -3.23761721E-29 2.75094168E-32                                      ! ATTENTION: the previous 3 three lines must actually make a single                              ! one - no carriage return. And these 2 lines of comment must be removed.
18.3000E+00  1.  28. -2.0                                                ! lambda=gap, gap's k  g1 g2.
8 1.1024358 3.1291507 -3.14287154 3.0858059 -1.43545 0.24047436 0. 0. 0.  ! NBCOEF, COEFS_C0-7, NORME.
11.0  3.5 35.E-3  0.E-4 3.E-8 0. 0. 0.      ! OMEGA,XI0exit,XI1exit,XI2exit,XI3exit,aexit,bexit,cexit.
18.3000E+00  1.  28. -2.0                                                ! lambda=gap, gap's k  g1 g2.
8 0.70490173 4.1601305 -4.3309575 3.540416 -1.3472703 0.18261076 0. 0. 0. ! NBCOEF, COEFS_C0-5, SHIFT.
-8.5  2.  12.E-3  75.E-6  0.E-6  0.  0.  0.                                 ! OMEGA,THETA,R1,U1,U2,R2.
0. -1                                                                           ! Lateral EFB, unused.
0  0.  0.  0.  0.  0.  0.  0.                                                ! NBCOEF, C0...C5, shift.
0.  0.  0.   0.  0.  0.                                                       ! omega+, xi, 4 dummies.
2  10.  ! Numerical method for field & derivatives,  flying mesh size is xpas/10=0.4/10. (KIRD,RESOL).
1.                                                                            ! Integration step size.
2 0.  0. 0.  0.                                                                  ! magnet positioning.
'FIT'                                                                                                        4
27
1 30 0 [200,400]             ! The following first 14 variables are initial periodic radius and angle
1 31 0 [-100,0]                                                                ! of the seven orbits.
1 40 0 [200,400]
1 41 0 [-100,0]
1 50 0 [200,400]
1 51 0 [-100,0]
1 60 0 [200,400]
1 61 0 [-100,0]
1 70 0 [200,400]
1 71 0 [-100,0]
1 80 0 [200,400]
1 81 0 [-100,0]
1 90 0 [200,400]
1 91 0 [-100,0]
3 9 0 .2                                                                                   ! Vary B0.
3 12 0 1.                                    ! Next twelve variables: the 12 field indices B1 to B12.
3 13 0 1.
3 14 0 1.
3 15 0 1.
3 16 0 5.
3 17 0 5.
3 18 0 9.
3 19 0 9.
3 20 0 9.
3 21 0 9.
3 22 0 9.
3 23 0 9.
21 7.743e-3
3.1 1 2 #End 0. 1. 0                 ! The following 14 constraints request periodic radius and angle
3.1 1 3 #End 0. 1. 0                                                          ! for the seven orbits.
3.1 2 2 #End 0. 1. 0
3.1 2 3 #End 0. 1. 0
3.1 3 2 #End 0. 1. 0
3.1 3 3 #End 0. 1. 0
3.1 4 2 #End 0. 1. 0
3.1 4 3 #End 0. 1. 0
3.1 5 2 #End 0. 1. 0
3.1 5 3 #End 0. 1. 0
3.1 6 2 #End 0. 1. 0
3.1 6 3 #End 0. 1. 0
3.1 7 2 #End 0. 1. 0
3.1 7 3 #End 0. 1. 0
3.4 1 7 #End 0. .00001 1 4             ! 6 constraints request equal rev. period for all 7 particles.
3.4 2 7 #End 0. .00001 1 4
3.4 3 7 #End 0. .00005 1 4
3 4 7 #End 1.4743416128820E-02 1.e9 1 4     ! Reference time is that of particle 4, not a constraint.
3.4 5 7 #End 0. .0001 1 4
3.4 6 7 #End 0. .0001 1 4
3.4 7 7 #End 0. .00001 1 4
 
'FAISCEAU'                                                                                                   5
'FAISTORE'                                                                                                   6
FITted.fai
1
'SYSTEM'                                                                                                     7
1
gnuplot < ./gnuplot_Trev.gnu
'END'                                              ! End of the sequence. Whatever follows is ignored.

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =       1249.382 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       7 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 1

     Particle  properties :
     PROTON
                     Mass          =    938.272        MeV/c2
                     Charge        =   1.602176E-19    C     
                     G  factor     =    1.79285              
                     COM life-time =   1.000000E+99    s     

              Reference  data :
                    mag. rigidity (kG.cm)   :   1249.3824      =p/q, such that dev.=B*L/rigidity
                    mass (MeV/c2)           :   938.27208    
                    momentum (MeV/c)        :   374.55542    
                    energy, total (MeV)     :   1010.2704    
                    energy, kinetic (MeV)   :   71.998311    
                    beta = v/c              :  0.3707477007    
                    gamma                   :   1.076735003    
                    beta*gamma              :  0.3991970265    
                    G*gamma                 :   1.930421496    
                    electric rigidity (MeV) :   138.8655625    =T[eV]*(gamma+1)/gamma, such that dev.=E*L/rigidity
  
 I, AMQ(1,I), AMQ(2,I)/QE, P/Pref, v/c, time, s :
  
     1   9.38272081E+02  1.00000000E+00  1.40000001E+00  4.87856155E-01  0.00000000E+00  0.00000000E+00
     2   9.38272081E+02  1.00000000E+00  1.50000001E+00  5.13735729E-01  0.00000000E+00  0.00000000E+00
     3   9.38272081E+02  1.00000000E+00  1.60000001E+00  5.38285352E-01  0.00000000E+00  0.00000000E+00
     4   9.38272081E+02  1.00000000E+00  1.70778172E+00  5.63293507E-01  0.00000000E+00  0.00000000E+00
     5   9.38272081E+02  1.00000000E+00  1.80000001E+00  5.83531441E-01  0.00000000E+00  0.00000000E+00
     6   9.38272081E+02  1.00000000E+00  1.90000001E+00  6.04312476E-01  0.00000000E+00  0.00000000E+00
     7   9.38272081E+02  1.00000000E+00  2.10037893E+00  6.42502199E-01  0.00000000E+00  0.00000000E+00

************************************************************************************************************************************
      3  Keyword, label(s) :  CYCLOTRON                                                                                IPASS= 1


                OPEN FILE zgoubi.plt                                                                      
                FOR PRINTING TRAJECTORIES

                    Cyclotron N-tuple,  number  of  dipoles  N :  1

            Total angular extent of the magnet :  45.00 Degres
            Reference geometrical radius R0  :     276.00 cm
            EFB type:                        :  spiral        

     Dipole # 1
            Positionning  angle ACENT :    0.000     degrees
            Positionning  wrt.  R0  :    0.0     cm
            B0 =   51.43     kGauss,       K =   0.0000    

     Entrance  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g0+g1*R/100    1.00
           COEFFICIENTS :  8   1.10244   3.12915  -3.14287   3.08581  -1.43545   0.24047
           Shift  of  EFB  is    0.000     cm

          OMEGA =  11.00 deg.     Spiral  angle  =   3.50 deg.

         Exit  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g0+g1*R/100    1.00
           COEFFICIENTS :  8   0.70490   4.16013  -4.33096   3.54042  -1.34727   0.18261
           Shift  of  EFB  is    0.000     cm

          OMEGA =  -8.50 deg.     Spiral  angle  =   2.00 deg.

         Lateral face :  unused


      Field & deriv. calculation : interpolation
                    3*3-point  interpolation, size of flying mesh :  integration step /   10.00    

                    Integration step :   1.000     cm   (i.e.,   3.6232E-03 rad  at mean radius RM =    276.0    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.4000   267.042   -15.052     0.000     0.000            0.393   267.036    -0.015     0.000     0.000            1
  A    1  1.5000   281.258   -26.633     0.000     0.000            0.393   281.307    -0.027     0.000     0.000            2
  A    1  1.6000   294.830   -38.756     0.000     0.000            0.393   294.835    -0.039     0.000     0.000            3
  A    1  1.7078   308.680   -51.710     0.000     0.000            0.393   308.678    -0.052     0.000     0.000            4
  A    1  1.8000   319.925   -62.713     0.000     0.000            0.393   319.927    -0.063     0.000     0.000            5
  A    1  1.9000   331.558   -74.553     0.000     0.000            0.393   331.554    -0.075     0.000     0.000            6
  A    1  2.1004   353.201   -97.765     0.000     0.000            0.393   353.186    -0.098     0.000     0.000            7


                CONDITIONS  DE  MAXWELL  (     1740.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    53.8200000     m ;  Time  (for ref. rigidity & particle) =   4.842220E-07 s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FIT                                                                                      IPASS= 1

     Pgm main. FITING=.T., FIT procedure launched.

           variable #            1       IR =            1 ,   ok.
           variable #            1       IP =           30 ,   ok.
           variable #            2       IR =            1 ,   ok.
           variable #            2       IP =           31 ,   ok.
           variable #            3       IR =            1 ,   ok.
           variable #            3       IP =           40 ,   ok.
           variable #            4       IR =            1 ,   ok.
           variable #            4       IP =           41 ,   ok.
           variable #            5       IR =            1 ,   ok.
           variable #            5       IP =           50 ,   ok.
           variable #            6       IR =            1 ,   ok.
           variable #            6       IP =           51 ,   ok.
           variable #            7       IR =            1 ,   ok.
           variable #            7       IP =           60 ,   ok.
           variable #            8       IR =            1 ,   ok.
           variable #            8       IP =           61 ,   ok.
           variable #            9       IR =            1 ,   ok.
           variable #            9       IP =           70 ,   ok.
           variable #           10       IR =            1 ,   ok.
           variable #           10       IP =           71 ,   ok.
           variable #           11       IR =            1 ,   ok.
           variable #           11       IP =           80 ,   ok.
           variable #           12       IR =            1 ,   ok.
           variable #           12       IP =           81 ,   ok.
           variable #           13       IR =            1 ,   ok.
           variable #           13       IP =           90 ,   ok.
           variable #           14       IR =            1 ,   ok.
           variable #           14       IP =           91 ,   ok.
           variable #           15       IR =            3 ,   ok.
           variable #           15       IP =            9 ,   ok.
           variable #           16       IR =            3 ,   ok.
           variable #           16       IP =           12 ,   ok.
           variable #           17       IR =            3 ,   ok.
           variable #           17       IP =           13 ,   ok.
           variable #           18       IR =            3 ,   ok.
           variable #           18       IP =           14 ,   ok.
           variable #           19       IR =            3 ,   ok.
           variable #           19       IP =           15 ,   ok.
           variable #           20       IR =            3 ,   ok.
           variable #           20       IP =           16 ,   ok.
           variable #           21       IR =            3 ,   ok.
           variable #           21       IP =           17 ,   ok.
           variable #           22       IR =            3 ,   ok.
           variable #           22       IP =           18 ,   ok.
           variable #           23       IR =            3 ,   ok.
           variable #           23       IP =           19 ,   ok.
           variable #           24       IR =            3 ,   ok.
           variable #           24       IP =           20 ,   ok.
           variable #           25       IR =            3 ,   ok.
           variable #           25       IP =           21 ,   ok.
           variable #           26       IR =            3 ,   ok.
           variable #           26       IP =           22 ,   ok.
           variable #           27       IR =            3 ,   ok.
           variable #           27       IP =           23 ,   ok.
           constraint #            1       IR =            3 ,   ok.
           constraint #            1       I  =            1 ,   ok.
           constraint #            2       IR =            3 ,   ok.
           constraint #            2       I  =            1 ,   ok.
           constraint #            3       IR =            3 ,   ok.
           constraint #            3       I  =            2 ,   ok.
           constraint #            4       IR =            3 ,   ok.
           constraint #            4       I  =            2 ,   ok.
           constraint #            5       IR =            3 ,   ok.
           constraint #            5       I  =            3 ,   ok.
           constraint #            6       IR =            3 ,   ok.
           constraint #            6       I  =            3 ,   ok.
           constraint #            7       IR =            3 ,   ok.
           constraint #            7       I  =            4 ,   ok.
           constraint #            8       IR =            3 ,   ok.
           constraint #            8       I  =            4 ,   ok.
           constraint #            9       IR =            3 ,   ok.
           constraint #            9       I  =            5 ,   ok.
           constraint #           10       IR =            3 ,   ok.
           constraint #           10       I  =            5 ,   ok.
           constraint #           11       IR =            3 ,   ok.
           constraint #           11       I  =            6 ,   ok.
           constraint #           12       IR =            3 ,   ok.
           constraint #           12       I  =            6 ,   ok.
           constraint #           13       IR =            3 ,   ok.
           constraint #           13       I  =            7 ,   ok.
           constraint #           14       IR =            3 ,   ok.
           constraint #           14       I  =            7 ,   ok.
           constraint #           15       IR =            3 ,   ok.
           constraint #           15       I  =            1 ,   ok.
           constraint #           16       IR =            3 ,   ok.
           constraint #           16       I  =            2 ,   ok.
           constraint #           17       IR =            3 ,   ok.
           constraint #           17       I  =            3 ,   ok.
           constraint #           18       IR =            3 ,   ok.
           constraint #           18       I  =            4 ,   ok.
           constraint #           19       IR =            3 ,   ok.
           constraint #           19       I  =            5 ,   ok.
           constraint #           20       IR =            3 ,   ok.
           constraint #           20       I  =            6 ,   ok.
           constraint #           21       IR =            3 ,   ok.
           constraint #           21       I  =            7 ,   ok.

                    FIT  variables  and  constraints  in  good  order,  FIT  will proceed. 
 STATUS OF VARIABLES  (Iteration #     1 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   1   1    30    200.        267.       267.04230       400.      0.667      OBJET      -                    -                   
   1   2    31   -100.       -15.1      -15.051666       0.00      0.333      OBJET      -                    -                   
   1   3    40    200.        281.       281.25821       400.      0.667      OBJET      -                    -                   
   1   4    41   -100.       -26.6      -26.633115       0.00      0.333      OBJET      -                    -                   
   1   5    50    200.        295.       294.82957       400.      0.667      OBJET      -                    -                   
   1   6    51   -100.       -38.8      -38.755774       0.00      0.333      OBJET      -                    -                   
   1   7    60    200.        309.       308.68046       400.      0.667      OBJET      -                    -                   
   1   8    61   -100.       -51.7      -51.710447       0.00      0.333      OBJET      -                    -                   
   1   9    70    200.        320.       319.92496       400.      0.667      OBJET      -                    -                   
   1  10    71   -100.       -62.7      -62.712620       0.00      0.333      OBJET      -                    -                   
   1  11    80    200.        332.       331.55753       400.      0.667      OBJET      -                    -                   
   1  12    81   -100.       -74.6      -74.553215       0.00      0.333      OBJET      -                    -                   
   1  13    90    200.        353.       353.20117       400.      0.667      OBJET      -                    -                   
   1  14    91   -100.       -97.8      -97.764704       0.00      0.333      OBJET      -                    -                   
   3  15     9    41.1        51.4       51.431190       61.7      6.857E-02  CYCLOTRON  -                    -                   
   3  16    12  -0.897      -0.449     -0.44871551       0.00      2.991E-03  CYCLOTRON  -                    -                   
   3  17    13    0.00       2.097E-03  2.09658166E-03  4.193E-03  1.398E-05  CYCLOTRON  -                    -                   
   3  18    14  -9.052E-06  -4.526E-06 -4.52609250E-06   0.00      3.017E-08  CYCLOTRON  -                    -                   
   3  19    15    0.00       3.959E-09  3.95913656E-09  7.918E-09  2.639E-11  CYCLOTRON  -                    -                   
   3  20    16  -3.414E-13  -5.690E-14 -5.68972605E-14  2.276E-13  1.897E-15  CYCLOTRON  -                    -                   
   3  21    17  -3.395E-16   8.487E-17  8.48686076E-17  5.092E-16  2.829E-18  CYCLOTRON  -                    -                   
   3  22    18  -2.513E-18  -2.513E-19 -2.51326976E-19  2.011E-18  1.508E-20  CYCLOTRON  -                    -                   
   3  23    19  -3.901E-21   4.876E-22  4.87639705E-22  4.876E-21  2.926E-23  CYCLOTRON  -                    -                   
   3  24    20  -1.542E-24  -1.542E-25 -1.54248545E-25  1.234E-24  9.255E-27  CYCLOTRON  -                    -                   
   3  25    21  -1.404E-26   1.755E-27  1.75499497E-27  1.755E-26  1.053E-28  CYCLOTRON  -                    -                   
   3  26    22  -3.238E-28  -3.238E-29 -3.23761721E-29  2.590E-28  1.943E-30  CYCLOTRON  -                    -                   
   3  27    23  -2.201E-31   2.751E-32  2.75094168E-32  2.751E-31  1.651E-33  CYCLOTRON  -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   7.7430E-03)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     3    0.000000E+00    1.000E+00    6.341529E-03    5.19E-03 CYCLOTRON  -                    -                    0
  3   1   3     3    0.000000E+00    1.000E+00    1.426407E-02    2.63E-02 CYCLOTRON  -                    -                    0
  3   2   2     3    0.000000E+00    1.000E+00    4.890085E-02    3.09E-01 CYCLOTRON  -                    -                    0
  3   2   3     3    0.000000E+00    1.000E+00    4.309459E-02    2.40E-01 CYCLOTRON  -                    -                    0
  3   3   2     3    0.000000E+00    1.000E+00    5.048516E-03    3.29E-03 CYCLOTRON  -                    -                    0
  3   3   3     3    0.000000E+00    1.000E+00    2.927308E-03    1.11E-03 CYCLOTRON  -                    -                    0
  3   4   2     3    0.000000E+00    1.000E+00    2.183807E-03    6.16E-04 CYCLOTRON  -                    -                    0
  3   4   3     3    0.000000E+00    1.000E+00    3.426864E-02    1.52E-01 CYCLOTRON  -                    -                    0
  3   5   2     3    0.000000E+00    1.000E+00    2.205872E-03    6.28E-04 CYCLOTRON  -                    -                    0
  3   5   3     3    0.000000E+00    1.000E+00    7.591342E-04    7.44E-05 CYCLOTRON  -                    -                    0
  3   6   2     3    0.000000E+00    1.000E+00    3.582300E-03    1.66E-03 CYCLOTRON  -                    -                    0
  3   6   3     3    0.000000E+00    1.000E+00    1.797278E-03    4.17E-04 CYCLOTRON  -                    -                    0
  3   7   2     3    0.000000E+00    1.000E+00    1.516982E-02    2.97E-02 CYCLOTRON  -                    -                    0
  3   7   3     3    0.000000E+00    1.000E+00    7.269286E-03    6.82E-03 CYCLOTRON  -                    -                    0
  3   1   7     3    0.000000E+00    1.000E-05    8.302084E-08    8.90E-03 CYCLOTRON  -                    -                    1   4.0E+00
  3   2   7     3    0.000000E+00    1.000E-05    3.336565E-07    1.44E-01 CYCLOTRON  -                    -                    1   4.0E+00
  3   3   7     3    0.000000E+00    5.000E-05    4.956878E-07    1.27E-02 CYCLOTRON  -                    -                    1   4.0E+00
  3   4   7     3    1.474342E-02    1.000E+09    1.474408E-02    5.66E-29 CYCLOTRON  -                    -                    1   4.0E+00
  3   5   7     3    0.000000E+00    1.000E-04    1.215562E-06    1.91E-02 CYCLOTRON  -                    -                    1   4.0E+00
  3   6   7     3    0.000000E+00    1.000E-04    1.574112E-06    3.20E-02 CYCLOTRON  -                    -                    1   4.0E+00
  3   7   7     3    0.000000E+00    1.000E-05    7.560322E-08    7.38E-03 CYCLOTRON  -                    -                    1   4.0E+00
 Fit reached penalty value   7.7429E-03



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =       1249.382 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       7 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 1


************************************************************************************************************************************
      3  Keyword, label(s) :  CYCLOTRON                                                                                IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Cyclotron N-tuple,  number  of  dipoles  N :  1

            Total angular extent of the magnet :  45.00 Degres
            Reference geometrical radius R0  :     276.00 cm
            EFB type:                        :  spiral        

     Dipole # 1
            Positionning  angle ACENT :    0.000     degrees
            Positionning  wrt.  R0  :    0.0     cm
            B0 =   51.43     kGauss,       K =   0.0000    

     Entrance  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g0+g1*R/100    1.00
           COEFFICIENTS :  8   1.10244   3.12915  -3.14287   3.08581  -1.43545   0.24047
           Shift  of  EFB  is    0.000     cm

          OMEGA =  11.00 deg.     Spiral  angle  =   3.50 deg.

         Exit  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g0+g1*R/100    1.00
           COEFFICIENTS :  8   0.70490   4.16013  -4.33096   3.54042  -1.34727   0.18261
           Shift  of  EFB  is    0.000     cm

          OMEGA =  -8.50 deg.     Spiral  angle  =   2.00 deg.

         Lateral face :  unused


      Field & deriv. calculation : interpolation
                    3*3-point  interpolation, size of flying mesh :  integration step /   10.00    

                    Integration step :   1.000     cm   (i.e.,   3.6232E-03 rad  at mean radius RM =    276.0    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.4000   267.042   -15.052     0.000     0.000            0.393   267.036    -0.015     0.000     0.000            1
  A    1  1.5000   281.258   -26.633     0.000     0.000            0.393   281.307    -0.027     0.000     0.000            2
  A    1  1.6000   294.830   -38.756     0.000     0.000            0.393   294.835    -0.039     0.000     0.000            3
  A    1  1.7078   308.680   -51.710     0.000     0.000            0.393   308.678    -0.052     0.000     0.000            4
  A    1  1.8000   319.925   -62.713     0.000     0.000            0.393   319.927    -0.063     0.000     0.000            5
  A    1  1.9000   331.558   -74.553     0.000     0.000            0.393   331.554    -0.075     0.000     0.000            6
  A    1  2.1004   353.201   -97.765     0.000     0.000            0.393   353.186    -0.098     0.000     0.000            7


                CONDITIONS  DE  MAXWELL  (     1740.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    53.8200000     m ;  Time  (for ref. rigidity & particle) =   4.842220E-07 s 

************************************************************************************************************************************

      4   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  7 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o   1   1.4000   267.042   -15.052     0.000     0.000      0.0000   0.4000  267.036  -15.066    0.000    0.000   2.156392E+02     1
               Time of flight (mus) :  1.47439951E-02 mass (MeV/c2) :   938.272    
o   1   1.5000   281.258   -26.633     0.000     0.000      0.0000   0.5000  281.307  -26.590    0.000    0.000   2.270847E+02     2
               Time of flight (mus) :  1.47444118E-02 mass (MeV/c2) :   938.272    
o   1   1.6000   294.830   -38.756     0.000     0.000      0.0000   0.6000  294.835  -38.753    0.000    0.000   2.379389E+02     3
               Time of flight (mus) :  1.47445738E-02 mass (MeV/c2) :   938.272    
R   1   1.7078   308.680   -51.710     0.000     0.000      0.0000   0.7078  308.678  -51.745    0.000    0.000   2.489849E+02     4
               Time of flight (mus) :  1.47440781E-02 mass (MeV/c2) :   938.272    
o   1   1.8000   319.925   -62.713     0.000     0.000      0.0000   0.8000  319.927  -62.713    0.000    0.000   2.579092E+02     5
               Time of flight (mus) :  1.47428626E-02 mass (MeV/c2) :   938.272    
o   1   1.9000   331.558   -74.553     0.000     0.000      0.0000   0.9000  331.554  -74.555    0.000    0.000   2.670875E+02     6
               Time of flight (mus) :  1.47425040E-02 mass (MeV/c2) :   938.272    
o   1   2.1004   353.201   -97.765     0.000     0.000      0.0000   1.1004  353.186  -97.757    0.000    0.000   2.839979E+02     7
               Time of flight (mus) :  1.47441537E-02 mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface/pi              alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                                   in ellips,  out 
   3.6244E-04 [m.rad]      2.0070E+01   2.0902E+02   3.080747E+00  -5.245419E-02        7        5   0.7143      (Y,T)         1
   0.0000E+00 [m.rad]      0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        7        7    1.000      (Z,P)         1
   3.2173E-05 [mu_s.MeV]   4.2751E-01   1.6748E-08   1.474380E-02   2.009899E+02        7        6   0.8571      (t,K)         1

(Y,T)  space (units : m, rad   ) :  
      sigma_Y = sqrt(surface/pi * beta) =   2.752436E-01
      sigma_T = sqrt(surface/pi * (1+alpha^2)/beta) =   2.646061E-02

(Z,P)  space (units : m, rad   ) :  
      sigma_Z = sqrt(surface/pi * beta) =   0.000000E+00
      sigma_P = sqrt(surface/pi * (1+alpha^2)/beta) =   0.000000E+00

(t,K)  space (units : mu_s, MeV) :  
      sigma_t = sqrt(surface/pi * beta) =   7.340605E-07
      sigma_K = sqrt(surface/pi * (1+alpha^2)/beta) =   4.766600E+01


  Beam  sigma  matrix  and  determinants : 

   7.575902E-02  -7.274090E-03   0.000000E+00   0.000000E+00
  -7.274090E-03   7.001641E-04   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     3.624423E-04    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)
      normalized (*beta*gamma) :     1.446859E-04    0.000000E+00

************************************************************************************************************************************
      6  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 1


                OPEN FILE FITted.fai                                                                      
                FOR PRINTING COORDINATES 

               Print will occur at element[s] labeled : 


************************************************************************************************************************************
      7  Keyword, label(s) :  SYSTEM                                                                                   IPASS= 1

     Number  of  commands :   1,  as  follows : 

 gnuplot < ./gnuplot_Trev.gnu

************************************************************************************************************************************
      8  Keyword, label(s) :  END                                                                                      IPASS= 1


                             7 particles have been launched
                     Made  it  to  the  end :      7

************************************************************************************************************************************
 Pgm zgoubi : Execution ended normally, upon keyword END or FIN
   
 File in:   CYCLO_FITisochronism.res
 File out:  zgoubi.res

  Zgoubi, author's dvlpmnt version.
  Job  started  on  14-04-2021,  at  17:23:42 
  JOB  ENDED  ON    14-04-2021,  AT  17:24:14 

   CPU time, total :    0.49101099999999998     
