# gnuplot_Trev.gnu 
set tit "Expected:  initial conditions = final"
set xtics nomirror; set xlabel "Y0 [cm]"; set ytics nomirror; set ylabel "Y"
set x2tics; set x2label "T0 [cm]"; set y2tics; set y2label "T"
set k t l
colY0=3;  colY=10;  colT0=4;  colT=11
plot \
     'FITted.fai' u colY0:colY w lp lt 3 dt 7 lw 2 pt 12 lc 1 tit "Y(Y_0)" ,\
     'FITted.fai' u colT0:colT axes x2y2 w lp lt 3 dt "- ." lw 2 pt 14 lc 2 tit "T(T_0)"

pause 1

 set terminal postscript eps blacktext color enh size 9.3cm,6cm "Times-Roman" 12
 set output "gnuplot_orbits_YY0_TT0.eps"
 replot
 set terminal X11
 unset output

set tit "Check isocronism"
unset x2tics; unset y2tics; unset x2label; unset y2label
#set y2tics ; set y2label "dT_{rev}/T_{ref}  (zoomed)"
set xlabel "perioric orbit radius, R  [cm]" ; set ylabel "dT_{rev}/T_{ref}"

set key t r 
T300s = 1.4736300330107593E-02; T300F = 1.474408E-02  
plot \
     'FITted.fai' u colY0:(($15-T300F)/T300F) w lp lt 3 dt 7 lw 2 pt 6 ps 1.2 lc 3  tit "12-index" 

#plot 'scanOrbits.fai' u colY0:(($15-T300s)/T300s) w lp lt 3 dt 7 lw 2 pt 4 ps 1.2 lc "black" tit "4-index" ,\
#     'FITted.fai' u colY0:(($15-T300F)/T300F) w lp lt 3 dt 7 lw 2 pt 6 ps 1.2 lc 3  tit "12-index" ,\
#     'FITted.fai' u colY0:(($15-T300F)/T300F) axes x1y2 w lp lt 3 dashtype "-" lw 2 pt 8 ps 1.2 lc 4 tit "''  zoom"; pause 1

 set terminal postscript eps blacktext color enh size 9.3cm,6cm "Times-Roman" 12
 set output "gnuplot_Trev.eps"
 replot
 set terminal X11
 unset output

pause 88
