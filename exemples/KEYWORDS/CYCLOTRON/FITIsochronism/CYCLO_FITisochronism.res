PSI-style CYCLOTRON spiral sector
'OBJET'                                                                                                      1
1249.382414
2
7 1
2.66987873E+02  -1.53906431E+01   0.00000000E+00   0.00000000E+00   0.00  1.40000001E+00 'o'
2.81206411E+02  -2.67988150E+01   0.00000000E+00   0.00000000E+00   0.00  1.50000001E+00 'o'
2.94772984E+02  -3.87525368E+01   0.00000000E+00   0.00000000E+00   0.00  1.60000001E+00 'o'
3.08548761E+02  -5.13851060E+01   0.00000000E+00   0.00000000E+00   0.00  1.70778172E+00 'R'
3.19902117E+02  -6.27214911E+01   0.00000000E+00   0.00000000E+00   0.00  1.80000001E+00 'o'
3.31519046E+02  -7.45153640E+01   0.00000000E+00   0.00000000E+00   0.00  1.90000001E+00 'o'
3.53078947E+02  -9.76560251E+01   0.00000000E+00   0.00000000E+00   0.00  2.10037893E+00 'o'
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
'PARTICUL'                                                                                                   2
PROTON
 
'CYCLOTRON'                                                                                                  3
0
1   45.0  2.760000E+02    1.0
0. 0. 9.92122800E-01 51.448226 0.50225380  0. -0.44865119  2.09656091E-03 -4.52634885E-06  3.95576857E-09 -5.84036362E-14  9.15856930E-17 -2.39129327E-19  4.69473569E-22 -1.16932080E-25  8.45215959E-28 -2.89373113E-29  3.03374162E-32
18.3000E+00  1.   28 -2.0                                        ! g0,  k , G0, G1    Entrance face.
8 1.10243581,  3.12915071, -3.14287154,  3.0858059 , -1.43544992, 0.24047436 0. 0. 0.
11.0   3.5  35.E-3   0.E-4   3.E-8   1.  1.  1.                     ! omega+, xi0,xi1,xi2,xi3,a,b,c.
18.3000E+00  1.   28. -2.0                                           ! g0,  k , G0, G1    Exit face.
8  0.70490173, 4.16013051, -4.33095751,  3.54041582, -1.34727027, 0.18261076  0. 0. 0.
-8.5  2.  12.E-3   75.E-6   0.E-6  1.  1.  1.       ! omega-, xi0s,xi1s,xi2s,xi3s,aexit,bexit,cexit.
0. -1                                                                       ! Lateral face, ignored.
0  0.   0.   0.   0.   0. 0.  0.
0.  0.   0.    0.    0. 0.
2 10.            ! Interpolation order for analytical derivaitevs; flying mesh size is step-sise/10.
1.0                                                                         ! INtegration step size.
2
0.  0. 0.  0.
 
'FIT'                                                                                                        4
28
1 30 0 [200,400]
1 31 0 [-100,0]
1 40 0 [200,400]
1 41 0 [-100,0]
1 50 0 [200,400]
1 51 0 [-100,0]
1 60 0 [200,400]
1 61 0 [-100,0]
1 70 0 [200,400]
1 71 0 [-100,0]
1 80 0 [200,400]
1 81 0 [-100,0]
1 90 0 [200,400]
1 91 0 [-100,0]
3 9 0 .2
3 10 0 .2
3 12 0 1.
3 13 0 1.
3 14 0 1.
3 15 0 1.
3 16 0 5.
3 17 0 5.
3 18 0 9.
3 19 0 9.
3 20 0 9.
3 21 0 9.
3 22 0 9.
3 23 0 9.
21  4.67e-2
3.1 1 2 #End 0. 1. 0
3.1 1 3 #End 0. 1. 0
3.1 2 2 #End 0. 1. 0
3.1 2 3 #End 0. 1. 0
3.1 3 2 #End 0. 1. 0
3.1 3 3 #End 0. 1. 0
3.1 4 2 #End 0. 1. 0
3.1 4 3 #End 0. 1. 0
3.1 5 2 #End 0. 1. 0
3.1 5 3 #End 0. 1. 0
3.1 6 2 #End 0. 1. 0
3.1 6 3 #End 0. 1. 0
3.1 7 2 #End 0. 1. 0
3.1 7 3 #End 0. 1. 0
3.4 1 7 #End 0. .00001 1 4
3.4 2 7 #End 0. .0001 1 4
3.4 3 7 #End 0. .001 1 4
3.4 4 7 #End 0. 1000. 1 4
3.4 5 7 #End 0. .001 1 4
3.4 6 7 #End 0. .0001 1 4
3.4 7 7 #End 0. .00001 1 4
! A posible variant to the constraints:
!   3 1 7 #End 1.47355580267539E-02 .00001 0
!   3 2 7 #End 1.47355580267539E-02 .0001 0
!   3 3 7 #End 1.47355580267539E-02 .001 0
!   3 4 7 #End 1.47355580267539E-02 .01 0   time is for Y=3.0849996848027092E+02  T=-5.1543915101107608E+01 D-1=7.07781721E-01
!   3 5 7 #End 1.47355580267539E-02 .001 0
!   3 6 7 #End 1.47355580267539E-02 .0001 0
!   3 7 7 #End 1.47355580267539E-02 .00001 0
 
'FAISTORE'                                                                                                   5
FITted.fai
1
 
'END'                                                                                                        5

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =       1249.382 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       7 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 1

     Particle  properties :
     PROTON
                     Mass          =    938.272        MeV/c2
                     Charge        =   1.602176E-19    C     
                     G  factor     =    1.79285              
                     COM life-time =   1.000000E+99    s     

              Reference  data :
                    mag. rigidity (kG.cm)   :   1249.3824      =p/q, such that dev.=B*L/rigidity
                    mass (MeV/c2)           :   938.27208    
                    momentum (MeV/c)        :   374.55542    
                    energy, total (MeV)     :   1010.2704    
                    energy, kinetic (MeV)   :   71.998311    
                    beta = v/c              :  0.3707477007    
                    gamma                   :   1.076735003    
                    beta*gamma              :  0.3991970265    
                    G*gamma                 :   1.930421496    
                    electric rigidity (MeV) :   138.8655625    =T[eV]*(gamma+1)/gamma, such that dev.=E*L/rigidity
  
 I, AMQ(1,I), AMQ(2,I)/QE, P/Pref, v/c, time, s :
  
     1   9.38272081E+02  1.00000000E+00  1.40000001E+00  4.87856155E-01  0.00000000E+00  0.00000000E+00
     2   9.38272081E+02  1.00000000E+00  1.50000001E+00  5.13735729E-01  0.00000000E+00  0.00000000E+00
     3   9.38272081E+02  1.00000000E+00  1.60000001E+00  5.38285352E-01  0.00000000E+00  0.00000000E+00
     4   9.38272081E+02  1.00000000E+00  1.70778172E+00  5.63293507E-01  0.00000000E+00  0.00000000E+00
     5   9.38272081E+02  1.00000000E+00  1.80000001E+00  5.83531441E-01  0.00000000E+00  0.00000000E+00
     6   9.38272081E+02  1.00000000E+00  1.90000001E+00  6.04312476E-01  0.00000000E+00  0.00000000E+00
     7   9.38272081E+02  1.00000000E+00  2.10037893E+00  6.42502199E-01  0.00000000E+00  0.00000000E+00

************************************************************************************************************************************
      3  Keyword, label(s) :  CYCLOTRON                                                                                IPASS= 1

                    Cyclotron N-tuple,  number  of  dipoles  N :  1

            Total angular extent of the magnet :  45.00 Degres
            Reference geometrical radius R0  :     276.00 cm
            EFB type:                        :  spiral        

     Dipole # 1
            Positionning  angle ACENT :    0.000     degrees
            Positionning  wrt.  R0  :    0.0     cm
            B0 =   51.45     kGauss,       K =  0.50225    

     Entrance  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g_0(1-r**2)**k 1.00
           COEFFICIENTS :  8   1.10244   3.12915  -3.14287   3.08581  -1.43545   0.24047
           Shift  of  EFB  is    0.000     cm

          OMEGA =  11.00 deg.     Spiral  angle  =   3.50 deg.

         Exit  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g_0(1-r**2)**k 1.00
           COEFFICIENTS :  8   0.70490   4.16013  -4.33096   3.54042  -1.34727   0.18261
           Shift  of  EFB  is    0.000     cm

          OMEGA =  -8.50 deg.     Spiral  angle  =   2.00 deg.

         Lateral face :  unused


      Field & deriv. calculation : interpolation
                    3*3-point  interpolation, size of flying mesh :  integration step /   10.00    

                    Integration step :   1.000     cm   (i.e.,   3.6232E-03 rad  at mean radius RM =    276.0    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.4000   266.988   -15.391     0.000     0.000            0.393   266.883    -0.015     0.000     0.000            1
  A    1  1.5000   281.206   -26.799     0.000     0.000            0.393   281.198    -0.027     0.000     0.000            2
  A    1  1.6000   294.773   -38.753     0.000     0.000            0.393   294.764    -0.039     0.000     0.000            3
  A    1  1.7078   308.549   -51.385     0.000     0.000            0.393   308.619    -0.051     0.000     0.000            4
  A    1  1.8000   319.902   -62.721     0.000     0.000            0.393   319.895    -0.063     0.000     0.000            5
  A    1  1.9000   331.519   -74.515     0.000     0.000            0.393   331.515    -0.075     0.000     0.000            6
  A    1  2.1004   353.079   -97.656     0.000     0.000            0.393   353.052    -0.098     0.000     0.000            7

 Cumulative length of optical axis =    53.8200000     m ;  Time  (for ref. rigidity & particle) =   4.842220E-07 s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FIT                                                                                      IPASS= 1

     Pgm main. FITING=.T., FIT procedure launched.

           variable #            1       IR =            1 ,   ok.
           variable #            1       IP =           30 ,   ok.
           variable #            2       IR =            1 ,   ok.
           variable #            2       IP =           31 ,   ok.
           variable #            3       IR =            1 ,   ok.
           variable #            3       IP =           40 ,   ok.
           variable #            4       IR =            1 ,   ok.
           variable #            4       IP =           41 ,   ok.
           variable #            5       IR =            1 ,   ok.
           variable #            5       IP =           50 ,   ok.
           variable #            6       IR =            1 ,   ok.
           variable #            6       IP =           51 ,   ok.
           variable #            7       IR =            1 ,   ok.
           variable #            7       IP =           60 ,   ok.
           variable #            8       IR =            1 ,   ok.
           variable #            8       IP =           61 ,   ok.
           variable #            9       IR =            1 ,   ok.
           variable #            9       IP =           70 ,   ok.
           variable #           10       IR =            1 ,   ok.
           variable #           10       IP =           71 ,   ok.
           variable #           11       IR =            1 ,   ok.
           variable #           11       IP =           80 ,   ok.
           variable #           12       IR =            1 ,   ok.
           variable #           12       IP =           81 ,   ok.
           variable #           13       IR =            1 ,   ok.
           variable #           13       IP =           90 ,   ok.
           variable #           14       IR =            1 ,   ok.
           variable #           14       IP =           91 ,   ok.
           variable #           15       IR =            3 ,   ok.
           variable #           15       IP =            9 ,   ok.
           variable #           16       IR =            3 ,   ok.
           variable #           16       IP =           10 ,   ok.
           variable #           17       IR =            3 ,   ok.
           variable #           17       IP =           12 ,   ok.
           variable #           18       IR =            3 ,   ok.
           variable #           18       IP =           13 ,   ok.
           variable #           19       IR =            3 ,   ok.
           variable #           19       IP =           14 ,   ok.
           variable #           20       IR =            3 ,   ok.
           variable #           20       IP =           15 ,   ok.
           variable #           21       IR =            3 ,   ok.
           variable #           21       IP =           16 ,   ok.
           variable #           22       IR =            3 ,   ok.
           variable #           22       IP =           17 ,   ok.
           variable #           23       IR =            3 ,   ok.
           variable #           23       IP =           18 ,   ok.
           variable #           24       IR =            3 ,   ok.
           variable #           24       IP =           19 ,   ok.
           variable #           25       IR =            3 ,   ok.
           variable #           25       IP =           20 ,   ok.
           variable #           26       IR =            3 ,   ok.
           variable #           26       IP =           21 ,   ok.
           variable #           27       IR =            3 ,   ok.
           variable #           27       IP =           22 ,   ok.
           variable #           28       IR =            3 ,   ok.
           variable #           28       IP =           23 ,   ok.
           constraint #            1       IR =            3 ,   ok.
           constraint #            1       I  =            1 ,   ok.
           constraint #            2       IR =            3 ,   ok.
           constraint #            2       I  =            1 ,   ok.
           constraint #            3       IR =            3 ,   ok.
           constraint #            3       I  =            2 ,   ok.
           constraint #            4       IR =            3 ,   ok.
           constraint #            4       I  =            2 ,   ok.
           constraint #            5       IR =            3 ,   ok.
           constraint #            5       I  =            3 ,   ok.
           constraint #            6       IR =            3 ,   ok.
           constraint #            6       I  =            3 ,   ok.
           constraint #            7       IR =            3 ,   ok.
           constraint #            7       I  =            4 ,   ok.
           constraint #            8       IR =            3 ,   ok.
           constraint #            8       I  =            4 ,   ok.
           constraint #            9       IR =            3 ,   ok.
           constraint #            9       I  =            5 ,   ok.
           constraint #           10       IR =            3 ,   ok.
           constraint #           10       I  =            5 ,   ok.
           constraint #           11       IR =            3 ,   ok.
           constraint #           11       I  =            6 ,   ok.
           constraint #           12       IR =            3 ,   ok.
           constraint #           12       I  =            6 ,   ok.
           constraint #           13       IR =            3 ,   ok.
           constraint #           13       I  =            7 ,   ok.
           constraint #           14       IR =            3 ,   ok.
           constraint #           14       I  =            7 ,   ok.
           constraint #           15       IR =            3 ,   ok.
           constraint #           15       I  =            1 ,   ok.
           constraint #           16       IR =            3 ,   ok.
           constraint #           16       I  =            2 ,   ok.
           constraint #           17       IR =            3 ,   ok.
           constraint #           17       I  =            3 ,   ok.
           constraint #           18       IR =            3 ,   ok.
           constraint #           18       I  =            4 ,   ok.
           constraint #           19       IR =            3 ,   ok.
           constraint #           19       I  =            5 ,   ok.
           constraint #           20       IR =            3 ,   ok.
           constraint #           20       I  =            6 ,   ok.
           constraint #           21       IR =            3 ,   ok.
           constraint #           21       I  =            7 ,   ok.

                    FIT  variables  and  constraints  in  good  order,  FIT  will proceed. 
 STATUS OF VARIABLES  (Iteration #     1 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   1   1    30    200.        267.       266.98787       400.      0.667      OBJET      -                    -                   
   1   2    31   -100.       -15.4      -15.390643       0.00      0.333      OBJET      -                    -                   
   1   3    40    200.        281.       281.20641       400.      0.667      OBJET      -                    -                   
   1   4    41   -100.       -26.8      -26.798815       0.00      0.333      OBJET      -                    -                   
   1   5    50    200.        295.       294.77298       400.      0.667      OBJET      -                    -                   
   1   6    51   -100.       -38.8      -38.752537       0.00      0.333      OBJET      -                    -                   
   1   7    60    200.        309.       308.54876       400.      0.667      OBJET      -                    -                   
   1   8    61   -100.       -51.4      -51.385106       0.00      0.333      OBJET      -                    -                   
   1   9    70    200.        320.       319.90212       400.      0.667      OBJET      -                    -                   
   1  10    71   -100.       -62.7      -62.721491       0.00      0.333      OBJET      -                    -                   
   1  11    80    200.        332.       331.51905       400.      0.667      OBJET      -                    -                   
   1  12    81   -100.       -74.5      -74.515364       0.00      0.333      OBJET      -                    -                   
   1  13    90    200.        353.       353.07895       400.      0.667      OBJET      -                    -                   
   1  14    91   -100.       -97.7      -97.656025       0.00      0.333      OBJET      -                    -                   
   3  15     9    41.2        51.4       51.448226       61.7      6.860E-02  CYCLOTRON  -                    -                   
   3  16    10   0.402       0.502      0.50225380      0.603      6.697E-04  CYCLOTRON  -                    -                   
   3  17    12  -0.897      -0.449     -0.44865119       0.00      2.991E-03  CYCLOTRON  -                    -                   
   3  18    13    0.00       2.097E-03  2.09656091E-03  4.193E-03  1.398E-05  CYCLOTRON  -                    -                   
   3  19    14  -9.053E-06  -4.526E-06 -4.52634885E-06   0.00      3.018E-08  CYCLOTRON  -                    -                   
   3  20    15    0.00       3.956E-09  3.95576857E-09  7.912E-09  2.637E-11  CYCLOTRON  -                    -                   
   3  21    16  -3.504E-13  -5.840E-14 -5.84036362E-14  2.336E-13  1.947E-15  CYCLOTRON  -                    -                   
   3  22    17  -3.663E-16   9.159E-17  9.15856930E-17  5.495E-16  3.053E-18  CYCLOTRON  -                    -                   
   3  23    18  -2.391E-18  -2.391E-19 -2.39129327E-19  1.913E-18  1.435E-20  CYCLOTRON  -                    -                   
   3  24    19  -3.756E-21   4.695E-22  4.69473569E-22  4.695E-21  2.817E-23  CYCLOTRON  -                    -                   
   3  25    20  -1.169E-24  -1.169E-25 -1.16932080E-25  9.355E-25  7.016E-27  CYCLOTRON  -                    -                   
   3  26    21  -6.762E-27   8.452E-28  8.45215959E-28  8.452E-27  5.071E-29  CYCLOTRON  -                    -                   
   3  27    22  -2.894E-28  -2.894E-29 -2.89373113E-29  2.315E-28  1.736E-30  CYCLOTRON  -                    -                   
   3  28    23  -2.427E-31   3.034E-32  3.03374162E-32  3.034E-31  1.820E-33  CYCLOTRON  -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   4.6700E-02)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     3    0.000000E+00    1.000E+00    1.052030E-01    2.37E-01 CYCLOTRON  -                    -                    0
  3   1   3     3    0.000000E+00    1.000E+00    1.025525E-01    2.25E-01 CYCLOTRON  -                    -                    0
  3   2   2     3    0.000000E+00    1.000E+00    8.531278E-03    1.56E-03 CYCLOTRON  -                    -                    0
  3   2   3     3    0.000000E+00    1.000E+00    1.623984E-03    5.65E-05 CYCLOTRON  -                    -                    0
  3   3   2     3    0.000000E+00    1.000E+00    8.575436E-03    1.58E-03 CYCLOTRON  -                    -                    0
  3   3   3     3    0.000000E+00    1.000E+00    3.805211E-03    3.10E-04 CYCLOTRON  -                    -                    0
  3   4   2     3    0.000000E+00    1.000E+00    7.056383E-02    1.07E-01 CYCLOTRON  -                    -                    0
  3   4   3     3    0.000000E+00    1.000E+00    1.088497E-01    2.54E-01 CYCLOTRON  -                    -                    0
  3   5   2     3    0.000000E+00    1.000E+00    6.888301E-03    1.02E-03 CYCLOTRON  -                    -                    0
  3   5   3     3    0.000000E+00    1.000E+00    1.866757E-03    7.47E-05 CYCLOTRON  -                    -                    0
  3   6   2     3    0.000000E+00    1.000E+00    3.946734E-03    3.34E-04 CYCLOTRON  -                    -                    0
  3   6   3     3    0.000000E+00    1.000E+00    2.415979E-04    1.25E-06 CYCLOTRON  -                    -                    0
  3   7   2     3    0.000000E+00    1.000E+00    2.718138E-02    1.58E-02 CYCLOTRON  -                    -                    0
  3   7   3     3    0.000000E+00    1.000E+00    1.791844E-02    6.88E-03 CYCLOTRON  -                    -                    0
  3   1   7     3    0.000000E+00    1.000E-05    7.852283E-07    1.32E-01 CYCLOTRON  -                    -                    1   4.0E+00
  3   2   7     3    0.000000E+00    1.000E-04    1.050796E-06    2.37E-03 CYCLOTRON  -                    -                    1   4.0E+00
  3   3   7     3    0.000000E+00    1.000E-03    2.175880E-06    1.01E-04 CYCLOTRON  -                    -                    1   4.0E+00
  3   4   7     3    0.000000E+00    1.000E+03    0.000000E+00    0.00E+00 CYCLOTRON  -                    -                    1   4.0E+00
  3   5   7     3    0.000000E+00    1.000E-03    2.350964E-06    1.18E-04 CYCLOTRON  -                    -                    1   4.0E+00
  3   6   7     3    0.000000E+00    1.000E-04    1.554631E-06    5.18E-03 CYCLOTRON  -                    -                    1   4.0E+00
  3   7   7     3    0.000000E+00    1.000E-05    2.108033E-07    9.52E-03 CYCLOTRON  -                    -                    1   4.0E+00
 Fit reached penalty value   4.6675E-02



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =       1249.382 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       7 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 1


************************************************************************************************************************************
      3  Keyword, label(s) :  CYCLOTRON                                                                                IPASS= 1

                    Cyclotron N-tuple,  number  of  dipoles  N :  1

            Total angular extent of the magnet :  45.00 Degres
            Reference geometrical radius R0  :     276.00 cm
            EFB type:                        :  spiral        

     Dipole # 1
            Positionning  angle ACENT :    0.000     degrees
            Positionning  wrt.  R0  :    0.0     cm
            B0 =   51.45     kGauss,       K =  0.50225    

     Entrance  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g_0(1-r**2)**k 1.00
           COEFFICIENTS :  8   1.10244   3.12915  -3.14287   3.08581  -1.43545   0.24047
           Shift  of  EFB  is    0.000     cm

          OMEGA =  11.00 deg.     Spiral  angle  =   3.50 deg.

         Exit  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g_0(1-r**2)**k 1.00
           COEFFICIENTS :  8   0.70490   4.16013  -4.33096   3.54042  -1.34727   0.18261
           Shift  of  EFB  is    0.000     cm

          OMEGA =  -8.50 deg.     Spiral  angle  =   2.00 deg.

         Lateral face :  unused


      Field & deriv. calculation : interpolation
                    3*3-point  interpolation, size of flying mesh :  integration step /   10.00    

                    Integration step :   1.000     cm   (i.e.,   3.6232E-03 rad  at mean radius RM =    276.0    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.4000   266.988   -15.391     0.000     0.000            0.393   266.883    -0.015     0.000     0.000            1
  A    1  1.5000   281.206   -26.799     0.000     0.000            0.393   281.198    -0.027     0.000     0.000            2
  A    1  1.6000   294.773   -38.753     0.000     0.000            0.393   294.764    -0.039     0.000     0.000            3
  A    1  1.7078   308.549   -51.385     0.000     0.000            0.393   308.619    -0.051     0.000     0.000            4
  A    1  1.8000   319.902   -62.721     0.000     0.000            0.393   319.895    -0.063     0.000     0.000            5
  A    1  1.9000   331.519   -74.515     0.000     0.000            0.393   331.515    -0.075     0.000     0.000            6
  A    1  2.1004   353.079   -97.656     0.000     0.000            0.393   353.052    -0.098     0.000     0.000            7

 Cumulative length of optical axis =    53.8200000     m ;  Time  (for ref. rigidity & particle) =   4.842220E-07 s 

************************************************************************************************************************************

      4   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      5  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 1


                OPEN FILE FITted.fai                                                                      
                FOR PRINTING COORDINATES 

               Print will occur at element[s] labeled : 


************************************************************************************************************************************
      6  Keyword, label(s) :  END                                                                                      IPASS= 1


                             7 particles have been launched
                     Made  it  to  the  end :      7

************************************************************************************************************************************
 Pgm zgoubi : Execution ended normally, upon keyword END or FIN
   
 File in:   CYCLO_FITisochronism.dat
 File out:  zgoubi.res

  Zgoubi, author's dvlpmnt version.
  Job  started  on  14-04-2021,  at  08:57:47 
  JOB  ENDED  ON    14-04-2021,  AT  08:57:48 

   CPU time, total :    0.40446100000000001     

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
