Test Half-Cell
'OBJET'                                                                                                      1
1249.382414
2
1 1
268.2012107       -15.58027271        0.  0.  0.         1.4000      'o'
1
'PARTICUL'                                                                                                   2
PROTON
 
'CYCLOTRON'                                                                                                  3
0   plot_spiral.H
1   45.0  2.760000E+02    1.0                                                                                 
0. 0. 0.99212277 51.4590015 .5 800. -0.476376328 2.27602517e-03 -4.81955890e-06 3.94715806e-09 0 0 0 0 0 0 0 0                
18.3000E+00  1.   28 -2.0                 g0,  k , G0, G1    Entrance face                                    
8 1.10243581,  3.12915071, -3.14287154,  3.0858059 , -1.43544992, 0.24047436 0. 0. 0.                         
11.0   3.5  35.E-3   0.E-4   3.E-8     1.  1.  1.       omega+, xi0,xi1,xi2,xi3,a,b,c                         
18.3000E+00  1.   28. -2.0                g0,  k , G0, G1    Exit face                                        
8  0.70490173, 4.16013051, -4.33095751,  3.54041582, -1.34727027, 0.18261076  0. 0. 0.                        
-8.5  2.  12.E-3   75.E-6   0.E-6     1.  1.  1.       omega-, xi0s,xi1s,xi2s,xi3s,aexit,bexit,cexit          
0. -1                                       g0,  k             Lateral face                                   
0  0.   0.   0.   0.   0. 0.  0.                        NC, C0...C5, shift                                    
0.  0.   0.    0.    0. 0.                              omega+, xi, 4                                         
2    10.   numerical field/derivatives,  flying mesh size is 0.415/10. (KIRD,RESOL)                           
1.0                                                                                                           
2                                                                                                             
0.  0. 0.  0.                                                                                                 
 
'FIT2'                                                                                                       4
2
1 31 0 [-200,0]
1 35 0 [1,3]
2 1e-8
3.1 1 2 #End 0. 1. 0
3.1 1 3 #End 0. 1. 0
 
'FAISTORE'                                                                                                   5
scanOrbits.fai
1
'FAISCEAU'                                                                                                   6
'REBELOTE'                                                                                                   7
15 0.2 0 1
1
OBJET 30   267.:350.
 
'END'                                                                                                        5

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =       1249.382 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 1

     Particle  properties :
     PROTON
                     Mass          =    938.272        MeV/c2
                     Charge        =   1.602176E-19    C     
                     G  factor     =    1.79285              
                     COM life-time =   1.000000E+99    s     

              Reference  data :
                    mag. rigidity (kG.cm)   :   1249.3824      =p/q, such that dev.=B*L/rigidity
                    mass (MeV/c2)           :   938.27208    
                    momentum (MeV/c)        :   374.55542    
                    energy, total (MeV)     :   1010.2704    
                    energy, kinetic (MeV)   :   71.998311    
                    beta = v/c              :  0.3707477007    
                    gamma                   :   1.076735003    
                    beta*gamma              :  0.3991970265    
                    G*gamma                 :   1.930421496    
                    electric rigidity (MeV) :   138.8655625    =T[eV]*(gamma+1)/gamma, such that dev.=E*L/rigidity
  
 I, AMQ(1,I), AMQ(2,I)/QE, P/Pref, v/c, time, s :
  
     1   9.38272081E+02  1.00000000E+00  1.40000000E+00  4.87856152E-01  0.00000000E+00  0.00000000E+00

************************************************************************************************************************************
      3  Keyword, label(s) :  CYCLOTRON                                                                                IPASS= 1

                    Cyclotron N-tuple,  number  of  dipoles  N :  1

            Total angular extent of the magnet :  45.00 Degres
            Reference geometrical radius R0  :     276.00 cm

     Dipole # 1
            Positionning  angle ACENT :    0.000     degrees
            Positionning  wrt.  R0  :    0.0     cm
            B0 =   51.46     kGauss,       K =  0.50000    

     Entrance  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g_0(1-r**2)**k 1.00
           COEFFICIENTS :  8   1.10244   3.12915  -3.14287   3.08581  -1.43545   0.24047
           Shift  of  EFB  is    0.000     cm

          OMEGA =  11.00 deg.     Spiral  angle  =   3.50 deg.

         Exit  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g_0(1-r**2)**k 1.00
           COEFFICIENTS :  8   0.70490   4.16013  -4.33096   3.54042  -1.34727   0.18261
           Shift  of  EFB  is    0.000     cm

          OMEGA =  -8.50 deg.     Spiral  angle  =   2.00 deg.

         Lateral face :  unused


      Field & deriv. calculation : interpolation
                    3*3-point  interpolation, size of flying mesh :  integration step /   10.00    

                    Integration step :   1.000     cm   (i.e.,   3.6232E-03 rad  at mean radius RM =    276.0    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.4000   268.201   -15.580     0.000     0.000            0.393   268.268    -0.016     0.000     0.000            1

 Cumulative length of optical axis =    53.8200000     m ;  Time  (for ref. rigidity & particle) =   4.842220E-07 s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FIT2                                                                                     IPASS= 1

     Pgm main. FITING=.T., FIT procedure launched.

           variable #            1       IR =            1 ,   ok.
           variable #            1       IP =           31 ,   ok.
           variable #            2       IR =            1 ,   ok.
           variable #            2       IP =           35 ,   ok.
           constraint #            1       IR =            3 ,   ok.
           constraint #            1       I  =            1 ,   ok.
           constraint #            2       IR =            3 ,   ok.
           constraint #            2       I  =            1 ,   ok.

                    FIT  variables  and  constraints  in  good  order,  FIT  will proceed. 
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   1   1    31   -200.       -15.6      -15.967113       0.00      7.740E-04  OBJET      -                    -                   
   1   2    35    1.00        1.40       1.3999806       3.00      1.097E-06  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-08)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     3    0.000000E+00    1.000E+00    4.490981E-05    Infinity CYCLOTRON  -                    -                    0
  3   1   3     3    0.000000E+00    1.000E+00    3.915338E-05    Infinity CYCLOTRON  -                    -                    0
 Fit reached penalty value   3.5499E-09



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =       1249.382 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 1


************************************************************************************************************************************
      3  Keyword, label(s) :  CYCLOTRON                                                                                IPASS= 1

                    Cyclotron N-tuple,  number  of  dipoles  N :  1

            Total angular extent of the magnet :  45.00 Degres
            Reference geometrical radius R0  :     276.00 cm

     Dipole # 1
            Positionning  angle ACENT :    0.000     degrees
            Positionning  wrt.  R0  :    0.0     cm
            B0 =   51.46     kGauss,       K =  0.50000    

     Entrance  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g_0(1-r**2)**k 1.00
           COEFFICIENTS :  8   1.10244   3.12915  -3.14287   3.08581  -1.43545   0.24047
           Shift  of  EFB  is    0.000     cm

          OMEGA =  11.00 deg.     Spiral  angle  =   3.50 deg.

         Exit  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g_0(1-r**2)**k 1.00
           COEFFICIENTS :  8   0.70490   4.16013  -4.33096   3.54042  -1.34727   0.18261
           Shift  of  EFB  is    0.000     cm

          OMEGA =  -8.50 deg.     Spiral  angle  =   2.00 deg.

         Lateral face :  unused


      Field & deriv. calculation : interpolation
                    3*3-point  interpolation, size of flying mesh :  integration step /   10.00    

                    Integration step :   1.000     cm   (i.e.,   3.6232E-03 rad  at mean radius RM =    276.0    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.4000   268.201   -15.967     0.000     0.000            0.393   268.201    -0.016     0.000     0.000            1

 Cumulative length of optical axis =    53.8200000     m ;  Time  (for ref. rigidity & particle) =   4.842220E-07 s 

************************************************************************************************************************************

      4   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      5  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 1


                OPEN FILE zgoubi.fai                                                                      
                FOR PRINTING COORDINATES 

               Print will occur at element[s] labeled : 


************************************************************************************************************************************
      6  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #      5)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.4000   268.201   -15.967     0.000     0.000      0.0000    0.4000  268.201  -15.967    0.000    0.000   2.165766E+02     1
               Time of flight (mus) :  1.48082469E-02 mass (MeV/c2) :   938.272    

************************************************************************************************************************************
      7  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 1


 Pgm rebel. At pass #    1/  16.  In element #    1,  parameter # 30  changed to    2.67000000E+02   (was    2.68201211E+02)

                                -----  REBELOTE  -----

     End of pass #        1 through the optical structure 

                     Total of          1 particles have been launched

     Multiple pass, 
          from element #     1 : OBJET     /label1=                    /label2=                    
                             to  REBELOTE  /label1=                    /label2=                    
     ending at pass #      16 at element #     7 : REBELOTE  /label1=                    /label2=                    


     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   1   1    31   -200.       -16.0      -14.999370       0.00      7.272E-04  OBJET      -                    -                   
   1   2    35    1.00        1.40       1.3912602       3.00      9.840E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-08)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     3    0.000000E+00    1.000E+00    5.803490E-05    Infinity CYCLOTRON  -                    -                    0
  3   1   3     3    0.000000E+00    1.000E+00    1.854033E-05    Infinity CYCLOTRON  -                    -                    0
 Fit reached penalty value   3.7118E-09



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 2

                          MAGNETIC  RIGIDITY =       1249.382 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 2


************************************************************************************************************************************
      3  Keyword, label(s) :  CYCLOTRON                                                                                IPASS= 2

                    Cyclotron N-tuple,  number  of  dipoles  N :  1

            Total angular extent of the magnet :  45.00 Degres
            Reference geometrical radius R0  :     276.00 cm

     Dipole # 1
            Positionning  angle ACENT :    0.000     degrees
            Positionning  wrt.  R0  :    0.0     cm
            B0 =   51.46     kGauss,       K =  0.50000    

     Entrance  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g_0(1-r**2)**k 1.00
           COEFFICIENTS :  8   1.10244   3.12915  -3.14287   3.08581  -1.43545   0.24047
           Shift  of  EFB  is    0.000     cm

          OMEGA =  11.00 deg.     Spiral  angle  =   3.50 deg.

         Exit  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g_0(1-r**2)**k 1.00
           COEFFICIENTS :  8   0.70490   4.16013  -4.33096   3.54042  -1.34727   0.18261
           Shift  of  EFB  is    0.000     cm

          OMEGA =  -8.50 deg.     Spiral  angle  =   2.00 deg.

         Lateral face :  unused


      Field & deriv. calculation : interpolation
                    3*3-point  interpolation, size of flying mesh :  integration step /   10.00    

                    Integration step :   1.000     cm   (i.e.,   3.6232E-03 rad  at mean radius RM =    276.0    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.3913   267.000   -14.999     0.000     0.000            0.393   267.000    -0.015     0.000     0.000            1

 Cumulative length of optical axis =    53.8200000     m ;  Time  (for ref. rigidity & particle) =   4.842220E-07 s 

************************************************************************************************************************************

      4   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      5  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 2


************************************************************************************************************************************
      6  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 2

0                                             TRACE DU FAISCEAU
                                           (follows element #      5)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.3913   267.000   -14.999     0.000     0.000      0.0000    0.3913  267.000  -14.999    0.000    0.000   2.156099E+02     1
               Time of flight (mus) :  1.48126102E-02 mass (MeV/c2) :   938.272    

************************************************************************************************************************************
      7  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 2


 Pgm rebel. At pass #    2/  16.  In element #    1,  parameter # 30  changed to    2.72928571E+02   (was    2.67000000E+02)

                                -----  REBELOTE  -----

     End of pass #        2 through the optical structure 

                     Total of          2 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   1   1    31   -200.       -15.0      -19.822266       0.00      6.664E-04  OBJET      -                    -                   
   1   2    35    1.00        1.39       1.4345981       3.00      7.949E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-08)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     3    0.000000E+00    1.000E+00    7.141441E-05    Infinity CYCLOTRON  -                    -                    0
  3   1   3     3    0.000000E+00    1.000E+00    4.697962E-05    Infinity CYCLOTRON  -                    -                    0
 Fit reached penalty value   7.3071E-09



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 3

                          MAGNETIC  RIGIDITY =       1249.382 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 3


************************************************************************************************************************************
      3  Keyword, label(s) :  CYCLOTRON                                                                                IPASS= 3

                    Cyclotron N-tuple,  number  of  dipoles  N :  1

            Total angular extent of the magnet :  45.00 Degres
            Reference geometrical radius R0  :     276.00 cm

     Dipole # 1
            Positionning  angle ACENT :    0.000     degrees
            Positionning  wrt.  R0  :    0.0     cm
            B0 =   51.46     kGauss,       K =  0.50000    

     Entrance  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g_0(1-r**2)**k 1.00
           COEFFICIENTS :  8   1.10244   3.12915  -3.14287   3.08581  -1.43545   0.24047
           Shift  of  EFB  is    0.000     cm

          OMEGA =  11.00 deg.     Spiral  angle  =   3.50 deg.

         Exit  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g_0(1-r**2)**k 1.00
           COEFFICIENTS :  8   0.70490   4.16013  -4.33096   3.54042  -1.34727   0.18261
           Shift  of  EFB  is    0.000     cm

          OMEGA =  -8.50 deg.     Spiral  angle  =   2.00 deg.

         Lateral face :  unused


      Field & deriv. calculation : interpolation
                    3*3-point  interpolation, size of flying mesh :  integration step /   10.00    

                    Integration step :   1.000     cm   (i.e.,   3.6232E-03 rad  at mean radius RM =    276.0    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.4346   272.929   -19.822     0.000     0.000            0.393   272.929    -0.020     0.000     0.000            1

 Cumulative length of optical axis =    53.8200000     m ;  Time  (for ref. rigidity & particle) =   4.842220E-07 s 

************************************************************************************************************************************

      4   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      5  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 3


************************************************************************************************************************************
      6  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 3

0                                             TRACE DU FAISCEAU
                                           (follows element #      5)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.4346   272.929   -19.822     0.000     0.000      0.0000    0.4346  272.929  -19.822    0.000    0.000   2.203789E+02     1
               Time of flight (mus) :  1.47919726E-02 mass (MeV/c2) :   938.272    

************************************************************************************************************************************
      7  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 3


 Pgm rebel. At pass #    3/  16.  In element #    1,  parameter # 30  changed to    2.78857143E+02   (was    2.72928571E+02)

                                -----  REBELOTE  -----

     End of pass #        3 through the optical structure 

                     Total of          3 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   1   1    31   -200.       -19.8      -24.771124       0.00      1.224E-03  OBJET      -                    -                   
   1   2    35    1.00        1.43       1.4786278       3.00      1.508E-06  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-08)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     3    0.000000E+00    1.000E+00    1.614526E-05    Infinity CYCLOTRON  -                    -                    0
  3   1   3     3    0.000000E+00    1.000E+00    9.341271E-05    Infinity CYCLOTRON  -                    -                    0
 Fit reached penalty value   8.9866E-09



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 4

                          MAGNETIC  RIGIDITY =       1249.382 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 4


************************************************************************************************************************************
      3  Keyword, label(s) :  CYCLOTRON                                                                                IPASS= 4

                    Cyclotron N-tuple,  number  of  dipoles  N :  1

            Total angular extent of the magnet :  45.00 Degres
            Reference geometrical radius R0  :     276.00 cm

     Dipole # 1
            Positionning  angle ACENT :    0.000     degrees
            Positionning  wrt.  R0  :    0.0     cm
            B0 =   51.46     kGauss,       K =  0.50000    

     Entrance  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g_0(1-r**2)**k 1.00
           COEFFICIENTS :  8   1.10244   3.12915  -3.14287   3.08581  -1.43545   0.24047
           Shift  of  EFB  is    0.000     cm

          OMEGA =  11.00 deg.     Spiral  angle  =   3.50 deg.

         Exit  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g_0(1-r**2)**k 1.00
           COEFFICIENTS :  8   0.70490   4.16013  -4.33096   3.54042  -1.34727   0.18261
           Shift  of  EFB  is    0.000     cm

          OMEGA =  -8.50 deg.     Spiral  angle  =   2.00 deg.

         Lateral face :  unused


      Field & deriv. calculation : interpolation
                    3*3-point  interpolation, size of flying mesh :  integration step /   10.00    

                    Integration step :   1.000     cm   (i.e.,   3.6232E-03 rad  at mean radius RM =    276.0    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.4786   278.857   -24.771     0.000     0.000            0.393   278.857    -0.025     0.000     0.000            1

 Cumulative length of optical axis =    53.8200000     m ;  Time  (for ref. rigidity & particle) =   4.842220E-07 s 

************************************************************************************************************************************

      4   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      5  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 4


************************************************************************************************************************************
      6  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 4

0                                             TRACE DU FAISCEAU
                                           (follows element #      5)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.4786   278.857   -24.771     0.000     0.000      0.0000    0.4786  278.857  -24.771    0.000    0.000   2.251416E+02     1
               Time of flight (mus) :  1.47740674E-02 mass (MeV/c2) :   938.272    

************************************************************************************************************************************
      7  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 4


 Pgm rebel. At pass #    4/  16.  In element #    1,  parameter # 30  changed to    2.84785714E+02   (was    2.78857143E+02)

                                -----  REBELOTE  -----

     End of pass #        4 through the optical structure 

                     Total of          4 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   1   1    31   -200.       -24.8      -29.852228       0.00      1.134E-03  OBJET      -                    -                   
   1   2    35    1.00        1.48       1.5232955       3.00      1.661E-06  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-08)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     3    0.000000E+00    1.000E+00    3.684872E-05    Infinity CYCLOTRON  -                    -                    0
  3   1   3     3    0.000000E+00    1.000E+00    7.021596E-05    Infinity CYCLOTRON  -                    -                    0
 Fit reached penalty value   6.2881E-09



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 5

                          MAGNETIC  RIGIDITY =       1249.382 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 5


************************************************************************************************************************************
      3  Keyword, label(s) :  CYCLOTRON                                                                                IPASS= 5

                    Cyclotron N-tuple,  number  of  dipoles  N :  1

            Total angular extent of the magnet :  45.00 Degres
            Reference geometrical radius R0  :     276.00 cm

     Dipole # 1
            Positionning  angle ACENT :    0.000     degrees
            Positionning  wrt.  R0  :    0.0     cm
            B0 =   51.46     kGauss,       K =  0.50000    

     Entrance  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g_0(1-r**2)**k 1.00
           COEFFICIENTS :  8   1.10244   3.12915  -3.14287   3.08581  -1.43545   0.24047
           Shift  of  EFB  is    0.000     cm

          OMEGA =  11.00 deg.     Spiral  angle  =   3.50 deg.

         Exit  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g_0(1-r**2)**k 1.00
           COEFFICIENTS :  8   0.70490   4.16013  -4.33096   3.54042  -1.34727   0.18261
           Shift  of  EFB  is    0.000     cm

          OMEGA =  -8.50 deg.     Spiral  angle  =   2.00 deg.

         Lateral face :  unused


      Field & deriv. calculation : interpolation
                    3*3-point  interpolation, size of flying mesh :  integration step /   10.00    

                    Integration step :   1.000     cm   (i.e.,   3.6232E-03 rad  at mean radius RM =    276.0    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.5233   284.786   -29.852     0.000     0.000            0.393   284.786    -0.030     0.000     0.000            1

 Cumulative length of optical axis =    53.8200000     m ;  Time  (for ref. rigidity & particle) =   4.842220E-07 s 

************************************************************************************************************************************

      4   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      5  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 5


************************************************************************************************************************************
      6  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 5

0                                             TRACE DU FAISCEAU
                                           (follows element #      5)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.5233   284.786   -29.852     0.000     0.000      0.0000    0.5233  284.786  -29.852    0.000    0.000   2.298975E+02     1
               Time of flight (mus) :  1.47593573E-02 mass (MeV/c2) :   938.272    

************************************************************************************************************************************
      7  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 5


 Pgm rebel. At pass #    5/  16.  In element #    1,  parameter # 30  changed to    2.90714286E+02   (was    2.84785714E+02)

                                -----  REBELOTE  -----

     End of pass #        5 through the optical structure 

                     Total of          5 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   1   1    31   -200.       -29.9      -35.070097       0.00      9.292E-04  OBJET      -                    -                   
   1   2    35    1.00        1.52       1.5685684       3.00      1.538E-06  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-08)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     3    0.000000E+00    1.000E+00    7.622258E-05    Infinity CYCLOTRON  -                    -                    0
  3   1   3     3    0.000000E+00    1.000E+00    4.234484E-06    Infinity CYCLOTRON  -                    -                    0
 Fit reached penalty value   5.8278E-09



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 6

                          MAGNETIC  RIGIDITY =       1249.382 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 6


************************************************************************************************************************************
      3  Keyword, label(s) :  CYCLOTRON                                                                                IPASS= 6

                    Cyclotron N-tuple,  number  of  dipoles  N :  1

            Total angular extent of the magnet :  45.00 Degres
            Reference geometrical radius R0  :     276.00 cm

     Dipole # 1
            Positionning  angle ACENT :    0.000     degrees
            Positionning  wrt.  R0  :    0.0     cm
            B0 =   51.46     kGauss,       K =  0.50000    

     Entrance  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g_0(1-r**2)**k 1.00
           COEFFICIENTS :  8   1.10244   3.12915  -3.14287   3.08581  -1.43545   0.24047
           Shift  of  EFB  is    0.000     cm

          OMEGA =  11.00 deg.     Spiral  angle  =   3.50 deg.

         Exit  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g_0(1-r**2)**k 1.00
           COEFFICIENTS :  8   0.70490   4.16013  -4.33096   3.54042  -1.34727   0.18261
           Shift  of  EFB  is    0.000     cm

          OMEGA =  -8.50 deg.     Spiral  angle  =   2.00 deg.

         Lateral face :  unused


      Field & deriv. calculation : interpolation
                    3*3-point  interpolation, size of flying mesh :  integration step /   10.00    

                    Integration step :   1.000     cm   (i.e.,   3.6232E-03 rad  at mean radius RM =    276.0    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.5686   290.714   -35.070     0.000     0.000            0.393   290.714    -0.035     0.000     0.000            1

 Cumulative length of optical axis =    53.8200000     m ;  Time  (for ref. rigidity & particle) =   4.842220E-07 s 

************************************************************************************************************************************

      4   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      5  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 6


************************************************************************************************************************************
      6  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 6

0                                             TRACE DU FAISCEAU
                                           (follows element #      5)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.5686   290.714   -35.070     0.000     0.000      0.0000    0.5686  290.714  -35.070    0.000    0.000   2.346463E+02     1
               Time of flight (mus) :  1.47480741E-02 mass (MeV/c2) :   938.272    

************************************************************************************************************************************
      7  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 6


 Pgm rebel. At pass #    6/  16.  In element #    1,  parameter # 30  changed to    2.96642857E+02   (was    2.90714286E+02)

                                -----  REBELOTE  -----

     End of pass #        6 through the optical structure 

                     Total of          6 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   1   1    31   -200.       -35.1      -40.428480       0.00      6.508E-04  OBJET      -                    -                   
   1   2    35    1.00        1.57       1.6144001       3.00      1.007E-06  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-08)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     3    0.000000E+00    1.000E+00    5.458293E-05    Infinity CYCLOTRON  -                    -                    0
  3   1   3     3    0.000000E+00    1.000E+00    2.584945E-05    Infinity CYCLOTRON  -                    -                    0
 Fit reached penalty value   3.6475E-09



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 7

                          MAGNETIC  RIGIDITY =       1249.382 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 7


************************************************************************************************************************************
      3  Keyword, label(s) :  CYCLOTRON                                                                                IPASS= 7

                    Cyclotron N-tuple,  number  of  dipoles  N :  1

            Total angular extent of the magnet :  45.00 Degres
            Reference geometrical radius R0  :     276.00 cm

     Dipole # 1
            Positionning  angle ACENT :    0.000     degrees
            Positionning  wrt.  R0  :    0.0     cm
            B0 =   51.46     kGauss,       K =  0.50000    

     Entrance  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g_0(1-r**2)**k 1.00
           COEFFICIENTS :  8   1.10244   3.12915  -3.14287   3.08581  -1.43545   0.24047
           Shift  of  EFB  is    0.000     cm

          OMEGA =  11.00 deg.     Spiral  angle  =   3.50 deg.

         Exit  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g_0(1-r**2)**k 1.00
           COEFFICIENTS :  8   0.70490   4.16013  -4.33096   3.54042  -1.34727   0.18261
           Shift  of  EFB  is    0.000     cm

          OMEGA =  -8.50 deg.     Spiral  angle  =   2.00 deg.

         Lateral face :  unused


      Field & deriv. calculation : interpolation
                    3*3-point  interpolation, size of flying mesh :  integration step /   10.00    

                    Integration step :   1.000     cm   (i.e.,   3.6232E-03 rad  at mean radius RM =    276.0    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.6144   296.643   -40.428     0.000     0.000            0.393   296.643    -0.040     0.000     0.000            1

 Cumulative length of optical axis =    53.8200000     m ;  Time  (for ref. rigidity & particle) =   4.842220E-07 s 

************************************************************************************************************************************

      4   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      5  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 7


************************************************************************************************************************************
      6  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 7

0                                             TRACE DU FAISCEAU
                                           (follows element #      5)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.6144   296.643   -40.428     0.000     0.000      0.0000    0.6144  296.643  -40.428    0.000    0.000   2.393872E+02     1
               Time of flight (mus) :  1.47404628E-02 mass (MeV/c2) :   938.272    

************************************************************************************************************************************
      7  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 7


 Pgm rebel. At pass #    7/  16.  In element #    1,  parameter # 30  changed to    3.02571429E+02   (was    2.96642857E+02)

                                -----  REBELOTE  -----

     End of pass #        7 through the optical structure 

                     Total of          7 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   1   1    31   -200.       -40.4      -45.920969       0.00      9.272E-04  OBJET      -                    -                   
   1   2    35    1.00        1.61       1.6608092       3.00      1.721E-06  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-08)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     3    0.000000E+00    1.000E+00    6.392055E-05    Infinity CYCLOTRON  -                    -                    0
  3   1   3     3    0.000000E+00    1.000E+00    9.650625E-06    Infinity CYCLOTRON  -                    -                    0
 Fit reached penalty value   4.1790E-09



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 8

                          MAGNETIC  RIGIDITY =       1249.382 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 8


************************************************************************************************************************************
      3  Keyword, label(s) :  CYCLOTRON                                                                                IPASS= 8

                    Cyclotron N-tuple,  number  of  dipoles  N :  1

            Total angular extent of the magnet :  45.00 Degres
            Reference geometrical radius R0  :     276.00 cm

     Dipole # 1
            Positionning  angle ACENT :    0.000     degrees
            Positionning  wrt.  R0  :    0.0     cm
            B0 =   51.46     kGauss,       K =  0.50000    

     Entrance  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g_0(1-r**2)**k 1.00
           COEFFICIENTS :  8   1.10244   3.12915  -3.14287   3.08581  -1.43545   0.24047
           Shift  of  EFB  is    0.000     cm

          OMEGA =  11.00 deg.     Spiral  angle  =   3.50 deg.

         Exit  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g_0(1-r**2)**k 1.00
           COEFFICIENTS :  8   0.70490   4.16013  -4.33096   3.54042  -1.34727   0.18261
           Shift  of  EFB  is    0.000     cm

          OMEGA =  -8.50 deg.     Spiral  angle  =   2.00 deg.

         Lateral face :  unused


      Field & deriv. calculation : interpolation
                    3*3-point  interpolation, size of flying mesh :  integration step /   10.00    

                    Integration step :   1.000     cm   (i.e.,   3.6232E-03 rad  at mean radius RM =    276.0    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.6608   302.571   -45.921     0.000     0.000            0.393   302.571    -0.046     0.000     0.000            1

 Cumulative length of optical axis =    53.8200000     m ;  Time  (for ref. rigidity & particle) =   4.842220E-07 s 

************************************************************************************************************************************

      4   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      5  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 8


************************************************************************************************************************************
      6  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 8

0                                             TRACE DU FAISCEAU
                                           (follows element #      5)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.6608   302.571   -45.921     0.000     0.000      0.0000    0.6608  302.571  -45.921    0.000    0.000   2.441192E+02     1
               Time of flight (mus) :  1.47363003E-02 mass (MeV/c2) :   938.272    

************************************************************************************************************************************
      7  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 8


 Pgm rebel. At pass #    8/  16.  In element #    1,  parameter # 30  changed to    3.08500000E+02   (was    3.02571429E+02)

                                -----  REBELOTE  -----

     End of pass #        8 through the optical structure 

                     Total of          8 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   1   1    31   -200.       -45.9      -51.543990       0.00      5.987E-04  OBJET      -                    -                   
   1   2    35    1.00        1.66       1.7077817       3.00      1.639E-06  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-08)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     3    0.000000E+00    1.000E+00    3.151973E-05    Infinity CYCLOTRON  -                    -                    0
  3   1   3     3    0.000000E+00    1.000E+00    7.515263E-05    Infinity CYCLOTRON  -                    -                    0
 Fit reached penalty value   6.6414E-09



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 9

                          MAGNETIC  RIGIDITY =       1249.382 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 9


************************************************************************************************************************************
      3  Keyword, label(s) :  CYCLOTRON                                                                                IPASS= 9

                    Cyclotron N-tuple,  number  of  dipoles  N :  1

            Total angular extent of the magnet :  45.00 Degres
            Reference geometrical radius R0  :     276.00 cm

     Dipole # 1
            Positionning  angle ACENT :    0.000     degrees
            Positionning  wrt.  R0  :    0.0     cm
            B0 =   51.46     kGauss,       K =  0.50000    

     Entrance  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g_0(1-r**2)**k 1.00
           COEFFICIENTS :  8   1.10244   3.12915  -3.14287   3.08581  -1.43545   0.24047
           Shift  of  EFB  is    0.000     cm

          OMEGA =  11.00 deg.     Spiral  angle  =   3.50 deg.

         Exit  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g_0(1-r**2)**k 1.00
           COEFFICIENTS :  8   0.70490   4.16013  -4.33096   3.54042  -1.34727   0.18261
           Shift  of  EFB  is    0.000     cm

          OMEGA =  -8.50 deg.     Spiral  angle  =   2.00 deg.

         Lateral face :  unused


      Field & deriv. calculation : interpolation
                    3*3-point  interpolation, size of flying mesh :  integration step /   10.00    

                    Integration step :   1.000     cm   (i.e.,   3.6232E-03 rad  at mean radius RM =    276.0    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.7078   308.500   -51.544     0.000     0.000            0.393   308.500    -0.052     0.000     0.000            1

 Cumulative length of optical axis =    53.8200000     m ;  Time  (for ref. rigidity & particle) =   4.842220E-07 s 

************************************************************************************************************************************

      4   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      5  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 9


************************************************************************************************************************************
      6  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 9

0                                             TRACE DU FAISCEAU
                                           (follows element #      5)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.7078   308.500   -51.544     0.000     0.000      0.0000    0.7078  308.500  -51.544    0.000    0.000   2.488411E+02     1
               Time of flight (mus) :  1.47355580E-02 mass (MeV/c2) :   938.272    

************************************************************************************************************************************
      7  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 9


 Pgm rebel. At pass #    9/  16.  In element #    1,  parameter # 30  changed to    3.14428571E+02   (was    3.08500000E+02)

                                -----  REBELOTE  -----

     End of pass #        9 through the optical structure 

                     Total of          9 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   1   1    31   -200.       -51.5      -57.288077       0.00      5.038E-04  OBJET      -                    -                   
   1   2    35    1.00        1.71       1.7553570       3.00      9.836E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-08)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     3    0.000000E+00    1.000E+00    2.319768E-05    Infinity CYCLOTRON  -                    -                    0
  3   1   3     3    0.000000E+00    1.000E+00    3.830161E-05    Infinity CYCLOTRON  -                    -                    0
 Fit reached penalty value   2.0051E-09



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 10

                          MAGNETIC  RIGIDITY =       1249.382 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 10


************************************************************************************************************************************
      3  Keyword, label(s) :  CYCLOTRON                                                                                IPASS= 10

                    Cyclotron N-tuple,  number  of  dipoles  N :  1

            Total angular extent of the magnet :  45.00 Degres
            Reference geometrical radius R0  :     276.00 cm

     Dipole # 1
            Positionning  angle ACENT :    0.000     degrees
            Positionning  wrt.  R0  :    0.0     cm
            B0 =   51.46     kGauss,       K =  0.50000    

     Entrance  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g_0(1-r**2)**k 1.00
           COEFFICIENTS :  8   1.10244   3.12915  -3.14287   3.08581  -1.43545   0.24047
           Shift  of  EFB  is    0.000     cm

          OMEGA =  11.00 deg.     Spiral  angle  =   3.50 deg.

         Exit  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g_0(1-r**2)**k 1.00
           COEFFICIENTS :  8   0.70490   4.16013  -4.33096   3.54042  -1.34727   0.18261
           Shift  of  EFB  is    0.000     cm

          OMEGA =  -8.50 deg.     Spiral  angle  =   2.00 deg.

         Lateral face :  unused


      Field & deriv. calculation : interpolation
                    3*3-point  interpolation, size of flying mesh :  integration step /   10.00    

                    Integration step :   1.000     cm   (i.e.,   3.6232E-03 rad  at mean radius RM =    276.0    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.7554   314.429   -57.288     0.000     0.000            0.393   314.429    -0.057     0.000     0.000            1

 Cumulative length of optical axis =    53.8200000     m ;  Time  (for ref. rigidity & particle) =   4.842220E-07 s 

************************************************************************************************************************************

      4   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      5  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 10


************************************************************************************************************************************
      6  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 10

0                                             TRACE DU FAISCEAU
                                           (follows element #      5)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.7554   314.429   -57.288     0.000     0.000      0.0000    0.7554  314.429  -57.288    0.000    0.000   2.535516E+02     1
               Time of flight (mus) :  1.47378998E-02 mass (MeV/c2) :   938.272    

************************************************************************************************************************************
      7  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 10


 Pgm rebel. At pass #   10/  16.  In element #    1,  parameter # 30  changed to    3.20357143E+02   (was    3.14428571E+02)

                                -----  REBELOTE  -----

     End of pass #       10 through the optical structure 

                     Total of         10 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   1   1    31   -200.       -57.3      -63.150348       0.00      6.243E-04  OBJET      -                    -                   
   1   2    35    1.00        1.76       1.8035671       3.00      1.265E-06  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-08)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     3    0.000000E+00    1.000E+00    3.255287E-05    Infinity CYCLOTRON  -                    -                    0
  3   1   3     3    0.000000E+00    1.000E+00    1.552343E-05    Infinity CYCLOTRON  -                    -                    0
 Fit reached penalty value   1.3007E-09



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 11

                          MAGNETIC  RIGIDITY =       1249.382 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 11


************************************************************************************************************************************
      3  Keyword, label(s) :  CYCLOTRON                                                                                IPASS= 11

                    Cyclotron N-tuple,  number  of  dipoles  N :  1

            Total angular extent of the magnet :  45.00 Degres
            Reference geometrical radius R0  :     276.00 cm

     Dipole # 1
            Positionning  angle ACENT :    0.000     degrees
            Positionning  wrt.  R0  :    0.0     cm
            B0 =   51.46     kGauss,       K =  0.50000    

     Entrance  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g_0(1-r**2)**k 1.00
           COEFFICIENTS :  8   1.10244   3.12915  -3.14287   3.08581  -1.43545   0.24047
           Shift  of  EFB  is    0.000     cm

          OMEGA =  11.00 deg.     Spiral  angle  =   3.50 deg.

         Exit  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g_0(1-r**2)**k 1.00
           COEFFICIENTS :  8   0.70490   4.16013  -4.33096   3.54042  -1.34727   0.18261
           Shift  of  EFB  is    0.000     cm

          OMEGA =  -8.50 deg.     Spiral  angle  =   2.00 deg.

         Lateral face :  unused


      Field & deriv. calculation : interpolation
                    3*3-point  interpolation, size of flying mesh :  integration step /   10.00    

                    Integration step :   1.000     cm   (i.e.,   3.6232E-03 rad  at mean radius RM =    276.0    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.8036   320.357   -63.150     0.000     0.000            0.393   320.357    -0.063     0.000     0.000            1

 Cumulative length of optical axis =    53.8200000     m ;  Time  (for ref. rigidity & particle) =   4.842220E-07 s 

************************************************************************************************************************************

      4   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      5  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 11


************************************************************************************************************************************
      6  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 11

0                                             TRACE DU FAISCEAU
                                           (follows element #      5)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.8036   320.357   -63.150     0.000     0.000      0.0000    0.8036  320.357  -63.150    0.000    0.000   2.582495E+02     1
               Time of flight (mus) :  1.47430675E-02 mass (MeV/c2) :   938.272    

************************************************************************************************************************************
      7  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 11


 Pgm rebel. At pass #   11/  16.  In element #    1,  parameter # 30  changed to    3.26285714E+02   (was    3.20357143E+02)

                                -----  REBELOTE  -----

     End of pass #       11 through the optical structure 

                     Total of         11 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   1   1    31   -200.       -63.2      -69.127475       0.00      5.765E-04  OBJET      -                    -                   
   1   2    35    1.00        1.80       1.8524712       3.00      1.616E-06  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-08)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     3    0.000000E+00    1.000E+00    3.027204E-05    Infinity CYCLOTRON  -                    -                    0
  3   1   3     3    0.000000E+00    1.000E+00    4.377339E-05    Infinity CYCLOTRON  -                    -                    0
 Fit reached penalty value   2.8325E-09



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 12

                          MAGNETIC  RIGIDITY =       1249.382 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 12


************************************************************************************************************************************
      3  Keyword, label(s) :  CYCLOTRON                                                                                IPASS= 12

                    Cyclotron N-tuple,  number  of  dipoles  N :  1

            Total angular extent of the magnet :  45.00 Degres
            Reference geometrical radius R0  :     276.00 cm

     Dipole # 1
            Positionning  angle ACENT :    0.000     degrees
            Positionning  wrt.  R0  :    0.0     cm
            B0 =   51.46     kGauss,       K =  0.50000    

     Entrance  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g_0(1-r**2)**k 1.00
           COEFFICIENTS :  8   1.10244   3.12915  -3.14287   3.08581  -1.43545   0.24047
           Shift  of  EFB  is    0.000     cm

          OMEGA =  11.00 deg.     Spiral  angle  =   3.50 deg.

         Exit  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g_0(1-r**2)**k 1.00
           COEFFICIENTS :  8   0.70490   4.16013  -4.33096   3.54042  -1.34727   0.18261
           Shift  of  EFB  is    0.000     cm

          OMEGA =  -8.50 deg.     Spiral  angle  =   2.00 deg.

         Lateral face :  unused


      Field & deriv. calculation : interpolation
                    3*3-point  interpolation, size of flying mesh :  integration step /   10.00    

                    Integration step :   1.000     cm   (i.e.,   3.6232E-03 rad  at mean radius RM =    276.0    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.8525   326.286   -69.127     0.000     0.000            0.393   326.286    -0.069     0.000     0.000            1

 Cumulative length of optical axis =    53.8200000     m ;  Time  (for ref. rigidity & particle) =   4.842220E-07 s 

************************************************************************************************************************************

      4   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      5  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 12


************************************************************************************************************************************
      6  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 12

0                                             TRACE DU FAISCEAU
                                           (follows element #      5)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.8525   326.286   -69.127     0.000     0.000      0.0000    0.8525  326.286  -69.128    0.000    0.000   2.629338E+02     1
               Time of flight (mus) :  1.47507041E-02 mass (MeV/c2) :   938.272    

************************************************************************************************************************************
      7  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 12


 Pgm rebel. At pass #   12/  16.  In element #    1,  parameter # 30  changed to    3.32214286E+02   (was    3.26285714E+02)

                                -----  REBELOTE  -----

     End of pass #       12 through the optical structure 

                     Total of         12 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   1   1    31   -200.       -69.1      -75.219302       0.00      7.335E-04  OBJET      -                    -                   
   1   2    35    1.00        1.85       1.9020605       3.00      1.676E-06  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-08)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     3    0.000000E+00    1.000E+00    2.711822E-05    Infinity CYCLOTRON  -                    -                    0
  3   1   3     3    0.000000E+00    1.000E+00    4.388017E-05    Infinity CYCLOTRON  -                    -                    0
 Fit reached penalty value   2.6609E-09



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 13

                          MAGNETIC  RIGIDITY =       1249.382 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 13


************************************************************************************************************************************
      3  Keyword, label(s) :  CYCLOTRON                                                                                IPASS= 13

                    Cyclotron N-tuple,  number  of  dipoles  N :  1

            Total angular extent of the magnet :  45.00 Degres
            Reference geometrical radius R0  :     276.00 cm

     Dipole # 1
            Positionning  angle ACENT :    0.000     degrees
            Positionning  wrt.  R0  :    0.0     cm
            B0 =   51.46     kGauss,       K =  0.50000    

     Entrance  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g_0(1-r**2)**k 1.00
           COEFFICIENTS :  8   1.10244   3.12915  -3.14287   3.08581  -1.43545   0.24047
           Shift  of  EFB  is    0.000     cm

          OMEGA =  11.00 deg.     Spiral  angle  =   3.50 deg.

         Exit  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g_0(1-r**2)**k 1.00
           COEFFICIENTS :  8   0.70490   4.16013  -4.33096   3.54042  -1.34727   0.18261
           Shift  of  EFB  is    0.000     cm

          OMEGA =  -8.50 deg.     Spiral  angle  =   2.00 deg.

         Lateral face :  unused


      Field & deriv. calculation : interpolation
                    3*3-point  interpolation, size of flying mesh :  integration step /   10.00    

                    Integration step :   1.000     cm   (i.e.,   3.6232E-03 rad  at mean radius RM =    276.0    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.9021   332.214   -75.219     0.000     0.000            0.393   332.214    -0.075     0.000     0.000            1

 Cumulative length of optical axis =    53.8200000     m ;  Time  (for ref. rigidity & particle) =   4.842220E-07 s 

************************************************************************************************************************************

      4   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      5  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 13


************************************************************************************************************************************
      6  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 13

0                                             TRACE DU FAISCEAU
                                           (follows element #      5)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.9021   332.214   -75.219     0.000     0.000      0.0000    0.9021  332.214  -75.219    0.000    0.000   2.676037E+02     1
               Time of flight (mus) :  1.47608429E-02 mass (MeV/c2) :   938.272    

************************************************************************************************************************************
      7  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 13


 Pgm rebel. At pass #   13/  16.  In element #    1,  parameter # 30  changed to    3.38142857E+02   (was    3.32214286E+02)

                                -----  REBELOTE  -----

     End of pass #       13 through the optical structure 

                     Total of         13 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   1   1    31   -200.       -75.2      -81.424338       0.00      7.021E-04  OBJET      -                    -                   
   1   2    35    1.00        1.90       1.9524852       3.00      1.717E-06  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-08)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     3    0.000000E+00    1.000E+00    7.069968E-05    Infinity CYCLOTRON  -                    -                    0
  3   1   3     3    0.000000E+00    1.000E+00    3.984696E-05    Infinity CYCLOTRON  -                    -                    0
 Fit reached penalty value   6.5862E-09



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 14

                          MAGNETIC  RIGIDITY =       1249.382 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 14


************************************************************************************************************************************
      3  Keyword, label(s) :  CYCLOTRON                                                                                IPASS= 14

                    Cyclotron N-tuple,  number  of  dipoles  N :  1

            Total angular extent of the magnet :  45.00 Degres
            Reference geometrical radius R0  :     276.00 cm

     Dipole # 1
            Positionning  angle ACENT :    0.000     degrees
            Positionning  wrt.  R0  :    0.0     cm
            B0 =   51.46     kGauss,       K =  0.50000    

     Entrance  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g_0(1-r**2)**k 1.00
           COEFFICIENTS :  8   1.10244   3.12915  -3.14287   3.08581  -1.43545   0.24047
           Shift  of  EFB  is    0.000     cm

          OMEGA =  11.00 deg.     Spiral  angle  =   3.50 deg.

         Exit  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g_0(1-r**2)**k 1.00
           COEFFICIENTS :  8   0.70490   4.16013  -4.33096   3.54042  -1.34727   0.18261
           Shift  of  EFB  is    0.000     cm

          OMEGA =  -8.50 deg.     Spiral  angle  =   2.00 deg.

         Lateral face :  unused


      Field & deriv. calculation : interpolation
                    3*3-point  interpolation, size of flying mesh :  integration step /   10.00    

                    Integration step :   1.000     cm   (i.e.,   3.6232E-03 rad  at mean radius RM =    276.0    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.9525   338.143   -81.424     0.000     0.000            0.393   338.143    -0.081     0.000     0.000            1

 Cumulative length of optical axis =    53.8200000     m ;  Time  (for ref. rigidity & particle) =   4.842220E-07 s 

************************************************************************************************************************************

      4   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      5  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 14


************************************************************************************************************************************
      6  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 14

0                                             TRACE DU FAISCEAU
                                           (follows element #      5)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.9525   338.143   -81.424     0.000     0.000      0.0000    0.9525  338.143  -81.424    0.000    0.000   2.722579E+02     1
               Time of flight (mus) :  1.47727340E-02 mass (MeV/c2) :   938.272    

************************************************************************************************************************************
      7  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 14


 Pgm rebel. At pass #   14/  16.  In element #    1,  parameter # 30  changed to    3.44071429E+02   (was    3.38142857E+02)

                                -----  REBELOTE  -----

     End of pass #       14 through the optical structure 

                     Total of         14 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   1   1    31   -200.       -81.4      -87.747943       0.00      1.251E-03  OBJET      -                    -                   
   1   2    35    1.00        1.95       2.0037893       3.00      2.931E-06  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-08)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     3    0.000000E+00    1.000E+00    2.952339E-05    Infinity CYCLOTRON  -                    -                    0
  3   1   3     3    0.000000E+00    1.000E+00    8.383249E-05    Infinity CYCLOTRON  -                    -                    0
 Fit reached penalty value   7.8995E-09



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 15

                          MAGNETIC  RIGIDITY =       1249.382 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 15


************************************************************************************************************************************
      3  Keyword, label(s) :  CYCLOTRON                                                                                IPASS= 15

                    Cyclotron N-tuple,  number  of  dipoles  N :  1

            Total angular extent of the magnet :  45.00 Degres
            Reference geometrical radius R0  :     276.00 cm

     Dipole # 1
            Positionning  angle ACENT :    0.000     degrees
            Positionning  wrt.  R0  :    0.0     cm
            B0 =   51.46     kGauss,       K =  0.50000    

     Entrance  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g_0(1-r**2)**k 1.00
           COEFFICIENTS :  8   1.10244   3.12915  -3.14287   3.08581  -1.43545   0.24047
           Shift  of  EFB  is    0.000     cm

          OMEGA =  11.00 deg.     Spiral  angle  =   3.50 deg.

         Exit  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g_0(1-r**2)**k 1.00
           COEFFICIENTS :  8   0.70490   4.16013  -4.33096   3.54042  -1.34727   0.18261
           Shift  of  EFB  is    0.000     cm

          OMEGA =  -8.50 deg.     Spiral  angle  =   2.00 deg.

         Lateral face :  unused


      Field & deriv. calculation : interpolation
                    3*3-point  interpolation, size of flying mesh :  integration step /   10.00    

                    Integration step :   1.000     cm   (i.e.,   3.6232E-03 rad  at mean radius RM =    276.0    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.0038   344.071   -87.748     0.000     0.000            0.393   344.071    -0.088     0.000     0.000            1

 Cumulative length of optical axis =    53.8200000     m ;  Time  (for ref. rigidity & particle) =   4.842220E-07 s 

************************************************************************************************************************************

      4   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      5  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 15


************************************************************************************************************************************
      6  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 15

0                                             TRACE DU FAISCEAU
                                           (follows element #      5)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   2.0038   344.071   -87.748     0.000     0.000      0.0000    1.0038  344.071  -87.748    0.000    0.000   2.768952E+02     1
               Time of flight (mus) :  1.47862324E-02 mass (MeV/c2) :   938.272    

************************************************************************************************************************************
      7  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 15


 Pgm rebel. At pass #   15/  16.  In element #    1,  parameter # 30  changed to    3.50000000E+02   (was    3.44071429E+02)

                                -----  REBELOTE  -----

     End of pass #       15 through the optical structure 

                     Total of         15 particles have been launched


      Next  pass  is  #    16 and  last  pass  through  the  optical  structure


     WRITE statements to zgoubi.res are re-established from now on.

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 16

                          MAGNETIC  RIGIDITY =       1249.382 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 16


************************************************************************************************************************************
      3  Keyword, label(s) :  CYCLOTRON                                                                                IPASS= 16

                    Cyclotron N-tuple,  number  of  dipoles  N :  1

            Total angular extent of the magnet :  45.00 Degres
            Reference geometrical radius R0  :     276.00 cm

     Dipole # 1
            Positionning  angle ACENT :    0.000     degrees
            Positionning  wrt.  R0  :    0.0     cm
            B0 =   51.46     kGauss,       K =  0.50000    

     Entrance  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g_0(1-r**2)**k 1.00
           COEFFICIENTS :  8   1.10244   3.12915  -3.14287   3.08581  -1.43545   0.24047
           Shift  of  EFB  is    0.000     cm

          OMEGA =  11.00 deg.     Spiral  angle  =   3.50 deg.

         Exit  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g_0(1-r**2)**k 1.00
           COEFFICIENTS :  8   0.70490   4.16013  -4.33096   3.54042  -1.34727   0.18261
           Shift  of  EFB  is    0.000     cm

          OMEGA =  -8.50 deg.     Spiral  angle  =   2.00 deg.

         Lateral face :  unused


      Field & deriv. calculation : interpolation
                    3*3-point  interpolation, size of flying mesh :  integration step /   10.00    

                    Integration step :   1.000     cm   (i.e.,   3.6232E-03 rad  at mean radius RM =    276.0    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.0038   350.000   -87.748     0.000     0.000            0.393   349.560    -0.110     0.000     0.000            1

 Cumulative length of optical axis =    53.8200000     m ;  Time  (for ref. rigidity & particle) =   9.684440E-07 s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FIT2                                                                                     IPASS= 16


     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 16

                          MAGNETIC  RIGIDITY =       1249.382 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 16


************************************************************************************************************************************
      3  Keyword, label(s) :  CYCLOTRON                                                                                IPASS= 16

                    Cyclotron N-tuple,  number  of  dipoles  N :  1

            Total angular extent of the magnet :  45.00 Degres
            Reference geometrical radius R0  :     276.00 cm

     Dipole # 1
            Positionning  angle ACENT :    0.000     degrees
            Positionning  wrt.  R0  :    0.0     cm
            B0 =   51.46     kGauss,       K =  0.50000    

     Entrance  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g_0(1-r**2)**k 1.00
           COEFFICIENTS :  8   1.10244   3.12915  -3.14287   3.08581  -1.43545   0.24047
           Shift  of  EFB  is    0.000     cm

          OMEGA =  11.00 deg.     Spiral  angle  =   3.50 deg.

         Exit  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g_0(1-r**2)**k 1.00
           COEFFICIENTS :  8   0.70490   4.16013  -4.33096   3.54042  -1.34727   0.18261
           Shift  of  EFB  is    0.000     cm

          OMEGA =  -8.50 deg.     Spiral  angle  =   2.00 deg.

         Lateral face :  unused


      Field & deriv. calculation : interpolation
                    3*3-point  interpolation, size of flying mesh :  integration step /   10.00    

                    Integration step :   1.000     cm   (i.e.,   3.6232E-03 rad  at mean radius RM =    276.0    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.0038   350.000   -87.748     0.000     0.000            0.393   349.560    -0.110     0.000     0.000            1

 Cumulative length of optical axis =    53.8200000     m ;  Time  (for ref. rigidity & particle) =   4.842220E-07 s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FIT2                                                                                     IPASS= 16

     Pgm main. FITING=.T., FIT procedure launched.

           variable #            1       IR =            1 ,   ok.
           variable #            1       IP =           31 ,   ok.
           variable #            2       IR =            1 ,   ok.
           variable #            2       IP =           35 ,   ok.
           constraint #            1       IR =            3 ,   ok.
           constraint #            1       I  =            1 ,   ok.
           constraint #            2       IR =            3 ,   ok.
           constraint #            2       I  =            1 ,   ok.

                    FIT  variables  and  constraints  in  good  order,  FIT  will proceed. 
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   1   1    31   -200.       -87.7      -94.188285       0.00      4.001E-04  OBJET      -                    -                   
   1   2    35    1.00        2.00       2.0560461       3.00      9.386E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-08)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     3    0.000000E+00    1.000E+00    1.365298E-05    Infinity CYCLOTRON  -                    -                    0
  3   1   3     3    0.000000E+00    1.000E+00    9.275235E-05    Infinity CYCLOTRON  -                    -                    0
 Fit reached penalty value   8.7894E-09



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 16

                          MAGNETIC  RIGIDITY =       1249.382 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 16


************************************************************************************************************************************
      3  Keyword, label(s) :  CYCLOTRON                                                                                IPASS= 16

                    Cyclotron N-tuple,  number  of  dipoles  N :  1

            Total angular extent of the magnet :  45.00 Degres
            Reference geometrical radius R0  :     276.00 cm

     Dipole # 1
            Positionning  angle ACENT :    0.000     degrees
            Positionning  wrt.  R0  :    0.0     cm
            B0 =   51.46     kGauss,       K =  0.50000    

     Entrance  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g_0(1-r**2)**k 1.00
           COEFFICIENTS :  8   1.10244   3.12915  -3.14287   3.08581  -1.43545   0.24047
           Shift  of  EFB  is    0.000     cm

          OMEGA =  11.00 deg.     Spiral  angle  =   3.50 deg.

         Exit  EFB
          Fringe  field  :  gap at R0 is  18.30 cm,    type is :  g_0(1-r**2)**k 1.00
           COEFFICIENTS :  8   0.70490   4.16013  -4.33096   3.54042  -1.34727   0.18261
           Shift  of  EFB  is    0.000     cm

          OMEGA =  -8.50 deg.     Spiral  angle  =   2.00 deg.

         Lateral face :  unused


      Field & deriv. calculation : interpolation
                    3*3-point  interpolation, size of flying mesh :  integration step /   10.00    

                    Integration step :   1.000     cm   (i.e.,   3.6232E-03 rad  at mean radius RM =    276.0    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.0560   350.000   -94.188     0.000     0.000            0.393   350.000    -0.094     0.000     0.000            1

 Cumulative length of optical axis =    53.8200000     m ;  Time  (for ref. rigidity & particle) =   4.842220E-07 s 

************************************************************************************************************************************

      4   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      5  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 16


************************************************************************************************************************************
      6  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 16

0                                             TRACE DU FAISCEAU
                                           (follows element #      5)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   2.0560   350.000   -94.188     0.000     0.000      0.0000    1.0560  350.000  -94.188    0.000    0.000   2.815146E+02     1
               Time of flight (mus) :  1.48010855E-02 mass (MeV/c2) :   938.272    

************************************************************************************************************************************
      7  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 16


                         >>>>  End  of  'REBELOTE'  procedure  <<<<

      There  has  been         16  passes  through  the  optical  structure 

                     Total of         16 particles have been launched

************************************************************************************************************************************
      8  Keyword, label(s) :  END                                                                                      IPASS= 17


************************************************************************************************************************************
 Pgm zgoubi : Execution ended normally, upon keyword END or FIN
   
 File in:   test.res
 File out:  zgoubi.res

  Zgoubi, author's dvlpmnt version.
  Job  started  on  02-09-2020,  at  14:25:02 
  JOB  ENDED  ON    02-09-2020,  AT  14:25:30 

   CPU time, total :     27.838754000000002     

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
