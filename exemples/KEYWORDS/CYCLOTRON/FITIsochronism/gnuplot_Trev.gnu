# gnuplot_Trev.gnu 
set xtics nomirror; set xlabel "Y0 [cm]"; set ytics nomirror; set ylabel "Y"
set x2tics; set x2label "T0 [cm]"; set y2tics; set y2label "T"
set k t l
colY0=3;  colY=10;  colT0=4;  colT=11
plot \
'scanOrbits.fai' u colY0:colY w lp lt 3 dt 7 lw 2 pt 4 lc 1 tit "Initial dT(R)" ,\
'FITted.fai' u colY0:colY w lp lt 3 dt 7 lw 2 pt 6 lc 2 tit "Fitted dT(R)" , 'FITted.fai' u colT0:colT axes x2y2 w lp lt 3 dt 7 lw 2 pt 7 lc 2 tit "Fitted dT(R)" 

pause 1

unset x2tics; unset y2tics; unset x2label; unset y2label
set y2tics ; set y2label "dT/T  (zoomed)"
set xlabel "R_0" ; set ylabel "dT/T"

set key t r 
T300s = 1.4736300330107593E-02
T300F = 1.474408E-02  

plot \
'scanOrbits.fai' u colY0:(($15-T300s)/T300s) w lp lt 3 dt 7 lw 2 pt 6 lc 1 tit "Initial dT(R)" ,\
'FITted.fai' u colY0:(($15-T300F)/T300F) w lp lt 3 dt 7 lw 2 pt 4 lc 3  tit "Fitted dT(R)" ,\
'FITted.fai' u colY0:(($15-T300F)/T300F) axes x1y2 w lp lt 1 lw 2 pt 4 lc 4 tit "'' zoomed"

 set terminal postscript eps blacktext color enh size 9.3cm,6cm "Times-Roman" 12
 set output "gnuplot_Trev.eps"
 replot
 set terminal X11
 unset output

pause 88
