
set key maxcol 1
set key c l

#set logscale y 

set tit "Spin component(s) vs. turn number  \n from zgoubi.fai"  font "roman, 14" 

set xtics nomirror font "roman, 14" 
set x2tics nomirror font "roman, 14" 
set ytics mirror font "roman, 14" 

set xlabel 'turn' font "roman, 22" 
set x2label 'G{/Symbol g}' font "roman, 22" 
set ylabel 'Y' font "roman, 22" 

# ELECTRON : 
 am = 0.511
 G = 1.1596e-3
# PROTON :
#am = 938.27203e6
#G = 1.79284735

m2cm = 100.
MeV2eV = 1e6
c = 2.99792458e8

mxIT = 10
i = 1

plot  \
   "dist.fai" u ($26== i ? $38 : 1/0):($10) w lp pt 2*i+1 ps .4 lc i  tit "OBJ 3.0, Y".i ,\
   "track.fai" u ($26== i ? $38 : 1/0):($10) w lp pt 2*i+1 ps .8 lc i+1  tit "OBJ 3,Y".i

     set terminal postscript eps blacktext color  enh  "Times-Roman" 12  
       set output "gnuplot_zgoubi.fai_turn-Y.eps"  
       replot  
       set terminal X11  
       unset output  

 
pause 1
exit

