Find periodic coordinates of 4 FFAG orbits (i.e., 4 different momenta) through single cell, and
! compute 4 transport matrices of a 42-cell ring, together with their corresponfing beam matrices.
'OBJET'                                                                                                      1
+5.171103865921708e+01                                                               ! 15.5 MeV electron.
5          ! Four reference momenta. The following initial values yield FIT penalty value of 1.05e-02,
1e-3 0.001 1e-3 0.001 0.001 0.0001                           ! the FIT that follows improves it to ~1e-4.
0. 0. 0. 0. 0. 1.
 
! 'INCLUDE'                                                                                               
! Include_SegmentStart : quad_matrix.link[#S_quadSection,*:#E_quadSec(n_inc, depth :  1 2)
'MARKER'    #S_quadSection                                                                                   2
'DRIFT'     DD1                                                                                              3
10.00
 
! 'INCLUDE'                                                                                               
! Include_SegmentStart : QF.inc[QF_S,*:QF_E,*]                       (n_inc, depth :  1 3)
'MARKER'   QF_S                                                                                              4
'CHANGREF'                                                                                                   5
0 -0.1111304250932937 1.47531616464
'MARKER' dum  .plt                                                                                           6
 
! 'INCLUDE'                                                                                               
! Include_SegmentStart : QF_MULT.inc[QFMULT_S,*:QFMULT_E,*]          (n_inc, depth :  1 4)
'MARKER'   QFMULT_S                                                                                          7
 
'MULTIPOL'  QF                                                                                               8
2
6 2.208947037893755 -5.072134730870179e-01 1.6409348483 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0 0
0 1 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0 0
0 1 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0.1
1 0 0 0
 
! Include_SegmentEnd : QF_MULT.inc                                   (had n_inc, depth :  1 4)
'MARKER'   QFMULT_E                                                                                          9
 
'MARKER' dum  .plt                                                                                          10
'CHANGREF'                                                                                                  11
0 0.1111304250932937 1.47531616464
! Include_SegmentEnd : QF.inc                                        (had n_inc, depth :  1 3)
'MARKER'   QF_E                                                                                             12
 
'DRIFT'     DQ                                                                                              13
4.00
 
! Include_SegmentEnd : quad_matrix.link                              (had n_inc, depth :  1 2)
'MARKER'    #E_quadSection                                                                                  14
 
'MATRIX'                                                                                                    15
1 11
 
'END'

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET       BEG                   .plt                                                   IPASS= 1

                          MAGNETIC  RIGIDITY =         51.711 kG*cm

                                         CALCUL  DES  TRAJECTOIRES

                              OBJET  (5)  FORME  DE     11 POINTS 



                                Y (cm)         T (mrd)       Z (cm)        P (mrd)       S (cm)        dp/p 
               Sampling :          0.10E-02      0.10E-02      0.10E-02      0.10E-02      0.10E-02      0.1000E-03
  Reference trajectry #      1 :    0.0           0.0           0.0           0.0           0.0           1.000    

************************************************************************************************************************************
      2  Keyword, label(s) :  MARKER      #S_quadSection                                                               IPASS= 1


************************************************************************************************************************************
      3  Keyword, label(s) :  DRIFT       DD1                                                                          IPASS= 1


                              Drift,  length =    10.00000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  1.0000000E+01  0.00000E+00

 Cumulative length of optical axis =   0.100000000     m   ;  Time  (for reference rigidity & particle) =   2.019126E-08 s 

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      QF_S                                                                         IPASS= 1


************************************************************************************************************************************
      5  Keyword, label(s) :  CHANGREF                                                                                 IPASS= 1


 CHANGE  OF  REFERENCE  FRAME,   XC =     0.000 cm , YC =    -0.111  cm ,   A =     1.47532 deg  (  0.025749 rad)

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  1.111673E-01 -2.574912E+01  0.000000E+00  0.000000E+00  9.9971379E+00 -9.54708E-08

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      dum                   .plt                                                   IPASS= 1


                OPEN FILE zgoubi.plt                                                                      
                FOR PRINTING TRAJECTORIES


************************************************************************************************************************************
      7  Keyword, label(s) :  MARKER      QFMULT_S                                                                     IPASS= 1


************************************************************************************************************************************
      8  Keyword, label(s) :  MULTIPOL    QF                                                                           IPASS= 1


     zgoubi.plt                                                                      
      already open...

      -----  MULTIPOLE   : 
                Length  of  element  =    6.0000000      cm
                Bore  radius      RO =    2.2089      cm                              B/RO^IM/Brho (strength)
               B-DIPOLE      = -5.0721347E-01 kG   (i.e.,  -5.0721347E-01 * SCAL)    -9.8086112E-01
               B-QUADRUPOLE  =  1.6409348E+00 kG   (i.e.,   1.6409348E+00 * SCAL)     1.4365567E+02
               B-SEXTUPOLE   =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   2.2089E+00

  ***  Warning : sharp edge model, vertical wedge focusing approximated with first order kick. FINT at entrance =    0.000    

  ***  Warning : sharp edge model,  vertical wedge focusing approximated with first order kick. FINT at exit =    0.000    

                    Integration step :  0.1000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000            6.000     0.111     0.026     0.000     0.000            1
  A    1  1.0000     0.001     0.000     0.000     0.000            6.000     0.112     0.026     0.000     0.000            2
  A    1  1.0000    -0.001     0.000     0.000     0.000            6.000     0.110     0.026     0.000     0.000            3
  A    1  1.0000     0.000     0.001     0.000     0.000            6.000     0.111     0.026     0.000     0.000            4
  A    1  1.0000     0.000    -0.001     0.000     0.000            6.000     0.111     0.026     0.000     0.000            5
  A    1  1.0000     0.000     0.000     0.001     0.000            6.000     0.111     0.026     0.001     0.000            6
  A    1  1.0000     0.000     0.000    -0.001     0.000            6.000     0.111     0.026    -0.001    -0.000            7
  A    1  1.0000     0.000     0.000     0.000     0.001            6.000     0.111     0.026     0.000     0.000            8
  A    1  1.0000     0.000     0.000     0.000    -0.001            6.000     0.111     0.026    -0.000    -0.000            9
  A    1  1.0001     0.000     0.000     0.000     0.000            6.000     0.111     0.026     0.000     0.000           10
  A    1  0.9999     0.000     0.000     0.000     0.000            6.000     0.111     0.026     0.000     0.000           11


                CONDITIONS  DE  MAXWELL  (      671.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.0000000     RAD


 Cumulative length of optical axis =   0.160000000     m ;  Time  (for ref. rigidity & particle) =   3.230601E-08 s 

************************************************************************************************************************************
      9  Keyword, label(s) :  MARKER      QFMULT_E                                                                     IPASS= 1


************************************************************************************************************************************
     10  Keyword, label(s) :  MARKER      dum                   .plt                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...

************************************************************************************************************************************
     11  Keyword, label(s) :  CHANGREF                                                                                 IPASS= 1


 CHANGE  OF  REFERENCE  FRAME,   XC =     0.000 cm , YC =     0.111  cm ,   A =     1.47532 deg  (  0.025749 rad)

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  8.338416E-05  1.484501E-02  0.000000E+00  0.000000E+00  1.5997811E+01  2.00065E-04

************************************************************************************************************************************
     12  Keyword, label(s) :  MARKER      QF_E                                                                         IPASS= 1


************************************************************************************************************************************
     13  Keyword, label(s) :  DRIFT       DQ                                                                           IPASS= 1


                              Drift,  length =     4.00000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  1.427642E-04  1.484501E-02  0.000000E+00  0.000000E+00  1.9997811E+01  2.00065E-04

 Cumulative length of optical axis =   0.200000000     m   ;  Time  (for reference rigidity & particle) =   4.038252E-08 s 

************************************************************************************************************************************
     14  Keyword, label(s) :  MARKER      #E_quadSection                                                               IPASS= 1


************************************************************************************************************************************
     15  Keyword, label(s) :  MATRIX                                                                                   IPASS= 1


  Reference, before change of frame (part #     1)  : 
   0.00000000E+00   1.42764189E-04   1.48450064E-02   0.00000000E+00   0.00000000E+00   1.99978108E+01   2.00065434E-04

           Frame for MATRIX calculation moved by :
            XC =    0.000 cm , YC =    0.000 cm ,   A =  0.00085 deg  ( = 0.000015 rad )


  Reference, after change of frame (part #     1)  : 
   0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00   1.99978108E+01   2.00065434E-04

  Reference particle (#     1), path length :   19.997811     cm  relative momentum :    1.00000    


                  TRANSFER  MATRIX  ORDRE  1  (MKSA units)

          0.436274        0.128647         0.00000         0.00000         0.00000       -3.368890E-03
          -7.90048       -3.752847E-02     0.00000         0.00000         0.00000       -4.721391E-02
           0.00000         0.00000         1.64169        0.280180         0.00000         0.00000    
           0.00000         0.00000         9.32807         2.20111         0.00000         0.00000    
         -4.721406E-02   -6.200352E-03     0.00000         0.00000         1.00000        2.564122E-05
           0.00000         0.00000         0.00000         0.00000         0.00000         1.00000    

          DetY-1 =      -0.0000000524,    DetZ-1 =      -0.0000000489

          R12=0 at    3.428     m,        R34=0 at  -0.1273     m

      First order symplectic conditions (expected values = 0) :
        -5.2418E-08   -4.8929E-08     0.000         0.000         0.000         0.000    


                TWISS  parameters,  periodicity  of   1  is  assumed 
                                   - UNCOUPLED -

       Beam  matrix  (beta/-alpha/-alpha/gamma) and  periodic  dispersion  (MKSA units)

           0.131283    -0.241755     0.000000     0.000000     0.000000    -0.005976
          -0.241755     8.062345     0.000000     0.000000     0.000000     0.000000
           0.000000     0.000000     0.000000     0.000000     0.000000     0.000000
           0.000000     0.000000     0.000000     0.000000     0.000000     0.000000
           0.000000     0.000000     0.000000     0.000000     0.000000     0.000000
           0.000000     0.000000     0.000000     0.000000     0.000000     0.000000

                                   Betatron  tunes

                    NU_Y =  0.21805477         NU_Z = undefined      

************************************************************************************************************************************
     16  Keyword, label(s) :  END                                                                                      IPASS= 1


                            11 particles have been launched
                     Made  it  to  the  end :     11

************************************************************************************************************************************
 Pgm zgoubi : Execution ended normally, upon keyword END or FIN
   
 File in:   cascadeOfSingleINCs.dat
 File out:  zgoubi.res

  Zgoubi, author's dvlpmnt version.
  Job  started  on  29-02-2020,  at  19:34:47 
  JOB  ENDED  ON    29-02-2020,  AT  19:34:47 

   CPU time, total :     9.9462999999999996E-002
