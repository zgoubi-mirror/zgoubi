
 set title "SVD Orbits, from SVDOC > zgoubi.SVDOrbits.out"    font "roman,22"   # offset 0,+.7    

 set xlabel "s [m]"        font "roman,22"   # offset +4,-.5 rotate by +20  
 set ylabel "x [cm]"             font "roman,22"   #offset -0,-1 rotate by -20 
 set y2label "y [cm]"             font "roman,22"   #offset -0,-1 rotate by -20 

 set xtics  font "roman,16" mirror
 set ytics  font "roman,16" nomirror      #offset 0,-.6
 set y2tics  font "roman,16" nomirror      #offset 0,-.6

set key b c maxrows 2 width 4
set key font "roman, 14"  samplen 1  

cm2mm = 1.

plot \
 "zgoubi.SVDOrbits.out"  u ($8):($4==$6+4 && $3==1 ? $2 *cm2mm  : 1/0) w p pt 4 ps 1.4 lw 2 lc rgb "red" tit "H, non-corrected" ,\
 "zgoubi.SVDOrbits.out"  u ($8):($4==$6+5 && $3==1 ? $2 *cm2mm  : 1/0) w p pt 5 ps .9 lw 2 lc rgb "green" tit "H,  corrected" ,\
 "zgoubi.SVDOrbits.out"  u ($8):($4==$6+4 && $3==2 ? $2 *cm2mm  : 1/0) axes x1y2 w p pt 6 ps 1.4 lw 2 lc rgb "blue" tit "V, non-corrected" ,\
 "zgoubi.SVDOrbits.out"  u ($8):($4==$6+5 && $3==2 ? $2 *cm2mm  : 1/0) axes x1y2 w p pt 7 ps .9 lw 2 lc rgb "brown" tit "V,  corrected" 
 
 set terminal postscript eps blacktext color enh 
 set output "gnuplot_SVDOrbits.eps"
 replot
 set terminal X11
 unset output

pause 1
exit
