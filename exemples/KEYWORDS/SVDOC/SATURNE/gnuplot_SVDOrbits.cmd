
 set title "SVD Orbits, from SVDOC->zgoubi.SVDOrbits.out"    font "roman,22"   # offset 0,+.7    

 set xlabel "PU number"        font "roman,22"   # offset +4,-.5 rotate by +20  
 set ylabel "x, y [cm]"             font "roman,22"   #offset -0,-1 rotate by -20 
 set y2label "x, y [cm]"             font "roman,22"   #offset -0,-1 rotate by -20 

 set xtics  font "roman,16" nomirror
 set ytics  font "roman,16" nomirror      #offset 0,-.6
 set y2tics  font "roman,16" nomirror      #offset 0,-.6

set key b c maxrows 2 width 4
set key font "roman, 14"  samplen 1  

plot \
 "zgoubi.SVDOrbits.out"  u 1:($4==$6+4 && $3==1 ? $2 : 1/0) w lp pt 4 ps 1.5 lc rgb "red" tit "H, non-corrected" ,\
 "zgoubi.SVDOrbits.out"  u 1:($4==$6+4 && $3==2 ? $2 : 1/0) w lp pt 6 ps 1.5 lc rgb "blue" tit "V, non-corrected" ,\
 "zgoubi.SVDOrbits.out"  u 1:($4==$6+5 && $3==1 ? $2 : 1/0) w lp pt 5 ps 2 lw 2 lc rgb "green" tit "H,  corrected" ,\
 "zgoubi.SVDOrbits.out"  u 1:($4==$6+5 && $3==2 ? $2 : 1/0) w lp pt 7 ps 2 lw 2 lc rgb "brown" tit "V,  corrected" 

 set terminal postscript eps blacktext color enh 
 set output "gnuplot_SVDOrbits.eps"
 replot
 set terminal X11
 unset output

pause 1
exit
