
set xlabel "s [cm]"; set xtics mirror; unset ytics; set k t l font "roman, 10"; zero = 0.
# in zgoubi.plt, test $19: particle number; test $51: FIT stage number; $14: s; $33: SX
set arrow from 50,0 to 50,-130 nohead; set arrow from 100,0 to 100,-130 nohead; set arrow from 150,0 to 150,-130 nohead
vLen=57
plot [:170] [0:-vLen*1.2]  \
"zgoubi.plt" u ($19==1 & $51==0 ? $14 : 1/0):(zero):($33*vLen):($34*vLen)  w  vector lw 1. lc rgb "red" ,\
"zgoubi.plt" u ($19==1 & $51==1 ? $14 : 1/0):(zero):($33*vLen):($34*vLen)  w  vector lw 3 lc rgb "blue" 

set terminal postscript eps blacktext color enh    # size 8.3cm,4cm "Times-Roman" 12 
 set output "gnuplot_spin.eps" 
 replot 
 set terminal X11 
 unset output 

pause 2
quit
