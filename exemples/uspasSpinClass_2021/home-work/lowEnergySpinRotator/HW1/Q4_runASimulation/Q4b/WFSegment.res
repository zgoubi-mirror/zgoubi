WFSegment.
! In addition to the comments on the right, use the Users' Guide.
'OBJET'                                                                                                      1
2.31147953865                                                         ! Rigidity of a 350 keV electron.
2
3  1                                    ! 3 electrons are needed for spin matrix computation by SPNPRT.
0.  0. 0. 0. 0. 1. 'o'                  ! Initial coordinates all zero, except for relative regidity 1.
0.  0. 0. 0. 0. 1. 'o'
0.  0. 0. 0. 0. 1. 'o'
1 1 1
'PARTICUL'                                                                                                   2
POSITRON
 
'SPNTRK'                         ! Set the initial spin components, proper for spin matrix computation.      3
4                                                                 !
1. 0. 0.                                                                                ! Initial SX=1.
0. 1. 0.                                                                                ! Initial SY=1.
0. 0. 1.                                                                                ! Initial SZ=1.
 
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./WFSegment_EBTheor.inc[#S_WFSegment,*:#E_WF(n_inc, depth :  1 2)
'MARKER'  #S_WFSegment                                                                                       4
'WIENFILT'                                                                                                   5
20              ! Logs particle data, at each integration step, in zgoubi.plt [see the guide, Sec.8.3].
0.5  -982938.94   0.0040737813 1                          ! Theoretical E (V/m) and B (T) field values.
0. 0. 0.                             ! Hard-edge entrance face.  Substitue 20. 5. 5. for fringe fields.
0.2401  1.8639  -0.5572  0.3904 0. 0.          ! Fringe field coefficients in Enge model, ignored here.
0.2401  1.8639  -0.5572  0.3904 0. 0.
0. 0. 0.                             ! Hard-edge exit     face.  Substitue 20. 5. 5. for fringe fields.
0.2401  1.8639  -0.5572  0.3904 0. 0.
0.2401  1.8639  -0.5572  0.3904 0. 0.
0.4                                                                            ! Integration step size.
1. 0. 0. 0.                                                              ! Positionning of the element.
! Include_SegmentEnd : ./WFSegment_EBTheor.inc                       (had n_inc, depth :  1 2)
'MARKER'  #E_WFSegment                                                                                       6
 
! This FIT procedure ans its data are commented.
! 'FIT2'                     ! The numbering of optical elements to be used is to be rad in zgoubi.res.
! 2
! 5 11 0 .8                                                     ! Keyword 5: WIENFILT; paremeter 11: E.
! 5 12 0 .8                                                     ! Keyword 5: WIENFILT; paremeter 12: B.
! 5 1E-15                                                          ! Six constraints; penalty is 1e-15:
! 3    1 2 #End *** 1. 0                                                                  ! Y=0 at end.
! 3    1 3 #End *** 1. 0                                                                  ! T=0 at end.
! 10.2 1 1 #End *** 1. 0                                                        ! 30 deg spin rotation,
! 10   1 4 #End *** 1. 0                                                  ! |S|=1, for all 3 particles.
! 10   2 4 #End *** 1. 0
! 10   3 4 #End *** 1. 0
 
'FAISCEAU'                                                  ! Log trajectory coordinates in zgoubi.res.      7
'SPNPRT'  MATRIX                                  ! Log spin coordinates and spin matrix in zgoubi.res.      8
'SYSTEM'                                                                                                     9
1
gnuplot < ./gnuplot_spin.gnu                    ! Produce a graph of spin motion along the Wien filter.
'END'

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 1

     Particle  properties :
     POSITRON
                     Mass          =   0.510999        MeV/c2
                     Charge        =   1.602176E-19    C     
                     G  factor     =   1.159652E-03          
                     COM life-time =   1.000000E+99    s     

              Reference  data :
                    mag. rigidity (kG.cm)   :   2.3114795      =p/q, such that dev.=B*L/rigidity
                    mass (MeV/c2)           :  0.51099895    
                    momentum (MeV/c)        :  0.69296413    
                    energy, total (MeV)     :  0.86099896    
                    energy, kinetic (MeV)   :  0.35000002    
                    beta = v/c              :  0.8048373615    
                    gamma                   :   1.684932950    
                    beta*gamma              :   1.356096989    
                    G*gamma                 :  1.9539361699E-03
                    m / G                   :   440.6484587    
                    electric rigidity (MeV) :  0.5577234240    =T[eV]*(gamma+1)/gamma, such that dev.=E*L/rigidity
  
 I, AMQ(1,I), AMQ(2,I)/QE, P/Pref, v/c, time, s :
  
     1   5.10998946E-01  1.00000000E+00  1.00000000E+00  8.04837361E-01  0.00000000E+00  0.00000000E+00
     2   5.10998946E-01  1.00000000E+00  1.00000000E+00  8.04837361E-01  0.00000000E+00  0.00000000E+00
     3   5.10998946E-01  1.00000000E+00  1.00000000E+00  8.04837361E-01  0.00000000E+00  0.00000000E+00

************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 1


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     2.3114795     kG*cm
                               beta      =    0.80483736    
                               gamma     =     1.6849329    
                               G*gamma   =    0.19539362E-02
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment                                                                 IPASS= 1


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 1


                OPEN FILE zgoubi.plt                                                                      
                FOR PRINTING TRAJECTORIES

Entrance hard edge is to be implemented
Exit hard edge is to be implemented

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.82939E+05 V/m
                               B                       =  4.07378E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80484    

                FACE  D'ENTREE
                DX =   0.000,  LAMBDA_E, LAMBDA_B =   0.000  0.000 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )

                FACE  DE  SORTIE
                DX =   0.000,  LAMBDA_E, LAMBDA_B =   0.000  0.000 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )

  ***  Warning : hard-edge fringe model entails vertical wedge focusing simulated with
                  first order kick  ***

                    Integration step :  0.4000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           50.000    -0.000    -0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           50.000    -0.000    -0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           50.000    -0.000    -0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      378.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment                                                                 IPASS= 1


************************************************************************************************************************************
      7  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #      6)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o   1   1.0000     0.000     0.000     0.000     0.000      0.0000  -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o   1   1.0000     0.000     0.000     0.000     0.000      0.0000  -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o   1   1.0000     0.000     0.000     0.000     0.000      0.0000  -0.0000   -0.000   -0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    

************************************************************************************************************************************
      8  Keyword, label(s) :  SPNPRT      MATRIX                                                                       IPASS= 1



                    ----------------------------------------------------------------------------------
                    Momentum  group  #1 (D=   1.000000E+00) ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455342   0.122008   0.333333   0.577350   0.001954  20.000038  14.142162


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   |Si,Sf|   (Z,Sf_yz) (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_yz : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.866025 -0.500001 -0.000000  1.000000      1.6849   30.000   90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500001  0.866025  0.000000  1.000000      1.6849   30.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.000000  1.000000  1.000000      1.6849    0.000   45.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6602E-01  8.6602E-01 -5.0000E-01 -5.0000E-01 -2.4299E-17 -2.4299E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00     1   1
  5.0000E-01  5.0000E-01  8.6602E-01  8.6602E-01  3.4799E-18  3.4799E-18  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00     2   1
  1.9333E-17  1.9333E-17 -1.5078E-17 -1.5078E-17  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00     3   1


                  Spin transfer matrix, momentum group # 1 :

          0.866025        0.500001        1.933328E-17
         -0.500001        0.866025       -1.507774E-17
         -2.429937E-17    3.479933E-18     1.00000    

     Determinant =       1.0000000000
     Trace =       2.7320498250;   spin precession acos((trace-1)/2) =      30.0000562972 deg
     Precession axis :  ( 0.0000,  0.0000, -1.0000)  ->  angle to (X,Y) plane, to X axis :   -90.0000,    66.9592 deg
     Spin precession/2pi (or Qs, fractional) :    8.3333E-02

************************************************************************************************************************************
      9  Keyword, label(s) :  SYSTEM                                                                                   IPASS= 1

     Number  of  commands :   1,  as  follows : 

 gnuplot < ./gnuplot_spin.gnu

************************************************************************************************************************************
     10  Keyword, label(s) :  END                                                                                      IPASS= 1


                             3 particles have been launched
                     Made  it  to  the  end :      3

************************************************************************************************************************************
 Pgm zgoubi : Execution ended normally, upon keyword END or FIN
   
 File in:   WFSegment.dat
 File out:  zgoubi.res

  Zgoubi, author's Revision: 1590.
  Job  started  on  03-02-2022,  at  10:25:01 
  JOB  ENDED  ON    03-02-2022,  AT  10:25:03 

   CPU time, total :     1.3329000000000001E-002
