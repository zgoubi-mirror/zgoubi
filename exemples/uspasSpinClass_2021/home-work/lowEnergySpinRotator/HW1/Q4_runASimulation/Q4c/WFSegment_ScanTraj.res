WFSegment.
'OBJET'                                                                                                      1
2.31147953865                                                         ! Rigidity of a 350 keV electron.
2
3  1                    ! 3 electrons, same initial coordinates, for spin matrix computation by SPNPRT.
0.  0. 0. 0. 0. 1. 'o'
0.  0. 0. 0. 0. 1. 'o'
0.  0. 0. 0. 0. 1. 'o'
1 1 1
'PARTICUL'                                                                                                   2
POSITRON
 
'SPNTRK'                                         ! Allows getting the rotation of all 3 spin components      3
4                                ! (they are computed independently), for matrix computation by SPNPRT.
1. 0. 0.
0. 1. 0.
0. 0. 1.
 
'MARKER'  #S_WFSegment_EBth                                                                                  4
'WIENFILT'                                                                                                   5
20
0.5 -982939 0.00407378  1                                 ! Theoretical E (V/m) and B (T) field values.
0. 0. 0.    ! Hard-edge entrance face, XE=lambdaE_e=lambdaB_e=0. Substitue 20. 5. 5. for fringe fields.
0.2401  1.8639  -0.5572  0.3904 0. 0.                        ! Fringe field coefficients in Enge model.
0.2401  1.8639  -0.5572  0.3904 0. 0.
0. 0. 0.        ! Hard-edge exit face, XS=lambdaE_s=lambdaB_s=0. Substitue 20. 5. 5. for fringe fields.
0.2401  1.8639  -0.5572  0.3904 0. 0.
0.2401  1.8639  -0.5572  0.3904 0. 0.
.2                ! Integration step size. It may be given different values, depending on the exercise.
1. 0. 0. 0.
'MARKER'  #E_WFSegment_EBth                                                                                  6
 
'FAISCEAU'                                              ! Log local particle coordinates in zgoubi.res.      7
'FAISTORE'                                                                                                   8
zgoubi.fai                                              ! Log local particle coordinates in zgoubi.res.
1
'REBELOTE'                                                                                                   9
400 0.1 0 1                        ! Repeat the previous sequence, 400 times, and prior to each repeat,
1                                                                      ! change value of one parameter,
WIENFILT 80 0.01:10.                           ! namely, number 80 (integration step size) in WIENFILT.
 
'SYSTEM'                                                                                                    10
2
gnuplot <./gnuplot_scanTraj.gnu
okular ./gnuplot_scanTraj.eps &
'END'

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 1

     Particle  properties :
     POSITRON
                     Mass          =   0.510999        MeV/c2
                     Charge        =   1.602176E-19    C     
                     G  factor     =   1.159652E-03          
                     COM life-time =   1.000000E+99    s     

              Reference  data :
                    mag. rigidity (kG.cm)   :   2.3114795      =p/q, such that dev.=B*L/rigidity
                    mass (MeV/c2)           :  0.51099895    
                    momentum (MeV/c)        :  0.69296413    
                    energy, total (MeV)     :  0.86099896    
                    energy, kinetic (MeV)   :  0.35000002    
                    beta = v/c              :  0.8048373615    
                    gamma                   :   1.684932950    
                    beta*gamma              :   1.356096989    
                    G*gamma                 :  1.9539361699E-03
                    electric rigidity (MeV) :  0.5577234240    =T[eV]*(gamma+1)/gamma, such that dev.=E*L/rigidity
  
 I, AMQ(1,I), AMQ(2,I)/QE, P/Pref, v/c, time, s :
  
     1   5.10998946E-01  1.00000000E+00  1.00000000E+00  8.04837361E-01  0.00000000E+00  0.00000000E+00
     2   5.10998946E-01  1.00000000E+00  1.00000000E+00  8.04837361E-01  0.00000000E+00  0.00000000E+00
     3   5.10998946E-01  1.00000000E+00  1.00000000E+00  8.04837361E-01  0.00000000E+00  0.00000000E+00

************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 1


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 may be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO    =         2.311 kG*cm
                               beta    =    0.80483736
                               gamma   =   1.68493295
                               gamma*G =   0.0019539362


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_EBth                                                            IPASS= 1


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 1


                OPEN FILE zgoubi.plt                                                                      
                FOR PRINTING TRAJECTORIES

Entrance hard edge is to be implemented
Exit hard edge is to be implemented

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.82939E+05 V/m
                               B                       =  4.07378E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80484    

                FACE  D'ENTREE
                DX =   0.000,  LAMBDA_E, LAMBDA_B =   0.000  0.000 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )

                FACE  DE  SORTIE
                DX =   0.000,  LAMBDA_E, LAMBDA_B =   0.000  0.000 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )

  ***  Warning : hard-edge fringe model entails vertical wedge focusing simulated with
                  first order kick  ***

                    Integration step :  0.2000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           50.000     0.000     0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           50.000     0.000     0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           50.000     0.000     0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      753.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.0000000     RAD


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_EBth                                                            IPASS= 1


************************************************************************************************************************************
      7  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #      6)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o   1   1.0000     0.000     0.000     0.000     0.000      0.0000   0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224531E-03 mass (MeV/c2) :  0.510999    
o   1   1.0000     0.000     0.000     0.000     0.000      0.0000   0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224531E-03 mass (MeV/c2) :  0.510999    
o   1   1.0000     0.000     0.000     0.000     0.000      0.0000   0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224531E-03 mass (MeV/c2) :  0.510999    

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 1


                OPEN FILE zgoubi.fai                                                                      
                FOR PRINTING COORDINATES 

               Print will occur at element[s] labeled : 


************************************************************************************************************************************
      9  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 1


 Pgm rebel. At pass #    1/ 401.  In element #    5,  parameter # 80  changed to    1.00000000E-02   (was    2.00000000E-01)

                                -----  REBELOTE  -----

     End of pass #        1 through the optical structure 

                     Total of          3 particles have been launched

     Multiple pass, 
          from element #     1 : OBJET     /label1=                    /label2=                    
                             to  REBELOTE  /label1=                    /label2=                    
     ending at pass #     401 at element #     9 : REBELOTE  /label1=                    /label2=                    


     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

                                -----  REBELOTE  -----

     End of pass #        2 through the optical structure 

                     Total of          6 particles have been launched

                                -----  REBELOTE  -----

     End of pass #        3 through the optical structure 

                     Total of          9 particles have been launched

                                -----  REBELOTE  -----

     End of pass #        4 through the optical structure 

                     Total of         12 particles have been launched

                                -----  REBELOTE  -----

     End of pass #        5 through the optical structure 

                     Total of         15 particles have been launched

                                -----  REBELOTE  -----

     End of pass #        6 through the optical structure 

                     Total of         18 particles have been launched

                                -----  REBELOTE  -----

     End of pass #        7 through the optical structure 

                     Total of         21 particles have been launched

                                -----  REBELOTE  -----

     End of pass #        8 through the optical structure 

                     Total of         24 particles have been launched

                                -----  REBELOTE  -----

     End of pass #        9 through the optical structure 

                     Total of         27 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       10 through the optical structure 

                     Total of         30 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       11 through the optical structure 

                     Total of         33 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       12 through the optical structure 

                     Total of         36 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       13 through the optical structure 

                     Total of         39 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       14 through the optical structure 

                     Total of         42 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       15 through the optical structure 

                     Total of         45 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       16 through the optical structure 

                     Total of         48 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       17 through the optical structure 

                     Total of         51 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       18 through the optical structure 

                     Total of         54 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       19 through the optical structure 

                     Total of         57 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       20 through the optical structure 

                     Total of         60 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       21 through the optical structure 

                     Total of         63 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       22 through the optical structure 

                     Total of         66 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       23 through the optical structure 

                     Total of         69 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       24 through the optical structure 

                     Total of         72 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       25 through the optical structure 

                     Total of         75 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       26 through the optical structure 

                     Total of         78 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       27 through the optical structure 

                     Total of         81 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       28 through the optical structure 

                     Total of         84 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       29 through the optical structure 

                     Total of         87 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       30 through the optical structure 

                     Total of         90 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       31 through the optical structure 

                     Total of         93 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       32 through the optical structure 

                     Total of         96 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       33 through the optical structure 

                     Total of         99 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       34 through the optical structure 

                     Total of        102 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       35 through the optical structure 

                     Total of        105 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       36 through the optical structure 

                     Total of        108 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       37 through the optical structure 

                     Total of        111 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       38 through the optical structure 

                     Total of        114 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       39 through the optical structure 

                     Total of        117 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       40 through the optical structure 

                     Total of        120 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       41 through the optical structure 

                     Total of        123 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       42 through the optical structure 

                     Total of        126 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       43 through the optical structure 

                     Total of        129 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       44 through the optical structure 

                     Total of        132 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       45 through the optical structure 

                     Total of        135 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       46 through the optical structure 

                     Total of        138 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       47 through the optical structure 

                     Total of        141 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       48 through the optical structure 

                     Total of        144 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       49 through the optical structure 

                     Total of        147 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       50 through the optical structure 

                     Total of        150 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       51 through the optical structure 

                     Total of        153 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       52 through the optical structure 

                     Total of        156 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       53 through the optical structure 

                     Total of        159 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       54 through the optical structure 

                     Total of        162 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       55 through the optical structure 

                     Total of        165 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       56 through the optical structure 

                     Total of        168 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       57 through the optical structure 

                     Total of        171 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       58 through the optical structure 

                     Total of        174 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       59 through the optical structure 

                     Total of        177 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       60 through the optical structure 

                     Total of        180 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       61 through the optical structure 

                     Total of        183 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       62 through the optical structure 

                     Total of        186 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       63 through the optical structure 

                     Total of        189 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       64 through the optical structure 

                     Total of        192 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       65 through the optical structure 

                     Total of        195 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       66 through the optical structure 

                     Total of        198 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       67 through the optical structure 

                     Total of        201 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       68 through the optical structure 

                     Total of        204 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       69 through the optical structure 

                     Total of        207 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       70 through the optical structure 

                     Total of        210 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       71 through the optical structure 

                     Total of        213 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       72 through the optical structure 

                     Total of        216 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       73 through the optical structure 

                     Total of        219 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       74 through the optical structure 

                     Total of        222 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       75 through the optical structure 

                     Total of        225 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       76 through the optical structure 

                     Total of        228 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       77 through the optical structure 

                     Total of        231 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       78 through the optical structure 

                     Total of        234 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       79 through the optical structure 

                     Total of        237 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       80 through the optical structure 

                     Total of        240 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       81 through the optical structure 

                     Total of        243 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       82 through the optical structure 

                     Total of        246 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       83 through the optical structure 

                     Total of        249 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       84 through the optical structure 

                     Total of        252 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       85 through the optical structure 

                     Total of        255 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       86 through the optical structure 

                     Total of        258 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       87 through the optical structure 

                     Total of        261 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       88 through the optical structure 

                     Total of        264 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       89 through the optical structure 

                     Total of        267 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       90 through the optical structure 

                     Total of        270 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       91 through the optical structure 

                     Total of        273 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       92 through the optical structure 

                     Total of        276 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       93 through the optical structure 

                     Total of        279 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       94 through the optical structure 

                     Total of        282 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       95 through the optical structure 

                     Total of        285 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       96 through the optical structure 

                     Total of        288 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       97 through the optical structure 

                     Total of        291 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       98 through the optical structure 

                     Total of        294 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       99 through the optical structure 

                     Total of        297 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      100 through the optical structure 

                     Total of        300 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      101 through the optical structure 

                     Total of        303 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      102 through the optical structure 

                     Total of        306 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      103 through the optical structure 

                     Total of        309 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      104 through the optical structure 

                     Total of        312 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      105 through the optical structure 

                     Total of        315 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      106 through the optical structure 

                     Total of        318 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      107 through the optical structure 

                     Total of        321 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      108 through the optical structure 

                     Total of        324 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      109 through the optical structure 

                     Total of        327 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      110 through the optical structure 

                     Total of        330 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      111 through the optical structure 

                     Total of        333 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      112 through the optical structure 

                     Total of        336 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      113 through the optical structure 

                     Total of        339 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      114 through the optical structure 

                     Total of        342 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      115 through the optical structure 

                     Total of        345 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      116 through the optical structure 

                     Total of        348 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      117 through the optical structure 

                     Total of        351 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      118 through the optical structure 

                     Total of        354 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      119 through the optical structure 

                     Total of        357 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      120 through the optical structure 

                     Total of        360 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      121 through the optical structure 

                     Total of        363 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      122 through the optical structure 

                     Total of        366 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      123 through the optical structure 

                     Total of        369 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      124 through the optical structure 

                     Total of        372 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      125 through the optical structure 

                     Total of        375 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      126 through the optical structure 

                     Total of        378 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      127 through the optical structure 

                     Total of        381 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      128 through the optical structure 

                     Total of        384 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      129 through the optical structure 

                     Total of        387 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      130 through the optical structure 

                     Total of        390 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      131 through the optical structure 

                     Total of        393 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      132 through the optical structure 

                     Total of        396 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      133 through the optical structure 

                     Total of        399 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      134 through the optical structure 

                     Total of        402 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      135 through the optical structure 

                     Total of        405 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      136 through the optical structure 

                     Total of        408 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      137 through the optical structure 

                     Total of        411 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      138 through the optical structure 

                     Total of        414 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      139 through the optical structure 

                     Total of        417 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      140 through the optical structure 

                     Total of        420 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      141 through the optical structure 

                     Total of        423 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      142 through the optical structure 

                     Total of        426 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      143 through the optical structure 

                     Total of        429 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      144 through the optical structure 

                     Total of        432 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      145 through the optical structure 

                     Total of        435 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      146 through the optical structure 

                     Total of        438 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      147 through the optical structure 

                     Total of        441 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      148 through the optical structure 

                     Total of        444 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      149 through the optical structure 

                     Total of        447 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      150 through the optical structure 

                     Total of        450 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      151 through the optical structure 

                     Total of        453 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      152 through the optical structure 

                     Total of        456 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      153 through the optical structure 

                     Total of        459 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      154 through the optical structure 

                     Total of        462 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      155 through the optical structure 

                     Total of        465 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      156 through the optical structure 

                     Total of        468 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      157 through the optical structure 

                     Total of        471 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      158 through the optical structure 

                     Total of        474 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      159 through the optical structure 

                     Total of        477 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      160 through the optical structure 

                     Total of        480 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      161 through the optical structure 

                     Total of        483 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      162 through the optical structure 

                     Total of        486 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      163 through the optical structure 

                     Total of        489 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      164 through the optical structure 

                     Total of        492 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      165 through the optical structure 

                     Total of        495 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      166 through the optical structure 

                     Total of        498 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      167 through the optical structure 

                     Total of        501 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      168 through the optical structure 

                     Total of        504 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      169 through the optical structure 

                     Total of        507 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      170 through the optical structure 

                     Total of        510 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      171 through the optical structure 

                     Total of        513 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      172 through the optical structure 

                     Total of        516 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      173 through the optical structure 

                     Total of        519 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      174 through the optical structure 

                     Total of        522 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      175 through the optical structure 

                     Total of        525 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      176 through the optical structure 

                     Total of        528 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      177 through the optical structure 

                     Total of        531 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      178 through the optical structure 

                     Total of        534 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      179 through the optical structure 

                     Total of        537 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      180 through the optical structure 

                     Total of        540 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      181 through the optical structure 

                     Total of        543 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      182 through the optical structure 

                     Total of        546 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      183 through the optical structure 

                     Total of        549 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      184 through the optical structure 

                     Total of        552 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      185 through the optical structure 

                     Total of        555 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      186 through the optical structure 

                     Total of        558 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      187 through the optical structure 

                     Total of        561 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      188 through the optical structure 

                     Total of        564 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      189 through the optical structure 

                     Total of        567 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      190 through the optical structure 

                     Total of        570 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      191 through the optical structure 

                     Total of        573 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      192 through the optical structure 

                     Total of        576 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      193 through the optical structure 

                     Total of        579 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      194 through the optical structure 

                     Total of        582 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      195 through the optical structure 

                     Total of        585 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      196 through the optical structure 

                     Total of        588 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      197 through the optical structure 

                     Total of        591 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      198 through the optical structure 

                     Total of        594 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      199 through the optical structure 

                     Total of        597 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      200 through the optical structure 

                     Total of        600 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      201 through the optical structure 

                     Total of        603 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      202 through the optical structure 

                     Total of        606 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      203 through the optical structure 

                     Total of        609 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      204 through the optical structure 

                     Total of        612 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      205 through the optical structure 

                     Total of        615 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      206 through the optical structure 

                     Total of        618 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      207 through the optical structure 

                     Total of        621 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      208 through the optical structure 

                     Total of        624 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      209 through the optical structure 

                     Total of        627 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      210 through the optical structure 

                     Total of        630 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      211 through the optical structure 

                     Total of        633 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      212 through the optical structure 

                     Total of        636 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      213 through the optical structure 

                     Total of        639 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      214 through the optical structure 

                     Total of        642 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      215 through the optical structure 

                     Total of        645 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      216 through the optical structure 

                     Total of        648 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      217 through the optical structure 

                     Total of        651 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      218 through the optical structure 

                     Total of        654 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      219 through the optical structure 

                     Total of        657 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      220 through the optical structure 

                     Total of        660 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      221 through the optical structure 

                     Total of        663 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      222 through the optical structure 

                     Total of        666 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      223 through the optical structure 

                     Total of        669 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      224 through the optical structure 

                     Total of        672 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      225 through the optical structure 

                     Total of        675 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      226 through the optical structure 

                     Total of        678 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      227 through the optical structure 

                     Total of        681 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      228 through the optical structure 

                     Total of        684 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      229 through the optical structure 

                     Total of        687 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      230 through the optical structure 

                     Total of        690 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      231 through the optical structure 

                     Total of        693 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      232 through the optical structure 

                     Total of        696 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      233 through the optical structure 

                     Total of        699 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      234 through the optical structure 

                     Total of        702 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      235 through the optical structure 

                     Total of        705 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      236 through the optical structure 

                     Total of        708 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      237 through the optical structure 

                     Total of        711 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      238 through the optical structure 

                     Total of        714 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      239 through the optical structure 

                     Total of        717 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      240 through the optical structure 

                     Total of        720 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      241 through the optical structure 

                     Total of        723 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      242 through the optical structure 

                     Total of        726 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      243 through the optical structure 

                     Total of        729 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      244 through the optical structure 

                     Total of        732 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      245 through the optical structure 

                     Total of        735 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      246 through the optical structure 

                     Total of        738 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      247 through the optical structure 

                     Total of        741 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      248 through the optical structure 

                     Total of        744 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      249 through the optical structure 

                     Total of        747 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      250 through the optical structure 

                     Total of        750 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      251 through the optical structure 

                     Total of        753 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      252 through the optical structure 

                     Total of        756 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      253 through the optical structure 

                     Total of        759 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      254 through the optical structure 

                     Total of        762 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      255 through the optical structure 

                     Total of        765 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      256 through the optical structure 

                     Total of        768 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      257 through the optical structure 

                     Total of        771 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      258 through the optical structure 

                     Total of        774 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      259 through the optical structure 

                     Total of        777 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      260 through the optical structure 

                     Total of        780 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      261 through the optical structure 

                     Total of        783 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      262 through the optical structure 

                     Total of        786 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      263 through the optical structure 

                     Total of        789 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      264 through the optical structure 

                     Total of        792 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      265 through the optical structure 

                     Total of        795 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      266 through the optical structure 

                     Total of        798 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      267 through the optical structure 

                     Total of        801 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      268 through the optical structure 

                     Total of        804 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      269 through the optical structure 

                     Total of        807 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      270 through the optical structure 

                     Total of        810 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      271 through the optical structure 

                     Total of        813 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      272 through the optical structure 

                     Total of        816 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      273 through the optical structure 

                     Total of        819 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      274 through the optical structure 

                     Total of        822 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      275 through the optical structure 

                     Total of        825 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      276 through the optical structure 

                     Total of        828 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      277 through the optical structure 

                     Total of        831 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      278 through the optical structure 

                     Total of        834 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      279 through the optical structure 

                     Total of        837 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      280 through the optical structure 

                     Total of        840 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      281 through the optical structure 

                     Total of        843 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      282 through the optical structure 

                     Total of        846 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      283 through the optical structure 

                     Total of        849 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      284 through the optical structure 

                     Total of        852 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      285 through the optical structure 

                     Total of        855 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      286 through the optical structure 

                     Total of        858 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      287 through the optical structure 

                     Total of        861 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      288 through the optical structure 

                     Total of        864 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      289 through the optical structure 

                     Total of        867 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      290 through the optical structure 

                     Total of        870 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      291 through the optical structure 

                     Total of        873 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      292 through the optical structure 

                     Total of        876 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      293 through the optical structure 

                     Total of        879 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      294 through the optical structure 

                     Total of        882 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      295 through the optical structure 

                     Total of        885 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      296 through the optical structure 

                     Total of        888 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      297 through the optical structure 

                     Total of        891 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      298 through the optical structure 

                     Total of        894 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      299 through the optical structure 

                     Total of        897 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      300 through the optical structure 

                     Total of        900 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      301 through the optical structure 

                     Total of        903 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      302 through the optical structure 

                     Total of        906 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      303 through the optical structure 

                     Total of        909 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      304 through the optical structure 

                     Total of        912 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      305 through the optical structure 

                     Total of        915 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      306 through the optical structure 

                     Total of        918 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      307 through the optical structure 

                     Total of        921 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      308 through the optical structure 

                     Total of        924 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      309 through the optical structure 

                     Total of        927 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      310 through the optical structure 

                     Total of        930 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      311 through the optical structure 

                     Total of        933 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      312 through the optical structure 

                     Total of        936 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      313 through the optical structure 

                     Total of        939 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      314 through the optical structure 

                     Total of        942 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      315 through the optical structure 

                     Total of        945 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      316 through the optical structure 

                     Total of        948 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      317 through the optical structure 

                     Total of        951 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      318 through the optical structure 

                     Total of        954 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      319 through the optical structure 

                     Total of        957 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      320 through the optical structure 

                     Total of        960 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      321 through the optical structure 

                     Total of        963 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      322 through the optical structure 

                     Total of        966 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      323 through the optical structure 

                     Total of        969 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      324 through the optical structure 

                     Total of        972 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      325 through the optical structure 

                     Total of        975 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      326 through the optical structure 

                     Total of        978 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      327 through the optical structure 

                     Total of        981 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      328 through the optical structure 

                     Total of        984 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      329 through the optical structure 

                     Total of        987 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      330 through the optical structure 

                     Total of        990 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      331 through the optical structure 

                     Total of        993 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      332 through the optical structure 

                     Total of        996 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      333 through the optical structure 

                     Total of        999 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      334 through the optical structure 

                     Total of       1002 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      335 through the optical structure 

                     Total of       1005 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      336 through the optical structure 

                     Total of       1008 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      337 through the optical structure 

                     Total of       1011 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      338 through the optical structure 

                     Total of       1014 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      339 through the optical structure 

                     Total of       1017 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      340 through the optical structure 

                     Total of       1020 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      341 through the optical structure 

                     Total of       1023 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      342 through the optical structure 

                     Total of       1026 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      343 through the optical structure 

                     Total of       1029 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      344 through the optical structure 

                     Total of       1032 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      345 through the optical structure 

                     Total of       1035 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      346 through the optical structure 

                     Total of       1038 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      347 through the optical structure 

                     Total of       1041 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      348 through the optical structure 

                     Total of       1044 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      349 through the optical structure 

                     Total of       1047 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      350 through the optical structure 

                     Total of       1050 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      351 through the optical structure 

                     Total of       1053 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      352 through the optical structure 

                     Total of       1056 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      353 through the optical structure 

                     Total of       1059 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      354 through the optical structure 

                     Total of       1062 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      355 through the optical structure 

                     Total of       1065 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      356 through the optical structure 

                     Total of       1068 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      357 through the optical structure 

                     Total of       1071 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      358 through the optical structure 

                     Total of       1074 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      359 through the optical structure 

                     Total of       1077 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      360 through the optical structure 

                     Total of       1080 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      361 through the optical structure 

                     Total of       1083 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      362 through the optical structure 

                     Total of       1086 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      363 through the optical structure 

                     Total of       1089 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      364 through the optical structure 

                     Total of       1092 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      365 through the optical structure 

                     Total of       1095 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      366 through the optical structure 

                     Total of       1098 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      367 through the optical structure 

                     Total of       1101 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      368 through the optical structure 

                     Total of       1104 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      369 through the optical structure 

                     Total of       1107 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      370 through the optical structure 

                     Total of       1110 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      371 through the optical structure 

                     Total of       1113 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      372 through the optical structure 

                     Total of       1116 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      373 through the optical structure 

                     Total of       1119 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      374 through the optical structure 

                     Total of       1122 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      375 through the optical structure 

                     Total of       1125 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      376 through the optical structure 

                     Total of       1128 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      377 through the optical structure 

                     Total of       1131 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      378 through the optical structure 

                     Total of       1134 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      379 through the optical structure 

                     Total of       1137 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      380 through the optical structure 

                     Total of       1140 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      381 through the optical structure 

                     Total of       1143 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      382 through the optical structure 

                     Total of       1146 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      383 through the optical structure 

                     Total of       1149 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      384 through the optical structure 

                     Total of       1152 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      385 through the optical structure 

                     Total of       1155 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      386 through the optical structure 

                     Total of       1158 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      387 through the optical structure 

                     Total of       1161 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      388 through the optical structure 

                     Total of       1164 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      389 through the optical structure 

                     Total of       1167 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      390 through the optical structure 

                     Total of       1170 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      391 through the optical structure 

                     Total of       1173 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      392 through the optical structure 

                     Total of       1176 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      393 through the optical structure 

                     Total of       1179 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      394 through the optical structure 

                     Total of       1182 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      395 through the optical structure 

                     Total of       1185 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      396 through the optical structure 

                     Total of       1188 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      397 through the optical structure 

                     Total of       1191 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      398 through the optical structure 

                     Total of       1194 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      399 through the optical structure 

                     Total of       1197 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      400 through the optical structure 

                     Total of       1200 particles have been launched


      Next  pass  is  #   401 and  last  pass  through  the  optical  structure


************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 401

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 401


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 401


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 may be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO    =         2.311 kG*cm
                               beta    =    0.80483736
                               gamma   =   1.68493295
                               gamma*G =   0.0019539362


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_EBth                                                            IPASS= 401


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 401


     zgoubi.plt                                                                      
      already open...
Entrance hard edge is to be implemented
Exit hard edge is to be implemented

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.82939E+05 V/m
                               B                       =  4.07378E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80484    

                FACE  D'ENTREE
                DX =   0.000,  LAMBDA_E, LAMBDA_B =   0.000  0.000 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )

                FACE  DE  SORTIE
                DX =   0.000,  LAMBDA_E, LAMBDA_B =   0.000  0.000 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )

  ***  Warning : hard-edge fringe model entails vertical wedge focusing simulated with
                  first order kick  ***

                    Integration step :   10.00     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           50.000     0.000     0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           50.000     0.000     0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           50.000     0.000     0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (       18.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.0000000     RAD


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   8.309704E-07 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_EBth                                                            IPASS= 401


************************************************************************************************************************************
      7  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 401

0                                             TRACE DU FAISCEAU
                                           (follows element #      6)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o   1   1.0000     0.000     0.000     0.000     0.000      0.0000   0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224530E-03 mass (MeV/c2) :  0.510999    
o   1   1.0000     0.000     0.000     0.000     0.000      0.0000   0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224530E-03 mass (MeV/c2) :  0.510999    
o   1   1.0000     0.000     0.000     0.000     0.000      0.0000   0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224530E-03 mass (MeV/c2) :  0.510999    

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 401


************************************************************************************************************************************
      9  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 401


                         >>>>  End  of  'REBELOTE'  procedure  <<<<

      There  has  been        401  passes  through  the  optical  structure 

                     Total of       1203 particles have been launched

************************************************************************************************************************************
     10  Keyword, label(s) :  SYSTEM                                                                                   IPASS= 402

     Number  of  commands :   2,  as  follows : 

 gnuplot <./gnuplot_scanTraj.gnu
 okular ./gnuplot_scanTraj.eps &

************************************************************************************************************************************
     11  Keyword, label(s) :  END                                                                                      IPASS= 402


************************************************************************************************************************************
 Pgm zgoubi : Execution ended normally, upon keyword END or FIN
   
 File in:   WFSegment_ScanTraj.dat
 File out:  zgoubi.res

  Zgoubi, author's dvlpmnt version.
  Job  started  on  30-03-2021,  at  05:18:20 
  JOB  ENDED  ON    30-03-2021,  AT  05:18:21 

   CPU time, total :    0.29382299999999995     
