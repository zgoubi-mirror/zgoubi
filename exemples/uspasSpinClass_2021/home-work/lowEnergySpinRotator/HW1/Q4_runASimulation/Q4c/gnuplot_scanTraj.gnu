

set xlabel "step size [cm]"; set ylabel " x [cm],  x' [mrad]"; set y2label "{/Symbol d q}_s/{/Symbol q}_s"
set xtics mirror; set ytics nomirror; set y2tics; set k c l  font "roman, 10"; set logscale x; set logscale x2
# data to compute step size, for abscissa axis, and relative change of spin rotation, vertical axis:
stpi = 0.01; stpf = 10.; nrblt = 400; dstp = (stpf-stpi)/(nrblt-1); nbtraj = 3; ttaInf = -30.; 

y2(x)=-0.0001; set arrow from 2.65, 0  to 2.65, .00035 nohead lc 1 dt 2

plot   \
  "zgoubi.fai" u ($38>1 && $26==1? stpi+dstp*$0/nbtraj :1/0):($10) axes x1y1 w p pt 5 ps .7 lc rgb "red"  ,\
  "zgoubi.fai" u ($38>1 && $26==1? stpi+dstp*$0/nbtraj :1/0):($11) axes x1y1 w lp pt 6 ps .7 lc rgb "blue"  ,\
  "zgoubi.fai" u ($38>1 && $26==1? stpi+dstp*$0/nbtraj :1/0):(-(atan($21/$20)/(4.*atan(1.))*180. -ttaInf)/ttaInf) axes x1y2 w lp pt 7 ps .7 lc rgb "green" tit "spin-R (right axis)" ,\
  y2(x) axes x1y2 lc 1 dt 2 notit

 set terminal postscript eps blacktext color enh    # size 8.3cm,4cm "Times-Roman" 12 
 set output "gnuplot_scanTraj.eps" 
 replot 
 set terminal X11 
 unset output 

pause .2
quit
