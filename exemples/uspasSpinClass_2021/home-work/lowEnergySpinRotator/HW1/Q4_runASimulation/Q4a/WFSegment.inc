
'MARKER'  #S_WFSegment                                                                                    
'WIENFILT'                                                                                                
20              ! Logs particle data, at each integration step, in zgoubi.plt [see the guide, Sec.8.3].
0.5  -19E5 8E-03 1                                            ! (Wrong) E (V/m) and B (T) field values.
0. 0. 0.                             ! Hard-edge entrance face.  Substitue 20. 5. 5. for fringe fields.
0.2401  1.8639  -0.5572  0.3904 0. 0.          ! Fringe field coefficients in Enge model, ignored here.
0.2401  1.8639  -0.5572  0.3904 0. 0.
0. 0. 0.                             ! Hard-edge exit     face.  Substitue 20. 5. 5. for fringe fields.
0.2401  1.8639  -0.5572  0.3904 0. 0.
0.2401  1.8639  -0.5572  0.3904 0. 0.
0.4                                                                            ! Integration step size.
1. 0. 0. 0.                                                              ! Positionning of the element.
'MARKER'  #E_WFSegment                                                                                    

'END'
