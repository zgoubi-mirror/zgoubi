

! Field fringes are accounted for here.
! Fall-off distance is lambda= 5cm for both E and B, at both_entranece and exit of Wien filter.
! This requires extending the entrance and exit integration regions, by a distance XE and XS, beyond 
! the hard-edge model effective field boundaries.   Take XE=XS=20cm, this is sufficient to ensure 
! negligible residual field integral of the cut-off fields. 

'MARKER'  #S_WFSegment_FF                                            ! Use for INCLUDE by WFSgment.dat.
'WIENFILT'                                                                                                
2               ! Logs particle data, at each integration step, in zgoubi.plt [see the guide, Sec.8.3].
0.5  -982938.94   0.0040737813 1                          ! Theoretical E (V/m) and B (T) field values.
20. 5. 5.                                            ! XE=20cm, lambda_E=5cm for E and B fringe fields.
0.2401  1.8639  -0.5572  0.3904 0. 0.          ! Fringe field coefficients in Enge model, ignored here.
0.2401  1.8639  -0.5572  0.3904 0. 0.
20. 5. 5.                                            ! XE=20cm, lambda_S=5cm for E and B fringe fields.
0.2401  1.8639  -0.5572  0.3904 0. 0.
0.2401  1.8639  -0.5572  0.3904 0. 0.
0.4                                                                            ! Integration step size.
1. 0. 0. 0.                                                              ! Positionning of the element.
'MARKER'  #E_WFSegment_FF                                            ! Use for INCLUDE by WFSgment.dat.

'END'
