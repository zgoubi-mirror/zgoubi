
set xlabel "s [cm]"; set xtics mirror; unset ytics; set k t l font "roman, 10"; zero = 0.
# in zgoubi.plt, test $19: particle number; test $51: FIT stage number; $14: s; $33: SX
set arrow from 50,0 to 50,-130 nohead; set arrow from 100,0 to 100,-130 nohead; set arrow from 150,0 to 150,-130 nohead
WFL = 50. ; FF=20. # efective length of spin rotator; fringe extents
vLen=20  # manage vector length on graph
plot [:70] [0:-vLen*.35]  \
"zgoubi.plt" every 5 u ($19==1 & $51==0 && ($22>=FF && $22<=WFL)? $22 : 1/0):(zero):($33*vLen):($34*vLen)  w  vector lw .4 lc rgb "red"  tit "before FIT" ,\
"zgoubi.plt" every 5 u ($19==1 & $51==1 && ($22>=FF && $22<=WFL)? $22 : 1/0):(zero):($33*vLen):($34*vLen)  w  vector lw 1 lc rgb "blue" tit "after" 

set terminal postscript eps blacktext color enh  
 set output "gnuplot_spin.eps" 
 replot 
 set terminal X11 
 unset output 

pause 2
quit
