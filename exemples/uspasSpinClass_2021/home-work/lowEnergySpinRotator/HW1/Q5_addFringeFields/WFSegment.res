WFSegment.
! In addition to the comments on the right, use the Users' Guide.
'OBJET'                                                                                                      1
2.31147953865                                                         ! Rigidity of a 350 keV electron.
2
3  1                                    ! 3 electrons are needed for spin matrix computation by SPNPRT.
0.  0. 0. 0. 0. 1. 'o'                  ! Initial coordinates all zero, except for relative regidity 1.
0.  0. 0. 0. 0. 1. 'o'
0.  0. 0. 0. 0. 1. 'o'
1 1 1
'PARTICUL'                                                                                                   2
POSITRON
 
'SPNTRK'                         ! Set the initial spin components, proper for spin matrix computation.      3
4                                                                 !
1. 0. 0.                                                                                ! Initial SX=1.
0. 1. 0.                                                                                ! Initial SY=1.
0. 0. 1.                                                                                ! Initial SZ=1.
 
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./WFSegment_EBTheor.inc[#S_WFSegment_FF,*:#E(n_inc, depth :  1 2)
'MARKER'  #S_WFSegment_FF                                            ! Use for INCLUDE by WFSgment.dat.      4
'WIENFILT'                                                                                                   5
2               ! Logs particle data, at each integration step, in zgoubi.plt [see the guide, Sec.8.3].
0.5  -982938.94   0.0040737813 1                          ! Theoretical E (V/m) and B (T) field values.
20. 5. 5.                                            ! XE=20cm, lambda_E=5cm for E and B fringe fields.
0.2401  1.8639  -0.5572  0.3904 0. 0.          ! Fringe field coefficients in Enge model, ignored here.
0.2401  1.8639  -0.5572  0.3904 0. 0.
20. 5. 5.                                            ! XE=20cm, lambda_S=5cm for E and B fringe fields.
0.2401  1.8639  -0.5572  0.3904 0. 0.
0.2401  1.8639  -0.5572  0.3904 0. 0.
0.4                                                                            ! Integration step size.
1. 0. 0. 0.                                                              ! Positionning of the element.
! Include_SegmentEnd : ./WFSegment_EBTheor.inc                       (had n_inc, depth :  1 2)
'MARKER'  #E_WFSegment_FF                                            ! Use for INCLUDE by WFSgment.dat.      6
 
'FIT2'                    ! The numbering of optical elements to be used is to be read in zgoubi.res.        7
2
5 11 0 .8                                                     ! Keyword 6: WIENFILT; paremeter 11: E.
5 12 0 .8                                                     ! Keyword 6: WIENFILT; paremeter 12: B.
5 1E-15                                                          ! Six constraints; penalty is 1e-15:
3    1 2 #End 0. 1. 0                                                                   ! Y=0 at end.
3    1 3 #End 0. 1. 0                                                                   ! T=0 at end.
10.2 1 1 #End 0.52359877559 1. 0                                              ! 30 deg spin rotation,
10   1 4 #End 1. 1. 0                                                   ! |S|=1, for all 3 particles.
10   2 4 #End 1. 1. 0
10   3 4 #End 1. 1. 0
 
'FAISCEAU'                                                  ! Log trajectory coordinates in zgoubi.res.      8
'SPNPRT'  MATRIX                                  ! Log spin coordinates and spin matrix in zgoubi.res.      9
'SYSTEM'                                                                                                    10
3
gnuplot < ./gnuplot_spin.gnu                                             ! Produce a graph of vec_S(X).
gnuplot < ./gnuplot_Zplt_XB.gnu                                              ! Produce a graph of B(X).
gnuplot < ./gnuplot_Zplt_XE.gnu                                              ! Produce a graph of E(X).
'END'

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 1

     Particle  properties :
     POSITRON
                     Mass          =   0.510999        MeV/c2
                     Charge        =   1.602176E-19    C     
                     G  factor     =   1.159652E-03          
                     COM life-time =   1.000000E+99    s     

              Reference  data :
                    mag. rigidity (kG.cm)   :   2.3114795      =p/q, such that dev.=B*L/rigidity
                    mass (MeV/c2)           :  0.51099895    
                    momentum (MeV/c)        :  0.69296413    
                    energy, total (MeV)     :  0.86099896    
                    energy, kinetic (MeV)   :  0.35000002    
                    beta = v/c              :  0.8048373615    
                    gamma                   :   1.684932950    
                    beta*gamma              :   1.356096989    
                    G*gamma                 :  1.9539361699E-03
                    m / G                   :   440.6484587    
                    electric rigidity (MeV) :  0.5577234240    =T[eV]*(gamma+1)/gamma, such that dev.=E*L/rigidity
  
 I, AMQ(1,I), AMQ(2,I)/QE, P/Pref, v/c, time, s :
  
     1   5.10998946E-01  1.00000000E+00  1.00000000E+00  8.04837361E-01  0.00000000E+00  0.00000000E+00
     2   5.10998946E-01  1.00000000E+00  1.00000000E+00  8.04837361E-01  0.00000000E+00  0.00000000E+00
     3   5.10998946E-01  1.00000000E+00  1.00000000E+00  8.04837361E-01  0.00000000E+00  0.00000000E+00

************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 1


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     2.3114795     kG*cm
                               beta      =    0.80483736    
                               gamma     =     1.6849329    
                               G*gamma   =    0.19539362E-02
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_FF                                                              IPASS= 1


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 1


                OPEN FILE zgoubi.plt                                                                      
                FOR PRINTING TRAJECTORIES


                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.82939E+05 V/m
                               B                       =  4.07378E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80484    

                FACE  D'ENTREE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  5.000 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                FACE  DE  SORTIE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  5.000 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                    Integration step :  0.4000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000    -0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000    -0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000    -0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      675.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_FF                                                              IPASS= 1


************************************************************************************************************************************
      7  Keyword, label(s) :  FIT2                                                                                     IPASS= 1

     Pgm main. FITING=.T., FIT procedure launched.

           variable #            1       IR =            5 ,   ok.
           variable #            1       IP =           11 ,   ok.
           variable #            2       IR =            5 ,   ok.
           variable #            2       IP =           12 ,   ok.
           constraint #            1       IR =            6 ,   ok.
           constraint #            1       I  =            1 ,   ok.
           constraint #            2       IR =            6 ,   ok.
           constraint #            2       I  =            1 ,   ok.
           constraint #            3       IR =            6 ,   ok.
           constraint #            4       IR =            6 ,   ok.
           constraint #            5       IR =            6 ,   ok.

                    FIT  variables  and  constraints  in  good  order,  FIT  will proceed. 
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -1.769E+06  -9.829E+05  -982883.82     -1.966E+05  6.728E-02  WIENFILT   -                    -                   
   5   2    12   8.148E-04   4.074E-03  4.07355283E-03  7.333E-03  2.787E-10  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    7.862599E-11    Infinity MARKER     #E_WFSegment_FF      -                    0
  3   1   3     6    0.000000E+00    1.000E+00    3.088120E-09    Infinity MARKER     #E_WFSegment_FF      -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    Infinity MARKER     #E_WFSegment_FF      -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WFSegment_FF      -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WFSegment_FF      -                    0
 Fit reached penalty value   3.5723E-16



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 1


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 1


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     2.3114795     kG*cm
                               beta      =    0.80483736    
                               gamma     =     1.6849329    
                               G*gamma   =    0.19539362E-02
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_FF                                                              IPASS= 1


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 1


     zgoubi.plt                                                                      
      already open...

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.82884E+05 V/m
                               B                       =  4.07355E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80484    

                FACE  D'ENTREE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  5.000 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                FACE  DE  SORTIE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  5.000 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                    Integration step :  0.4000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000     0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000     0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000     0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      675.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_FF                                                              IPASS= 1


************************************************************************************************************************************

      7   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o   1   1.0000     0.000     0.000     0.000     0.000      0.0000  -0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     1
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o   1   1.0000     0.000     0.000     0.000     0.000      0.0000  -0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     2
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    
o   1   1.0000     0.000     0.000     0.000     0.000      0.0000  -0.0000    0.000    0.000   -0.000   -0.000   5.000000E+01     3
               Time of flight (mus) :  2.07224535E-03 mass (MeV/c2) :  0.510999    

************************************************************************************************************************************
      9  Keyword, label(s) :  SPNPRT      MATRIX                                                                       IPASS= 1

                          -- 1  GROUPS  OF  MOMENTA  FOLLOW   --

                    ----------------------------------------------------------------------------------
                    Momentum  group  #1 (D=   1.000000E+00) ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455342   0.122008   0.333333   0.577350   0.001954  19.999999  14.142135


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   |Si,Sf|   (Z,Sf_yz) (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_yz : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.866025 -0.500000 -0.000000  1.000000      1.6849   30.000   90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500000  0.866025 -0.000000  1.000000      1.6849   30.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000  0.000000  1.000000  1.000000      1.6849    0.000   45.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6603E-01  8.6603E-01 -5.0000E-01 -5.0000E-01 -1.2760E-17 -1.2760E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00     1   1
  5.0000E-01  5.0000E-01  8.6603E-01  8.6603E-01 -1.7359E-17 -1.7359E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00     2   1
  1.9772E-17  1.9772E-17  8.7302E-18  8.7302E-18  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00     3   1


                  Spin transfer matrix, momentum group # 1 :

          0.866025        0.500000        1.977188E-17
         -0.500000        0.866025        8.730209E-18
         -1.275961E-17   -1.735898E-17     1.00000    

     Determinant =       1.0000000000
     Trace =       2.7320508262;   spin precession acos((trace-1)/2) =      29.9999989312 deg
     Precession axis :  (-0.0000,  0.0000, -1.0000)  ->  angle to (X,Y) plane, to X axis :   -90.0000,   128.7285 deg
     Spin precession/2pi (or Qs, fractional) :    8.3333E-02

************************************************************************************************************************************
     10  Keyword, label(s) :  SYSTEM                                                                                   IPASS= 1

     Number  of  commands :   3,  as  follows : 

 gnuplot < ./gnuplot_spin.gnu
 gnuplot < ./gnuplot_Zplt_XB.gnu
 gnuplot < ./gnuplot_Zplt_XE.gnu

************************************************************************************************************************************
     11  Keyword, label(s) :  END                                                                                      IPASS= 1

                             3 particles have been launched
                     Made  it  to  the  end :      3

************************************************************************************************************************************
 Pgm zgoubi : Execution ended normally, upon keyword END or FIN
   
 File in:   WFSegment.dat
 File out:  zgoubi.res

  Zgoubi, author's Revision: 1590.
  Job  started  on  04-02-2022,  at  10:35:33 
  JOB  ENDED  ON    04-02-2022,  AT  10:35:40 

   CPU time, total :    0.20290199999999997     

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
