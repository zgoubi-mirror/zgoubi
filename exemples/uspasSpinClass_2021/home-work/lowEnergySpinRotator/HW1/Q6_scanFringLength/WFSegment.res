WFSegment.
! In addition to the comments on the right, use the Users' Guide.
'OBJET'                                                                                                      1
2.31147953865                                                         ! Rigidity of a 350 keV electron.
2
3  1                                    ! 3 electrons are needed for spin matrix computation by SPNPRT.
0.  0. 0. 0. 0. 1. 'o'                  ! Initial coordinates all zero, except for relative regidity 1.
0.  0. 0. 0. 0. 1. 'o'
0.  0. 0. 0. 0. 1. 'o'
1 1 1
'PARTICUL'                                                                                                   2
POSITRON
 
'SPNTRK'                         ! Set the initial spin components, proper for spin matrix computation.      3
4                                                                 !
1. 0. 0.                                                                                ! Initial SX=1.
0. 1. 0.                                                                                ! Initial SY=1.
0. 0. 1.                                                                                ! Initial SZ=1.
 
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./WFSegment_EBTheor.inc[#S_WFSegment_FF,*:#E(n_inc, depth :  1 2)
'MARKER'  #S_WFSegment_FF                                                                                    4
'WIENFILT'                                                                                                   5
2               ! Logs particle data, at each integration step, in zgoubi.plt [see the guide, Sec.8.3].
0.5  -982938.94   0.0040737813 1                          ! Theoretical E (V/m) and B (T) field values.
20. 5. 5.                            ! Hard-edge entrance face.  Substitue 20. 5. 5. for fringe fields.
0.2401  1.8639  -0.5572  0.3904 0. 0.          ! Fringe field coefficients in Enge model, ignored here.
0.2401  1.8639  -0.5572  0.3904 0. 0.
20. 5. 5.                            ! Hard-edge exit     face.  Substitue 20. 5. 5. for fringe fields.
0.2401  1.8639  -0.5572  0.3904 0. 0.
0.2401  1.8639  -0.5572  0.3904 0. 0.
0.4                                                                            ! Integration step size.
1. 0. 0. 0.                                                              ! Positionning of the element.
! Include_SegmentEnd : ./WFSegment_EBTheor.inc                       (had n_inc, depth :  1 2)
'MARKER'  #E_WFSegment_FF                                                                                    6
 
'FIT2'                      ! The numbering of optical elements to be used is to be rad in zgoubi.res.       7
2
5 11 0 .8                                                      ! Keyword 5: WIENFILT; paremeter 11: E.
5 12 0 .8                                                      ! Keyword 5: WIENFILT; paremeter 12: B.
5 1E-15                                                           ! Six constraints; penalty is 1e-15:
3    1 2 #End 0. 1. 0                                                                    ! Y=0 at end.
3    1 3 #End 0. 1. 0                                                                    ! T=0 at end.
10.2 1 1 #End 0.52359877559 1. 0                                               ! 30 deg spin rotation,
10   1 4 #End 1. 1. 0                                                    ! |S|=1, for all 3 particles.
10   2 4 #End 1. 1. 0
10   3 4 #End 1. 1. 0
 
'REBELOTE'                                                                                                   8
37 0.1 0 1                   ! NPASS is of the form int*(7[cm]-3[cm])+1 to allow for lambdaB/lambdaE=1.
2
WIENFILT 22 3.:7.                                       ! vary lambda_B at entrance EFB from 3 to 7 cm.
WIENFILT 52 3.:7.                                           ! vary lambda_B at exit EFB from 3 to 7 cm.
 
'FAISCEAU'                                                  ! Log trajectory coordinates in zgoubi.res.      9
'SPNPRT'  MATRIX                                  ! Log spin coordinates and spin matrix in zgoubi.res.     10
'SYSTEM'                                                                                                    11
2
gnuplot <./gnuplot_Zplt_sYZ.gnu
gnuplot < ./gnuplot_scanEB_FFratio.gnu
'END'

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 1

     Particle  properties :
     POSITRON
                     Mass          =   0.510999        MeV/c2
                     Charge        =   1.602176E-19    C     
                     G  factor     =   1.159652E-03          
                     COM life-time =   1.000000E+99    s     

              Reference  data :
                    mag. rigidity (kG.cm)   :   2.3114795      =p/q, such that dev.=B*L/rigidity
                    mass (MeV/c2)           :  0.51099895    
                    momentum (MeV/c)        :  0.69296413    
                    energy, total (MeV)     :  0.86099896    
                    energy, kinetic (MeV)   :  0.35000002    
                    beta = v/c              :  0.8048373615    
                    gamma                   :   1.684932950    
                    beta*gamma              :   1.356096989    
                    G*gamma                 :  1.9539361699E-03
                    m / G                   :   440.6484587    
                    electric rigidity (MeV) :  0.5577234240    =T[eV]*(gamma+1)/gamma, such that dev.=E*L/rigidity
  
 I, AMQ(1,I), AMQ(2,I)/QE, P/Pref, v/c, time, s :
  
     1   5.10998946E-01  1.00000000E+00  1.00000000E+00  8.04837361E-01  0.00000000E+00  0.00000000E+00
     2   5.10998946E-01  1.00000000E+00  1.00000000E+00  8.04837361E-01  0.00000000E+00  0.00000000E+00
     3   5.10998946E-01  1.00000000E+00  1.00000000E+00  8.04837361E-01  0.00000000E+00  0.00000000E+00

************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 1


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     2.3114795     kG*cm
                               beta      =    0.80483736    
                               gamma     =     1.6849329    
                               G*gamma   =    0.19539362E-02
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_FF                                                              IPASS= 1


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 1


                OPEN FILE zgoubi.plt                                                                      
                FOR PRINTING TRAJECTORIES


                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.82939E+05 V/m
                               B                       =  4.07378E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80484    

                FACE  D'ENTREE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  5.000 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                FACE  DE  SORTIE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  5.000 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                    Integration step :  0.4000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000    -0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000    -0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000    -0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      675.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_FF                                                              IPASS= 1


************************************************************************************************************************************
      7  Keyword, label(s) :  FIT2                                                                                     IPASS= 1

     Pgm main. FITING=.T., FIT procedure launched.

           variable #            1       IR =            5 ,   ok.
           variable #            1       IP =           11 ,   ok.
           variable #            2       IR =            5 ,   ok.
           variable #            2       IP =           12 ,   ok.
           constraint #            1       IR =            6 ,   ok.
           constraint #            1       I  =            1 ,   ok.
           constraint #            2       IR =            6 ,   ok.
           constraint #            2       I  =            1 ,   ok.
           constraint #            3       IR =            6 ,   ok.
           constraint #            4       IR =            6 ,   ok.
           constraint #            5       IR =            6 ,   ok.

                    FIT  variables  and  constraints  in  good  order,  FIT  will proceed. 
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -1.769E+06  -9.829E+05  -982883.82     -1.966E+05  6.728E-02  WIENFILT   -                    -                   
   5   2    12   8.148E-04   4.074E-03  4.07355283E-03  7.333E-03  2.787E-10  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    7.862599E-11    Infinity MARKER     #E_WFSegment_FF      -                    0
  3   1   3     6    0.000000E+00    1.000E+00    3.088120E-09    Infinity MARKER     #E_WFSegment_FF      -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    Infinity MARKER     #E_WFSegment_FF      -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WFSegment_FF      -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00         NaN MARKER     #E_WFSegment_FF      -                    0
 Fit reached penalty value   3.5723E-16



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 1


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 1


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     2.3114795     kG*cm
                               beta      =    0.80483736    
                               gamma     =     1.6849329    
                               G*gamma   =    0.19539362E-02
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_FF                                                              IPASS= 1


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 1


     zgoubi.plt                                                                      
      already open...

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.82884E+05 V/m
                               B                       =  4.07355E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80484    

                FACE  D'ENTREE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  5.000 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                FACE  DE  SORTIE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  5.000 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                    Integration step :  0.4000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000     0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000     0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000     0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      675.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_FF                                                              IPASS= 1


************************************************************************************************************************************

      7   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 1


 Pgm rebel. At pass #    1/  38.  In element #    5,  parameter # 22  changed to    3.00000000E+00   (was    5.00000000E+00)

 Pgm rebel. At pass #    1/  38.  In element #    5,  parameter # 52  changed to    3.00000000E+00   (was    5.00000000E+00)

                                -----  REBELOTE  -----

     End of pass #        1 through the optical structure 

                     Total of          3 particles have been launched

     Multiple pass, 
          from element #     1 : OBJET     /label1=                    /label2=                    
                             to  REBELOTE  /label1=                    /label2=                    
     ending at pass #      38 at element #     8 : REBELOTE  /label1=                    /label2=                    


     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -1.769E+06  -9.829E+05  -985526.75     -1.966E+05  1.361E-06  WIENFILT   -                    -                   
   5   2    12   8.148E-04   4.074E-03  4.08274252E-03  7.333E-03  5.673E-15  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00   -5.476486E-05    9.99E-01 MARKER     #E_WFSegment_FF      -                    0
  3   1   3     6    0.000000E+00    1.000E+00    1.392049E-06    6.46E-04 MARKER     #E_WFSegment_FF      -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    1.02E-07 MARKER     #E_WFSegment_FF      -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 Fit reached penalty value   3.0011E-09



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 2

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 2


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 2


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     2.3114795     kG*cm
                               beta      =    0.80483736    
                               gamma     =     1.6849329    
                               G*gamma   =    0.19539362E-02
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_FF                                                              IPASS= 2


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 2


     zgoubi.plt                                                                      
      already open...

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.85527E+05 V/m
                               B                       =  4.08274E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80519    

                FACE  D'ENTREE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  3.000 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                FACE  DE  SORTIE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  3.000 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                    Integration step :  0.4000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      678.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_FF                                                              IPASS= 2


************************************************************************************************************************************

      7   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 2


 Pgm rebel. At pass #    2/  38.  In element #    5,  parameter # 22  changed to    3.11111111E+00   (was    3.00000000E+00)

 Pgm rebel. At pass #    2/  38.  In element #    5,  parameter # 52  changed to    3.11111111E+00   (was    3.00000000E+00)

                                -----  REBELOTE  -----

     End of pass #        2 through the optical structure 

                     Total of          6 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -1.769E+06  -9.855E+05  -985413.53     -1.966E+05  1.889E-05  WIENFILT   -                    -                   
   5   2    12   8.148E-04   4.083E-03  4.08234424E-03  7.333E-03  7.817E-14  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00   -5.244780E-05    9.99E-01 MARKER     #E_WFSegment_FF      -                    0
  3   1   3     6    0.000000E+00    1.000E+00    1.333311E-06    6.46E-04 MARKER     #E_WFSegment_FF      -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    9.26E-08 MARKER     #E_WFSegment_FF      -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 Fit reached penalty value   2.7525E-09



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 3

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 3


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 3


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     2.3114795     kG*cm
                               beta      =    0.80483736    
                               gamma     =     1.6849329    
                               G*gamma   =    0.19539362E-02
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_FF                                                              IPASS= 3


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 3


     zgoubi.plt                                                                      
      already open...

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.85414E+05 V/m
                               B                       =  4.08234E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80517    

                FACE  D'ENTREE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  3.111 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                FACE  DE  SORTIE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  3.111 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                    Integration step :  0.4000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      678.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_FF                                                              IPASS= 3


************************************************************************************************************************************

      7   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 3


 Pgm rebel. At pass #    3/  38.  In element #    5,  parameter # 22  changed to    3.22222222E+00   (was    3.11111111E+00)

 Pgm rebel. At pass #    3/  38.  In element #    5,  parameter # 52  changed to    3.22222222E+00   (was    3.11111111E+00)

                                -----  REBELOTE  -----

     End of pass #        3 through the optical structure 

                     Total of          9 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -1.769E+06  -9.854E+05  -985296.30     -1.966E+05  4.141E-04  WIENFILT   -                    -                   
   5   2    12   8.148E-04   4.082E-03  4.08193238E-03  7.333E-03  1.713E-12  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00   -5.004517E-05    9.99E-01 MARKER     #E_WFSegment_FF      -                    0
  3   1   3     6    0.000000E+00    1.000E+00    1.272583E-06    6.46E-04 MARKER     #E_WFSegment_FF      -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    8.52E-08 MARKER     #E_WFSegment_FF      -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 Fit reached penalty value   2.5061E-09



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 4

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 4


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 4


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     2.3114795     kG*cm
                               beta      =    0.80483736    
                               gamma     =     1.6849329    
                               G*gamma   =    0.19539362E-02
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_FF                                                              IPASS= 4


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 4


     zgoubi.plt                                                                      
      already open...

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.85296E+05 V/m
                               B                       =  4.08193E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80516    

                FACE  D'ENTREE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  3.222 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                FACE  DE  SORTIE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  3.222 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                    Integration step :  0.4000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      678.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_FF                                                              IPASS= 4


************************************************************************************************************************************

      7   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 4


 Pgm rebel. At pass #    4/  38.  In element #    5,  parameter # 22  changed to    3.33333333E+00   (was    3.22222222E+00)

 Pgm rebel. At pass #    4/  38.  In element #    5,  parameter # 52  changed to    3.33333333E+00   (was    3.22222222E+00)

                                -----  REBELOTE  -----

     End of pass #        4 through the optical structure 

                     Total of         12 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -1.769E+06  -9.853E+05  -985175.06     -1.966E+05  3.857E-06  WIENFILT   -                    -                   
   5   2    12   8.148E-04   4.082E-03  4.08150701E-03  7.333E-03  1.599E-14  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00   -4.755698E-05    9.99E-01 MARKER     #E_WFSegment_FF      -                    0
  3   1   3     6    0.000000E+00    1.000E+00    1.209030E-06    6.46E-04 MARKER     #E_WFSegment_FF      -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    7.43E-08 MARKER     #E_WFSegment_FF      -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 Fit reached penalty value   2.2631E-09



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 5

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 5


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 5


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     2.3114795     kG*cm
                               beta      =    0.80483736    
                               gamma     =     1.6849329    
                               G*gamma   =    0.19539362E-02
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_FF                                                              IPASS= 5


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 5


     zgoubi.plt                                                                      
      already open...

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.85175E+05 V/m
                               B                       =  4.08151E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80514    

                FACE  D'ENTREE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  3.333 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                FACE  DE  SORTIE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  3.333 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                    Integration step :  0.4000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      678.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_FF                                                              IPASS= 5


************************************************************************************************************************************

      7   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 5


 Pgm rebel. At pass #    5/  38.  In element #    5,  parameter # 22  changed to    3.44444444E+00   (was    3.33333333E+00)

 Pgm rebel. At pass #    5/  38.  In element #    5,  parameter # 52  changed to    3.44444444E+00   (was    3.33333333E+00)

                                -----  REBELOTE  -----

     End of pass #        5 through the optical structure 

                     Total of         15 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -1.769E+06  -9.852E+05  -985049.83     -1.966E+05  2.375E-04  WIENFILT   -                    -                   
   5   2    12   8.148E-04   4.082E-03  4.08106818E-03  7.333E-03  9.828E-13  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00   -4.498322E-05    9.99E-01 MARKER     #E_WFSegment_FF      -                    0
  3   1   3     6    0.000000E+00    1.000E+00    1.143820E-06    6.46E-04 MARKER     #E_WFSegment_FF      -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    7.08E-08 MARKER     #E_WFSegment_FF      -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 Fit reached penalty value   2.0248E-09



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 6

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 6


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 6


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     2.3114795     kG*cm
                               beta      =    0.80483736    
                               gamma     =     1.6849329    
                               G*gamma   =    0.19539362E-02
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_FF                                                              IPASS= 6


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 6


     zgoubi.plt                                                                      
      already open...

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.85050E+05 V/m
                               B                       =  4.08107E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80513    

                FACE  D'ENTREE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  3.444 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                FACE  DE  SORTIE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  3.444 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                    Integration step :  0.4000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      678.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_FF                                                              IPASS= 6


************************************************************************************************************************************

      7   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 6


 Pgm rebel. At pass #    6/  38.  In element #    5,  parameter # 22  changed to    3.55555556E+00   (was    3.44444444E+00)

 Pgm rebel. At pass #    6/  38.  In element #    5,  parameter # 52  changed to    3.55555556E+00   (was    3.44444444E+00)

                                -----  REBELOTE  -----

     End of pass #        6 through the optical structure 

                     Total of         18 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -1.769E+06  -9.850E+05  -984920.62     -1.966E+05  3.888E-06  WIENFILT   -                    -                   
   5   2    12   8.148E-04   4.081E-03  4.08061599E-03  7.333E-03  1.610E-14  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00   -4.232390E-05    9.99E-01 MARKER     #E_WFSegment_FF      -                    0
  3   1   3     6    0.000000E+00    1.000E+00    1.076539E-06    6.47E-04 MARKER     #E_WFSegment_FF      -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    6.19E-08 MARKER     #E_WFSegment_FF      -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 Fit reached penalty value   1.7925E-09



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 7

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 7


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 7


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     2.3114795     kG*cm
                               beta      =    0.80483736    
                               gamma     =     1.6849329    
                               G*gamma   =    0.19539362E-02
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_FF                                                              IPASS= 7


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 7


     zgoubi.plt                                                                      
      already open...

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.84921E+05 V/m
                               B                       =  4.08062E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80511    

                FACE  D'ENTREE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  3.556 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                FACE  DE  SORTIE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  3.556 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                    Integration step :  0.4000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      678.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_FF                                                              IPASS= 7


************************************************************************************************************************************

      7   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 7


 Pgm rebel. At pass #    7/  38.  In element #    5,  parameter # 22  changed to    3.66666667E+00   (was    3.55555556E+00)

 Pgm rebel. At pass #    7/  38.  In element #    5,  parameter # 52  changed to    3.66666667E+00   (was    3.55555556E+00)

                                -----  REBELOTE  -----

     End of pass #        7 through the optical structure 

                     Total of         21 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -1.769E+06  -9.849E+05  -984787.43     -1.966E+05  7.102E-04  WIENFILT   -                    -                   
   5   2    12   8.148E-04   4.081E-03  4.08015049E-03  7.333E-03  2.940E-12  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00   -3.957908E-05    9.99E-01 MARKER     #E_WFSegment_FF      -                    0
  3   1   3     6    0.000000E+00    1.000E+00    1.006540E-06    6.46E-04 MARKER     #E_WFSegment_FF      -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    5.56E-08 MARKER     #E_WFSegment_FF      -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 Fit reached penalty value   1.5675E-09



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 8

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 8


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 8


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     2.3114795     kG*cm
                               beta      =    0.80483736    
                               gamma     =     1.6849329    
                               G*gamma   =    0.19539362E-02
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_FF                                                              IPASS= 8


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 8


     zgoubi.plt                                                                      
      already open...

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.84787E+05 V/m
                               B                       =  4.08015E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80509    

                FACE  D'ENTREE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  3.667 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                FACE  DE  SORTIE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  3.667 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                    Integration step :  0.4000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      678.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_FF                                                              IPASS= 8


************************************************************************************************************************************

      7   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 8


 Pgm rebel. At pass #    8/  38.  In element #    5,  parameter # 22  changed to    3.77777778E+00   (was    3.66666667E+00)

 Pgm rebel. At pass #    8/  38.  In element #    5,  parameter # 52  changed to    3.77777778E+00   (was    3.66666667E+00)

                                -----  REBELOTE  -----

     End of pass #        8 through the optical structure 

                     Total of         24 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -1.769E+06  -9.848E+05  -984650.29     -1.966E+05  4.948E-04  WIENFILT   -                    -                   
   5   2    12   8.148E-04   4.080E-03  4.07967175E-03  7.333E-03  2.049E-12  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00   -3.674878E-05    9.99E-01 MARKER     #E_WFSegment_FF      -                    0
  3   1   3     6    0.000000E+00    1.000E+00    9.346765E-07    6.46E-04 MARKER     #E_WFSegment_FF      -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    4.66E-08 MARKER     #E_WFSegment_FF      -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 Fit reached penalty value   1.3513E-09



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 9

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 9


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 9


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     2.3114795     kG*cm
                               beta      =    0.80483736    
                               gamma     =     1.6849329    
                               G*gamma   =    0.19539362E-02
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_FF                                                              IPASS= 9


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 9


     zgoubi.plt                                                                      
      already open...

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.84650E+05 V/m
                               B                       =  4.07967E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80507    

                FACE  D'ENTREE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  3.778 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                FACE  DE  SORTIE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  3.778 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                    Integration step :  0.4000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      678.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_FF                                                              IPASS= 9


************************************************************************************************************************************

      7   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 9


 Pgm rebel. At pass #    9/  38.  In element #    5,  parameter # 22  changed to    3.88888889E+00   (was    3.77777778E+00)

 Pgm rebel. At pass #    9/  38.  In element #    5,  parameter # 52  changed to    3.88888889E+00   (was    3.77777778E+00)

                                -----  REBELOTE  -----

     End of pass #        9 through the optical structure 

                     Total of         27 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -1.769E+06  -9.847E+05  -984509.20     -1.966E+05  6.151E-04  WIENFILT   -                    -                   
   5   2    12   8.148E-04   4.080E-03  4.07917984E-03  7.333E-03  2.548E-12  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00   -3.383305E-05    9.99E-01 MARKER     #E_WFSegment_FF      -                    0
  3   1   3     6    0.000000E+00    1.000E+00    8.605493E-07    6.47E-04 MARKER     #E_WFSegment_FF      -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    4.16E-08 MARKER     #E_WFSegment_FF      -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 Fit reached penalty value   1.1454E-09



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 10

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 10


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 10


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     2.3114795     kG*cm
                               beta      =    0.80483736    
                               gamma     =     1.6849329    
                               G*gamma   =    0.19539362E-02
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_FF                                                              IPASS= 10


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 10


     zgoubi.plt                                                                      
      already open...

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.84509E+05 V/m
                               B                       =  4.07918E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80506    

                FACE  D'ENTREE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  3.889 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                FACE  DE  SORTIE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  3.889 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                    Integration step :  0.4000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      678.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_FF                                                              IPASS= 10


************************************************************************************************************************************

      7   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 10


 Pgm rebel. At pass #   10/  38.  In element #    5,  parameter # 22  changed to    4.00000000E+00   (was    3.88888889E+00)

 Pgm rebel. At pass #   10/  38.  In element #    5,  parameter # 52  changed to    4.00000000E+00   (was    3.88888889E+00)

                                -----  REBELOTE  -----

     End of pass #       10 through the optical structure 

                     Total of         30 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -1.769E+06  -9.845E+05  -984364.17     -1.966E+05  1.997E-04  WIENFILT   -                    -                   
   5   2    12   8.148E-04   4.079E-03  4.07867484E-03  7.333E-03  8.268E-13  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00   -3.083197E-05    9.99E-01 MARKER     #E_WFSegment_FF      -                    0
  3   1   3     6    0.000000E+00    1.000E+00    7.843735E-07    6.47E-04 MARKER     #E_WFSegment_FF      -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    3.23E-08 MARKER     #E_WFSegment_FF      -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 Fit reached penalty value   9.5123E-10



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 11

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 11


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 11


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     2.3114795     kG*cm
                               beta      =    0.80483736    
                               gamma     =     1.6849329    
                               G*gamma   =    0.19539362E-02
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_FF                                                              IPASS= 11


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 11


     zgoubi.plt                                                                      
      already open...

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.84364E+05 V/m
                               B                       =  4.07867E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80504    

                FACE  D'ENTREE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  4.000 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                FACE  DE  SORTIE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  4.000 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                    Integration step :  0.4000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      678.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_FF                                                              IPASS= 11


************************************************************************************************************************************

      7   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 11


 Pgm rebel. At pass #   11/  38.  In element #    5,  parameter # 22  changed to    4.11111111E+00   (was    4.00000000E+00)

 Pgm rebel. At pass #   11/  38.  In element #    5,  parameter # 52  changed to    4.11111111E+00   (was    4.00000000E+00)

                                -----  REBELOTE  -----

     End of pass #       11 through the optical structure 

                     Total of         33 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -1.769E+06  -9.844E+05  -984215.22     -1.966E+05  1.489E-04  WIENFILT   -                    -                   
   5   2    12   8.148E-04   4.079E-03  4.07815682E-03  7.333E-03  6.169E-13  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00   -2.774563E-05    9.99E-01 MARKER     #E_WFSegment_FF      -                    0
  3   1   3     6    0.000000E+00    1.000E+00    7.057941E-07    6.47E-04 MARKER     #E_WFSegment_FF      -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    2.67E-08 MARKER     #E_WFSegment_FF      -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 Fit reached penalty value   7.7032E-10



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 12

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 12


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 12


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     2.3114795     kG*cm
                               beta      =    0.80483736    
                               gamma     =     1.6849329    
                               G*gamma   =    0.19539362E-02
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_FF                                                              IPASS= 12


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 12


     zgoubi.plt                                                                      
      already open...

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.84215E+05 V/m
                               B                       =  4.07816E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80502    

                FACE  D'ENTREE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  4.111 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                FACE  DE  SORTIE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  4.111 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                    Integration step :  0.4000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      678.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_FF                                                              IPASS= 12


************************************************************************************************************************************

      7   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 12


 Pgm rebel. At pass #   12/  38.  In element #    5,  parameter # 22  changed to    4.22222222E+00   (was    4.11111111E+00)

 Pgm rebel. At pass #   12/  38.  In element #    5,  parameter # 52  changed to    4.22222222E+00   (was    4.11111111E+00)

                                -----  REBELOTE  -----

     End of pass #       12 through the optical structure 

                     Total of         36 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -1.769E+06  -9.842E+05  -984062.35     -1.966E+05  4.269E-04  WIENFILT   -                    -                   
   5   2    12   8.148E-04   4.078E-03  4.07762586E-03  7.333E-03  1.769E-12  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00   -2.457411E-05    9.99E-01 MARKER     #E_WFSegment_FF      -                    0
  3   1   3     6    0.000000E+00    1.000E+00    6.251769E-07    6.47E-04 MARKER     #E_WFSegment_FF      -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    1.91E-08 MARKER     #E_WFSegment_FF      -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 Fit reached penalty value   6.0428E-10



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 13

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 13


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 13


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     2.3114795     kG*cm
                               beta      =    0.80483736    
                               gamma     =     1.6849329    
                               G*gamma   =    0.19539362E-02
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_FF                                                              IPASS= 13


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 13


     zgoubi.plt                                                                      
      already open...

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.84062E+05 V/m
                               B                       =  4.07763E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80500    

                FACE  D'ENTREE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  4.222 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                FACE  DE  SORTIE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  4.222 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                    Integration step :  0.4000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      678.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_FF                                                              IPASS= 13


************************************************************************************************************************************

      7   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 13


 Pgm rebel. At pass #   13/  38.  In element #    5,  parameter # 22  changed to    4.33333333E+00   (was    4.22222222E+00)

 Pgm rebel. At pass #   13/  38.  In element #    5,  parameter # 52  changed to    4.33333333E+00   (was    4.22222222E+00)

                                -----  REBELOTE  -----

     End of pass #       13 through the optical structure 

                     Total of         39 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -1.769E+06  -9.841E+05  -983905.59     -1.966E+05  2.053E-05  WIENFILT   -                    -                   
   5   2    12   8.148E-04   4.078E-03  4.07708202E-03  7.333E-03  8.506E-14  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00   -2.131754E-05    9.99E-01 MARKER     #E_WFSegment_FF      -                    0
  3   1   3     6    0.000000E+00    1.000E+00    5.424357E-07    6.47E-04 MARKER     #E_WFSegment_FF      -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    1.62E-08 MARKER     #E_WFSegment_FF      -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 Fit reached penalty value   4.5473E-10



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 14

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 14


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 14


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     2.3114795     kG*cm
                               beta      =    0.80483736    
                               gamma     =     1.6849329    
                               G*gamma   =    0.19539362E-02
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_FF                                                              IPASS= 14


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 14


     zgoubi.plt                                                                      
      already open...

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.83906E+05 V/m
                               B                       =  4.07708E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80498    

                FACE  D'ENTREE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  4.333 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                FACE  DE  SORTIE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  4.333 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                    Integration step :  0.4000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      678.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_FF                                                              IPASS= 14


************************************************************************************************************************************

      7   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 14


 Pgm rebel. At pass #   14/  38.  In element #    5,  parameter # 22  changed to    4.44444444E+00   (was    4.33333333E+00)

 Pgm rebel. At pass #   14/  38.  In element #    5,  parameter # 52  changed to    4.44444444E+00   (was    4.33333333E+00)

                                -----  REBELOTE  -----

     End of pass #       14 through the optical structure 

                     Total of         42 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -1.769E+06  -9.839E+05  -983744.94     -1.966E+05  8.377E-04  WIENFILT   -                    -                   
   5   2    12   8.148E-04   4.077E-03  4.07652539E-03  7.333E-03  3.471E-12  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00   -1.797603E-05    9.99E-01 MARKER     #E_WFSegment_FF      -                    0
  3   1   3     6    0.000000E+00    1.000E+00    4.574571E-07    6.47E-04 MARKER     #E_WFSegment_FF      -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    9.30E-09 MARKER     #E_WFSegment_FF      -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 Fit reached penalty value   3.2335E-10



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 15

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 15


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 15


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     2.3114795     kG*cm
                               beta      =    0.80483736    
                               gamma     =     1.6849329    
                               G*gamma   =    0.19539362E-02
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_FF                                                              IPASS= 15


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 15


     zgoubi.plt                                                                      
      already open...

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.83745E+05 V/m
                               B                       =  4.07653E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80496    

                FACE  D'ENTREE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  4.444 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                FACE  DE  SORTIE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  4.444 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                    Integration step :  0.4000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      678.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_FF                                                              IPASS= 15


************************************************************************************************************************************

      7   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 15


 Pgm rebel. At pass #   15/  38.  In element #    5,  parameter # 22  changed to    4.55555556E+00   (was    4.44444444E+00)

 Pgm rebel. At pass #   15/  38.  In element #    5,  parameter # 52  changed to    4.55555556E+00   (was    4.44444444E+00)

                                -----  REBELOTE  -----

     End of pass #       15 through the optical structure 

                     Total of         45 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -1.769E+06  -9.837E+05  -983580.42     -1.966E+05  1.162E-07  WIENFILT   -                    -                   
   5   2    12   8.148E-04   4.077E-03  4.07595604E-03  7.333E-03  4.814E-16  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00   -1.454973E-05    9.99E-01 MARKER     #E_WFSegment_FF      -                    0
  3   1   3     6    0.000000E+00    1.000E+00    3.703513E-07    6.47E-04 MARKER     #E_WFSegment_FF      -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    7.02E-09 MARKER     #E_WFSegment_FF      -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 Fit reached penalty value   2.1183E-10



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 16

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 16


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 16


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     2.3114795     kG*cm
                               beta      =    0.80483736    
                               gamma     =     1.6849329    
                               G*gamma   =    0.19539362E-02
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_FF                                                              IPASS= 16


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 16


     zgoubi.plt                                                                      
      already open...

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.83580E+05 V/m
                               B                       =  4.07596E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80493    

                FACE  D'ENTREE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  4.556 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                FACE  DE  SORTIE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  4.556 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                    Integration step :  0.4000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      678.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_FF                                                              IPASS= 16


************************************************************************************************************************************

      7   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 16


 Pgm rebel. At pass #   16/  38.  In element #    5,  parameter # 22  changed to    4.66666667E+00   (was    4.55555556E+00)

 Pgm rebel. At pass #   16/  38.  In element #    5,  parameter # 52  changed to    4.66666667E+00   (was    4.55555556E+00)

                                -----  REBELOTE  -----

     End of pass #       16 through the optical structure 

                     Total of         48 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -1.769E+06  -9.836E+05  -983412.03     -1.966E+05  2.318E-04  WIENFILT   -                    -                   
   5   2    12   8.148E-04   4.076E-03  4.07537404E-03  7.333E-03  9.603E-13  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00   -1.103881E-05    9.99E-01 MARKER     #E_WFSegment_FF      -                    0
  3   1   3     6    0.000000E+00    1.000E+00    2.809598E-07    6.47E-04 MARKER     #E_WFSegment_FF      -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    4.86E-09 MARKER     #E_WFSegment_FF      -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 Fit reached penalty value   1.2193E-10



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 17

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 17


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 17


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     2.3114795     kG*cm
                               beta      =    0.80483736    
                               gamma     =     1.6849329    
                               G*gamma   =    0.19539362E-02
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_FF                                                              IPASS= 17


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 17


     zgoubi.plt                                                                      
      already open...

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.83412E+05 V/m
                               B                       =  4.07537E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80491    

                FACE  D'ENTREE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  4.667 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                FACE  DE  SORTIE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  4.667 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                    Integration step :  0.4000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      678.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_FF                                                              IPASS= 17


************************************************************************************************************************************

      7   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 17


 Pgm rebel. At pass #   17/  38.  In element #    5,  parameter # 22  changed to    4.77777778E+00   (was    4.66666667E+00)

 Pgm rebel. At pass #   17/  38.  In element #    5,  parameter # 52  changed to    4.77777778E+00   (was    4.66666667E+00)

                                -----  REBELOTE  -----

     End of pass #       17 through the optical structure 

                     Total of         51 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -1.769E+06  -9.834E+05  -983239.80     -1.966E+05  7.975E-06  WIENFILT   -                    -                   
   5   2    12   8.148E-04   4.075E-03  4.07477948E-03  7.333E-03  3.305E-14  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00   -7.443415E-06    9.99E-01 MARKER     #E_WFSegment_FF      -                    0
  3   1   3     6    0.000000E+00    1.000E+00    1.895335E-07    6.48E-04 MARKER     #E_WFSegment_FF      -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    1.33E-09 MARKER     #E_WFSegment_FF      -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 Fit reached penalty value   5.5440E-11



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 18

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 18


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 18


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     2.3114795     kG*cm
                               beta      =    0.80483736    
                               gamma     =     1.6849329    
                               G*gamma   =    0.19539362E-02
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_FF                                                              IPASS= 18


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 18


     zgoubi.plt                                                                      
      already open...

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.83240E+05 V/m
                               B                       =  4.07478E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80489    

                FACE  D'ENTREE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  4.778 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                FACE  DE  SORTIE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  4.778 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                    Integration step :  0.4000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      678.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_FF                                                              IPASS= 18


************************************************************************************************************************************

      7   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 18


 Pgm rebel. At pass #   18/  38.  In element #    5,  parameter # 22  changed to    4.88888889E+00   (was    4.77777778E+00)

 Pgm rebel. At pass #   18/  38.  In element #    5,  parameter # 52  changed to    4.88888889E+00   (was    4.77777778E+00)

                                -----  REBELOTE  -----

     End of pass #       18 through the optical structure 

                     Total of         54 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -1.769E+06  -9.832E+05  -983063.74     -1.966E+05  1.186E-04  WIENFILT   -                    -                   
   5   2    12   8.148E-04   4.075E-03  4.07417243E-03  7.333E-03  4.908E-13  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00   -3.763748E-06    9.99E-01 MARKER     #E_WFSegment_FF      -                    0
  3   1   3     6    0.000000E+00    1.000E+00    9.581956E-08    6.48E-04 MARKER     #E_WFSegment_FF      -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    7.85E-10 MARKER     #E_WFSegment_FF      -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 Fit reached penalty value   1.4175E-11



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 19

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 19


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 19


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     2.3114795     kG*cm
                               beta      =    0.80483736    
                               gamma     =     1.6849329    
                               G*gamma   =    0.19539362E-02
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_FF                                                              IPASS= 19


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 19


     zgoubi.plt                                                                      
      already open...

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.83064E+05 V/m
                               B                       =  4.07417E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80486    

                FACE  D'ENTREE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  4.889 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                FACE  DE  SORTIE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  4.889 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                    Integration step :  0.4000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000    -0.000     0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      678.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_FF                                                              IPASS= 19


************************************************************************************************************************************

      7   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 19


 Pgm rebel. At pass #   19/  38.  In element #    5,  parameter # 22  changed to    5.00000000E+00   (was    4.88888889E+00)

 Pgm rebel. At pass #   19/  38.  In element #    5,  parameter # 52  changed to    5.00000000E+00   (was    4.88888889E+00)

                                -----  REBELOTE  -----

     End of pass #       19 through the optical structure 

                     Total of         57 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -1.769E+06  -9.831E+05  -982883.86     -1.966E+05  9.330E-02  WIENFILT   -                    -                   
   5   2    12   8.148E-04   4.074E-03  4.07355300E-03  7.333E-03  3.868E-10  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    7.261142E-10    3.72E-08 MARKER     #E_WFSegment_FF      -                    0
  3   1   3     6    0.000000E+00    1.000E+00    2.851889E-08    5.74E-05 MARKER     #E_WFSegment_FF      -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    7.45E-07 MARKER     #E_WFSegment_FF      -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 Fit reached penalty value   8.2441E-16



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 20

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 20


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 20


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     2.3114795     kG*cm
                               beta      =    0.80483736    
                               gamma     =     1.6849329    
                               G*gamma   =    0.19539362E-02
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_FF                                                              IPASS= 20


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 20


     zgoubi.plt                                                                      
      already open...

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.82884E+05 V/m
                               B                       =  4.07355E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80484    

                FACE  D'ENTREE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  5.000 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                FACE  DE  SORTIE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  5.000 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                    Integration step :  0.4000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000     0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000     0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000     0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      675.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_FF                                                              IPASS= 20


************************************************************************************************************************************

      7   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 20


 Pgm rebel. At pass #   20/  38.  In element #    5,  parameter # 22  changed to    5.11111111E+00   (was    5.00000000E+00)

 Pgm rebel. At pass #   20/  38.  In element #    5,  parameter # 52  changed to    5.11111111E+00   (was    5.00000000E+00)

                                -----  REBELOTE  -----

     End of pass #       20 through the optical structure 

                     Total of         60 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -1.769E+06  -9.829E+05  -982700.16     -1.966E+05  3.658E-04  WIENFILT   -                    -                   
   5   2    12   8.148E-04   4.074E-03  4.07292119E-03  7.333E-03  1.516E-12  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    3.847615E-06    9.99E-01 MARKER     #E_WFSegment_FF      -                    0
  3   1   3     6    0.000000E+00    1.000E+00   -9.796733E-08    6.48E-04 MARKER     #E_WFSegment_FF      -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    7.86E-10 MARKER     #E_WFSegment_FF      -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 Fit reached penalty value   1.4814E-11



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 21

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 21


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 21


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     2.3114795     kG*cm
                               beta      =    0.80483736    
                               gamma     =     1.6849329    
                               G*gamma   =    0.19539362E-02
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_FF                                                              IPASS= 21


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 21


     zgoubi.plt                                                                      
      already open...

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.82700E+05 V/m
                               B                       =  4.07292E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80481    

                FACE  D'ENTREE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  5.111 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                FACE  DE  SORTIE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  5.111 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                    Integration step :  0.4000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      678.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_FF                                                              IPASS= 21


************************************************************************************************************************************

      7   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 21


 Pgm rebel. At pass #   21/  38.  In element #    5,  parameter # 22  changed to    5.22222222E+00   (was    5.11111111E+00)

 Pgm rebel. At pass #   21/  38.  In element #    5,  parameter # 52  changed to    5.22222222E+00   (was    5.11111111E+00)

                                -----  REBELOTE  -----

     End of pass #       21 through the optical structure 

                     Total of         63 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -1.769E+06  -9.827E+05  -982512.68     -1.966E+05  3.225E-04  WIENFILT   -                    -                   
   5   2    12   8.148E-04   4.073E-03  4.07227715E-03  7.333E-03  1.337E-12  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    7.778867E-06    9.99E-01 MARKER     #E_WFSegment_FF      -                    0
  3   1   3     6    0.000000E+00    1.000E+00   -1.980889E-07    6.48E-04 MARKER     #E_WFSegment_FF      -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    1.70E-09 MARKER     #E_WFSegment_FF      -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 Fit reached penalty value   6.0550E-11



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 22

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 22


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 22


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     2.3114795     kG*cm
                               beta      =    0.80483736    
                               gamma     =     1.6849329    
                               G*gamma   =    0.19539362E-02
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_FF                                                              IPASS= 22


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 22


     zgoubi.plt                                                                      
      already open...

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.82513E+05 V/m
                               B                       =  4.07228E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80479    

                FACE  D'ENTREE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  5.222 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                FACE  DE  SORTIE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  5.222 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                    Integration step :  0.4000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      678.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_FF                                                              IPASS= 22


************************************************************************************************************************************

      7   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 22


 Pgm rebel. At pass #   22/  38.  In element #    5,  parameter # 22  changed to    5.33333333E+00   (was    5.22222222E+00)

 Pgm rebel. At pass #   22/  38.  In element #    5,  parameter # 52  changed to    5.33333333E+00   (was    5.22222222E+00)

                                -----  REBELOTE  -----

     End of pass #       22 through the optical structure 

                     Total of         66 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -1.769E+06  -9.825E+05  -982321.42     -1.966E+05  3.219E-04  WIENFILT   -                    -                   
   5   2    12   8.148E-04   4.072E-03  4.07162093E-03  7.333E-03  1.335E-12  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    1.179350E-05    9.99E-01 MARKER     #E_WFSegment_FF      -                    0
  3   1   3     6    0.000000E+00    1.000E+00   -3.003876E-07    6.48E-04 MARKER     #E_WFSegment_FF      -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    3.34E-09 MARKER     #E_WFSegment_FF      -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 Fit reached penalty value   1.3918E-10



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 23

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 23


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 23


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     2.3114795     kG*cm
                               beta      =    0.80483736    
                               gamma     =     1.6849329    
                               G*gamma   =    0.19539362E-02
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_FF                                                              IPASS= 23


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 23


     zgoubi.plt                                                                      
      already open...

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.82321E+05 V/m
                               B                       =  4.07162E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80476    

                FACE  D'ENTREE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  5.333 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                FACE  DE  SORTIE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  5.333 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                    Integration step :  0.4000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      678.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_FF                                                              IPASS= 23


************************************************************************************************************************************

      7   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 23


 Pgm rebel. At pass #   23/  38.  In element #    5,  parameter # 22  changed to    5.44444444E+00   (was    5.33333333E+00)

 Pgm rebel. At pass #   23/  38.  In element #    5,  parameter # 52  changed to    5.44444444E+00   (was    5.33333333E+00)

                                -----  REBELOTE  -----

     End of pass #       23 through the optical structure 

                     Total of         69 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -1.769E+06  -9.823E+05  -982126.40     -1.966E+05  6.771E-04  WIENFILT   -                    -                   
   5   2    12   8.148E-04   4.072E-03  4.07095263E-03  7.333E-03  2.806E-12  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    1.589125E-05    9.99E-01 MARKER     #E_WFSegment_FF      -                    0
  3   1   3     6    0.000000E+00    1.000E+00   -4.047999E-07    6.48E-04 MARKER     #E_WFSegment_FF      -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    8.44E-09 MARKER     #E_WFSegment_FF      -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 Fit reached penalty value   2.5270E-10



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 24

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 24


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 24


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     2.3114795     kG*cm
                               beta      =    0.80483736    
                               gamma     =     1.6849329    
                               G*gamma   =    0.19539362E-02
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_FF                                                              IPASS= 24


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 24


     zgoubi.plt                                                                      
      already open...

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.82126E+05 V/m
                               B                       =  4.07095E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80473    

                FACE  D'ENTREE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  5.444 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                FACE  DE  SORTIE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  5.444 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                    Integration step :  0.4000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      678.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_FF                                                              IPASS= 24


************************************************************************************************************************************

      7   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 24


 Pgm rebel. At pass #   24/  38.  In element #    5,  parameter # 22  changed to    5.55555556E+00   (was    5.44444444E+00)

 Pgm rebel. At pass #   24/  38.  In element #    5,  parameter # 52  changed to    5.55555556E+00   (was    5.44444444E+00)

                                -----  REBELOTE  -----

     End of pass #       24 through the optical structure 

                     Total of         72 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -1.769E+06  -9.821E+05  -981927.63     -1.966E+05  3.602E-05  WIENFILT   -                    -                   
   5   2    12   8.148E-04   4.071E-03  4.07027231E-03  7.333E-03  1.493E-13  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    2.007179E-05    9.99E-01 MARKER     #E_WFSegment_FF      -                    0
  3   1   3     6    0.000000E+00    1.000E+00   -5.112675E-07    6.48E-04 MARKER     #E_WFSegment_FF      -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    1.33E-08 MARKER     #E_WFSegment_FF      -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 Fit reached penalty value   4.0314E-10



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 25

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 25


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 25


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     2.3114795     kG*cm
                               beta      =    0.80483736    
                               gamma     =     1.6849329    
                               G*gamma   =    0.19539362E-02
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_FF                                                              IPASS= 25


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 25


     zgoubi.plt                                                                      
      already open...

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.81928E+05 V/m
                               B                       =  4.07027E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80470    

                FACE  D'ENTREE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  5.556 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                FACE  DE  SORTIE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  5.556 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                    Integration step :  0.4000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      678.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_FF                                                              IPASS= 25


************************************************************************************************************************************

      7   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 25


 Pgm rebel. At pass #   25/  38.  In element #    5,  parameter # 22  changed to    5.66666667E+00   (was    5.55555556E+00)

 Pgm rebel. At pass #   25/  38.  In element #    5,  parameter # 52  changed to    5.66666667E+00   (was    5.55555556E+00)

                                -----  REBELOTE  -----

     End of pass #       25 through the optical structure 

                     Total of         75 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -1.769E+06  -9.819E+05  -981725.13     -1.966E+05  5.826E-06  WIENFILT   -                    -                   
   5   2    12   8.148E-04   4.070E-03  4.06958006E-03  7.333E-03  2.416E-14  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    2.433473E-05    9.99E-01 MARKER     #E_WFSegment_FF      -                    0
  3   1   3     6    0.000000E+00    1.000E+00   -6.199707E-07    6.49E-04 MARKER     #E_WFSegment_FF      -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    2.08E-08 MARKER     #E_WFSegment_FF      -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 Fit reached penalty value   5.9256E-10



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 26

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 26


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 26


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     2.3114795     kG*cm
                               beta      =    0.80483736    
                               gamma     =     1.6849329    
                               G*gamma   =    0.19539362E-02
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_FF                                                              IPASS= 26


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 26


     zgoubi.plt                                                                      
      already open...

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.81725E+05 V/m
                               B                       =  4.06958E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80467    

                FACE  D'ENTREE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  5.667 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                FACE  DE  SORTIE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  5.667 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                    Integration step :  0.4000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      678.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_FF                                                              IPASS= 26


************************************************************************************************************************************

      7   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 26


 Pgm rebel. At pass #   26/  38.  In element #    5,  parameter # 22  changed to    5.77777778E+00   (was    5.66666667E+00)

 Pgm rebel. At pass #   26/  38.  In element #    5,  parameter # 52  changed to    5.77777778E+00   (was    5.66666667E+00)

                                -----  REBELOTE  -----

     End of pass #       26 through the optical structure 

                     Total of         78 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -1.769E+06  -9.817E+05  -981518.90     -1.966E+05  1.693E-04  WIENFILT   -                    -                   
   5   2    12   8.148E-04   4.070E-03  4.06887596E-03  7.333E-03  7.014E-13  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    2.867963E-05    9.99E-01 MARKER     #E_WFSegment_FF      -                    0
  3   1   3     6    0.000000E+00    1.000E+00   -7.306476E-07    6.49E-04 MARKER     #E_WFSegment_FF      -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    2.95E-08 MARKER     #E_WFSegment_FF      -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 Fit reached penalty value   8.2305E-10



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 27

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 27


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 27


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     2.3114795     kG*cm
                               beta      =    0.80483736    
                               gamma     =     1.6849329    
                               G*gamma   =    0.19539362E-02
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_FF                                                              IPASS= 27


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 27


     zgoubi.plt                                                                      
      already open...

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.81519E+05 V/m
                               B                       =  4.06888E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80464    

                FACE  D'ENTREE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  5.778 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                FACE  DE  SORTIE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  5.778 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                    Integration step :  0.4000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      678.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_FF                                                              IPASS= 27


************************************************************************************************************************************

      7   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 27


 Pgm rebel. At pass #   27/  38.  In element #    5,  parameter # 22  changed to    5.88888889E+00   (was    5.77777778E+00)

 Pgm rebel. At pass #   27/  38.  In element #    5,  parameter # 52  changed to    5.88888889E+00   (was    5.77777778E+00)

                                -----  REBELOTE  -----

     End of pass #       27 through the optical structure 

                     Total of         81 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -1.769E+06  -9.815E+05  -981308.98     -1.966E+05  2.417E-05  WIENFILT   -                    -                   
   5   2    12   8.148E-04   4.069E-03  4.06816011E-03  7.333E-03  9.976E-14  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    3.310590E-05    9.99E-01 MARKER     #E_WFSegment_FF      -                    0
  3   1   3     6    0.000000E+00    1.000E+00   -8.434918E-07    6.49E-04 MARKER     #E_WFSegment_FF      -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    3.62E-08 MARKER     #E_WFSegment_FF      -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 Fit reached penalty value   1.0967E-09



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 28

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 28


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 28


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     2.3114795     kG*cm
                               beta      =    0.80483736    
                               gamma     =     1.6849329    
                               G*gamma   =    0.19539362E-02
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_FF                                                              IPASS= 28


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 28


     zgoubi.plt                                                                      
      already open...

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.81309E+05 V/m
                               B                       =  4.06816E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80461    

                FACE  D'ENTREE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  5.889 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                FACE  DE  SORTIE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  5.889 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                    Integration step :  0.4000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      678.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_FF                                                              IPASS= 28


************************************************************************************************************************************

      7   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 28


 Pgm rebel. At pass #   28/  38.  In element #    5,  parameter # 22  changed to    6.00000000E+00   (was    5.88888889E+00)

 Pgm rebel. At pass #   28/  38.  In element #    5,  parameter # 52  changed to    6.00000000E+00   (was    5.88888889E+00)

                                -----  REBELOTE  -----

     End of pass #       28 through the optical structure 

                     Total of         84 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -1.769E+06  -9.813E+05  -981095.36     -1.966E+05  1.287E-04  WIENFILT   -                    -                   
   5   2    12   8.148E-04   4.068E-03  4.06743257E-03  7.333E-03  5.337E-13  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    3.761283E-05    9.99E-01 MARKER     #E_WFSegment_FF      -                    0
  3   1   3     6    0.000000E+00    1.000E+00   -9.584573E-07    6.49E-04 MARKER     #E_WFSegment_FF      -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    4.72E-08 MARKER     #E_WFSegment_FF      -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 Fit reached penalty value   1.4156E-09



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 29

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 29


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 29


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     2.3114795     kG*cm
                               beta      =    0.80483736    
                               gamma     =     1.6849329    
                               G*gamma   =    0.19539362E-02
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_FF                                                              IPASS= 29


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 29


     zgoubi.plt                                                                      
      already open...

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.81095E+05 V/m
                               B                       =  4.06743E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80458    

                FACE  D'ENTREE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  6.000 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                FACE  DE  SORTIE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  6.000 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                    Integration step :  0.4000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      678.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_FF                                                              IPASS= 29


************************************************************************************************************************************

      7   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 29


 Pgm rebel. At pass #   29/  38.  In element #    5,  parameter # 22  changed to    6.11111111E+00   (was    6.00000000E+00)

 Pgm rebel. At pass #   29/  38.  In element #    5,  parameter # 52  changed to    6.11111111E+00   (was    6.00000000E+00)

                                -----  REBELOTE  -----

     End of pass #       29 through the optical structure 

                     Total of         87 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -1.769E+06  -9.811E+05  -980878.08     -1.966E+05  6.881E-05  WIENFILT   -                    -                   
   5   2    12   8.148E-04   4.067E-03  4.06669345E-03  7.333E-03  2.856E-13  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    4.219955E-05    9.99E-01 MARKER     #E_WFSegment_FF      -                    0
  3   1   3     6    0.000000E+00    1.000E+00   -1.075553E-06    6.49E-04 MARKER     #E_WFSegment_FF      -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    6.24E-08 MARKER     #E_WFSegment_FF      -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 Fit reached penalty value   1.7820E-09



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 30

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 30


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 30


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     2.3114795     kG*cm
                               beta      =    0.80483736    
                               gamma     =     1.6849329    
                               G*gamma   =    0.19539362E-02
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_FF                                                              IPASS= 30


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 30


     zgoubi.plt                                                                      
      already open...

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.80878E+05 V/m
                               B                       =  4.06669E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80455    

                FACE  D'ENTREE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  6.111 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                FACE  DE  SORTIE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  6.111 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                    Integration step :  0.4000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      678.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_FF                                                              IPASS= 30


************************************************************************************************************************************

      7   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 30


 Pgm rebel. At pass #   30/  38.  In element #    5,  parameter # 22  changed to    6.22222222E+00   (was    6.11111111E+00)

 Pgm rebel. At pass #   30/  38.  In element #    5,  parameter # 52  changed to    6.22222222E+00   (was    6.11111111E+00)

                                -----  REBELOTE  -----

     End of pass #       30 through the optical structure 

                     Total of         90 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -1.769E+06  -9.809E+05  -980657.14     -1.966E+05  9.186E-05  WIENFILT   -                    -                   
   5   2    12   8.148E-04   4.067E-03  4.06594285E-03  7.333E-03  3.815E-13  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    4.686499E-05    9.99E-01 MARKER     #E_WFSegment_FF      -                    0
  3   1   3     6    0.000000E+00    1.000E+00   -1.194606E-06    6.49E-04 MARKER     #E_WFSegment_FF      -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    7.57E-08 MARKER     #E_WFSegment_FF      -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    5.61E-24 MARKER     #E_WFSegment_FF      -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    5.61E-24 MARKER     #E_WFSegment_FF      -                    0
 Fit reached penalty value   2.1978E-09



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 31

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 31


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 31


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     2.3114795     kG*cm
                               beta      =    0.80483736    
                               gamma     =     1.6849329    
                               G*gamma   =    0.19539362E-02
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_FF                                                              IPASS= 31


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 31


     zgoubi.plt                                                                      
      already open...

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.80657E+05 V/m
                               B                       =  4.06594E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80452    

                FACE  D'ENTREE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  6.222 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                FACE  DE  SORTIE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  6.222 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                    Integration step :  0.4000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      678.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_FF                                                              IPASS= 31


************************************************************************************************************************************

      7   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 31


 Pgm rebel. At pass #   31/  38.  In element #    5,  parameter # 22  changed to    6.33333333E+00   (was    6.22222222E+00)

 Pgm rebel. At pass #   31/  38.  In element #    5,  parameter # 52  changed to    6.33333333E+00   (was    6.22222222E+00)

                                -----  REBELOTE  -----

     End of pass #       31 through the optical structure 

                     Total of         93 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -1.769E+06  -9.807E+05  -980432.56     -1.966E+05  2.961E-05  WIENFILT   -                    -                   
   5   2    12   8.148E-04   4.066E-03  4.06518087E-03  7.333E-03  1.231E-13  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    5.160787E-05    9.99E-01 MARKER     #E_WFSegment_FF      -                    0
  3   1   3     6    0.000000E+00    1.000E+00   -1.315503E-06    6.49E-04 MARKER     #E_WFSegment_FF      -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    9.00E-08 MARKER     #E_WFSegment_FF      -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 Fit reached penalty value   2.6651E-09



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 32

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 32


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 32


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     2.3114795     kG*cm
                               beta      =    0.80483736    
                               gamma     =     1.6849329    
                               G*gamma   =    0.19539362E-02
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_FF                                                              IPASS= 32


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 32


     zgoubi.plt                                                                      
      already open...

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.80433E+05 V/m
                               B                       =  4.06518E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80448    

                FACE  D'ENTREE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  6.333 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                FACE  DE  SORTIE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  6.333 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                    Integration step :  0.4000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      678.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_FF                                                              IPASS= 32


************************************************************************************************************************************

      7   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 32


 Pgm rebel. At pass #   32/  38.  In element #    5,  parameter # 22  changed to    6.44444444E+00   (was    6.33333333E+00)

 Pgm rebel. At pass #   32/  38.  In element #    5,  parameter # 52  changed to    6.44444444E+00   (was    6.33333333E+00)

                                -----  REBELOTE  -----

     End of pass #       32 through the optical structure 

                     Total of         96 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -1.769E+06  -9.804E+05  -980204.36     -1.966E+05  5.748E-04  WIENFILT   -                    -                   
   5   2    12   8.148E-04   4.065E-03  4.06440763E-03  7.333E-03  2.386E-12  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    5.642671E-05    9.99E-01 MARKER     #E_WFSegment_FF      -                    0
  3   1   3     6    0.000000E+00    1.000E+00   -1.438620E-06    6.50E-04 MARKER     #E_WFSegment_FF      -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    1.10E-07 MARKER     #E_WFSegment_FF      -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 Fit reached penalty value   3.1860E-09



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 33

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 33


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 33


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     2.3114795     kG*cm
                               beta      =    0.80483736    
                               gamma     =     1.6849329    
                               G*gamma   =    0.19539362E-02
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_FF                                                              IPASS= 33


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 33


     zgoubi.plt                                                                      
      already open...

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.80204E+05 V/m
                               B                       =  4.06441E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80445    

                FACE  D'ENTREE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  6.444 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                FACE  DE  SORTIE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  6.444 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                    Integration step :  0.4000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      678.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_FF                                                              IPASS= 33


************************************************************************************************************************************

      7   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 33


 Pgm rebel. At pass #   33/  38.  In element #    5,  parameter # 22  changed to    6.55555556E+00   (was    6.44444444E+00)

 Pgm rebel. At pass #   33/  38.  In element #    5,  parameter # 52  changed to    6.55555556E+00   (was    6.44444444E+00)

                                -----  REBELOTE  -----

     End of pass #       33 through the optical structure 

                     Total of         99 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -1.769E+06  -9.802E+05  -979972.57     -1.966E+05  1.073E-03  WIENFILT   -                    -                   
   5   2    12   8.148E-04   4.064E-03  4.06362330E-03  7.333E-03  4.456E-12  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    6.131988E-05    9.99E-01 MARKER     #E_WFSegment_FF      -                    0
  3   1   3     6    0.000000E+00    1.000E+00   -1.563280E-06    6.50E-04 MARKER     #E_WFSegment_FF      -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235988E-01    1.25E-07 MARKER     #E_WFSegment_FF      -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 Fit reached penalty value   3.7626E-09



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 34

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 34


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 34


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     2.3114795     kG*cm
                               beta      =    0.80483736    
                               gamma     =     1.6849329    
                               G*gamma   =    0.19539362E-02
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_FF                                                              IPASS= 34


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 34


     zgoubi.plt                                                                      
      already open...

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.79973E+05 V/m
                               B                       =  4.06362E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80441    

                FACE  D'ENTREE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  6.556 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                FACE  DE  SORTIE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  6.556 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                    Integration step :  0.4000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      678.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_FF                                                              IPASS= 34


************************************************************************************************************************************

      7   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 34


 Pgm rebel. At pass #   34/  38.  In element #    5,  parameter # 22  changed to    6.66666667E+00   (was    6.55555556E+00)

 Pgm rebel. At pass #   34/  38.  In element #    5,  parameter # 52  changed to    6.66666667E+00   (was    6.55555556E+00)

                                -----  REBELOTE  -----

     End of pass #       34 through the optical structure 

                     Total of        102 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -1.769E+06  -9.800E+05  -979737.20     -1.966E+05  5.155E-05  WIENFILT   -                    -                   
   5   2    12   8.148E-04   4.064E-03  4.06282803E-03  7.333E-03  2.140E-13  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    6.628552E-05    9.99E-01 MARKER     #E_WFSegment_FF      -                    0
  3   1   3     6    0.000000E+00    1.000E+00   -1.690317E-06    6.50E-04 MARKER     #E_WFSegment_FF      -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235987E-01    1.50E-07 MARKER     #E_WFSegment_FF      -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 Fit reached penalty value   4.3966E-09



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 35

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 35


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 35


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     2.3114795     kG*cm
                               beta      =    0.80483736    
                               gamma     =     1.6849329    
                               G*gamma   =    0.19539362E-02
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_FF                                                              IPASS= 35


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 35


     zgoubi.plt                                                                      
      already open...

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.79737E+05 V/m
                               B                       =  4.06283E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80438    

                FACE  D'ENTREE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  6.667 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                FACE  DE  SORTIE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  6.667 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                    Integration step :  0.4000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      678.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_FF                                                              IPASS= 35


************************************************************************************************************************************

      7   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 35


 Pgm rebel. At pass #   35/  38.  In element #    5,  parameter # 22  changed to    6.77777778E+00   (was    6.66666667E+00)

 Pgm rebel. At pass #   35/  38.  In element #    5,  parameter # 52  changed to    6.77777778E+00   (was    6.66666667E+00)

                                -----  REBELOTE  -----

     End of pass #       35 through the optical structure 

                     Total of        105 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -1.769E+06  -9.797E+05  -979498.28     -1.966E+05  9.728E-04  WIENFILT   -                    -                   
   5   2    12   8.148E-04   4.063E-03  4.06202203E-03  7.333E-03  4.040E-12  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    7.132173E-05    9.99E-01 MARKER     #E_WFSegment_FF      -                    0
  3   1   3     6    0.000000E+00    1.000E+00   -1.819045E-06    6.50E-04 MARKER     #E_WFSegment_FF      -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235987E-01    1.71E-07 MARKER     #E_WFSegment_FF      -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 Fit reached penalty value   5.0901E-09



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 36

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 36


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 36


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     2.3114795     kG*cm
                               beta      =    0.80483736    
                               gamma     =     1.6849329    
                               G*gamma   =    0.19539362E-02
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_FF                                                              IPASS= 36


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 36


     zgoubi.plt                                                                      
      already open...

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.79498E+05 V/m
                               B                       =  4.06202E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80434    

                FACE  D'ENTREE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  6.778 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                FACE  DE  SORTIE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  6.778 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                    Integration step :  0.4000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      678.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_FF                                                              IPASS= 36


************************************************************************************************************************************

      7   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 36


 Pgm rebel. At pass #   36/  38.  In element #    5,  parameter # 22  changed to    6.88888889E+00   (was    6.77777778E+00)

 Pgm rebel. At pass #   36/  38.  In element #    5,  parameter # 52  changed to    6.88888889E+00   (was    6.77777778E+00)

                                -----  REBELOTE  -----

     End of pass #       36 through the optical structure 

                     Total of        108 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -1.769E+06  -9.795E+05  -979255.83     -1.966E+05  4.618E-04  WIENFILT   -                    -                   
   5   2    12   8.148E-04   4.062E-03  4.06120554E-03  7.333E-03  1.916E-12  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    7.642652E-05    9.99E-01 MARKER     #E_WFSegment_FF      -                    0
  3   1   3     6    0.000000E+00    1.000E+00   -1.949384E-06    6.50E-04 MARKER     #E_WFSegment_FF      -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235987E-01    1.97E-07 MARKER     #E_WFSegment_FF      -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 Fit reached penalty value   5.8448E-09



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 37

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 37


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 37


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     2.3114795     kG*cm
                               beta      =    0.80483736    
                               gamma     =     1.6849329    
                               G*gamma   =    0.19539362E-02
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_FF                                                              IPASS= 37


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 37


     zgoubi.plt                                                                      
      already open...

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.79256E+05 V/m
                               B                       =  4.06121E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80430    

                FACE  D'ENTREE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  6.889 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                FACE  DE  SORTIE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  6.889 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                    Integration step :  0.4000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      678.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_FF                                                              IPASS= 37


************************************************************************************************************************************

      7   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 37


 Pgm rebel. At pass #   37/  38.  In element #    5,  parameter # 22  changed to    7.00000000E+00   (was    6.88888889E+00)

 Pgm rebel. At pass #   37/  38.  In element #    5,  parameter # 52  changed to    7.00000000E+00   (was    6.88888889E+00)

                                -----  REBELOTE  -----

     End of pass #       37 through the optical structure 

                     Total of        111 particles have been launched


      Next  pass  is  #    38 and  last  pass  through  the  optical  structure


     WRITE statements to zgoubi.res are re-established from now on.

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 38

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 38


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 38


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     2.3114795     kG*cm
                               beta      =    0.80483736    
                               gamma     =     1.6849329    
                               G*gamma   =    0.19539362E-02
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_FF                                                              IPASS= 38


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 38


     zgoubi.plt                                                                      
      already open...

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.79256E+05 V/m
                               B                       =  4.06121E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80430    

                FACE  D'ENTREE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  7.000 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                FACE  DE  SORTIE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  7.000 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                    Integration step :  0.4000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.001     0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.001     0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.001     0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      678.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   4.144491E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_FF                                                              IPASS= 38


************************************************************************************************************************************
      7  Keyword, label(s) :  FIT2                                                                                     IPASS= 38


     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 38

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 38


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 38


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     2.3114795     kG*cm
                               beta      =    0.80483736    
                               gamma     =     1.6849329    
                               G*gamma   =    0.19539362E-02
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_FF                                                              IPASS= 38


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 38


     zgoubi.plt                                                                      
      already open...

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.79256E+05 V/m
                               B                       =  4.06121E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80430    

                FACE  D'ENTREE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  7.000 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                FACE  DE  SORTIE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  7.000 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                    Integration step :  0.4000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.001     0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.001     0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.001     0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      678.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_FF                                                              IPASS= 38


************************************************************************************************************************************
      7  Keyword, label(s) :  FIT2                                                                                     IPASS= 38

     Pgm main. FITING=.T., FIT procedure launched.

           variable #            1       IR =            5 ,   ok.
           variable #            1       IP =           11 ,   ok.
           variable #            2       IR =            5 ,   ok.
           variable #            2       IP =           12 ,   ok.
           constraint #            1       IR =            6 ,   ok.
           constraint #            1       I  =            1 ,   ok.
           constraint #            2       IR =            6 ,   ok.
           constraint #            2       I  =            1 ,   ok.
           constraint #            3       IR =            6 ,   ok.
           constraint #            4       IR =            6 ,   ok.
           constraint #            5       IR =            6 ,   ok.

                    FIT  variables  and  constraints  in  good  order,  FIT  will proceed. 
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   5   1    11  -1.769E+06  -9.793E+05  -979009.90     -1.966E+05  2.696E-04  WIENFILT   -                    -                   
   5   2    12   8.148E-04   4.061E-03  4.06037886E-03  7.333E-03  1.121E-12  WIENFILT   -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    8.159792E-05    9.99E-01 MARKER     #E_WFSegment_FF      -                    0
  3   1   3     6    0.000000E+00    1.000E+00   -2.081237E-06    6.50E-04 MARKER     #E_WFSegment_FF      -                    0
 10   1   1     6    5.235988E-01    1.000E+00    5.235987E-01    2.25E-07 MARKER     #E_WFSegment_FF      -                    0
 10   1   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 10   2   4     6    1.000000E+00    1.000E+00    1.000000E+00    0.00E+00 MARKER     #E_WFSegment_FF      -                    0
 Fit reached penalty value   6.6626E-09



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 38

                          MAGNETIC  RIGIDITY =          2.311 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 38


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 38


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     2.3114795     kG*cm
                               beta      =    0.80483736    
                               gamma     =     1.6849329    
                               G*gamma   =    0.19539362E-02
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_WFSegment_FF                                                              IPASS= 38


************************************************************************************************************************************
      5  Keyword, label(s) :  WIENFILT                                                                                 IPASS= 38


     zgoubi.plt                                                                      
      already open...

                          ------ SEPARATEUR ELECTROSTATIQUE ------
                                        HORIZONTAL
                               Length                  =  0.50000     m
                               E                       = -9.79010E+05 V/m
                               B                       =  4.06038E-03 T

                               Mass  of  particles     =  0.51100     MeV/c2

                               Charge of  particles    =  1.60218E-19 C
                               Reference  beta         =  0.80484    
                               BETA  WANTED = -E/c.B = 0.80427    

                FACE  D'ENTREE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  7.000 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                FACE  DE  SORTIE
                DX =  20.000,  LAMBDA_E, LAMBDA_B =   5.000  7.000 CM    (hard-edge if LAMBDA_E * LAMBDA_B = 0 )
                COEFFICIENTS DE E :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)
                COEFFICIENTS DE B :  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000  (unused if hard-edge)

                    Integration step :  0.4000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000           90.000     0.000    -0.000    -0.000    -0.000            3


                CONDITIONS  DE  MAXWELL  (      678.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =   0.500000000     m ;  Time  (for ref. rigidity & particle) =   2.072245E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_WFSegment_FF                                                              IPASS= 38


************************************************************************************************************************************

      7   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 38


                         >>>>  End  of  'REBELOTE'  procedure  <<<<

      There  has  been         38  passes  through  the  optical  structure 

                     Total of        114 particles have been launched

************************************************************************************************************************************
      9  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 39

0                                             TRACE DU FAISCEAU
                                           (follows element #      8)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o   1   1.0000     0.000     0.000     0.000     0.000      0.0000  -0.0000    0.000   -0.000   -0.000   -0.000   5.000104E+01     1
               Time of flight (mus) :  2.07380893E-03 mass (MeV/c2) :  0.510999    
o   1   1.0000     0.000     0.000     0.000     0.000      0.0000  -0.0000    0.000   -0.000   -0.000   -0.000   5.000104E+01     2
               Time of flight (mus) :  2.07380893E-03 mass (MeV/c2) :  0.510999    
o   1   1.0000     0.000     0.000     0.000     0.000      0.0000  -0.0000    0.000   -0.000   -0.000   -0.000   5.000104E+01     3
               Time of flight (mus) :  2.07380893E-03 mass (MeV/c2) :  0.510999    

************************************************************************************************************************************
     10  Keyword, label(s) :  SPNPRT      MATRIX                                                                       IPASS= 39

                          -- 1  GROUPS  OF  MOMENTA  FOLLOW   --

                    ----------------------------------------------------------------------------------
                    Momentum  group  #1 (D=   9.999999E-01) ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.455342   0.122008   0.333333   0.577350   0.001954  19.999999  14.142135


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   |Si,Sf|   (Z,Sf_yz) (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_yz : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.866025 -0.500000 -0.000000  1.000000      1.6849   30.000   90.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.500000  0.866025 -0.000000  1.000000      1.6849   30.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000  0.000000  1.000000  1.000000      1.6849    0.000   45.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  8.6603E-01  8.6603E-01 -5.0000E-01 -5.0000E-01 -1.2865E-17 -1.2865E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00     1   1
  5.0000E-01  5.0000E-01  8.6603E-01  8.6603E-01 -1.7389E-17 -1.7389E-17  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00     2   1
  1.9877E-17  1.9877E-17  8.7006E-18  8.7006E-18  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.68493E+00     3   1


                  Spin transfer matrix, momentum group # 1 :

          0.866025        0.500000        1.987705E-17
         -0.500000        0.866025        8.700593E-18
         -1.286506E-17   -1.738863E-17     1.00000    

     Determinant =       1.0000000000
     Trace =       2.7320508463;   spin precession acos((trace-1)/2) =      29.9999977803 deg
     Precession axis :  (-0.0000,  0.0000, -1.0000)  ->  angle to (X,Y) plane, to X axis :   -90.0000,   128.5482 deg
     Spin precession/2pi (or Qs, fractional) :    8.3333E-02

************************************************************************************************************************************
     11  Keyword, label(s) :  SYSTEM                                                                                   IPASS= 39

     Number  of  commands :   2,  as  follows : 

 gnuplot <./gnuplot_Zplt_sYZ.gnu
 gnuplot < ./gnuplot_scanEB_FFratio.gnu

************************************************************************************************************************************
     12  Keyword, label(s) :  END                                                                                      IPASS= 39


************************************************************************************************************************************
 Pgm zgoubi : Execution ended normally, upon keyword END or FIN
   
 File in:   WFSegment.dat
 File out:  zgoubi.res

  Zgoubi, author's Revision: 1590.
  Job  started  on  04-02-2022,  at  10:40:00 
  JOB  ENDED  ON    04-02-2022,  AT  10:40:27 

   CPU time, total :     20.138503000000000     

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
