set title "Plotted from file Zplt "

set key maxcol 1; set key t r 

set xtics mirror; set ytics mirror

set size ratio 1

set xlabel 's  [m]'; set ylabel 'Y  [m]'

cm2m = 0.01; kG2T= 0.1; MeV2eV = 1e6; c = 2.99792458e8

plot \
   'zgoubi.plt' u ($14 *cm2m):($10 *cm2m) w p ps .4 notit ,\
   'zgoubi.plt' u ($14 *cm2m):($10 *cm2m) w p ps 0 

     set terminal postscript eps color  enh  
       set output "gnuplot_Zplt_sY.eps"  
       replot  
       set terminal X11  
       unset output  

pause 2
exit

