
set xlab "Fringe ratio {/Symbol l}_B/{/Symbol l}_E"; set ylab "|{/Symbol d}E/E|,     |{/Symbol d}B/B|"; set y2lab "penalty"
set k c t spacin 1; set xtics mirror; set ytics nomirror; set y2tics; set logscale y2; set format y2 "%.0s*10^{%T}"
# grep status of FIT variables, in zgoubi.res:
system "grep '   1    11  ' zgoubi.res |  cat > grep.res4E.out"
system "grep '   2    12  ' zgoubi.res |  cat > grep.res4B.out"
system "grep 'penalty value ' zgoubi.res |  cat > grep.penalty.out"
# Parameters of the lambda_B/lambda_E scan, to determine the horizontal scale:
NPASS = 37; lmdaBi = 3.; lmdaBf = 7.; lmdaE = 5.; dlmda = (lmdaBf-lmdaBi)/(NPASS-1); Eth= -982939.444; Bth= 4.0737834E-03

plot  [.58:1.42] [:.0042] \
"grep.res4E.out" u ($0>0? (lmdaBi+ dlmda*($0-1))/lmdaE :1/0):(abs(($6-Eth)/Eth)) axes x1y1 w lp pt 4 lc rgb "red" tit "E" ,\
"grep.res4B.out" u ($0>0? (lmdaBi+ dlmda*($0-1))/lmdaE :1/0):(abs(($6-Bth)/Bth)) axes x1y1 w lp pt 6 lc rgb "blue" tit "B" ,\
"grep.penalty.out" u ($0>0? (lmdaBi+dlmda*($0-1))/lmdaE :1/0):($5) axes x1y2 w l dt 2 lc rgb "black" tit "penalty" ; pause 1

 set terminal postscript eps blacktext color enh    # size 8.3cm,4cm "Times-Roman" 12 
 set output "gnuplot_scanEB_FFratio.eps" 
 replot 
 set terminal X11 
 unset output 

pause .2
exit


