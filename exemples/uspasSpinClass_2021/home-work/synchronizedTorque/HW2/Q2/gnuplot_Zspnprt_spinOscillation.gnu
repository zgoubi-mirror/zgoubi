


# set tit "Observation azimuth is at snake. \n If G.{/Symbol g}=half-int, spin precesses in (Y,Z) plane.
set xlabel "turn"; set ylabel "S_X,   S_Y,   S_Z"; set key b l
nbtrj=3 # number of trajectories tracked

do for [it=1:nbtrj] {
 unset label; set label sprintf("particle %3.5g",it) at 10, 0.8
plot [] [-1:1] \
  'zgoubi.SPNPRT.Out' every nbtrj::(it+2) u ($22):($13) w lp lw .3 pt 4 ps .8 lc rgb "red" ,\
  'zgoubi.SPNPRT.Out' every nbtrj::(it+2) u ($22):($14) w lp lw .3 pt 6 ps .8 lc rgb "blue" ,\
  'zgoubi.SPNPRT.Out' every nbtrj::(it+2) u ($22):($15) w lp lw .3 pt 8 ps .8 lc rgb "black"
pause .5
set terminal postscript eps blacktext color  enh  
set output sprintf('gnuplot_Zspnprt_spinOsc_trj%i.eps',it)
replot  
set terminal X11  
unset output
}

exit
