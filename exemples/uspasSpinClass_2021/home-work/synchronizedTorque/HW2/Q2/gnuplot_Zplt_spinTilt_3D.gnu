
set xlabel "X"; set ylabel "Y"; set zlabel "Z"; set xrange [-1:1]; set yrange [-1:1]; set zrange [-1:1]
set xyplane 0; set view equal xyz; set view 49, 339; unset colorbox
set urange [-pi/2:pi/2]; set vrange [0:2*pi]; set parametric; R = 1.   # radius of sphere 
nbtrj=3 # number of trajectories tracked

do for [it=1:nbtrj] {
  unset label; set label sprintf("  particle %i",it) at -1, .9, 1.
  splot  R*cos(u)*cos(v),R*cos(u)*sin(v),R*sin(u) w l lw .2 lc rgb "cyan" notit ,\
  'zgoubi.plt'   u ($19==it? $33 :1/0):($34):($35) w lp lw .2 ps .4 lc palette
  pause .5
  set terminal postscript eps blacktext color  enh  
  set output sprintf('gnuplot_Zplt_S3D_trj%i.eps',it)
  replot; set terminal X11; unset output
}
exit

