


# set tit "s_{/Symbol p} motion"
set xlabel "S_X"; set ylabel "S_Y"; set size ratio -1; set xrange [-1:1]; set yrange [-1:1]; set key t l
nbtrj=3 # number of trajectories tracked

do for [it=1:nbtrj] {
  unset label; set label sprintf("particle %i",it) at -.9, .8
  plot 'zgoubi.plt' u ($19==it? $33 :1/0):($34) w lp lw .3 ps .2 lc rgb "blue"
  pause .5
  set terminal postscript eps blacktext color  enh  
  set output sprintf('gnuplot_Zplt_SX-SY_trj%i.eps',it)
  replot; set terminal X11; unset output
}

exit
