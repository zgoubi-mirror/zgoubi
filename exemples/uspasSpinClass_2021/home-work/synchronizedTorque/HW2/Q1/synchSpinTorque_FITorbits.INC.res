Cyclotron, classical. Synchronized spin kick in a uniform field
'OBJET'                                                                                                      1
64.62444403717985                      ! Reference Brho ("BORO" in the users' guide) -> 200keV proton.
2
3 1
12.924889  0. 0. 0. 0.   1.          'o'                                ! Ggamma=1.793229 -> 0.200MeV;
309.47295  0. 0. 0. 0.  23.943951797 'i'                                  ! Ggamma=2 -> 108.411628MeV;
608.30878  0. 0. 0. 0.  47.064911290 'h'                                ! Ggamma=2.5 -> 370.082556MeV.
1 1 1                          ! For any particle: set to 1 to enable ray-tracing, or to -9 to ignore.
 
'PARTICUL'                                           ! This is required for spin motion to be computed       2
PROTON                                         ! - by default zgoubi otherwise only requires rigidity.
'SPNTRK'                                                                      ! Request spin tracking.       3
4.1                                                      ! All initial spins taken parallel to Z axis.
0. 0. 1.
 
'SPNPRT'  PRINT             ! Log spin coordinates in zgoubi.res; PRINT adds log in zgoubi.SPNPRT.Out.       4
 
! 'INCLUDE'                                                         ! Grabs lines form a file, here, the  
! Include_SegmentStart : 60degSector.inc[#S_60degSectorUnifB,*:#E_60d(n_inc, depth :  1 2)
'MARKER'    #S_60degSectorUnifB                               ! Label should not exceed 20 characters.       5
'DIPOLE'                                                     ! Analytical modeling of a dipole magnet.       6
2                  ! IL=2, only purpose is to logged trajectories in zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30.  5. 0. 0. 0.                 ! Reference azimuthal angle ACN; BM field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6                      ! Entrance face placed at omega+=30 deg from ACN.
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6                        ! Exit face placed at omega-=-30 deg from ACN.
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
2 10    ! '2' is for 2nd degree interpolation. Could also be '25' (5*5 points grid) or 4 (4th degree).
1.                         ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                  ! Magnet positionning RE, TE, RS, TS. Could be instead non-zero, e.g.,
!                                2 RE=50. 0. RS=50. 0., as long as Yo is amended accordingly in OBJET.
! Include_SegmentEnd : 60degSector.inc                               (had n_inc, depth :  1 2)
'MARKER'    #E_60degSectorUnifB                               ! Label should not exceed 20 characters.       7
! 'INCLUDE'                                                         ! Grabs lines form a file, here, the  
! Include_SegmentStart : 60degSector.inc[#S_60degSectorUnifB,*:#E_60d(n_inc, depth :  1 2)
'MARKER'    #S_60degSectorUnifB                               ! Label should not exceed 20 characters.       8
'DIPOLE'                                                     ! Analytical modeling of a dipole magnet.       9
2                  ! IL=2, only purpose is to logged trajectories in zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30.  5. 0. 0. 0.                 ! Reference azimuthal angle ACN; BM field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6                      ! Entrance face placed at omega+=30 deg from ACN.
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6                        ! Exit face placed at omega-=-30 deg from ACN.
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
2 10    ! '2' is for 2nd degree interpolation. Could also be '25' (5*5 points grid) or 4 (4th degree).
1.                         ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                  ! Magnet positionning RE, TE, RS, TS. Could be instead non-zero, e.g.,
!                                2 RE=50. 0. RS=50. 0., as long as Yo is amended accordingly in OBJET.
! Include_SegmentEnd : 60degSector.inc                               (had n_inc, depth :  1 2)
'MARKER'    #E_60degSectorUnifB                               ! Label should not exceed 20 characters.      10
! 'INCLUDE'                                                         ! Grabs lines form a file, here, the  
! Include_SegmentStart : 60degSector.inc[#S_60degSectorUnifB,*:#E_60d(n_inc, depth :  1 2)
'MARKER'    #S_60degSectorUnifB                               ! Label should not exceed 20 characters.      11
'DIPOLE'                                                     ! Analytical modeling of a dipole magnet.      12
2                  ! IL=2, only purpose is to logged trajectories in zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30.  5. 0. 0. 0.                 ! Reference azimuthal angle ACN; BM field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6                      ! Entrance face placed at omega+=30 deg from ACN.
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6                        ! Exit face placed at omega-=-30 deg from ACN.
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
2 10    ! '2' is for 2nd degree interpolation. Could also be '25' (5*5 points grid) or 4 (4th degree).
1.                         ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                  ! Magnet positionning RE, TE, RS, TS. Could be instead non-zero, e.g.,
!                                2 RE=50. 0. RS=50. 0., as long as Yo is amended accordingly in OBJET.
! Include_SegmentEnd : 60degSector.inc                               (had n_inc, depth :  1 2)
'MARKER'    #E_60degSectorUnifB                               ! Label should not exceed 20 characters.      13
! 'INCLUDE'                                                         ! Grabs lines form a file, here, the  
! Include_SegmentStart : 60degSector.inc[#S_60degSectorUnifB,*:#E_60d(n_inc, depth :  1 2)
'MARKER'    #S_60degSectorUnifB                               ! Label should not exceed 20 characters.      14
'DIPOLE'                                                     ! Analytical modeling of a dipole magnet.      15
2                  ! IL=2, only purpose is to logged trajectories in zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30.  5. 0. 0. 0.                 ! Reference azimuthal angle ACN; BM field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6                      ! Entrance face placed at omega+=30 deg from ACN.
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6                        ! Exit face placed at omega-=-30 deg from ACN.
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
2 10    ! '2' is for 2nd degree interpolation. Could also be '25' (5*5 points grid) or 4 (4th degree).
1.                         ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                  ! Magnet positionning RE, TE, RS, TS. Could be instead non-zero, e.g.,
!                                2 RE=50. 0. RS=50. 0., as long as Yo is amended accordingly in OBJET.
! Include_SegmentEnd : 60degSector.inc                               (had n_inc, depth :  1 2)
'MARKER'    #E_60degSectorUnifB                               ! Label should not exceed 20 characters.      16
! 'INCLUDE'                                                         ! Grabs lines form a file, here, the  
! Include_SegmentStart : 60degSector.inc[#S_60degSectorUnifB,*:#E_60d(n_inc, depth :  1 2)
'MARKER'    #S_60degSectorUnifB                               ! Label should not exceed 20 characters.      17
'DIPOLE'                                                     ! Analytical modeling of a dipole magnet.      18
2                  ! IL=2, only purpose is to logged trajectories in zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30.  5. 0. 0. 0.                 ! Reference azimuthal angle ACN; BM field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6                      ! Entrance face placed at omega+=30 deg from ACN.
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6                        ! Exit face placed at omega-=-30 deg from ACN.
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
2 10    ! '2' is for 2nd degree interpolation. Could also be '25' (5*5 points grid) or 4 (4th degree).
1.                         ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                  ! Magnet positionning RE, TE, RS, TS. Could be instead non-zero, e.g.,
!                                2 RE=50. 0. RS=50. 0., as long as Yo is amended accordingly in OBJET.
! Include_SegmentEnd : 60degSector.inc                               (had n_inc, depth :  1 2)
'MARKER'    #E_60degSectorUnifB                               ! Label should not exceed 20 characters.      19
! 'INCLUDE'                                                         ! Grabs lines form a file, here, the  
! Include_SegmentStart : 60degSector.inc[#S_60degSectorUnifB,*:#E_60d(n_inc, depth :  1 2)
'MARKER'    #S_60degSectorUnifB                               ! Label should not exceed 20 characters.      20
'DIPOLE'                                                     ! Analytical modeling of a dipole magnet.      21
2                  ! IL=2, only purpose is to logged trajectories in zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30.  5. 0. 0. 0.                 ! Reference azimuthal angle ACN; BM field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6                      ! Entrance face placed at omega+=30 deg from ACN.
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6                        ! Exit face placed at omega-=-30 deg from ACN.
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
2 10    ! '2' is for 2nd degree interpolation. Could also be '25' (5*5 points grid) or 4 (4th degree).
1.                         ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                  ! Magnet positionning RE, TE, RS, TS. Could be instead non-zero, e.g.,
!                                2 RE=50. 0. RS=50. 0., as long as Yo is amended accordingly in OBJET.
! Include_SegmentEnd : 60degSector.inc                               (had n_inc, depth :  1 2)
'MARKER'    #E_60degSectorUnifB                               ! Label should not exceed 20 characters.      22
'FAISCEAU'                                                                                                  23
'SPINR'                                                                                                     24
1                                                                                     ! Spin rotation,
0. 30.                                                          1 about the X-axis, by 10 degree here.
 
'FIT'                                                                    ! To be uncommented for use.       25
3                                                                                      ! 3 variables.
1 30 0 .4                                                                         ! Y0 of particle 1,
1 40 0 .4                                                                         ! Y0 of particle 2,
1 50 0 .4                                                                         ! Y0 of particle 3.
6  1e-15                       ! 6 constraints; penalty, all apply at end of sequence, which is here.
3.1 1 2 #End 0. 1. 0                            ! Partilce 1 Y0 equal to current Y, right before FIT,
3.1 1 3 #End 0. 1. 0                            ! partilce 1 T0 equal to current Y, right before FIT,
3.1 2 2 #End 0. 1. 0                            ! Partilce 2 Y0 equal to current Y, right before FIT,
3.1 2 3 #End 0. 1. 0                            ! partilce 2 T0 equal to current Y, right before FIT,
3.1 3 2 #End 0. 1. 0                            ! Partilce 3 Y0 equal to current Y, right before FIT,
3.1 3 3 #End 0. 1. 0                            ! partilce 3 T0 equal to current Y, right before FIT.
 
! 'REBELOTE'                                               ! Multiturn tracking. Uncomment to operate.
! 39 0.2  99                                                                   ! 39 additional passes.
 
! 'SYSTEM'
! 3                                                                            ! Produce graphs, from
! gnuplot <./gnuplot_Zspnprt_spinOscillation.gnu                              ! zgoubi.SPNPRT.out, or
! gnuplot < ./gnuplot_Zplt_spinTilt.gnu                                            ! from zgoubi.plt.
! gnuplot <./gnuplot_Zplt_spinTilt_3D.gnu
 
'FAISCEAU'                                 ! Log current coordinates in zgoubi.res execution listing.       26
'FAISTORE'                       ! Create zgoubi.fai and log current coordinates therein (usually for       27
zgoubi.fai                                                          ! graphic or data post-treatmet).
1
'END'

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 1

     Particle  properties :
     PROTON
                     Mass          =    938.272        MeV/c2
                     Charge        =   1.602176E-19    C     
                     G  factor     =    1.79285              
                     COM life-time =   1.000000E+99    s     

              Reference  data :
                    mag. rigidity (kG.cm)   :   64.624444      =p/q, such that dev.=B*L/rigidity
                    mass (MeV/c2)           :   938.27208    
                    momentum (MeV/c)        :   19.373921    
                    energy, total (MeV)     :   938.47208    
                    energy, kinetic (MeV)   :  0.19999999    
                    beta = v/c              :  2.0644110049E-02
                    gamma                   :   1.000213158    
                    beta*gamma              :  2.0648510502E-02
                    G*gamma                 :   1.793229509    
                    m / G                   :   523.3418681    
                    electric rigidity (MeV) :  0.3999573557    =T[eV]*(gamma+1)/gamma, such that dev.=E*L/rigidity
  
 I, AMQ(1,I), AMQ(2,I)/QE, P/Pref, v/c, time, s :
  
     1   9.38272081E+02  1.00000000E+00  1.00000000E+00  2.06441100E-02  0.00000000E+00  0.00000000E+00
     2   9.38272081E+02  1.00000000E+00  2.39439518E+01  4.43198098E-01  0.00000000E+00  0.00000000E+00
     3   9.38272081E+02  1.00000000E+00  4.70649113E+01  6.96930208E-01  0.00000000E+00  0.00000000E+00

************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 1


                Spin  tracking  requested.


                          Particle  mass          =    938.2721     MeV/c2
                          Gyromagnetic  factor  G =    1.792847    

                          Initial spin conditions type  4 :
                              KSO2=1. Same spin for all particles
                              Particles # 1 to 3 may be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     64.624444     kG*cm
                               beta      =    0.20644110E-01
                               gamma     =     1.0002132    
                               G*gamma   =     1.7932295    
                               M / G     =     523.3418681    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.000000
                               <SY> =     0.000000
                               <SZ> =     1.000000
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  SPNPRT      PRINT                                                                        IPASS= 1

                          -- 1  GROUPS  OF  MOMENTA  FOLLOW   --

                    ----------------------------------------------------------------------------------
                    Momentum  group  #1 (D=   1.000000E+00) ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.000000   0.000000   1.000000   1.000000         0.000000   0.000000   1.000000   1.000000   2.097743   0.000000   0.000000


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   |Si,Sf|   (Z,Sf_yz) (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_yz : projection of Sf on YZ plane)

 o  1  0.000000  0.000000  1.000000  1.000000     0.000000  0.000000  1.000000  1.000000      1.0002    0.000   45.000    0.000    1
 i  1  0.000000  0.000000  1.000000  1.000000     0.000000  0.000000  1.000000  1.000000      1.1155    0.000   45.000    0.000    2
 h  1  0.000000  0.000000  1.000000  1.000000     0.000000  0.000000  1.000000  1.000000      1.3944    0.000   45.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.00021E+00     1   1
  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  2.39440E+01  1.11554E+00     2   1
  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  4.70649E+01  1.39443E+00     3   1

************************************************************************************************************************************
      5  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
      6  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


                OPEN FILE zgoubi.plt                                                                      
                FOR PRINTING TRAJECTORIES

                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 2
                     9-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :   1.000     cm   (i.e.,   2.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925    -0.000     0.000     0.000            1
  A    1 23.9440   309.473     0.000     0.000     0.000            1.047   309.473    -0.000     0.000     0.000            2
  A    1 47.0649   608.309     0.000     0.000     0.000            1.047   608.309    -0.000     0.000     0.000            3


                CONDITIONS  DE  MAXWELL  (      960.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.523598776     m ;  Time  (for ref. rigidity & particle) =   8.460222E-08 s 

************************************************************************************************************************************
      7  Keyword, label(s) :  MARKER      #E_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
      8  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
      9  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 2
                     9-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :   1.000     cm   (i.e.,   2.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925    -0.000     0.000     0.000            1
  A    1 23.9440   309.473     0.000     0.000     0.000            1.047   309.473    -0.000     0.000     0.000            2
  A    1 47.0649   608.309     0.000     0.000     0.000            1.047   608.309    -0.000     0.000     0.000            3


                CONDITIONS  DE  MAXWELL  (      960.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    1.04719755     m ;  Time  (for ref. rigidity & particle) =   1.692044E-07 s 

************************************************************************************************************************************
     10  Keyword, label(s) :  MARKER      #E_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     11  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     12  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 2
                     9-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :   1.000     cm   (i.e.,   2.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925    -0.000     0.000     0.000            1
  A    1 23.9440   309.473     0.000     0.000     0.000            1.047   309.473    -0.000     0.000     0.000            2
  A    1 47.0649   608.309     0.000     0.000     0.000            1.047   608.309    -0.000     0.000     0.000            3


                CONDITIONS  DE  MAXWELL  (      960.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    1.57079633     m ;  Time  (for ref. rigidity & particle) =   2.538067E-07 s 

************************************************************************************************************************************
     13  Keyword, label(s) :  MARKER      #E_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     14  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     15  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 2
                     9-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :   1.000     cm   (i.e.,   2.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925     0.000     0.000     0.000            1
  A    1 23.9440   309.473     0.000     0.000     0.000            1.047   309.473     0.000     0.000     0.000            2
  A    1 47.0649   608.309     0.000     0.000     0.000            1.047   608.309     0.000     0.000     0.000            3


                CONDITIONS  DE  MAXWELL  (      960.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    2.09439510     m ;  Time  (for ref. rigidity & particle) =   3.384089E-07 s 

************************************************************************************************************************************
     16  Keyword, label(s) :  MARKER      #E_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     17  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     18  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 2
                     9-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :   1.000     cm   (i.e.,   2.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925     0.000     0.000     0.000            1
  A    1 23.9440   309.473     0.000     0.000     0.000            1.047   309.473     0.000     0.000     0.000            2
  A    1 47.0649   608.309     0.000     0.000     0.000            1.047   608.309     0.000     0.000     0.000            3


                CONDITIONS  DE  MAXWELL  (      960.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    2.61799388     m ;  Time  (for ref. rigidity & particle) =   4.230111E-07 s 

************************************************************************************************************************************
     19  Keyword, label(s) :  MARKER      #E_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     20  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     21  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 2
                     9-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :   1.000     cm   (i.e.,   2.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925     0.000     0.000     0.000            1
  A    1 23.9440   309.473     0.000     0.000     0.000            1.047   309.473    -0.000     0.000     0.000            2
  A    1 47.0649   608.309     0.000     0.000     0.000            1.047   608.309    -0.000     0.000     0.000            3


                CONDITIONS  DE  MAXWELL  (      960.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    3.14159265     m ;  Time  (for ref. rigidity & particle) =   5.076133E-07 s 

************************************************************************************************************************************
     22  Keyword, label(s) :  MARKER      #E_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     23  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #     22)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o   1   1.0000    12.925     0.000     0.000     0.000      0.0000   0.0000   12.925    0.000    0.000    0.000   8.120937E+01     1
               Time of flight (mus) :  0.13121675     mass (MeV/c2) :   938.272    
i   1  23.9440   309.473     0.000     0.000     0.000      0.0000  22.9440  309.473   -0.000    0.000    0.000   1.944476E+03     2
               Time of flight (mus) :  0.14634703     mass (MeV/c2) :   938.272    
h   1  47.0649   608.309     0.000     0.000     0.000      0.0000  46.0649  608.309   -0.000    0.000    0.000   3.822117E+03     3
               Time of flight (mus) :  0.18293379     mass (MeV/c2) :   938.272    

************************************************************************************************************************************
     24  Keyword, label(s) :  SPINR                                                                                    IPASS= 1


                              Spin rotator. Axis at   0.000000E+00 deg.

                                     Angle  =   3.000000E+01 deg. 

************************************************************************************************************************************
     25  Keyword, label(s) :  FIT                                                                                      IPASS= 1

     Pgm main. FITING=.T., FIT procedure launched.

           variable #            1       IR =            1 ,   ok.
           variable #            1       IP =           30 ,   ok.
           variable #            2       IR =            1 ,   ok.
           variable #            2       IP =           40 ,   ok.
           variable #            3       IR =            1 ,   ok.
           variable #            3       IP =           50 ,   ok.
           constraint #            1       IR =           24 ,   ok.
           constraint #            1       I  =            1 ,   ok.
           constraint #            2       IR =           24 ,   ok.
           constraint #            2       I  =            1 ,   ok.
           constraint #            3       IR =           24 ,   ok.
           constraint #            3       I  =            2 ,   ok.
           constraint #            4       IR =           24 ,   ok.
           constraint #            4       I  =            2 ,   ok.
           constraint #            5       IR =           24 ,   ok.
           constraint #            5       I  =            3 ,   ok.
           constraint #            6       IR =           24 ,   ok.
           constraint #            6       I  =            3 ,   ok.

                    FIT  variables  and  constraints  in  good  order,  FIT  will proceed. 
 STATUS OF VARIABLES  (Iteration #     1 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   1   1    30    7.75        12.9       12.924889       18.1      3.447E-02  OBJET      -                    -                   
   1   2    40    186.        309.       309.47295       433.      0.825      OBJET      -                    -                   
   1   3    50    365.        608.       608.30878       852.       1.62      OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2    24    0.000000E+00    1.000E+00    4.160228E-11    8.59E-05 SPINR      -                    -                    0
  3   1   3    24    0.000000E+00    1.000E+00    4.489777E-09    1.00E+00 SPINR      -                    -                    0
  3   2   2    24    0.000000E+00    1.000E+00    1.250555E-12    7.76E-08 SPINR      -                    -                    0
  3   2   3    24    0.000000E+00    1.000E+00    1.015507E-11    5.12E-06 SPINR      -                    -                    0
  3   3   2    24    0.000000E+00    1.000E+00    3.410605E-13    5.77E-09 SPINR      -                    -                    0
  3   3   3    24    0.000000E+00    1.000E+00    2.747802E-12    3.75E-07 SPINR      -                    -                    0
 Fit reached penalty value   2.0160E-17



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 1


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 1


                Spin  tracking  requested.


                          Particle  mass          =    938.2721     MeV/c2
                          Gyromagnetic  factor  G =    1.792847    

                          Initial spin conditions type  4 :
                              KSO2=1. Same spin for all particles
                              Particles # 1 to 3 may be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     64.624444     kG*cm
                               beta      =    0.20644110E-01
                               gamma     =     1.0002132    
                               G*gamma   =     1.7932295    
                               M / G     =     523.3418681    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.000000
                               <SY> =     0.000000
                               <SZ> =     1.000000
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  SPNPRT      PRINT                                                                        IPASS= 1

                          -- 1  GROUPS  OF  MOMENTA  FOLLOW   --

                    ----------------------------------------------------------------------------------
                    Momentum  group  #1 (D=   1.000000E+00) ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.000000   0.000000   1.000000   1.000000         0.000000   0.000000   1.000000   1.000000   2.097743   0.000000   0.000000


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   |Si,Sf|   (Z,Sf_yz) (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_yz : projection of Sf on YZ plane)

 o  1  0.000000  0.000000  1.000000  1.000000     0.000000  0.000000  1.000000  1.000000      1.0002    0.000   45.000    0.000    1
 i  1  0.000000  0.000000  1.000000  1.000000     0.000000  0.000000  1.000000  1.000000      1.1155    0.000   45.000    0.000    2
 h  1  0.000000  0.000000  1.000000  1.000000     0.000000  0.000000  1.000000  1.000000      1.3944    0.000   45.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.00021E+00     1   1
  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  2.39440E+01  1.11554E+00     2   1
  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  4.70649E+01  1.39443E+00     3   1

************************************************************************************************************************************
      5  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
      6  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 2
                     9-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :   1.000     cm   (i.e.,   2.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925    -0.000     0.000     0.000            1
  A    1 23.9440   309.473     0.000     0.000     0.000            1.047   309.473    -0.000     0.000     0.000            2
  A    1 47.0649   608.309     0.000     0.000     0.000            1.047   608.309    -0.000     0.000     0.000            3


                CONDITIONS  DE  MAXWELL  (      960.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.523598776     m ;  Time  (for ref. rigidity & particle) =   8.460222E-08 s 

************************************************************************************************************************************
      7  Keyword, label(s) :  MARKER      #E_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
      8  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
      9  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 2
                     9-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :   1.000     cm   (i.e.,   2.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925    -0.000     0.000     0.000            1
  A    1 23.9440   309.473     0.000     0.000     0.000            1.047   309.473    -0.000     0.000     0.000            2
  A    1 47.0649   608.309     0.000     0.000     0.000            1.047   608.309    -0.000     0.000     0.000            3


                CONDITIONS  DE  MAXWELL  (      960.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    1.04719755     m ;  Time  (for ref. rigidity & particle) =   1.692044E-07 s 

************************************************************************************************************************************
     10  Keyword, label(s) :  MARKER      #E_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     11  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     12  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 2
                     9-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :   1.000     cm   (i.e.,   2.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925    -0.000     0.000     0.000            1
  A    1 23.9440   309.473     0.000     0.000     0.000            1.047   309.473    -0.000     0.000     0.000            2
  A    1 47.0649   608.309     0.000     0.000     0.000            1.047   608.309    -0.000     0.000     0.000            3


                CONDITIONS  DE  MAXWELL  (      960.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    1.57079633     m ;  Time  (for ref. rigidity & particle) =   2.538067E-07 s 

************************************************************************************************************************************
     13  Keyword, label(s) :  MARKER      #E_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     14  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     15  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 2
                     9-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :   1.000     cm   (i.e.,   2.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925     0.000     0.000     0.000            1
  A    1 23.9440   309.473     0.000     0.000     0.000            1.047   309.473     0.000     0.000     0.000            2
  A    1 47.0649   608.309     0.000     0.000     0.000            1.047   608.309     0.000     0.000     0.000            3


                CONDITIONS  DE  MAXWELL  (      960.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    2.09439510     m ;  Time  (for ref. rigidity & particle) =   3.384089E-07 s 

************************************************************************************************************************************
     16  Keyword, label(s) :  MARKER      #E_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     17  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     18  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 2
                     9-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :   1.000     cm   (i.e.,   2.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925     0.000     0.000     0.000            1
  A    1 23.9440   309.473     0.000     0.000     0.000            1.047   309.473     0.000     0.000     0.000            2
  A    1 47.0649   608.309     0.000     0.000     0.000            1.047   608.309     0.000     0.000     0.000            3


                CONDITIONS  DE  MAXWELL  (      960.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    2.61799388     m ;  Time  (for ref. rigidity & particle) =   4.230111E-07 s 

************************************************************************************************************************************
     19  Keyword, label(s) :  MARKER      #E_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     20  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     21  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 2
                     9-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :   1.000     cm   (i.e.,   2.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925     0.000     0.000     0.000            1
  A    1 23.9440   309.473     0.000     0.000     0.000            1.047   309.473    -0.000     0.000     0.000            2
  A    1 47.0649   608.309     0.000     0.000     0.000            1.047   608.309    -0.000     0.000     0.000            3


                CONDITIONS  DE  MAXWELL  (      960.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    3.14159265     m ;  Time  (for ref. rigidity & particle) =   5.076133E-07 s 

************************************************************************************************************************************
     22  Keyword, label(s) :  MARKER      #E_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     23  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #     22)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o   1   1.0000    12.925     0.000     0.000     0.000      0.0000   0.0000   12.925    0.000    0.000    0.000   8.120937E+01     1
               Time of flight (mus) :  0.13121675     mass (MeV/c2) :   938.272    
i   1  23.9440   309.473     0.000     0.000     0.000      0.0000  22.9440  309.473   -0.000    0.000    0.000   1.944476E+03     2
               Time of flight (mus) :  0.14634703     mass (MeV/c2) :   938.272    
h   1  47.0649   608.309     0.000     0.000     0.000      0.0000  46.0649  608.309   -0.000    0.000    0.000   3.822117E+03     3
               Time of flight (mus) :  0.18293379     mass (MeV/c2) :   938.272    

************************************************************************************************************************************
     24  Keyword, label(s) :  SPINR                                                                                    IPASS= 1


                              Spin rotator. Axis at   0.000000E+00 deg.

                                     Angle  =   3.000000E+01 deg. 

************************************************************************************************************************************

     25   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
     26  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #     25)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o   1   1.0000    12.925     0.000     0.000     0.000      0.0000   0.0000   12.925    0.000    0.000    0.000   8.120937E+01     1
               Time of flight (mus) :  0.13121675     mass (MeV/c2) :   938.272    
i   1  23.9440   309.473     0.000     0.000     0.000      0.0000  22.9440  309.473   -0.000    0.000    0.000   1.944476E+03     2
               Time of flight (mus) :  0.14634703     mass (MeV/c2) :   938.272    
h   1  47.0649   608.309     0.000     0.000     0.000      0.0000  46.0649  608.309   -0.000    0.000    0.000   3.822117E+03     3
               Time of flight (mus) :  0.18293379     mass (MeV/c2) :   938.272    

************************************************************************************************************************************
     27  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 1


                OPEN FILE zgoubi.fai                                                                      
                FOR PRINTING COORDINATES 

               Print will occur at element[s] labeled : 


************************************************************************************************************************************
     28  Keyword, label(s) :  END                                                                                      IPASS= 1

                             3 particles have been launched
                     Made  it  to  the  end :      3

************************************************************************************************************************************
 Pgm zgoubi : Execution ended normally, upon keyword END or FIN
   
 File in:   synchSpinTorque_FITorbits.INC.dat
 File out:  zgoubi.res

  Zgoubi, author's Revision: 1590.
  Job  started  on  04-02-2022,  at  15:38:14 
  JOB  ENDED  ON    04-02-2022,  AT  15:38:14 

   CPU time, total :    0.29619099999999998     

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
