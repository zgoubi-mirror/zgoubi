Cyclotron, classical. Synchronized spin kick in a uniform field
'OBJET'                                                                                                      1
64.62444403717985                      ! Reference Brho ("BORO" in the users' guide) -> 200keV proton.
2
9 3
12.924889  0. 0. 0. 0.   1.          'o'                                ! Ggamma=1.793229 -> 0.200MeV;
12.924889  0. 0. 0. 0.   1.          'o'                                ! Ggamma=1.793229 -> 0.200MeV;
12.924889  0. 0. 0. 0.   1.          'o'                                ! Ggamma=1.793229 -> 0.200MeV;
309.47295  0. 0. 0. 0.  23.943951797 'i'                               ! Ggamma=2 ->    108.411628MeV;
309.47295  0. 0. 0. 0.  23.943951797 'i'                               ! Ggamma=2 ->    108.411628MeV;
309.47295  0. 0. 0. 0.  23.943951797 'i'                               ! Ggamma=2 ->    108.411628MeV;
608.30878  0. 0. 0. 0.  47.064911290 'h'                               ! Ggamma=2.5 ->  370.082556MeV.
608.30878  0. 0. 0. 0.  47.064911290 'h'                               ! Ggamma=2.5 ->  370.082556MeV.
608.30878  0. 0. 0. 0.  47.064911290 'h'                               ! Ggamma=2.5 ->  370.082556MeV.
1 1 1 1 1 1 1 1 1              ! For any particle: set to 1 to enable ray-tracing, or to -9 to ignore.
! 309.47295  0. 0. 0. 0.  ***        'i'                                  ! Ggamma=2 -> 108.411628MeV;
! 608.30878  0. 0. 0. 0.  ***        'h'                                ! Ggamma=2.5 -> 370.082556MeV.
 
'PARTICUL'                                           ! This is required for spin motion to be computed       2
PROTON                                         ! - by default zgoubi otherwise only requires rigidity.
'SPNTRK'                                                                      ! Request spin tracking.       3
4                                                      ! All initial spins taken parallel to Z axis.
1. 0. 0.
0. 1. 0.
0. 0. 1.
1. 0. 0.
0. 1. 0.
0. 0. 1.
1. 0. 0.
0. 1. 0.
0. 0. 1.
 
'SPNPRT'  PRINT             ! Log spin coordinates in zgoubi.res; PRINT adds log in zgoubi.SPNPRT.Out.       4
 
! 'INCLUDE'                                                         ! Grabs lines form a file, here, the  
! Include_SegmentStart : 60degSector.inc[#S_60degSectorUnifB,*:#E_60d(n_inc, depth :  1 2)
'MARKER'    #S_60degSectorUnifB                               ! Label should not exceed 20 characters.       5
'DIPOLE'                                                     ! Analytical modeling of a dipole magnet.       6
2                  ! IL=2, only purpose is to logged trajectories in zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30.  5. 0. 0. 0.                 ! Reference azimuthal angle ACN; BM field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6                      ! Entrance face placed at omega+=30 deg from ACN.
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6                        ! Exit face placed at omega-=-30 deg from ACN.
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
2 10    ! '2' is for 2nd degree interpolation. Could also be '25' (5*5 points grid) or 4 (4th degree).
1.                         ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                  ! Magnet positionning RE, TE, RS, TS. Could be instead non-zero, e.g.,
!                                2 RE=50. 0. RS=50. 0., as long as Yo is amended accordingly in OBJET.
! Include_SegmentEnd : 60degSector.inc                               (had n_inc, depth :  1 2)
'MARKER'    #E_60degSectorUnifB                               ! Label should not exceed 20 characters.       7
! 'INCLUDE'                                                         ! Grabs lines form a file, here, the  
! Include_SegmentStart : 60degSector.inc[#S_60degSectorUnifB,*:#E_60d(n_inc, depth :  1 2)
'MARKER'    #S_60degSectorUnifB                               ! Label should not exceed 20 characters.       8
'DIPOLE'                                                     ! Analytical modeling of a dipole magnet.       9
2                  ! IL=2, only purpose is to logged trajectories in zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30.  5. 0. 0. 0.                 ! Reference azimuthal angle ACN; BM field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6                      ! Entrance face placed at omega+=30 deg from ACN.
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6                        ! Exit face placed at omega-=-30 deg from ACN.
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
2 10    ! '2' is for 2nd degree interpolation. Could also be '25' (5*5 points grid) or 4 (4th degree).
1.                         ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                  ! Magnet positionning RE, TE, RS, TS. Could be instead non-zero, e.g.,
!                                2 RE=50. 0. RS=50. 0., as long as Yo is amended accordingly in OBJET.
! Include_SegmentEnd : 60degSector.inc                               (had n_inc, depth :  1 2)
'MARKER'    #E_60degSectorUnifB                               ! Label should not exceed 20 characters.      10
! 'INCLUDE'                                                         ! Grabs lines form a file, here, the  
! Include_SegmentStart : 60degSector.inc[#S_60degSectorUnifB,*:#E_60d(n_inc, depth :  1 2)
'MARKER'    #S_60degSectorUnifB                               ! Label should not exceed 20 characters.      11
'DIPOLE'                                                     ! Analytical modeling of a dipole magnet.      12
2                  ! IL=2, only purpose is to logged trajectories in zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30.  5. 0. 0. 0.                 ! Reference azimuthal angle ACN; BM field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6                      ! Entrance face placed at omega+=30 deg from ACN.
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6                        ! Exit face placed at omega-=-30 deg from ACN.
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
2 10    ! '2' is for 2nd degree interpolation. Could also be '25' (5*5 points grid) or 4 (4th degree).
1.                         ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                  ! Magnet positionning RE, TE, RS, TS. Could be instead non-zero, e.g.,
!                                2 RE=50. 0. RS=50. 0., as long as Yo is amended accordingly in OBJET.
! Include_SegmentEnd : 60degSector.inc                               (had n_inc, depth :  1 2)
'MARKER'    #E_60degSectorUnifB                               ! Label should not exceed 20 characters.      13
! 'INCLUDE'                                                         ! Grabs lines form a file, here, the  
! Include_SegmentStart : 60degSector.inc[#S_60degSectorUnifB,*:#E_60d(n_inc, depth :  1 2)
'MARKER'    #S_60degSectorUnifB                               ! Label should not exceed 20 characters.      14
'DIPOLE'                                                     ! Analytical modeling of a dipole magnet.      15
2                  ! IL=2, only purpose is to logged trajectories in zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30.  5. 0. 0. 0.                 ! Reference azimuthal angle ACN; BM field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6                      ! Entrance face placed at omega+=30 deg from ACN.
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6                        ! Exit face placed at omega-=-30 deg from ACN.
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
2 10    ! '2' is for 2nd degree interpolation. Could also be '25' (5*5 points grid) or 4 (4th degree).
1.                         ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                  ! Magnet positionning RE, TE, RS, TS. Could be instead non-zero, e.g.,
!                                2 RE=50. 0. RS=50. 0., as long as Yo is amended accordingly in OBJET.
! Include_SegmentEnd : 60degSector.inc                               (had n_inc, depth :  1 2)
'MARKER'    #E_60degSectorUnifB                               ! Label should not exceed 20 characters.      16
! 'INCLUDE'                                                         ! Grabs lines form a file, here, the  
! Include_SegmentStart : 60degSector.inc[#S_60degSectorUnifB,*:#E_60d(n_inc, depth :  1 2)
'MARKER'    #S_60degSectorUnifB                               ! Label should not exceed 20 characters.      17
'DIPOLE'                                                     ! Analytical modeling of a dipole magnet.      18
2                  ! IL=2, only purpose is to logged trajectories in zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30.  5. 0. 0. 0.                 ! Reference azimuthal angle ACN; BM field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6                      ! Entrance face placed at omega+=30 deg from ACN.
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6                        ! Exit face placed at omega-=-30 deg from ACN.
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
2 10    ! '2' is for 2nd degree interpolation. Could also be '25' (5*5 points grid) or 4 (4th degree).
1.                         ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                  ! Magnet positionning RE, TE, RS, TS. Could be instead non-zero, e.g.,
!                                2 RE=50. 0. RS=50. 0., as long as Yo is amended accordingly in OBJET.
! Include_SegmentEnd : 60degSector.inc                               (had n_inc, depth :  1 2)
'MARKER'    #E_60degSectorUnifB                               ! Label should not exceed 20 characters.      19
! 'INCLUDE'                                                         ! Grabs lines form a file, here, the  
! Include_SegmentStart : 60degSector.inc[#S_60degSectorUnifB,*:#E_60d(n_inc, depth :  1 2)
'MARKER'    #S_60degSectorUnifB                               ! Label should not exceed 20 characters.      20
'DIPOLE'                                                     ! Analytical modeling of a dipole magnet.      21
2                  ! IL=2, only purpose is to logged trajectories in zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30.  5. 0. 0. 0.                 ! Reference azimuthal angle ACN; BM field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6                      ! Entrance face placed at omega+=30 deg from ACN.
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6                        ! Exit face placed at omega-=-30 deg from ACN.
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
2 10    ! '2' is for 2nd degree interpolation. Could also be '25' (5*5 points grid) or 4 (4th degree).
1.                         ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                  ! Magnet positionning RE, TE, RS, TS. Could be instead non-zero, e.g.,
!                                2 RE=50. 0. RS=50. 0., as long as Yo is amended accordingly in OBJET.
! Include_SegmentEnd : 60degSector.inc                               (had n_inc, depth :  1 2)
'MARKER'    #E_60degSectorUnifB                               ! Label should not exceed 20 characters.      22
'FAISCEAU'                                                                                                  23
'SPINR'                                                                                                     24
1                                                                                     ! Spin rotation,
0. 30.                                                          1 about the X-axis, by 10 degree here.
 
! 'FIT'                                                                   ! To be uncommented for use.
! 3                                                                                     ! 3 variables.
! 1 30 0 .4
! 1 40 0 .4
! 1 50 0 .4
! 6  1e-15                      ! 6 constraints; penalty, all apply at end of sequence, which is here.
! 3.1 1 2 #End 0. 1. 0
! 3.1 1 3 #End 0. 1. 0
! 3.1 2 2 #End 0. 1. 0
! 3.1 2 3 #End 0. 1. 0
! 3.1 3 2 #End 0. 1. 0
! 3.1 3 3 #End 0. 1. 0
! 'END'
 
'FAISCEAU'                                 ! Log current coordinates in zgoubi.res execution listing.       25
'SPNPRT' MATRIX                                                                                             26
 
'END'

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       9 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 1

     Particle  properties :
     PROTON
                     Mass          =    938.272        MeV/c2
                     Charge        =   1.602176E-19    C     
                     G  factor     =    1.79285              
                     COM life-time =   1.000000E+99    s     

              Reference  data :
                    mag. rigidity (kG.cm)   :   64.624444      =p/q, such that dev.=B*L/rigidity
                    mass (MeV/c2)           :   938.27208    
                    momentum (MeV/c)        :   19.373921    
                    energy, total (MeV)     :   938.47208    
                    energy, kinetic (MeV)   :  0.19999999    
                    beta = v/c              :  2.0644110049E-02
                    gamma                   :   1.000213158    
                    beta*gamma              :  2.0648510502E-02
                    G*gamma                 :   1.793229509    
                    m / G                   :   523.3418681    
                    electric rigidity (MeV) :  0.3999573557    =T[eV]*(gamma+1)/gamma, such that dev.=E*L/rigidity
  
 I, AMQ(1,I), AMQ(2,I)/QE, P/Pref, v/c, time, s :
  
     1   9.38272081E+02  1.00000000E+00  1.00000000E+00  2.06441100E-02  0.00000000E+00  0.00000000E+00
     2   9.38272081E+02  1.00000000E+00  1.00000000E+00  2.06441100E-02  0.00000000E+00  0.00000000E+00
     3   9.38272081E+02  1.00000000E+00  1.00000000E+00  2.06441100E-02  0.00000000E+00  0.00000000E+00
     4   9.38272081E+02  1.00000000E+00  2.39439518E+01  4.43198098E-01  0.00000000E+00  0.00000000E+00
     5   9.38272081E+02  1.00000000E+00  2.39439518E+01  4.43198098E-01  0.00000000E+00  0.00000000E+00
     6   9.38272081E+02  1.00000000E+00  2.39439518E+01  4.43198098E-01  0.00000000E+00  0.00000000E+00
     7   9.38272081E+02  1.00000000E+00  4.70649113E+01  6.96930208E-01  0.00000000E+00  0.00000000E+00
     8   9.38272081E+02  1.00000000E+00  4.70649113E+01  6.96930208E-01  0.00000000E+00  0.00000000E+00
     9   9.38272081E+02  1.00000000E+00  4.70649113E+01  6.96930208E-01  0.00000000E+00  0.00000000E+00

************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 1


                Spin  tracking  requested.


                          Particle  mass          =    938.2721     MeV/c2
                          Gyromagnetic  factor  G =    1.792847    

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 9 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     64.624444     kG*cm
                               beta      =    0.20644110E-01
                               gamma     =     1.0002132    
                               G*gamma   =     1.7932295    
                               M / G     =     523.3418681    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        9  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  SPNPRT      PRINT                                                                        IPASS= 1

                          -- 3  GROUPS  OF  MOMENTA  FOLLOW   --

                    ----------------------------------------------------------------------------------
                    Momentum  group  #1 (D=   1.000000E+00) ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.333333   0.333333   0.333333   0.577350   1.793230   0.000000   0.000000


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   |Si,Sf|   (Z,Sf_yz) (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_yz : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     1.000000  0.000000  0.000000  1.000000      1.0002    0.000    0.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.000000  1.000000  0.000000  1.000000      1.0002    0.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000  0.000000  1.000000  1.000000      1.0002    0.000   45.000    0.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  1.0000E+00  1.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.00021E+00     1   1
  0.0000E+00  0.0000E+00  1.0000E+00  1.0000E+00  0.0000E+00  0.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.00021E+00     2   1
  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.00021E+00     3   1

                    ----------------------------------------------------------------------------------
                    Momentum  group  #2 (D=   2.394395E+01) ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.333333   0.333333   0.333333   0.577350   2.000000   0.000000   0.000000


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   |Si,Sf|   (Z,Sf_yz) (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_yz : projection of Sf on YZ plane)

 i  1  1.000000  0.000000  0.000000  1.000000     1.000000  0.000000  0.000000  1.000000      1.1155    0.000    0.000   90.000    4
 i  1  0.000000  1.000000  0.000000  1.000000     0.000000  1.000000  0.000000  1.000000      1.1155    0.000   90.000   90.000    5
 i  1  0.000000  0.000000  1.000000  1.000000     0.000000  0.000000  1.000000  1.000000      1.1155    0.000   45.000    0.000    6



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  1.0000E+00  1.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  1.0000E+00  1.0000E+00  2.39440E+01  1.11554E+00     4   1
  0.0000E+00  0.0000E+00  1.0000E+00  1.0000E+00  0.0000E+00  0.0000E+00  1.0000E+00  1.0000E+00  2.39440E+01  1.11554E+00     5   1
  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  2.39440E+01  1.11554E+00     6   1

                    ----------------------------------------------------------------------------------
                    Momentum  group  #3 (D=   4.706491E+01) ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.333333   0.333333   0.333333   0.577350   2.500000   0.000000   0.000000


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   |Si,Sf|   (Z,Sf_yz) (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_yz : projection of Sf on YZ plane)

 h  1  1.000000  0.000000  0.000000  1.000000     1.000000  0.000000  0.000000  1.000000      1.3944    0.000    0.000   90.000    7
 h  1  0.000000  1.000000  0.000000  1.000000     0.000000  1.000000  0.000000  1.000000      1.3944    0.000   90.000   90.000    8
 h  1  0.000000  0.000000  1.000000  1.000000     0.000000  0.000000  1.000000  1.000000      1.3944    0.000   45.000    0.000    9



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  1.0000E+00  1.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  1.0000E+00  1.0000E+00  4.70649E+01  1.39443E+00     7   1
  0.0000E+00  0.0000E+00  1.0000E+00  1.0000E+00  0.0000E+00  0.0000E+00  1.0000E+00  1.0000E+00  4.70649E+01  1.39443E+00     8   1
  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  4.70649E+01  1.39443E+00     9   1

************************************************************************************************************************************
      5  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
      6  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


                OPEN FILE zgoubi.plt                                                                      
                FOR PRINTING TRAJECTORIES

                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 2
                     9-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :   1.000     cm   (i.e.,   2.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925    -0.000     0.000     0.000            1
  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925    -0.000     0.000     0.000            2
  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925    -0.000     0.000     0.000            3
  A    1 23.9440   309.473     0.000     0.000     0.000            1.047   309.473    -0.000     0.000     0.000            4
  A    1 23.9440   309.473     0.000     0.000     0.000            1.047   309.473    -0.000     0.000     0.000            5
  A    1 23.9440   309.473     0.000     0.000     0.000            1.047   309.473    -0.000     0.000     0.000            6
  A    1 47.0649   608.309     0.000     0.000     0.000            1.047   608.309    -0.000     0.000     0.000            7
  A    1 47.0649   608.309     0.000     0.000     0.000            1.047   608.309    -0.000     0.000     0.000            8
  A    1 47.0649   608.309     0.000     0.000     0.000            1.047   608.309    -0.000     0.000     0.000            9


                CONDITIONS  DE  MAXWELL  (     2880.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.523598776     m ;  Time  (for ref. rigidity & particle) =   8.460222E-08 s 

************************************************************************************************************************************
      7  Keyword, label(s) :  MARKER      #E_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
      8  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
      9  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 2
                     9-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :   1.000     cm   (i.e.,   2.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925    -0.000     0.000     0.000            1
  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925    -0.000     0.000     0.000            2
  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925    -0.000     0.000     0.000            3
  A    1 23.9440   309.473     0.000     0.000     0.000            1.047   309.473    -0.000     0.000     0.000            4
  A    1 23.9440   309.473     0.000     0.000     0.000            1.047   309.473    -0.000     0.000     0.000            5
  A    1 23.9440   309.473     0.000     0.000     0.000            1.047   309.473    -0.000     0.000     0.000            6
  A    1 47.0649   608.309     0.000     0.000     0.000            1.047   608.309    -0.000     0.000     0.000            7
  A    1 47.0649   608.309     0.000     0.000     0.000            1.047   608.309    -0.000     0.000     0.000            8
  A    1 47.0649   608.309     0.000     0.000     0.000            1.047   608.309    -0.000     0.000     0.000            9


                CONDITIONS  DE  MAXWELL  (     2880.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    1.04719755     m ;  Time  (for ref. rigidity & particle) =   1.692044E-07 s 

************************************************************************************************************************************
     10  Keyword, label(s) :  MARKER      #E_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     11  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     12  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 2
                     9-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :   1.000     cm   (i.e.,   2.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925    -0.000     0.000     0.000            1
  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925    -0.000     0.000     0.000            2
  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925    -0.000     0.000     0.000            3
  A    1 23.9440   309.473     0.000     0.000     0.000            1.047   309.473    -0.000     0.000     0.000            4
  A    1 23.9440   309.473     0.000     0.000     0.000            1.047   309.473    -0.000     0.000     0.000            5
  A    1 23.9440   309.473     0.000     0.000     0.000            1.047   309.473    -0.000     0.000     0.000            6
  A    1 47.0649   608.309     0.000     0.000     0.000            1.047   608.309    -0.000     0.000     0.000            7
  A    1 47.0649   608.309     0.000     0.000     0.000            1.047   608.309    -0.000     0.000     0.000            8
  A    1 47.0649   608.309     0.000     0.000     0.000            1.047   608.309    -0.000     0.000     0.000            9


                CONDITIONS  DE  MAXWELL  (     2880.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    1.57079633     m ;  Time  (for ref. rigidity & particle) =   2.538067E-07 s 

************************************************************************************************************************************
     13  Keyword, label(s) :  MARKER      #E_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     14  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     15  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 2
                     9-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :   1.000     cm   (i.e.,   2.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925     0.000     0.000     0.000            1
  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925     0.000     0.000     0.000            2
  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925     0.000     0.000     0.000            3
  A    1 23.9440   309.473     0.000     0.000     0.000            1.047   309.473     0.000     0.000     0.000            4
  A    1 23.9440   309.473     0.000     0.000     0.000            1.047   309.473     0.000     0.000     0.000            5
  A    1 23.9440   309.473     0.000     0.000     0.000            1.047   309.473     0.000     0.000     0.000            6
  A    1 47.0649   608.309     0.000     0.000     0.000            1.047   608.309     0.000     0.000     0.000            7
  A    1 47.0649   608.309     0.000     0.000     0.000            1.047   608.309     0.000     0.000     0.000            8
  A    1 47.0649   608.309     0.000     0.000     0.000            1.047   608.309     0.000     0.000     0.000            9


                CONDITIONS  DE  MAXWELL  (     2880.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    2.09439510     m ;  Time  (for ref. rigidity & particle) =   3.384089E-07 s 

************************************************************************************************************************************
     16  Keyword, label(s) :  MARKER      #E_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     17  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     18  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 2
                     9-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :   1.000     cm   (i.e.,   2.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925     0.000     0.000     0.000            1
  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925     0.000     0.000     0.000            2
  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925     0.000     0.000     0.000            3
  A    1 23.9440   309.473     0.000     0.000     0.000            1.047   309.473     0.000     0.000     0.000            4
  A    1 23.9440   309.473     0.000     0.000     0.000            1.047   309.473     0.000     0.000     0.000            5
  A    1 23.9440   309.473     0.000     0.000     0.000            1.047   309.473     0.000     0.000     0.000            6
  A    1 47.0649   608.309     0.000     0.000     0.000            1.047   608.309     0.000     0.000     0.000            7
  A    1 47.0649   608.309     0.000     0.000     0.000            1.047   608.309     0.000     0.000     0.000            8
  A    1 47.0649   608.309     0.000     0.000     0.000            1.047   608.309     0.000     0.000     0.000            9


                CONDITIONS  DE  MAXWELL  (     2880.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    2.61799388     m ;  Time  (for ref. rigidity & particle) =   4.230111E-07 s 

************************************************************************************************************************************
     19  Keyword, label(s) :  MARKER      #E_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     20  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     21  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 2
                     9-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :   1.000     cm   (i.e.,   2.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925     0.000     0.000     0.000            1
  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925     0.000     0.000     0.000            2
  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925     0.000     0.000     0.000            3
  A    1 23.9440   309.473     0.000     0.000     0.000            1.047   309.473    -0.000     0.000     0.000            4
  A    1 23.9440   309.473     0.000     0.000     0.000            1.047   309.473    -0.000     0.000     0.000            5
  A    1 23.9440   309.473     0.000     0.000     0.000            1.047   309.473    -0.000     0.000     0.000            6
  A    1 47.0649   608.309     0.000     0.000     0.000            1.047   608.309    -0.000     0.000     0.000            7
  A    1 47.0649   608.309     0.000     0.000     0.000            1.047   608.309    -0.000     0.000     0.000            8
  A    1 47.0649   608.309     0.000     0.000     0.000            1.047   608.309    -0.000     0.000     0.000            9


                CONDITIONS  DE  MAXWELL  (     2880.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    3.14159265     m ;  Time  (for ref. rigidity & particle) =   5.076133E-07 s 

************************************************************************************************************************************
     22  Keyword, label(s) :  MARKER      #E_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     23  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #     22)
                                                  9 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o   1   1.0000    12.925     0.000     0.000     0.000      0.0000   0.0000   12.925    0.000    0.000    0.000   8.120937E+01     1
               Time of flight (mus) :  0.13121675     mass (MeV/c2) :   938.272    
o   1   1.0000    12.925     0.000     0.000     0.000      0.0000   0.0000   12.925    0.000    0.000    0.000   8.120937E+01     2
               Time of flight (mus) :  0.13121675     mass (MeV/c2) :   938.272    
o   1   1.0000    12.925     0.000     0.000     0.000      0.0000   0.0000   12.925    0.000    0.000    0.000   8.120937E+01     3
               Time of flight (mus) :  0.13121675     mass (MeV/c2) :   938.272    
i   1  23.9440   309.473     0.000     0.000     0.000      0.0000  22.9440  309.473   -0.000    0.000    0.000   1.944476E+03     4
               Time of flight (mus) :  0.14634703     mass (MeV/c2) :   938.272    
i   1  23.9440   309.473     0.000     0.000     0.000      0.0000  22.9440  309.473   -0.000    0.000    0.000   1.944476E+03     5
               Time of flight (mus) :  0.14634703     mass (MeV/c2) :   938.272    
i   1  23.9440   309.473     0.000     0.000     0.000      0.0000  22.9440  309.473   -0.000    0.000    0.000   1.944476E+03     6
               Time of flight (mus) :  0.14634703     mass (MeV/c2) :   938.272    
h   1  47.0649   608.309     0.000     0.000     0.000      0.0000  46.0649  608.309   -0.000    0.000    0.000   3.822117E+03     7
               Time of flight (mus) :  0.18293379     mass (MeV/c2) :   938.272    
h   1  47.0649   608.309     0.000     0.000     0.000      0.0000  46.0649  608.309   -0.000    0.000    0.000   3.822117E+03     8
               Time of flight (mus) :  0.18293379     mass (MeV/c2) :   938.272    
h   1  47.0649   608.309     0.000     0.000     0.000      0.0000  46.0649  608.309   -0.000    0.000    0.000   3.822117E+03     9
               Time of flight (mus) :  0.18293379     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface/pi              alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                                   in ellips,  out 
   2.5922E-12 [m.rad]      1.7176E+00   2.2792E+12   3.102355E+00   1.492291E-12        9        9    1.000      (Y,T)         1
   0.0000E+00 [m.rad]      0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        9        9    1.000      (Z,P)         1
   8.1450E-06 [mu_s.MeV]  -4.1389E+05   5.7870E+01   1.534992E-01   1.595647E+02        9        9    1.000      (t,K)         1

(Y,T)  space (units : m, rad   ) :  
      sigma_Y = sqrt(surface/pi * beta) =   2.430651E+00
      sigma_T = sqrt(surface/pi * (1+alpha^2)/beta) =   2.119544E-12

(Z,P)  space (units : m, rad   ) :  
      sigma_Z = sqrt(surface/pi * beta) =   0.000000E+00
      sigma_P = sqrt(surface/pi * (1+alpha^2)/beta) =   0.000000E+00

(t,K)  space (units : mu_s, MeV) :  
      sigma_t = sqrt(surface/pi * beta) =   2.171064E-02
      sigma_K = sqrt(surface/pi * (1+alpha^2)/beta) =   1.552756E+02


  Beam  sigma  matrix  and  determinants : 

   5.908062E+00  -4.452232E-12   0.000000E+00   0.000000E+00
  -4.452232E-12   4.492468E-24   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     2.592182E-12    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)
      normalized (*beta*gamma) :     5.352470E-14    0.000000E+00

************************************************************************************************************************************
     24  Keyword, label(s) :  SPINR                                                                                    IPASS= 1


                              Spin rotator. Axis at   0.000000E+00 deg.

                                     Angle  =   3.000000E+01 deg. 

************************************************************************************************************************************
     25  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #     24)
                                                  9 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o   1   1.0000    12.925     0.000     0.000     0.000      0.0000   0.0000   12.925    0.000    0.000    0.000   8.120937E+01     1
               Time of flight (mus) :  0.13121675     mass (MeV/c2) :   938.272    
o   1   1.0000    12.925     0.000     0.000     0.000      0.0000   0.0000   12.925    0.000    0.000    0.000   8.120937E+01     2
               Time of flight (mus) :  0.13121675     mass (MeV/c2) :   938.272    
o   1   1.0000    12.925     0.000     0.000     0.000      0.0000   0.0000   12.925    0.000    0.000    0.000   8.120937E+01     3
               Time of flight (mus) :  0.13121675     mass (MeV/c2) :   938.272    
i   1  23.9440   309.473     0.000     0.000     0.000      0.0000  22.9440  309.473   -0.000    0.000    0.000   1.944476E+03     4
               Time of flight (mus) :  0.14634703     mass (MeV/c2) :   938.272    
i   1  23.9440   309.473     0.000     0.000     0.000      0.0000  22.9440  309.473   -0.000    0.000    0.000   1.944476E+03     5
               Time of flight (mus) :  0.14634703     mass (MeV/c2) :   938.272    
i   1  23.9440   309.473     0.000     0.000     0.000      0.0000  22.9440  309.473   -0.000    0.000    0.000   1.944476E+03     6
               Time of flight (mus) :  0.14634703     mass (MeV/c2) :   938.272    
h   1  47.0649   608.309     0.000     0.000     0.000      0.0000  46.0649  608.309   -0.000    0.000    0.000   3.822117E+03     7
               Time of flight (mus) :  0.18293379     mass (MeV/c2) :   938.272    
h   1  47.0649   608.309     0.000     0.000     0.000      0.0000  46.0649  608.309   -0.000    0.000    0.000   3.822117E+03     8
               Time of flight (mus) :  0.18293379     mass (MeV/c2) :   938.272    
h   1  47.0649   608.309     0.000     0.000     0.000      0.0000  46.0649  608.309   -0.000    0.000    0.000   3.822117E+03     9
               Time of flight (mus) :  0.18293379     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface/pi              alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                                   in ellips,  out 
   2.5922E-12 [m.rad]      1.7176E+00   2.2792E+12   3.102355E+00   1.492291E-12        9        9    1.000      (Y,T)         1
   0.0000E+00 [m.rad]      0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        9        9    1.000      (Z,P)         1
   8.1450E-06 [mu_s.MeV]  -4.1389E+05   5.7870E+01   1.534992E-01   1.595647E+02        9        9    1.000      (t,K)         1

(Y,T)  space (units : m, rad   ) :  
      sigma_Y = sqrt(surface/pi * beta) =   2.430651E+00
      sigma_T = sqrt(surface/pi * (1+alpha^2)/beta) =   2.119544E-12

(Z,P)  space (units : m, rad   ) :  
      sigma_Z = sqrt(surface/pi * beta) =   0.000000E+00
      sigma_P = sqrt(surface/pi * (1+alpha^2)/beta) =   0.000000E+00

(t,K)  space (units : mu_s, MeV) :  
      sigma_t = sqrt(surface/pi * beta) =   2.171064E-02
      sigma_K = sqrt(surface/pi * (1+alpha^2)/beta) =   1.552756E+02


  Beam  sigma  matrix  and  determinants : 

   5.908062E+00  -4.452232E-12   0.000000E+00   0.000000E+00
  -4.452232E-12   4.492468E-24   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     2.592182E-12    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)
      normalized (*beta*gamma) :     5.352470E-14    0.000000E+00

************************************************************************************************************************************
     26  Keyword, label(s) :  SPNPRT      MATRIX                                                                       IPASS= 1

                          -- 3  GROUPS  OF  MOMENTA  FOLLOW   --

                    ----------------------------------------------------------------------------------
                    Momentum  group  #1 (D=   1.000000E+00) ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350        -0.231692   0.188869   0.493944   0.577350   1.793230  60.334870  21.467564


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   |Si,Sf|   (Z,Sf_yz) (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_yz : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.268269  0.834281  0.481672  1.000000      1.0002   74.439   63.435   61.205    1
 o  1  0.000000  1.000000  0.000000  1.000000    -0.963344  0.232327  0.134134  1.000000      1.0002   76.566   63.435   82.291    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.500000  0.866025  1.000000      1.0002   30.000   49.107   30.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  2.6827E-01  1.0000E+00  0.0000E+00  8.3428E-01  0.0000E+00  4.8167E-01  1.0000E+00  1.0000E+00  1.00000E+00  1.00021E+00     1   1
 -9.6334E-01  0.0000E+00  2.3233E-01  1.0000E+00  0.0000E+00  1.3413E-01  1.0000E+00  1.0000E+00  1.00000E+00  1.00021E+00     2   1
  0.0000E+00  0.0000E+00 -5.0000E-01  0.0000E+00  8.6603E-01  1.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.00021E+00     3   1


                  Spin transfer matrix, momentum group # 1 :

          0.268269       -0.963344         0.00000    
          0.834281        0.232327       -0.500000    
          0.481672        0.134134        0.866025    

     Determinant =       1.0000000000
     Trace =       1.3666212399;   spin precession acos((trace-1)/2) =      79.4373462271 deg
     Precession axis :  ( 0.3225, -0.2450,  0.9143)  ->  angle to (X,Y) plane, to X axis :    66.1073,   -37.2194 deg
     Spin precession/2pi (or Qs, fractional) :    2.2066E-01

                    ----------------------------------------------------------------------------------
                    Momentum  group  #2 (D=   2.394395E+01) ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350         0.333333   0.122009   0.455342   0.577350   2.000000  20.000006  14.142127


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   |Si,Sf|   (Z,Sf_yz) (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_yz : projection of Sf on YZ plane)

 i  1  1.000000  0.000000  0.000000  1.000000     1.000000  0.000000  0.000000  1.000000      1.1155    0.000   63.435   90.000    4
 i  1  0.000000  1.000000  0.000000  1.000000    -0.000000  0.866025  0.500000  1.000000      1.1155   30.000   63.435   60.000    5
 i  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.500000  0.866025  1.000000      1.1155   30.000   49.107   30.000    6



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  1.0000E+00  1.0000E+00  0.0000E+00  2.8059E-07  0.0000E+00  1.6200E-07  1.0000E+00  1.0000E+00  2.39440E+01  1.11554E+00     4   1
 -3.2400E-07  0.0000E+00  8.6603E-01  1.0000E+00  0.0000E+00  5.0000E-01  1.0000E+00  1.0000E+00  2.39440E+01  1.11554E+00     5   1
  0.0000E+00  0.0000E+00 -5.0000E-01  0.0000E+00  8.6603E-01  1.0000E+00  1.0000E+00  1.0000E+00  2.39440E+01  1.11554E+00     6   1


                  Spin transfer matrix, momentum group # 2 :

           1.00000       -3.239976E-07     0.00000    
          2.805902E-07    0.866025       -0.500000    
          1.619988E-07    0.500000        0.866025    

     Determinant =       1.0000000000
     Trace =       2.7320508076;   spin precession acos((trace-1)/2) =      30.0000000000 deg
     Precession axis :  ( 1.0000, -0.0000,  0.0000)  ->  angle to (X,Y) plane, to X axis :     0.0000,    -0.0000 deg
     Spin precession/2pi (or Qs, fractional) :    8.3333E-02

                    ----------------------------------------------------------------------------------
                    Momentum  group  #3 (D=   4.706491E+01) ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.333333   0.333333   0.333333   0.577350        -0.333333  -0.455342   0.122008   0.577350   2.500000 119.999992  64.807400


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   |Si,Sf|   (Z,Sf_yz) (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_yz : projection of Sf on YZ plane)

 h  1  1.000000  0.000000  0.000000  1.000000    -1.000000 -0.000000 -0.000000  1.000000      1.3944 -180.000  116.565   90.000    7
 h  1  0.000000  1.000000  0.000000  1.000000     0.000000 -0.866025 -0.500000  1.000000      1.3944 -150.000  116.565  120.000    8
 h  1  0.000000  0.000000  1.000000  1.000000     0.000000 -0.500000  0.866025  1.000000      1.3944   30.000   49.107   30.000    9



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

 -1.0000E+00  1.0000E+00 -3.4319E-07  0.0000E+00 -1.9814E-07  0.0000E+00  1.0000E+00  1.0000E+00  4.70649E+01  1.39443E+00     7   1
  0.0000E+00  3.9628E-07 -8.6603E-01  1.0000E+00 -5.0000E-01  0.0000E+00  1.0000E+00  1.0000E+00  4.70649E+01  1.39443E+00     8   1
  0.0000E+00  0.0000E+00 -5.0000E-01  0.0000E+00  8.6603E-01  1.0000E+00  1.0000E+00  1.0000E+00  4.70649E+01  1.39443E+00     9   1


                  Spin transfer matrix, momentum group # 3 :

          -1.00000        3.962777E-07     0.00000    
         -3.431865E-07   -0.866025       -0.500000    
         -1.981388E-07   -0.500000        0.866025    

     Determinant =       1.0000000000
     Trace =      -1.0000000000;   spin precession acos((trace-1)/2) =     179.9999780994 deg
     Precession axis :  ( 0.0000,  0.2588, -0.9659)  ->  angle to (X,Y) plane, to X axis :   -75.0000,    90.0000 deg
     Spin precession/2pi (or Qs, fractional) :    5.0000E-01

************************************************************************************************************************************
     27  Keyword, label(s) :  END                                                                                      IPASS= 1

                             9 particles have been launched
                     Made  it  to  the  end :      9

************************************************************************************************************************************
 Pgm zgoubi : Execution ended normally, upon keyword END or FIN
   
 File in:   spinTunes.INC.dat
 File out:  zgoubi.res

  Zgoubi, author's Revision: 1590.
  Job  started  on  04-02-2022,  at  17:59:11 
  JOB  ENDED  ON    04-02-2022,  AT  17:59:12 

   CPU time, total :    0.45629999999999998     
