#set zrange  [0:]; set xrange [-25:25]; set yrange [-25:25]; set xyplane 0
#dip1=5; dip2=20; dd=3 # positining of 1st and last dipoles in zgoubi.dat sequence, and increment
dip1=5; dip2=20; dd=3 # positining of 1st and last dipoles in zgoubi.dat sequence, and increment
# magnifies apparent spin tilt   speed up graphic           pi/3                   z norm
         mag = 40.            ;     speedUp=1     ;     pi3 = 4.*atan(1.)/3  ;     nz=0.18

# Change particle number here
# trj = 1:     12.9248888074 0. 0. 0. 0.  1.     'm'                 ! Ggamma=1.793229,    0.200MeV
# trj = 2:     3.0947295453790e2 0. 0. 0. 0.  23.9439548880185 'm'   ! Ggamma=2,         108.411628MeV
# trj = 3:     6.08308775712857e2 0. 0. 0. 0. 47.0649136542578 'm'   ! Ggamma=2.5        370.082556MeV

trj = 3; R=6.08308775712857e2 ; Gg=2.5         ; mag =800
trj = 2; R=3.0947295453790e2 ;  Gg=2.          ; mag =200
trj = 1; R=12.9248888074 ;      Gg= 1.793229;  ; mag =200

set xlabel "orbital angle"; set ylabel "S_X,   S_Y,   S_Z"

set label "Vert. bars: every 2{/Symbol p} spin precession" at 1, 0.4

do for [nPrec=1:4] {
set arrow  from  nPrec* 2*pi/Gg, -1 to  nPrec* 2*pi/Gg, 1  nohead
}

plot \
 "zgoubi.plt" u ($19==trj && $50==1 ? $14/R :1/0):($33) w lp pt 4 ps .4 ,\
 "zgoubi.plt" u ($19==trj && $50==1 ? $14/R :1/0):($34) w lp pt 6 ps .4 ,\
 "zgoubi.plt" u ($19==trj && $50==1 ? $14/R :1/0):($35) w lp pt 8 ps .4 

set terminal postscript eps blacktext color  enh  
set output "gnuplot_Zplt_theta-SZ.eps"  
replot; set terminal X11; unset output

unset label

pause 1

# Just 2D, projected in (X,Y) plane, first:
set title "Six dipoles over a turn, each dipole its color"
set xlabel "X_{Lab}"; set ylabel "Y_{Lab}"; set zlabel "S_Z"; set xtics; set ytics; set ztics  #unset ztics
set size ratio -1

plot \
for [dip=dip1:dip2:dd] "zgoubi.plt"  u \
($19==trj && $42==dip? $10*cos(-$22-pi3*(dip-6.)/3.) +mag*(cos(-$22-pi3*(dip-6.)/3.)*$33-sin(-$22-pi3*(dip-6.)/3.)*$34) :1/0): \
($10*sin(-$22-pi3*(dip-6.)/3.) +mag*(sin(-$22-pi3*(dip-6.)/3.)*$33+cos(-$22-pi3*(dip-6.)/3.)*$34)) w l lw 3 notit ,\
for [dip=dip1:dip2:dd] "zgoubi.plt"  u \
($19==trj && $42==dip? $10*cos(-$22-pi3*(dip-6.)/3.) :1/0): \
($10*sin(-$22-pi3*(dip-6.)/3.) ) w l lw 10 notit 

pause 1

set terminal postscript eps blacktext color  enh  
set output "gnuplot_Zplt_spinDance_2D.eps"  
#
plot \
for [dip=dip1:dip2:dd] "zgoubi.plt"  u \
($19==trj && $42==dip? $10*cos(-$22-pi3*(dip-6.)/3.) +mag*(cos(-$22-pi3*(dip-6.)/3.)*$33-sin(-$22-pi3*(dip-6.)/3.)*$34) :1/0): \
($10*sin(-$22-pi3*(dip-6.)/3.) +mag*(sin(-$22-pi3*(dip-6.)/3.)*$33+cos(-$22-pi3*(dip-6.)/3.)*$34)) w l lw 3 notit 
#
set terminal X11; unset output   

exit
