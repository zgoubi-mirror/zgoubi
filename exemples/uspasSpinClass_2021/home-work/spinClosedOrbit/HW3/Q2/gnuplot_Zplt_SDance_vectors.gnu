# CHOOSE PARTICLE TO PLOT, FROM HERE:
trj = 3; R=6.08308775712857e2 ; Gg=2.5         ; mag =800
trj = 1; R=12.9248888074 ;      Gg= 1.793229;  ; mag =20
trj = 2; R=3.0947295453790e2 ;  Gg=2.          ; mag =200
# Positining of 1st and last dipoles in zgoubi.dat sequence, and increment
dip1=5; dip2=20; dd=3 ; pi3 = 4.*atan(1.)/3 

set xlabel "orbital angle"; set ylabel "S_X,   S_Y,   S_Z"; set yrange [-1:+1]
set label "Vert. bars: every 2{/Symbol p} spin precession" at 1, 0.4
do for [nPrec=1:4] {
set arrow  from  nPrec* 2*pi/Gg, -1 to  nPrec* 2*pi/Gg, 1  nohead
}

# spin components versus orbital angle:
plot \
 "zgoubi.plt" u ($19==trj && $50==1 ? $14/R :1/0):($33) w lp pt 4 ps .4 ,\
 "zgoubi.plt" u ($19==trj && $50==1 ? $14/R :1/0):($34) w lp pt 6 ps .4 ,\
 "zgoubi.plt" u ($19==trj && $50==1 ? $14/R :1/0):($35) w lp pt 8 ps .4 

set terminal postscript eps blacktext color  enh  
set output "gnuplot_Zplt_theta-SZ.eps"  
replot; set terminal X11; unset output

pause 1

# 2D: spin projected in (X,Y) plane, first:
#set title "Six dipoles over a turn, each dipole its color \n Thick circle: trajectory. Spin vector angle to Z is magnified"
unset yrange; unset label; unset arrow
# set xlabel "X_{Lab}"; set ylabel "Y_{Lab}"; set zlabel "S_Z"; set xtics; set ytics; set ztics; set size ratio -1

unset border ; unset xlabel; unset ylabel; unset xtics; unset ytics;  
set size ratio -1


plot \
for [dip=dip1:dip2:dd] \
"zgoubi.plt"  u ($19==trj && $42==dip? $10*cos(-$22-pi3*(dip-6.)/3.) :1/0):($10*sin(-$22-pi3*(dip-6.)/3.) ) w l lw 6 notit ,\
for [dip=dip1:dip2:dd] \
"zgoubi.plt"  u ($19==trj && $42==dip? $10*cos(-$22-pi3*(dip-6.)/3.) :1/0):($10*sin(-$22-pi3*(dip-6.)/3.) ):($19==trj && $42==dip? mag*(cos(-$22-pi3*(dip-6.)/3.)*$33-sin(-$22-pi3*(dip-6.)/3.)*$34) :1/0):(mag*(sin(-$22-pi3*(dip-6.)/3.)*$33+cos(-$22-pi3*(dip-6.)/3.)*$34)) w vectors  notit

set terminal postscript eps blacktext color  enh ; set output "gnuplot_Zplt_spinDance_2D.eps"
set size ratio -1
plot \
for [dip=dip1:dip2:dd] \
"zgoubi.plt"  u ($19==trj && $42==dip? $10*cos(-$22-pi3*(dip-6.)/3.) :1/0):($10*sin(-$22-pi3*(dip-6.)/3.) ) w l lw 6 notit ,\
for [dip=dip1:dip2:dd] \
"zgoubi.plt"  u ($19==trj && $42==dip? $10*cos(-$22-pi3*(dip-6.)/3.) :1/0):($10*sin(-$22-pi3*(dip-6.)/3.) ):($19==trj && $42==dip? mag*(cos(-$22-pi3*(dip-6.)/3.)*$33-sin(-$22-pi3*(dip-6.)/3.)*$34) :1/0):(mag*(sin(-$22-pi3*(dip-6.)/3.)*$33+cos(-$22-pi3*(dip-6.)/3.)*$34)) w vectors notit
set terminal X11; unset output   

pause 1

exit
