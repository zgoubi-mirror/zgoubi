Cyclotron, classical. Analytical model of dipole field. 
'OBJET'   
64.62444403717985                                                                     ! 200keV proton.
2
1 1
12.9248888074 0. 0. 0. 0.  1.  'm'         ! D=1 => 200keV proton. R=Brho/B=64.624444037[kG.cm]/5[kG].
1
'PARTICUL'                                               ! This is required to get the time-of-flight, 
PROTON                                          ! otherwise, by default zgoubi only requires rigidity.
'FAISCEAU'                                                               ! Local particle coordinates.
'MARKER'    #S_60degSectorUnifB                               ! Label should not exceed 20 characters.
'DIPOLE'                                                     ! Analytical modeling of a dipole magnet. 
20                  ! IL=2, only purpose is to logged trajectories in zgoubi.plt, for further plotting. 
60. 50.                                                        ! Sector angle AT; reference radius RM.
30.  5. 0. 0. 0.                 ! Reference azimuthal angle ACN; BM field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge, 
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6                      ! Entrance face placed at omega+=30 deg from ACN.
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6                        ! Exit face placed at omega-=-30 deg from ACN.
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
2 10    ! '2' is for 2nd degree interpolation. Could also be '25' (5*5 points grid) or 4 (4th degree).
1.                               ! Integration step size. Small enough for orbits to close accurately.
2  0. 0. 0. 0.                  ! Magnet positionning RE, TE, RS, TS. Could be instead non-zero, e.g., 
!                                2 RE=50. 0. RS=50. 0., as long as Yo is amended accordingly in OBJET.
'MARKER'    #E_60degSectorUnifB                               ! Label should not exceed 20 characters.
'FAISCEAU'                                                               ! Local particle coordinates.
'FIT'                                  ! Adjust Yo at OBJET so to get final Y = Y0 -> a closed circle.
1   nofinal   
1 30 0 [12.,65.]                                                                      ! Variable : Yo.
1  2e-12  199              ! constraint; default penalty would be 1e-10; maximu 199 calls to function.
3.1 1 2 #End 0. 1. 0                                                        ! Constraint:  Y_final=Yo.
 'FAISTORE'                                                   ! Log particle data here, to zgoubi.fai,
zgoubi.fai                                                 ! for further plotting (by gnuplot, below).
1
'REBELOTE'                                                                ! Momentum scan, 60 samples.
4  0.2  0        1 60 different rigidities; log to video ; take initial coordinates as found in OBJET.
1                                                          ! Change parameter(s) as stated next lines.
OBJET 35  1:5.0063899693             ! Change relative rigity (35) in OBJET; range (0.2 MeV to 5 MeV).
 'END'                 
