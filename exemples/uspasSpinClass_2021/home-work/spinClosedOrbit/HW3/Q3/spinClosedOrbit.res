Cyclotron, classical. Synchronized spin kick in a uniform field
'OBJET'                                                                                                      1
64.62444403717985                      ! Reference Brho ("BORO" in the users' guide) -> 200keV proton.
2
3 1
12.9248888074 0. 0. 0. 0.  1.     'm'                 ! Ggamma=1.793229,    0.200MeV
3.0947295453790e2 0. 0. 0. 0.  23.9439548880185 'm'   ! Ggamma=2,         108.411628MeV
6.08308775712857e2 0. 0. 0. 0. 47.0649136542578 'm'   ! Ggamma=2.5        370.082556MeV
1 1 1                           ! For any particle: set to 1 to enable ray-tracing, or -9 to ignore.
 
'PARTICUL'                                          ! This is required for spin motion to be computed,       2
PROTON                                          ! otherwise, by default zgoubi only requires rigidity.
'SPNTRK'                                                                      ! Request spin tracking.       3
4                                                 ! Individual initial spins taken parallel to Z axis.
0.32253242 -0.24498407 0.91430610
1. 0. 0.
0. -0.25881905 0.96592584
 
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./60degSector.inc[#S_60degSectorUnifB,*:#E_6(n_inc, depth : *1 2)
'MARKER'    #S_60degSectorUnifB                               ! Label should not exceed 20 characters.       4
'DIPOLE'                                                     ! Analytical modeling of a dipole magnet.       5
2                  ! IL=2, only purpose is to logged trajectories in zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30.  5. 0. 0. 0.                 ! Reference azimuthal angle ACN; BM field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6                      ! Entrance face placed at omega+=30 deg from ACN.
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6                        ! Exit face placed at omega-=-30 deg from ACN.
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
2 10    ! '2' is for 2nd degree interpolation. Could also be '25' (5*5 points grid) or 4 (4th degree).
1.                               ! Integration step size. Small enough for orbits to close accurately.
2  0. 0. 0. 0.                  ! Magnet positionning RE, TE, RS, TS. Could be instead non-zero, e.g.,
!                                2 RE=50. 0. RS=50. 0., as long as Yo is amended accordingly in OBJET.
! Include_SegmentEnd : ./60degSector.inc                             (had n_inc, depth :  1 2)
'MARKER'    #E_60degSectorUnifB                               ! Label should not exceed 20 characters.       6
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./60degSector.inc[#S_60degSectorUnifB,*:#E_6(n_inc, depth : *1 2)
'MARKER'    #S_60degSectorUnifB                               ! Label should not exceed 20 characters.       7
'DIPOLE'                                                     ! Analytical modeling of a dipole magnet.       8
2                  ! IL=2, only purpose is to logged trajectories in zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30.  5. 0. 0. 0.                 ! Reference azimuthal angle ACN; BM field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6                      ! Entrance face placed at omega+=30 deg from ACN.
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6                        ! Exit face placed at omega-=-30 deg from ACN.
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
2 10    ! '2' is for 2nd degree interpolation. Could also be '25' (5*5 points grid) or 4 (4th degree).
1.                               ! Integration step size. Small enough for orbits to close accurately.
2  0. 0. 0. 0.                  ! Magnet positionning RE, TE, RS, TS. Could be instead non-zero, e.g.,
!                                2 RE=50. 0. RS=50. 0., as long as Yo is amended accordingly in OBJET.
! Include_SegmentEnd : ./60degSector.inc                             (had n_inc, depth :  1 2)
'MARKER'    #E_60degSectorUnifB                               ! Label should not exceed 20 characters.       9
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./60degSector.inc[#S_60degSectorUnifB,*:#E_6(n_inc, depth : *1 2)
'MARKER'    #S_60degSectorUnifB                               ! Label should not exceed 20 characters.      10
'DIPOLE'                                                     ! Analytical modeling of a dipole magnet.      11
2                  ! IL=2, only purpose is to logged trajectories in zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30.  5. 0. 0. 0.                 ! Reference azimuthal angle ACN; BM field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6                      ! Entrance face placed at omega+=30 deg from ACN.
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6                        ! Exit face placed at omega-=-30 deg from ACN.
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
2 10    ! '2' is for 2nd degree interpolation. Could also be '25' (5*5 points grid) or 4 (4th degree).
1.                               ! Integration step size. Small enough for orbits to close accurately.
2  0. 0. 0. 0.                  ! Magnet positionning RE, TE, RS, TS. Could be instead non-zero, e.g.,
!                                2 RE=50. 0. RS=50. 0., as long as Yo is amended accordingly in OBJET.
! Include_SegmentEnd : ./60degSector.inc                             (had n_inc, depth :  1 2)
'MARKER'    #E_60degSectorUnifB                               ! Label should not exceed 20 characters.      12
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./60degSector.inc[#S_60degSectorUnifB,*:#E_6(n_inc, depth : *1 2)
'MARKER'    #S_60degSectorUnifB                               ! Label should not exceed 20 characters.      13
'DIPOLE'                                                     ! Analytical modeling of a dipole magnet.      14
2                  ! IL=2, only purpose is to logged trajectories in zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30.  5. 0. 0. 0.                 ! Reference azimuthal angle ACN; BM field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6                      ! Entrance face placed at omega+=30 deg from ACN.
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6                        ! Exit face placed at omega-=-30 deg from ACN.
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
2 10    ! '2' is for 2nd degree interpolation. Could also be '25' (5*5 points grid) or 4 (4th degree).
1.                               ! Integration step size. Small enough for orbits to close accurately.
2  0. 0. 0. 0.                  ! Magnet positionning RE, TE, RS, TS. Could be instead non-zero, e.g.,
!                                2 RE=50. 0. RS=50. 0., as long as Yo is amended accordingly in OBJET.
! Include_SegmentEnd : ./60degSector.inc                             (had n_inc, depth :  1 2)
'MARKER'    #E_60degSectorUnifB                               ! Label should not exceed 20 characters.      15
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./60degSector.inc[#S_60degSectorUnifB,*:#E_6(n_inc, depth : *1 2)
'MARKER'    #S_60degSectorUnifB                               ! Label should not exceed 20 characters.      16
'DIPOLE'                                                     ! Analytical modeling of a dipole magnet.      17
2                  ! IL=2, only purpose is to logged trajectories in zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30.  5. 0. 0. 0.                 ! Reference azimuthal angle ACN; BM field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6                      ! Entrance face placed at omega+=30 deg from ACN.
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6                        ! Exit face placed at omega-=-30 deg from ACN.
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
2 10    ! '2' is for 2nd degree interpolation. Could also be '25' (5*5 points grid) or 4 (4th degree).
1.                               ! Integration step size. Small enough for orbits to close accurately.
2  0. 0. 0. 0.                  ! Magnet positionning RE, TE, RS, TS. Could be instead non-zero, e.g.,
!                                2 RE=50. 0. RS=50. 0., as long as Yo is amended accordingly in OBJET.
! Include_SegmentEnd : ./60degSector.inc                             (had n_inc, depth :  1 2)
'MARKER'    #E_60degSectorUnifB                               ! Label should not exceed 20 characters.      18
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./60degSector.inc[#S_60degSectorUnifB,*:#E_6(n_inc, depth : *1 2)
'MARKER'    #S_60degSectorUnifB                               ! Label should not exceed 20 characters.      19
'DIPOLE'                                                     ! Analytical modeling of a dipole magnet.      20
2                  ! IL=2, only purpose is to logged trajectories in zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30.  5. 0. 0. 0.                 ! Reference azimuthal angle ACN; BM field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6                      ! Entrance face placed at omega+=30 deg from ACN.
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6                        ! Exit face placed at omega-=-30 deg from ACN.
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
2 10    ! '2' is for 2nd degree interpolation. Could also be '25' (5*5 points grid) or 4 (4th degree).
1.                               ! Integration step size. Small enough for orbits to close accurately.
2  0. 0. 0. 0.                  ! Magnet positionning RE, TE, RS, TS. Could be instead non-zero, e.g.,
!                                2 RE=50. 0. RS=50. 0., as long as Yo is amended accordingly in OBJET.
! Include_SegmentEnd : ./60degSector.inc                             (had n_inc, depth :  1 2)
'MARKER'    #E_60degSectorUnifB                               ! Label should not exceed 20 characters.      21
'FAISCEAU'                                                                                                  22
 
'SPINR'                                                                                                     23
1                                                                                     ! Spin rotation,
0. 30.                                                          1 about the X-axis, by 10 degree here.
 
'SPNPRT'                                                                                                    24
 
'END'

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 1

     Particle  properties :
     PROTON
                     Mass          =    938.272        MeV/c2
                     Charge        =   1.602176E-19    C     
                     G  factor     =    1.79285              
                     COM life-time =   1.000000E+99    s     

              Reference  data :
                    mag. rigidity (kG.cm)   :   64.624444      =p/q, such that dev.=B*L/rigidity
                    mass (MeV/c2)           :   938.27208    
                    momentum (MeV/c)        :   19.373921    
                    energy, total (MeV)     :   938.47208    
                    energy, kinetic (MeV)   :  0.19999999    
                    beta = v/c              :  2.0644110049E-02
                    gamma                   :   1.000213158    
                    beta*gamma              :  2.0648510502E-02
                    G*gamma                 :   1.793229509    
                    electric rigidity (MeV) :  0.3999573557    =T[eV]*(gamma+1)/gamma, such that dev.=E*L/rigidity
  
 I, AMQ(1,I), AMQ(2,I)/QE, P/Pref, v/c, time, s :
  
     1   9.38272081E+02  1.00000000E+00  1.00000000E+00  2.06441100E-02  0.00000000E+00  0.00000000E+00
     2   9.38272081E+02  1.00000000E+00  2.39439549E+01  4.43198144E-01  0.00000000E+00  0.00000000E+00
     3   9.38272081E+02  1.00000000E+00  4.70649137E+01  6.96930226E-01  0.00000000E+00  0.00000000E+00

************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 1


                Spin  tracking  requested.


                          Particle  mass          =    938.2721     MeV/c2
                          Gyromagnetic  factor  G =    1.792847    

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 may be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO    =        64.624 kG*cm
                               beta    =    0.02064411
                               gamma   =   1.00021316
                               gamma*G =   1.7932295094


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.561982
                               <SY> =    -0.214081
                               <SZ> =     0.798965
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
      5  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


                OPEN FILE zgoubi.plt                                                                      
                FOR PRINTING TRAJECTORIES

                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 2
                     9-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :   1.000     cm   (i.e.,   2.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925    -0.000     0.000     0.000            1
  A    1 23.9440   309.473     0.000     0.000     0.000            1.047   309.473    -0.000     0.000     0.000            2
  A    1 47.0649   608.309     0.000     0.000     0.000            1.047   608.309    -0.000     0.000     0.000            3


                CONDITIONS  DE  MAXWELL  (      960.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.523598776     m ;  Time  (for ref. rigidity & particle) =   8.460222E-08 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #E_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
      7  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
      8  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 2
                     9-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :   1.000     cm   (i.e.,   2.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925    -0.000     0.000     0.000            1
  A    1 23.9440   309.473     0.000     0.000     0.000            1.047   309.473    -0.000     0.000     0.000            2
  A    1 47.0649   608.309     0.000     0.000     0.000            1.047   608.309    -0.000     0.000     0.000            3


                CONDITIONS  DE  MAXWELL  (      960.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    1.04719755     m ;  Time  (for ref. rigidity & particle) =   1.692044E-07 s 

************************************************************************************************************************************
      9  Keyword, label(s) :  MARKER      #E_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     10  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     11  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 2
                     9-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :   1.000     cm   (i.e.,   2.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925    -0.000     0.000     0.000            1
  A    1 23.9440   309.473     0.000     0.000     0.000            1.047   309.473    -0.000     0.000     0.000            2
  A    1 47.0649   608.309     0.000     0.000     0.000            1.047   608.309    -0.000     0.000     0.000            3


                CONDITIONS  DE  MAXWELL  (      960.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    1.57079633     m ;  Time  (for ref. rigidity & particle) =   2.538067E-07 s 

************************************************************************************************************************************
     12  Keyword, label(s) :  MARKER      #E_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     13  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     14  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 2
                     9-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :   1.000     cm   (i.e.,   2.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925     0.000     0.000     0.000            1
  A    1 23.9440   309.473     0.000     0.000     0.000            1.047   309.473     0.000     0.000     0.000            2
  A    1 47.0649   608.309     0.000     0.000     0.000            1.047   608.309     0.000     0.000     0.000            3


                CONDITIONS  DE  MAXWELL  (      960.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    2.09439510     m ;  Time  (for ref. rigidity & particle) =   3.384089E-07 s 

************************************************************************************************************************************
     15  Keyword, label(s) :  MARKER      #E_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     16  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     17  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 2
                     9-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :   1.000     cm   (i.e.,   2.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925     0.000     0.000     0.000            1
  A    1 23.9440   309.473     0.000     0.000     0.000            1.047   309.473     0.000     0.000     0.000            2
  A    1 47.0649   608.309     0.000     0.000     0.000            1.047   608.309     0.000     0.000     0.000            3


                CONDITIONS  DE  MAXWELL  (      960.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    2.61799388     m ;  Time  (for ref. rigidity & particle) =   4.230111E-07 s 

************************************************************************************************************************************
     18  Keyword, label(s) :  MARKER      #E_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     19  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     20  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 2
                     9-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :   1.000     cm   (i.e.,   2.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925     0.000     0.000     0.000            1
  A    1 23.9440   309.473     0.000     0.000     0.000            1.047   309.473     0.000     0.000     0.000            2
  A    1 47.0649   608.309     0.000     0.000     0.000            1.047   608.309    -0.000     0.000     0.000            3


                CONDITIONS  DE  MAXWELL  (      960.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    3.14159265     m ;  Time  (for ref. rigidity & particle) =   5.076133E-07 s 

************************************************************************************************************************************
     21  Keyword, label(s) :  MARKER      #E_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     22  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #     21)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

m   1   1.0000    12.925     0.000     0.000     0.000      0.0000   0.0000   12.925    0.000    0.000    0.000   8.120937E+01     1
               Time of flight (mus) :  0.13121675     mass (MeV/c2) :   938.272    
m   1  23.9440   309.473     0.000     0.000     0.000      0.0000  22.9440  309.473    0.000    0.000    0.000   1.944476E+03     2
               Time of flight (mus) :  0.14634704     mass (MeV/c2) :   938.272    
m   1  47.0649   608.309     0.000     0.000     0.000      0.0000  46.0649  608.309   -0.000    0.000    0.000   3.822117E+03     3
               Time of flight (mus) :  0.18293380     mass (MeV/c2) :   938.272    

************************************************************************************************************************************
     23  Keyword, label(s) :  SPINR                                                                                    IPASS= 1


                              Spin rotator. Axis at   0.000000E+00 deg.

                                     Angle  =   3.000000E+01 deg. 

************************************************************************************************************************************
     24  Keyword, label(s) :  SPNPRT                                                                                   IPASS= 1



                         Momentum  group  #1 ; average  over 3 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.440844  -0.167934   0.626744   0.784445         0.440844  -0.167934   0.626744   0.784445   2.097743        NaN        NaN


                Spin  components  of  each  of  the      3  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   |Si,Sf|   (Z,Sf_yz) (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_yz : projection of Sf on YZ plane)

 m  1  0.322532 -0.244984  0.914306  1.000000     0.322532 -0.244984  0.914306  1.000000      1.0002    0.000   45.993   23.893    1
 m  1  1.000000  0.000000  0.000000  1.000000     1.000000  0.000000  0.000000  1.000000      1.1155    0.000   63.435   90.000    2
 m  1  0.000000 -0.258819  0.965926  1.000000    -0.000000 -0.258819  0.965926  1.000000      1.3944    0.000   45.993   15.000    3



                Min/Max  components  of  each  of  the      3  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

  3.2253E-01  3.2253E-01 -2.4498E-01 -2.4498E-01  9.1431E-01  9.1431E-01  1.0000E+00  1.0000E+00  1.00000E+00  1.00021E+00     1   1
  1.0000E+00  1.0000E+00  4.6329E-09  4.6329E-09  2.6748E-09  2.6748E-09  1.0000E+00  1.0000E+00  2.39440E+01  1.11554E+00     2   1
 -3.3686E-09 -3.3686E-09 -2.5882E-01 -2.5882E-01  9.6593E-01  9.6593E-01  1.0000E+00  1.0000E+00  4.70649E+01  1.39443E+00     3   1

************************************************************************************************************************************
     25  Keyword, label(s) :  END                                                                                      IPASS= 1


                             3 particles have been launched
                     Made  it  to  the  end :      3

************************************************************************************************************************************
 Pgm zgoubi : Execution ended normally, upon keyword END or FIN
   
 File in:   spinClosedOrbit.dat
 File out:  zgoubi.res

  Zgoubi, author's dvlpmnt version.
  Job  started  on  29-03-2021,  at  13:00:54 
  JOB  ENDED  ON    29-03-2021,  AT  13:00:54 

   CPU time, total :    0.15267400000000000     
