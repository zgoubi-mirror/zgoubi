
set xlabel "orbital angle [rad]"; set ylabel "cos({/Symbol f})"

set key c r

# Change particle number here
# trj = 1:     12.9248888074 0. 0. 0. 0.  1.     'm'                 ! Ggamma=1.793229,    0.200MeV
# trj = 2:     3.0947295453790e2 0. 0. 0. 0.  23.9439548880185 'm'   ! Ggamma=2,         108.411628MeV
# trj = 3:     6.08308775712857e2 0. 0. 0. 0. 47.0649136542578 'm'   ! Ggamma=2.5        370.082556MeV

array R[3]
trj = 1; R[1]=12.9248888074 ;      Gg= 1.793229;  ; mag =200
trj = 2; R[2]=3.0947295453790e2 ;  Gg=2.          ; mag =200
trj = 3; R[3]=6.08308775712857e2 ; Gg=2.5         ; mag =800

plot for [trj=1:3] \
 "< paste zgoubi_closedSOrbit.plt zgoubi_precess.plt" u ($19==trj ? $14/R[trj] :1/0):(sqrt($33*$85 + $34*$86 + $35*$87)) w p tit 'traj #'.trj

pause 1

set terminal postscript eps blacktext color  enh  
set output "gnuplot_Zplt_spinAngle.eps"  
#
plot for [trj=1:3] \
 "< paste zgoubi_closedSOrbit.plt zgoubi_precess.plt" u ($19==trj ? $14/R[trj] :1/0):(sqrt($33*$85 + $34*$86 + $35*$87)) w p tit 'traj #'.trj
#
set terminal X11; unset output   

exit
