function analyze(x, data){
    n = 0;mean = 0;
    val_min = 0;val_max = 0;

    for(val in data){
        n += 1;
        delta = val - mean;
        mean += delta/n;
        val_min = (n == 1)?val:((val < val_min)?val:val_min);
        val_max = (n == 1)?val:((val > val_max)?val:val_max);
    }
    if(n > 0){
        print x, mean, val_min, val_max;
    }
}

{
    curr = $38;
    yval = $(col_num);

    if(NR==1 || prev != curr){
        analyze(prev, data);
        delete data;
        prev = curr;
    }
    data[yval] = 1;
}

END{
    analyze(curr, data);
}
