
set xlabel "|B_sL|"; set ylabel "P_f"


pi = 4.*atan(1.)
G =  -4.18415
Brho_n =  4.8139470
A = (1+G)/ (2.*pi*Brho_n)
alf =  9.484842e-5

f(x) = 2. * exp(-pi/2. * (A * x )**2/alf) - 1.
zro(x) =0

set arrow from 0.061454684, graph 0 to 0.061454684, graph .5 nohead lt 1 lw .6 dt 2

set label "B_sL = 0.06145" at 0.06145-.0015, -.8 rot 

plot [0:.122]  f(x)  tit "P_f(B_sL)" ,\
  [0:0.061454684] zro(x) w l lw .6 lt 1 dt 2 notit

print " P_f(BL=0.061454684) = ", f(0.061454684)

 set terminal postscript eps blacktext color enh # size 9.3cm,6cm "Times-Roman" 12
 set output "gnuplot_theorSyvsBL.eps"
replot
 set terminal X11
 unset output

pause 2
