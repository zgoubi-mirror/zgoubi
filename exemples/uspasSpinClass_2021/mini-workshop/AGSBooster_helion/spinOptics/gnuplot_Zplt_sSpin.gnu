
set title "Plotted from file zgoubi.plt "

set key maxrow 3
set key t c 

#set logscale y 

set xtics mirror
set ytics mirror

set xlabel 's  [m]'
set ylabel 'S_X, S_Y, S_Z  [m]'

cm2m = 0.01
MeV2eV = 1e6
am = 938.27203
c = 2.99792458e8

set xrange [:205]
set yrange [:1.2]

plot  \
   'zgoubi.plt' u ($19== 1 ? $14 *cm2m : 1/0):($33) w lp ps .2 ti 'S_X, prtcl 1' , \
   'zgoubi.plt' u ($19== 1 ? $14 *cm2m : 1/0):($34) w lp ps .2 ti 'S_Y' , \
   'zgoubi.plt' u ($19== 1 ? $14 *cm2m : 1/0):($35) w lp ps .2 ti 'S_Z'  , \
   'zgoubi.plt' u ($19== 2 ? $14 *cm2m : 1/0):($33) w lp ps .2 ti 'S_X, prtcl 2' , \
   'zgoubi.plt' u ($19== 2 ? $14 *cm2m : 1/0):($34) w lp ps .2 ti 'S_Y' , \
   'zgoubi.plt' u ($19== 2 ? $14 *cm2m : 1/0):($35) w lp ps .2 ti 'S_Z'  , \
   'zgoubi.plt' u ($19== 3 ? $14 *cm2m : 1/0):($33) w lp ps .2 ti 'S_X, prtcl 3' , \
   'zgoubi.plt' u ($19== 3 ? $14 *cm2m : 1/0):($34) w lp ps .2 ti 'S_Y' , \
   'zgoubi.plt' u ($19== 3 ? $14 *cm2m : 1/0):($35) w lp ps .2 ti 'S_Z' 

     set terminal postscript eps color  enh  
       set output "gnuplot_Zplt_sSpin.eps"  
       replot  
       set terminal X11  
       unset output  

 
pause 1
exit

