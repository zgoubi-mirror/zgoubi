'MARKER'  scalin_S
'SCALING'                  ! This sets the power supplies for identical optics, now that the 
1 7    ! reference rigidity has been changed, from 1 Tm down to 0.3074552 (injection value).
BEND
-1
0.3074552
1
MULTIPOL
-1
0.3074552
1
MULTIPOL  QH*                     ! These two families, QH* and QV* (* stands for whatever),
-1                                                                      ! control the tunes.
1.0864492 *0.3074552   !  FIT variable #12
1
MULTIPOL  QV*
-1
1.0657342 *0.3074552  !  FIT variable #16
1
MULTIPOL  SH*                     ! Chromaticies may be controlled, via these two sextupoles
-1                                                                               ! families.
0.3074552
1
MULTIPOL  SV*
-1
0.3074552
1
MULTIPOL DVCA*                                 ! Make sure all vertical kickers are zero-ed.
-1
0.
1
'MARKER'  scalin_E

'END'
