Translated from mad8 twiss file.
'OBJET'                                                                                          
1000.000000                                                    ! Reference rigidity (kG.cm).
5                  ! An option to generate 11 particles (convenient for MATRIX computation), 
.01 .001 .01 .001 0. .001                        ! with the sampling specified in this line, 
0. 0. 0. 0. 0. 1.                          ! and centered on these values, Y, T, Z, P, s, D. 
'FAISCEAU'                                                     ! Local particle coordinates.
 
'SCALING'                          ! A sort of "power supplies rack", allows tweaking fields
1 7                    ! (a field scaling factor), in 7 different families of magnets, here.
BEND
-1
1.
1
MULTIPOL
-1
1.
1
MULTIPOL  QHA*                        ! These two families, QHA* and QVA* (* is a wild card)
-1                                                                      ! control the tunes.
1.0864492   !  FIT variable #12
1
MULTIPOL  QVA* 
-1
1.0657342   !  FIT variable #16
1
MULTIPOL  SH*                                ! Chromaticies may be controlled, via these two 
-1                                                                     ! sextupole families.
1.
1
MULTIPOL  SV*
-1
1.
1
MULTIPOL DVCA*                                 ! Make sure all vertical kickers are zero-ed.
-1
0.
1

'MARKER'  superA_S
'INCLUDE'
1
LA1.inc[LA1S:LA1E]

'INCLUDE'
1
LA2.inc[LA2S:LA2E]

'INCLUDE'
1
LA3.inc[LA3S:LA3E]

'INCLUDE'
1
LA2.inc[LA2S:LA2E]

'INCLUDE'
1
LA1.inc[LA1S:LA1E]

'INCLUDE'
1
LA6.inc[LA6S:LA6E]

'INCLUDE'
1
LA1.inc[LA1S:LA1E]

'INCLUDE'
1
LA2.inc[LA2S:LA2E]

'MARKER'  superA_E

'FIT'                           ! This FIT and following MATRIX are commented. Uncommented, 
6   nofinal       ! FIT will find the 4D closed orbit and set the (fractional) tune values.
1 30 0 [-1,1]
1 31 0 [-10,10]
1 32 0 [-1,1]
1 33 0 [-10,10]
3 12 0 1. 
3 16 0 1. 
6  1e-15                                              ! 6 constraints (and penalty = 1e-15):
3.1 1 2 #End 0. 1. 0                                              ! Y_0= Y(end of sequence),
3.1 1 3 #End 0. 1. 0                                              ! T_0= T(end of sequence),
3.1 1 4 #End 0. 1. 0                                              ! Z_0= Z(end of sequence),
3.1 1 5 #End 0. 1. 0                                              ! P_0= P(end of sequence).
0.1 7 7 #End 0.788333333333 100. 0                                              ! Qy=4.73/6.
0.1 8 8 #End 0.803333333333 100. 0                                              ! Qy=4.82/6.
'FAISCEAU'                                                     ! Local particle coordinates.
'MATRIX'
1  11
'END'

!  This FIT finds the 4D closed orbit. It repeats at each of the 4 passes that TWISS causes,
!     so ensuring that on-momentum ! and chromatic orbits are closed. The latter is computed
'FIT'                                                      ! so to determine chromaticities.
4                  ! Four variables:  initial positions and angles at OBJET, Y0, T0, Z0, P0.
1 30 0 [-1,1]              
1 31 0 [-10,10]                    
1 32 0 [-1,1]                                           
1 33 0 [-10,10]  
4                                                 ! 4 constraints (default penalty = 1e-10):
3.1 1 2 #End 0. 1. 0                                              ! Y_0= Y(end of sequence),
3.1 1 3 #End 0. 1. 0                                              ! T_0= T(end of sequence),
3.1 1 4 #End 0. 1. 0                                              ! Z_0= Z(end of sequence),
3.1 1 5 #End 0. 1. 0                                              ! P_0= P(end of sequence).
'FAISCEAU'                                                     ! Local particle coordinates.
'TWISS'       ! Compute and transport periodic optical functions along the optical sequence,
2 1. 1.    ! compute chromas. This is performed in 4 successive passes through the sequence.

'SYSTEM'                                                                      ! System call.
1
gnuplot < ./gnuplot_TWISS.gnu         ! Plot on-momentum closed orbit and optical functions. 

'END'


