set term postscript eps enhanced color size 9.3cm,6cm  # "Times-Roman" 12
set output "damping_time_y.eps"
set nogrid
set size 1.0,1.0
set xlabel "Turns [x10^{3}]"
set ylabel "{/Symbol e}_{y} [{/Symbol m}m]"
#unset key

A = 1
B = 0.175
C = 0

f(x)=A*exp(-x/B)

fit f(x) "./ave_sig.txt" u ($1/1000):($1>0?($9**2*0.01**2/9.6995264*1e6):1/0) via A,B

plot "./ave_sig.txt" u ($1/1000):($1>0?($9**2*0.01**2/9.6995264*1e6):1/0) w l lc rgb 'red' lw 1.5 tit '{/Symbol e}_y' ,\
f(x) lc rgb 'blue' lw 1.5 tit 'Fit'

exit
