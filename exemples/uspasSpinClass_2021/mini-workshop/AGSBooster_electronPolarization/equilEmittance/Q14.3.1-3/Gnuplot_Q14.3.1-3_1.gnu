set fit logfile '/dev/null'

fname="zgoubi.fai"

f(x)=ave

set print "ave_sig.txt"

do for [z=10:1000:10] {

  FIT_STDFIT=0
  fit f(x) fname u 38:(($38==z)?($1==1?$9:1/0):1/0) via ave
  d_ave=ave
  d_sig=FIT_STDFIT

  FIT_STDFIT=0
  fit f(x) fname u 38:(($38==z)?($1==1?$10:1/0):1/0) via ave
  x_ave=ave
  x_sig=FIT_STDFIT

  FIT_STDFIT=0
  fit f(x) fname u 38:(($38==z)?($1==1?$11:1/0):1/0) via ave
  xp_ave=ave
  xp_sig=FIT_STDFIT

  FIT_STDFIT=0
  fit f(x) fname u 38:(($38==z)?($1==1?$12:1/0):1/0) via ave
  y_ave=ave
  y_sig=FIT_STDFIT

  FIT_STDFIT=0
  fit f(x) fname u 38:(($38==z)?($1==1?$13:1/0):1/0) via ave
  yp_ave=ave
  yp_sig=FIT_STDFIT

  print z,d_ave,d_sig,x_ave,x_sig,xp_ave,xp_sig,y_ave,y_sig,yp_ave,yp_sig

}

unset print

exit
