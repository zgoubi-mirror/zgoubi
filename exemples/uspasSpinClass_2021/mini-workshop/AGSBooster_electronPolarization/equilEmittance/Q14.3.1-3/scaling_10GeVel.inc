
'MARKER'  SCALING_S

'SCALING'                                                                                         
1 6
BEND
-1
33.3564095089
1
MULTIPOL
-1
33.3564095089* 1.
1
MULTIPOL  QH*                                        ! Quadrupoles set for Qx=4.73, Qy=4.82.
-1
33.3564095089* 1.0864799   !  FIT variable #12
1
MULTIPOL  QV*
-1
33.3564095089* 1.0657626   !  FIT variable #16
1
MULTIPOL  SH*
-1
33.3564095089
1
MULTIPOL  SV*
-1
33.3564095089
1

'MARKER'  SCALING_E

'END'