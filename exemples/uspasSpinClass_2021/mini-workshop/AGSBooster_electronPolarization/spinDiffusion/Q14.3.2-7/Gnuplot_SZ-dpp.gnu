# Gnuplot script for plotting the final vertical spin component as a function of the particle's momentum offset for the two rotator schemes

set term postscript eps enhanced color size 9.3cm,6cm "Times-Roman" 12
set output "final_spin.eps"
set grid
set size 1.0,1.0
set xlabel "{/Symbol D}p/p"
set ylabel "S_{z}"
set xtics 0.02
set ytics 0.002
set key bottom left

plot [][:1.0005] \
"< sort -nk2 zgoubi_unmatched.fai" u 2:22 w lp lc rgb 'red' lw 1.5 title "Scheme 1/unmatched"  ,\
"< sort -nk2 zgoubi_matched.fai"   u 2:22 w lp lc rgb 'blue' lw 1.5 title "Scheme 2/matched"

exit
