# Gnuplot script for plotting the field components

set term postscript eps enhanced color size 9.3cm,6cm # "Times-Roman" 12
set output "spin_rotator_fields.eps"
set grid
set size 1.0,1.0
set xlabel "s [m]"
set ylabel "B (T)"
set xtics 5
set ytics 2
set key bottom left

cm2m = 1e-2
kG2T = 0.1

plot [-1:][-0.1:] \
"./zgoubi.plt" u ($14*cm2m):($23*kG2T) w l lc rgb 'red' lw 1.5 title "B_{x}",\
"./zgoubi.plt" u ($14*cm2m):($24*kG2T) w l lc rgb 'green' lw 1.5 title "B_{y}",\
"./zgoubi.plt" u ($14*cm2m):($25*kG2T) w l lc rgb 'blue' lw 1.5 title "B_{z}"

exit
