# Gnuplot script for plotting the spin components

set term postscript eps enhanced color size 9.3cm,6cm # "Times-Roman" 12
set output "spin_rotator_spin.eps"
set grid
set size 1.0,1.0
set xlabel "s [m]"
set ylabel "S"
set xtics 5
set ytics 0.5
set key bottom left

cm2m = 1e-2

plot [-1:][-1.1:1.1] \
"./zgoubi.plt" u ($14*cm2m):($33) w l lc rgb 'red' lw 1.5 title "S_{x}",\
"./zgoubi.plt" u ($14*cm2m):($34) w l lc rgb 'green' lw 1.5 title "S_{y}",\
"./zgoubi.plt" u ($14*cm2m):($35) w l lc rgb 'blue' lw 1.5 title "S_{z}"

exit
