Spin rotator, version 2
'OBJET'                                                                                                      1
33.3564095089e3  10GeV E_k+M electron.
1
1 1 1 1 1 1
0.E+00   0.E+00   0.E+00   0.E+00   0.00  0.E+00
0.  0.  0. 0. 0.  1.
 
'PARTICUL'                                                                                                   2
POSITRON
 
'SPNTRK'                                                                                                     3
4.1
0. 0. 1.
 
'SCALING'                                                                                                    4
1 1
BEND
-1
1
1
 
'DRIFT'    DRIF                                                                                              5
10.
'SOLENOID'    SOLE                                                                                           6
2  .sole
1000.0  2.0  52.3354354
25  25
1.  cm
1 0. 0. 0.
'DRIFT'    DRIF                                                                                              7
10.
'BEND'     DHA1T                                                                                             8
2  .Bend
100.   0.0E+00   23.08831973
0.00  0.00   0.00
4 .2401  1.8639  -.5572  .3904 0. 0. 0.
0.00  0.00   0.00
4 .2401  1.8639  -.5572  .3904 0. 0. 0.
1.0000E+00 cm  Bend
3 0. 0. 0.
'DRIFT'    DRIF                                                                                              9
10.
'DRIFT'    DRIF                                                                                             10
10.
'BEND'     DHA1T                                                                                            11
2  .Bend
100.   0.0E+00   -23.08831973
0.00  0.00   0.00
4 .2401  1.8639  -.5572  .3904 0. 0. 0.
0.00  0.00   0.00
4 .2401  1.8639  -.5572  .3904 0. 0. 0.
1.0000E+00 cm  Bend
3 0. 0. 0.
'DRIFT'    DRIF                                                                                             12
10.
'SOLENOID'    SOLE                                                                                          13
2  .sole
1000.0  2.0  -52.3354354
25  25
1.  cm
1 0. 0. 0.
'DRIFT'    DRIFEND                                                                                          14
10.
 
'END'

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =      33356.410 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (1)  BUILT  UP  FROM       1 POINTS 



                                 D         Y(cm)       T(mrd)      Z(cm)       P(mrd)      X(cm)

                     NUMBER         1           1           1           1           1           0


                    SAMPLING      0.0000      0.00        0.00        0.00        0.00        0.00


************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 1

     Particle  properties :
     POSITRON
                     Mass          =   0.510999        MeV/c2
                     Charge        =   1.602176E-19    C     
                     G  factor     =   1.159652E-03          
                     COM life-time =   1.000000E+99    s     

              Reference  data :
                    mag. rigidity (kG.cm)   :   33356.410      =p/q, such that dev.=B*L/rigidity
                    mass (MeV/c2)           :  0.51099895    
                    momentum (MeV/c)        :   10000.000    
                    energy, total (MeV)     :   10000.000    
                    energy, kinetic (MeV)   :   9999.4890    
                    beta = v/c              :  0.9999999987    
                    gamma                   :   19569.51200    
                    beta*gamma              :   19569.51198    
                    G*gamma                 :   22.69382727    
                    m / G                   :   440.6484587    
                    electric rigidity (MeV) :   9999.999984    =T[eV]*(gamma+1)/gamma, such that dev.=E*L/rigidity
  
 I, AMQ(1,I), AMQ(2,I)/QE, P/Pref, v/c, time, s :
  
     1   5.10998946E-01  1.00000000E+00  1.00000000E+00  9.99999999E-01  0.00000000E+00  0.00000000E+00

************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 1


                Spin  tracking  requested.


                          Particle  mass          =   0.5109989     MeV/c2
                          Gyromagnetic  factor  G =   1.1596522E-03

                          Initial spin conditions type  4 :
                              KSO2=1. Same spin for all particles
                              Particles # 1 to 1 may be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     33356.410     kG*cm
                               beta      =     1.0000000    
                               gamma     =     19569.512    
                               G*gamma   =     22.693827    
                               M / G     =     440.6484587    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        1  PARTICULES :
                               <SX> =     0.000000
                               <SY> =     0.000000
                               <SZ> =     1.000000
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  SCALING                                                                                  IPASS= 1


               PRINT option is OFF.

               Scaling  request  on  1  families  of  optical  elements :
                                                      

     Family number   1
          Element [/label(s) ( 0)] to be scaled :          BEND
               Family not labeled ;  this scaling will apply to all (unlabeled) elements  "BEND      "
               Scaling of fields follows increase of rigidity taken from CAVITE, starting scaling value    1.00000000E+00

************************************************************************************************************************************
      5  Keyword, label(s) :  DRIFT       DRIF                                                                         IPASS= 1


                              Drift,  length =    10.00000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  1.0000000E+01  3.33564E-04
TRAJ #1 SX, SY, SZ, |S| :  1    0.000000E+00   0.000000E+00   1.000000E+00   1.000000E+00

 Cumulative length of optical axis =   0.100000000     m   ;  Time  (for reference rigidity & particle) =   3.335641E-10 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  SOLENOID    SOLE                                                                         IPASS= 1


                OPEN FILE zgoubi.plt                                                                      
                FOR PRINTING TRAJECTORIES


      -----  SOLENOID    : 
                Length  of  element  :    1000.      cm
                Inner radius  RO =   2.000      cm
                B-CNTRL  =   52.34      kG ;   K=B/Brhof =   1.5690E-03 /m ;   theor. angle  BL/(2*Brho) =   0.7845     rad
                Entrance and exit integration extents : 
                       XE =   25.00     cm,   XS =   25.00     cm
                MODL =  1 -> Solenoid model is axial field model   
                Spin X-rotation angle phi=(1+G)*B0.L/Brho =    90.0000.  Strength phi/180 =    50.0000%

                    Field has been * by scaling factor    1.0000000       (Brho_ref =    33356.410    )

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000         1050.000     0.000     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (     1051.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    10.1000000     m ;  Time  (for ref. rigidity & particle) =   3.368997E-08 s 

************************************************************************************************************************************
      7  Keyword, label(s) :  DRIFT       DRIF                                                                         IPASS= 1


                              Drift,  length =    10.00000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  1.0200000E+03  3.40235E-02
TRAJ #1 SX, SY, SZ, |S| :  1    0.000000E+00   1.000000E+00   1.223753E-04   1.000000E+00

 Cumulative length of optical axis =    10.2000000     m   ;  Time  (for reference rigidity & particle) =   3.402354E-08 s 

************************************************************************************************************************************
      8  Keyword, label(s) :  BEND        DHA1T                                                                        IPASS= 1


     zgoubi.plt                                                                      
      already open...

      +++++        BEND  : 

                Length    =   1.000000E+02 cm
                Arc length    =   1.000200E+02 cm
                Deviation    =   3.966636E+00 deg.,    6.923087E-02 rad
                GAP   =   0.000000E+00 cm
                Gradient   =   0.000000E+00 kG/cm
                Grad-prime   =   0.000000E+00 kG/cm^2

                Field  =  2.3088320E+01  kG   (i.e.,   2.3088320E+01 * SCAL)
                Reference curvature radius (Brho/B) =   1.4447309E+03 cm
                Skew  angle  =   0.000000E+00  rad

               Entrance  face  
                DX =      0.000    LAMBDA =      0.000
                Wedge  angle  =  0.000000 RD

               Exit      face  
                DX =      0.173    LAMBDA =      0.000
                Wedge  angle  =  0.000000 RD

  ***  Warning : entrance sharp edge entails vertical wedge focusing approximated with first order kick, FINT values entr/exit :    0.000    

  ***  Warning : exit sharp edge entails vertical wedge focusing approximated with first order kick, FINT values entr/exit :    0.000    

                    Field has been * by scaling factor    1.0000000       (Brho_ref =    33356.410    )

     CHXC - KPOS =  3 :  automatic positioning of element, 
        XCE, YCE, ALE =   0.000000000       0.000000000     -3.4615433077E-02 cm/cm/rad

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000          100.173    -0.000    -0.035     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      101.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  3 :  Automatic  positionning  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle = -3.46154331E-02 rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    11.2001997     m ;  Time  (for ref. rigidity & particle) =   3.735984E-08 s 

************************************************************************************************************************************
      9  Keyword, label(s) :  DRIFT       DRIF                                                                         IPASS= 1


                              Drift,  length =    10.00000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -3.774411E-14 -6.314393E-13  0.000000E+00  0.000000E+00  1.1300200E+03  3.76934E-02
TRAJ #1 SX, SY, SZ, |S| :  1    9.999999E-01  -3.169917E-04   1.223753E-04   1.000000E+00

 Cumulative length of optical axis =    11.3001997     m   ;  Time  (for reference rigidity & particle) =   3.769341E-08 s 

************************************************************************************************************************************
     10  Keyword, label(s) :  DRIFT       DRIF                                                                         IPASS= 1


                              Drift,  length =    10.00000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -4.405851E-14 -6.314393E-13  0.000000E+00  0.000000E+00  1.1400200E+03  3.80270E-02
TRAJ #1 SX, SY, SZ, |S| :  1    9.999999E-01  -3.169917E-04   1.223753E-04   1.000000E+00

 Cumulative length of optical axis =    11.4001997     m   ;  Time  (for reference rigidity & particle) =   3.802697E-08 s 

************************************************************************************************************************************
     11  Keyword, label(s) :  BEND        DHA1T                                                                        IPASS= 1


     zgoubi.plt                                                                      
      already open...

      +++++        BEND  : 

                Length    =   1.000000E+02 cm
                Arc length    =   1.000200E+02 cm
                Deviation    =  -3.966636E+00 deg.,   -6.923087E-02 rad
                GAP   =   0.000000E+00 cm
                Gradient   =   0.000000E+00 kG/cm
                Grad-prime   =   0.000000E+00 kG/cm^2

                Field  = -2.3088320E+01  kG   (i.e.,  -2.3088320E+01 * SCAL)
                Reference curvature radius (Brho/B) =  -1.4447309E+03 cm
                Skew  angle  =   0.000000E+00  rad

               Entrance  face  
                DX =      0.000    LAMBDA =      0.000
                Wedge  angle  =  0.000000 RD

               Exit      face  
                DX =      0.173    LAMBDA =      0.000
                Wedge  angle  =  0.000000 RD

  ***  Warning : entrance sharp edge entails vertical wedge focusing approximated with first order kick, FINT values entr/exit :    0.000    

  ***  Warning : exit sharp edge entails vertical wedge focusing approximated with first order kick, FINT values entr/exit :    0.000    

                    Field has been * by scaling factor    1.0000000       (Brho_ref =    33356.410    )

     CHXC - KPOS =  3 :  automatic positioning of element, 
        XCE, YCE, ALE =  -0.000000000       0.000000000      3.4615433077E-02 cm/cm/rad

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000          100.173    -0.000     0.035     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      101.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  3 :  Automatic  positionning  of  element.
          X =  -0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =  3.46154331E-02 rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    12.4003995     m ;  Time  (for ref. rigidity & particle) =   4.136328E-08 s 

************************************************************************************************************************************
     12  Keyword, label(s) :  DRIFT       DRIF                                                                         IPASS= 1


                              Drift,  length =    10.00000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -7.444913E-14  2.081668E-14  0.000000E+00  0.000000E+00  1.2500399E+03  4.16968E-02
TRAJ #1 SX, SY, SZ, |S| :  1   -6.453171E-16   1.000000E+00   1.223753E-04   1.000000E+00

 Cumulative length of optical axis =    12.5003995     m   ;  Time  (for reference rigidity & particle) =   4.169684E-08 s 

************************************************************************************************************************************
     13  Keyword, label(s) :  SOLENOID    SOLE                                                                         IPASS= 1


     zgoubi.plt                                                                      
      already open...

      -----  SOLENOID    : 
                Length  of  element  :    1000.      cm
                Inner radius  RO =   2.000      cm
                B-CNTRL  =  -52.34      kG ;   K=B/Brhof =  -1.5690E-03 /m ;   theor. angle  BL/(2*Brho) =  -0.7845     rad
                Entrance and exit integration extents : 
                       XE =   25.00     cm,   XS =   25.00     cm
                MODL =  1 -> Solenoid model is axial field model   
                Spin X-rotation angle phi=(1+G)*B0.L/Brho =   -90.0000.  Strength phi/180 =   -50.0000%

                    Field has been * by scaling factor    1.0000000       (Brho_ref =    33356.410    )

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000         1050.000    -0.000     0.000    -0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (     1051.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    22.5003995     m ;  Time  (for ref. rigidity & particle) =   7.505325E-08 s 

************************************************************************************************************************************
     14  Keyword, label(s) :  DRIFT       DRIFEND                                                                      IPASS= 1


                              Drift,  length =    10.00000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -2.374420E-14  3.955320E-14 -2.356167E-14  3.951450E-14  2.2600399E+03  7.53868E-02
TRAJ #1 SX, SY, SZ, |S| :  1   -3.162947E-15   9.172532E-16   1.000000E+00   1.000000E+00

 Cumulative length of optical axis =    22.6003995     m   ;  Time  (for reference rigidity & particle) =   7.538682E-08 s 

************************************************************************************************************************************
     15  Keyword, label(s) :  END                                                                                      IPASS= 1


                             1 particles have been launched
                     Made  it  to  the  end :      1

************************************************************************************************************************************
 Pgm zgoubi : Execution ended normally, upon keyword END or FIN
   
 File in:   Q14.3.2-6_tab14.27.dat
 File out:  zgoubi.res

  Zgoubi, author's Revision: 1584.
  Job  started  on  22-11-2021,  at  09:48:27 
  JOB  ENDED  ON    22-11-2021,  AT  09:48:27 

   CPU time, total :     9.6703999999999984E-002
