# Gnuplot script for plotting the polarization

set term postscript eps enhanced color size 9.3cm,6cm "Times-Roman" 12
set output "pol_align_all.eps"
unset grid
set size 1.0,1.0
set xlabel "Turns [x10^{3}]"
set ylabel "P_{z}"
#set xtics 0.2
#set ytics 0.0005

#unset key
set key l c
set key maxcol 2

A1 = 1; A5 = 1
B1 = 1; B5 = 1

f1(x)=A1*exp(-x/B1)
f5(x)=A5*exp(-x/B5)

fit f1(x) "./ave_pol_align_1mm.txt" u ($1/1000):($6) via A1,B1
fit f5(x) "./ave_pol_align_5mm.txt" u ($1/1000):($6) via A5,B5

plot [] [0.:1.] \
"./ave_pol_align_1mm.txt" u ($1/1000):($6) w l lw 2. tit "1mm" ,\
f1(x) lc rgb 'blue' lw .6 notit ,\
"./ave_pol_align_5mm.txt" u ($1/1000):($6) w l lw 2. tit "5mm" ,\
f5(x) lc rgb 'blue' lw .6 notit

exit
