# Gnuplot script for plotting the polarization

set term postscript eps enhanced color size 9.3cm,6cm "Times-Roman" 12
set output "pol_align.eps"
unset grid
set size 1.0,1.0
set xlabel "Turns [x10^{3}]"
set ylabel "P_{z}"
#set xtics 0.2
#set ytics 0.0005

#unset key
set key l c

A = 1
B = 1

f(x)=A*exp(-x/B)

fit f(x) "./ave_pol_align.txt" u ($1/1000):($6) via A,B

plot [] [0.:1.]  "./ave_pol_align.txt" \
u ($1/1000):($6) w l lc rgb 'red' lw 1.5 tit "track" ,\
f(x) lc rgb 'blue' lw 1.5 tit "Fit"

exit
