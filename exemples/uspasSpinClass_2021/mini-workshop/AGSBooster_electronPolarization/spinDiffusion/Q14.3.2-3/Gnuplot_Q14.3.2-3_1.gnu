Gnuplot script for calculating the polarization

set fit logfile '/dev/null'

fname="zgoubi.fai"

f(x)=ave

set print "ave_pol_align.txt"

do for [z=1:1:1] {

  FIT_STDFIT=0
  fit f(x) fname u 38:($38==z?($1==1?$20:1/0):1/0) via ave
  px_ave=ave
  px_sig=FIT_STDFIT

  FIT_STDFIT=0
  fit f(x) fname u 38:($38==z?($1==1?$21:1/0):1/0) via ave
  py_ave=ave
  py_sig=FIT_STDFIT

  FIT_STDFIT=0
  fit f(x) fname u 38:($38==z?($1==1?$22:1/0):1/0) via ave
  pz_ave=ave
  pz_sig=FIT_STDFIT

  print z,px_ave,px_sig,py_ave,py_sig,pz_ave,pz_sig

}

do for [z=10:1000:10] {

  FIT_STDFIT=0
  fit f(x) fname u 38:($38==z?($1==1?$20:1/0):1/0) via ave
  px_ave=ave
  px_sig=FIT_STDFIT

  FIT_STDFIT=0
  fit f(x) fname u 38:($38==z?($1==1?$21:1/0):1/0) via ave
  py_ave=ave
  py_sig=FIT_STDFIT

  FIT_STDFIT=0
  fit f(x) fname u 38:($38==z?($1==1?$22:1/0):1/0) via ave
  pz_ave=ave
  pz_sig=FIT_STDFIT

  print z,px_ave,px_sig,py_ave,py_sig,pz_ave,pz_sig

}

unset print

exit
