FFAG triplet. 150MeV machine. CPU time, analyt. : 11:01:35->
 'OBJET'                                                                             1                        1
1839.090113 150MeV
5
.001 .01 .001 .01 .001 .0001
445.23392   0.   0.  0. 0.   0.273042677097  'o'    ! 12MeV  Brho=502.1500877
 
 'PARTICUL'                                                                                                   2
938.27231  1.60217733D-19 0. 0. 0.
 
 'MARKER'  @S_Cell                                                                                            3
 'FFAG'                                                                                                       4
0
3   30.     540.                        NMAG, AT=tetaF+2tetaD+2Atan(XFF/R0), R0
6.465  0.  -1.21744691E+01  7.6             mag 1 : ACNT, dum, B0, K
6.3  03.                                EFB 1 : lambda, gap const/var=0/.ne.0
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
1.715 0.    1.E6  -1.E6  1.E6  1.E6
6.3  03.                                EFB 2
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-1.715  0.    1.E6  -1.E6  1.E6  1.E6
0. -1                                   EFB 3 : inhibited by iop=0
0  0.      0.      0.      0.      0. 0.  0.
0.  0.   0.    0.    0. 0.
15.   0.   1.69055873E+01  7.6                  mag 2 : ACNT, dum, B0, K,dummies
6.3  03.                                EFB 1
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
5.12   0.    1.E6  -1.E6  1.E6  1.E6
6.3  03.                                EFB 2
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-5.12   0.    1.E6  -1.E6  1.E6  1.E6
0. -1                                   EFB 3
0  0.      0.      0.      0.      0. 0.  0.
0.  0.   0.    0.    0. 0.
23.535  0.  -1.21744691E+01  7.6             mag 3 : ACNT, dum, B0, K
6.3  03.                                EFB 1
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
1.715 0.    1.E6  -1.E6  1.E6  1.E6
6.3  03.                                EFB 2
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-1.715  0.    1.E6  -1.E6  1.E6  1.E6
0. -1                                   EFB 3
0  0.      0.      0.      0.      0. 0.  0.
0.  0.   0.    0.    0. 0.
0 2  125.             ! KIRD anal/num (=0/2,25,4), resol(mesh=step/resol).
.25                    ! Integration step size (cm) (comparing
2   0.  0. 0. 0.       ! anal/num yield best num prec for resol=1e3*step).
 'MARKER'  @E_Cell                                                                                            5
 
 'FIT'   ! This FIT makes sure orbit for MATRIX is periodic                                                   6
1  noFinal
1 30 0  2.
2  1e-9 99            ! A 1e-9 penalty controls the accuracy
3.1 1 2 #End 0. 1. 0  ! of the convergence to periodic coordinates.
3.1 1 3 #End 0. 1. 0  ! 3.1 is the constraint for periodicity.
! Zero angle results from radial sector.
 
 'MARKER' afterFIT                                                                                            7
 
 'MATRIX'                                                                                                     8
1 11
 
 'END'                                                                                                        9

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET       1                                                                            IPASS= 1

                          MAGNETIC  RIGIDITY =       1839.090 kG*cm

                                         CALCUL  DES  TRAJECTOIRES

                              OBJET  (5)  FORME  DE     11 POINTS 



                                Y (cm)         T (mrd)       Z (cm)        P (mrd)       S (cm)        dp/p 
               Sampling :          0.10E-02      0.10E-01      0.10E-02      0.10E-01      0.10E-02      0.1000E-03
  Reference trajectry #      1 :   0.45E+03       0.0           0.0           0.0           0.0          0.2730    

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 1

     Particle  properties :
     Particle name unknown.
                     Mass          =    938.272        MeV/c2
                     Charge        =   1.602177E-19    C     

              Reference  data :
                    mag. rigidity (kG.cm)   :   1839.0901      =p/q, such that dev.=B*L/rigidity
                    mass (MeV/c2)           :   938.27231    
                    momentum (MeV/c)        :   551.34564    
                    energy, total (MeV)     :   1088.2725    
                    energy, kinetic (MeV)   :   150.00015    
                    beta = v/c              :  0.5066246350    
                    gamma                   :   1.159868456    
                    beta*gamma              :  0.5876179332    
                    electric rigidity (MeV) :   279.3254283    =T[eV]*(gamma+1)/gamma, such that dev.=E*L/rigidity
  
 I, AMQ(1,I), AMQ(2,I)/QE, P/Pref, v/c, time, s :
  
     1   9.38272310E+02  1.00000053E+00  2.73042677E-01  1.58418681E-01  0.00000000E+00  0.00000000E+00
     2   9.38272310E+02  1.00000053E+00  2.73042677E-01  1.58418681E-01  0.00000000E+00  0.00000000E+00
     3   9.38272310E+02  1.00000053E+00  2.73042677E-01  1.58418681E-01  0.00000000E+00  0.00000000E+00
     4   9.38272310E+02  1.00000053E+00  2.73042677E-01  1.58418681E-01  0.00000000E+00  0.00000000E+00
     5   9.38272310E+02  1.00000053E+00  2.73042677E-01  1.58418681E-01  0.00000000E+00  0.00000000E+00
     6   9.38272310E+02  1.00000053E+00  2.73042677E-01  1.58418681E-01  0.00000000E+00  0.00000000E+00
     7   9.38272310E+02  1.00000053E+00  2.73042677E-01  1.58418681E-01  0.00000000E+00  0.00000000E+00
     8   9.38272310E+02  1.00000053E+00  2.73042677E-01  1.58418681E-01  0.00000000E+00  0.00000000E+00
     9   9.38272310E+02  1.00000053E+00  2.73042677E-01  1.58418681E-01  0.00000000E+00  0.00000000E+00
    10   9.38272310E+02  1.00000053E+00  2.73142677E-01  1.58475244E-01  0.00000000E+00  0.00000000E+00
    11   9.38272310E+02  1.00000053E+00  2.72942677E-01  1.58362117E-01  0.00000000E+00  0.00000000E+00

************************************************************************************************************************************
      3  Keyword, label(s) :  MARKER      @S_Cell                                                                      IPASS= 1


************************************************************************************************************************************
      4  Keyword, label(s) :  FFAG                                                                                     IPASS= 1

                    FFAG  N-tuple,  number  of  dipoles,  N :  3

            Total angular extent of the magnet :  30.0000 Degres
            Reference geometrical radius R0  :     540.0000 cm

     Dipole # 1
            Positionning  angle ACENT :    6.46500     degrees
            Positionning  wrt.  R0  :    0.00000     cm
            B0 =  -12.174469     kGauss,       K =   7.6000    

     Entrance  EFB
          Fringe  field  :  gap at R0 is   6.30 cm,    type is :  g_0(r0/r)^K
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  is    0.000     cm

          OMEGA =   1.72 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

         Exit  EFB
          Fringe  field  :  gap at R0 is   6.30 cm,    type is :  g_0(r0/r)^K
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  is    0.000     cm

          OMEGA =  -1.72 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

         Lateral face :  unused

     Dipole # 2
            Positionning  angle ACENT :    15.0000     degrees
            Positionning  wrt.  R0  :    0.00000     cm
            B0 =   16.905587     kGauss,       K =   7.6000    

     Entrance  EFB
          Fringe  field  :  gap at R0 is   6.30 cm,    type is :  g_0(r0/r)^K
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  is    0.000     cm

          OMEGA =   5.12 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

         Exit  EFB
          Fringe  field  :  gap at R0 is   6.30 cm,    type is :  g_0(r0/r)^K
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  is    0.000     cm

          OMEGA =  -5.12 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

         Lateral face :  unused

     Dipole # 3
            Positionning  angle ACENT :    23.5350     degrees
            Positionning  wrt.  R0  :    0.00000     cm
            B0 =  -12.174469     kGauss,       K =   7.6000    

     Entrance  EFB
          Fringe  field  :  gap at R0 is   6.30 cm,    type is :  g_0(r0/r)^K
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  is    0.000     cm

          OMEGA =   1.72 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

         Exit  EFB
          Fringe  field  :  gap at R0 is   6.30 cm,    type is :  g_0(r0/r)^K
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  is    0.000     cm

          OMEGA =  -1.72 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

         Lateral face :  unused


     KIRD, resol, IDB, IDZ :  0  2  2  3

     Field & deriv. calculation : analytic     
                    Derivatives computed to order 2

                    Integration step :  0.2500     cm   (i.e.,   4.6296E-04 rad  at mean radius RM =    540.0    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.2730   445.234     0.000     0.000     0.000            0.524   445.234     0.000     0.000     0.000            1
  A    1  0.2730   445.235     0.000     0.000     0.000            0.524   445.234    -0.000     0.000     0.000            2
  A    1  0.2730   445.233     0.000     0.000     0.000            0.524   445.234     0.000     0.000     0.000            3
  A    1  0.2730   445.234     0.010     0.000     0.000            0.524   445.235    -0.000     0.000     0.000            4
  A    1  0.2730   445.234    -0.010     0.000     0.000            0.524   445.233     0.000     0.000     0.000            5
  A    1  0.2730   445.234     0.000     0.001     0.000            0.524   445.234     0.000     0.001    -0.000            6
  A    1  0.2730   445.234     0.000    -0.001     0.000            0.524   445.234     0.000    -0.001     0.000            7
  A    1  0.2730   445.234     0.000     0.000     0.010            0.524   445.234     0.000     0.003     0.000            8
  A    1  0.2730   445.234     0.000     0.000    -0.010            0.524   445.234     0.000    -0.003    -0.000            9
  A    1  0.2731   445.234     0.000     0.000     0.000            0.524   445.260     0.000     0.000     0.000           10
  A    1  0.2729   445.234     0.000     0.000     0.000            0.524   445.208    -0.000     0.000     0.000           11

 Cumulative length of optical axis =    2.93400800     m ;  Time  (for ref. rigidity & particle) =   1.931765E-08 s 

************************************************************************************************************************************
      5  Keyword, label(s) :  MARKER      @E_Cell                                                                      IPASS= 1


************************************************************************************************************************************
      6  Keyword, label(s) :  FIT                                                                                      IPASS= 1

     Pgm main. FITING=.T., FIT procedure launched.

           variable #            1       IR =            1 ,   ok.
           variable #            1       IP =           30 ,   ok.
           constraint #            1       IR =            5 ,   ok.
           constraint #            1       I  =            1 ,   ok.
           constraint #            2       IR =            5 ,   ok.
           constraint #            2       I  =            1 ,   ok.

                    FIT  variables  and  constraints  in  good  order,  FIT  will proceed. 

 STATUS OF VARIABLES  (Iteration #     0 /     99 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   1   1    30   -445.        445.       445.23392      1.336E+03   5.94      OBJET      1                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-09)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     5    0.000000E+00    1.000E+00    1.528820E-06    1.21E-02 MARKER     @E_Cell              -                    0
  3   1   3     5    0.000000E+00    1.000E+00    1.381327E-05    9.88E-01 MARKER     @E_Cell              -                    0
 Fit reached penalty value   1.9314E-10

************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET       1                                                                            IPASS= 1

                          MAGNETIC  RIGIDITY =       1839.090 kG*cm

                                         CALCUL  DES  TRAJECTOIRES

                              OBJET  (5)  FORME  DE     11 POINTS 



                                Y (cm)         T (mrd)       Z (cm)        P (mrd)       S (cm)        dp/p 
               Sampling :          0.10E-02      0.10E-01      0.10E-02      0.10E-01      0.10E-02      0.1000E-03
  Reference trajectry #      1 :   0.45E+03       0.0           0.0           0.0           0.0          0.2730    

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 1


************************************************************************************************************************************
      3  Keyword, label(s) :  MARKER      @S_Cell                                                                      IPASS= 1


************************************************************************************************************************************
      4  Keyword, label(s) :  FFAG                                                                                     IPASS= 1

                    FFAG  N-tuple,  number  of  dipoles,  N :  3

            Total angular extent of the magnet :  30.0000 Degres
            Reference geometrical radius R0  :     540.0000 cm

     Dipole # 1
            Positionning  angle ACENT :    6.46500     degrees
            Positionning  wrt.  R0  :    0.00000     cm
            B0 =  -12.174469     kGauss,       K =   7.6000    

     Entrance  EFB
          Fringe  field  :  gap at R0 is   6.30 cm,    type is :  g_0(r0/r)^K
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  is    0.000     cm

          OMEGA =   1.72 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

         Exit  EFB
          Fringe  field  :  gap at R0 is   6.30 cm,    type is :  g_0(r0/r)^K
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  is    0.000     cm

          OMEGA =  -1.72 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

         Lateral face :  unused

     Dipole # 2
            Positionning  angle ACENT :    15.0000     degrees
            Positionning  wrt.  R0  :    0.00000     cm
            B0 =   16.905587     kGauss,       K =   7.6000    

     Entrance  EFB
          Fringe  field  :  gap at R0 is   6.30 cm,    type is :  g_0(r0/r)^K
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  is    0.000     cm

          OMEGA =   5.12 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

         Exit  EFB
          Fringe  field  :  gap at R0 is   6.30 cm,    type is :  g_0(r0/r)^K
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  is    0.000     cm

          OMEGA =  -5.12 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

         Lateral face :  unused

     Dipole # 3
            Positionning  angle ACENT :    23.5350     degrees
            Positionning  wrt.  R0  :    0.00000     cm
            B0 =  -12.174469     kGauss,       K =   7.6000    

     Entrance  EFB
          Fringe  field  :  gap at R0 is   6.30 cm,    type is :  g_0(r0/r)^K
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  is    0.000     cm

          OMEGA =   1.72 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

         Exit  EFB
          Fringe  field  :  gap at R0 is   6.30 cm,    type is :  g_0(r0/r)^K
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  is    0.000     cm

          OMEGA =  -1.72 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

         Lateral face :  unused


     KIRD, resol, IDB, IDZ :  0  2  2  3

     Field & deriv. calculation : analytic     
                    Derivatives computed to order 2

                    Integration step :  0.2500     cm   (i.e.,   4.6296E-04 rad  at mean radius RM =    540.0    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.2730   445.234     0.000     0.000     0.000            0.524   445.234     0.000     0.000     0.000            1
  A    1  0.2730   445.235     0.000     0.000     0.000            0.524   445.234    -0.000     0.000     0.000            2
  A    1  0.2730   445.233     0.000     0.000     0.000            0.524   445.234     0.000     0.000     0.000            3
  A    1  0.2730   445.234     0.010     0.000     0.000            0.524   445.235    -0.000     0.000     0.000            4
  A    1  0.2730   445.234    -0.010     0.000     0.000            0.524   445.233     0.000     0.000     0.000            5
  A    1  0.2730   445.234     0.000     0.001     0.000            0.524   445.234     0.000     0.001    -0.000            6
  A    1  0.2730   445.234     0.000    -0.001     0.000            0.524   445.234     0.000    -0.001     0.000            7
  A    1  0.2730   445.234     0.000     0.000     0.010            0.524   445.234     0.000     0.003     0.000            8
  A    1  0.2730   445.234     0.000     0.000    -0.010            0.524   445.234     0.000    -0.003    -0.000            9
  A    1  0.2731   445.234     0.000     0.000     0.000            0.524   445.260     0.000     0.000     0.000           10
  A    1  0.2729   445.234     0.000     0.000     0.000            0.524   445.208    -0.000     0.000     0.000           11

 Cumulative length of optical axis =    2.93400800     m ;  Time  (for ref. rigidity & particle) =   1.931765E-08 s 

************************************************************************************************************************************
      5  Keyword, label(s) :  MARKER      @E_Cell                                                                      IPASS= 1


************************************************************************************************************************************

      6   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      7  Keyword, label(s) :  MARKER      afterFIT                                                                     IPASS= 1


************************************************************************************************************************************
      8  Keyword, label(s) :  MATRIX                                                                                   IPASS= 1


  Reference, before change of frame (part #     1)  : 
  -7.26957323E-01   4.45233922E+02   1.38132658E-05   0.00000000E+00   0.00000000E+00   2.41165108E+02   5.07793780E-02

           Frame for MATRIX calculation moved by :
            XC =    0.000 cm , YC =  445.234 cm ,   A =  0.00000 deg  ( = 0.000000 rad )


  Reference, after change of frame (part #     1)  : 
  -7.26957323E-01   0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00   2.41165108E+02   5.07793780E-02

  Reference particle (#     1), path length :   241.16511     cm  relative momentum :   0.273043    


                  TRANSFER  MATRIX  ORDRE  1  (MKSA units)

         -0.392694        0.675612         0.00000         0.00000         0.00000        0.716634    
          -1.25189       -0.392694         0.00000         0.00000         0.00000        0.644181    
           0.00000         0.00000        0.814072         2.58596         0.00000         0.00000    
           0.00000         0.00000       -0.130430        0.814071         0.00000         0.00000    
          0.644181        0.716634         0.00000         0.00000         1.00000       -5.143935E-02
           0.00000         0.00000         0.00000         0.00000         0.00000         1.00000    

          DetY-1 =       0.0000000056,    DetZ-1 =       0.0000000449

          R12=0 at    1.720     m,        R34=0 at   -3.177     m

      First order symplectic conditions (expected values = 0) :
         5.5654E-09    4.4900E-08     0.000         0.000         0.000         0.000    


                TWISS  parameters,  periodicity  of   1  is  assumed 
                                   - UNCOUPLED -

       Beam  matrix  (beta/-alpha/-alpha/gamma) and  periodic  dispersion  (MKSA units)

           0.734624    -0.000000     0.000000     0.000000     0.000000     0.514567
          -0.000000     1.361240     0.000000     0.000000     0.000000    -0.000000
           0.000000     0.000000     4.452678    -0.000000     0.000000     0.000000
           0.000000     0.000000    -0.000000     0.224584     0.000000     0.000000
           0.000000     0.000000     0.000000     0.000000     0.000000     0.000000
           0.000000     0.000000     0.000000     0.000000     0.000000     0.000000

                                   Betatron  tunes

                    NU_Y =  0.31422834         NU_Z =  0.98623183E-01

************************************************************************************************************************************
      9  Keyword, label(s) :  END                                                                                      IPASS= 1


                            11 particles have been launched
                     Made  it  to  the  end :     11

************************************************************************************************************************************
 Pgm zgoubi : Execution ended normally, upon keyword END or FIN
   
            ZGOUBI RUN COMPLETED. 

  Zgoubi, author's dvlpmnt version.
  Job  started  on  22-11-2019,  at  03:15:31 
  JOB  ENDED  ON    22-11-2019,  AT  03:15:31 

   CPU time, total :    0.14276200000000000     

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
