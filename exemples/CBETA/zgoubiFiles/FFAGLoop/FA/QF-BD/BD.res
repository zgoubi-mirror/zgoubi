BD field map
 'OBJET'                                                                                                      1
140.086551
1
5   1  1  1  1  1
1.  0. 0. 0. 0. 0.
0. 0. 0. 0. 0. 1.e6
 
 'TOSCA' FA.PIP04\FA.QUA04\1                                                                                  2
0  2
-9.76000000E-04  1.00000000E+00  1.00000000E+00  1.00000000E+00
HEADER_8  ZroBXY
801 83 1  15.1  1.0
./BD_v6_x+-4p1_y+-1p3_x+-40_stp1mm_integral_2D.table
0 0 0 0
2
.2
2   0.00000000E+00 -1.90000000E-02  0.00000000E+00
 
 'SYSTEM'                                                                                                     3
4
gnuplot < ./gnuplot_Zplt_XY.cmd
cp gnuplot_zgoubi.plt_XY.eps gnuplot_1.eps
gnuplot < ./gnuplot_Zplt_XB.cmd
cp gnuplot_zgoubi.plt_XB.eps gnuplot_2.eps &
 
 'END'                                                                                                        4

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =        140.087 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (1)  BUILT  UP  FROM       5 POINTS 



                                 D         Y(cm)       T(mrd)      Z(cm)       P(mrd)      X(cm)

                     NUMBER         1           5           1           1           1           0


                    SAMPLING      0.0000      1.00        0.00        0.00        0.00        0.00


************************************************************************************************************************************
      2  Keyword, label(s) :  TOSCA       FA.PIP04\FA.QUA04\1                                                          IPASS= 1


                OPEN FILE zgoubi.plt                                                                      
                FOR PRINTING TRAJECTORIES


     Status of MOD.MOD2 is 15.1 ; NDIM = 2 ; number of field data files used is   1.
     Field map set stored in field array HC(IMAP = 1)
     3-D map. MOD=15.  Single field map, with field coefficient value : 
                 1.000000E+00

     ZroBXY OPTION :  found in map title. BX and BY values in Z=0 plane forced to zero.

           New field map(s) opened, cartesian mesh (MOD.le.19) ; 
           name(s) of map data file(s) : 

          './BD_v6_x+-4p1_y+-1p3_x+-40_stp1mm_integral_2D.table'

   ----
   Map file number    1 ( of 1 ) (unit # 24 ) :
     ./BD_v6_x+-4p1_y+-1p3_x+-40_stp1mm_integral_2D.table field map,
     FORMAT type : regular,
     field multiplication factor :  1.00000000E+00.

 HEADER  (8 lines) : 
       801 27 83 2                                                                                                           
       1 X [CM]                                                                                                              
       2 Y [CM]                                                                                                              
       3 Z [CM]                                                                                                              
       4 BX [1]                                                                                                              
       5 BY [1]                                                                                                              
       6 BZ [1]                                                                                                              
       0                                                                                                                     
R_min (cm), DR (cm), DTTA (deg), DZ (cm) :    8.01000000E+02   2.70000000E+01   8.30000000E+01   2.00000000E+00

  Sbr fmapw/fmapr3 : completed job of reading map.
     Field map scaled by factor  a(1) =   1.000000E+00

     Min/max fields found in map (series) read   :   8.132666E+06  /  -2.018294E+06
       @  X,  Y, Z :   0.00      -4.10       0.00                  /  -1.20       4.10       0.00    
     Given normalisation coeffs on field, x, y, z :  -9.760000E-04   1.000000E+00   1.000000E+00   1.000000E+00
     this yields min/max normalised fields (kG) : -7.937482E+03                      1.969855E+03
       @  X (cm),  Y (cm), Z (cm) :   0.00      -4.10       0.00   /  -1.20       4.10       0.00    

     Length of element,  XL =  8.000000E+01 cm 
                                               from  XI =  -4.000000E+01 cm 
                                               to    XF =   4.000000E+01 cm 

     Nbr of nodes in X = 801;  nbr of nodes in Y =   83
     X-size of mesh =  1.000000E-01 cm ; Y-size =  1.000000E-01 cm

                     OPTION  DE  CALCUL  : 2
                     LISSAGE  A   9  POINTS 

                    Integration step :  0.2000     cm

  A    1********     0.000     0.000     0.000     0.000           40.000     0.019    -0.000     0.000     0.000            1
  A    1********     1.000     0.000     0.000     0.000           40.000     1.019    -0.000     0.000     0.000            2
  A    1********    -1.000     0.000     0.000     0.000           40.000    -0.981    -0.000     0.000     0.000            3
  A    1********     2.000     0.000     0.000     0.000           40.000     2.019    -0.000     0.000     0.000            4
  A    1********    -2.000     0.000     0.000     0.000           40.000    -1.981    -0.000     0.000     0.000            5


                CONDITIONS  DE  MAXWELL  (     2005.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



     KPOS =  2.  Element  is  mis-aligned  wrt.  the  optical  axis.
          X =   0.000     CM   Y = -1.9000E-02 cm,  tilt  angle =   0.0000000     RAD


 Cumulative length of optical axis =   0.800000000     m ;  Time  (for ref. rigidity & particle) =   5.967818E-08 s 

************************************************************************************************************************************
      3  Keyword, label(s) :  SYSTEM                                                                                   IPASS= 1

     Number  of  commands :   4,  as  follows : 

 gnuplot < ./gnuplot_Zplt_XY.cmd
 cp gnuplot_zgoubi.plt_XY.eps gnuplot_1.eps
 gnuplot < ./gnuplot_Zplt_XB.cmd
 cp gnuplot_zgoubi.plt_XB.eps gnuplot_2.eps &

************************************************************************************************************************************
      4  Keyword, label(s) :  END                                                                                      IPASS= 1


                             5 particles have been launched
                     Made  it  to  the  end :      5

************************************************************************************************************************************
 Pgm zgoubi : Execution ended normally, upon keyword END or FIN
   
            ZGOUBI RUN COMPLETED. 

  Zgoubi, author's dvlpmnt version.
  Job  started  on  22-08-2019,  at  16:29:27 
  JOB  ENDED  ON    22-08-2019,  AT  16:29:33 

   CPU time, total :    0.24731200000000000     
