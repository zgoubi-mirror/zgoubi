
set key maxcol 2
set key b l 

set xlabel "z /cm"
set ylabel "B_y /G"

plot  [-.5:46] \
 "3cell_mod_x=+-4p1_y+1p3_z=0+45cm_step=1mm_2D.table" u ($3):($1==+2. && $2==0 ? $5 : 1/0) w lp ps .2 pt 14   tit "x=+2cm" ,\
 "3cell_mod_x=+-4p1_y+1p3_z=0+45cm_step=1mm_2D.table" u ($3):($1==+1. && $2==0 ? $5 : 1/0) w lp ps .2 pt 14   tit "x=+1cm" ,\
 "3cell_mod_x=+-4p1_y+1p3_z=0+45cm_step=1mm_2D.table" u ($3):($1==0. && $2==0 ? $5 : 1/0) w lp ps .2 pt 14   tit "x=0cm" ,\
 "3cell_mod_x=+-4p1_y+1p3_z=0+45cm_step=1mm_2D.table" u ($3):($1==-1. && $2==0 ? $5 : 1/0) w lp ps .2 pt 14   tit "x=-1cm" ,\
 "3cell_mod_x=+-4p1_y+1p3_z=0+45cm_step=1mm_2D.table" u ($3):($1==-2. && $2==0 ? $5 : 1/0) w lp ps .2 pt 14   tit "x=-2cm" 


set terminal postscript eps blacktext color enh size 8.3cm,4cm "Times-Roman" 12 
 set output "gnuplot_mapField_3Cell.eps" 
 replot 
 set terminal X11 
 unset output 

pause 2
exit
