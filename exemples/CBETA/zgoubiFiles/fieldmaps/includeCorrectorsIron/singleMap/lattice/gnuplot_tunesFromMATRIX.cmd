
# set title "Time of flight, orbits at mid-drift \n From field maps"    font "roman,14"   # offset 0,+.7    

 set xlabel "{/Symbol g }m_0 [MeV]"        font "roman,16"   # offset +4,-.5 rotate by +20  
 set x2label "a{/Symbol .g}"        font "roman,16"   # offset +4,-.5 rotate by +20  
 set ylabel "dL/L_{ref}"             font "roman,16"   #offset -0,-1 rotate by -20 
 set y2label "x_{co}/mm, x'_{co}/10mrad"             font "roman,16"   #offset -0,-1 rotate by -20 

 set xtics  font "roman,12" nomirror
 set x2tics  font "roman,12" nomirror
 set ytics  font "roman,12" nomirror      #offset 0,-.6
 set y2tics  font "roman,12" nomirror      #offset 0,-.6

 set key maxcol 1
set key top l  font "roman, 12"  samplen 1  

#set grid

a =  1.15965218E-03
am = 0.510998920 
c = 2.99792458e8
pi = 3.1415926

#fit_limit=1e-20
#q0 = 0.25
#q1 =1.
#q2 =1.
#q3 =1.
# Qy(x) = q0 + q1/x + q2/x**2 + q3/x**3
# fit [] Qy(x)             "tunesFromMatrix_40-167.out" u (($12+am)/am*a):($4) via q0, q1, q2, q3
#print "Qy(44.8339) : ", Qy(44.8339)*138*6
#exit

set xrange  [39     :170]
set x2range [39/am*a:170 /am*a]
set y2range [-17:35]

Ekref = 114.-am
Lref =   44.366178     # 4.437327E+01 # cm
Tref = 1.47991126E-03  # 1.48014766E-03 # mu_s

 plot \
           "tunesFromMatrix_40-167.out" u ($12+am):($3>0 && $4>0 ? ($11-Lref)/Lref : 1/0) w lp ps .2 lt 8  lc rgb "red" tit "1-map, dL/L_"  ,\
           "tunesFromMatrix_40-167.out" u (a * ($12+am)/am):($3>0 && $4>0 ? $1*1e3 : 1/0) axes x2y2 w lp ps .2 lt 40 lc rgb "green" tit "          x_{co}" ,\
           "tunesFromMatrix_40-167.out" u (a * ($12+am)/am):($3>0 && $4>0 ? $2*1e2 : 1/0) axes x2y2 w lp ps .2 lt 40 lc rgb "blue" tit "          x'_{co}" ,\
           "orbits_4.data" u ($8):(+35) axes x1y2 w impulse lt 1 lw 1.2 lc 4 tit "  design E"  ,\
           "orbits_4.data" u ($8):(-17) axes x1y2 w impulse lt 1 lw 1.2 lc 4 notit 

 set terminal postscript eps blacktext color enh size 8.cm,5cm "Times-Roman" 12
 set output "gnuplot_tunesFromMATRIX_pathAndTOF.eps"
 replot
 set terminal X11
 unset output

pause .1


#################

# set title "Cell tunes \n From field maps"    font "roman,14"   # offset 0,+.7    

 set xlabel "{/Symbol g }m_0 [MeV]"        font "roman,16"   # offset +4,-.5 rotate by +20  
# set x2label "a{/Symbol .g}"        font "roman,16"   # offset +4,-.5 rotate by +20  
 set ylabel "Q_x, Q_y"             font "roman,13"   #offset -0,-1 rotate by -20 
 unset y2label 

 set xtics  font "roman,12" nomirror
# set x2tics  font "roman,12" nomirror
 unset y2tics  
 set ytics  font "roman,12" mirror      #offset 0,-.6

set key maxcol 1
set key t c  font "roman, 10"  samplen 1  
 
#set grid

 plot \
      "tunesFromMatrix_40-167.out" u ($12+am):($3>0 && $4>0 ? $3 : 1/0) axes x1y1 w lp ps .2 lt 20 tit "  Sep. maps, Q_x"  ,\
      "tunesFromMatrix_40-167.out" u ($12+am):($3>0 && $4>0 ? $4 : 1/0) axes x1y1 w lp ps .2 lt 30 tit "             Q_y"  ,\
           "orbits_4.data" u ($8):(+.45) w impulse lt 1 lw 1.2 lc 4 tit "design E" 

set samples 100000
 set terminal postscript eps blacktext color enh size 8.cm,5cm "Times-Roman" 12
 set output "gnuplot_tunesFromMATRIX_QxQy.eps"
 replot
 set terminal X11
 unset output

      pause 1

 exit
