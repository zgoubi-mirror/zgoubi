      #Tunes versus turn number !  

#      set title "Emittances, from tunesFromFai"  font "roman,14" 

      set xlabel "E [MeV]" font "roman,12" 
      set ylabel  "Max. {/Symbol e}_{x,y}/{/Symbol p}, norm. [m]" font "roman,12" 
      set y2label "Qx, Qy" font "roman,12" 

      set xtics font "roman,12" 
      set ytics nomirror font "roman,12" 
      set y2tics nomirror font "roman,12"  

#      set yrange [.6848:.6852]
#      set y2range [.6748:.6752]

      set key  b c font "roman,12" spacing 1
     set key maxrow 2

       set logscale y
#       set logscale y2
       set format y "10^{%L}"
#       set format y2 "10^{%L}"

am = 0.511

set label "(A)" at 154, 2  font "roman, 20"

set xrange [38:170.]
set y2range [0:.45]
      plot  \
       "tunesFromFai_H.out"  u ($11):($7 *$11/am) axes x1y1 w l lc rgb "red" lw 3 tit "{/Symbol e}_x  /H"  ,\
       "tunesFromFai_Hz.out"  u ($11):($7 *$11/am) axes x1y1 w l lc rgb "red" lw 3 tit "{/Symbol e}_x /Hz"  ,\
       "tunesFromFai_V.out"   u ($11):($8 *$11/am) axes x1y1 w l lc rgb "blue" lw 3 tit "{/Symbol e}_y /V"  ,\
       "tunesFromFai_H.out"   u ($11):(1.-$5) axis x1y2 w l lc rgb "red"  tit "Qx /H" ,\
       "tunesFromFai_Hz.out"   u ($11):(1.-$5) axis x1y2 w l lc rgb "red"  tit "Qx /Hz" ,\
       "tunesFromFai_V.out"   u ($11):(1.-$6) axis x1y2 w l lc rgb "blue"  tit "Qy /V" ,\
           "orbits_4.data" u ($8+am):(+10.) axes x1y1 w impulse lt 1 lw 2 lc 2 notit  

      set terminal postscript eps blacktext color  enh size 8.3cm,4cm "Times-Roman" 12  
       set output "gnuplot_tunesFromFai_emittances.eps"  
       replot  
       set terminal X11  
       unset output  

      pause 2
 
 exit
