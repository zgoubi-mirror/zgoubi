

\thispagestyle{empty}

\begin{minipage}{1.\linewidth}
\bf
  \flushright{F. M\'eot}
\vspace{-2ex}
  
  \flushright{BNL C-AD}
\vspace{-2ex}
  
  \flushright{Zgoubi 2019 Workshop, Boulder, CO}
\vspace{-2ex}
  
\flushright{24-29 Aug. 2019} 
\end{minipage}


\vspace{5ex}

\centerline{\LARGE \bf
  Simulation of CBETA ERL Using Field Maps
}

~

\centerline{\LARGE \bf
1. Periodic Orbits and Optical Functions 
}

\centerline{\LARGE \bf
  Along the Permanent Magnet Return Loop
}

\vspace{5ex}
\author{
F.~M\'eot
\\
Collider-Accelerator Department, BNL, Upton, NY 11973 \\
}


\section*{Recommended readings}


\nin - Ref. [1] in the following: In the documentation folder, ICAP18\_CBETA.pdf, ``Simulation of CBETA BNL-CORNELL ERL Using Field Maps'', ICAP 18, Key West (2018)

\smallskip
\nin - Zgoubi Users' Guide, regarding the keywords used in the exercise (SYSTEM, TOSCA, MULTIPOL, FAISTORE, TWISS, OPTICS, etc.). Hint: use the index (last 3 pages of the Guide) to locate the related sections in Part~A (general explanations) and Part~B (input data formatting including units). 

\smallskip
\nin - During the exercise, it is recommended to keep 2 copies of the guide at hand, with one copy opened at the Index (last 3 pages of the document).

\section*{Specific software installations needed in this exercise, on your preferred computer }

The exercises propose  animations using zgoubi.dat and small gnuplot scripts. We'll also use ``zpop'' plotter/data analyser (it allllows playing with/analysing data from zgoubi.fai, zgoubi.plt files: plots whatever against whatever from these files, provides tunes and ellipse parameter computation, etc.) We'll also need to navigate in the zgoubi fortran files folder, and re-compile zgoubi from time to time, that's part of the exercise.

\smallskip

\nib {\bf Working environment:}  linux or mac os

The following installations are easy and quick to achieve in  linux environment (and mac as well), count less than 30\,min if all goes well,  which is the case under linux. In windows environment (unfortunately we still  see this kind of thing), you may need a linux emulator, e.g., cygwin.

\smallskip

\nib {\bf Zgoubi package:}

\smallskip
- Download zgoubi from \\
\centerline{\blue https://sourceforge.net/p/zgoubi/code/HEAD/tree/trunk/ }  \\
In there clic ``Download Snapshot'' this will generate a tar ball, or a zip (about 500\,MB, count a couple of minutes),
of the form zgoubi-code-r132x.zip.

Save it in your preferred  folder on your computer
(although: recommended name of that folder is  ``zgoubi'', for easier communication, file exchanges, etc.)

\smallskip
- Untar (or unzip) this package, go in your new (congrats!)  [pathTo]/zgoubi/zgoubi-code-r132x folder. In there, just type ``make'' to compile.
Note: this ``make'' requires gfortran (if you do not have it: ``sudo apt-get install gfortran'' under linux).


{\blue You're done. You just  installed both zgoubi and zpop. Congrats!}

Note: If you now want to play with zgoubi fortran files, or fix the last bug, they are essentially in  [pathTo]/zgoubi/zgoubi-code-r132x/zgoubi/ (that's a lot of ``zgoubi''... ok).

\smallskip
\nib {\bf To run zgoubi}:  just type the  address of the executable \\
\centerline{\blue [pathTo]/zgoubi/zgoubi-code-r132x/zgoubi/zgoubi}

Example:\ \ \ \ {\sl \red [pathTo]/zgoubi/zgoubi-code-r132x/zgoubi -in myZgoubi.dat \ \ \ \  (or myZgoubi.res)}

\smallskip
\nib {\bf To run zpop:} \\
- It has to be run in an xterm terminal. Under linux the launch command is  ``xterm'' (if you do not have that command: ``sudo apt-get install xterm'')  \\
- the launch command: \\
\centerline{\blue [pathTo]/zgoubi/zgoubi-code-r132x/zpop/zpop}

Note: zpop fortran files are essentially all in [pathTo]/zgoubi/zgoubi-code-r132x/zpop/.

\smallskip
\nib {\bf We'll need gnuplot.}

If you do not have it: ``sudo apt-get install gnuplot''.

\smallskip
\nib {\bf And finally, We'll need a .eps viewer.} Under linux: any of ``gv'', ``okular'', ``evince'', ``ghostview'' (you don't have any? Ok: ``sudo apt-get install gv'',  ``sudo apt-get install evince'', etc.).

In mac os: ``open''



\section*{Simulation files}

Along with being available in the workshop environment, the complete workshop CBETA package is as well available in \\
\centerline{\blue [pathTo]/zgoubi/zgoubi-code/exemples/CBETA}

Any possible update (in the course of the exercise for instance, including the solutions in due time) can be downloaded from \\
\centerline{\blue $$https://sourceforge.net/p/zgoubi/code/HEAD/tree/trunk/exemples/CBETA/$$}



\section*{Keywords we play with in this exercise}

\nin \texttt{SYSTEM} is used for mostly on-line plottng and animations, based on simple gnuplot scritps. 
\nin \texttt{TOSCA} is used to handle 2D or 3D OPERA field maps of the Halbach magnets,  for the simulation of the return arc.
It is also used for the simulation of most of the dipoles and quadrupoles of the SX and RX sections.\\
\nin \texttt{FIT} is used to find periodic orbits in the cell, as well as the values of the optical functions at the
downstream end of a spreader line (which is the start of the FFAG return loop). \\ 
\nin \texttt{TWISS} is used to get and transport periodical optical functions (logged in zgoubi.TWISS.out)\\
\nin \texttt{OPTICS} is used to transport non-periodical optical functions (logged in zgoubi.OPTICS.out) \\
\nin and several others of course, that you already know about however.


\section*{Working hypotheses:}

The interest of using field maps is that it brings greatest accuracy in the modeling of optical elements (namely, of their magnetic or electric, or ExB field). Allied with stepwise ray-tracing techniques, it brings greatest accuracy on
the resolution of the Lorentz equation. 

And yes, zgoubi knows how to integrate in RF cavities, this has been done during the Neutrino Factory design studies.
This is in the plans for the  simulation of CBETA 6-cavity linac. 

\bigskip

(i) Ref. [1] provides the necessary infos regarding CBETA design and terminology. 

\smallskip

(ii) There is no quadrupole knob over the FA-TA-ZA-ZB-TB-FB return loop (Fig.1 in~[1]), however there is corrector dipole windings
(Fig.~9 in~[1]) but we will ignore them here. 
In these conditions the reference orbits and the optical functions (Fig.4 in~[1]),  
along the loop are fully determined by their values at the start of the FA arc (which coincides with the end of the SX spreader line).

\smallskip

(iii) For this exercise we will work in the folder \\
\centerline{\blue [pathTo]/CBETA/zgoubiFiles/FFAGLoop/FA }

\smallskip

(iv) The field maps of CBETA permanent magnets are stored in \\
\centerline{\blue [pathTo]/CBETA/zgoubiFiles/fieldmaps} 
gnuplot scripts found there allow visualizing some properties of the field, from this field maps.

Note the presence there of ``binarize.f''. This short fortran file converts the ascii  field maps to binary format,
the file name is just added the suffix ``\_b'' (zgoubi interprets ``\_b'', see the Guide).
That saves a factor of about 5 in CPU time to read the maps, from zgoubi.dat, by zgoubi. And a similar factor in file volume.

\clearpage

\section*{ Numerical experiments: }


\subsubsection*{First part: explore the FFAG arc cell (Figs. 2, 3  in [1]), compute its periodic optical functions (Fig. 4 in [1]).} 

\nin 1/ Let's take a look at the field maps: go to [pathTo]/CBETA/zgoubiFiles/fieldmaps.

This is all 2D field maps. We do at the Lab have, and use, their 3D versions, much more voluminous. We'll save time here working 2D.
  
  Use the gnuplot script therein to get views of $\vec B_Z(X or Y)$ components (mid plane symmetry results in $\vec B_{X,Y}(X or Y)=0$).


  \smallskip
  
\nin 2/ Go to [pathTo]/CBETA/zgoubiFiles/FFAGLoop/FA.  Therein, from the  FA arc cell simlation 
file ``FA-042\_1stcell\_FITInitialBta.res'', extract the  QF and BD \texttt{TOSCA} sections,
to two different files, QF.dat and BD.dat respectively. 

Complete these QF.dat and BD.dat to produce -~and plot (using zpop -~recommend, for zgoubi learning purposes~- otherwise a quick gnuplot), for each independently, the field experienced across the field map at $x=0, \, \pm 1, \, \pm 2$\,cm from the Halbach magnet axis (in a similar manner to Fig.~5-bottom in [1] or to what we just saw in question 1/).

\smallskip

\nin 3/ We'll now compute the periodic orbits in the cell.

From the simulation  file ``FA-042\_1stcell\_FITInitialBta.res'', extract the FA arc cell. 
Complete first the missing negative drift data (``****'').
% QF:  -33.35           ! = (80cm - 13.3cm)/2 
% BD: -33.9           ! = (80cm - 12.2cm)/2

Create an OBJET/KOBJ=2 with 4 particles at the design energies 42, 78, 114  and 150\,MeV, and with their initial
$Y_0$ and $T_0$ coordinates estimated from Fig.\,4.
Using \texttt{FIT}, find the exact $Y_0, \ T_0$ periodic orbit coordinates.
Note an outcome of FIT: zgoubi.FIT.out.dat, a copy of the data file with the variables updated. 
% -1.2 -100. 0. 0. 0. 1. 
% -1.4  -50. 0. 0. 0. 1.85724040E+00 'o' 
%   .0  -.02 0. 0. 0. 2.71445930E+00 'o' 
%  2.2    0. 0. 0. 0. 3.57167210E+00 'o' 


Plot the 4 orbits across the cell (in a similar manner to Fig.\,4). 

Plot the magnetic field along the 4 orbits.

\smallskip

\nin 4/ Compute the Twiss parameters at   cell ends, all 4 design energies, in just one go.
% Use OBJET/KOBJ=5.4

\smallskip

\nin 5/ More challenging: transport the optical function through the cell, using zgoubi.plt. At the manner of Fig.4-right in Ref.[1].

betaFromPlt.f from [pathTo]/zgoubi-code/toolbox/betaFromPlt does that -~you'll have to compile that Fortran executable on your own system.


\subsubsection*{Second part: find the optical functions at the 4 design energies, at the start of the FA arc}
 
\nib There are two subtleties in the handling of the periodic optical functions of the permanent magnet return loop.

\nin - First, while the QF-BD doublet FA cell repeats itself 16 times over the upstream FA arc of the return loop (Fig.~2.1.2, p.~19, in [1]), the arc actually starts with a half BD magnet.
This is for the purpose of optical matching at the connection between S1 and the FA arc. 

Thus, the optical functions upstream of that half-BD, at the location of the downstream end of S1, are determined by the periodic functions of the FA cell.

\nin - Next, an aspect proper to the use of field maps:  the S1-FA arc connection happens to be located within the extent of
the half-BD field map, thus getting them from there requires a numerical method.

We choose here to use a FIT procedure as a convenient and simple way to achieve that.

~

\nin 6/ Add the half-BD section that ensures the connection to S1,
upstream of the FA cell. Using the optical functions at the downstream end of that sequence as
the constraints, find the optical functions at the S1-FA connection.  

~

\nin 5/ Check your results:

- inject the initial orbit coordinates at the start of the FFAG loop (file FFAGLoop.dat),
plot them, all 4 energies (in a similar manner to the 42\,MeV orbit in [2], Fig.~13-left)

- inject the initial optical functions at the start of the FFAG loop, 
for instance for the 42\,MeV case, and verify that they propagate correctly all the way. 


 

