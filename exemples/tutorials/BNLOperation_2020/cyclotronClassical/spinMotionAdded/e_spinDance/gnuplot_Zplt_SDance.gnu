set xlabel "X_{Lab}"; set ylabel "Y_{Lab}"; set zlabel "S_Z"; set xtics; set ytics; set ztics  #unset ztics
set zrange  [0:]; set xrange [-25:25]; set yrange [-25:25]; set xyplane 0
dip1=6; dip2=21; dd=3 # positining of 1st and last dipoles in zgoubi.dat sequence, and increment
# magnifies apparent spin tilt   speed up graphic           pi/3                   z norm
         mag = 10.            ;     speedUp=1     ;     pi3 = 4.*atan(1.)/3  ;     nz=0.18

# Just 2D, projected in (X,Y) plane, first:
set size ratio -1

do for [i=1:239]{ plot \
for [dip=dip1:dip2:dd] "zgoubi.plt" every 1::::speedUp*i u \
($19==1 && $42==dip? $10*cos(-$22-pi3*(dip-6.)/3.) :1/0): \
($10*sin(-$22-pi3*(dip-6.)/3.)) w l lw 3 notit ,\
for [dip=dip1:dip2:dd] "zgoubi.plt" every 1::::speedUp*i u \
($19==1 && $42==dip? $10*cos(-$22-pi3*(dip-6.)/3.) +mag*(cos(-$22-pi3*(dip-6.)/3.)*$33-sin(-$22-pi3*(dip-6.)/3.)*$34) :1/0): \
($10*sin(-$22-pi3*(dip-6.)/3.) +mag*(sin(-$22-pi3*(dip-6.)/3.)*$33+cos(-$22-pi3*(dip-6.)/3.)*$34)) w l notit }

set terminal postscript eps blacktext color  enh  
set output "gnuplot_Zplt_spinDance_2D.eps"  
do for [i=1:239]{ plot \
for [dip=dip1:dip2:dd] "zgoubi.plt" every 1::::speedUp*i u \
($19==1 && $42==dip? $10*cos(-$22-pi3*(dip-6.)/3.) :1/0): \
($10*sin(-$22-pi3*(dip-6.)/3.)) w l lw 3 notit ,\
for [dip=dip1:dip2:dd] "zgoubi.plt" every 1::::speedUp*i u \
($19==1 && $42==dip? $10*cos(-$22-pi3*(dip-6.)/3.) +mag*(cos(-$22-pi3*(dip-6.)/3.)*$33-sin(-$22-pi3*(dip-6.)/3.)*$34) :1/0): \
($10*sin(-$22-pi3*(dip-6.)/3.) +mag*(sin(-$22-pi3*(dip-6.)/3.)*$33+cos(-$22-pi3*(dip-6.)/3.)*$34)) w l notit }
set terminal X11  
unset output   

unset size

# Then 3D:
do for [i=1:239]{ splot \
for [dip=dip1:dip2:dd] "zgoubi.plt" every speedUp*i::::speedUp*i u ($19==1&& $42==dip? $10*cos(-$22-pi3*(dip-6)/3):1/0):\
($10*sin(-$22-pi3*(dip-6)/3)):($1*0):(mag*(cos(-$22-pi3*(dip-6)/3)*$33-sin(-$22-pi3*(dip-6)/3)*$34)): \
(mag*(sin(-$22-pi3*(dip-6)/3)*$33+cos(-$22-pi3*(dip-6)/3)*$34)):($35/nz)  w vectors notit ,\
for [dip=dip1:dip2:dd] "zgoubi.plt" every 1::::speedUp*i u ($19==1 && $42==dip? $10*cos(-$22-pi3*(dip-6)/3) :1/0): \
($10*sin(-$22-pi3*(dip-6)/3)):($1*0):($19==1&&$42==dip? $10*cos(-$22-pi3*(dip-6)/3):1/0):($10*sin(-$22-pi3*(dip-6)/3)): \
($1*0) w l lw 3 notit ,\
for [dip=dip1:dip2:dd] "zgoubi.plt" every 1::::speedUp*i u ($19==1 && $42==dip? $10*cos(-$22-pi3*(dip-6)/3)+mag*( \
cos(-$22-pi3*(dip-6)/3)*$33-sin(-$22-pi3*(dip-6)/3)*$34):1/0):($10*sin(-$22-pi3*(dip-6)/3)+mag*(sin(-$22-pi3*(dip-6)/3) \
*$33+cos(-$22-pi3*(dip-6)/3)*$34)):($35/nz):($19==1&&$42==dip? $10*cos(-$22-pi3*(dip-6)/3 +mag*(cos(-$22-pi3*(dip-6)/3) \
*$33 -sin(-$22-pi3*(dip-6)/3)*$34)) :1/0): ($10*sin(-$22-pi3*(dip-6)/3) +mag*(sin(-$22-pi3*(dip-6)/3)*$33+cos(-$22-pi3* \
(dip-6)/3) *$34)):($35/nz) w l lw 3 notit  }

do for [i=1:239]{ splot \
for [dip=dip1:dip2:dd] "zgoubi.plt" every 1::::speedUp*i u ($19==1 && $42==dip? $10*cos(-$22-pi3*(dip-6.)/3.) :1/0): \
($10*sin(-$22-pi3*(dip-6.)/3.)):($1*0):(mag*(cos(-$22-pi3*(dip-6.)/3.)*$33-sin(-$22-pi3*(dip-6.)/3.)*$34)):(mag*(sin(-$22-pi3*(dip-6.)/3.)*$33+cos(-$22-pi3*(dip-6.)/3.)*$34)):($35/nz)  w vectors notit ,\
for [dip=dip1:dip2:dd] "zgoubi.plt" every 1::::speedUp*i u ($19==1 && $42==dip? $10*cos(-$22-pi3*(dip-6.)/3.) :1/0): \
($10*sin(-$22-pi3*(dip-6.)/3.)):($1*0):($19==1 && $42==dip? $10*cos(-$22-pi3*(dip-6.)/3.) :1/0):($10*sin(-$22-pi3*(dip-6.)/3.)): \
($1*0) w l lw 3 notit , for [dip=dip1:dip2:dd] "zgoubi.plt" every 1::::speedUp*i u ($19==1 && $42==dip? $10*cos(-$22-pi3*(dip-6.)/3.)\
+mag*(cos(-$22-pi3*(dip-6.)/3.)*$33-sin(-$22-pi3*(dip-6.)/3.)*$34) :1/0): ($10*sin(-$22-pi3*(dip-6.)/3.) +mag*(sin(-$22-pi3*(dip-6.)/3.)*$33+cos(-$22-pi3*(dip-6.)/3.)*$34)):($35/nz):($19==1 && $42==dip? $10*cos(-$22-pi3*(dip-6.)/3. +mag*(cos(-$22-pi3*(dip-6.)/3.)*$33-sin(-$22-pi3*(dip-6.)/3.)*$34)) :1/0): \
($10*sin(-$22-pi3*(dip-6.)/3.) +mag*(sin(-$22-pi3*(dip-6.)/3.)*$33+cos(-$22-pi3*(dip-6.)/3.)*$34)):($35/nz) w l lw 3 notit  }


set terminal postscript eps blacktext color  enh  
set output "gnuplot_Zplt_spinDance.eps"  
do for [i=1:239]{ splot \
for [dip=dip1:dip2:dd] "zgoubi.plt" every 1::::speedUp*i u ($19==1 && $42==dip? $10*cos(-$22-pi3*(dip-6.)/3.) :1/0): \
($10*sin(-$22-pi3*(dip-6.)/3.)):($1*0):(mag*(cos(-$22-pi3*(dip-6.)/3.)*$33-sin(-$22-pi3*(dip-6.)/3.)*$34)):(mag*(sin(-$22-pi3*(dip-6.)/3.)*$33+cos(-$22-pi3*(dip-6.)/3.)*$34)):($35/nz)  w vectors notit ,\
for [dip=dip1:dip2:dd] "zgoubi.plt" every 1::::speedUp*i u ($19==1 && $42==dip? $10*cos(-$22-pi3*(dip-6.)/3.) :1/0): \
($10*sin(-$22-pi3*(dip-6.)/3.)):($1*0):($19==1 && $42==dip? $10*cos(-$22-pi3*(dip-6.)/3.) :1/0):($10*sin(-$22-pi3*(dip-6.)/3.)): \
($1*0) w l lw 3 notit , for [dip=dip1:dip2:dd] "zgoubi.plt" every 1::::speedUp*i u ($19==1 && $42==dip? $10*cos(-$22-pi3*(dip-6.)/3.)\
+mag*(cos(-$22-pi3*(dip-6.)/3.)*$33-sin(-$22-pi3*(dip-6.)/3.)*$34) :1/0): ($10*sin(-$22-pi3*(dip-6.)/3.) +mag*(sin(-$22-pi3*(dip-6.)/3.)*$33+cos(-$22-pi3*(dip-6.)/3.)*$34)):($35/nz):($19==1 && $42==dip? $10*cos(-$22-pi3*(dip-6.)/3. +mag*(cos(-$22-pi3*(dip-6.)/3.)*$33-sin(-$22-pi3*(dip-6.)/3.)*$34)) :1/0): \
($10*sin(-$22-pi3*(dip-6.)/3.) +mag*(sin(-$22-pi3*(dip-6.)/3.)*$33+cos(-$22-pi3*(dip-6.)/3.)*$34)):($35/nz) w l lw 3 notit  }
set terminal X11  
unset output   
pause 3
exit