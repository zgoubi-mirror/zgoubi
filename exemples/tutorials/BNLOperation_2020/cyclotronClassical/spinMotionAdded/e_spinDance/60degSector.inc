Cyclotron, classical.
! 60 degree sector, simulated using DIPOLE.  This file produces the transport matrix.
'OBJET'                                                                              
64.62444403717985                      ! Reference Brho ("BORO" in the users' guide) -> 200keV proton.
5
12.9248888 0. 0. 0. 0.  1. 'm'                                ! Closed orbit coordinates for D=p/p_0=1
1                                              ! => 200keV proton. R=Brho/B=64.624444037[kG.cm]/5[kG].

'MARKER'    #S_60degSectorUnifB                               ! Label should not exceed 20 characters.
'DIPOLE'                                                    ! Analytical definition of a dipole field.
2              ! IL=2, only purpose is to log trajectories to zgoubi.plt (for further plotting, here).
60. 50.                                                        ! Sector angle AT; reference radius RM.
30.  5. 0. 0. 0.                 ! Reference azimuthal angle ACN; BM field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6                      ! Entrance face placed at omega+=30 deg from ACN.
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6                        ! Exit face placed at omega-=-30 deg from ACN.
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
2 10    ! '2' is for 2nd degree interpolation. Could also be '25' (5*5 points grid) or 4 (4th degree).
1.                         ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                  ! Magnet positionning RE, TE, RS, TS. Could be instead non-zero, e.g.,
!                                2 RE=50. 0. RS=50. 0., as long as Yo is amended accordingly in OBJET.

'MARKER'    #E_60degSectorUnifB                               ! Label should not exceed 20 characters.

'MATRIX'
1 0

'END'
 