set xlabel "G{Symbol g}"; set ylabel "Spin precession angle [deg]";  set y2label "difference num./theor."; set logscale y2
set xtics; set ytics nomirror; set y2tics; am = 938.27208; G = 1.79284735; pi = 4.*atan(1.)
plot "zgoubi.fai" u ($31*$25/$29):((4.*pi -atan($21/$20))/pi*180.) w lp pt 8 ps .1 tit "ray tracing" , "zgoubi.fai" u \
($31*$25/$29):(abs((4.*pi -atan($21/$20))/pi*180.-$31*$25/$29*360.)) axes x1y2 w lp pt 8 ps .1 tit "diff." ; pause 1.

set terminal postscript eps blacktext color  enh  
set output "gnuplot_Zfai_spin.eps"  
replot  
set terminal X11  
unset output   
pause 3
exit

