Cyclotron, classical. Analytical model of dipole field.
 
! 'INCLUDE'
! Include_SegmentStart : ./cyclo_ProbAddSpin.inc[ProbAddSpin_S,*:Prob(n_inc, depth :  1 2)
'MARKER'  ProbAddSpin_S                                                   ! Just for edition purposes.       1
'OBJET'                                                                                                      2
64.62444403717985                                                                            ! 200keV.
2
2 1
12.9248888074 0. 0. 0. 0.  1.     'm'      ! D=1 => 200keV proton. R=Brho/B=64.624444037[kG.cm]/5[kG].
67.997983 0. 0. 0. 0. 5.2610112 'o'           ! p[MeV/c]=101.926, Brho[kG.cm]=339.990, kin-E[MeV]=5.52.
1 1
'PARTICUL'                                               ! This is required to get the time-of-flight,       3
PROTON                                                      ! otherwise zgoubi only requires rigidity.
'SPNTRK'                                                                      ! Request spin tracking.       4
1                                              ! All spins initial longitudinal (parallel to OX axis).
! 'INCLUDE'
! Include_SegmentStart : ./60degSector.inc[#S_60degSectorUnifB,*:#E_6(n_inc, depth : *1 3)
'MARKER'    #S_60degSectorUnifB                               ! Label should not exceed 20 characters.       5
'DIPOLE'                                                    ! Analytical definition of a dipole field.       6
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30.  5. 0. 0. 0.                 ! Reference azimuthal angle ACN; BM field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6                      ! Entrance face placed at omega+=30 deg from ACN.
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6                        ! Exit face placed at omega-=-30 deg from ACN.
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
2 10    ! '2' is for 2nd degree interpolation. Could also be '25' (5*5 points grid) or 4 (4th degree).
.3                         ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                  ! Magnet positionning RE, TE, RS, TS. Could be instead non-zero, e.g.,
'SPNPRT'                                                                      ! Request spin tracking.       7
'FAISCEAU'                                                                                                   8
!                                2 RE=50. 0. RS=50. 0., as long as Yo is amended accordingly in OBJET.
! Include_SegmentEnd : ./60degSector.inc                             (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorUnifB                               ! Label should not exceed 20 characters.       9
! 'INCLUDE'
! Include_SegmentStart : ./60degSector.inc[#S_60degSectorUnifB,*:#E_6(n_inc, depth : *1 3)
'MARKER'    #S_60degSectorUnifB                               ! Label should not exceed 20 characters.      10
'DIPOLE'                                                    ! Analytical definition of a dipole field.      11
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30.  5. 0. 0. 0.                 ! Reference azimuthal angle ACN; BM field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6                      ! Entrance face placed at omega+=30 deg from ACN.
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6                        ! Exit face placed at omega-=-30 deg from ACN.
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
2 10    ! '2' is for 2nd degree interpolation. Could also be '25' (5*5 points grid) or 4 (4th degree).
.3                         ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                  ! Magnet positionning RE, TE, RS, TS. Could be instead non-zero, e.g.,
!                                2 RE=50. 0. RS=50. 0., as long as Yo is amended accordingly in OBJET.
! Include_SegmentEnd : ./60degSector.inc                             (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorUnifB                               ! Label should not exceed 20 characters.      12
! 'INCLUDE'
! Include_SegmentStart : ./60degSector.inc[#S_60degSectorUnifB,*:#E_6(n_inc, depth : *1 3)
'MARKER'    #S_60degSectorUnifB                               ! Label should not exceed 20 characters.      13
'DIPOLE'                                                    ! Analytical definition of a dipole field.      14
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30.  5. 0. 0. 0.                 ! Reference azimuthal angle ACN; BM field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6                      ! Entrance face placed at omega+=30 deg from ACN.
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6                        ! Exit face placed at omega-=-30 deg from ACN.
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
2 10    ! '2' is for 2nd degree interpolation. Could also be '25' (5*5 points grid) or 4 (4th degree).
.3                         ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                  ! Magnet positionning RE, TE, RS, TS. Could be instead non-zero, e.g.,
!                                2 RE=50. 0. RS=50. 0., as long as Yo is amended accordingly in OBJET.
! Include_SegmentEnd : ./60degSector.inc                             (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorUnifB                               ! Label should not exceed 20 characters.      15
! 'INCLUDE'
! Include_SegmentStart : ./60degSector.inc[#S_60degSectorUnifB,*:#E_6(n_inc, depth : *1 3)
'MARKER'    #S_60degSectorUnifB                               ! Label should not exceed 20 characters.      16
'DIPOLE'                                                    ! Analytical definition of a dipole field.      17
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30.  5. 0. 0. 0.                 ! Reference azimuthal angle ACN; BM field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6                      ! Entrance face placed at omega+=30 deg from ACN.
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6                        ! Exit face placed at omega-=-30 deg from ACN.
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
2 10    ! '2' is for 2nd degree interpolation. Could also be '25' (5*5 points grid) or 4 (4th degree).
.3                         ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                  ! Magnet positionning RE, TE, RS, TS. Could be instead non-zero, e.g.,
!                                2 RE=50. 0. RS=50. 0., as long as Yo is amended accordingly in OBJET.
! Include_SegmentEnd : ./60degSector.inc                             (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorUnifB                               ! Label should not exceed 20 characters.      18
! 'INCLUDE'
! Include_SegmentStart : ./60degSector.inc[#S_60degSectorUnifB,*:#E_6(n_inc, depth : *1 3)
'MARKER'    #S_60degSectorUnifB                               ! Label should not exceed 20 characters.      19
'DIPOLE'                                                    ! Analytical definition of a dipole field.      20
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30.  5. 0. 0. 0.                 ! Reference azimuthal angle ACN; BM field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6                      ! Entrance face placed at omega+=30 deg from ACN.
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6                        ! Exit face placed at omega-=-30 deg from ACN.
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
2 10    ! '2' is for 2nd degree interpolation. Could also be '25' (5*5 points grid) or 4 (4th degree).
.3                         ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                  ! Magnet positionning RE, TE, RS, TS. Could be instead non-zero, e.g.,
!                                2 RE=50. 0. RS=50. 0., as long as Yo is amended accordingly in OBJET.
! Include_SegmentEnd : ./60degSector.inc                             (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorUnifB                               ! Label should not exceed 20 characters.      21
! 'INCLUDE'
! Include_SegmentStart : ./60degSector.inc[#S_60degSectorUnifB,*:#E_6(n_inc, depth : *1 3)
'MARKER'    #S_60degSectorUnifB                               ! Label should not exceed 20 characters.      22
'DIPOLE'                                                    ! Analytical definition of a dipole field.      23
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30.  5. 0. 0. 0.                 ! Reference azimuthal angle ACN; BM field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6                      ! Entrance face placed at omega+=30 deg from ACN.
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6                        ! Exit face placed at omega-=-30 deg from ACN.
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
2 10    ! '2' is for 2nd degree interpolation. Could also be '25' (5*5 points grid) or 4 (4th degree).
.3                         ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                  ! Magnet positionning RE, TE, RS, TS. Could be instead non-zero, e.g.,
!                                2 RE=50. 0. RS=50. 0., as long as Yo is amended accordingly in OBJET.
! Include_SegmentEnd : ./60degSector.inc                             (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorUnifB                               ! Label should not exceed 20 characters.      24
'FAISCEAU'                                                               ! Local particle coordinates.      25
'SPNPRT'                                                      ! Local spin data, logged to zgoubi.res.      26
'FAISTORE'                                                    ! Log particle data here, to zgoubi.fai,      27
zgoubi.fai                             ! for further plotting of spin coordinates (by gnuplot, below).
1
'REBELOTE'                                                               ! Momentum scan, 60 samples.       28
3 0.2  99
'SYSTEM'                                                                                                    29
1
gnuplot gnuplot_Zplt_SXmotion.gnu
 
'END'

************************************************************************************************************************************
      1  Keyword, label(s) :  MARKER      ProbAddSpin_S                                                                IPASS= 1


************************************************************************************************************************************
      2  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       2 POINTS 



************************************************************************************************************************************
      3  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 1

     Particle  properties :
     PROTON
                     Mass          =    938.272        MeV/c2
                     Charge        =   1.602176E-19    C     
                     G  factor     =    1.79285              
                     COM life-time =   1.000000E+99    s     

              Reference  data :
                    mag. rigidity (kG.cm)   :   64.624444      =p/q, such that dev.=B*L/rigidity
                    mass (MeV/c2)           :   938.27208    
                    momentum (MeV/c)        :   19.373921    
                    energy, total (MeV)     :   938.47208    
                    energy, kinetic (MeV)   :  0.19999999    
                    beta = v/c              :  2.0644110049E-02
                    gamma                   :   1.000213158    
                    beta*gamma              :  2.0648510502E-02
                    G*gamma                 :   1.793229509    
                    electric rigidity (MeV) :  0.3999573557    =T[eV]*(gamma+1)/gamma, such that dev.=E*L/rigidity
  
 I, AMQ(1,I), AMQ(2,I)/QE, P/Pref, v/c, time, s :
  
     1   9.38272081E+02  1.00000000E+00  1.00000000E+00  2.06441100E-02  0.00000000E+00  0.00000000E+00
     2   9.38272081E+02  1.00000000E+00  5.26101120E+00  1.07996684E-01  0.00000000E+00  0.00000000E+00

************************************************************************************************************************************
      4  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 1


                Spin  tracking  requested.


                          Particle  mass          =    938.2721     MeV/c2
                          Gyromagnetic  factor  G =    1.792847    

                          Initial spin conditions type  1 :
                               All particles have spin parallel to  X  AXIS

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO    =        64.624 kG*cm
                               beta    =    0.02064411
                               gamma   =   1.00021316
                               gamma*G =   1.7932295094


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        2  PARTICULES :
                               <SX> =     1.000000
                               <SY> =     0.000000
                               <SZ> =     0.000000
                               <S>  =     1.000000

************************************************************************************************************************************
      5  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
      6  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


                OPEN FILE zgoubi.plt                                                                      
                FOR PRINTING TRAJECTORIES

                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 2
                     9-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.3000     cm   (i.e.,   6.0000E-03 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925    -0.000     0.000     0.000            1
  A    1  5.2610    67.998     0.000     0.000     0.000            1.047    67.998     0.000     0.000     0.000            2


                CONDITIONS  DE  MAXWELL  (      283.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.523598776     m ;  Time  (for ref. rigidity & particle) =            NaN s 

************************************************************************************************************************************
      7  Keyword, label(s) :  SPNPRT                                                                                   IPASS= 1



                         Momentum  group  #1 ; average  over 2 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   1.000000   0.000000   0.000000   1.000000        -0.307331  -0.951588   0.000000   0.999986   1.798312 107.898732   0.304961


                Spin  components  of  each  of  the      2  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   |Si,Sf|   (Z,Sf_yz) (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_yz : projection of Sf on YZ plane)

 m  1  1.000000  0.000000  0.000000  1.000000    -0.302266 -0.953224  0.000000  1.000000      1.0002 -107.594   90.000   90.000    1
 o  1  1.000000  0.000000  0.000000  1.000000    -0.312396 -0.949952  0.000000  1.000000      1.0059 -108.204   90.000   90.000    2



                Min/Max  components  of  each  of  the      2  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

 -3.0227E-01 -3.0227E-01 -9.5322E-01 -9.5322E-01  0.0000E+00  0.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.00021E+00     1   1
 -3.1240E-01 -3.1240E-01 -9.4995E-01 -9.4995E-01  0.0000E+00  0.0000E+00  1.0000E+00  1.0000E+00  5.26101E+00  1.00588E+00     2   1

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  2 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

m  1   1.0000    12.925     0.000     0.000     0.000      0.0000    0.0000   12.925   -0.000    0.000    0.000   1.353491E+01     1
               Time of flight (mus) :  2.18694855E-02 mass (MeV/c2) :   938.272    
o  1   5.2610    67.998     0.000     0.000     0.000      0.0000    4.2610   67.998    0.000    0.000    0.000   7.120732E+01     2
               Time of flight (mus) :  2.19934585E-02 mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.046144E-01   6.052017E-09        2        0    0.000      (Y,T)         1
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        2        2    1.000      (Z,P)         1
          NaN   0.0000E+00   1.0000E+00   2.193147E-02   2.860000E+00        2        0    0.000      (t,K)         1

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =            NaN
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =            NaN


  Beam  sigma  matrix  and  determinants : 

   7.582615E-02   4.553559E-09   0.000000E+00   0.000000E+00
   4.553559E-09   2.734531E-16   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)
      normalized (*beta*gamma) :     0.000000E+00    0.000000E+00

************************************************************************************************************************************
      9  Keyword, label(s) :  MARKER      #E_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     10  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     11  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 2
                     9-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.3000     cm   (i.e.,   6.0000E-03 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925    -0.000     0.000     0.000            1
  A    1  5.2610    67.998     0.000     0.000     0.000            1.047    67.998     0.000     0.000     0.000            2


                CONDITIONS  DE  MAXWELL  (      283.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    1.04719755     m ;  Time  (for ref. rigidity & particle) =            NaN s 

************************************************************************************************************************************
     12  Keyword, label(s) :  MARKER      #E_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     13  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     14  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 2
                     9-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.3000     cm   (i.e.,   6.0000E-03 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925    -0.000     0.000     0.000            1
  A    1  5.2610    67.998     0.000     0.000     0.000            1.047    67.998    -0.000     0.000     0.000            2


                CONDITIONS  DE  MAXWELL  (      283.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    1.57079633     m ;  Time  (for ref. rigidity & particle) =            NaN s 

************************************************************************************************************************************
     15  Keyword, label(s) :  MARKER      #E_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     16  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     17  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 2
                     9-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.3000     cm   (i.e.,   6.0000E-03 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925     0.000     0.000     0.000            1
  A    1  5.2610    67.998     0.000     0.000     0.000            1.047    67.998    -0.000     0.000     0.000            2


                CONDITIONS  DE  MAXWELL  (      283.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    2.09439510     m ;  Time  (for ref. rigidity & particle) =            NaN s 

************************************************************************************************************************************
     18  Keyword, label(s) :  MARKER      #E_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     19  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     20  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 2
                     9-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.3000     cm   (i.e.,   6.0000E-03 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925     0.000     0.000     0.000            1
  A    1  5.2610    67.998     0.000     0.000     0.000            1.047    67.998    -0.000     0.000     0.000            2


                CONDITIONS  DE  MAXWELL  (      283.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    2.61799388     m ;  Time  (for ref. rigidity & particle) =            NaN s 

************************************************************************************************************************************
     21  Keyword, label(s) :  MARKER      #E_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     22  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     23  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 2
                     9-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.3000     cm   (i.e.,   6.0000E-03 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925     0.000     0.000     0.000            1
  A    1  5.2610    67.998     0.000     0.000     0.000            1.047    67.998    -0.000     0.000     0.000            2


                CONDITIONS  DE  MAXWELL  (      283.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    3.14159265     m ;  Time  (for ref. rigidity & particle) =            NaN s 

************************************************************************************************************************************
     24  Keyword, label(s) :  MARKER      #E_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     25  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #     24)
                                                  2 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

m  1   1.0000    12.925     0.000     0.000     0.000      0.0000    0.0000   12.925    0.000    0.000    0.000   8.120947E+01     1
               Time of flight (mus) :  0.13121691     mass (MeV/c2) :   938.272    
o  1   5.2610    67.998     0.000     0.000     0.000      0.0000    4.2610   67.998   -0.000    0.000    0.000   4.272439E+02     2
               Time of flight (mus) :  0.13196075     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
          NaN   0.0000E+00   1.0000E+00   4.046144E-01  -1.180696E-15        2        0    0.000      (Y,T)         1
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        2        2    1.000      (Z,P)         1
   0.0000E+00   0.0000E+00   1.0000E+00   1.315888E-01   2.860000E+00        2        0    0.000      (t,K)         1

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =            NaN
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =            NaN

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   7.582614E-02  -4.986413E-16   0.000000E+00   0.000000E+00
  -4.986413E-16   3.279121E-30   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :              NaN    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)
      normalized (*beta*gamma) :              NaN    0.000000E+00

************************************************************************************************************************************
     26  Keyword, label(s) :  SPNPRT                                                                                   IPASS= 1



                         Momentum  group  #1 ; average  over 2 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   1.000000   0.000000   0.000000   1.000000         0.298762   0.953793   0.000000   0.999490   1.798312  72.607602   1.829784


                Spin  components  of  each  of  the      2  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   |Si,Sf|   (Z,Sf_yz) (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_yz : projection of Sf on YZ plane)

 m  1  1.000000  0.000000  0.000000  1.000000     0.268291  0.963338  0.000000  1.000000      1.0002   74.437   90.000   90.000    1
 o  1  1.000000  0.000000  0.000000  1.000000     0.329232  0.944249  0.000000  1.000000      1.0059   70.778   90.000   90.000    2



                Min/Max  components  of  each  of  the      2  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

 -3.0227E-01  2.6829E-01 -9.5322E-01  9.6334E-01  0.0000E+00  0.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.00021E+00     1   1
 -3.1240E-01  3.2923E-01 -9.4995E-01  9.4425E-01  0.0000E+00  0.0000E+00  1.0000E+00  1.0000E+00  5.26101E+00  1.00588E+00     2   1

************************************************************************************************************************************
     27  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 1


                OPEN FILE zgoubi.fai                                                                      
                FOR PRINTING COORDINATES 

               Print will occur at element[s] labeled : 


************************************************************************************************************************************
     28  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 1


                                -----  REBELOTE  -----

     End of pass #        1 through the optical structure 

                     Total of          2 particles have been launched

     Multiple pass, 
          from element #     1 : MARKER    /label1=ProbAddSpin_S       /label2=                    
                             to  REBELOTE  /label1=ProbAddSpin_S       /label2=                    
     ending at pass #       4 at element #    28 : REBELOTE  /label1=                    /label2=                    


     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

                                -----  REBELOTE  -----

     End of pass #        2 through the optical structure 

                     Total of          2 particles have been launched

                                -----  REBELOTE  -----

     End of pass #        3 through the optical structure 

                     Total of          2 particles have been launched


      Next  pass  is  #     4 and  last  pass  through  the  optical  structure


************************************************************************************************************************************
      1  Keyword, label(s) :  MARKER      ProbAddSpin_S                                                                IPASS= 4


************************************************************************************************************************************
      2  Keyword, label(s) :  OBJET                                                                                    IPASS= 4



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         2 particles

************************************************************************************************************************************
      3  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 4


************************************************************************************************************************************
      4  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 4


               Final  spins  of  last  run  taken  as  initial  spins.

************************************************************************************************************************************
      5  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 4


************************************************************************************************************************************
      6  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 4


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 2
                     9-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.3000     cm   (i.e.,   6.0000E-03 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925    -0.000     0.000     0.000            1
  A    1  5.2610    67.998     0.000     0.000     0.000            1.047    67.998     0.000     0.000     0.000            2


                CONDITIONS  DE  MAXWELL  (      283.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.523598776     m ;  Time  (for ref. rigidity & particle) =            NaN s 

************************************************************************************************************************************
      7  Keyword, label(s) :  SPNPRT                                                                                   IPASS= 4



                         Momentum  group  #1 ; average  over 2 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   1.000000   0.000000   0.000000   1.000000        -0.339034   0.935342   0.000000   0.994891   1.798312 109.924075   5.794313


                Spin  components  of  each  of  the      2  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   |Si,Sf|   (Z,Sf_yz) (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_yz : projection of Sf on YZ plane)

 m  1  1.000000  0.000000  0.000000  1.000000    -0.433948  0.900938  0.000000  1.000000      1.0002 -115.718   90.000   90.000    1
 o  1  1.000000  0.000000  0.000000  1.000000    -0.244119  0.969745  0.000000  1.000000      1.0059 -104.130   90.000   90.000    2



                Min/Max  components  of  each  of  the      2  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

 -8.5604E-01  8.3718E-01 -9.5322E-01  9.6334E-01  0.0000E+00  0.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.00021E+00     1   1
 -8.4495E-01  8.3531E-01 -9.4995E-01  9.6975E-01  0.0000E+00  0.0000E+00  1.0000E+00  1.0000E+00  5.26101E+00  1.00588E+00     2   1

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 4

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  2 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

m  1   1.0000    12.925     0.000     0.000     0.000      0.0000    0.0000   12.925   -0.000    0.000    0.000   2.571633E+02     1
               Time of flight (mus) :  0.41552022     mass (MeV/c2) :   938.272    
o  1   5.2610    67.998     0.000     0.000     0.000      0.0000    4.2610   67.998    0.000    0.000    0.000   1.352939E+03     2
               Time of flight (mus) :  0.41787572     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
          NaN   0.0000E+00   1.0000E+00   4.046144E-01   6.052015E-09        2        0    0.000      (Y,T)         4
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        2        2    1.000      (Z,P)         4
   1.2930E-10  -7.6115E+07   3.3701E+04   4.166980E-01   2.860000E+00        2        2    1.000      (t,K)         4

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =            NaN
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =            NaN

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   1.177750E-03
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   2.660000E+00


  Beam  sigma  matrix  and  determinants : 

   7.582615E-02   4.553559E-09   0.000000E+00   0.000000E+00
   4.553559E-09   2.734531E-16   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :              NaN    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)
      normalized (*beta*gamma) :              NaN    0.000000E+00

************************************************************************************************************************************
      9  Keyword, label(s) :  MARKER      #E_60degSectorUnifB                                                          IPASS= 4


************************************************************************************************************************************
     10  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 4


************************************************************************************************************************************
     11  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 4


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 2
                     9-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.3000     cm   (i.e.,   6.0000E-03 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925    -0.000     0.000     0.000            1
  A    1  5.2610    67.998     0.000     0.000     0.000            1.047    67.998     0.000     0.000     0.000            2


                CONDITIONS  DE  MAXWELL  (      283.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    1.04719755     m ;  Time  (for ref. rigidity & particle) =            NaN s 

************************************************************************************************************************************
     12  Keyword, label(s) :  MARKER      #E_60degSectorUnifB                                                          IPASS= 4


************************************************************************************************************************************
     13  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 4


************************************************************************************************************************************
     14  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 4


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 2
                     9-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.3000     cm   (i.e.,   6.0000E-03 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925    -0.000     0.000     0.000            1
  A    1  5.2610    67.998     0.000     0.000     0.000            1.047    67.998    -0.000     0.000     0.000            2


                CONDITIONS  DE  MAXWELL  (      283.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    1.57079633     m ;  Time  (for ref. rigidity & particle) =            NaN s 

************************************************************************************************************************************
     15  Keyword, label(s) :  MARKER      #E_60degSectorUnifB                                                          IPASS= 4


************************************************************************************************************************************
     16  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 4


************************************************************************************************************************************
     17  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 4


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 2
                     9-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.3000     cm   (i.e.,   6.0000E-03 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925     0.000     0.000     0.000            1
  A    1  5.2610    67.998     0.000     0.000     0.000            1.047    67.998    -0.000     0.000     0.000            2


                CONDITIONS  DE  MAXWELL  (      283.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    2.09439510     m ;  Time  (for ref. rigidity & particle) =            NaN s 

************************************************************************************************************************************
     18  Keyword, label(s) :  MARKER      #E_60degSectorUnifB                                                          IPASS= 4


************************************************************************************************************************************
     19  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 4


************************************************************************************************************************************
     20  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 4


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 2
                     9-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.3000     cm   (i.e.,   6.0000E-03 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925     0.000     0.000     0.000            1
  A    1  5.2610    67.998     0.000     0.000     0.000            1.047    67.998    -0.000     0.000     0.000            2


                CONDITIONS  DE  MAXWELL  (      283.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    2.61799388     m ;  Time  (for ref. rigidity & particle) =            NaN s 

************************************************************************************************************************************
     21  Keyword, label(s) :  MARKER      #E_60degSectorUnifB                                                          IPASS= 4


************************************************************************************************************************************
     22  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 4


************************************************************************************************************************************
     23  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 4


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 2
                     9-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.3000     cm   (i.e.,   6.0000E-03 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925     0.000     0.000     0.000            1
  A    1  5.2610    67.998     0.000     0.000     0.000            1.047    67.998    -0.000     0.000     0.000            2


                CONDITIONS  DE  MAXWELL  (      283.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    3.14159265     m ;  Time  (for ref. rigidity & particle) =            NaN s 

************************************************************************************************************************************
     24  Keyword, label(s) :  MARKER      #E_60degSectorUnifB                                                          IPASS= 4


************************************************************************************************************************************
     25  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 4

0                                             TRACE DU FAISCEAU
                                           (follows element #     24)
                                                  2 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

m  1   1.0000    12.925     0.000     0.000     0.000      0.0000    0.0000   12.925    0.000    0.000    0.000   3.248379E+02     1
               Time of flight (mus) :  0.52486765     mass (MeV/c2) :   938.272    
o  1   5.2610    67.998     0.000     0.000     0.000      0.0000    4.2610   67.998   -0.000    0.000    0.000   1.708976E+03     2
               Time of flight (mus) :  0.52784301     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   1.1760E-22   8.0472E+07   2.0256E+21   4.046144E-01  -1.014163E-14        2        2    1.000      (Y,T)         4
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        2        2    1.000      (Z,P)         4
   1.8286E-10  -6.7985E+07   3.8023E+04   5.263553E-01   2.860000E+00        2        2    1.000      (t,K)         4

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   2.753655E-01
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   1.093960E-14

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   1.487684E-03
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   2.660000E+00


  Beam  sigma  matrix  and  determinants : 

   7.582614E-02  -3.012388E-15   0.000000E+00   0.000000E+00
  -3.012388E-15   1.196748E-28   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     3.743392E-23    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)
      normalized (*beta*gamma) :     7.729547E-25    0.000000E+00

************************************************************************************************************************************
     26  Keyword, label(s) :  SPNPRT                                                                                   IPASS= 4



                         Momentum  group  #1 ; average  over 2 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   1.000000   0.000000   0.000000   1.000000         0.346225  -0.929461   0.000000   0.991852   1.798312  69.569590   7.319136


                Spin  components  of  each  of  the      2  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   |Si,Sf|   (Z,Sf_yz) (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_yz : projection of Sf on YZ plane)

 m  1  1.000000  0.000000  0.000000  1.000000     0.465608 -0.884991  0.000000  1.000000      1.0002   62.250   90.000   90.000    1
 o  1  1.000000  0.000000  0.000000  1.000000     0.226843 -0.973931  0.000000  1.000000      1.0059   76.889   90.000   90.000    2



                Min/Max  components  of  each  of  the      2  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

 -8.5604E-01  8.3718E-01 -9.5322E-01  9.6334E-01  0.0000E+00  0.0000E+00  1.0000E+00  1.0000E+00  1.00000E+00  1.00021E+00     1   1
 -8.4495E-01  8.3531E-01 -9.7393E-01  9.6975E-01  0.0000E+00  0.0000E+00  1.0000E+00  1.0000E+00  5.26101E+00  1.00588E+00     2   1

************************************************************************************************************************************
     27  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 4


************************************************************************************************************************************
     28  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 4


                         >>>>  End  of  'REBELOTE'  procedure  <<<<

      There  has  been          4  passes  through  the  optical  structure 

                     Total of          2 particles have been launched

************************************************************************************************************************************
     29  Keyword, label(s) :  SYSTEM                                                                                   IPASS= 5

     Number  of  commands :   1,  as  follows : 

 gnuplot gnuplot_Zplt_SXmotion.gnu

************************************************************************************************************************************
     30  Keyword, label(s) :  END                                                                                      IPASS= 5


************************************************************************************************************************************
 Pgm zgoubi : Execution ended normally, upon keyword END or FIN
   
 File in:   cyclo_ProbAddSpinD.dat
 File out:  zgoubi.res

  Zgoubi, author's dvlpmnt version.
  Job  started  on  19-05-2020,  at  09:12:30 
  JOB  ENDED  ON    19-05-2020,  AT  09:12:34 

   CPU time, total :    0.17279600000000001     
