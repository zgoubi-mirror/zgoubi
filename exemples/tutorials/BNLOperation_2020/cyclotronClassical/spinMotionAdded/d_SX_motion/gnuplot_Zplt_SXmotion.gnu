set xlabel "angle  [rad]"; set ylabel "S_X";  set y2label "rel. difference"; set logscale y2
set xtics; set ytics nomirror; set y2tics; cm2m = 0.01; am = 938.27208; G = 1.79284735; pi = 4.*atan(1.)
R1= 12.9248888074e-2; Ek1 = .200
R2= 67.997983e-2    ; Ek2 = 5.52000
#trj = 1; R = R1; Ek = Ek1; gma = (am+Ek)/am
trj = 2; R = R2; Ek = Ek2; gma = (am+Ek)/am
plot "zgoubi.plt" u ($19==trj? $14*cm2m/R :1/0):($33) w p pt 8 ps .1 tit "ray tracing", cos(G*gma*x) w l tit "theor." ,\
     "zgoubi.plt" u ($19==trj? $14*cm2m/R :1/0):($33-cos(G*gma*$14*cm2m/R)) axes x1y2 w l tit "diff."
pause .1

set terminal postscript eps blacktext color  enh  
set output "gnuplot_Zplt_Sx-vs-tta.eps"  
replot  
set terminal X11  
unset output   
pause 3
exit

