Cyclotron, classical. Analytical model of dipole field.
'OBJET'                                                                                                      1
64.62444403717985                                                                             ! 200keV.
2
1 1
12.924870 0. 0. 0. 0.  1.  'm'              ! D=1 => 200keV proton. R=Brho/B=64.624444037[kG.cm]/5[kG].
1
'PARTICUL'                                                ! This is required to get the time-of-flight,      2
PROTON                                                       ! otherwise zgoubi only requires rigidity.
'SPNTRK'                                                                                                     3
2
'FAISTORE'                                                                                                   4
zgoubi.fai                                                            ! Log particle coordinates, here.
1
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./180degSector.inc[#S_180degSectorUnifB,*:#E(n_inc, depth : f1,2)
'MARKER'    #S_180degSectorUnifB                              ! Label should not exceed 20 characters.       5
'DIPOLE'                                                                                                     6
2
180. 50.                                                                    ! Sector angle AT ->  180.
90.  5. 0. 0. 0.                                                                         ! ACN ->  90.
0.  0.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
90. 0.  1.E6  -1.E6  1.E6  1.E6                                        ! Entrance face, omega+ ->  90.
0.   0.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-90. 0.  1.E6  -1.E6  1.E6  1.E6                             ! Exit face, omega- ->  -30 deg from ACN.
0. 0.
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
2 10
1.
2  0. 0. 0. 0.
! Include_SegmentEnd : ./180degSector.inc                            (had n_inc, depth :  1 2)
'MARKER'    #E_180degSectorUnifB                              ! Label should not exceed 20 characters.       7
'FAISCEAU'                                                                ! Local particle coordinates.      8
'CAVITE'                                                                                                     9
7
0 22862934.0
285e3 -0.5235987755982988
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./180degSector.inc[#S_180degSectorUnifB,*:#E(n_inc, depth : f1,2)
'MARKER'    #S_180degSectorUnifB                              ! Label should not exceed 20 characters.      10
'DIPOLE'                                                                                                    11
2
180. 50.                                                                    ! Sector angle AT ->  180.
90.  5. 0. 0. 0.                                                                         ! ACN ->  90.
0.  0.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
90. 0.  1.E6  -1.E6  1.E6  1.E6                                        ! Entrance face, omega+ ->  90.
0.   0.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-90. 0.  1.E6  -1.E6  1.E6  1.E6                             ! Exit face, omega- ->  -30 deg from ACN.
0. 0.
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
2 10
1.
2  0. 0. 0. 0.
! Include_SegmentEnd : ./180degSector.inc                            (had n_inc, depth :  1 2)
'MARKER'    #E_180degSectorUnifB                              ! Label should not exceed 20 characters.      12
'CAVITE'                                                                                                    13
7
0 22862934.0
285e3 -3.665191429188092
'REBELOTE'                                                                                                  14
26 0.4 99                                                                         ! 26+1 turn tracking.
'FAISCEAU'                                                                                                  15
'END'

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 1

     Particle  properties :
     PROTON
                     Mass          =    938.272        MeV/c2
                     Charge        =   1.602176E-19    C     
                     G  factor     =    1.79285              
                     COM life-time =   1.000000E+99    s     

              Reference  data :
                    mag. rigidity (kG.cm)   :   64.624444      =p/q, such that dev.=B*L/rigidity
                    mass (MeV/c2)           :   938.27208    
                    momentum (MeV/c)        :   19.373921    
                    energy, total (MeV)     :   938.47208    
                    energy, kinetic (MeV)   :  0.19999999    
                    beta = v/c              :  2.0644110049E-02
                    gamma                   :   1.000213158    
                    beta*gamma              :  2.0648510502E-02
                    G*gamma                 :   1.793229509    
                    electric rigidity (MeV) :  0.3999573557    =T[eV]*(gamma+1)/gamma, such that dev.=E*L/rigidity
  
 I, AMQ(1,I), AMQ(2,I)/QE, P/Pref, v/c, time, s :
  
     1   9.38272081E+02  1.00000000E+00  1.00000000E+00  2.06441100E-02  0.00000000E+00  0.00000000E+00

************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 1


                Spin  tracking  requested.


                          Particle  mass          =    938.2721     MeV/c2
                          Gyromagnetic  factor  G =    1.792847    

                          Initial spin conditions type  2 :
                               All particles have spin parallel to  Y  AXIS

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO    =        64.624 kG*cm
                               beta    =    0.02064411
                               gamma   =   1.00021316
                               gamma*G =   1.7932295094


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        1  PARTICULES :
                               <SX> =     0.000000
                               <SY> =     1.000000
                               <SZ> =     0.000000
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 1


                OPEN FILE zgoubi.fai                                                                      
                FOR PRINTING COORDINATES 

               Print will occur at element[s] labeled : 


************************************************************************************************************************************
      5  Keyword, label(s) :  MARKER      #S_180degSectorUnifB                                                         IPASS= 1


************************************************************************************************************************************
      6  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


                OPEN FILE zgoubi.plt                                                                      
                FOR PRINTING TRAJECTORIES

                    Dipole  magnet

           ANGLES : A.TOTAL =  1.8000E+02 degrees     A.CENTRAL =  9.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  90.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -90.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 2
                     9-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :   1.000     cm   (i.e.,   2.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            3.142    12.925    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (       42.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    1.57079633     m ;  Time  (for ref. rigidity & particle) =   2.538067E-07 s 

************************************************************************************************************************************
      7  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

m  1   1.0000    12.925     0.000     0.000     0.000      0.0000    0.0000   12.925   -0.000    0.000    0.000   4.060469E+01     1
               Time of flight (mus) :  6.56083798E-02 mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   1.292487E-01  -1.214263E-07        1        1    1.000      (Y,T)         1
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)         1
   0.0000E+00   0.0000E+00   1.0000E+00   6.560838E-02   2.000000E-01        1        1    1.000      (t,K)         1

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)
      normalized (*beta*gamma) :     0.000000E+00    0.000000E+00

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                                                   IPASS= 1

               Accelerating cavity. Type is :   Isochron. 

                    Cavity  frequency                 =   2.286293E+07 Hz
                    Harmonic                          =   1.000000E+00 
                    Max energy  gain                  =   2.850000E-01 MeV
                    Cumulated distance since origin   =   1.570796E+00 m
                    Cumulated   TOF      "     "      =   2.538067E-13 s
                    Particle mass                     =   9.382721E+02 MeV/c2
                             charge                   =   1.602176E-19 C

************************************************************************************************************************************
     10  Keyword, label(s) :  MARKER      #S_180degSectorUnifB                                                         IPASS= 1


************************************************************************************************************************************
     11  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  1.8000E+02 degrees     A.CENTRAL =  9.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  90.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -90.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 2
                     9-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :   1.000     cm   (i.e.,   2.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            3.142    20.904     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (       54.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    3.14159265     m ;  Time  (for ref. rigidity & particle) =   5.076133E-07 s 

************************************************************************************************************************************
     12  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
     13  Keyword, label(s) :  CAVITE                                                                                   IPASS= 1

               Accelerating cavity. Type is :   Isochron. 

                    Cavity  frequency                 =   2.286293E+07 Hz
                    Harmonic                          =   1.000000E+00 
                    Max energy  gain                  =   2.850000E-01 MeV
                    Cumulated distance since origin   =   3.141593E+00 m
                    Cumulated   TOF      "     "      =   5.076133E-13 s
                    Particle mass                     =   9.382721E+02 MeV/c2
                             charge                   =   1.602176E-19 C

************************************************************************************************************************************
     14  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 1


                                -----  REBELOTE  -----

     End of pass #        1 through the optical structure 

                     Total of          1 particles have been launched

     Multiple pass, 
          from element #     1 : OBJET     /label1=                    /label2=                    
                             to  REBELOTE  /label1=                    /label2=                    
     ending at pass #      27 at element #    14 : REBELOTE  /label1=                    /label2=                    


     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

                                -----  REBELOTE  -----

     End of pass #        2 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #        3 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #        4 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #        5 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #        6 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #        7 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #        8 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #        9 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       10 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       11 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       12 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       13 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       14 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       15 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       16 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       17 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       18 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       19 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       20 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       21 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       22 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       23 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       24 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       25 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       26 through the optical structure 

                     Total of          1 particles have been launched


      Next  pass  is  #    27 and  last  pass  through  the  optical  structure


************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 27



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 27


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 27


               Final  spins  of  last  run  taken  as  initial  spins.

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 27


************************************************************************************************************************************
      5  Keyword, label(s) :  MARKER      #S_180degSectorUnifB                                                         IPASS= 27


************************************************************************************************************************************
      6  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 27


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  1.8000E+02 degrees     A.CENTRAL =  9.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  90.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -90.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 2
                     9-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :   1.000     cm   (i.e.,   2.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            3.142    29.534    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      103.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    1.57079633     m ;  Time  (for ref. rigidity & particle) =   1.345175E-05 s 

************************************************************************************************************************************
      7  Keyword, label(s) :  MARKER                                                                                   IPASS= 27


************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 27

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

m  1   1.0000    12.925     0.000     0.000     0.000      0.0000    1.5244   29.534   -0.000    0.000    0.000   6.805396E+03     1
               Time of flight (mus) :   3.4842774     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   2.953375E-01  -4.315801E-08        1        1    1.000      (Y,T)        27
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        27
   0.0000E+00   0.0000E+00   1.0000E+00   3.484277E+00   1.273757E+00        1        1    1.000      (t,K)        27

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)
      normalized (*beta*gamma) :     0.000000E+00    0.000000E+00

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                                                   IPASS= 27

               Accelerating cavity. Type is :   Isochron. 

                    Cavity  frequency                 =   2.286293E+07 Hz
                    Harmonic                          =   1.000000E+00 
                    Max energy  gain                  =   2.850000E-01 MeV
                    Cumulated distance since origin   =   1.570796E+00 m
                    Cumulated   TOF      "     "      =   1.345175E-11 s
                    Particle mass                     =   9.382721E+02 MeV/c2
                             charge                   =   1.602176E-19 C

************************************************************************************************************************************
     10  Keyword, label(s) :  MARKER      #S_180degSectorUnifB                                                         IPASS= 27


************************************************************************************************************************************
     11  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 27


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  1.8000E+02 degrees     A.CENTRAL =  9.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  90.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -90.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 2
                     9-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :   1.000     cm   (i.e.,   2.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            3.142    32.209     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (       98.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    3.14159265     m ;  Time  (for ref. rigidity & particle) =   1.370556E-05 s 

************************************************************************************************************************************
     12  Keyword, label(s) :  MARKER                                                                                   IPASS= 27


************************************************************************************************************************************
     13  Keyword, label(s) :  CAVITE                                                                                   IPASS= 27

               Accelerating cavity. Type is :   Isochron. 

                    Cavity  frequency                 =   2.286293E+07 Hz
                    Harmonic                          =   1.000000E+00 
                    Max energy  gain                  =   2.850000E-01 MeV
                    Cumulated distance since origin   =   3.141593E+00 m
                    Cumulated   TOF      "     "      =   1.370556E-11 s
                    Particle mass                     =   9.382721E+02 MeV/c2
                             charge                   =   1.602176E-19 C

************************************************************************************************************************************
     14  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 27


                         >>>>  End  of  'REBELOTE'  procedure  <<<<

      There  has  been         27  passes  through  the  optical  structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
     15  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 28

0                                             TRACE DU FAISCEAU
                                           (follows element #     14)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

m  1   1.0000    12.925     0.000     0.000     0.000      0.0000    1.2419   32.209    0.000    0.000    0.000   6.902381E+03     1
               Time of flight (mus) :   3.5499516     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   3.220919E-01   4.751856E-08        1        1    1.000      (Y,T)        28
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        28
   0.0000E+00   0.0000E+00   1.0000E+00   3.549952E+00   1.004752E+00        1        1    1.000      (t,K)        28

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)
      normalized (*beta*gamma) :     0.000000E+00    0.000000E+00

************************************************************************************************************************************
     16  Keyword, label(s) :  END                                                                                      IPASS= 28


************************************************************************************************************************************
 Pgm zgoubi : Execution ended normally, upon keyword END or FIN
   
 File in:   zgoubi_detuned_h3.dat
 File out:  zgoubi.res

  Zgoubi, author's dvlpmnt version.
  Job  started  on  19-05-2020,  at  09:00:42 
  JOB  ENDED  ON    19-05-2020,  AT  09:00:42 

   CPU time, total :    0.19030799999999998     
