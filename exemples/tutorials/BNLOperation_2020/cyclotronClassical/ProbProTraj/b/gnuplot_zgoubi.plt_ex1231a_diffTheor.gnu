set xtics mirror; set x2tics; set ytics nomirror; set y2tics nomirror; set logscale y2
set xlabel 's  /circumference'; set ylabel 'Y  [cm]'; set y2label 'Y_{num.} - Y_{theor}  [cm]'
cm2m = 0.01; pi=4.*atan(1.); dev=2.*pi/6. ; numDip1=3; numDip2=13; dN=2
R=12.9248888*cm2m; trajNb = 1; Circ=2.*pi*R; k=-0.03; Z0=0.1; z(x) = Z0*cos(sqrt(-k) * x*Circ/R)
plot [:] [-0.11:0.11] \
for [numEl=numDip1:numDip2:dN] 'zgoubi.plt' u ($42==numEl && $19==trajNb? $14*cm2m /Circ:1/0):($12) \
w p ps .9  notit , z(x) w l lw 2 lc rgb "black" ,\
for [numEl=numDip1:numDip2:dN] 'zgoubi.plt' u ($42==numEl && $19==trajNb? $14*cm2m /Circ:1/0): \
($12 -z($14*cm2m/Circ)) axes x1y2 w p ps .2 lc rgb "black" notit ; pause 1

     set terminal postscript eps blacktext color  enh 
       set output "gnuplot_zgoubi.plt_ex1231a_diffTheor.eps"  
       replot  
       set terminal X11  
       unset output  
exit

