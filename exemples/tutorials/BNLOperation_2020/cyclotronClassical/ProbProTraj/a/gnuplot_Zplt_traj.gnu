set xtics nomirror; set x2tics; set ytics; set xlabel 's  /C(E) '; set ylabel 'Y  [cm]'
set palette defined ( 1 "red", 2 "blue", 3 "black" ) ; unset colorbox
array R[3]; R[1]=0.12924888; R[2]=0.301078986; R[3]=0.75754671; pi = 4.*atan(1.); cm2m = 0.01
secotr1=3    # number (NOEL) of 1st DIPOLE in zgoubi,res (col. 42 in zgoubi.plt) 
# in zgoubi.plt, col. 19: particle number; col. 42: keyword number; col. 14: distance; col. 10: Y ; col. 12: YZ
plot  for [i=1:6] for [p=1:3] 'zgoubi.plt' u ($19==p && $42==secotr1 +2*(i-1) ? $14*cm2m /(2.*pi*R[$19]) :1/0) \
:($10*cm2m-R[p]):($19) w p ps .2 lc palette notit ; pause 1

     set terminal postscript eps blacktext color  enh
       set output "gnuplot_Zplt_trajY.eps"  
       replot  
       set terminal X11  
       unset output  
 
pause 1

set ylabel 'Z  [cm]'; plot  for [i=1:6] for [p=1:3] 'zgoubi.plt' u ($19==p && $42==secotr1 +2*(i-1) ? $14*cm2m \
/(2.*pi*R[$19]) :1/0):($12):($19) w p ps .2 lc palette notit ; pause 1
      
     set terminal postscript eps blacktext color  enh
       set output "gnuplot_Zplt_trajZ.eps"  
       replot  
       set terminal X11  
       unset output  
 
pause 1
exit
