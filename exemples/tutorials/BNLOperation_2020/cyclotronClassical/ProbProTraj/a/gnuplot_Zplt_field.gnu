set xtics nomirror; set x2tics; set ytics; set xlabel 'R  [m]'; set ylabel 'B_Z  [kG]'
cm2m=0.1; secotr1=3    # number (NOEL) of 1st DIPOLE in zgoubi,res (col. 42 in zgoubi.plt)
plot  for [p=1:3] 'zgoubi.plt' u ($19==p ? $10*cm2m :1/0):($25) w l lw 2 notit

     set terminal postscript eps blacktext color  enh
       set output "gnuplot_Zplt_BZ.eps"  
       replot  
       set terminal X11  
       unset output  
 
pause 1

exit
