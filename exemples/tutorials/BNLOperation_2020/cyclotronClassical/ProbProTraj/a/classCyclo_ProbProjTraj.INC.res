Test field map of a 60 cyclotron sector, with radial index
 
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./projTraj.inc[ProbProjTraj_S,*:ProbProjTraj(n_inc, depth :  1 2)
'MARKER'   ProbProjTraj_S                                                                                    1
'OBJET'                                                                                                      2
64.62444403717985                      ! Reference Brho ("BORO" in the users' guide) -> 200keV proton.
2
3 1
12.924888 0.1 0. 0.1 0. 1.        'o' ! Reference: p[MeV/c]=193.739, Brho[kG.cm]=BORO, kin-E[MeV]=0.2.
30.107898 0.1 0. 0.1 0. 2.2365445 'm'           ! p[MeV/c]=433.306, Brho[kG.cm]=144.535, kin-E[MeV]=1.
75.754671 0.1 0. 0.1 0. 5.0063900 'o'           ! p[MeV/c]=969.934, Brho[kG.cm]=323.535, kin-E[MeV]=5.
1 1 1
! 'INCLUDE'                                                                                               
! Include_SegmentStart : 60DegSectorR200.inc[#S_60DegSectorR200,*:#E_(n_inc, depth : *1 3)
'DIPOLE'   #S_60DegSectorR200                               ! Analytical definition of a dipole field.       3
2          ! IL=2, purpose: log stepwise particle data to zgoubi.plt. Avoid if unused: takes CPU time.
60. 12.924888                                                  ! Sector angle AT; reference radius RM.
30.  5. -0.03  0. 0.             ! Reference azimuthal angle ACN; BM field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : 60DegSectorR200.inc                           (had n_inc, depth :  1 3)
'FAISCEAU'  #E_60DegSectorR200                                                                               4
! 'INCLUDE'                                                                                               
! Include_SegmentStart : 60DegSectorR200.inc[#S_60DegSectorR200,*:#E_(n_inc, depth : *1 3)
'DIPOLE'   #S_60DegSectorR200                               ! Analytical definition of a dipole field.       5
2          ! IL=2, purpose: log stepwise particle data to zgoubi.plt. Avoid if unused: takes CPU time.
60. 12.924888                                                  ! Sector angle AT; reference radius RM.
30.  5. -0.03  0. 0.             ! Reference azimuthal angle ACN; BM field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : 60DegSectorR200.inc                           (had n_inc, depth :  1 3)
'FAISCEAU'  #E_60DegSectorR200                                                                               6
! 'INCLUDE'                                                                                               
! Include_SegmentStart : 60DegSectorR200.inc[#S_60DegSectorR200,*:#E_(n_inc, depth : *1 3)
'DIPOLE'   #S_60DegSectorR200                               ! Analytical definition of a dipole field.       7
2          ! IL=2, purpose: log stepwise particle data to zgoubi.plt. Avoid if unused: takes CPU time.
60. 12.924888                                                  ! Sector angle AT; reference radius RM.
30.  5. -0.03  0. 0.             ! Reference azimuthal angle ACN; BM field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : 60DegSectorR200.inc                           (had n_inc, depth :  1 3)
'FAISCEAU'  #E_60DegSectorR200                                                                               8
! 'INCLUDE'                                                                                               
! Include_SegmentStart : 60DegSectorR200.inc[#S_60DegSectorR200,*:#E_(n_inc, depth : *1 3)
'DIPOLE'   #S_60DegSectorR200                               ! Analytical definition of a dipole field.       9
2          ! IL=2, purpose: log stepwise particle data to zgoubi.plt. Avoid if unused: takes CPU time.
60. 12.924888                                                  ! Sector angle AT; reference radius RM.
30.  5. -0.03  0. 0.             ! Reference azimuthal angle ACN; BM field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : 60DegSectorR200.inc                           (had n_inc, depth :  1 3)
'FAISCEAU'  #E_60DegSectorR200                                                                              10
! 'INCLUDE'                                                                                               
! Include_SegmentStart : 60DegSectorR200.inc[#S_60DegSectorR200,*:#E_(n_inc, depth : *1 3)
'DIPOLE'   #S_60DegSectorR200                               ! Analytical definition of a dipole field.      11
2          ! IL=2, purpose: log stepwise particle data to zgoubi.plt. Avoid if unused: takes CPU time.
60. 12.924888                                                  ! Sector angle AT; reference radius RM.
30.  5. -0.03  0. 0.             ! Reference azimuthal angle ACN; BM field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : 60DegSectorR200.inc                           (had n_inc, depth :  1 3)
'FAISCEAU'  #E_60DegSectorR200                                                                              12
! 'INCLUDE'                                                                                               
! Include_SegmentStart : 60DegSectorR200.inc[#S_60DegSectorR200,*:#E_(n_inc, depth : *1 3)
'DIPOLE'   #S_60DegSectorR200                               ! Analytical definition of a dipole field.      13
2          ! IL=2, purpose: log stepwise particle data to zgoubi.plt. Avoid if unused: takes CPU time.
60. 12.924888                                                  ! Sector angle AT; reference radius RM.
30.  5. -0.03  0. 0.             ! Reference azimuthal angle ACN; BM field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : 60DegSectorR200.inc                           (had n_inc, depth :  1 3)
'FAISCEAU'  #E_60DegSectorR200                                                                              14
'REBELOTE'                                                                                                  15
9 0.1 99                                                     ! There will be a total of 9+1=10 tunrs.
'SYSTEM'                                                                                                    16
1
gnuplot < ./gnuplot_Zplt_traj.gnu                          ! Plot the projected Y(s) and Z(s) motions.
! Include_SegmentEnd : ./projTraj.inc                                (had n_inc, depth :  1 2)
'MARKER'   ProbProjTraj_E                                                                                   17
 
'END'

************************************************************************************************************************************
      1  Keyword, label(s) :  MARKER      ProbProjTraj_S                                                               IPASS= 1


************************************************************************************************************************************
      2  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      3  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 1


                OPEN FILE zgoubi.plt                                                                      
                FOR PRINTING TRAJECTORIES

                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.100     0.000     0.100            1.047    12.926     0.000     0.001     0.000            1
  A    1  2.2365    30.108     0.100     0.000     0.100            1.047    30.111     0.000     0.003     0.000            2
  A    1  5.0064    75.755     0.100     0.000     0.100            1.047    75.762     0.000     0.008     0.000            3


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.135349111     m ;  Time  (for ref. rigidity & particle) =            NaN s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000    12.925     0.100     0.000     0.100      0.0000    0.0000   12.926    0.051    0.001    0.098   1.353556E+01     1
m  1   2.2365    30.108     0.100     0.000     0.100      0.0000    1.2365   30.111    0.053    0.003    0.096   3.153043E+01     2
o  1   5.0064    75.755     0.100     0.000     0.100      0.0000    4.0064   75.762    0.060    0.008    0.089   7.933397E+01     3


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   9.2708E-08  -3.1366E+01   2.3825E+06   3.959935E-01   5.472957E-05        3        3    1.000      (Y,T)         1
   1.2284E-11   2.7189E+01   1.7959E+02   4.031880E-05   9.443908E-05        3        3    1.000      (Z,P)         1
   3.6213E-03  -2.6056E+01   7.4416E-04   1.383179E-03   5.323265E+01        3        3    1.000      (t,K)         1

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   2.651544E-01
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   3.492565E-06

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   2.649935E-05
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   4.014637E-06

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   9.261652E-04
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   3.245236E+01


  Beam  sigma  matrix  and  determinants : 

   7.030685E-02   9.255987E-07   7.026215E-06  -1.064047E-06
   9.255987E-07   1.219801E-11   9.247853E-11  -1.402133E-11
   7.026215E-06   9.247853E-11   7.022157E-10  -1.063134E-10
  -1.064047E-06  -1.402133E-11  -1.063134E-10   1.611731E-11

      sqrt(det_Y), sqrt(det_Z) :     2.950976E-08    3.910189E-12    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      5  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.100     0.000     0.100            1.047    12.926    -0.000     0.003     0.000            1
  A    1  2.2365    30.108     0.100     0.000     0.100            1.047    30.111    -0.000     0.006     0.000            2
  A    1  5.0064    75.755     0.100     0.000     0.100            1.047    75.763    -0.000     0.014     0.000            3


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.270698221     m ;  Time  (for ref. rigidity & particle) =            NaN s 

************************************************************************************************************************************
      6  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #      5)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000    12.925     0.100     0.000     0.100      0.0000    0.0000   12.926   -0.047    0.003    0.093   2.707178E+01     1
m  1   2.2365    30.108     0.100     0.000     0.100      0.0000    1.2365   30.111   -0.043    0.006    0.084   6.306248E+01     2
o  1   5.0064    75.755     0.100     0.000     0.100      0.0000    4.0064   75.763   -0.029    0.014    0.058   1.586725E+02     3


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   3.1065E-07  -2.0795E+01   7.1103E+05   3.959986E-01  -3.984134E-05        3        3    1.000      (Y,T)         1
   1.1366E-10   1.8944E+01   5.7909E+01   7.402868E-05   7.869584E-05        3        3    1.000      (Z,P)         1
   7.2429E-03  -2.6055E+01   1.4883E-03   2.766434E-03   5.323265E+01        3        3    1.000      (t,K)         1

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   2.651600E-01
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   7.763788E-06

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   4.577308E-05
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   1.499497E-05

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   1.852386E-03
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   3.245236E+01


  Beam  sigma  matrix  and  determinants : 

   7.030983E-02   2.056270E-06   1.213035E-05  -3.975337E-06
   2.056270E-06   6.027640E-11   3.541893E-10  -1.163692E-10
   1.213035E-05   3.541893E-10   2.095175E-09  -6.854119E-10
  -3.975337E-06  -1.163692E-10  -6.854119E-10   2.248493E-10

      sqrt(det_Y), sqrt(det_Z) :     9.888389E-08    3.618021E-11    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      7  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.100     0.000     0.100            1.047    12.925    -0.000     0.004     0.000            1
  A    1  2.2365    30.108     0.100     0.000     0.100            1.047    30.108    -0.000     0.008     0.000            2
  A    1  5.0064    75.755     0.100     0.000     0.100            1.047    75.758    -0.000     0.017     0.000            3


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.406047332     m ;  Time  (for ref. rigidity & particle) =            NaN s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000    12.925     0.100     0.000     0.100      0.0000    0.0000   12.925   -0.100    0.004    0.086   4.060740E+01     1
m  1   2.2365    30.108     0.100     0.000     0.100      0.0000    1.2365   30.108   -0.099    0.008    0.066   9.459322E+01     2
o  1   5.0064    75.755     0.100     0.000     0.100      0.0000    4.0064   75.758   -0.094    0.017    0.014   2.380088E+02     3


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   3.7460E-07  -5.5938E+00   5.8958E+05   3.959691E-01  -9.780791E-05        3        3    1.000      (Y,T)         1
   4.5215E-10   1.0873E+01   1.9067E+01   9.583523E-05   5.540399E-05        3        3    1.000      (Z,P)         1
   1.0865E-02  -2.6054E+01   2.2324E-03   4.149643E-03   5.323265E+01        3        3    1.000      (t,K)         1

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   2.651426E-01
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   2.555482E-06

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   5.238510E-05
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   3.000009E-05

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   2.778585E-03
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   3.245236E+01


  Beam  sigma  matrix  and  determinants : 

   7.030060E-02   6.669928E-07   1.383227E-05  -7.954299E-06
   6.669928E-07   6.530487E-12   1.291001E-10  -7.548014E-11
   1.383227E-05   1.291001E-10   2.744199E-09  -1.564953E-09
  -7.954299E-06  -7.548014E-11  -1.564953E-09   9.000054E-10

      sqrt(det_Y), sqrt(det_Z) :     1.192383E-07    1.439250E-10    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.100     0.000     0.100            1.047    12.924    -0.000     0.005     0.000            1
  A    1  2.2365    30.108     0.100     0.000     0.100            1.047    30.105    -0.000     0.010     0.000            2
  A    1  5.0064    75.755     0.100     0.000     0.100            1.047    75.750    -0.000     0.016    -0.000            3


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.541396443     m ;  Time  (for ref. rigidity & particle) =            NaN s 

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000    12.925     0.100     0.000     0.100      0.0000    0.0000   12.924   -0.055    0.005    0.075   5.414171E+01     1
m  1   2.2365    30.108     0.100     0.000     0.100      0.0000    1.2365   30.105   -0.063    0.010    0.043   1.261210E+02     2
o  1   5.0064    75.755     0.100     0.000     0.100      0.0000    4.0064   75.750   -0.083    0.016   -0.032   3.173379E+02     3


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   4.4549E-08   2.1870E+02   4.9566E+06   3.959306E-01  -6.706067E-05        3        3    1.000      (Y,T)         1
   1.2240E-09   5.0075E+00   5.0357E+00   1.028064E-04   2.836033E-05        3        3    1.000      (Z,P)         1
   1.4487E-02  -2.6053E+01   2.9764E-03   5.532723E-03   5.323265E+01        3        3    1.000      (t,K)         1

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   2.651153E-01
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   1.169766E-05

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   4.429501E-05
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   4.491657E-05

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   3.704700E-03
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   3.245236E+01


  Beam  sigma  matrix  and  determinants : 

   7.028612E-02  -3.101196E-06   1.144470E-05  -1.190311E-05
  -3.101196E-06   1.368352E-10  -5.044374E-10   5.251248E-10
   1.144470E-05  -5.044374E-10   1.962048E-09  -1.951057E-09
  -1.190311E-05   5.251248E-10  -1.951057E-09   2.017499E-09

      sqrt(det_Y), sqrt(det_Z) :     1.418044E-08    3.896243E-10    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.100     0.000     0.100            1.047    12.924     0.000     0.006     0.000            1
  A    1  2.2365    30.108     0.100     0.000     0.100            1.047    30.105     0.000     0.011     0.000            2
  A    1  5.0064    75.755     0.100     0.000     0.100            1.047    75.746    -0.000     0.012    -0.000            3


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.676745553     m ;  Time  (for ref. rigidity & particle) =            NaN s 

************************************************************************************************************************************
     12  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #     11)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000    12.925     0.100     0.000     0.100      0.0000    0.0000   12.924    0.043    0.006    0.062   6.767531E+01     1
m  1   2.2365    30.108     0.100     0.000     0.100      0.0000    1.2365   30.105    0.032    0.011    0.016   1.576468E+02     2
o  1   5.0064    75.755     0.100     0.000     0.100      0.0000    4.0064   75.746   -0.005    0.012   -0.072   3.966605E+02     3


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   8.9339E-07   1.9017E+01   2.4713E+05   3.959161E-01   2.357858E-05        3        3    1.000      (Y,T)         1
   2.5776E-09   1.4167E+00   8.0185E-01   9.490537E-05   1.764493E-06        3        3    1.000      (Z,P)         1
   1.8108E-02  -2.6054E+01   3.7204E-03   6.915702E-03   5.323265E+01        3        3    1.000      (t,K)         1

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   2.650992E-01
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   2.042793E-05

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   2.564926E-05
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   5.546951E-05

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   4.630734E-03
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   3.245236E+01


  Beam  sigma  matrix  and  determinants : 

   7.027757E-02  -5.407957E-06   5.238839E-06  -1.466183E-05
  -5.407957E-06   4.173005E-10  -3.855961E-10   1.123696E-09
   5.238839E-06  -3.855961E-10   6.578845E-10  -1.162352E-09
  -1.466183E-05   1.123696E-09  -1.162352E-09   3.076867E-09

      sqrt(det_Y), sqrt(det_Z) :     2.843746E-07    8.204634E-10    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     13  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.100     0.000     0.100            1.047    12.925     0.000     0.007     0.000            1
  A    1  2.2365    30.108     0.100     0.000     0.100            1.047    30.107     0.000     0.011    -0.000            2
  A    1  5.0064    75.755     0.100     0.000     0.100            1.047    75.749     0.000     0.005    -0.000            3


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.812094664     m ;  Time  (for ref. rigidity & particle) =            NaN s 

************************************************************************************************************************************
     14  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #     13)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000    12.925     0.100     0.000     0.100      0.0000    0.0000   12.925    0.100    0.007    0.046   8.120947E+01     1
m  1   2.2365    30.108     0.100     0.000     0.100      0.0000    1.2365   30.107    0.097    0.011   -0.012   1.891736E+02     2
o  1   5.0064    75.755     0.100     0.000     0.100      0.0000    4.0064   75.749    0.078    0.005   -0.096   4.759828E+02     3


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   1.4336E-06   5.6671E+00   1.5402E+05   3.959375E-01   9.145149E-05        3        3    1.000      (Y,T)         1
   4.4854E-09  -4.0544E-01   4.8840E-01   7.490475E-05  -2.060044E-05        3        3    1.000      (Z,P)         1
   2.1727E-02  -2.6055E+01   4.4646E-03   8.298695E-03   5.323265E+01        3        3    1.000      (t,K)         1

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   2.651071E-01
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   9.905371E-06

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   2.640673E-05
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   5.834281E-05

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   5.556752E-03
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   3.245236E+01


  Beam  sigma  matrix  and  determinants : 

   7.028179E-02  -2.586033E-06  -3.608130E-06  -1.527858E-05
  -2.586033E-06   9.811638E-11   1.717129E-10   5.465474E-10
  -3.608130E-06   1.717129E-10   6.973154E-10   5.788734E-10
  -1.527858E-05   5.465474E-10   5.788734E-10   3.403883E-09

      sqrt(det_Y), sqrt(det_Z) :     4.563220E-07    1.427755E-09    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     15  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 1


                                -----  REBELOTE  -----

     End of pass #        1 through the optical structure 

                     Total of          3 particles have been launched

     Multiple pass, 
          from element #     1 : MARKER    /label1=ProbProjTraj_S      /label2=                    
                             to  REBELOTE  /label1=ProbProjTraj_S      /label2=                    
     ending at pass #      10 at element #    15 : REBELOTE  /label1=                    /label2=                    


     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

                                -----  REBELOTE  -----

     End of pass #        2 through the optical structure 

                     Total of          3 particles have been launched

                                -----  REBELOTE  -----

     End of pass #        3 through the optical structure 

                     Total of          3 particles have been launched

                                -----  REBELOTE  -----

     End of pass #        4 through the optical structure 

                     Total of          3 particles have been launched

                                -----  REBELOTE  -----

     End of pass #        5 through the optical structure 

                     Total of          3 particles have been launched

                                -----  REBELOTE  -----

     End of pass #        6 through the optical structure 

                     Total of          3 particles have been launched

                                -----  REBELOTE  -----

     End of pass #        7 through the optical structure 

                     Total of          3 particles have been launched

                                -----  REBELOTE  -----

     End of pass #        8 through the optical structure 

                     Total of          3 particles have been launched

                                -----  REBELOTE  -----

     End of pass #        9 through the optical structure 

                     Total of          3 particles have been launched


      Next  pass  is  #    10 and  last  pass  through  the  optical  structure


************************************************************************************************************************************
      1  Keyword, label(s) :  MARKER      ProbProjTraj_S                                                               IPASS= 10


************************************************************************************************************************************
      2  Keyword, label(s) :  OBJET                                                                                    IPASS= 10



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         3 particles

************************************************************************************************************************************
      3  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 10


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.100     0.000     0.100            1.047    12.925     0.000    -0.004    -0.000            1
  A    1  2.2365    30.108     0.100     0.000     0.100            1.047    30.105     0.000     0.002    -0.000            2
  A    1  5.0064    75.755     0.100     0.000     0.100            1.047    75.762     0.000     0.014     0.000            3


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.135349111     m ;  Time  (for ref. rigidity & particle) =            NaN s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 10

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000    12.925     0.100     0.000     0.100      0.0000    0.0000   12.925    0.098   -0.004   -0.085   7.444201E+02     1
m  1   2.2365    30.108     0.100     0.000     0.100      0.0000    1.2365   30.105    0.046    0.002   -0.099   1.734092E+03     2
o  1   5.0064    75.755     0.100     0.000     0.100      0.0000    4.0064   75.762    0.049    0.014    0.054   4.363161E+03     3


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   1.4623E-05   9.3138E-01   1.5106E+04   3.959745E-01   6.454148E-05        3        3    1.000      (Y,T)        10
   6.3247E-09  -2.3622E+00   2.7799E+00   4.004752E-05  -4.322696E-05        3        3    1.000      (Z,P)        10
   1.9916E-01  -2.6056E+01   4.0927E-02   7.607122E-02   5.323265E+01        3        3    1.000      (t,K)        10

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   2.651665E-01
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   2.398855E-05

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   7.480986E-05
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   6.903144E-05

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   5.093666E-02
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   3.245236E+01


  Beam  sigma  matrix  and  determinants : 

   7.031329E-02  -4.335326E-06   1.980858E-05   1.721470E-05
  -4.335326E-06   5.754506E-10  -1.291699E-09  -6.494730E-10
   1.980858E-05  -1.291699E-09   5.596515E-09   4.755651E-09
   1.721470E-05  -6.494730E-10   4.755651E-09   4.765339E-09

      sqrt(det_Y), sqrt(det_Z) :     4.654758E-06    2.013224E-09    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      5  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 10


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.100     0.000     0.100            1.047    12.926     0.000    -0.005    -0.000            1
  A    1  2.2365    30.108     0.100     0.000     0.100            1.047    30.108     0.000    -0.001    -0.000            2
  A    1  5.0064    75.755     0.100     0.000     0.100            1.047    75.762    -0.000     0.017     0.000            3


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.270698221     m ;  Time  (for ref. rigidity & particle) =            NaN s 

************************************************************************************************************************************
      6  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 10

0                                             TRACE DU FAISCEAU
                                           (follows element #      5)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000    12.925     0.100     0.000     0.100      0.0000    0.0000   12.926    0.035   -0.005   -0.074   7.579559E+02     1
m  1   2.2365    30.108     0.100     0.000     0.100      0.0000    1.2365   30.108    0.100   -0.001   -0.099   1.765619E+03     2
o  1   5.0064    75.755     0.100     0.000     0.100      0.0000    4.0064   75.762   -0.041    0.017    0.010   4.442499E+03     3


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   3.2352E-05   1.0931E+00   6.8276E+03   3.959873E-01   3.134923E-05        3        3    1.000      (Y,T)        10
   5.1598E-09  -2.4829E+00   5.4163E+00   3.455502E-05  -5.469798E-05        3        3    1.000      (Z,P)        10
   2.0279E-01  -2.6055E+01   4.1670E-02   7.745441E-02   5.323265E+01        3        3    1.000      (t,K)        10

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   2.651617E-01
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   5.753689E-05

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   9.431783E-05
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   4.661164E-05

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   5.186291E-02
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   3.245236E+01


  Beam  sigma  matrix  and  determinants : 

   7.031072E-02  -1.125675E-05   2.487010E-05   1.091401E-05
  -1.125675E-05   3.310494E-09  -4.367881E-09  -2.596884E-09
   2.487010E-05  -4.367881E-09   8.895853E-09   4.077988E-09
   1.091401E-05  -2.596884E-09   4.077988E-09   2.172645E-09

      sqrt(det_Y), sqrt(det_Z) :     1.029800E-05    1.642422E-09    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      7  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 10


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.100     0.000     0.100            1.047    12.926    -0.000    -0.006    -0.000            1
  A    1  2.2365    30.108     0.100     0.000     0.100            1.047    30.110     0.000    -0.004    -0.000            2
  A    1  5.0064    75.755     0.100     0.000     0.100            1.047    75.756    -0.000     0.016    -0.000            3


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.406047332     m ;  Time  (for ref. rigidity & particle) =            NaN s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 10

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000    12.925     0.100     0.000     0.100      0.0000    0.0000   12.926   -0.062   -0.006   -0.061   7.714921E+02     1
m  1   2.2365    30.108     0.100     0.000     0.100      0.0000    1.2365   30.110    0.060   -0.004   -0.092   1.797150E+03     2
o  1   5.0064    75.755     0.100     0.000     0.100      0.0000    4.0064   75.756   -0.098    0.016   -0.037   4.521835E+03     3


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   5.0035E-05   5.1225E-01   4.4137E+03   3.959760E-01  -3.327090E-05        3        3    1.000      (Y,T)        10
   4.2745E-09  -1.2752E+00   6.9755E+00   1.766645E-05  -6.343944E-05        3        3    1.000      (Z,P)        10
   2.0641E-01  -2.6055E+01   4.2414E-02   7.883761E-02   5.323265E+01        3        3    1.000      (t,K)        10

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   2.651321E-01
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   6.749345E-05

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   9.742237E-05
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   2.263242E-05

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   5.278909E-02
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   3.245236E+01


  Beam  sigma  matrix  and  determinants : 

   7.029501E-02  -8.158469E-06   2.531270E-05   3.890061E-06
  -8.158469E-06   4.555365E-09  -4.102931E-09  -1.486642E-09
   2.531270E-05  -4.102931E-09   9.491117E-09   1.735019E-09
   3.890061E-06  -1.486642E-09   1.735019E-09   5.122263E-10

      sqrt(det_Y), sqrt(det_Z) :     1.592667E-05    1.360628E-09    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 10


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.100     0.000     0.100            1.047    12.925    -0.000    -0.007    -0.000            1
  A    1  2.2365    30.108     0.100     0.000     0.100            1.047    30.111    -0.000    -0.007    -0.000            2
  A    1  5.0064    75.755     0.100     0.000     0.100            1.047    75.749    -0.000     0.011    -0.000            3


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.541396443     m ;  Time  (for ref. rigidity & particle) =            NaN s 

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 10

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000    12.925     0.100     0.000     0.100      0.0000    0.0000   12.925   -0.099   -0.007   -0.046   7.850275E+02     1
m  1   2.2365    30.108     0.100     0.000     0.100      0.0000    1.2365   30.111   -0.036   -0.007   -0.078   1.828682E+03     2
o  1   5.0064    75.755     0.100     0.000     0.100      0.0000    4.0064   75.749   -0.075    0.011   -0.075   4.601163E+03     3


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   2.1597E-05  -1.2758E-01   1.0223E+04   3.959487E-01  -7.006293E-05        3        3    1.000      (Y,T)        10
   3.4800E-09   4.6636E-01   6.3507E+00  -8.893564E-06  -6.634189E-05        3        3    1.000      (Z,P)        10
   2.1003E-01  -2.6056E+01   4.3159E-02   8.022074E-02   5.323265E+01        3        3    1.000      (t,K)        10

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   2.651019E-01
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   2.614133E-05

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   8.387391E-05
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   1.457264E-05

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   5.371516E-02
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   3.245236E+01


  Beam  sigma  matrix  and  determinants : 

   7.027902E-02   8.770193E-07   2.133321E-05  -2.553670E-06
   8.770193E-07   6.833690E-10  -3.469493E-10  -3.154210E-10
   2.133321E-05  -3.469493E-10   7.034832E-09  -5.166008E-10
  -2.553670E-06  -3.154210E-10  -5.166008E-10   2.123617E-10

      sqrt(det_Y), sqrt(det_Z) :     6.874397E-06    1.107724E-09    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 10


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.100     0.000     0.100            1.047    12.924    -0.000    -0.007    -0.000            1
  A    1  2.2365    30.108     0.100     0.000     0.100            1.047    30.109    -0.000    -0.009    -0.000            2
  A    1  5.0064    75.755     0.100     0.000     0.100            1.047    75.746     0.000     0.004    -0.000            3


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.676745553     m ;  Time  (for ref. rigidity & particle) =            NaN s 

************************************************************************************************************************************
     12  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 10

0                                             TRACE DU FAISCEAU
                                           (follows element #     11)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000    12.925     0.100     0.000     0.100      0.0000    0.0000   12.924   -0.040   -0.007   -0.029   7.985616E+02     1
m  1   2.2365    30.108     0.100     0.000     0.100      0.0000    1.2365   30.109   -0.098   -0.009   -0.057   1.860213E+03     2
o  1   5.0064    75.755     0.100     0.000     0.100      0.0000    4.0064   75.746    0.008    0.004   -0.097   4.680485E+03     3


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   2.7100E-05  -8.8353E-01   8.1466E+03   3.959280E-01  -4.326654E-05        3        3    1.000      (Y,T)        10
   2.7306E-09   1.5664E+00   3.8472E+00  -4.097476E-05  -6.111950E-05        3        3    1.000      (Z,P)        10
   2.1364E-01  -2.6057E+01   4.3905E-02   8.160378E-02   5.323265E+01        3        3    1.000      (t,K)        10

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   2.650951E-01
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   4.342196E-05

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   5.782667E-05
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   2.793328E-05

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   5.464116E-02
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   3.245236E+01


  Beam  sigma  matrix  and  determinants : 

   7.027539E-02   7.621594E-06   1.405946E-05  -7.312447E-06
   7.621594E-06   1.885467E-09   2.274747E-09  -6.498166E-10
   1.405946E-05   2.274747E-09   3.343924E-09  -1.361495E-09
  -7.312447E-06  -6.498166E-10  -1.361495E-09   7.802679E-10

      sqrt(det_Y), sqrt(det_Z) :     8.626309E-06    8.691889E-10    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     13  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 10


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.100     0.000     0.100            1.047    12.924     0.000    -0.007    -0.000            1
  A    1  2.2365    30.108     0.100     0.000     0.100            1.047    30.106    -0.000    -0.011    -0.000            2
  A    1  5.0064    75.755     0.100     0.000     0.100            1.047    75.750     0.000    -0.004    -0.000            3


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.812094664     m ;  Time  (for ref. rigidity & particle) =            NaN s 

************************************************************************************************************************************
     14  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 10

0                                             TRACE DU FAISCEAU
                                           (follows element #     13)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000    12.925     0.100     0.000     0.100      0.0000    0.0000   12.924    0.058   -0.007   -0.011   8.120952E+02     1
m  1   2.2365    30.108     0.100     0.000     0.100      0.0000    1.2365   30.106   -0.069   -0.011   -0.032   1.891741E+03     2
o  1   5.0064    75.755     0.100     0.000     0.100      0.0000    4.0064   75.750    0.085   -0.004   -0.097   4.759808E+03     3


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   5.1036E-05  -4.4540E-01   4.3266E+03   3.959321E-01   2.477507E-05        3        3    1.000      (Y,T)        10
   2.0888E-09   1.1313E+00   1.1291E+00  -7.280835E-05  -4.689902E-05        3        3    1.000      (Z,P)        10
   2.1726E-01  -2.6057E+01   4.4649E-02   8.298678E-02   5.323265E+01        3        3    1.000      (t,K)        10

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   2.651162E-01
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   6.707952E-05

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   2.739991E-05
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   3.663983E-05

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   5.556719E-02
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   3.245236E+01


  Beam  sigma  matrix  and  determinants : 

   7.028657E-02   7.235614E-06   5.282075E-06  -9.708590E-06
   7.235614E-06   4.499663E-09   1.696350E-09  -1.073038E-09
   5.282075E-06   1.696350E-09   7.507548E-10  -7.521961E-10
  -9.708590E-06  -1.073038E-09  -7.521961E-10   1.342477E-09

      sqrt(det_Y), sqrt(det_Z) :     1.624536E-05    6.648850E-10    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     15  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 10


                         >>>>  End  of  'REBELOTE'  procedure  <<<<

      There  has  been         10  passes  through  the  optical  structure 

                     Total of          3 particles have been launched

************************************************************************************************************************************
     16  Keyword, label(s) :  SYSTEM                                                                                   IPASS= 11

     Number  of  commands :   1,  as  follows : 

 gnuplot < ./gnuplot_Zplt_traj.gnu

************************************************************************************************************************************
     17  Keyword, label(s) :  MARKER                                                                                   IPASS= 11


************************************************************************************************************************************
     18  Keyword, label(s) :  END                                                                                      IPASS= 11


************************************************************************************************************************************
 Pgm zgoubi : Execution ended normally, upon keyword END or FIN
   
 File in:   classCyclo_ProbProjTraj.INC.dat
 File out:  zgoubi.res

  Zgoubi, author's dvlpmnt version.
  Job  started  on  28-05-2020,  at  13:47:53 
  JOB  ENDED  ON    28-05-2020,  AT  13:48:12 

   CPU time, total :    0.59132699999999994     
