Scan field B(R) across 12 -> 75 cm
 
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./projTraj.inc[ProbProjTrajB_S,*:ProbProjTra(n_inc, depth :  1 2)
'MARKER'   ProbProjTrajB_S                                                                                   1
'OBJET'                                                                                                      2
64.62444403717985                      ! Reference Brho ("BORO" in the users' guide) -> 200keV proton.
2
1 1
12.924888 0.1 0. 0.1 0. 1.        'o' ! Reference: p[MeV/c]=193.739, Brho[kG.cm]=BORO, kin-E[MeV]=0.2.
1
! 'INCLUDE'                                                                                               
! Include_SegmentStart : 60DegSectorR200.inc[#S_60DegSectorR200,*:#E_(n_inc, depth : *1 3)
'DIPOLE'   #S_60DegSectorR200                               ! Analytical definition of a dipole field.       3
2          ! IL=2, purpose: log stepwise particle data to zgoubi.plt. Avoid if unused: takes CPU time.
60. 12.924888                                                  ! Sector angle AT; reference radius RM.
30.  5. -0.03  0. 0.             ! Reference azimuthal angle ACN; BM field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : 60DegSectorR200.inc                           (had n_inc, depth :  1 3)
'FAISCEAU'  #E_60DegSectorR200                                                                               4
'FIT'                                                                                                        5
1
2 30 0 [12,80]                     ! Vary particle's Y0 at OBJET, to have it match its D (=Brho/BORO).
1  1e-20
3.1 1 2 #End 0. 1. 0                                                            ! Consrain Y_final=Y0.
'REBELOTE'                                                                                                   6
25 0.1 0 1                                                ! Scan parameter 35 (partile 1's D) in OBJT.
1
OBJET 35 1:5.0063900
'SYSTEM'                                                                                                     7
1
gnuplot < ./gnuplot_Zplt_field.gnu                               ! Plot B(R), as read fron zgoubi.plt.
! Include_SegmentEnd : ./projTraj.inc                                (had n_inc, depth :  1 2)
'MARKER'   ProbProjTrajB_E                                                                                   8
 
'END'

************************************************************************************************************************************
      1  Keyword, label(s) :  MARKER      ProbProjTrajB_S                                                              IPASS= 1


************************************************************************************************************************************
      2  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      3  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 1


                OPEN FILE zgoubi.plt                                                                      
                FOR PRINTING TRAJECTORIES

                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.100     0.000     0.100            1.047    12.926     0.000     0.001     0.000            1


                CONDITIONS  DE  MAXWELL  (       28.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.135349111     m ;  Time  (for ref. rigidity & particle) =            NaN s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000    12.925     0.100     0.000     0.100      0.0000    0.0000   12.926    0.051    0.001    0.098   1.353556E+01     1


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   1.292601E-01   5.132862E-05        1        1    1.000      (Y,T)         1
   0.0000E+00   0.0000E+00   1.0000E+00   1.346147E-05   9.835966E-05        1        1    1.000      (Z,P)         1
   0.0000E+00   0.0000E+00   1.0000E+00   4.514977E-04   1.937392E+01        1        1    1.000      (t,K)         1

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      5  Keyword, label(s) :  FIT                                                                                      IPASS= 1

     Pgm main. FITING=.T., FIT procedure launched.

           variable #            1       IR =            2 ,   ok.
           variable #            1       IP =           30 ,   ok.
           constraint #            1       IR =            4 ,   ok.
           constraint #            1       I  =            1 ,   ok.

                    FIT  variables  and  constraints  in  good  order,  FIT  will proceed. 
 STATUS OF VARIABLES  (Iteration #    13 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        12.9       12.927203       80.0      1.950E-10  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-20)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     4    0.000000E+00    1.000E+00    9.644907E-11    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   9.3024E-21



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  MARKER      ProbProjTrajB_S                                                              IPASS= 1


************************************************************************************************************************************
      2  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      3  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.927     0.100     0.000     0.100            1.047    12.927    -0.000     0.001     0.000            1


                CONDITIONS  DE  MAXWELL  (       28.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.135349111     m ;  Time  (for ref. rigidity & particle) =   2.186948E-08 s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000    12.927     0.100     0.000     0.100      0.0000    0.0000   12.927   -0.100    0.001    0.098   1.353758E+01     1


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   1.292720E-01  -1.000021E-04        1        1    1.000      (Y,T)         1
   0.0000E+00   0.0000E+00   1.0000E+00   1.346346E-05   9.835918E-05        1        1    1.000      (Z,P)         1
   0.0000E+00   0.0000E+00   1.0000E+00   4.515649E-04   1.937392E+01        1        1    1.000      (t,K)         1

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************

      5   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      6  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 1


 Pgm rebel. At pass #    1/  26.  In element #    2,  parameter # 35  changed to    1.00000000E+00   (was    1.00000000E+00)

                                -----  REBELOTE  -----

     End of pass #        1 through the optical structure 

                     Total of          1 particles have been launched

     Multiple pass, 
          from element #     1 : MARKER    /label1=ProbProjTrajB_S     /label2=                    
                             to  REBELOTE  /label1=ProbProjTrajB_S     /label2=                    
     ending at pass #      26 at element #     6 : REBELOTE  /label1=                    /label2=                    


     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     1 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        12.9       12.927203       80.0      0.227      OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-20)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     4    0.000000E+00    1.000E+00    9.644907E-11    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   9.3024E-21



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  MARKER      ProbProjTrajB_S                                                              IPASS= 2


************************************************************************************************************************************
      2  Keyword, label(s) :  OBJET                                                                                    IPASS= 2

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      3  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 2


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.927     0.100     0.000     0.100            1.047    12.927    -0.000     0.001     0.000            1


                CONDITIONS  DE  MAXWELL  (       28.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.135349111     m ;  Time  (for ref. rigidity & particle) =   2.186948E-08 s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 2

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000    12.927     0.100     0.000     0.100      0.0000    0.0000   12.927   -0.100    0.001    0.098   1.353758E+01     1


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   1.292720E-01  -1.000021E-04        1        1    1.000      (Y,T)         2
   0.0000E+00   0.0000E+00   1.0000E+00   1.346346E-05   9.835918E-05        1        1    1.000      (Z,P)         2
   0.0000E+00   0.0000E+00   1.0000E+00   4.515649E-04   1.937392E+01        1        1    1.000      (t,K)         2

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************

      5   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      6  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 2


 Pgm rebel. At pass #    2/  26.  In element #    2,  parameter # 35  changed to    1.16693292E+00   (was    1.00000000E+00)

                                -----  REBELOTE  -----

     End of pass #        2 through the optical structure 

                     Total of          2 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #    14 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        15.2       15.163906       80.0      5.851E-10  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-20)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     4    0.000000E+00    1.000E+00    4.725997E-11    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   2.2335E-21



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  MARKER      ProbProjTrajB_S                                                              IPASS= 3


************************************************************************************************************************************
      2  Keyword, label(s) :  OBJET                                                                                    IPASS= 3

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      3  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 3


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.1669    15.164     0.100     0.000     0.100            1.047    15.164    -0.000     0.002     0.000            1


                CONDITIONS  DE  MAXWELL  (       32.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.135349111     m ;  Time  (for ref. rigidity & particle) =   2.186948E-08 s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 3

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.1669    15.164     0.100     0.000     0.100      0.0000    0.1669   15.164   -0.100    0.002    0.098   1.587989E+01     1


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   1.516391E-01  -1.000003E-04        1        1    1.000      (Y,T)         3
   0.0000E+00   0.0000E+00   1.0000E+00   1.577739E-05   9.806604E-05        1        1    1.000      (Z,P)         3
   0.0000E+00   0.0000E+00   1.0000E+00   5.296960E-04   2.260807E+01        1        1    1.000      (t,K)         3

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************

      5   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      6  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 3


 Pgm rebel. At pass #    3/  26.  In element #    2,  parameter # 35  changed to    1.33386583E+00   (was    1.16693292E+00)

                                -----  REBELOTE  -----

     End of pass #        3 through the optical structure 

                     Total of          3 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #    15 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        17.4       17.425078       80.0      5.851E-10  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-20)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     4    0.000000E+00    1.000E+00    3.264233E-11    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   1.0655E-21



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  MARKER      ProbProjTrajB_S                                                              IPASS= 4


************************************************************************************************************************************
      2  Keyword, label(s) :  OBJET                                                                                    IPASS= 4

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      3  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 4


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.3339    17.425     0.100     0.000     0.100            1.047    17.425    -0.000     0.002     0.000            1


                CONDITIONS  DE  MAXWELL  (       37.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.135349111     m ;  Time  (for ref. rigidity & particle) =   2.186948E-08 s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 4

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.3339    17.425     0.100     0.000     0.100      0.0000    0.3339   17.425   -0.100    0.002    0.098   1.824782E+01     1


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   1.742508E-01  -1.000002E-04        1        1    1.000      (Y,T)         4
   0.0000E+00   0.0000E+00   1.0000E+00   1.811179E-05   9.776692E-05        1        1    1.000      (Z,P)         4
   0.0000E+00   0.0000E+00   1.0000E+00   6.086819E-04   2.584221E+01        1        1    1.000      (t,K)         4

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************

      5   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      6  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 4


 Pgm rebel. At pass #    4/  26.  In element #    2,  parameter # 35  changed to    1.50079875E+00   (was    1.33386583E+00)

                                -----  REBELOTE  -----

     End of pass #        4 through the optical structure 

                     Total of          4 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #    14 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        19.7       19.711539       80.0      6.501E-11  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-20)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     4    0.000000E+00    1.000E+00    3.037215E-11    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   9.2247E-22



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  MARKER      ProbProjTrajB_S                                                              IPASS= 5


************************************************************************************************************************************
      2  Keyword, label(s) :  OBJET                                                                                    IPASS= 5

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      3  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 5


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.5008    19.712     0.100     0.000     0.100            1.047    19.712    -0.000     0.002     0.000            1


                CONDITIONS  DE  MAXWELL  (       41.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.135349111     m ;  Time  (for ref. rigidity & particle) =   2.186948E-08 s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 5

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.5008    19.712     0.100     0.000     0.100      0.0000    0.5008   19.712   -0.100    0.002    0.097   2.064224E+01     1


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   1.971154E-01  -1.000013E-04        1        1    1.000      (Y,T)         5
   0.0000E+00   0.0000E+00   1.0000E+00   2.046728E-05   9.746154E-05        1        1    1.000      (Z,P)         5
   0.0000E+00   0.0000E+00   1.0000E+00   6.885511E-04   2.907636E+01        1        1    1.000      (t,K)         5

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************

      5   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      6  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 5


 Pgm rebel. At pass #    5/  26.  In element #    2,  parameter # 35  changed to    1.66773167E+00   (was    1.50079875E+00)

                                -----  REBELOTE  -----

     End of pass #        5 through the optical structure 

                     Total of          5 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #    18 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        22.0       22.024158       80.0      6.501E-11  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-20)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     4    0.000000E+00    1.000E+00    3.147349E-11    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   9.9058E-22



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  MARKER      ProbProjTrajB_S                                                              IPASS= 6


************************************************************************************************************************************
      2  Keyword, label(s) :  OBJET                                                                                    IPASS= 6

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      3  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 6


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.6677    22.024     0.100     0.000     0.100            1.047    22.024    -0.000     0.002     0.000            1


                CONDITIONS  DE  MAXWELL  (       46.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.135349111     m ;  Time  (for ref. rigidity & particle) =   2.186948E-08 s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 6

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.6677    22.024     0.100     0.000     0.100      0.0000    0.6677   22.024   -0.100    0.002    0.097   2.306405E+01     1


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   2.202416E-01  -1.000004E-04        1        1    1.000      (Y,T)         6
   0.0000E+00   0.0000E+00   1.0000E+00   2.284450E-05   9.714963E-05        1        1    1.000      (Z,P)         6
   0.0000E+00   0.0000E+00   1.0000E+00   7.693340E-04   3.231050E+01        1        1    1.000      (t,K)         6

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************

      5   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      6  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 6


 Pgm rebel. At pass #    6/  26.  In element #    2,  parameter # 35  changed to    1.83466458E+00   (was    1.66773167E+00)

                                -----  REBELOTE  -----

     End of pass #        6 through the optical structure 

                     Total of          6 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #    13 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        24.4       24.363853       80.0      5.851E-10  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-20)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     4    0.000000E+00    1.000E+00    2.702194E-11    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   7.3019E-22



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  MARKER      ProbProjTrajB_S                                                              IPASS= 7


************************************************************************************************************************************
      2  Keyword, label(s) :  OBJET                                                                                    IPASS= 7

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      3  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 7


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.8347    24.364     0.100     0.000     0.100            1.047    24.364    -0.000     0.003     0.000            1


                CONDITIONS  DE  MAXWELL  (       51.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.135349111     m ;  Time  (for ref. rigidity & particle) =   2.186948E-08 s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 7

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.8347    24.364     0.100     0.000     0.100      0.0000    0.8347   24.364   -0.100    0.003    0.097   2.551422E+01     1


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   2.436385E-01  -1.000001E-04        1        1    1.000      (Y,T)         7
   0.0000E+00   0.0000E+00   1.0000E+00   2.524413E-05   9.683092E-05        1        1    1.000      (Z,P)         7
   0.0000E+00   0.0000E+00   1.0000E+00   8.510628E-04   3.554465E+01        1        1    1.000      (t,K)         7

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************

      5   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      6  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 7


 Pgm rebel. At pass #    7/  26.  In element #    2,  parameter # 35  changed to    2.00159750E+00   (was    1.83466458E+00)

                                -----  REBELOTE  -----

     End of pass #        7 through the optical structure 

                     Total of          7 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #    14 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        26.7       26.731598       80.0      1.950E-10  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-20)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     4    0.000000E+00    1.000E+00    7.500844E-11    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   5.6263E-21



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  MARKER      ProbProjTrajB_S                                                              IPASS= 8


************************************************************************************************************************************
      2  Keyword, label(s) :  OBJET                                                                                    IPASS= 8

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      3  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 8


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.0016    26.732     0.100     0.000     0.100            1.047    26.732    -0.000     0.003     0.000            1


                CONDITIONS  DE  MAXWELL  (       55.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.135349111     m ;  Time  (for ref. rigidity & particle) =   2.186948E-08 s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 8

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   2.0016    26.732     0.100     0.000     0.100      0.0000    1.0016   26.732   -0.100    0.003    0.097   2.799376E+01     1


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   2.673160E-01  -1.000028E-04        1        1    1.000      (Y,T)         8
   0.0000E+00   0.0000E+00   1.0000E+00   2.766688E-05   9.650511E-05        1        1    1.000      (Z,P)         8
   0.0000E+00   0.0000E+00   1.0000E+00   9.337714E-04   3.877879E+01        1        1    1.000      (t,K)         8

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************

      5   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      6  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 8


 Pgm rebel. At pass #    8/  26.  In element #    2,  parameter # 35  changed to    2.16853042E+00   (was    2.00159750E+00)

                                -----  REBELOTE  -----

     End of pass #        8 through the optical structure 

                     Total of          8 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #    18 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        29.1       29.128427       80.0      1.950E-10  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-20)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     4    0.000000E+00    1.000E+00    8.133938E-11    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   6.6161E-21



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  MARKER      ProbProjTrajB_S                                                              IPASS= 9


************************************************************************************************************************************
      2  Keyword, label(s) :  OBJET                                                                                    IPASS= 9

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      3  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 9


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.1685    29.128     0.100     0.000     0.100            1.047    29.128    -0.000     0.003     0.000            1


                CONDITIONS  DE  MAXWELL  (       60.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.135349111     m ;  Time  (for ref. rigidity & particle) =   2.186948E-08 s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 9

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   2.1685    29.128     0.100     0.000     0.100      0.0000    1.1685   29.128   -0.100    0.003    0.096   3.050376E+01     1


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   2.912843E-01  -1.000019E-04        1        1    1.000      (Y,T)         9
   0.0000E+00   0.0000E+00   1.0000E+00   3.011352E-05   9.617188E-05        1        1    1.000      (Z,P)         9
   0.0000E+00   0.0000E+00   1.0000E+00   1.017496E-03   4.201294E+01        1        1    1.000      (t,K)         9

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************

      5   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      6  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 9


 Pgm rebel. At pass #    9/  26.  In element #    2,  parameter # 35  changed to    2.33546333E+00   (was    2.16853042E+00)

                                -----  REBELOTE  -----

     End of pass #        9 through the optical structure 

                     Total of          9 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #    15 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        31.6       31.555439       80.0      1.950E-10  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-20)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     4    0.000000E+00    1.000E+00    9.316992E-11    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   8.6806E-21



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  MARKER      ProbProjTrajB_S                                                              IPASS= 10


************************************************************************************************************************************
      2  Keyword, label(s) :  OBJET                                                                                    IPASS= 10

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      3  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 10


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.3355    31.555     0.100     0.000     0.100            1.047    31.555    -0.000     0.003     0.000            1


                CONDITIONS  DE  MAXWELL  (       65.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.135349111     m ;  Time  (for ref. rigidity & particle) =   2.186948E-08 s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 10

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   2.3355    31.555     0.100     0.000     0.100      0.0000    1.3355   31.555   -0.100    0.003    0.096   3.304537E+01     1


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   3.155544E-01  -1.000016E-04        1        1    1.000      (Y,T)        10
   0.0000E+00   0.0000E+00   1.0000E+00   3.258485E-05   9.583090E-05        1        1    1.000      (Z,P)        10
   0.0000E+00   0.0000E+00   1.0000E+00   1.102275E-03   4.524708E+01        1        1    1.000      (t,K)        10

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************

      5   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      6  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 10


 Pgm rebel. At pass #   10/  26.  In element #    2,  parameter # 35  changed to    2.50239625E+00   (was    2.33546333E+00)

                                -----  REBELOTE  -----

     End of pass #       10 through the optical structure 

                     Total of         10 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #    16 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        34.0       34.013804       80.0      6.501E-11  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-20)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     4    0.000000E+00    1.000E+00    3.534950E-11    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   1.2496E-21



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  MARKER      ProbProjTrajB_S                                                              IPASS= 11


************************************************************************************************************************************
      2  Keyword, label(s) :  OBJET                                                                                    IPASS= 11

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      3  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 11


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.5024    34.014     0.100     0.000     0.100            1.047    34.014    -0.000     0.004     0.000            1


                CONDITIONS  DE  MAXWELL  (       70.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.135349111     m ;  Time  (for ref. rigidity & particle) =   2.186948E-08 s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 11

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   2.5024    34.014     0.100     0.000     0.100      0.0000    1.5024   34.014   -0.100    0.004    0.095   3.561980E+01     1


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   3.401380E-01  -1.000016E-04        1        1    1.000      (Y,T)        11
   0.0000E+00   0.0000E+00   1.0000E+00   3.508171E-05   9.548180E-05        1        1    1.000      (Z,P)        11
   0.0000E+00   0.0000E+00   1.0000E+00   1.188149E-03   4.848123E+01        1        1    1.000      (t,K)        11

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************

      5   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      6  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 11


 Pgm rebel. At pass #   11/  26.  In element #    2,  parameter # 35  changed to    2.66932917E+00   (was    2.50239625E+00)

                                -----  REBELOTE  -----

     End of pass #       11 through the optical structure 

                     Total of         11 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #    14 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        36.5       36.504769       80.0      1.950E-10  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-20)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     4    0.000000E+00    1.000E+00    6.731682E-11    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   4.5316E-21



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  MARKER      ProbProjTrajB_S                                                              IPASS= 12


************************************************************************************************************************************
      2  Keyword, label(s) :  OBJET                                                                                    IPASS= 12

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      3  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 12


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.6693    36.505     0.100     0.000     0.100            1.047    36.505    -0.000     0.004     0.000            1


                CONDITIONS  DE  MAXWELL  (       75.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.135349111     m ;  Time  (for ref. rigidity & particle) =   2.186948E-08 s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 12

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   2.6693    36.505     0.100     0.000     0.100      0.0000    1.6693   36.505   -0.100    0.004    0.095   3.822838E+01     1


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   3.650477E-01  -1.000018E-04        1        1    1.000      (Y,T)        12
   0.0000E+00   0.0000E+00   1.0000E+00   3.760503E-05   9.512421E-05        1        1    1.000      (Z,P)        12
   0.0000E+00   0.0000E+00   1.0000E+00   1.275162E-03   5.171537E+01        1        1    1.000      (t,K)        12

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************

      5   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      6  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 12


 Pgm rebel. At pass #   12/  26.  In element #    2,  parameter # 35  changed to    2.83626208E+00   (was    2.66932917E+00)

                                -----  REBELOTE  -----

     End of pass #       12 through the optical structure 

                     Total of         12 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #    16 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        39.0       39.029666       80.0      1.950E-10  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-20)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     4    0.000000E+00    1.000E+00    1.944755E-11    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   3.7821E-22



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  MARKER      ProbProjTrajB_S                                                              IPASS= 13


************************************************************************************************************************************
      2  Keyword, label(s) :  OBJET                                                                                    IPASS= 13

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      3  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 13


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.8363    39.030     0.100     0.000     0.100            1.047    39.030    -0.000     0.004     0.000            1


                CONDITIONS  DE  MAXWELL  (       80.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.135349111     m ;  Time  (for ref. rigidity & particle) =   2.186948E-08 s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 13

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   2.8363    39.030     0.100     0.000     0.100      0.0000    1.8363   39.030   -0.100    0.004    0.095   4.087250E+01     1


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   3.902967E-01  -1.000022E-04        1        1    1.000      (Y,T)        13
   0.0000E+00   0.0000E+00   1.0000E+00   4.015575E-05   9.475771E-05        1        1    1.000      (Z,P)        13
   0.0000E+00   0.0000E+00   1.0000E+00   1.363360E-03   5.494952E+01        1        1    1.000      (t,K)        13

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************

      5   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      6  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 13


 Pgm rebel. At pass #   13/  26.  In element #    2,  parameter # 35  changed to    3.00319500E+00   (was    2.83626208E+00)

                                -----  REBELOTE  -----

     End of pass #       13 through the optical structure 

                     Total of         13 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #    15 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        41.6       41.589922       80.0      1.950E-10  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-20)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     4    0.000000E+00    1.000E+00    8.974865E-11    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   8.0548E-21



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  MARKER      ProbProjTrajB_S                                                              IPASS= 14


************************************************************************************************************************************
      2  Keyword, label(s) :  OBJET                                                                                    IPASS= 14

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      3  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 14


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  3.0032    41.590     0.100     0.000     0.100            1.047    41.590    -0.000     0.004     0.000            1


                CONDITIONS  DE  MAXWELL  (       85.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.135349111     m ;  Time  (for ref. rigidity & particle) =   2.186948E-08 s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 14

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   3.0032    41.590     0.100     0.000     0.100      0.0000    2.0032   41.590   -0.100    0.004    0.094   4.355364E+01     1


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.158992E-01  -1.000030E-04        1        1    1.000      (Y,T)        14
   0.0000E+00   0.0000E+00   1.0000E+00   4.273490E-05   9.438186E-05        1        1    1.000      (Z,P)        14
   0.0000E+00   0.0000E+00   1.0000E+00   1.452793E-03   5.818366E+01        1        1    1.000      (t,K)        14

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************

      5   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      6  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 14


 Pgm rebel. At pass #   14/  26.  In element #    2,  parameter # 35  changed to    3.17012792E+00   (was    3.00319500E+00)

                                -----  REBELOTE  -----

     End of pass #       14 through the optical structure 

                     Total of         14 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #    17 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        44.2       44.187063       80.0      6.501E-11  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-20)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     4    0.000000E+00    1.000E+00    1.978151E-11    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   3.9131E-22



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  MARKER      ProbProjTrajB_S                                                              IPASS= 15


************************************************************************************************************************************
      2  Keyword, label(s) :  OBJET                                                                                    IPASS= 15

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      3  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 15


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  3.1701    44.187     0.100     0.000     0.100            1.047    44.187    -0.000     0.005     0.000            1


                CONDITIONS  DE  MAXWELL  (       91.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.135349111     m ;  Time  (for ref. rigidity & particle) =   2.186948E-08 s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 15

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   3.1701    44.187     0.100     0.000     0.100      0.0000    2.1701   44.187   -0.100    0.005    0.094   4.627341E+01     1


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.418706E-01  -1.000008E-04        1        1    1.000      (Y,T)        15
   0.0000E+00   0.0000E+00   1.0000E+00   4.534359E-05   9.399616E-05        1        1    1.000      (Z,P)        15
   0.0000E+00   0.0000E+00   1.0000E+00   1.543515E-03   6.141781E+01        1        1    1.000      (t,K)        15

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************

      5   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      6  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 15


 Pgm rebel. At pass #   15/  26.  In element #    2,  parameter # 35  changed to    3.33706083E+00   (was    3.17012792E+00)

                                -----  REBELOTE  -----

     End of pass #       15 through the optical structure 

                     Total of         15 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #    17 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        46.8       46.822734       80.0      6.501E-11  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-20)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     4    0.000000E+00    1.000E+00    4.328626E-11    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   1.8737E-21



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  MARKER      ProbProjTrajB_S                                                              IPASS= 16


************************************************************************************************************************************
      2  Keyword, label(s) :  OBJET                                                                                    IPASS= 16

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      3  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 16


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  3.3371    46.823     0.100     0.000     0.100            1.047    46.823    -0.000     0.005     0.000            1


                CONDITIONS  DE  MAXWELL  (       96.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.135349111     m ;  Time  (for ref. rigidity & particle) =   2.186948E-08 s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 16

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   3.3371    46.823     0.100     0.000     0.100      0.0000    2.3371   46.823   -0.100    0.005    0.094   4.903352E+01     1


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.682273E-01  -1.000015E-04        1        1    1.000      (Y,T)        16
   0.0000E+00   0.0000E+00   1.0000E+00   4.798296E-05   9.360013E-05        1        1    1.000      (Z,P)        16
   0.0000E+00   0.0000E+00   1.0000E+00   1.635582E-03   6.465195E+01        1        1    1.000      (t,K)        16

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************

      5   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      6  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 16


 Pgm rebel. At pass #   16/  26.  In element #    2,  parameter # 35  changed to    3.50399375E+00   (was    3.33706083E+00)

                                -----  REBELOTE  -----

     End of pass #       16 through the optical structure 

                     Total of         16 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #    18 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        49.5       49.498700       80.0      1.950E-10  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-20)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     4    0.000000E+00    1.000E+00    6.629364E-12    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   4.3948E-23



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  MARKER      ProbProjTrajB_S                                                              IPASS= 17


************************************************************************************************************************************
      2  Keyword, label(s) :  OBJET                                                                                    IPASS= 17

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      3  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 17


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  3.5040    49.499     0.100     0.000     0.100            1.047    49.499    -0.000     0.005     0.000            1


                CONDITIONS  DE  MAXWELL  (      101.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.135349111     m ;  Time  (for ref. rigidity & particle) =   2.186948E-08 s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 17

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   3.5040    49.499     0.100     0.000     0.100      0.0000    2.5040   49.499   -0.100    0.005    0.093   5.183584E+01     1


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.949870E-01  -1.000029E-04        1        1    1.000      (Y,T)        17
   0.0000E+00   0.0000E+00   1.0000E+00   5.065429E-05   9.319318E-05        1        1    1.000      (Z,P)        17
   0.0000E+00   0.0000E+00   1.0000E+00   1.729057E-03   6.788610E+01        1        1    1.000      (t,K)        17

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************

      5   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      6  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 17


 Pgm rebel. At pass #   17/  26.  In element #    2,  parameter # 35  changed to    3.67092667E+00   (was    3.50399375E+00)

                                -----  REBELOTE  -----

     End of pass #       17 through the optical structure 

                     Total of         17 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #    15 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        52.2       52.216868       80.0      6.501E-11  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-20)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     4    0.000000E+00    1.000E+00    3.863931E-11    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   1.4930E-21



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  MARKER      ProbProjTrajB_S                                                              IPASS= 18


************************************************************************************************************************************
      2  Keyword, label(s) :  OBJET                                                                                    IPASS= 18

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      3  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 18


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  3.6709    52.217     0.100     0.000     0.100            1.047    52.217    -0.000     0.005     0.000            1


                CONDITIONS  DE  MAXWELL  (      107.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.135349111     m ;  Time  (for ref. rigidity & particle) =   2.186948E-08 s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 18

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   3.6709    52.217     0.100     0.000     0.100      0.0000    2.6709   52.217   -0.100    0.005    0.093   5.468235E+01     1


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   5.221687E-01  -1.000014E-04        1        1    1.000      (Y,T)        18
   0.0000E+00   0.0000E+00   1.0000E+00   5.335890E-05   9.277471E-05        1        1    1.000      (Z,P)        18
   0.0000E+00   0.0000E+00   1.0000E+00   1.824007E-03   7.112024E+01        1        1    1.000      (t,K)        18

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************

      5   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      6  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 18


 Pgm rebel. At pass #   18/  26.  In element #    2,  parameter # 35  changed to    3.83785958E+00   (was    3.67092667E+00)

                                -----  REBELOTE  -----

     End of pass #       18 through the optical structure 

                     Total of         18 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #    16 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        55.0       54.979301       80.0      1.755E-09  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-20)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     4    0.000000E+00    1.000E+00    5.451284E-11    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   2.9716E-21



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  MARKER      ProbProjTrajB_S                                                              IPASS= 19


************************************************************************************************************************************
      2  Keyword, label(s) :  OBJET                                                                                    IPASS= 19

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      3  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 19


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  3.8379    54.979     0.100     0.000     0.100            1.047    54.979    -0.000     0.006     0.000            1


                CONDITIONS  DE  MAXWELL  (      112.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.135349111     m ;  Time  (for ref. rigidity & particle) =   2.186948E-08 s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 19

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   3.8379    54.979     0.100     0.000     0.100      0.0000    2.8379   54.979   -0.100    0.006    0.092   5.757521E+01     1


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   5.497930E-01  -1.000032E-04        1        1    1.000      (Y,T)        19
   0.0000E+00   0.0000E+00   1.0000E+00   5.609826E-05   9.234407E-05        1        1    1.000      (Z,P)        19
   0.0000E+00   0.0000E+00   1.0000E+00   1.920502E-03   7.435439E+01        1        1    1.000      (t,K)        19

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************

      5   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      6  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 19


 Pgm rebel. At pass #   19/  26.  In element #    2,  parameter # 35  changed to    4.00479250E+00   (was    3.83785958E+00)

                                -----  REBELOTE  -----

     End of pass #       19 through the optical structure 

                     Total of         19 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #    16 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        57.8       57.788233       80.0      5.851E-10  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-20)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     4    0.000000E+00    1.000E+00    6.646417E-11    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   4.4175E-21



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  MARKER      ProbProjTrajB_S                                                              IPASS= 20


************************************************************************************************************************************
      2  Keyword, label(s) :  OBJET                                                                                    IPASS= 20

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      3  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 20


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  4.0048    57.788     0.100     0.000     0.100            1.047    57.788    -0.000     0.006     0.000            1


                CONDITIONS  DE  MAXWELL  (      118.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.135349111     m ;  Time  (for ref. rigidity & particle) =   2.186948E-08 s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 20

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   4.0048    57.788     0.100     0.000     0.100      0.0000    3.0048   57.788   -0.100    0.006    0.092   6.051677E+01     1


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   5.778823E-01  -1.000021E-04        1        1    1.000      (Y,T)        20
   0.0000E+00   0.0000E+00   1.0000E+00   5.887392E-05   9.190052E-05        1        1    1.000      (Z,P)        20
   0.0000E+00   0.0000E+00   1.0000E+00   2.018622E-03   7.758853E+01        1        1    1.000      (t,K)        20

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************

      5   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      6  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 20


 Pgm rebel. At pass #   20/  26.  In element #    2,  parameter # 35  changed to    4.17172542E+00   (was    4.00479250E+00)

                                -----  REBELOTE  -----

     End of pass #       20 through the optical structure 

                     Total of         20 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #    20 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        60.6       60.646097       80.0      1.950E-10  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-20)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     4    0.000000E+00    1.000E+00    8.236611E-11    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   6.7842E-21



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  MARKER      ProbProjTrajB_S                                                              IPASS= 21


************************************************************************************************************************************
      2  Keyword, label(s) :  OBJET                                                                                    IPASS= 21

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      3  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 21


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  4.1717    60.646     0.100     0.000     0.100            1.047    60.646    -0.000     0.006     0.000            1


                CONDITIONS  DE  MAXWELL  (      124.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.135349111     m ;  Time  (for ref. rigidity & particle) =   2.186948E-08 s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 21

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   4.1717    60.646     0.100     0.000     0.100      0.0000    3.1717   60.646   -0.100    0.006    0.091   6.350957E+01     1


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   6.064610E-01  -1.000017E-04        1        1    1.000      (Y,T)        21
   0.0000E+00   0.0000E+00   1.0000E+00   6.168758E-05   9.144329E-05        1        1    1.000      (Z,P)        21
   0.0000E+00   0.0000E+00   1.0000E+00   2.118451E-03   8.082268E+01        1        1    1.000      (t,K)        21

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************

      5   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      6  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 21


 Pgm rebel. At pass #   21/  26.  In element #    2,  parameter # 35  changed to    4.33865833E+00   (was    4.17172542E+00)

                                -----  REBELOTE  -----

     End of pass #       21 through the optical structure 

                     Total of         21 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #    14 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        63.6       63.555540       80.0      1.755E-09  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-20)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     4    0.000000E+00    1.000E+00    4.847323E-11    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   2.3497E-21



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  MARKER      ProbProjTrajB_S                                                              IPASS= 22


************************************************************************************************************************************
      2  Keyword, label(s) :  OBJET                                                                                    IPASS= 22

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      3  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 22


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  4.3387    63.556     0.100     0.000     0.100            1.047    63.556    -0.000     0.006     0.000            1


                CONDITIONS  DE  MAXWELL  (      130.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.135349111     m ;  Time  (for ref. rigidity & particle) =   2.186948E-08 s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 22

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   4.3387    63.556     0.100     0.000     0.100      0.0000    3.3387   63.556   -0.100    0.006    0.091   6.655639E+01     1


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   6.355554E-01  -1.000015E-04        1        1    1.000      (Y,T)        22
   0.0000E+00   0.0000E+00   1.0000E+00   6.454105E-05   9.097150E-05        1        1    1.000      (Z,P)        22
   0.0000E+00   0.0000E+00   1.0000E+00   2.220082E-03   8.405682E+01        1        1    1.000      (t,K)        22

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************

      5   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      6  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 22


 Pgm rebel. At pass #   22/  26.  In element #    2,  parameter # 35  changed to    4.50559125E+00   (was    4.33865833E+00)

                                -----  REBELOTE  -----

     End of pass #       22 through the optical structure 

                     Total of         22 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #    15 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        66.5       66.519461       80.0      5.851E-10  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-20)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     4    0.000000E+00    1.000E+00    9.040946E-11    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   8.1739E-21



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  MARKER      ProbProjTrajB_S                                                              IPASS= 23


************************************************************************************************************************************
      2  Keyword, label(s) :  OBJET                                                                                    IPASS= 23

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      3  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 23


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  4.5056    66.519     0.100     0.000     0.100            1.047    66.519    -0.000     0.007     0.000            1


                CONDITIONS  DE  MAXWELL  (      136.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.135349111     m ;  Time  (for ref. rigidity & particle) =   2.186948E-08 s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 23

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   4.5056    66.519     0.100     0.000     0.100      0.0000    3.5056   66.519   -0.100    0.007    0.090   6.966025E+01     1


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   6.651946E-01  -1.000015E-04        1        1    1.000      (Y,T)        23
   0.0000E+00   0.0000E+00   1.0000E+00   6.743633E-05   9.048422E-05        1        1    1.000      (Z,P)        23
   0.0000E+00   0.0000E+00   1.0000E+00   2.323616E-03   8.729097E+01        1        1    1.000      (t,K)        23

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************

      5   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      6  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 23


 Pgm rebel. At pass #   23/  26.  In element #    2,  parameter # 35  changed to    4.67252417E+00   (was    4.50559125E+00)

                                -----  REBELOTE  -----

     End of pass #       23 through the optical structure 

                     Total of         23 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #    17 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        69.5       69.541040       80.0      1.950E-10  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-20)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     4    0.000000E+00    1.000E+00    8.554935E-11    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   7.3187E-21



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  MARKER      ProbProjTrajB_S                                                              IPASS= 24


************************************************************************************************************************************
      2  Keyword, label(s) :  OBJET                                                                                    IPASS= 24

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      3  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 24


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  4.6725    69.541     0.100     0.000     0.100            1.047    69.541    -0.000     0.007     0.000            1


                CONDITIONS  DE  MAXWELL  (      142.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.135349111     m ;  Time  (for ref. rigidity & particle) =   2.186948E-08 s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 24

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   4.6725    69.541     0.100     0.000     0.100      0.0000    3.6725   69.541   -0.100    0.007    0.090   7.282450E+01     1


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   6.954104E-01  -1.000017E-04        1        1    1.000      (Y,T)        24
   0.0000E+00   0.0000E+00   1.0000E+00   7.037560E-05   8.998038E-05        1        1    1.000      (Z,P)        24
   0.0000E+00   0.0000E+00   1.0000E+00   2.429164E-03   9.052511E+01        1        1    1.000      (t,K)        24

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************

      5   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      6  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 24


 Pgm rebel. At pass #   24/  26.  In element #    2,  parameter # 35  changed to    4.83945708E+00   (was    4.67252417E+00)

                                -----  REBELOTE  -----

     End of pass #       24 through the optical structure 

                     Total of         24 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #    17 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        72.6       72.623779       80.0      5.851E-10  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-20)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     4    0.000000E+00    1.000E+00    1.180922E-11    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   1.3946E-22



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  MARKER      ProbProjTrajB_S                                                              IPASS= 25


************************************************************************************************************************************
      2  Keyword, label(s) :  OBJET                                                                                    IPASS= 25

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      3  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 25


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  4.8395    72.624     0.100     0.000     0.100            1.047    72.624    -0.000     0.007     0.000            1


                CONDITIONS  DE  MAXWELL  (      148.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.135349111     m ;  Time  (for ref. rigidity & particle) =   2.186948E-08 s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 25

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   4.8395    72.624     0.100     0.000     0.100      0.0000    3.8395   72.624   -0.100    0.007    0.089   7.605279E+01     1


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   7.262378E-01  -1.000022E-04        1        1    1.000      (Y,T)        25
   0.0000E+00   0.0000E+00   1.0000E+00   7.336124E-05   8.945882E-05        1        1    1.000      (Z,P)        25
   0.0000E+00   0.0000E+00   1.0000E+00   2.536848E-03   9.375926E+01        1        1    1.000      (t,K)        25

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************

      5   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      6  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 25


 Pgm rebel. At pass #   25/  26.  In element #    2,  parameter # 35  changed to    5.00639000E+00   (was    4.83945708E+00)

                                -----  REBELOTE  -----

     End of pass #       25 through the optical structure 

                     Total of         25 particles have been launched


      Next  pass  is  #    26 and  last  pass  through  the  optical  structure


     WRITE statements to zgoubi.res are re-established from now on.

************************************************************************************************************************************
      1  Keyword, label(s) :  MARKER      ProbProjTrajB_S                                                              IPASS= 26


************************************************************************************************************************************
      2  Keyword, label(s) :  OBJET                                                                                    IPASS= 26

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      3  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 26


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  5.0064    72.624     0.100     0.000     0.100            1.047    73.867     0.030     0.007     0.000            1


                CONDITIONS  DE  MAXWELL  (      149.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.135349111     m ;  Time  (for ref. rigidity & particle) =   4.373897E-08 s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 26

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   5.0064    72.624     0.100     0.000     0.100      0.0000    4.0064   73.867   29.890    0.007    0.090   7.651160E+01     1


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   7.386723E-01   2.989005E-02        1        1    1.000      (Y,T)        26
   0.0000E+00   0.0000E+00   1.0000E+00   7.386223E-05   8.968552E-05        1        1    1.000      (Z,P)        26
   0.0000E+00   0.0000E+00   1.0000E+00   2.552152E-03   9.699340E+01        1        1    1.000      (t,K)        26

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      5  Keyword, label(s) :  FIT                                                                                      IPASS= 26


************************************************************************************************************************************
      1  Keyword, label(s) :  MARKER      ProbProjTrajB_S                                                              IPASS= 26


************************************************************************************************************************************
      2  Keyword, label(s) :  OBJET                                                                                    IPASS= 26

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      3  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 26


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  5.0064    72.624     0.100     0.000     0.100            1.047    73.867     0.030     0.007     0.000            1


                CONDITIONS  DE  MAXWELL  (      149.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.135349111     m ;  Time  (for ref. rigidity & particle) =   2.186948E-08 s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 26

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   5.0064    72.624     0.100     0.000     0.100      0.0000    4.0064   73.867   29.890    0.007    0.090   7.651160E+01     1


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   7.386723E-01   2.989005E-02        1        1    1.000      (Y,T)        26
   0.0000E+00   0.0000E+00   1.0000E+00   7.386223E-05   8.968552E-05        1        1    1.000      (Z,P)        26
   0.0000E+00   0.0000E+00   1.0000E+00   2.552152E-03   9.699340E+01        1        1    1.000      (t,K)        26

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      5  Keyword, label(s) :  FIT                                                                                      IPASS= 26

     Pgm main. FITING=.T., FIT procedure launched.

           variable #            1       IR =            2 ,   ok.
           variable #            1       IP =           30 ,   ok.
           constraint #            1       IR =            4 ,   ok.
           constraint #            1       I  =            1 ,   ok.

                    FIT  variables  and  constraints  in  good  order,  FIT  will proceed. 
 STATUS OF VARIABLES  (Iteration #    15 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        75.8       75.771550       80.0      1.950E-10  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-20)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     4    0.000000E+00    1.000E+00    1.072920E-11    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   1.1512E-22



************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed. Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  MARKER      ProbProjTrajB_S                                                              IPASS= 26


************************************************************************************************************************************
      2  Keyword, label(s) :  OBJET                                                                                    IPASS= 26

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      3  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 26


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  5.0064    75.772     0.100     0.000     0.100            1.047    75.772    -0.000     0.008     0.000            1


                CONDITIONS  DE  MAXWELL  (      154.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.135349111     m ;  Time  (for ref. rigidity & particle) =   2.186948E-08 s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 26

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   5.0064    75.772     0.100     0.000     0.100      0.0000    4.0064   75.772   -0.100    0.008    0.089   7.934919E+01     1


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   7.577155E-01  -1.000031E-04        1        1    1.000      (Y,T)        26
   0.0000E+00   0.0000E+00   1.0000E+00   7.639585E-05   8.891825E-05        1        1    1.000      (Z,P)        26
   0.0000E+00   0.0000E+00   1.0000E+00   2.646804E-03   9.699340E+01        1        1    1.000      (t,K)        26

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************

      5   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      6  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 26


                         >>>>  End  of  'REBELOTE'  procedure  <<<<

      There  has  been         26  passes  through  the  optical  structure 

                     Total of         26 particles have been launched

************************************************************************************************************************************
      7  Keyword, label(s) :  SYSTEM                                                                                   IPASS= 27

     Number  of  commands :   1,  as  follows : 

 gnuplot < ./gnuplot_Zplt_field.gnu

************************************************************************************************************************************
      8  Keyword, label(s) :  MARKER                                                                                   IPASS= 27


************************************************************************************************************************************
      9  Keyword, label(s) :  END                                                                                      IPASS= 27


************************************************************************************************************************************
 Pgm zgoubi : Execution ended normally, upon keyword END or FIN
   
 File in:   classCyclo_ProbProjTrajField.INC.dat
 File out:  zgoubi.res

  Zgoubi, author's dvlpmnt version.
  Job  started  on  28-05-2020,  at  13:44:28 
  JOB  ENDED  ON    28-05-2020,  AT  13:44:30 

   CPU time, total :    0.75737200000000005     
