
#set title "Plotted from file zgoubi.plt,  to Cartesian Lab frame"
set key maxcol 1 ; set key t r
set xtics ;  set ytics 
set xlabel "X_{Lab}  [m]" ; set ylabel "Y_{Lab} [m]" 
cm2m = 0.01
#set xrange [-0.77:0.77] ; set yrange [-0.77:0.77]
set size ratio 1  ;  set polar
plot for [i=1:3] "zgoubi.plt" u ($19==i ? $22 :1/0):($10 *cm2m)  \
w l lw 2 lc rgb "black"  notit
set terminal postscript eps blacktext color  enh  
set output "gnuplot_Zplt_XYLab.eps"  
replot  
set terminal X11  
unset output   
pause 3
exit

