Uniform field sector
'OBJET'                                                                                                      1
64.62444403717985                      ! Reference Brho ("BORO" in the users' guide) -> 200keV proton.
2
3 1
10.011362 0. 0. 0. 0. 0.7745802 'a'          ! p[MeV/c]= 15.007, Brho[kG.cm]= 50.057, kin-E[MeV]=0.12.
12.924888 0. 0. 0. 0. 1.        'b'                                                  ! kin-E[MeV]=0.2.
67.997983 0. 0. 0. 0. 5.2610112 'c'          ! p[MeV/c]=101.926, Brho[kG.cm]=339.990, kin-E[MeV]=5.52.
1 1 1
'MARKER'   #S_halfDipole                                                                                     2
'TOSCA'                                                                                                      3
0 2    ! IL=2 to log step-by-step coordinates, spin, etc., to zgoubi.plt (avoid, if CPU time matters).
1. 1. 1. 1.       ! Normalization coefficients, for B, X, Y and Z coordinate values read from the map.
HEADER_8                                            ! The field map file starts with an 8-line header.
315 151 1 22.1 1.            ! IZ=1 for 2D map; MOD=22 for polar frame; .MOD2=.1 if only one map file.
geneSectorMap.map
0 0 0 0       ! Possible vertical boundaries within the field map, to start/stop stepwise integration.
2
1. ! cm                                                                       ! Integration step size.
2                                                                        ! Magnet positionning option.
0. 0. 0. 0.                                                                     ! Magnet positionning.
'MARKER'   #E_halfDipole                                                                                     4
'FAISCEAU'                                                                                                   5
'SYSTEM'                      ! This SYSTEM command runs gnuplot, for a graph of the two trajectories.       6
1
gnuplot <./gnuplot_Zplt.gnu
'MARKER'   FieldMapSector_E                                               ! Just for edition purposes.       7
'END'

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  MARKER      #S_halfDipole                                                                IPASS= 1


************************************************************************************************************************************
      3  Keyword, label(s) :  TOSCA                                                                                    IPASS= 1


                OPEN FILE zgoubi.plt                                                                      
                FOR PRINTING TRAJECTORIES


     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           2-D or 3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00

          New field map(s) now used, polar mesh (MOD .ge. 20) ;  name(s) of map data file(s) are : 
          'geneSectorMap.map'

   ----
   Map file number    1 ( of 1) :

     geneSectorMap.map map,  FORMAT type : regular.  Field multiplication factor :  1.00000000E+00

 HEADER  (8 lines) : 
        1.0000000000000000       0.50000000000000000       0.57324840764331209        9.8813129168249309E-324      !  Rmi/cm,
      # Field map generated using geneSectorMap.f                                                                            
     # AT/rd,  AT/deg, Rmi/cm, Rma/cm, RM/cm, NR, dR/cm, NX, dX/cm, dA/rd :                                                  
     #   3.14159265E+00   1.80000000E+02   1.00000000E+00   7.60000000E+01   5.00000000E+01 151   5.00000000E-01 315   5.0025
      # For TOSCA:          315         151  1 22.1 1.  !IZ=1 -> 2D ; MOD=22 -> polar map ; .MOD2=.1 -> one map file         
      # R*cosA (A:0->360), Z==0, R*sinA, BY, BZ, BX                                                                          
      # cm                 cm    cm      kG  kG  kG                                                                          
      #                                                                                                                      
 
R_min (cm), DR (cm), DTTA (deg), DZ (cm) :   1.000000E+00  5.000000E-01  5.732484E-01  9.881313-324



     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, Z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.7746    10.011     0.000     0.000     0.000            1.571    10.011    -0.000     0.000     0.000            1
  A    1  1.0000    12.925     0.000     0.000     0.000            1.571    12.925    -0.000     0.000     0.000            2
  A    1  5.2610    67.998     0.000     0.000     0.000            1.571    67.998    -0.000     0.000     0.000            3


                CONDITIONS  DE  MAXWELL  (      288.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #E_halfDipole                                                                IPASS= 1


************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

a  1   0.7746    10.011     0.000     0.000     0.000      0.0000   -0.2254   10.011   -0.000    0.000    0.000   3.145152E+01     1
b  1   1.0000    12.925     0.000     0.000     0.000      0.0000    0.0000   12.925   -0.000    0.000    0.000   4.060469E+01     2
c  1   5.2610    67.998     0.000     0.000     0.000      0.0000    4.2610   67.998   -0.000    0.000    0.000   2.136220E+02     3


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   1.1786E-07  -1.0556E+00   1.8967E+06   3.031137E-01  -2.011707E-07        3        3    1.000      (Y,T)         1
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        3        3    1.000      (Z,P)         1
   8.5202E-08  -4.1213E+06   2.8812E+02   3.176399E-03   4.543566E+01        3        3    1.000      (t,K)         1

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   2.667499E-01
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   2.044981E-07

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   2.795331E-03
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   3.998476E+01


  Beam  sigma  matrix  and  determinants : 

   7.115552E-02   3.960166E-08   0.000000E+00   0.000000E+00
   3.960166E-08   4.181949E-14   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     3.751528E-08    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  SYSTEM                                                                                   IPASS= 1

     Number  of  commands :   1,  as  follows : 

 gnuplot <./gnuplot_Zplt.gnu

************************************************************************************************************************************
      7  Keyword, label(s) :  MARKER      FieldMapSector_E                                                             IPASS= 1


************************************************************************************************************************************
      8  Keyword, label(s) :  END                                                                                      IPASS= 1


                             3 particles have been launched
                     Made  it  to  the  end :      3

************************************************************************************************************************************
 Pgm zgoubi : Execution ended normally, upon keyword END or FIN
   
 File in:   sector180deg.dat
 File out:  zgoubi.res

  Zgoubi, author's dvlpmnt version.
  Job  started  on  24-04-2020,  at  11:01:31 
  JOB  ENDED  ON    24-04-2020,  AT  11:01:31 

   CPU time, total :    0.20398200000000000     
