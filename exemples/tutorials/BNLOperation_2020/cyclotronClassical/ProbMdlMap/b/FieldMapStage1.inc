Uniform field sector. Test file & INCLUDE file FieldMapSector.inc.
'MARKER'   FieldMapSector_S                                               ! Just for edition purposes.
'OBJET'
64.62444403717985                      ! Reference Brho ("BORO" in the users' guide) -> 200keV proton.
2
2 1
10.011362 0. 0. 0. 0. 0.7745802 'o'          ! p[MeV/c]= 15.007, Brho[kG.cm]= 50.057, kin-E[MeV]=0.12.
67.997983 0. 0. 0. 0. 5.2610112 'o'          ! p[MeV/c]=101.926, Brho[kG.cm]=339.990, kin-E[MeV]=5.52.
1 1
'MARKER'   #S_halfDipole
'TOSCA'
0 2    ! IL=2 to log step-by-step coordinates, spin, etc., to zgoubi.plt (avoid, if CPU time matters). 
1. 1. 1. 1.       ! Normalization coefficients, for B, X, Y and Z coordinate values read from the map.
HEADER_8                                            ! The field map file starts with an 8-line header.
315 151 1 22.1 1.            ! IZ=1 for 2D map; MOD=22 for polar frame; .MOD2=.1 if only one map file.
geneSectorMap.out
0 0 0 0       ! Possible vertical boundaries within the field map, to start/stop stepwise integration. 
2
1. ! cm                                                                       ! Integration step size.
2                                                                        ! Magnet positionning option.
0. 0. 0. 0.                                                                     ! Magnet positionning.
'MARKER'   #E_halfDipole
'FAISCEAU'
'SYSTEM'                      ! This SYSTEM command runs gnuplot, for a graph of the two trajectories.
1
gnuplot <./gnuplot_Zplt.gnu
'MARKER'   FieldMapSector_E                                               ! Just for edition purposes.
'END' 
