set xtics mirror ;  set ytics mirror; set xlabel "{/Symbol q}  [rad]" ; set ylabel "x({/Symbol q})  [norm.]" 

nu = 1./7.  ; xh = 1. ; phi =0.

y(x)  = xh * cos( 2.*pi * nu    *x + phi)
ym(x) = xh * cos( 2.*pi *(1-nu) *x + phi)

print 'nu = ',nu
i=0
print 'i  y_nu(i) y_nu1(i) ', i, y(i), ym(i)
i=1
print 'i  y_nu(i) y_nu1(i) ', i, y(i), ym(i)
i=2
print 'i  y_nu(i) y_nu1(i) ', i, y(i), ym(i)
i=3
print 'i  y_nu(i) y_nu1(i) ', i, y(i), ym(i)
i=4
print 'i  y_nu(i) y_nu1(i) ', i, y(i), ym(i)

x=0
g(x)= xh * cos( 2.*pi * nu    *x + phi)
gm(x) = xh * cos( 2.*pi *(1-nu) *x + phi)
x=1
g(x)= xh * cos( 2.*pi * nu    *x + phi)
gm(x) = xh * cos( 2.*pi *(1-nu) *x + phi)
x=2
g(x)= xh * cos( 2.*pi * nu    *x + phi)
gm(x) = xh * cos( 2.*pi *(1-nu) *x + phi)
x=3
g(x)= xh * cos( 2.*pi * nu    *x + phi)
gm(x) = xh * cos( 2.*pi *(1-nu) *x + phi)
x=4
g(x)= xh * cos( 2.*pi * nu    *x + phi)
gm(x) = xh * cos( 2.*pi *(1-nu) *x + phi)
x=5
g(x)= xh * cos( 2.*pi * nu    *x + phi)
gm(x) = xh * cos( 2.*pi *(1-nu) *x + phi)

set sample 1000
plot [-.2:22]   y(x/(2.*pi*nu)) w l lw 3 lc rgb "red" notit, '+' u (0*2.*pi*nu):(g(0)) w p pt 1 ps 2 lw 4 lc rgb "red" notit , '+' u (1*2.*pi*nu):(g(1)) w p pt 1 ps 2 lw 4 lc rgb "red" notit , '+' u (2*2.*pi*nu):(g(2)) w p pt 1 ps 2 lw 4 lc rgb "red" notit , '+' u (3*2.*pi*nu):(g(3)) w p pt 1 ps 2 lw 4 lc rgb "red" notit , '+' u (4*2.*pi*nu):(g(4)) w p pt 1 ps 2 lw 4 lc rgb "red" notit ,\
ym(x/(2.*pi*nu)) w l lw 2 lc rgb "blue" notit,'+' u (0*2.*pi*(1-nu)):(gm(0)) w p pt 4 ps 2 lw 3 lc rgb "blue" notit , '+' u (1*2.*pi*(1-nu)):(gm(1)) w p pt 4 ps 2 lw 3 lc rgb "blue" notit, '+' u (2*2.*pi*(1-nu)):(gm(2)) w p pt 4 ps 2 lw 3 lc rgb "blue" notit, '+' u (3*2.*pi*(1-nu)):(gm(3)) w p pt 4 ps 2 lw 3 lc rgb "blue" notit, '+' u (4*2.*pi*(1-nu)):(gm(4)) w p pt 4 ps 2 lw 3 lc rgb "blue" notit

pause 1

set terminal postscript eps blacktext color  enh  
set output "gnuplo_Nu1mNuIndet.eps"  
replot  
set terminal X11  
unset output   

exit



