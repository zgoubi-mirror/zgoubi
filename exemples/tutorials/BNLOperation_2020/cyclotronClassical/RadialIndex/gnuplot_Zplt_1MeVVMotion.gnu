set xtics ;  set ytics ; set xlabel "s  [m]" ; set ylabel "Z  [m]" ; cm2m = 0.01; unset colorbox ; set xrange [:36]
plot for [i=4:1:-1] "zgoubi.plt" u ($19==i && $42 >10? $14 *cm2m :1/0):($12 *cm2m ):($19) w l lw 2 tit "P[mrad]=0.".(i-1)

set terminal postscript eps blacktext color  enh  
set output "gnuplot_Zplt_cycloClass_traj_k.eps"  
replot  
set terminal X11  
unset output   
pause 1

set xtics;  set ytics; set xlabel "s  [m]"; set ylabel "Y  [m]"; cm2m = 0.01; unset colorbox # ; set yrange [:.2641510025]
plot for [i=4:1:-1] "zgoubi.plt" u ($19==i && $42 >10? $14 *cm2m :1/0):($10 *cm2m ):($19) w l lw 2  tit "P[mrad]=0.".(i-1)

set terminal postscript eps blacktext color  enh  
set output "gnuplot_Zplt_cycloClass_trajY_k.eps"  
replot  
set terminal X11  
unset output   
pause 1
exit

