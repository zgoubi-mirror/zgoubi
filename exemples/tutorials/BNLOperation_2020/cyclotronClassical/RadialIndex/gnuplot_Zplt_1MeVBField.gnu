set xtics ;  set ytics ; set xlabel "s  [m]" ; set ylabel "B_Z  [T]" ; cm2m = 0.01; kG2T=0.1; unset colorbox
plot for [i=4:1:-1] "zgoubi.plt" u ($19==i && $42 >10? $14 *cm2m :1/0):($25 *kG2T) w l lw 2 tit "P[mrad]=0.".(i-1)

set terminal postscript eps blacktext color  enh  
set output "gnuplot_Zplt_cycloClass_BField_k.eps"  
replot  
set terminal X11  
unset output   
pause 1
exit

