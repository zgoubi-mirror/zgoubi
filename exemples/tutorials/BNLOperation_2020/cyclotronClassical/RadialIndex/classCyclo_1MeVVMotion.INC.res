Test field map of a 60 cyclotron sector, with radial index
 
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./1MeVVMotion.inc[1MeVVMotion_S,*:1MeVVMotio(n_inc, depth :  1 2)
'MARKER'   1MeVVMotion_S                                                   ! Just for edition purposes.      1
!                                            First stage: find closed orbit at 1 MeV, for some k value.
'OBJET'                                                                                                      2
64.62444403717985                       ! Reference Brho ("BORO" in the users' guide) -> 200keV proton.
1.1
1         1    1   4    1   1
0.        1.   0.  0.1  0.  1.
30.107900 0.   0.  0.   0. 2.2365445724 'm'             ! 1 MeV proton -> Brho/Brho_ref = 2.2365445724.
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.       3
'DIPOLE'                                                    ! Analytical definition of a dipole field.       4
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.       5
'FIT'                                         ! This matching procedure finds the closed orbit radius.       6
1   nofinal
2  40  0  .9            ! Variable : Y_0. Variation can be up to 90%
1  1e-15  99            ! Penalty; max numb of calls to function
3.1 1 2 #End 0. 1. 0    ! Constraint :  Y_final=Y_0
'FAISTORE'                                                                                                   7
initialRs.fai                                                       ! Log coordinates in initialRs.fai.
1
!                                                            Second stage: raytrace the four particles:
'OBJET'                                                                                                      8
64.62444403717985                       ! Reference Brho ("BORO" in the users' guide) -> 200keV proton.
3
1 999 1
1 999 1
1. 1. 1. 1. 1. 1. 1. '*'
0.  0.  0.  0.  0.  0.  0.
0
initialRs.fai
'FAISCEAU'                                                                ! Particle coordinates, here.      9
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.      10
'DIPOLE'                                                    ! Analytical definition of a dipole field.      11
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.      12
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.      13
'DIPOLE'                                                    ! Analytical definition of a dipole field.      14
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.      15
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.      16
'DIPOLE'                                                    ! Analytical definition of a dipole field.      17
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.      18
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.      19
'DIPOLE'                                                    ! Analytical definition of a dipole field.      20
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.      21
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.      22
'DIPOLE'                                                    ! Analytical definition of a dipole field.      23
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.      24
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.      25
'DIPOLE'                                                    ! Analytical definition of a dipole field.      26
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.      27
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.      28
'DIPOLE'                                                    ! Analytical definition of a dipole field.      29
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.      30
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.      31
'DIPOLE'                                                    ! Analytical definition of a dipole field.      32
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.      33
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.      34
'DIPOLE'                                                    ! Analytical definition of a dipole field.      35
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.      36
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.      37
'DIPOLE'                                                    ! Analytical definition of a dipole field.      38
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.      39
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.      40
'DIPOLE'                                                    ! Analytical definition of a dipole field.      41
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.      42
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.      43
'DIPOLE'                                                    ! Analytical definition of a dipole field.      44
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.      45
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.      46
'DIPOLE'                                                    ! Analytical definition of a dipole field.      47
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.      48
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.      49
'DIPOLE'                                                    ! Analytical definition of a dipole field.      50
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.      51
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.      52
'DIPOLE'                                                    ! Analytical definition of a dipole field.      53
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.      54
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.      55
'DIPOLE'                                                    ! Analytical definition of a dipole field.      56
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.      57
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.      58
'DIPOLE'                                                    ! Analytical definition of a dipole field.      59
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.      60
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.      61
'DIPOLE'                                                    ! Analytical definition of a dipole field.      62
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.      63
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.      64
'DIPOLE'                                                    ! Analytical definition of a dipole field.      65
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.      66
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.      67
'DIPOLE'                                                    ! Analytical definition of a dipole field.      68
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.      69
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.      70
'DIPOLE'                                                    ! Analytical definition of a dipole field.      71
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.      72
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.      73
'DIPOLE'                                                    ! Analytical definition of a dipole field.      74
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.      75
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.      76
'DIPOLE'                                                    ! Analytical definition of a dipole field.      77
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.      78
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.      79
'DIPOLE'                                                    ! Analytical definition of a dipole field.      80
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.      81
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.      82
'DIPOLE'                                                    ! Analytical definition of a dipole field.      83
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.      84
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.      85
'DIPOLE'                                                    ! Analytical definition of a dipole field.      86
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.      87
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.      88
'DIPOLE'                                                    ! Analytical definition of a dipole field.      89
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.      90
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.      91
'DIPOLE'                                                    ! Analytical definition of a dipole field.      92
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.      93
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.      94
'DIPOLE'                                                    ! Analytical definition of a dipole field.      95
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.      96
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.      97
'DIPOLE'                                                    ! Analytical definition of a dipole field.      98
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.      99
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     100
'DIPOLE'                                                    ! Analytical definition of a dipole field.     101
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     102
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     103
'DIPOLE'                                                    ! Analytical definition of a dipole field.     104
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     105
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     106
'DIPOLE'                                                    ! Analytical definition of a dipole field.     107
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     108
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     109
'DIPOLE'                                                    ! Analytical definition of a dipole field.     110
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     111
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     112
'DIPOLE'                                                    ! Analytical definition of a dipole field.     113
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     114
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     115
'DIPOLE'                                                    ! Analytical definition of a dipole field.     116
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     117
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     118
'DIPOLE'                                                    ! Analytical definition of a dipole field.     119
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     120
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     121
'DIPOLE'                                                    ! Analytical definition of a dipole field.     122
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     123
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     124
'DIPOLE'                                                    ! Analytical definition of a dipole field.     125
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     126
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     127
'DIPOLE'                                                    ! Analytical definition of a dipole field.     128
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     129
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     130
'DIPOLE'                                                    ! Analytical definition of a dipole field.     131
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     132
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     133
'DIPOLE'                                                    ! Analytical definition of a dipole field.     134
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     135
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     136
'DIPOLE'                                                    ! Analytical definition of a dipole field.     137
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     138
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     139
'DIPOLE'                                                    ! Analytical definition of a dipole field.     140
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     141
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     142
'DIPOLE'                                                    ! Analytical definition of a dipole field.     143
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     144
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     145
'DIPOLE'                                                    ! Analytical definition of a dipole field.     146
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     147
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     148
'DIPOLE'                                                    ! Analytical definition of a dipole field.     149
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     150
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     151
'DIPOLE'                                                    ! Analytical definition of a dipole field.     152
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     153
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     154
'DIPOLE'                                                    ! Analytical definition of a dipole field.     155
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     156
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     157
'DIPOLE'                                                    ! Analytical definition of a dipole field.     158
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     159
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     160
'DIPOLE'                                                    ! Analytical definition of a dipole field.     161
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     162
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     163
'DIPOLE'                                                    ! Analytical definition of a dipole field.     164
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     165
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     166
'DIPOLE'                                                    ! Analytical definition of a dipole field.     167
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     168
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     169
'DIPOLE'                                                    ! Analytical definition of a dipole field.     170
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     171
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     172
'DIPOLE'                                                    ! Analytical definition of a dipole field.     173
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     174
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     175
'DIPOLE'                                                    ! Analytical definition of a dipole field.     176
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     177
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     178
'DIPOLE'                                                    ! Analytical definition of a dipole field.     179
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     180
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     181
'DIPOLE'                                                    ! Analytical definition of a dipole field.     182
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     183
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     184
'DIPOLE'                                                    ! Analytical definition of a dipole field.     185
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     186
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     187
'DIPOLE'                                                    ! Analytical definition of a dipole field.     188
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     189
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     190
'DIPOLE'                                                    ! Analytical definition of a dipole field.     191
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     192
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     193
'DIPOLE'                                                    ! Analytical definition of a dipole field.     194
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     195
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     196
'DIPOLE'                                                    ! Analytical definition of a dipole field.     197
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     198
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     199
'DIPOLE'                                                    ! Analytical definition of a dipole field.     200
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     201
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     202
'DIPOLE'                                                    ! Analytical definition of a dipole field.     203
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     204
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     205
'DIPOLE'                                                    ! Analytical definition of a dipole field.     206
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     207
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     208
'DIPOLE'                                                    ! Analytical definition of a dipole field.     209
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     210
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     211
'DIPOLE'                                                    ! Analytical definition of a dipole field.     212
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     213
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     214
'DIPOLE'                                                    ! Analytical definition of a dipole field.     215
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     216
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     217
'DIPOLE'                                                    ! Analytical definition of a dipole field.     218
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     219
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     220
'DIPOLE'                                                    ! Analytical definition of a dipole field.     221
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     222
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     223
'DIPOLE'                                                    ! Analytical definition of a dipole field.     224
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     225
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     226
'DIPOLE'                                                    ! Analytical definition of a dipole field.     227
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     228
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     229
'DIPOLE'                                                    ! Analytical definition of a dipole field.     230
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     231
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     232
'DIPOLE'                                                    ! Analytical definition of a dipole field.     233
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     234
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     235
'DIPOLE'                                                    ! Analytical definition of a dipole field.     236
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     237
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     238
'DIPOLE'                                                    ! Analytical definition of a dipole field.     239
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     240
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     241
'DIPOLE'                                                    ! Analytical definition of a dipole field.     242
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     243
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     244
'DIPOLE'                                                    ! Analytical definition of a dipole field.     245
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     246
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     247
'DIPOLE'                                                    ! Analytical definition of a dipole field.     248
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     249
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     250
'DIPOLE'                                                    ! Analytical definition of a dipole field.     251
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     252
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     253
'DIPOLE'                                                    ! Analytical definition of a dipole field.     254
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     255
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     256
'DIPOLE'                                                    ! Analytical definition of a dipole field.     257
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     258
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     259
'DIPOLE'                                                    ! Analytical definition of a dipole field.     260
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     261
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     262
'DIPOLE'                                                    ! Analytical definition of a dipole field.     263
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     264
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     265
'DIPOLE'                                                    ! Analytical definition of a dipole field.     266
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     267
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     268
'DIPOLE'                                                    ! Analytical definition of a dipole field.     269
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     270
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     271
'DIPOLE'                                                    ! Analytical definition of a dipole field.     272
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     273
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     274
'DIPOLE'                                                    ! Analytical definition of a dipole field.     275
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     276
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     277
'DIPOLE'                                                    ! Analytical definition of a dipole field.     278
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     279
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     280
'DIPOLE'                                                    ! Analytical definition of a dipole field.     281
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     282
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     283
'DIPOLE'                                                    ! Analytical definition of a dipole field.     284
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     285
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     286
'DIPOLE'                                                    ! Analytical definition of a dipole field.     287
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     288
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     289
'DIPOLE'                                                    ! Analytical definition of a dipole field.     290
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     291
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     292
'DIPOLE'                                                    ! Analytical definition of a dipole field.     293
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     294
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     295
'DIPOLE'                                                    ! Analytical definition of a dipole field.     296
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     297
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     298
'DIPOLE'                                                    ! Analytical definition of a dipole field.     299
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     300
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     301
'DIPOLE'                                                    ! Analytical definition of a dipole field.     302
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     303
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     304
'DIPOLE'                                                    ! Analytical definition of a dipole field.     305
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     306
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     307
'DIPOLE'                                                    ! Analytical definition of a dipole field.     308
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     309
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     310
'DIPOLE'                                                    ! Analytical definition of a dipole field.     311
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     312
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     313
'DIPOLE'                                                    ! Analytical definition of a dipole field.     314
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     315
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     316
'DIPOLE'                                                    ! Analytical definition of a dipole field.     317
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     318
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     319
'DIPOLE'                                                    ! Analytical definition of a dipole field.     320
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     321
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     322
'DIPOLE'                                                    ! Analytical definition of a dipole field.     323
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     324
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     325
'DIPOLE'                                                    ! Analytical definition of a dipole field.     326
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     327
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     328
'DIPOLE'                                                    ! Analytical definition of a dipole field.     329
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     330
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     331
'DIPOLE'                                                    ! Analytical definition of a dipole field.     332
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     333
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     334
'DIPOLE'                                                    ! Analytical definition of a dipole field.     335
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     336
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     337
'DIPOLE'                                                    ! Analytical definition of a dipole field.     338
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     339
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     340
'DIPOLE'                                                    ! Analytical definition of a dipole field.     341
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     342
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     343
'DIPOLE'                                                    ! Analytical definition of a dipole field.     344
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     345
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     346
'DIPOLE'                                                    ! Analytical definition of a dipole field.     347
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     348
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     349
'DIPOLE'                                                    ! Analytical definition of a dipole field.     350
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     351
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     352
'DIPOLE'                                                    ! Analytical definition of a dipole field.     353
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     354
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     355
'DIPOLE'                                                    ! Analytical definition of a dipole field.     356
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     357
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     358
'DIPOLE'                                                    ! Analytical definition of a dipole field.     359
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     360
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     361
'DIPOLE'                                                    ! Analytical definition of a dipole field.     362
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     363
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     364
'DIPOLE'                                                    ! Analytical definition of a dipole field.     365
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     366
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.     367
'DIPOLE'                                                    ! Analytical definition of a dipole field.     368
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.     369
'FAISCEAU'                                                                ! Particle coordinates, here.    370
'SYSTEM'                                                                                                   371
2
gnuplot <./gnuplot_Zplt_1MeVVMotion.gnu
gnuplot <./gnuplot_Zplt_1MeVBField.gnu
! Include_SegmentEnd : ./1MeVVMotion.inc                             (had n_inc, depth :  1 2)
'MARKER'   1MeVVMotion_E                                                    ! Just for edition purposes.   372
 
'END'

************************************************************************************************************************************
      1  Keyword, label(s) :  MARKER      1MeVVMotion_S                                                                IPASS= 1


************************************************************************************************************************************
      2  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (1)  BUILT  UP  FROM       4 POINTS 



                                 D         Y(cm)       T(mrd)      Z(cm)       P(mrd)      X(cm)

                     NUMBER         1           1           1           1           4           0


                    SAMPLING      1.0000      0.00        1.00        0.00        0.10        0.00


                                         TRAJECTOIRY SETTING UP

                              OBJET  (1)  BUILT  UP  FROM       4 POINTS 



                                 D         Y(cm)       T(mrd)      Z(cm)       P(mrd)      X(cm)

                     NUMBER         1           1           1           1           4           0


                    SAMPLING      1.0000      0.00        1.00        0.00        0.10        0.00


************************************************************************************************************************************
      3  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
      4  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


                OPEN FILE zgoubi.plt                                                                      
                FOR PRINTING TRAJECTORIES

                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    30.108     0.000     0.000     0.000            1.047    29.304    -0.047     0.000     0.000            1
  A    1  2.2365    30.108     0.000     0.000     0.100            1.047    29.304    -0.047     0.003     0.000            2
  A    1  2.2365    30.108     0.000     0.000     0.200            1.047    29.304    -0.047     0.006     0.000            3
  A    1  2.2365    30.108     0.000     0.000     0.300            1.047    29.304    -0.047     0.009     0.000            4


                CONDITIONS  DE  MAXWELL  (      252.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.523598776     m ;  Time  (for ref. rigidity & particle) =            NaN s 

************************************************************************************************************************************
      5  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
      6  Keyword, label(s) :  FIT                                                                                      IPASS= 1

     Pgm main. FITING=.T., FIT procedure launched.

           variable #            1       IR =            2 ,   ok.
           variable #            1       IP =           40 ,   ok.
           constraint #            1       IR =            5 ,   ok.
           constraint #            1       I  =            1 ,   ok.

                    FIT  variables  and  constraints  in  good  order,  FIT  will proceed. 
 STATUS OF VARIABLES  (Iteration #    13 /     99 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    40    3.01        28.5       28.539607       57.2      3.777E-08  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     5    0.000000E+00    1.000E+00    1.398914E-08    1.00E+00 MARKER     -                    -                    0
 Fit reached penalty value   1.9570E-16



   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      7  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 1


                OPEN FILE initialRs.fai                                                                   
                FOR PRINTING COORDINATES 

               Print will occur at element[s] labeled : 


************************************************************************************************************************************
      8  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

          Now reading file header  (4 lines) : 

# COORDINATES  - STORAGE FILE, 28-05-2020 13:26:08. Test field map of a 60 cyclotron sector, with radial index
#  1 ,  2   , 3 ,  4 , 5 ,  6  ,7 ,   8  ,  9   , 10,  11, 12,  13, 14, 15  ,  16 ,  17 ,  18 , 19   ,    20  , 21 ,  22 ,  23 ,    24   ,   25 ,    26,  27 ,  28  ,   29  ,30,  31  ,   32 , unused,   34 ,   35 ,    36,  37  , 38   , 39  ,  40   ,  41/lbl1, 42/lbl2,  43
# KEX,  Do-1, Yo,  To, Zo,  Po, So,   to,   D-1,  Y,   T,  Z,   P,  S, time,   SXo,  SYo,  SZo, modSo,    SX,   SY,   SZ,   modS,   ENEKI,   ENERG,  IT,  IREP, SORT,    M,  Q,    G,     tau, unused,   RET,   DPR,    PS,  BORO, IPASS, NOEL,   KLEY,  LABEL1, LABEL2,    LE
# int, float, cm, mrd, cm, mrd, cm, mu_s, float, cm, mrd, cm, mrd, cm, mu_s, float,float,float, float, float,float,float,float,     MeV,   MeV, int,  int,   cm, MeV/c2, C, float, float,  float, float, float, float, kG.cm,   int,  int, string,  string, string, string,  M
          Header reading went on ok, it seems. Now proceeding... 


      Reading in file initialRs.fai has ended after gathering       4 particles in requested range :  [      1,     999]

************************************************************************************************************************************
      9  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #      8)
                                                  4 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

O  1   2.2365    28.540     0.000     0.000     0.000      0.0000    1.2365   28.540   -0.000    0.000    0.000   2.988661E+01     1
O  1   2.2365    28.540     0.000     0.000     0.100      0.0000    1.2365   28.540   -0.000    0.003    0.099   2.988661E+01     2
O  1   2.2365    28.540     0.000     0.000     0.200      0.0000    1.2365   28.540   -0.000    0.006    0.198   2.988661E+01     3
O  1   2.2365    28.540     0.000     0.000     0.300      0.0000    1.2365   28.540   -0.000    0.009    0.297   2.988661E+01     4


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   2.0232E-22  -5.9058E+05   9.7140E+04   2.853961E-01  -1.611821E-08        4        4    1.000      (Y,T)         1
          NaN   0.0000E+00   1.0000E+00   4.469152E-05   1.486117E-04        4        0    0.000      (Z,P)         1
   0.0000E+00   0.0000E+00   1.0000E+00   9.969099E-04   4.333064E+01        4        0    0.000      (t,K)         1

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   2.501149E-09
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   1.520615E-08

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =            NaN
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =            NaN

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   6.255744E-18   3.803283E-17  -7.984291E-14  -2.654998E-13
   3.803283E-17   2.312269E-16  -4.854182E-13  -1.614150E-12
  -7.984291E-14  -4.854182E-13   1.109629E-09   3.689824E-09
  -2.654998E-13  -1.614150E-12   3.689824E-09   1.226969E-08

      sqrt(det_Y), sqrt(det_Z) :     6.439934E-23             NaN    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     10  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
     11  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000     0.006     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000     0.012     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000     0.018     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.523598776     m ;  Time  (for ref. rigidity & particle) =   8.460222E-08 s 

************************************************************************************************************************************
     12  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
     13  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
     14  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000     0.009     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000     0.017     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000     0.026     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    1.04719755     m ;  Time  (for ref. rigidity & particle) =   1.692044E-07 s 

************************************************************************************************************************************
     15  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
     16  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
     17  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000     0.011     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000     0.023     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000     0.034     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    1.57079633     m ;  Time  (for ref. rigidity & particle) =   2.538067E-07 s 

************************************************************************************************************************************
     18  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
     19  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
     20  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000     0.014     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000     0.028     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000     0.041     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    2.09439510     m ;  Time  (for ref. rigidity & particle) =   3.384089E-07 s 

************************************************************************************************************************************
     21  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
     22  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
     23  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000     0.016     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000     0.032     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000     0.048     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    2.61799388     m ;  Time  (for ref. rigidity & particle) =   4.230111E-07 s 

************************************************************************************************************************************
     24  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
     25  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
     26  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000     0.018     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000     0.036     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000     0.054     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    3.14159265     m ;  Time  (for ref. rigidity & particle) =   5.076133E-07 s 

************************************************************************************************************************************
     27  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
     28  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
     29  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000     0.019     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000     0.039     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000     0.058     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    3.66519143     m ;  Time  (for ref. rigidity & particle) =   5.922155E-07 s 

************************************************************************************************************************************
     30  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
     31  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
     32  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000     0.021     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000     0.041     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000     0.062     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    4.18879020     m ;  Time  (for ref. rigidity & particle) =   6.768178E-07 s 

************************************************************************************************************************************
     33  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
     34  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
     35  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000     0.021     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000     0.043     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000     0.064     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    4.71238898     m ;  Time  (for ref. rigidity & particle) =   7.614200E-07 s 

************************************************************************************************************************************
     36  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
     37  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
     38  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000     0.022     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000     0.044     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000     0.066     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    5.23598776     m ;  Time  (for ref. rigidity & particle) =   8.460222E-07 s 

************************************************************************************************************************************
     39  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
     40  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
     41  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000     0.022    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000     0.044    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000     0.066    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    5.75958653     m ;  Time  (for ref. rigidity & particle) =   9.306244E-07 s 

************************************************************************************************************************************
     42  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
     43  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
     44  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000     0.022    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000     0.043    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000     0.065    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    6.28318531     m ;  Time  (for ref. rigidity & particle) =   1.015227E-06 s 

************************************************************************************************************************************
     45  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
     46  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
     47  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000     0.021    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000     0.041    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000     0.062    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    6.80678408     m ;  Time  (for ref. rigidity & particle) =   1.099829E-06 s 

************************************************************************************************************************************
     48  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
     49  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
     50  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000     0.020    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000     0.039    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000     0.059    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    7.33038286     m ;  Time  (for ref. rigidity & particle) =   1.184431E-06 s 

************************************************************************************************************************************
     51  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
     52  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
     53  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000     0.018    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000     0.036    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000     0.054    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    7.85398163     m ;  Time  (for ref. rigidity & particle) =   1.269033E-06 s 

************************************************************************************************************************************
     54  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
     55  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
     56  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000     0.016    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000     0.032    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000     0.048    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    8.37758041     m ;  Time  (for ref. rigidity & particle) =   1.353636E-06 s 

************************************************************************************************************************************
     57  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
     58  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
     59  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000     0.014    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000     0.028    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000     0.042    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    8.90117919     m ;  Time  (for ref. rigidity & particle) =   1.438238E-06 s 

************************************************************************************************************************************
     60  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
     61  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
     62  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000     0.012    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000     0.023    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000     0.035    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    9.42477796     m ;  Time  (for ref. rigidity & particle) =   1.522840E-06 s 

************************************************************************************************************************************
     63  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
     64  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
     65  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000     0.009    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000     0.018    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000     0.027    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    9.94837674     m ;  Time  (for ref. rigidity & particle) =   1.607442E-06 s 

************************************************************************************************************************************
     66  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
     67  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
     68  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000     0.006    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000     0.012    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000     0.018    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    10.4719755     m ;  Time  (for ref. rigidity & particle) =   1.692044E-06 s 

************************************************************************************************************************************
     69  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
     70  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
     71  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000     0.003    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000     0.006    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000     0.010    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    10.9955743     m ;  Time  (for ref. rigidity & particle) =   1.776647E-06 s 

************************************************************************************************************************************
     72  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
     73  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
     74  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000     0.000    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000     0.000    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000     0.001    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    11.5191731     m ;  Time  (for ref. rigidity & particle) =   1.861249E-06 s 

************************************************************************************************************************************
     75  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
     76  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
     77  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000    -0.003    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000    -0.006    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000    -0.008    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    12.0427718     m ;  Time  (for ref. rigidity & particle) =   1.945851E-06 s 

************************************************************************************************************************************
     78  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
     79  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
     80  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000    -0.006    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000    -0.011    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000    -0.017    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    12.5663706     m ;  Time  (for ref. rigidity & particle) =   2.030453E-06 s 

************************************************************************************************************************************
     81  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
     82  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
     83  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000    -0.009    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000    -0.017    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000    -0.026    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    13.0899694     m ;  Time  (for ref. rigidity & particle) =   2.115055E-06 s 

************************************************************************************************************************************
     84  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
     85  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
     86  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000    -0.011    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000    -0.022    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000    -0.034    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    13.6135682     m ;  Time  (for ref. rigidity & particle) =   2.199658E-06 s 

************************************************************************************************************************************
     87  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
     88  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
     89  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000    -0.014    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000    -0.027    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000    -0.041    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    14.1371669     m ;  Time  (for ref. rigidity & particle) =   2.284260E-06 s 

************************************************************************************************************************************
     90  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
     91  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
     92  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000    -0.016    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000    -0.032    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000    -0.048    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    14.6607657     m ;  Time  (for ref. rigidity & particle) =   2.368862E-06 s 

************************************************************************************************************************************
     93  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
     94  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
     95  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000    -0.018    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000    -0.036    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000    -0.053    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    15.1843645     m ;  Time  (for ref. rigidity & particle) =   2.453464E-06 s 

************************************************************************************************************************************
     96  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
     97  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
     98  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000    -0.019    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000    -0.039    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000    -0.058    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    15.7079633     m ;  Time  (for ref. rigidity & particle) =   2.538067E-06 s 

************************************************************************************************************************************
     99  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    100  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    101  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000    -0.021    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000    -0.041    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000    -0.062    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    16.2315620     m ;  Time  (for ref. rigidity & particle) =   2.622669E-06 s 

************************************************************************************************************************************
    102  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    103  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    104  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000    -0.021    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000    -0.043    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000    -0.064    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    16.7551608     m ;  Time  (for ref. rigidity & particle) =   2.707271E-06 s 

************************************************************************************************************************************
    105  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    106  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    107  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000    -0.022    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000    -0.044    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000    -0.066    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    17.2787596     m ;  Time  (for ref. rigidity & particle) =   2.791873E-06 s 

************************************************************************************************************************************
    108  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    109  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    110  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000    -0.022     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000    -0.044     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000    -0.066     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    17.8023584     m ;  Time  (for ref. rigidity & particle) =   2.876475E-06 s 

************************************************************************************************************************************
    111  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    112  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    113  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000    -0.022     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000    -0.043     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000    -0.065     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    18.3259571     m ;  Time  (for ref. rigidity & particle) =   2.961078E-06 s 

************************************************************************************************************************************
    114  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    115  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    116  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000    -0.021     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000    -0.042     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000    -0.062     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    18.8495559     m ;  Time  (for ref. rigidity & particle) =   3.045680E-06 s 

************************************************************************************************************************************
    117  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    118  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    119  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000    -0.020     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000    -0.039     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000    -0.059     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    19.3731547     m ;  Time  (for ref. rigidity & particle) =   3.130282E-06 s 

************************************************************************************************************************************
    120  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    121  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    122  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000    -0.018     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000    -0.036     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000    -0.054     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    19.8967535     m ;  Time  (for ref. rigidity & particle) =   3.214884E-06 s 

************************************************************************************************************************************
    123  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    124  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    125  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000    -0.016     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000    -0.033     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000    -0.049     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    20.4203522     m ;  Time  (for ref. rigidity & particle) =   3.299487E-06 s 

************************************************************************************************************************************
    126  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    127  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    128  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000    -0.014     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000    -0.028     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000    -0.042     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    20.9439510     m ;  Time  (for ref. rigidity & particle) =   3.384089E-06 s 

************************************************************************************************************************************
    129  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    130  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    131  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000    -0.012     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000    -0.023     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000    -0.035     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    21.4675498     m ;  Time  (for ref. rigidity & particle) =   3.468691E-06 s 

************************************************************************************************************************************
    132  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    133  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    134  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000    -0.009     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000    -0.018     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000    -0.027     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    21.9911486     m ;  Time  (for ref. rigidity & particle) =   3.553293E-06 s 

************************************************************************************************************************************
    135  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    136  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    137  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000    -0.006     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000    -0.013     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000    -0.019     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    22.5147474     m ;  Time  (for ref. rigidity & particle) =   3.637895E-06 s 

************************************************************************************************************************************
    138  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    139  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    140  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000    -0.003     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000    -0.007     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000    -0.010     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    23.0383461     m ;  Time  (for ref. rigidity & particle) =   3.722498E-06 s 

************************************************************************************************************************************
    141  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    142  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    143  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000    -0.000     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000    -0.001     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000    -0.001     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    23.5619449     m ;  Time  (for ref. rigidity & particle) =   3.807100E-06 s 

************************************************************************************************************************************
    144  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    145  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    146  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000     0.003     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000     0.005     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000     0.008     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    24.0855437     m ;  Time  (for ref. rigidity & particle) =   3.891702E-06 s 

************************************************************************************************************************************
    147  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    148  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    149  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000     0.005     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000     0.011     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000     0.016     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    24.6091425     m ;  Time  (for ref. rigidity & particle) =   3.976304E-06 s 

************************************************************************************************************************************
    150  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    151  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    152  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000     0.008     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000     0.017     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000     0.025     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    25.1327412     m ;  Time  (for ref. rigidity & particle) =   4.060907E-06 s 

************************************************************************************************************************************
    153  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    154  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    155  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000     0.011     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000     0.022     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000     0.033     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    25.6563400     m ;  Time  (for ref. rigidity & particle) =   4.145509E-06 s 

************************************************************************************************************************************
    156  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    157  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    158  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000     0.013     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000     0.027     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000     0.040     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    26.1799388     m ;  Time  (for ref. rigidity & particle) =   4.230111E-06 s 

************************************************************************************************************************************
    159  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    160  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    161  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000     0.016     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000     0.031     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000     0.047     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    26.7035376     m ;  Time  (for ref. rigidity & particle) =   4.314713E-06 s 

************************************************************************************************************************************
    162  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    163  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    164  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000     0.018     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000     0.035     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000     0.053     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    27.2271363     m ;  Time  (for ref. rigidity & particle) =   4.399315E-06 s 

************************************************************************************************************************************
    165  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    166  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    167  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000     0.019     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000     0.038     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000     0.058     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    27.7507351     m ;  Time  (for ref. rigidity & particle) =   4.483918E-06 s 

************************************************************************************************************************************
    168  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    169  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    170  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000     0.021     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000     0.041     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000     0.062     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    28.2743339     m ;  Time  (for ref. rigidity & particle) =   4.568520E-06 s 

************************************************************************************************************************************
    171  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    172  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    173  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000     0.021     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000     0.043     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000     0.064     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    28.7979327     m ;  Time  (for ref. rigidity & particle) =   4.653122E-06 s 

************************************************************************************************************************************
    174  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    175  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    176  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000     0.022     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000     0.044     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000     0.066     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    29.3215314     m ;  Time  (for ref. rigidity & particle) =   4.737724E-06 s 

************************************************************************************************************************************
    177  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    178  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    179  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000     0.022    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000     0.044    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000     0.066    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    29.8451302     m ;  Time  (for ref. rigidity & particle) =   4.822326E-06 s 

************************************************************************************************************************************
    180  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    181  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    182  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000     0.022    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000     0.043    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000     0.065    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    30.3687290     m ;  Time  (for ref. rigidity & particle) =   4.906929E-06 s 

************************************************************************************************************************************
    183  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    184  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    185  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000     0.021    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000     0.042    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000     0.063    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    30.8923278     m ;  Time  (for ref. rigidity & particle) =   4.991531E-06 s 

************************************************************************************************************************************
    186  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    187  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    188  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000     0.020    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000     0.039    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000     0.059    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    31.4159265     m ;  Time  (for ref. rigidity & particle) =   5.076133E-06 s 

************************************************************************************************************************************
    189  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    190  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    191  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000     0.018    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000     0.037    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000     0.055    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    31.9395253     m ;  Time  (for ref. rigidity & particle) =   5.160735E-06 s 

************************************************************************************************************************************
    192  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    193  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    194  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000     0.016    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000     0.033    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000     0.049    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    32.4631241     m ;  Time  (for ref. rigidity & particle) =   5.245338E-06 s 

************************************************************************************************************************************
    195  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    196  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    197  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000     0.014    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000     0.029    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000     0.043    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    32.9867229     m ;  Time  (for ref. rigidity & particle) =   5.329940E-06 s 

************************************************************************************************************************************
    198  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    199  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    200  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000     0.012    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000     0.024    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000     0.036    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    33.5103216     m ;  Time  (for ref. rigidity & particle) =   5.414542E-06 s 

************************************************************************************************************************************
    201  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    202  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    203  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000     0.009    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000     0.019    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000     0.028    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    34.0339204     m ;  Time  (for ref. rigidity & particle) =   5.499144E-06 s 

************************************************************************************************************************************
    204  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    205  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    206  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000     0.007    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000     0.013    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000     0.020    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    34.5575192     m ;  Time  (for ref. rigidity & particle) =   5.583746E-06 s 

************************************************************************************************************************************
    207  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    208  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    209  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000     0.004    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000     0.007    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000     0.011    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    35.0811180     m ;  Time  (for ref. rigidity & particle) =   5.668349E-06 s 

************************************************************************************************************************************
    210  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    211  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    212  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000     0.001    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000     0.001    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000     0.002    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    35.6047167     m ;  Time  (for ref. rigidity & particle) =   5.752951E-06 s 

************************************************************************************************************************************
    213  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    214  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    215  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000    -0.002    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000    -0.005    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000    -0.007    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    36.1283155     m ;  Time  (for ref. rigidity & particle) =   5.837553E-06 s 

************************************************************************************************************************************
    216  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    217  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    218  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000    -0.005    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000    -0.011    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000    -0.016    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    36.6519143     m ;  Time  (for ref. rigidity & particle) =   5.922155E-06 s 

************************************************************************************************************************************
    219  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    220  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    221  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000    -0.008    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000    -0.016    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000    -0.024    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    37.1755131     m ;  Time  (for ref. rigidity & particle) =   6.006758E-06 s 

************************************************************************************************************************************
    222  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    223  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    224  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000    -0.011    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000    -0.022    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000    -0.032    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    37.6991118     m ;  Time  (for ref. rigidity & particle) =   6.091360E-06 s 

************************************************************************************************************************************
    225  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    226  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    227  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000    -0.013    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000    -0.027    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000    -0.040    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    38.2227106     m ;  Time  (for ref. rigidity & particle) =   6.175962E-06 s 

************************************************************************************************************************************
    228  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    229  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    230  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000    -0.016    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000    -0.031    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000    -0.047    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    38.7463094     m ;  Time  (for ref. rigidity & particle) =   6.260564E-06 s 

************************************************************************************************************************************
    231  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    232  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    233  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000    -0.018    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000    -0.035    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000    -0.053    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    39.2699082     m ;  Time  (for ref. rigidity & particle) =   6.345166E-06 s 

************************************************************************************************************************************
    234  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    235  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    236  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000    -0.019    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000    -0.038    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000    -0.057    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    39.7935069     m ;  Time  (for ref. rigidity & particle) =   6.429769E-06 s 

************************************************************************************************************************************
    237  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    238  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    239  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000    -0.020    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000    -0.041    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000    -0.061    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    40.3171057     m ;  Time  (for ref. rigidity & particle) =   6.514371E-06 s 

************************************************************************************************************************************
    240  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    241  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    242  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000    -0.021    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000    -0.043    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000    -0.064    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    40.8407045     m ;  Time  (for ref. rigidity & particle) =   6.598973E-06 s 

************************************************************************************************************************************
    243  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    244  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    245  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000    -0.022    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000    -0.044    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000    -0.066    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    41.3643033     m ;  Time  (for ref. rigidity & particle) =   6.683575E-06 s 

************************************************************************************************************************************
    246  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    247  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    248  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000    -0.022     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000    -0.044     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000    -0.066     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    41.8879020     m ;  Time  (for ref. rigidity & particle) =   6.768178E-06 s 

************************************************************************************************************************************
    249  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    250  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    251  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000    -0.022     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000    -0.043     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000    -0.065     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    42.4115008     m ;  Time  (for ref. rigidity & particle) =   6.852780E-06 s 

************************************************************************************************************************************
    252  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    253  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    254  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000    -0.021     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000    -0.042     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000    -0.063     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    42.9350996     m ;  Time  (for ref. rigidity & particle) =   6.937382E-06 s 

************************************************************************************************************************************
    255  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    256  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    257  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000    -0.020     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000    -0.040     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000    -0.060     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    43.4586984     m ;  Time  (for ref. rigidity & particle) =   7.021984E-06 s 

************************************************************************************************************************************
    258  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    259  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    260  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000    -0.018     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000    -0.037     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000    -0.055     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    43.9822972     m ;  Time  (for ref. rigidity & particle) =   7.106586E-06 s 

************************************************************************************************************************************
    261  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    262  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    263  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000    -0.017     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000    -0.033     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000    -0.050     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    44.5058959     m ;  Time  (for ref. rigidity & particle) =   7.191189E-06 s 

************************************************************************************************************************************
    264  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    265  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    266  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000    -0.014     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000    -0.029     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000    -0.043     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    45.0294947     m ;  Time  (for ref. rigidity & particle) =   7.275791E-06 s 

************************************************************************************************************************************
    267  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    268  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    269  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000    -0.012     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000    -0.024     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000    -0.036     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    45.5530935     m ;  Time  (for ref. rigidity & particle) =   7.360393E-06 s 

************************************************************************************************************************************
    270  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    271  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    272  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000    -0.010     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000    -0.019     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000    -0.029     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    46.0766923     m ;  Time  (for ref. rigidity & particle) =   7.444995E-06 s 

************************************************************************************************************************************
    273  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    274  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    275  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000    -0.007     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000    -0.013     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000    -0.020     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    46.6002910     m ;  Time  (for ref. rigidity & particle) =   7.529597E-06 s 

************************************************************************************************************************************
    276  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    277  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    278  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000    -0.004     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000    -0.008     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000    -0.012     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    47.1238898     m ;  Time  (for ref. rigidity & particle) =   7.614200E-06 s 

************************************************************************************************************************************
    279  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    280  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    281  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000    -0.001     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000    -0.002     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000    -0.003     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    47.6474886     m ;  Time  (for ref. rigidity & particle) =   7.698802E-06 s 

************************************************************************************************************************************
    282  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    283  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    284  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000     0.002     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000     0.004     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000     0.006     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    48.1710874     m ;  Time  (for ref. rigidity & particle) =   7.783404E-06 s 

************************************************************************************************************************************
    285  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    286  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    287  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000     0.005     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000     0.010     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000     0.015     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    48.6946861     m ;  Time  (for ref. rigidity & particle) =   7.868006E-06 s 

************************************************************************************************************************************
    288  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    289  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    290  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000     0.008     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000     0.016     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000     0.024     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    49.2182849     m ;  Time  (for ref. rigidity & particle) =   7.952609E-06 s 

************************************************************************************************************************************
    291  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    292  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    293  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000     0.011     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000     0.021     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000     0.032     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    49.7418837     m ;  Time  (for ref. rigidity & particle) =   8.037211E-06 s 

************************************************************************************************************************************
    294  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    295  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    296  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000     0.013     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000     0.026     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000     0.039     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    50.2654825     m ;  Time  (for ref. rigidity & particle) =   8.121813E-06 s 

************************************************************************************************************************************
    297  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    298  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    299  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000     0.015     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000     0.031     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000     0.046     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    50.7890812     m ;  Time  (for ref. rigidity & particle) =   8.206415E-06 s 

************************************************************************************************************************************
    300  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    301  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    302  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000     0.017     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000     0.035     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000     0.052     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    51.3126800     m ;  Time  (for ref. rigidity & particle) =   8.291017E-06 s 

************************************************************************************************************************************
    303  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    304  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    305  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000     0.019     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000     0.038     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000     0.057     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    51.8362788     m ;  Time  (for ref. rigidity & particle) =   8.375620E-06 s 

************************************************************************************************************************************
    306  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    307  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    308  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000     0.020     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000     0.041     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000     0.061     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    52.3598776     m ;  Time  (for ref. rigidity & particle) =   8.460222E-06 s 

************************************************************************************************************************************
    309  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    310  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    311  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000     0.021     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000     0.043     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000     0.064     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    52.8834763     m ;  Time  (for ref. rigidity & particle) =   8.544824E-06 s 

************************************************************************************************************************************
    312  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    313  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    314  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000     0.022     0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000     0.044     0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000     0.065     0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    53.4070751     m ;  Time  (for ref. rigidity & particle) =   8.629426E-06 s 

************************************************************************************************************************************
    315  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    316  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    317  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000     0.022    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000     0.044    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000     0.066    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    53.9306739     m ;  Time  (for ref. rigidity & particle) =   8.714029E-06 s 

************************************************************************************************************************************
    318  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    319  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    320  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000     0.022    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000     0.043    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000     0.065    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    54.4542727     m ;  Time  (for ref. rigidity & particle) =   8.798631E-06 s 

************************************************************************************************************************************
    321  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    322  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    323  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000     0.021    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000     0.042    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000     0.063    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    54.9778714     m ;  Time  (for ref. rigidity & particle) =   8.883233E-06 s 

************************************************************************************************************************************
    324  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    325  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    326  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000     0.020    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000     0.040    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000     0.060    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    55.5014702     m ;  Time  (for ref. rigidity & particle) =   8.967835E-06 s 

************************************************************************************************************************************
    327  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    328  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    329  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000     0.019    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000     0.037    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000     0.056    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    56.0250690     m ;  Time  (for ref. rigidity & particle) =   9.052437E-06 s 

************************************************************************************************************************************
    330  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    331  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    332  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000     0.017    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000     0.033    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000     0.050    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    56.5486678     m ;  Time  (for ref. rigidity & particle) =   9.137040E-06 s 

************************************************************************************************************************************
    333  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    334  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    335  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000     0.015    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000     0.029    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000     0.044    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    57.0722665     m ;  Time  (for ref. rigidity & particle) =   9.221642E-06 s 

************************************************************************************************************************************
    336  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    337  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    338  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000     0.012    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000     0.025    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000     0.037    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    57.5958653     m ;  Time  (for ref. rigidity & particle) =   9.306244E-06 s 

************************************************************************************************************************************
    339  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    340  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    341  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000     0.010    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000     0.019    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000     0.029    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    58.1194641     m ;  Time  (for ref. rigidity & particle) =   9.390846E-06 s 

************************************************************************************************************************************
    342  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    343  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    344  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000     0.007    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000     0.014    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000     0.021    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    58.6430629     m ;  Time  (for ref. rigidity & particle) =   9.475449E-06 s 

************************************************************************************************************************************
    345  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    346  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    347  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000     0.004    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000     0.008    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000     0.012    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    59.1666616     m ;  Time  (for ref. rigidity & particle) =   9.560051E-06 s 

************************************************************************************************************************************
    348  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    349  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    350  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000     0.001    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000     0.002    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000     0.003    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    59.6902604     m ;  Time  (for ref. rigidity & particle) =   9.644653E-06 s 

************************************************************************************************************************************
    351  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    352  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    353  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000    -0.002    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000    -0.004    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000    -0.006    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    60.2138592     m ;  Time  (for ref. rigidity & particle) =   9.729255E-06 s 

************************************************************************************************************************************
    354  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    355  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    356  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000    -0.005    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000    -0.010    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000    -0.015    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    60.7374580     m ;  Time  (for ref. rigidity & particle) =   9.813857E-06 s 

************************************************************************************************************************************
    357  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    358  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    359  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540    -0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540    -0.000    -0.008    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540    -0.000    -0.015    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540    -0.000    -0.023    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    61.2610567     m ;  Time  (for ref. rigidity & particle) =   9.898460E-06 s 

************************************************************************************************************************************
    360  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    361  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    362  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000    -0.010    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000    -0.021    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000    -0.031    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    61.7846555     m ;  Time  (for ref. rigidity & particle) =   9.983062E-06 s 

************************************************************************************************************************************
    363  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    364  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    365  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000    -0.013    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000    -0.026    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000    -0.039    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    62.3082543     m ;  Time  (for ref. rigidity & particle) =   1.006766E-05 s 

************************************************************************************************************************************
    366  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    367  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
    368  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  2.2365    28.540     0.000     0.000     0.000            1.047    28.540     0.000     0.000     0.000            1
  A    1  2.2365    28.540     0.000     0.000     0.100            1.047    28.540     0.000    -0.015    -0.000            2
  A    1  2.2365    28.540     0.000     0.000     0.200            1.047    28.540     0.000    -0.030    -0.000            3
  A    1  2.2365    28.540     0.000     0.000     0.300            1.047    28.540     0.000    -0.046    -0.000            4


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    62.8318531     m ;  Time  (for ref. rigidity & particle) =   1.015227E-05 s 

************************************************************************************************************************************
    369  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    370  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #    369)
                                                  4 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

O  1   2.2365    28.540     0.000     0.000     0.000      0.0000    1.2365   28.540    0.000    0.000    0.000   3.616279E+03     1
O  1   2.2365    28.540     0.000     0.000     0.100      0.0000    1.2365   28.540    0.000   -0.015   -0.072   3.616279E+03     2
O  1   2.2365    28.540     0.000     0.000     0.200      0.0000    1.2365   28.540    0.000   -0.030   -0.144   3.616279E+03     3
O  1   2.2365    28.540     0.000     0.000     0.300      0.0000    1.2365   28.540    0.000   -0.046   -0.216   3.616279E+03     4


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   2.8366E-23   1.2877E+02   5.9474E-01   2.853961E-01   1.039826E-09        4        4    1.000      (Y,T)         1
   1.8681E-13  -2.3057E+05   4.8821E+05  -2.285908E-04  -1.079574E-04        4        4    1.000      (Z,P)         1
   0.0000E+00   0.0000E+00   1.0000E+00   1.206261E-01   4.333064E+01        4        0    0.000      (t,K)         1

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   2.317318E-12
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   5.017390E-10

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   1.703817E-04
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   8.046656E-05

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   5.369963E-24  -1.162654E-21   3.787955E-16   1.788943E-16
  -1.162654E-21   2.517420E-19  -8.191893E-14  -3.868798E-14
   3.787955E-16  -8.191893E-14   2.902994E-08   1.371003E-08
   1.788943E-16  -3.868798E-14   1.371003E-08   6.474867E-09

      sqrt(det_Y), sqrt(det_Z) :     9.029106E-24    5.946237E-14    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
    371  Keyword, label(s) :  SYSTEM                                                                                   IPASS= 1

     Number  of  commands :   2,  as  follows : 

 gnuplot <./gnuplot_Zplt_1MeVVMotion.gnu
 gnuplot <./gnuplot_Zplt_1MeVBField.gnu

************************************************************************************************************************************
    372  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
    373  Keyword, label(s) :  END                                                                                      IPASS= 1


                             4 particles have been launched
                     Made  it  to  the  end :      4

************************************************************************************************************************************
 Pgm zgoubi : Execution ended normally, upon keyword END or FIN
   
 File in:   classCyclo_1MeVVMotion.INC.dat
 File out:  zgoubi.res

  Zgoubi, author's dvlpmnt version.
  Job  started  on  28-05-2020,  at  13:26:06 
  JOB  ENDED  ON    28-05-2020,  AT  13:26:20 

   CPU time, total :     2.7886400000000000     
