Test field map of a 60 cyclotron sector, with radial index
 
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./isochroLoss.inc[isoChroLoss_S,*:isoChroLos(n_inc, depth :  1 2)
'MARKER'   isoChroLoss_S                                                   ! Just for edition purposes.      1
'OBJET'                                                                                                      2
64.62444403717985                       ! Reference Brho ("BORO" in the users' guide) -> 200keV proton.
2
1 1
4.0039    0. 0. 0. 0. 0.3162126 'o'          ! p[MeV/c]= 6.126277, Brho[kG.cm]=20.435, kin-E[MeV]=0.02.
1
'PARTICUL'                                         ! Necessary as time of flight computation is needed.      3
PROTON
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./sectorWithIndex.inc[#S_60degSectorWIdx,*:#(n_inc, depth : x1*3)
'MARKER'    #S_60degSectorWIdx                                ! Label should not exceed 20 characters.       4
'DIPOLE'                                                    ! Analytical definition of a dipole field.       5
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                        ! Sector angle AT; reference radius RM.
30. 5. -0.03 0. 0.  ! Reference azimuthal angle ACN; BM field at RM; indices, N (=k=-0.03) at RM=50cm.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : ./sectorWithIndex.inc                         (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorWIdx                                ! Label should not exceed 20 characters.       6
'FIT2'                                         ! This matching procedure finds the closed orbit radius.      7
1   nofinal
2 30 0 [0.5,80.]         ! Variable : Y_0
1  1e-15  99            ! Penalty; max numb of calls to function
3.1 1 2 #End 0. 1. 0    ! Constraint :  Y_final=Y_0
'FAISCEAU'                                                                ! Particle coordinates, here.      8
'FAISTORE'                                                                                                   9
zgoubi.fai
1
'REBELOTE'              ! A do-loop. Repeat the above, after changing particle rigidity to a new value.     10
20 0.2  0 1            ! 20 diffrnt rigidities; I/O options; coordinates as from OBJET; changes follow:
1                                    ! Parameter 35 to be changed, in OBJET: relative momentum, namely,
OBJET 35  0.3162126:5.00639   ! Acceleration to 5MeV. Commented here, for use in subsequent exercises.
!OBJET 35  0.3162126:2.2365445724                              ! for energy scan from 0.02 MeV to 1 MeV.
'SYSTEM'                                                                                                    11
1
gnuplot <./gnuplot_Zfai_scanTrev.gnu                                            ! Plot revolution time.
! Include_SegmentEnd : ./isochroLoss.inc                             (had n_inc, depth :  1 2)
'MARKER'   isoChroLoss_E                                                   ! Just for edition purposes.     12
 
'END'

************************************************************************************************************************************
      1  Keyword, label(s) :  MARKER      isoChroLoss_S                                                                IPASS= 1


************************************************************************************************************************************
      2  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      3  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 1

     Particle  properties :
     PROTON
                     Mass          =    938.272        MeV/c2
                     Charge        =   1.602176E-19    C     
                     G  factor     =    1.79285              
                     COM life-time =   1.000000E+99    s     

              Reference  data :
                    mag. rigidity (kG.cm)   :   64.624444      =p/q, such that dev.=B*L/rigidity
                    mass (MeV/c2)           :   938.27208    
                    momentum (MeV/c)        :   19.373921    
                    energy, total (MeV)     :   938.47208    
                    energy, kinetic (MeV)   :  0.19999999    
                    beta = v/c              :  2.0644110049E-02
                    gamma                   :   1.000213158    
                    beta*gamma              :  2.0648510502E-02
                    G*gamma                 :   1.793229509    
                    electric rigidity (MeV) :  0.3999573557    =T[eV]*(gamma+1)/gamma, such that dev.=E*L/rigidity
  
 I, AMQ(1,I), AMQ(2,I)/QE, P/Pref, v/c, time, s :
  
     1   9.38272081E+02  1.00000000E+00  3.16212600E-01  6.52918002E-03  0.00000000E+00  0.00000000E+00

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 1


************************************************************************************************************************************
      5  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


                OPEN FILE zgoubi.plt                                                                      
                FOR PRINTING TRAJECTORIES

                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.004     0.000     0.000     0.000            1.047     3.990    -0.006     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (       10.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.523598776     m ;  Time  (for ref. rigidity & particle) =            NaN s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
      7  Keyword, label(s) :  FIT2                                                                                     IPASS= 1

     Pgm main. FITING=.T., FIT procedure launched.

           variable #            1       IR =            2 ,   ok.
           variable #            1       IP =           30 ,   ok.
           constraint #            1       IR =            6 ,   ok.
           constraint #            1       I  =            1 ,   ok.

                    FIT  variables  and  constraints  in  good  order,  FIT  will proceed. 
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30   0.500        4.00       3.9771501       80.0      1.516E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    2.815512E-09    Infinity MARKER     -                    -                    0
 Fit reached penalty value   7.9271E-18



   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     3.977     0.000     0.000     0.000      0.0000   -0.6838    3.977   -0.002    0.000    0.000   4.164871E+00     1
               Time of flight (mus) :  2.12775798E-02 mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   3.977150E-02  -1.687374E-06        1        1    1.000      (Y,T)         1
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)         1
   0.0000E+00   0.0000E+00   1.0000E+00   2.127758E-02   2.000000E-02        1        1    1.000      (t,K)         1

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)
      normalized (*beta*gamma) :     0.000000E+00    0.000000E+00

************************************************************************************************************************************
      9  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 1


                OPEN FILE zgoubi.fai                                                                      
                FOR PRINTING COORDINATES 

               Print will occur at element[s] labeled : 


************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 1


 Pgm rebel. At pass #    1/  21.  In element #    2,  parameter # 35  changed to    3.16212600E-01   (was    3.16212600E-01)

                                -----  REBELOTE  -----

     End of pass #        1 through the optical structure 

                     Total of          1 particles have been launched

     Multiple pass, 
          from element #     1 : MARKER    /label1=isoChroLoss_S       /label2=                    
                             to  REBELOTE  /label1=isoChroLoss_S       /label2=                    
     ending at pass #      21 at element #    10 : REBELOTE  /label1=                    /label2=                    


     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30   0.500        3.98       3.9771501       80.0      7.950E-02  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    2.815512E-09    Infinity MARKER     -                    -                    0
 Fit reached penalty value   7.9271E-18



   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 2

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     3.977     0.000     0.000     0.000      0.0000   -0.6838    3.977   -0.002    0.000    0.000   4.164871E+00     1
               Time of flight (mus) :  2.12775798E-02 mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   3.977150E-02  -1.687374E-06        1        1    1.000      (Y,T)         2
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)         2
   0.0000E+00   0.0000E+00   1.0000E+00   2.127758E-02   2.000000E-02        1        1    1.000      (t,K)         2

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)
      normalized (*beta*gamma) :     0.000000E+00    0.000000E+00

************************************************************************************************************************************
      9  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 2


************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 2


 Pgm rebel. At pass #    2/  21.  In element #    2,  parameter # 35  changed to    5.63064042E-01   (was    3.16212600E-01)

                                -----  REBELOTE  -----

     End of pass #        2 through the optical structure 

                     Total of          2 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30   0.500        3.98       7.0948887       80.0      3.033E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    1.587297E-08    Infinity MARKER     -                    -                    0
 Fit reached penalty value   2.5195E-16



   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 3

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.5631     7.095     0.000     0.000     0.000      0.0000   -0.4369    7.095   -0.000    0.000    0.000   7.429752E+00     1
               Time of flight (mus) :  2.13175075E-02 mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   7.094889E-02  -5.503692E-08        1        1    1.000      (Y,T)         3
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)         3
   0.0000E+00   0.0000E+00   1.0000E+00   2.131751E-02   6.341283E-02        1        1    1.000      (t,K)         3

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)
      normalized (*beta*gamma) :     0.000000E+00    0.000000E+00

************************************************************************************************************************************
      9  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 3


************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 3


 Pgm rebel. At pass #    3/  21.  In element #    2,  parameter # 35  changed to    8.09915484E-01   (was    5.63064042E-01)

                                -----  REBELOTE  -----

     End of pass #        3 through the optical structure 

                     Total of          3 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30   0.500        7.09       10.224062       80.0      1.213E-06  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    3.171223E-09    Infinity MARKER     -                    -                    0
 Fit reached penalty value   1.0057E-17



   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 4

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.8099    10.224     0.000     0.000     0.000      0.0000   -0.1901   10.224   -0.000    0.000    0.000   1.070661E+01     1
               Time of flight (mus) :  2.13581538E-02 mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   1.022406E-01  -1.468376E-08        1        1    1.000      (Y,T)         4
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)         4
   0.0000E+00   0.0000E+00   1.0000E+00   2.135815E-02   1.311974E-01        1        1    1.000      (t,K)         4

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)
      normalized (*beta*gamma) :     0.000000E+00    0.000000E+00

************************************************************************************************************************************
      9  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 4


************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 4


 Pgm rebel. At pass #    4/  21.  In element #    2,  parameter # 35  changed to    1.05676693E+00   (was    8.09915484E-01)

                                -----  REBELOTE  -----

     End of pass #        4 through the optical structure 

                     Total of          4 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30   0.500        10.2       13.364820       80.0      7.582E-08  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    4.404033E-10    Infinity MARKER     -                    -                    0
 Fit reached penalty value   1.9396E-19



   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 5

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0568    13.365     0.000     0.000     0.000      0.0000    0.0568   13.365   -0.000    0.000    0.000   1.399561E+01     1
               Time of flight (mus) :  2.13996404E-02 mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   1.336482E-01  -2.006073E-09        1        1    1.000      (Y,T)         5
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)         5
   0.0000E+00   0.0000E+00   1.0000E+00   2.139964E-02   2.233485E-01        1        1    1.000      (t,K)         5

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)
      normalized (*beta*gamma) :     0.000000E+00    0.000000E+00

************************************************************************************************************************************
      9  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 5


************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 5


 Pgm rebel. At pass #    5/  21.  In element #    2,  parameter # 35  changed to    1.30361837E+00   (was    1.05676693E+00)

                                -----  REBELOTE  -----

     End of pass #        5 through the optical structure 

                     Total of          5 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30   0.500        13.4       16.517296       80.0      7.582E-08  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    3.296201E-09    Infinity MARKER     -                    -                    0
 Fit reached penalty value   1.0865E-17



   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 6

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.3036    16.517     0.000     0.000     0.000      0.0000    0.3036   16.517   -0.000    0.000    0.000   1.729687E+01     1
               Time of flight (mus) :  2.14419827E-02 mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   1.651730E-01  -1.482506E-09        1        1    1.000      (Y,T)         6
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)         6
   0.0000E+00   0.0000E+00   1.0000E+00   2.144198E-02   3.398588E-01        1        1    1.000      (t,K)         6

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)
      normalized (*beta*gamma) :     0.000000E+00    0.000000E+00

************************************************************************************************************************************
      9  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 6


************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 6


 Pgm rebel. At pass #    6/  21.  In element #    2,  parameter # 35  changed to    1.55046981E+00   (was    1.30361837E+00)

                                -----  REBELOTE  -----

     End of pass #        6 through the optical structure 

                     Total of          6 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30   0.500        16.5       19.681621       80.0      3.033E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    1.860631E-08    Infinity MARKER     -                    -                    0
 Fit reached penalty value   3.4619E-16



   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 7

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.5505    19.682     0.000     0.000     0.000      0.0000    0.5505   19.682    0.000    0.000    0.000   2.061054E+01     1
               Time of flight (mus) :  2.14851891E-02 mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   1.968162E-01   1.443649E-09        1        1    1.000      (Y,T)         7
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)         7
   0.0000E+00   0.0000E+00   1.0000E+00   2.148519E-02   4.807194E-01        1        1    1.000      (t,K)         7

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)
      normalized (*beta*gamma) :     0.000000E+00    0.000000E+00

************************************************************************************************************************************
      9  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 7


************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 7


 Pgm rebel. At pass #    7/  21.  In element #    2,  parameter # 35  changed to    1.79732125E+00   (was    1.55046981E+00)

                                -----  REBELOTE  -----

     End of pass #        7 through the optical structure 

                     Total of          7 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30   0.500        19.7       22.857930       80.0      3.033E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    2.900102E-08    Infinity MARKER     -                    -                    0
 Fit reached penalty value   8.4106E-16



   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 8

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.7973    22.858     0.000     0.000     0.000      0.0000    0.7973   22.858    0.000    0.000    0.000   2.393677E+01     1
               Time of flight (mus) :  2.15292673E-02 mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   2.285793E-01   2.034707E-09        1        1    1.000      (Y,T)         8
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)         8
   0.0000E+00   0.0000E+00   1.0000E+00   2.152927E-02   6.459192E-01        1        1    1.000      (t,K)         8

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)
      normalized (*beta*gamma) :     0.000000E+00    0.000000E+00

************************************************************************************************************************************
      9  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 8


************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 8


 Pgm rebel. At pass #    8/  21.  In element #    2,  parameter # 35  changed to    2.04417269E+00   (was    1.79732125E+00)

                                -----  REBELOTE  -----

     End of pass #        8 through the optical structure 

                     Total of          8 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30   0.500        22.9       26.046362       80.0      6.065E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    2.584151E-08    Infinity MARKER     -                    -                    0
 Fit reached penalty value   6.6778E-16



   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 9

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   2.0442    26.046     0.000     0.000     0.000      0.0000    1.0442   26.046   -0.000    0.000    0.000   2.727569E+01     1
               Time of flight (mus) :  2.15742247E-02 mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   2.604636E-01  -1.841272E-09        1        1    1.000      (Y,T)         9
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)         9
   0.0000E+00   0.0000E+00   1.0000E+00   2.157422E-02   8.354455E-01        1        1    1.000      (t,K)         9

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)
      normalized (*beta*gamma) :     0.000000E+00    0.000000E+00

************************************************************************************************************************************
      9  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 9


************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 9


 Pgm rebel. At pass #    9/  21.  In element #    2,  parameter # 35  changed to    2.29102414E+00   (was    2.04417269E+00)

                                -----  REBELOTE  -----

     End of pass #        9 through the optical structure 

                     Total of          9 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30   0.500        26.0       29.247055       80.0      7.582E-08  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    7.858638E-10    Infinity MARKER     -                    -                    0
 Fit reached penalty value   6.1758E-19



   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 10

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   2.2910    29.247     0.000     0.000     0.000      0.0000    1.2910   29.247    0.000    0.000    0.000   3.062744E+01     1
               Time of flight (mus) :  2.16200684E-02 mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   2.924705E-01   2.160306E-11        1        1    1.000      (Y,T)        10
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        10
   0.0000E+00   0.0000E+00   1.0000E+00   2.162007E-02   1.049283E+00        1        1    1.000      (t,K)        10

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)
      normalized (*beta*gamma) :     0.000000E+00    0.000000E+00

************************************************************************************************************************************
      9  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 10


************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 10


 Pgm rebel. At pass #   10/  21.  In element #    2,  parameter # 35  changed to    2.53787558E+00   (was    2.29102414E+00)

                                -----  REBELOTE  -----

     End of pass #       10 through the optical structure 

                     Total of         10 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30   0.500        29.2       32.460152       80.0      1.516E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    2.208295E-08    Infinity MARKER     -                    -                    0
 Fit reached penalty value   4.8766E-16



   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 11

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   2.5379    32.460     0.000     0.000     0.000      0.0000    1.5379   32.460   -0.000    0.000    0.000   3.399219E+01     1
               Time of flight (mus) :  2.16668063E-02 mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   3.246015E-01  -1.205362E-09        1        1    1.000      (Y,T)        11
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        11
   0.0000E+00   0.0000E+00   1.0000E+00   2.166681E-02   1.287416E+00        1        1    1.000      (t,K)        11

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)
      normalized (*beta*gamma) :     0.000000E+00    0.000000E+00

************************************************************************************************************************************
      9  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 11


************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 11


 Pgm rebel. At pass #   11/  21.  In element #    2,  parameter # 35  changed to    2.78472702E+00   (was    2.53787558E+00)

                                -----  REBELOTE  -----

     End of pass #       11 through the optical structure 

                     Total of         11 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30   0.500        32.5       35.685799       80.0      1.516E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    1.463812E-08    Infinity MARKER     -                    -                    0
 Fit reached penalty value   2.1427E-16



   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 12

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   2.7847    35.686     0.000     0.000     0.000      0.0000    1.7847   35.686   -0.000    0.000    0.000   3.737008E+01     1
               Time of flight (mus) :  2.17144459E-02 mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   3.568580E-01  -7.329586E-10        1        1    1.000      (Y,T)        12
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        12
   0.0000E+00   0.0000E+00   1.0000E+00   2.171445E-02   1.549826E+00        1        1    1.000      (t,K)        12

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)
      normalized (*beta*gamma) :     0.000000E+00    0.000000E+00

************************************************************************************************************************************
      9  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 12


************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 12


 Pgm rebel. At pass #   12/  21.  In element #    2,  parameter # 35  changed to    3.03157846E+00   (was    2.78472702E+00)

                                -----  REBELOTE  -----

     End of pass #       12 through the optical structure 

                     Total of         12 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30   0.500        35.7       38.924144       80.0      1.516E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    2.020656E-08    Infinity MARKER     -                    -                    0
 Fit reached penalty value   4.0831E-16



   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 13

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   3.0316    38.924     0.000     0.000     0.000      0.0000    2.0316   38.924   -0.000    0.000    0.000   4.076127E+01     1
               Time of flight (mus) :  2.17629948E-02 mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   3.892414E-01  -9.039758E-10        1        1    1.000      (Y,T)        13
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        13
   0.0000E+00   0.0000E+00   1.0000E+00   2.176299E-02   1.836492E+00        1        1    1.000      (t,K)        13

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)
      normalized (*beta*gamma) :     0.000000E+00    0.000000E+00

************************************************************************************************************************************
      9  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 13


************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 13


 Pgm rebel. At pass #   13/  21.  In element #    2,  parameter # 35  changed to    3.27842991E+00   (was    3.03157846E+00)

                                -----  REBELOTE  -----

     End of pass #       13 through the optical structure 

                     Total of         13 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30   0.500        38.9       42.175337       80.0      1.516E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    2.054890E-08    Infinity MARKER     -                    -                    0
 Fit reached penalty value   4.2226E-16



   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 14

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   3.2784    42.175     0.000     0.000     0.000      0.0000    2.2784   42.175    0.000    0.000    0.000   4.416591E+01     1
               Time of flight (mus) :  2.18124610E-02 mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.217534E-01   8.428115E-10        1        1    1.000      (Y,T)        14
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        14
   0.0000E+00   0.0000E+00   1.0000E+00   2.181246E-02   2.147392E+00        1        1    1.000      (t,K)        14

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)
      normalized (*beta*gamma) :     0.000000E+00    0.000000E+00

************************************************************************************************************************************
      9  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 14


************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 14


 Pgm rebel. At pass #   14/  21.  In element #    2,  parameter # 35  changed to    3.52528135E+00   (was    3.27842991E+00)

                                -----  REBELOTE  -----

     End of pass #       14 through the optical structure 

                     Total of         14 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30   0.500        42.2       45.439534       80.0      1.516E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    1.333522E-08    Infinity MARKER     -                    -                    0
 Fit reached penalty value   1.7783E-16



   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 15

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   3.5253    45.440     0.000     0.000     0.000      0.0000    2.5253   45.440    0.000    0.000    0.000   4.758417E+01     1
               Time of flight (mus) :  2.18628524E-02 mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.543953E-01   5.064215E-10        1        1    1.000      (Y,T)        15
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        15
   0.0000E+00   0.0000E+00   1.0000E+00   2.186285E-02   2.482502E+00        1        1    1.000      (t,K)        15

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)
      normalized (*beta*gamma) :     0.000000E+00    0.000000E+00

************************************************************************************************************************************
      9  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 15


************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 15


 Pgm rebel. At pass #   15/  21.  In element #    2,  parameter # 35  changed to    3.77213279E+00   (was    3.52528135E+00)

                                -----  REBELOTE  -----

     End of pass #       15 through the optical structure 

                     Total of         15 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30   0.500        45.4       48.716891       80.0      3.033E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    2.522658E-08    Infinity MARKER     -                    -                    0
 Fit reached penalty value   6.3638E-16



   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 16

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   3.7721    48.717     0.000     0.000     0.000      0.0000    2.7721   48.717   -0.000    0.000    0.000   5.101621E+01     1
               Time of flight (mus) :  2.19141771E-02 mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.871689E-01  -9.026391E-10        1        1    1.000      (Y,T)        16
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        16
   0.0000E+00   0.0000E+00   1.0000E+00   2.191418E-02   2.841797E+00        1        1    1.000      (t,K)        16

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)
      normalized (*beta*gamma) :     0.000000E+00    0.000000E+00

************************************************************************************************************************************
      9  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 16


************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 16


 Pgm rebel. At pass #   16/  21.  In element #    2,  parameter # 35  changed to    4.01898423E+00   (was    3.77213279E+00)

                                -----  REBELOTE  -----

     End of pass #       16 through the optical structure 

                     Total of         16 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30   0.500        48.7       52.007570       80.0      3.033E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    7.395684E-10    Infinity MARKER     -                    -                    0
 Fit reached penalty value   5.4696E-19



   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 17

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   4.0190    52.008     0.000     0.000     0.000      0.0000    3.0190   52.008    0.000    0.000    0.000   5.446220E+01     1
               Time of flight (mus) :  2.19664432E-02 mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   5.200757E-01   2.328898E-11        1        1    1.000      (Y,T)        17
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        17
   0.0000E+00   0.0000E+00   1.0000E+00   2.196644E-02   3.225248E+00        1        1    1.000      (t,K)        17

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)
      normalized (*beta*gamma) :     0.000000E+00    0.000000E+00

************************************************************************************************************************************
      9  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 17


************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 17


 Pgm rebel. At pass #   17/  21.  In element #    2,  parameter # 35  changed to    4.26583567E+00   (was    4.01898423E+00)

                                -----  REBELOTE  -----

     End of pass #       17 through the optical structure 

                     Total of         17 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30   0.500        52.0       55.311732       80.0      1.516E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    1.602132E-08    Infinity MARKER     -                    -                    0
 Fit reached penalty value   2.5668E-16



   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 18

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   4.2658    55.312     0.000     0.000     0.000      0.0000    3.2658   55.312    0.000    0.000    0.000   5.792231E+01     1
               Time of flight (mus) :  2.20196591E-02 mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   5.531173E-01   5.028071E-10        1        1    1.000      (Y,T)        18
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        18
   0.0000E+00   0.0000E+00   1.0000E+00   2.201966E-02   3.632826E+00        1        1    1.000      (t,K)        18

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)
      normalized (*beta*gamma) :     0.000000E+00    0.000000E+00

************************************************************************************************************************************
      9  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 18


************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 18


 Pgm rebel. At pass #   18/  21.  In element #    2,  parameter # 35  changed to    4.51268712E+00   (was    4.26583567E+00)

                                -----  REBELOTE  -----

     End of pass #       18 through the optical structure 

                     Total of         18 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30   0.500        55.3       58.629547       80.0      1.516E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    3.124003E-08    Infinity MARKER     -                    -                    0
 Fit reached penalty value   9.7594E-16



   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 19

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   4.5127    58.630     0.000     0.000     0.000      0.0000    3.5127   58.630   -0.000    0.000    0.000   6.139672E+01     1
               Time of flight (mus) :  2.20738333E-02 mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   5.862955E-01  -9.266634E-10        1        1    1.000      (Y,T)        19
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        19
   0.0000E+00   0.0000E+00   1.0000E+00   2.207383E-02   4.064499E+00        1        1    1.000      (t,K)        19

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)
      normalized (*beta*gamma) :     0.000000E+00    0.000000E+00

************************************************************************************************************************************
      9  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 19


************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 19


 Pgm rebel. At pass #   19/  21.  In element #    2,  parameter # 35  changed to    4.75953856E+00   (was    4.51268712E+00)

                                -----  REBELOTE  -----

     End of pass #       19 through the optical structure 

                     Total of         19 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30   0.500        58.6       61.961184       80.0      3.033E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    2.437664E-08    Infinity MARKER     -                    -                    0
 Fit reached penalty value   5.9422E-16



   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 20

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   4.7595    61.961     0.000     0.000     0.000      0.0000    3.7595   61.961    0.000    0.000    0.000   6.488560E+01     1
               Time of flight (mus) :  2.21289744E-02 mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   6.196118E-01   6.838580E-10        1        1    1.000      (Y,T)        20
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        20
   0.0000E+00   0.0000E+00   1.0000E+00   2.212897E-02   4.520236E+00        1        1    1.000      (t,K)        20

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)
      normalized (*beta*gamma) :     0.000000E+00    0.000000E+00

************************************************************************************************************************************
      9  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 20


************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 20


 Pgm rebel. At pass #   20/  21.  In element #    2,  parameter # 35  changed to    5.00639000E+00   (was    4.75953856E+00)

                                -----  REBELOTE  -----

     End of pass #       20 through the optical structure 

                     Total of         20 particles have been launched


      Next  pass  is  #    21 and  last  pass  through  the  optical  structure


     WRITE statements to zgoubi.res are re-established from now on.

************************************************************************************************************************************
      1  Keyword, label(s) :  MARKER      isoChroLoss_S                                                                IPASS= 21


************************************************************************************************************************************
      2  Keyword, label(s) :  OBJET                                                                                    IPASS= 21

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      3  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 21


************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 21


************************************************************************************************************************************
      5  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 21


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  5.0064    61.961     0.000     0.000     0.000            1.047    63.514     0.043     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      131.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.523598776     m ;  Time  (for ref. rigidity & particle) =   8.460222E-08 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER                                                                                   IPASS= 21


************************************************************************************************************************************
      7  Keyword, label(s) :  FIT2                                                                                     IPASS= 21


************************************************************************************************************************************
      1  Keyword, label(s) :  MARKER      isoChroLoss_S                                                                IPASS= 21


************************************************************************************************************************************
      2  Keyword, label(s) :  OBJET                                                                                    IPASS= 21

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      3  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 21


************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      #S_60degSectorWIdx                                                           IPASS= 21


************************************************************************************************************************************
      5  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 21


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   1.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  5.0064    61.961     0.000     0.000     0.000            1.047    63.514     0.043     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      131.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.523598776     m ;  Time  (for ref. rigidity & particle) =   8.460222E-08 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER                                                                                   IPASS= 21


************************************************************************************************************************************
      7  Keyword, label(s) :  FIT2                                                                                     IPASS= 21

     Pgm main. FITING=.T., FIT procedure launched.

           variable #            1       IR =            2 ,   ok.
           variable #            1       IP =           30 ,   ok.
           constraint #            1       IR =            6 ,   ok.
           constraint #            1       I  =            1 ,   ok.

                    FIT  variables  and  constraints  in  good  order,  FIT  will proceed. 
 STATUS OF VARIABLES  (Iteration #     0 /    999 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30   0.500        62.0       65.306818       80.0      6.065E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-15)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     6    0.000000E+00    1.000E+00    2.808945E-08    Infinity MARKER     -                    -                    0
 Fit reached penalty value   7.8902E-16



   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 21

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   5.0064    65.307     0.000     0.000     0.000      0.0000    4.0064   65.307    0.000    0.000    0.000   6.838914E+01     1
               Time of flight (mus) :  2.21850911E-02 mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   6.530682E-01   7.478666E-10        1        1    1.000      (Y,T)        21
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        21
   0.0000E+00   0.0000E+00   1.0000E+00   2.218509E-02   5.000000E+00        1        1    1.000      (t,K)        21

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)
      normalized (*beta*gamma) :     0.000000E+00    0.000000E+00

************************************************************************************************************************************
      9  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 21


************************************************************************************************************************************
     10  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 21


                         >>>>  End  of  'REBELOTE'  procedure  <<<<

      There  has  been         21  passes  through  the  optical  structure 

                     Total of         21 particles have been launched

************************************************************************************************************************************
     11  Keyword, label(s) :  SYSTEM                                                                                   IPASS= 22

     Number  of  commands :   1,  as  follows : 

 gnuplot <./gnuplot_Zfai_scanTrev.gnu

************************************************************************************************************************************
     12  Keyword, label(s) :  MARKER                                                                                   IPASS= 22


************************************************************************************************************************************
     13  Keyword, label(s) :  END                                                                                      IPASS= 22


************************************************************************************************************************************
 Pgm zgoubi : Execution ended normally, upon keyword END or FIN
   
 File in:   classCyclo_ProbIsoChroLoss.INC.dat
 File out:  zgoubi.res

  Zgoubi, author's dvlpmnt version.
  Job  started  on  28-05-2020,  at  13:37:59 
  JOB  ENDED  ON    28-05-2020,  AT  13:38:00 

   CPU time, total :    0.34982099999999999     
