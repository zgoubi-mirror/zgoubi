set xtics ;  set ytics nomirror ; set y2tics; set xlabel "R  [m]" ; set ylabel "T_{rev} [{/Symbol m}s]" 
cm2m = 0.01; nSec=6; set y2label "T_{rev}  at  k=0[{/Symbol m}s]" ; set key c r
plot "zgoubi.fai"    u ($10 *cm2m):($15 * nSec) w lp pt 4 ps 1.2 lc rgb "black" tit "k=-0.03" 

set terminal postscript eps blacktext color  enh  
set output "gnuplot_Zfai_cycloClass_ProbIsochro.eps"  
replot  
set terminal X11  
unset output   
pause 1
exit

