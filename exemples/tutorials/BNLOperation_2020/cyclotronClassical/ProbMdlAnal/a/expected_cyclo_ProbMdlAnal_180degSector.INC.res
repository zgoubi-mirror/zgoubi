Cyclotron, classical. Analytical model of dipole field.
'MARKER'  ProbMdlAnal_S                                                   ! Just for edition purposes.       1
'OBJET'                                                                                                      2
64.62444403717985                                                                            ! 200keV.
2
1 1
12.9248888074 0. 0. 0. 0.  1.  'm'         ! D=1 => 200keV proton. R=Brho/B=64.624444037[kG.cm]/5[kG].
1
'PARTICUL'                                               ! This is required to get the time-of-flight,       3
PROTON                                                      ! otherwise zgoubi only requires rigidity.
'FAISCEAU'                                                               ! Local particle coordinates.       4
'MARKER'    #S_60degSectorUnifB                               ! Label should not exceed 20 characters.       5
'DIPOLE'                                                    ! Analytical definition of a dipole field.       6
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
180. 50.                                                       ! Sector angle AT; reference radius RM.
90.  5. 0. 0. 0.                 ! Reference azimuthal angle ACN; BM field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
90. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-90. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
1.                                  ! Integration step size. The smaller, the better the orbits close.
2  0. 0. 0. 0.                  ! Magnet positionning RE, TE, RS, TS. Could be isntead non-zero, e.g.,
!                                2 RE=50. 0. RS=50. 0., as long as Yo is amended accordingly in OBJET.
'MARKER'    #E_60degSectorUnifB                               ! Label should not exceed 20 characters.       7
'FAISCEAU'                                                               ! Local particle coordinates.       8
'FIT'                                                     ! Adjust Yo at OBJET so to get final Y = Yo.       9
1   nofinal
2 30 0 [12.,65.]                                                                      ! Variable : Yo.
1  2e-12  199            ! ! constraint; default penalty would be 1e-10; maximu 199 calls to function.
3.1 1 2 #End 0. 1. 0                                                        ! Constraint:  Y_final=Yo.
'FAISTORE'                                                   ! Log particle data here, to zgoubi.fai,       10
zgoubi.fai                                                 ! for further plotting (by gnuplot, below).
1
'REBELOTE'                                                               ! Momentum scan, 60 samples.       11
60 0.2  0 1                         ! 60 different rigidities; log to video ; take initial coordinates
1                                      ! as found in OBJET ; change parameter(s) as stated next lines.
OBJET 35  1:5.0063899693             ! Change relative rigity (35) in OBJET; range (0.2 MeV to 5 MeV).
'SYSTEM'                                                                                                    12
1                                                                          ! 2 SYSTEM commands follow.
/usr/bin/gnuplot < ./gnuplot_TOF.gnu &                             ! Launch plot by ./gnuplot_TOF.gnu.
'MARKER'  ProbMdlAnal_E                                                   ! Just for edition purposes.      13
'END'

************************************************************************************************************************************
      1  Keyword, label(s) :  MARKER      ProbMdlAnal_S                                                                IPASS= 1


************************************************************************************************************************************
      2  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      3  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 1

     Particle  properties :
     PROTON
                     Mass          =    938.272        MeV/c2
                     Charge        =   1.602176E-19    C     
                     G  factor     =    1.79285              
                     COM life-time =   1.000000E+99    s     

              Reference  data :
                    mag. rigidity (kG.cm)   :   64.624444      =p/q, such that dev.=B*L/rigidity
                    mass (MeV/c2)           :   938.27208    
                    momentum (MeV/c)        :   19.373921    
                    energy, total (MeV)     :   938.47208    
                    energy, kinetic (MeV)   :  0.19999999    
                    beta = v/c              :  2.0644110049E-02
                    gamma                   :   1.000213158    
                    beta*gamma              :  2.0648510502E-02
                    G*gamma                 :   1.793229509    
                    electric rigidity (MeV) :  0.3999573557    =T[eV]*(gamma+1)/gamma, such that dev.=E*L/rigidity
  
 I, AMQ(1,I), AMQ(2,I)/QE, P/Pref, v/c, time, s :
  
     1   9.38272081E+02  1.00000000E+00  1.00000000E+00  2.06441100E-02  0.00000000E+00  0.00000000E+00

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

m  1   1.0000    12.925     0.000     0.000     0.000      0.0000    0.0000   12.925    0.000    0.000    0.000   0.000000E+00     1
               Time of flight (mus) :   0.0000000     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   1.292489E-01   0.000000E+00        1        1    1.000      (Y,T)         1
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)         1
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   2.000000E-01        1        1    1.000      (t,K)         1

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)
      normalized (*beta*gamma) :     0.000000E+00    0.000000E+00

************************************************************************************************************************************
      5  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
      6  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


                OPEN FILE zgoubi.plt                                                                      
                FOR PRINTING TRAJECTORIES

                    Dipole  magnet

           ANGLES : A.TOTAL =  1.8000E+02 degrees     A.CENTRAL =  9.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  90.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -90.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :   1.000     cm   (i.e.,   2.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            3.142    12.925    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (       42.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    1.57079633     m ;  Time  (for ref. rigidity & particle) =            NaN s 

************************************************************************************************************************************
      7  Keyword, label(s) :  MARKER      #E_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

m  1   1.0000    12.925     0.000     0.000     0.000      0.0000    0.0000   12.925   -0.000    0.000    0.000   4.060469E+01     1
               Time of flight (mus) :  6.56083798E-02 mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   1.292485E-01  -1.214263E-07        1        1    1.000      (Y,T)         1
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)         1
   0.0000E+00   0.0000E+00   1.0000E+00   6.560838E-02   2.000000E-01        1        1    1.000      (t,K)         1

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)
      normalized (*beta*gamma) :     0.000000E+00    0.000000E+00

************************************************************************************************************************************
      9  Keyword, label(s) :  FIT                                                                                      IPASS= 1

     Pgm main. FITING=.T., FIT procedure launched.

           variable #            1       IR =            2 ,   ok.
           variable #            1       IP =           30 ,   ok.
           constraint #            1       IR =            8 ,   ok.
           constraint #            1       I  =            1 ,   ok.

                    FIT  variables  and  constraints  in  good  order,  FIT  will proceed. 

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        12.9       12.924870       65.0      3.324E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    6.359687E-07    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   4.0446E-13

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 1


                OPEN FILE zgoubi.fai                                                                      
                FOR PRINTING COORDINATES 

               Print will occur at element[s] labeled : 


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 1


 Pgm rebel. At pass #    1/  61.  In element #    2,  parameter # 35  changed to    1.00000000E+00   (was    1.00000000E+00)

                                -----  REBELOTE  -----

     End of pass #        1 through the optical structure 

                     Total of          1 particles have been launched

     Multiple pass, 
          from element #     1 : MARKER    /label1=ProbMdlAnal_S       /label2=                    
                             to  REBELOTE  /label1=ProbMdlAnal_S       /label2=                    
     ending at pass #      61 at element #    11 : REBELOTE  /label1=                    /label2=                    


     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        12.9       12.924870       65.0      0.177      OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    6.359687E-07    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   4.0446E-13

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 2


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 2


 Pgm rebel. At pass #    2/  61.  In element #    2,  parameter # 35  changed to    1.06790491E+00   (was    1.00000000E+00)

                                -----  REBELOTE  -----

     End of pass #        2 through the optical structure 

                     Total of          2 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        13.8       13.802537       65.0      9.973E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    2.682141E-07    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   7.1939E-14

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 3


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 3


 Pgm rebel. At pass #    3/  61.  In element #    2,  parameter # 35  changed to    1.13580983E+00   (was    1.06790491E+00)

                                -----  REBELOTE  -----

     End of pass #        3 through the optical structure 

                     Total of          3 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        14.7       14.680202       65.0      3.324E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    4.990881E-07    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   2.4909E-13

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 4


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 4


 Pgm rebel. At pass #    4/  61.  In element #    2,  parameter # 35  changed to    1.20371474E+00   (was    1.13580983E+00)

                                -----  REBELOTE  -----

     End of pass #        4 through the optical structure 

                     Total of          4 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        15.6       15.557868       65.0      3.324E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    1.685296E-07    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   2.8402E-14

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 5


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 5


 Pgm rebel. At pass #    5/  61.  In element #    2,  parameter # 35  changed to    1.27161966E+00   (was    1.20371474E+00)

                                -----  REBELOTE  -----

     End of pass #        5 through the optical structure 

                     Total of          5 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        16.4       16.435533       65.0      3.324E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    9.822701E-07    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   9.6485E-13

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 6


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 6


 Pgm rebel. At pass #    6/  61.  In element #    2,  parameter # 35  changed to    1.33952457E+00   (was    1.27161966E+00)

                                -----  REBELOTE  -----

     End of pass #        6 through the optical structure 

                     Total of          6 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        17.3       17.313199       65.0      3.324E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    8.451714E-07    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   7.1431E-13

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 7


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 7


 Pgm rebel. At pass #    7/  61.  In element #    2,  parameter # 35  changed to    1.40742949E+00   (was    1.33952457E+00)

                                -----  REBELOTE  -----

     End of pass #        7 through the optical structure 

                     Total of          7 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        18.2       18.190862       65.0      9.973E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    8.042657E-07    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   6.4684E-13

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 8


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 8


 Pgm rebel. At pass #    8/  61.  In element #    2,  parameter # 35  changed to    1.47533440E+00   (was    1.40742949E+00)

                                -----  REBELOTE  -----

     End of pass #        8 through the optical structure 

                     Total of          8 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        19.1       19.068527       65.0      3.324E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    8.301503E-08    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   6.8915E-15

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 9


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 9


 Pgm rebel. At pass #    9/  61.  In element #    2,  parameter # 35  changed to    1.54323932E+00   (was    1.47533440E+00)

                                -----  REBELOTE  -----

     End of pass #        9 through the optical structure 

                     Total of          9 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        19.9       19.946191       65.0      9.973E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    1.049767E-06    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   1.1020E-12

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 10


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 10


 Pgm rebel. At pass #   10/  61.  In element #    2,  parameter # 35  changed to    1.61114423E+00   (was    1.54323932E+00)

                                -----  REBELOTE  -----

     End of pass #       10 through the optical structure 

                     Total of         10 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        20.8       20.823856       65.0      3.324E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    2.230535E-07    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   4.9753E-14

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 11


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 11


 Pgm rebel. At pass #   11/  61.  In element #    2,  parameter # 35  changed to    1.67904915E+00   (was    1.61114423E+00)

                                -----  REBELOTE  -----

     End of pass #       11 through the optical structure 

                     Total of         11 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        21.7       21.701519       65.0      9.973E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    2.955957E-07    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   8.7377E-14

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 12


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 12


 Pgm rebel. At pass #   12/  61.  In element #    2,  parameter # 35  changed to    1.74695406E+00   (was    1.67904915E+00)

                                -----  REBELOTE  -----

     End of pass #       12 through the optical structure 

                     Total of         12 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        22.6       22.579183       65.0      9.973E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    6.583980E-07    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   4.3349E-13

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 13


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 13


 Pgm rebel. At pass #   13/  61.  In element #    2,  parameter # 35  changed to    1.81485898E+00   (was    1.74695406E+00)

                                -----  REBELOTE  -----

     End of pass #       13 through the optical structure 

                     Total of         13 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        23.5       23.456847       65.0      9.973E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    8.897213E-07    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   7.9160E-13

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 14


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 14


 Pgm rebel. At pass #   14/  61.  In element #    2,  parameter # 35  changed to    1.88276389E+00   (was    1.81485898E+00)

                                -----  REBELOTE  -----

     End of pass #       14 through the optical structure 

                     Total of         14 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        24.3       24.334511       65.0      9.973E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    1.010442E-06    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   1.0210E-12

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 15


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 15


 Pgm rebel. At pass #   15/  61.  In element #    2,  parameter # 35  changed to    1.95066881E+00   (was    1.88276389E+00)

                                -----  REBELOTE  -----

     End of pass #       15 through the optical structure 

                     Total of         15 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        25.2       25.212174       65.0      9.973E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    1.041838E-06    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   1.0854E-12

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 16


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 16


 Pgm rebel. At pass #   16/  61.  In element #    2,  parameter # 35  changed to    2.01857372E+00   (was    1.95066881E+00)

                                -----  REBELOTE  -----

     End of pass #       16 through the optical structure 

                     Total of         16 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        26.1       26.089838       65.0      9.973E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    9.976275E-07    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   9.9526E-13

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 17


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 17


 Pgm rebel. At pass #   17/  61.  In element #    2,  parameter # 35  changed to    2.08647864E+00   (was    2.01857372E+00)

                                -----  REBELOTE  -----

     End of pass #       17 through the optical structure 

                     Total of         17 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        27.0       26.967502       65.0      9.973E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    8.896551E-07    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   7.9149E-13

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 18


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 18


 Pgm rebel. At pass #   18/  61.  In element #    2,  parameter # 35  changed to    2.15438355E+00   (was    2.08647864E+00)

                                -----  REBELOTE  -----

     End of pass #       18 through the optical structure 

                     Total of         18 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        27.8       27.845166       65.0      9.973E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    7.267198E-07    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   5.2812E-13

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 19


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 19


 Pgm rebel. At pass #   19/  61.  In element #    2,  parameter # 35  changed to    2.22228847E+00   (was    2.15438355E+00)

                                -----  REBELOTE  -----

     End of pass #       19 through the optical structure 

                     Total of         19 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        28.7       28.722829       65.0      9.973E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    5.181901E-07    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   2.6852E-13

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 20


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 20


 Pgm rebel. At pass #   20/  61.  In element #    2,  parameter # 35  changed to    2.29019338E+00   (was    2.22228847E+00)

                                -----  REBELOTE  -----

     End of pass #       20 through the optical structure 

                     Total of         20 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        29.6       29.600493       65.0      9.973E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    2.702901E-07    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   7.3057E-14

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 21


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 21


 Pgm rebel. At pass #   21/  61.  In element #    2,  parameter # 35  changed to    2.35809829E+00   (was    2.29019338E+00)

                                -----  REBELOTE  -----

     End of pass #       21 through the optical structure 

                     Total of         21 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        30.5       30.478157       65.0      9.973E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    1.150114E-08    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   1.3228E-16

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 22


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 22


 Pgm rebel. At pass #   22/  61.  In element #    2,  parameter # 35  changed to    2.42600321E+00   (was    2.35809829E+00)

                                -----  REBELOTE  -----

     End of pass #       22 through the optical structure 

                     Total of         22 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        31.4       31.355821       65.0      9.973E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    3.230475E-07    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   1.0436E-13

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 23


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 23


 Pgm rebel. At pass #   23/  61.  In element #    2,  parameter # 35  changed to    2.49390812E+00   (was    2.42600321E+00)

                                -----  REBELOTE  -----

     End of pass #       23 through the optical structure 

                     Total of         23 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        32.2       32.233484       65.0      9.973E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    6.598096E-07    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   4.3535E-13

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 24


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 24


 Pgm rebel. At pass #   24/  61.  In element #    2,  parameter # 35  changed to    2.56181304E+00   (was    2.49390812E+00)

                                -----  REBELOTE  -----

     End of pass #       24 through the optical structure 

                     Total of         24 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        33.1       33.111148       65.0      9.973E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    1.018680E-06    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   1.0377E-12

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 25


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 25


 Pgm rebel. At pass #   25/  61.  In element #    2,  parameter # 35  changed to    2.62971795E+00   (was    2.56181304E+00)

                                -----  REBELOTE  -----

     End of pass #       25 through the optical structure 

                     Total of         25 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        34.0       33.988812       65.0      9.973E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    1.396891E-06    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   1.9513E-12

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 26


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 26


 Pgm rebel. At pass #   26/  61.  In element #    2,  parameter # 35  changed to    2.69762287E+00   (was    2.62971795E+00)

                                -----  REBELOTE  -----

     End of pass #       26 through the optical structure 

                     Total of         26 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        34.9       34.866475       65.0      3.324E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    2.022519E-07    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   4.0906E-14

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 27


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 27


 Pgm rebel. At pass #   27/  61.  In element #    2,  parameter # 35  changed to    2.76552778E+00   (was    2.69762287E+00)

                                -----  REBELOTE  -----

     End of pass #       27 through the optical structure 

                     Total of         27 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        35.7       35.744138       65.0      9.973E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    2.080357E-07    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   4.3279E-14

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 28


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 28


 Pgm rebel. At pass #   28/  61.  In element #    2,  parameter # 35  changed to    2.83343270E+00   (was    2.76552778E+00)

                                -----  REBELOTE  -----

     End of pass #       28 through the optical structure 

                     Total of         28 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        36.6       36.621802       65.0      9.973E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    6.315036E-07    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   3.9880E-13

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 29


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 29


 Pgm rebel. At pass #   29/  61.  In element #    2,  parameter # 35  changed to    2.90133761E+00   (was    2.83343270E+00)

                                -----  REBELOTE  -----

     End of pass #       29 through the optical structure 

                     Total of         29 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        37.5       37.499466       65.0      9.973E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    1.066652E-06    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   1.1377E-12

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 30


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 30


 Pgm rebel. At pass #   30/  61.  In element #    2,  parameter # 35  changed to    2.96924253E+00   (was    2.90133761E+00)

                                -----  REBELOTE  -----

     End of pass #       30 through the optical structure 

                     Total of         30 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        38.4       38.377129       65.0      3.324E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    4.822586E-07    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   2.3257E-13

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 31


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 31


 Pgm rebel. At pass #   31/  61.  In element #    2,  parameter # 35  changed to    3.03714744E+00   (was    2.96924253E+00)

                                -----  REBELOTE  -----

     End of pass #       31 through the optical structure 

                     Total of         31 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        39.3       39.254792       65.0      9.973E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    2.738474E-08    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   7.4992E-16

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 32


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 32


 Pgm rebel. At pass #   32/  61.  In element #    2,  parameter # 35  changed to    3.10505236E+00   (was    3.03714744E+00)

                                -----  REBELOTE  -----

     End of pass #       32 through the optical structure 

                     Total of         32 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        40.1       40.132456       65.0      9.973E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    4.357379E-07    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   1.8987E-13

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 33


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 33


 Pgm rebel. At pass #   33/  61.  In element #    2,  parameter # 35  changed to    3.17295727E+00   (was    3.10505236E+00)

                                -----  REBELOTE  -----

     End of pass #       33 through the optical structure 

                     Total of         33 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        41.0       41.010120       65.0      9.973E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    9.062486E-07    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   8.2129E-13

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 34


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 34


 Pgm rebel. At pass #   34/  61.  In element #    2,  parameter # 35  changed to    3.24086219E+00   (was    3.17295727E+00)

                                -----  REBELOTE  -----

     End of pass #       34 through the optical structure 

                     Total of         34 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        41.9       41.887784       65.0      9.973E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    1.383473E-06    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   1.9140E-12

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 35


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 35


 Pgm rebel. At pass #   35/  61.  In element #    2,  parameter # 35  changed to    3.30876710E+00   (was    3.24086219E+00)

                                -----  REBELOTE  -----

     End of pass #       35 through the optical structure 

                     Total of         35 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        42.8       42.765446       65.0      3.324E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    1.279282E-07    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   1.6366E-14

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 36


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 36


 Pgm rebel. At pass #   36/  61.  In element #    2,  parameter # 35  changed to    3.37667202E+00   (was    3.30876710E+00)

                                -----  REBELOTE  -----

     End of pass #       36 through the optical structure 

                     Total of         36 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        43.6       43.643110       65.0      9.973E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    3.606224E-07    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   1.3005E-13

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 37


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 37


 Pgm rebel. At pass #   37/  61.  In element #    2,  parameter # 35  changed to    3.44457693E+00   (was    3.37667202E+00)

                                -----  REBELOTE  -----

     End of pass #       37 through the optical structure 

                     Total of         37 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        44.5       44.520774       65.0      9.973E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    8.540380E-07    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   7.2938E-13

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 38


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 38


 Pgm rebel. At pass #   38/  61.  In element #    2,  parameter # 35  changed to    3.51248185E+00   (was    3.44457693E+00)

                                -----  REBELOTE  -----

     End of pass #       38 through the optical structure 

                     Total of         38 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        45.4       45.398438       65.0      9.973E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    1.351882E-06    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   1.8276E-12

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 39


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 39


 Pgm rebel. At pass #   39/  61.  In element #    2,  parameter # 35  changed to    3.58038676E+00   (was    3.51248185E+00)

                                -----  REBELOTE  -----

     End of pass #       39 through the optical structure 

                     Total of         39 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        46.3       46.276100       65.0      3.324E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    1.408574E-07    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   1.9841E-14

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 40


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 40


 Pgm rebel. At pass #   40/  61.  In element #    2,  parameter # 35  changed to    3.64829167E+00   (was    3.58038676E+00)

                                -----  REBELOTE  -----

     End of pass #       40 through the optical structure 

                     Total of         40 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        47.2       47.153764       65.0      9.973E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    3.646027E-07    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   1.3294E-13

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 41


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 41


 Pgm rebel. At pass #   41/  61.  In element #    2,  parameter # 35  changed to    3.71619659E+00   (was    3.64829167E+00)

                                -----  REBELOTE  -----

     End of pass #       41 through the optical structure 

                     Total of         41 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        48.0       48.031428       65.0      9.973E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    8.733644E-07    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   7.6277E-13

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 42


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 42


 Pgm rebel. At pass #   42/  61.  In element #    2,  parameter # 35  changed to    3.78410150E+00   (was    3.71619659E+00)

                                -----  REBELOTE  -----

     End of pass #       42 through the optical structure 

                     Total of         42 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        48.9       48.909092       65.0      9.973E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    1.385156E-06    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   1.9187E-12

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 43


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 43


 Pgm rebel. At pass #   43/  61.  In element #    2,  parameter # 35  changed to    3.85200642E+00   (was    3.78410150E+00)

                                -----  REBELOTE  -----

     End of pass #       43 through the optical structure 

                     Total of         43 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        49.8       49.786754       65.0      3.324E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    9.487631E-08    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   9.0015E-15

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 44


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 44


 Pgm rebel. At pass #   44/  61.  In element #    2,  parameter # 35  changed to    3.91991133E+00   (was    3.85200642E+00)

                                -----  REBELOTE  -----

     End of pass #       44 through the optical structure 

                     Total of         44 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        50.7       50.664418       65.0      9.973E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    4.221815E-07    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   1.7824E-13

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 45


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 45


 Pgm rebel. At pass #   45/  61.  In element #    2,  parameter # 35  changed to    3.98781625E+00   (was    3.91991133E+00)

                                -----  REBELOTE  -----

     End of pass #       45 through the optical structure 

                     Total of         45 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        51.5       51.542082       65.0      9.973E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    9.415433E-07    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   8.8650E-13

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 46


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 46


 Pgm rebel. At pass #   46/  61.  In element #    2,  parameter # 35  changed to    4.05572116E+00   (was    3.98781625E+00)

                                -----  REBELOTE  -----

     End of pass #       46 through the optical structure 

                     Total of         46 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        52.4       52.419745       65.0      3.324E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    5.315432E-07    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   2.8254E-13

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 47


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 47


 Pgm rebel. At pass #   47/  61.  In element #    2,  parameter # 35  changed to    4.12362608E+00   (was    4.05572116E+00)

                                -----  REBELOTE  -----

     End of pass #       47 through the optical structure 

                     Total of         47 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        53.3       53.297408       65.0      9.973E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    8.104585E-09    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   6.5684E-17

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 48


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 48


 Pgm rebel. At pass #   48/  61.  In element #    2,  parameter # 35  changed to    4.19153099E+00   (was    4.12362608E+00)

                                -----  REBELOTE  -----

     End of pass #       48 through the optical structure 

                     Total of         48 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        54.2       54.175072       65.0      9.973E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    5.171215E-07    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   2.6741E-13

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 49


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 49


 Pgm rebel. At pass #   49/  61.  In element #    2,  parameter # 35  changed to    4.25943591E+00   (was    4.19153099E+00)

                                -----  REBELOTE  -----

     End of pass #       49 through the optical structure 

                     Total of         49 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        55.1       55.052736       65.0      9.973E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    1.043995E-06    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   1.0899E-12

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 50


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 50


 Pgm rebel. At pass #   50/  61.  In element #    2,  parameter # 35  changed to    4.32734082E+00   (was    4.25943591E+00)

                                -----  REBELOTE  -----

     End of pass #       50 through the optical structure 

                     Total of         50 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        55.9       55.930399       65.0      3.324E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    4.221784E-07    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   1.7823E-13

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 51


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 51


 Pgm rebel. At pass #   51/  61.  In element #    2,  parameter # 35  changed to    4.39524574E+00   (was    4.32734082E+00)

                                -----  REBELOTE  -----

     End of pass #       51 through the optical structure 

                     Total of         51 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        56.8       56.808062       65.0      9.973E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    1.076350E-07    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   1.1585E-14

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 52


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 52


 Pgm rebel. At pass #   52/  61.  In element #    2,  parameter # 35  changed to    4.46315065E+00   (was    4.39524574E+00)

                                -----  REBELOTE  -----

     End of pass #       52 through the optical structure 

                     Total of         52 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        57.7       57.685726       65.0      9.973E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    6.387479E-07    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   4.0800E-13

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 53


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 53


 Pgm rebel. At pass #   53/  61.  In element #    2,  parameter # 35  changed to    4.53105557E+00   (was    4.46315065E+00)

                                -----  REBELOTE  -----

     End of pass #       53 through the optical structure 

                     Total of         53 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        58.6       58.563390       65.0      9.973E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    1.171064E-06    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   1.3714E-12

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 54


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 54


 Pgm rebel. At pass #   54/  61.  In element #    2,  parameter # 35  changed to    4.59896048E+00   (was    4.53105557E+00)

                                -----  REBELOTE  -----

     End of pass #       54 through the optical structure 

                     Total of         54 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        59.4       59.441053       65.0      3.324E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    2.900747E-07    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   8.4143E-14

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 55


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 55


 Pgm rebel. At pass #   55/  61.  In element #    2,  parameter # 35  changed to    4.66686540E+00   (was    4.59896048E+00)

                                -----  REBELOTE  -----

     End of pass #       55 through the optical structure 

                     Total of         55 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        60.3       60.318716       65.0      9.973E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    2.444044E-07    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   5.9733E-14

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 56


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 56


 Pgm rebel. At pass #   56/  61.  In element #    2,  parameter # 35  changed to    4.73477031E+00   (was    4.66686540E+00)

                                -----  REBELOTE  -----

     End of pass #       56 through the optical structure 

                     Total of         56 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        61.2       61.196380       65.0      9.973E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    7.798465E-07    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   6.0816E-13

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 57


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 57


 Pgm rebel. At pass #   57/  61.  In element #    2,  parameter # 35  changed to    4.80267523E+00   (was    4.73477031E+00)

                                -----  REBELOTE  -----

     End of pass #       57 through the optical structure 

                     Total of         57 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        62.1       62.074044       65.0      9.973E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    1.316184E-06    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   1.7323E-12

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 58


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 58


 Pgm rebel. At pass #   58/  61.  In element #    2,  parameter # 35  changed to    4.87058014E+00   (was    4.80267523E+00)

                                -----  REBELOTE  -----

     End of pass #       58 through the optical structure 

                     Total of         58 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        63.0       62.951706       65.0      3.324E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    1.412178E-07    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   1.9942E-14

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 59


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 59


 Pgm rebel. At pass #   59/  61.  In element #    2,  parameter # 35  changed to    4.93848505E+00   (was    4.87058014E+00)

                                -----  REBELOTE  -----

     End of pass #       59 through the optical structure 

                     Total of         59 particles have been launched

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        63.8       63.829370       65.0      9.973E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    3.967393E-07    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   1.5740E-13

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 60


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 60


 Pgm rebel. At pass #   60/  61.  In element #    2,  parameter # 35  changed to    5.00638997E+00   (was    4.93848505E+00)

                                -----  REBELOTE  -----

     End of pass #       60 through the optical structure 

                     Total of         60 particles have been launched


      Next  pass  is  #    61 and  last  pass  through  the  optical  structure


     WRITE statements to zgoubi.res are re-established from now on.

************************************************************************************************************************************
      1  Keyword, label(s) :  MARKER      ProbMdlAnal_S                                                                IPASS= 61


************************************************************************************************************************************
      2  Keyword, label(s) :  OBJET                                                                                    IPASS= 61

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      3  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 61


************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 61

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

m  1   5.0064    63.829     0.000     0.000     0.000      0.0000    4.0064   63.829    0.000    0.000    0.000   0.000000E+00     1
               Time of flight (mus) :   0.0000000     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   6.382937E-01   0.000000E+00        1        1    1.000      (Y,T)        61
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        61
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   5.000000E+00        1        1    1.000      (t,K)        61

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)
      normalized (*beta*gamma) :     0.000000E+00    0.000000E+00

************************************************************************************************************************************
      5  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 61


************************************************************************************************************************************
      6  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 61


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  1.8000E+02 degrees     A.CENTRAL =  9.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  90.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -90.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :   1.000     cm   (i.e.,   2.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  5.0064    63.829     0.000     0.000     0.000            3.142    65.585    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      203.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    1.57079633     m ;  Time  (for ref. rigidity & particle) =   2.538067E-07 s 

************************************************************************************************************************************
      7  Keyword, label(s) :  MARKER      #E_60degSectorUnifB                                                          IPASS= 61


************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 61

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

m  1   5.0064    63.829     0.000     0.000     0.000      0.0000    4.0064   65.585   -0.000    0.000    0.000   2.032831E+02     1
               Time of flight (mus) :  6.59440236E-02 mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   6.558470E-01  -1.051140E-10        1        1    1.000      (Y,T)        61
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        61
   0.0000E+00   0.0000E+00   1.0000E+00   6.594402E-02   5.000000E+00        1        1    1.000      (t,K)        61

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)
      normalized (*beta*gamma) :     0.000000E+00    0.000000E+00

************************************************************************************************************************************
      9  Keyword, label(s) :  FIT                                                                                      IPASS= 61


************************************************************************************************************************************
      1  Keyword, label(s) :  MARKER      ProbMdlAnal_S                                                                IPASS= 61


************************************************************************************************************************************
      2  Keyword, label(s) :  OBJET                                                                                    IPASS= 61

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      3  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 61


************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 61

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

m  1   5.0064    63.829     0.000     0.000     0.000      0.0000    4.0064   63.829    0.000    0.000    0.000   0.000000E+00     1
               Time of flight (mus) :   0.0000000     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   6.382937E-01   0.000000E+00        1        1    1.000      (Y,T)        61
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        61
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   5.000000E+00        1        1    1.000      (t,K)        61

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)
      normalized (*beta*gamma) :     0.000000E+00    0.000000E+00

************************************************************************************************************************************
      5  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 61


************************************************************************************************************************************
      6  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 61


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  1.8000E+02 degrees     A.CENTRAL =  9.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  90.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -90.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :   1.000     cm   (i.e.,   2.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  5.0064    63.829     0.000     0.000     0.000            3.142    65.585    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      203.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    1.57079633     m ;  Time  (for ref. rigidity & particle) =   2.538067E-07 s 

************************************************************************************************************************************
      7  Keyword, label(s) :  MARKER      #E_60degSectorUnifB                                                          IPASS= 61


************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 61

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

m  1   5.0064    63.829     0.000     0.000     0.000      0.0000    4.0064   65.585   -0.000    0.000    0.000   2.032831E+02     1
               Time of flight (mus) :  6.59440236E-02 mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   6.558470E-01  -1.051140E-10        1        1    1.000      (Y,T)        61
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        61
   0.0000E+00   0.0000E+00   1.0000E+00   6.594402E-02   5.000000E+00        1        1    1.000      (t,K)        61

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)
      normalized (*beta*gamma) :     0.000000E+00    0.000000E+00

************************************************************************************************************************************
      9  Keyword, label(s) :  FIT                                                                                      IPASS= 61

     Pgm main. FITING=.T., FIT procedure launched.

           variable #            1       IR =            2 ,   ok.
           variable #            1       IP =           30 ,   ok.
           constraint #            1       IR =            8 ,   ok.
           constraint #            1       I  =            1 ,   ok.

                    FIT  variables  and  constraints  in  good  order,  FIT  will proceed. 

 STATUS OF VARIABLES  (Iteration #     0 /    199 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   2   1    30    12.0        64.7       64.707034       65.0      9.973E-07  OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   2.0000E-12)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     8    0.000000E+00    1.000E+00    9.354330E-07    1.00E+00 FAISCEAU   -                    -                    0
 Fit reached penalty value   8.7503E-13

   Last run following FIT[2] is skipped, as requested.  Now carrying on beyond FIT keyword.

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 61


************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 61


                         >>>>  End  of  'REBELOTE'  procedure  <<<<

      There  has  been         61  passes  through  the  optical  structure 

                     Total of         61 particles have been launched

************************************************************************************************************************************
     12  Keyword, label(s) :  SYSTEM                                                                                   IPASS= 62

     Number  of  commands :   1,  as  follows : 

 /usr/bin/gnuplot < ./gnuplot_TOF.gnu &

************************************************************************************************************************************
     13  Keyword, label(s) :  MARKER      ProbMdlAnal_E                                                                IPASS= 62


************************************************************************************************************************************
     14  Keyword, label(s) :  END                                                                                      IPASS= 62


************************************************************************************************************************************
 Pgm zgoubi : Execution ended normally, upon keyword END or FIN
   
 File in:   cyclo_ProbMdlAnal_180degSector.INC.dat
 File out:  zgoubi.res

  Zgoubi, author's dvlpmnt version.
  Job  started  on  24-04-2020,  at  15:07:41 
  JOB  ENDED  ON    24-04-2020,  AT  15:07:43 

   CPU time, total :     1.6808609999999999     
