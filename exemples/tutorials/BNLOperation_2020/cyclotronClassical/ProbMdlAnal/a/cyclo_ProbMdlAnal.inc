Cyclotron, classical. Analytical model of dipole field. 
'MARKER'  ProbMdlAnal_S                                                   ! Just for edition purposes.
'OBJET'   
64.62444403717985                                                                            ! 200keV.
2
1 1
12.9248888074 0. 0. 0. 0.  1.  'm'         ! D=1 => 200keV proton. R=Brho/B=64.624444037[kG.cm]/5[kG].
1
'PARTICUL'                                               ! This is required to get the time-of-flight, 
PROTON                                                      ! otherwise zgoubi only requires rigidity.
'FAISCEAU'                                                               ! Local particle coordinates.
'MARKER'    #S_60degSectorUnifB                               ! Label should not exceed 20 characters.
'DIPOLE'                                                    ! Analytical definition of a dipole field. 
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting. 
360. 50.                                                        ! Sector angle AT; reference radius RM.
180.  5. 0. 0. 0.                 ! Reference azimuthal angle ACN; BM field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge, 
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
180. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-180. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
1.                                  ! Integration step size. The smaller, the better the orbits close.
2  0. 0. 0. 0.                  ! Magnet positionning RE, TE, RS, TS. Could be isntead non-zero, e.g., 
!                                2 RE=50. 0. RS=50. 0., as long as Yo is amended accordingly in OBJET.
'MARKER'    #E_60degSectorUnifB                               ! Label should not exceed 20 characters.
'FAISCEAU'                                                               ! Local particle coordinates.
'FIT'                                                     ! Adjust Yo at OBJET so to get final Y = Yo.
1   nofinal   
2 30 0 [12.,65.]                                                                      ! Variable : Yo.
1  2e-12  199            ! ! constraint; default penalty would be 1e-10; maximu 199 calls to function.
3.1 1 2 #End 0. 1. 0                                                        ! Constraint:  Y_final=Yo.
 'FAISTORE'                                                   ! Log particle data here, to zgoubi.fai,
zgoubi.fai                                                 ! for further plotting (by gnuplot, below).
1
 'REBELOTE'                                                               ! Momentum scan, 60 samples.
60 0.2  0 1                         ! 60 different rigidities; log to video ; take initial coordinates
1                                      ! as found in OBJET ; change parameter(s) as stated next lines.
OBJET 35  1:5.0063899693             ! Change relative rigity (35) in OBJET; range (0.2 MeV to 5 MeV).
'SYSTEM'                                                         
1                                                                          ! 2 SYSTEM commands follow.
/usr/bin/gnuplot < ./gnuplot_TOF.gnu &                             ! Launch plot by ./gnuplot_TOF.gnu. 
'MARKER'  ProbMdlAnal_E                                                   ! Just for edition purposes.
 'END'                 
