set xlabel "R [m]"; set ylabel "T_{rev} [{/Symbol m}s]"; set y2label "f_{rev} [MHz]"
set xtics mirror; set ytics nomirror; set y2tics nomirror ; set key  t l ;  set key spacin 1.2
sector = 180. # should be AT
nSector=360/sector; Hz2MHz=1e-6; M=938.272e6; c=2.99792458e8; B=0.5; freqNonRel(x)= Hz2MHz* c**2*B/M/(2.*pi)
plot \
"zgoubi.fai" u 10:($15 *nSector) axes x1y1 w lp pt 5 ps .6 lw 2 linecolor rgb "blue" tit "T_{rev}" ,\
"zgoubi.fai" u 10:(1/($15*nSector)) axes x1y2 w lp pt 6 ps .6 lw 2 linecol rgb "red" tit "f_{rev}" 

pause 1

     set terminal postscript eps blacktext color enh
       set output "gnuplot_cycloBUnif_MomScan.eps"  
       replot  
       set terminal X11  
       unset output  

exit
