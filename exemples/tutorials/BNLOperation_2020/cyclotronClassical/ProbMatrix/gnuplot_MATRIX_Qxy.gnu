set xlabel "kin. E [MeV]";set ylabel "{/Symbol n}_x, ({/Symbol n}_x^2+{/Symbol n}_y^2)^{1/2}";set y2label "{/Symbol n}_y"
set key t l maxrow 1; set xtics; set ytics nomirror; set y2tics nomirror
BORO = 64.62444403717985; am = 938.27203e6; c = 2.99792458e8; BrhoRef = BORO *1e-3; eV2MeV = 1e-6
plot "zgoubi.MATRIX.out" u ((sqrt(($47*BrhoRef*c)**2 + am*am)-am)*eV2MeV):($56) w lp pt 5 lt 1 lw .5 lc rgb "red" \
tit "{/Symbol n}_x " , "zgoubi.MATRIX.out" u ((sqrt(($47*BrhoRef*c)**2 + am*am)-am)*eV2MeV):($57) axes x1y2 w lp \
pt 6 lt 3 lw .5 lc rgb "blue" tit "{/Symbol n}_y " , "zgoubi.MATRIX.out" u ((sqrt(($47*BrhoRef*c)**2 + am*am)-am)* \
eV2MeV):(sqrt($56**2+$57**2)) w lp pt 7 lt 1 lw .5 lc rgb "black" tit "    ({/Symbol n}_x^2+{/Symbol n}_y^2)^{1/2}"
pause 1

set terminal postscript eps blacktext color enh   # "Times-Roman" 12
 set output "gnuplot_MATRIX_Qxy.eps"
 replot
 set terminal X11
 unset output


exit

