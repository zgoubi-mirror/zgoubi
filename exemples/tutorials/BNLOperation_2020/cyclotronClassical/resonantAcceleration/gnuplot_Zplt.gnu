set xtics ;  set ytics ; set xlabel "X_{Lab} [m]" ; set ylabel "Y_{Lab} [m]" 
set size ratio 1 ; set polar ; cm2m = 0.01 ; pi = 4.*atan(1.)
set arrow from 0, 0 to 0, 0.67 nohead linecolor "red" lw 6
set arrow from 0, -0.75 to 0, 0 nohead linecolor "blue" lw 6
noel_1=6 ; noel_2=10  # 1st CAVITE is element $42=noel_1; 2nd CAVITE is $42=noel_2. $42=column number in zgoubi.plt.
plot "zgoubi.plt" u ($42==noel_1? $22    :1/0):($10 *cm2m) w p pt 5 ps .2 lc rgb "black"  notit ,\
     "zgoubi.plt" u ($42==noel_2? $22+pi :1/0):($10 *cm2m) w p pt 5 ps .2 lc rgb "black"  notit 

set terminal postscript eps blacktext color  enh  
set output "gnuplot_Zplt_cycloClass_Accel_XYLab.eps"  
replot  
set terminal X11  
unset output   
pause 3
exit

