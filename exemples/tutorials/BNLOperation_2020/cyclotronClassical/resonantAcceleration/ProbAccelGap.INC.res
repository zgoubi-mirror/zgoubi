ProbAccelGap
 
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./probAccelGap.inc[ProbAccelGap_S,*:ProbAcce(n_inc, depth :  1 2)
'MARKER'  ProbAccelGap_S                                                  ! Just for edition purposes.      11
'OBJET'                                                                                                     12
64.62444403717985                                                                            ! 200keV.
2
1 1
4.087013 0. 0. 0. 0. 0.3162126 'o'            ! D=0.3162126 => Brho[kG.cm]= 20.435064, kin-E[keV]= 20.
1
'PARTICUL'                                                   ! Usage of CAVITE requires partical data.      13
PROTON
'FAISTORE'                                                        ! Store particle data, turn-by-turn.      14
zgoubi.fai  afterCAVITE   ! A "label 1" is indicated => log to zgoubi.fai will occur there, see below.
1
! 'INCLUDE'                                                          ! Inset a 180 deg sector field map.  
! Include_SegmentStart : FieldMapSector.inc[#S_halfDipole,*:#E_halfDi(n_inc, depth :  1 3)
'MARKER'   #S_halfDipole                                                                                    15
'TOSCA'                                                                                                     16
0  2
1.  1. 1. 1.
HEADER_8
315 151 1 22.1 1.            ! IZ=1 for 2D map; MOD=22 for polar frame; .MOD2=.1 if only one map file.
geneSectorMap.out
0 0 0 0
2
1.
2
0. 0. 0. 0.
! Include_SegmentEnd : FieldMapSector.inc                            (had n_inc, depth :  1 3)
'MARKER'   #E_halfDipole                                                                                    17
'FAISCEAU'                                                       ! Particle coordinates before cavity.      18
'CAVITE'                                                                                                    19
3                                     ! dW = qVsin(phi), independent of time (phi forced to constant).
0. 0.                                                                                        ! Unused.
100e3  1.57079632679                                           ! Peak voltage 100 kV; RF phase = pi/2.
! 'INCLUDE'                                                          ! Inset a 180 deg sector field map.  
! Include_SegmentStart : FieldMapSector.inc[#S_halfDipole,*:#E_halfDi(n_inc, depth :  1 3)
'TOSCA'                                                                                                     20
0  2
1.  1. 1. 1.
HEADER_8
315 151 1 22.1 1.            ! IZ=1 for 2D map; MOD=22 for polar frame; .MOD2=.1 if only one map file.
geneSectorMap.out
0 0 0 0
2
1.
2
0. 0. 0. 0.
! Include_SegmentEnd : FieldMapSector.inc                            (had n_inc, depth :  1 3)
'MARKER'   #E_halfDipole                                                                                    21
'FAISCEAU'                                                       ! Particle coordinates before cavity.      22
'CAVITE'                                                                                                    23
3                                     ! dW = qVsin(phi), independent of time (phi forced to constant).
0. 0.                                                                                        ! Unused.
100e3  1.57079632679                                           ! Peak voltage 100 kV; RF phase = pi/2.
'MARKER'   afterCAVITE                                  ! Log particle data to zgoubi.fai occurs here.      24
'REBELOTE'             ! Repeat NRBLT=24 times, for a total of 25 turns; K = 99: coordinates at end of      25
24  0.1 99                          ! previous pass are used as initial coordinates for the next pass.
'FAISCEAU'                                                                        ! Final coordinates.      26
 
'SYSTEM'                                                                                                    27
1                                                                          ! 1 SYSTEM command follows.
/usr/bin/gnuplot < ./gnuplot_Zplt.gnu &                                           ! Plot trajectories.
! Include_SegmentEnd : ./probAccelGap.inc                            (had n_inc, depth :  1 2)
'MARKER'  ProbAccelGap_E                                                  ! Just for edition purposes.      28

'END'

************************************************************************************************************************************
      1  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
      2  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      3  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 1

     Particle  properties :
     PROTON
                     Mass          =    938.272        MeV/c2
                     Charge        =   1.602176E-19    C     
                     G  factor     =    1.79285              
                     COM life-time =   1.000000E+99    s     

              Reference  data :
                    mag. rigidity (kG.cm)   :   64.624444      =p/q, such that dev.=B*L/rigidity
                    mass (MeV/c2)           :   938.27208    
                    momentum (MeV/c)        :   19.373921    
                    energy, total (MeV)     :   938.47208    
                    energy, kinetic (MeV)   :  0.19999999    
                    beta = v/c              :  2.0644110049E-02
                    gamma                   :   1.000213158    
                    beta*gamma              :  2.0648510502E-02
                    G*gamma                 :   1.793229509    
                    electric rigidity (MeV) :  0.3999573557    =T[eV]*(gamma+1)/gamma, such that dev.=E*L/rigidity
  
 I, AMQ(1,I), AMQ(2,I)/QE, P/Pref, v/c, time, s :
  
     1   9.38272081E+02  1.00000000E+00  3.16212600E-01  6.52918002E-03  0.00000000E+00  0.00000000E+00

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 1


                OPEN FILE zgoubi.fai                                                                      
                FOR PRINTING COORDINATES 

               Print will occur at element[s] labeled : 
                    afterCAVITE         

************************************************************************************************************************************
      5  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
      6  Keyword, label(s) :  TOSCA                                                                                    IPASS= 1


                OPEN FILE zgoubi.plt                                                                      
                FOR PRINTING TRAJECTORIES


     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           2-D or 3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00

          New field map(s) now used, polar mesh (MOD .ge. 20) ;  name(s) of map data file(s) are : 
          'geneSectorMap.out'

   ----
   Map file number    1 ( of 1) :

     geneSectorMap.out map,  FORMAT type : regular.  Field multiplication factor :  1.00000000E+00

 HEADER  (8 lines) : 
        1.0000000000000000       0.50000000000000000       0.57324840764331209        0.0000000000000000           !  Rmi/cm,
      # Field map generated using geneSectorMap.f                                                                            
     # AT/rd,  AT/deg, Rmi/cm, Rma/cm, RM/cm, NR, dR/cm, NX, dX/cm, dA/rd :                                                  
     #   3.14159265E+00   1.80000000E+02   1.00000000E+00   7.60000000E+01   5.00000000E+01 151   5.00000000E-01 315   5.0025
      # For TOSCA:          315         151  1 22.1 1.  !IZ=1 -> 2D ; MOD=22 -> polar map ; .MOD2=.1 -> one map file         
      # R*cosA (A:0->360), Z==0, R*sinA, BY, BZ, BX                                                                          
      # cm                 cm    cm      kG  kG  kG                                                                          
      #                                                                                                                      
 
R_min (cm), DR (cm), DTTA (deg), DZ (cm) :   1.000000E+00  5.000000E-01  5.732484E-01  0.000000E+00



     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, Z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571     4.086    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (       14.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =            NaN s 

************************************************************************************************************************************
      7  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000   -0.6838    4.086   -0.030    0.000    0.000   1.283831E+01     1
               Time of flight (mus) :  6.55886195E-02 mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.085806E-02  -2.998651E-05        1        1    1.000      (Y,T)         1
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)         1
   0.0000E+00   0.0000E+00   1.0000E+00   6.558862E-02   2.000000E-02        1        1    1.000      (t,K)         1

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)
      normalized (*beta*gamma) :     0.000000E+00    0.000000E+00

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                                                   IPASS= 1

               Accelerating cavity. Type is :   OPTION 3  

                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =            NaN s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  TOSCA                                                                                    IPASS= 1


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           2-D or 3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, Z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    15.937     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (       33.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =            NaN s 

************************************************************************************************************************************
     11  Keyword, label(s) :  MARKER      ProbAccelGap_S                                                               IPASS= 1


************************************************************************************************************************************
     12  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #     11)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000   -0.2254   15.937    0.012    0.000    0.000   4.428959E+01     1
               Time of flight (mus) :  0.13119076     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   1.593684E-01   1.175977E-05        1        1    1.000      (Y,T)         1
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)         1
   0.0000E+00   0.0000E+00   1.0000E+00   1.311908E-01   1.200000E-01        1        1    1.000      (t,K)         1

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)
      normalized (*beta*gamma) :     0.000000E+00    0.000000E+00

************************************************************************************************************************************
     13  Keyword, label(s) :  CAVITE                                                                                   IPASS= 1

               Accelerating cavity. Type is :   OPTION 3  

                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =            NaN s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     14  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
     15  Keyword, label(s) :  REBELOTE    #S_halfDipole                                                                IPASS= 1


                                -----  REBELOTE  -----

     End of pass #        1 through the optical structure 

                     Total of          1 particles have been launched

     Multiple pass, 
          from element #     1 : MARKER    /label1=                    /label2=                    
                             to  REBELOTE  /label1=                    /label2=                    
     ending at pass #      25 at element #    15 : REBELOTE  /label1=#S_halfDipole       /label2=                    


     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

                                -----  REBELOTE  -----

     End of pass #        2 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #        3 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #        4 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #        5 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #        6 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #        7 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #        8 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #        9 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       10 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       11 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       12 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       13 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       14 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       15 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       16 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       17 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       18 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       19 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       20 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       21 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       22 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       23 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       24 through the optical structure 

                     Total of          1 particles have been launched


      Next  pass  is  #    25 and  last  pass  through  the  optical  structure


************************************************************************************************************************************
      1  Keyword, label(s) :  MARKER                                                                                   IPASS= 25


************************************************************************************************************************************
      2  Keyword, label(s) :  OBJET                                                                                    IPASS= 25



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      3  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 25


************************************************************************************************************************************
      4  Keyword, label(s) :  FAISTORE                                                                                 IPASS= 25


************************************************************************************************************************************
      5  Keyword, label(s) :  MARKER                                                                                   IPASS= 25


************************************************************************************************************************************
      6  Keyword, label(s) :  TOSCA                                                                                    IPASS= 25


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           2-D or 3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, Z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    59.955    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      200.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =            NaN s 

************************************************************************************************************************************
      7  Keyword, label(s) :  MARKER                                                                                   IPASS= 25


************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 25

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    3.9152   59.955   -0.002    0.000    0.000   6.512197E+03     1
               Time of flight (mus) :   3.2224112     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   5.995461E-01  -1.873075E-06        1        1    1.000      (Y,T)        25
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        25
   0.0000E+00   0.0000E+00   1.0000E+00   3.222411E+00   4.820000E+00        1        1    1.000      (t,K)        25

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)
      normalized (*beta*gamma) :     0.000000E+00    0.000000E+00

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                                                   IPASS= 25

               Accelerating cavity. Type is :   OPTION 3  

                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =            NaN s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  TOSCA                                                                                    IPASS= 25


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           2-D or 3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, Z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    68.417     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      201.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =            NaN s 

************************************************************************************************************************************
     11  Keyword, label(s) :  MARKER      ProbAccelGap_S                                                               IPASS= 25


************************************************************************************************************************************
     12  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 25

0                                             TRACE DU FAISCEAU
                                           (follows element #     11)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    3.9661   68.417    0.002    0.000    0.000   6.713843E+03     1
               Time of flight (mus) :   3.2883496     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   6.841725E-01   1.853468E-06        1        1    1.000      (Y,T)        25
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        25
   0.0000E+00   0.0000E+00   1.0000E+00   3.288350E+00   4.920000E+00        1        1    1.000      (t,K)        25

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)
      normalized (*beta*gamma) :     0.000000E+00    0.000000E+00

************************************************************************************************************************************
     13  Keyword, label(s) :  CAVITE                                                                                   IPASS= 25

               Accelerating cavity. Type is :   OPTION 3  

                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =            NaN s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     14  Keyword, label(s) :  MARKER                                                                                   IPASS= 25


************************************************************************************************************************************
     15  Keyword, label(s) :  REBELOTE    #S_halfDipole                                                                IPASS= 25


                         >>>>  End  of  'REBELOTE'  procedure  <<<<

      There  has  been         25  passes  through  the  optical  structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
     16  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 26

0                                             TRACE DU FAISCEAU
                                           (follows element #     15)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    4.0164   68.417    0.002    0.000    0.000   6.713843E+03     1
               Time of flight (mus) :   3.2883496     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   6.841725E-01   1.834866E-06        1        1    1.000      (Y,T)        26
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        26
   0.0000E+00   0.0000E+00   1.0000E+00   3.288350E+00   5.020000E+00        1        1    1.000      (t,K)        26

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)
      normalized (*beta*gamma) :     0.000000E+00    0.000000E+00

************************************************************************************************************************************
     17  Keyword, label(s) :  SYSTEM      #E_halfDipole                                                                IPASS= 26

     Number  of  commands :   1,  as  follows : 

 /usr/bin/gnuplot < ./gnuplot_Zplt.gnu &

************************************************************************************************************************************
     18  Keyword, label(s) :  MARKER                                                                                   IPASS= 26


************************************************************************************************************************************
     19  Keyword, label(s) :  END                                                                                      IPASS= 26


************************************************************************************************************************************
 Pgm zgoubi : Execution ended normally, upon keyword END or FIN
   
 File in:   ProbAccelGap.INC.dat
 File out:  zgoubi.res

  Zgoubi, author's dvlpmnt version.
  Job  started  on  23-03-2020,  at  20:24:05 
  JOB  ENDED  ON    23-03-2020,  AT  20:24:05 

   CPU time, total :    0.37241800000000003     
