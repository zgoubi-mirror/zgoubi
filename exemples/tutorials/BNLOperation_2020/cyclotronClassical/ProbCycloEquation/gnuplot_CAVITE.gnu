set xlabel "W  [MeV]" ; set ylabel "cos({/Symbol f})"; set xtics; set ytics mirror
pi = 4. * atan(1.); E0 = 938.2720813; qV=400e-3; Ei=0.2; W = 10 # locate max of cos(phi) at 10 MeV
omgR = 1. /  (1. + W/E0); mxTurn=80; y1(x)=1; ym1(x)=-1
plot [0.2:20] [-1.1:1.8]  for [i=2:1:-1] \
'zgoubi.CAVITE.Out' u ($5==i && $6<mxTurn? $10 :1/0):(cos($11)) w p pt i+4 ps .5 notit ,\
cos(pi/2.)  +pi*(1.-omgR *(1.+x/(2*E0))) *(x-Ei)/(.5*qV) w l lw 2 lc rgb "blue" tit "V/gap=200kV, {/Symbol f}_0={/Symbol  p}/2" ,\
cos(3*pi/4.)+pi*(1.-omgR *(1.+x/(2*E0))) *(x-Ei)/(.5*qV) w l lw 2 lc rgb "red"  tit "             {/Symbol f}_0={/Symbol 3p}/4" ,\
y1(x) w l lw .5 lc rgb "black" notit, ym1(x) w l lw .5 lc rgb "black" notit

     set terminal postscript eps blacktext color  enh size 6cm,5cm "Times-Roman" 12  
       set output "gnuplot_CAVITE_cycloEq.eps"  
       replot  
       set terminal X11  
       unset output  

pause 1
exit
