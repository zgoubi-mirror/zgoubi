Cyclotron, classical. H and V focusing
'OBJET'                                                                                                      1
64.62444403717985                                                                            ! 200keV.
1
3  1   1   1   1   1
2.8 0.  0.  0.  0.  1. 'm'                            ! Sampling: 0, +/-2.8cm add to Y0=12.9248888074.
12.9248888074 0.  0.  0. 0. 1. 'm'                                            ! 200keV. R=Brho/B=*/.5.
! 'INCLUDE'                                                                                               
! Include_SegmentStart : 60DegSectorR200.inc[#S_60DegSectorR200,*:#E_(n_inc, depth : *1 2)
'DIPOLE'   #S_60DegSectorR200                               ! Analytical definition of a dipole field.       2
2          ! IL=2, purpose: log stepwise particle data to zgoubi.plt. Avoid if unused: takes CPU time.
60. 12.924888                                                  ! Sector angle AT; reference radius RM.
30.  5. -0.03  0. 0.             ! Reference azimuthal angle ACN; BM field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : 60DegSectorR200.inc                           (had n_inc, depth :  1 2)
'FAISCEAU'  #E_60DegSectorR200                                                                               3
! 'INCLUDE'                                                                                               
! Include_SegmentStart : 60DegSectorR200.inc[#S_60DegSectorR200,*:#E_(n_inc, depth : *1 2)
'DIPOLE'   #S_60DegSectorR200                               ! Analytical definition of a dipole field.       4
2          ! IL=2, purpose: log stepwise particle data to zgoubi.plt. Avoid if unused: takes CPU time.
60. 12.924888                                                  ! Sector angle AT; reference radius RM.
30.  5. -0.03  0. 0.             ! Reference azimuthal angle ACN; BM field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : 60DegSectorR200.inc                           (had n_inc, depth :  1 2)
'FAISCEAU'  #E_60DegSectorR200                                                                               5
! 'INCLUDE'                                                                                               
! Include_SegmentStart : 60DegSectorR200.inc[#S_60DegSectorR200,*:#E_(n_inc, depth : *1 2)
'DIPOLE'   #S_60DegSectorR200                               ! Analytical definition of a dipole field.       6
2          ! IL=2, purpose: log stepwise particle data to zgoubi.plt. Avoid if unused: takes CPU time.
60. 12.924888                                                  ! Sector angle AT; reference radius RM.
30.  5. -0.03  0. 0.             ! Reference azimuthal angle ACN; BM field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : 60DegSectorR200.inc                           (had n_inc, depth :  1 2)
'FAISCEAU'  #E_60DegSectorR200                                                                               7
! 'INCLUDE'                                                                                               
! Include_SegmentStart : 60DegSectorR200.inc[#S_60DegSectorR200,*:#E_(n_inc, depth : *1 2)
'DIPOLE'   #S_60DegSectorR200                               ! Analytical definition of a dipole field.       8
2          ! IL=2, purpose: log stepwise particle data to zgoubi.plt. Avoid if unused: takes CPU time.
60. 12.924888                                                  ! Sector angle AT; reference radius RM.
30.  5. -0.03  0. 0.             ! Reference azimuthal angle ACN; BM field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : 60DegSectorR200.inc                           (had n_inc, depth :  1 2)
'FAISCEAU'  #E_60DegSectorR200                                                                               9
! 'INCLUDE'                                                                                               
! Include_SegmentStart : 60DegSectorR200.inc[#S_60DegSectorR200,*:#E_(n_inc, depth : *1 2)
'DIPOLE'   #S_60DegSectorR200                               ! Analytical definition of a dipole field.      10
2          ! IL=2, purpose: log stepwise particle data to zgoubi.plt. Avoid if unused: takes CPU time.
60. 12.924888                                                  ! Sector angle AT; reference radius RM.
30.  5. -0.03  0. 0.             ! Reference azimuthal angle ACN; BM field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : 60DegSectorR200.inc                           (had n_inc, depth :  1 2)
'FAISCEAU'  #E_60DegSectorR200                                                                              11
! 'INCLUDE'                                                                                               
! Include_SegmentStart : 60DegSectorR200.inc[#S_60DegSectorR200,*:#E_(n_inc, depth : *1 2)
'DIPOLE'   #S_60DegSectorR200                               ! Analytical definition of a dipole field.      12
2          ! IL=2, purpose: log stepwise particle data to zgoubi.plt. Avoid if unused: takes CPU time.
60. 12.924888                                                  ! Sector angle AT; reference radius RM.
30.  5. -0.03  0. 0.             ! Reference azimuthal angle ACN; BM field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                ! EFB 3 (unused).
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
0.5                        ! Integration step size. The smaller, the more accurately the orbits close.
2  0. 0. 0. 0.                                                   ! Magnet positionning RE, TE, RS, TS.
! Include_SegmentEnd : 60DegSectorR200.inc                           (had n_inc, depth :  1 2)
'FAISCEAU'  #E_60DegSectorR200                                                                              13
'REBELOTE'                                                               ! Scan k, 3 differnet values.      14
3  0.2  0 1
1                                                                        ! Change data under 1 kyword.
DIPOLE 6  -0.5:0.5                               ! Change index (parameter 6) in DIPOLE; -0.5, 0. 0.5.
'SYSTEM'                                                                                                    15
1                                                                          ! 1 SYSTEM command follows.
/usr/bin/gnuplot < ./gnuplot_Zplt_vk.gnu &                                              ! Launch plot.
'END'

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (1)  BUILT  UP  FROM       3 POINTS 



                                 D         Y(cm)       T(mrd)      Z(cm)       P(mrd)      X(cm)

                     NUMBER         1           3           1           1           1           0


                    SAMPLING      1.0000      2.80        0.00        0.00        0.00        0.00


************************************************************************************************************************************
      2  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 1


                OPEN FILE zgoubi.plt                                                                      
                FOR PRINTING TRAJECTORIES

                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925    -0.000     0.000     0.000            1
  A    1  1.0000    15.725     0.000     0.000     0.000            1.047    14.147    -0.183     0.000     0.000            2
  A    1  1.0000    10.125     0.000     0.000     0.000            1.047    11.268     0.185     0.000     0.000            3


                CONDITIONS  DE  MAXWELL  (       83.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.135349111     m ;  Time  (for ref. rigidity & particle) =   2.186948E-08 s 

************************************************************************************************************************************
      3  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #      2)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

O  1   1.0000    12.925     0.000     0.000     0.000      0.0000    0.0000   12.925   -0.000    0.000    0.000   1.353491E+01     1
A  1   1.0000    15.725     0.000     0.000     0.000      0.0000    0.0000   14.147 -183.289    0.000    0.000   1.598770E+01     2
B  1   1.0000    10.125     0.000     0.000     0.000      0.0000    0.0000   11.268  184.976    0.000    0.000   1.108235E+01     3


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   4.6957E-04   1.1824E+01   9.3108E-01   1.277990E-01   5.620997E-04        3        3    1.000      (Y,T)         1
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        3        3    1.000      (Z,P)         1
   0.0000E+00   0.0000E+00   1.0000E+00   4.514786E-04   1.937392E+01        3        0    0.000      (t,K)         1

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   1.179689E-02
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   1.503441E-01

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   1.391666E-04  -1.767283E-03   0.000000E+00   0.000000E+00
  -1.767283E-03   2.260334E-02   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     1.494674E-04    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      4  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925    -0.000     0.000     0.000            1
  A    1  1.0000    15.725     0.000     0.000     0.000            1.047    11.379    -0.189     0.000     0.000            2
  A    1  1.0000    10.125     0.000     0.000     0.000            1.047    14.024     0.189     0.000     0.000            3


                CONDITIONS  DE  MAXWELL  (       83.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.270698221     m ;  Time  (for ref. rigidity & particle) =   4.373897E-08 s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

O  1   1.0000    12.925     0.000     0.000     0.000      0.0000    0.0000   12.925   -0.000    0.000    0.000   2.706982E+01     1
A  1   1.0000    15.725     0.000     0.000     0.000      0.0000    0.0000   11.379 -189.165    0.000    0.000   2.959373E+01     2
B  1   1.0000    10.125     0.000     0.000     0.000      0.0000    0.0000   14.024  189.212    0.000    0.000   2.455372E+01     3


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   5.1181E-04  -1.0237E+01   7.2233E-01   1.277585E-01   1.562114E-05        3        3    1.000      (Y,T)         1
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        3        3    1.000      (Z,P)         1
   0.0000E+00   0.0000E+00   1.0000E+00   9.030390E-04   1.937392E+01        3        0    0.000      (t,K)         1

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   1.084797E-02
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   1.544719E-01

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   1.176784E-04   1.667768E-03   0.000000E+00   0.000000E+00
   1.667768E-03   2.386158E-02   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     1.629142E-04    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925    -0.000     0.000     0.000            1
  A    1  1.0000    15.725     0.000     0.000     0.000            1.047    10.140    -0.010     0.000     0.000            2
  A    1  1.0000    10.125     0.000     0.000     0.000            1.047    15.734     0.010     0.000     0.000            3


                CONDITIONS  DE  MAXWELL  (       83.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.406047332     m ;  Time  (for ref. rigidity & particle) =   6.560845E-08 s 

************************************************************************************************************************************
      7  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #      6)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

O  1   1.0000    12.925     0.000     0.000     0.000      0.0000    0.0000   12.925   -0.000    0.000    0.000   4.060473E+01     1
A  1   1.0000    15.725     0.000     0.000     0.000      0.0000    0.0000   10.140  -10.096    0.000    0.000   4.075374E+01     2
B  1   1.0000    10.125     0.000     0.000     0.000      0.0000    0.0000   15.734   10.050    0.000    0.000   4.048543E+01     3


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   2.2101E-06  -2.6698E+02   7.4132E+02   1.293278E-01  -1.514902E-05        3        3    1.000      (Y,T)         1
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        3        3    1.000      (Z,P)         1
   0.0000E+00   0.0000E+00   1.0000E+00   1.354758E-03   1.937392E+01        3        0    0.000      (t,K)         1

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   2.283694E-02
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   8.224572E-03

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   5.215259E-04   1.878228E-04   0.000000E+00   0.000000E+00
   1.878228E-04   6.764358E-05   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     7.035119E-07    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      8  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925     0.000     0.000     0.000            1
  A    1  1.0000    15.725     0.000     0.000     0.000            1.047    11.178     0.179     0.000     0.000            2
  A    1  1.0000    10.125     0.000     0.000     0.000            1.047    14.276    -0.179     0.000     0.000            3


                CONDITIONS  DE  MAXWELL  (       83.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.541396443     m ;  Time  (for ref. rigidity & particle) =   8.747794E-08 s 

************************************************************************************************************************************
      9  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #      8)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

O  1   1.0000    12.925     0.000     0.000     0.000      0.0000    0.0000   12.925    0.000    0.000    0.000   5.413964E+01     1
A  1   1.0000    15.725     0.000     0.000     0.000      0.0000    0.0000   11.178  178.701    0.000    0.000   5.178594E+01     2
B  1   1.0000    10.125     0.000     0.000     0.000      0.0000    0.0000   14.276 -178.616    0.000    0.000   5.654589E+01     3


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   4.2682E-04   1.3582E+01   1.1843E+00   1.279292E-01   2.865784E-05        3        3    1.000      (Y,T)         1
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        3        3    1.000      (Z,P)         1
   0.0000E+00   0.0000E+00   1.0000E+00   1.806488E-03   1.937392E+01        3        0    0.000      (t,K)         1

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   1.268450E-02
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   1.458740E-01

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   1.608967E-04  -1.845345E-03   0.000000E+00   0.000000E+00
  -1.845345E-03   2.127923E-02   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     1.358621E-04    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     10  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925     0.000     0.000     0.000            1
  A    1  1.0000    15.725     0.000     0.000     0.000            1.047    13.891     0.193     0.000     0.000            2
  A    1  1.0000    10.125     0.000     0.000     0.000            1.047    11.478    -0.195     0.000     0.000            3


                CONDITIONS  DE  MAXWELL  (       83.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.676745553     m ;  Time  (for ref. rigidity & particle) =   1.093474E-07 s 

************************************************************************************************************************************
     11  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #     10)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

O  1   1.0000    12.925     0.000     0.000     0.000      0.0000    0.0000   12.925    0.000    0.000    0.000   6.767455E+01     1
A  1   1.0000    15.725     0.000     0.000     0.000      0.0000    0.0000   13.891  193.014    0.000    0.000   6.512335E+01     2
B  1   1.0000    10.125     0.000     0.000     0.000      0.0000    0.0000   11.478 -194.645    0.000    0.000   7.028713E+01     3


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   5.5232E-04  -8.8695E+00   5.5921E-01   1.276440E-01  -5.436692E-04        3        3    1.000      (Y,T)         1
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        3        3    1.000      (Z,P)         1
   0.0000E+00   0.0000E+00   1.0000E+00   2.258063E-03   1.937392E+01        3        0    0.000      (t,K)         1

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   9.915374E-03
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   1.582618E-01

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   9.831463E-05   1.559345E-03   0.000000E+00   0.000000E+00
   1.559345E-03   2.504680E-02   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     1.758092E-04    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     12  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N = -3.0000E-02     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925     0.000     0.000     0.000            1
  A    1  1.0000    15.725     0.000     0.000     0.000            1.047    15.710     0.020     0.000     0.000            2
  A    1  1.0000    10.125     0.000     0.000     0.000            1.047    10.135    -0.020     0.000     0.000            3


                CONDITIONS  DE  MAXWELL  (       83.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.812094664     m ;  Time  (for ref. rigidity & particle) =   1.312169E-07 s 

************************************************************************************************************************************
     13  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #     12)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

O  1   1.0000    12.925     0.000     0.000     0.000      0.0000    0.0000   12.925    0.000    0.000    0.000   8.120947E+01     1
A  1   1.0000    15.725     0.000     0.000     0.000      0.0000    0.0000   15.710   19.991    0.000    0.000   8.097206E+01     2
B  1   1.0000    10.125     0.000     0.000     0.000      0.0000    0.0000   10.135  -20.259    0.000    0.000   8.150866E+01     3


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   3.8794E-06  -3.0287E+02   4.1951E+02   1.292315E-01  -8.930736E-05        3        3    1.000      (Y,T)         1
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        3        3    1.000      (Z,P)         1
   0.0000E+00   0.0000E+00   1.0000E+00   2.709543E-03   1.937392E+01        3        0    0.000      (t,K)         1

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   2.276017E-02
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   1.643243E-02

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   5.180255E-04   3.740029E-04   0.000000E+00   0.000000E+00
   3.740029E-04   2.700247E-04   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     1.234844E-06    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     14  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 1


 Pgm rebel. At pass #    1/   4.  In element #    2,  parameter #  6  changed to   -5.00000000E-01   (was   -3.00000000E-02)

 Pgm rebel. At pass #    1/   4.  In element #    4,  parameter #  6  changed to   -5.00000000E-01   (was   -3.00000000E-02)

 Pgm rebel. At pass #    1/   4.  In element #    6,  parameter #  6  changed to   -5.00000000E-01   (was   -3.00000000E-02)

 Pgm rebel. At pass #    1/   4.  In element #    8,  parameter #  6  changed to   -5.00000000E-01   (was   -3.00000000E-02)

 Pgm rebel. At pass #    1/   4.  In element #   10,  parameter #  6  changed to   -5.00000000E-01   (was   -3.00000000E-02)

 Pgm rebel. At pass #    1/   4.  In element #   12,  parameter #  6  changed to   -5.00000000E-01   (was   -3.00000000E-02)

                                -----  REBELOTE  -----

     End of pass #        1 through the optical structure 

                     Total of          3 particles have been launched

     Multiple pass, 
          from element #     1 : OBJET     /label1=                    /label2=                    
                             to  REBELOTE  /label1=                    /label2=                    
     ending at pass #       4 at element #    14 : REBELOTE  /label1=                    /label2=                    


     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

                                -----  REBELOTE  -----

     End of pass #        2 through the optical structure 

                     Total of          6 particles have been launched

                                -----  REBELOTE  -----

     End of pass #        3 through the optical structure 

                     Total of          9 particles have been launched


      Next  pass  is  #     4 and  last  pass  through  the  optical  structure


************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 4

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (1)  BUILT  UP  FROM       3 POINTS 



                                 D         Y(cm)       T(mrd)      Z(cm)       P(mrd)      X(cm)

                     NUMBER         1           3           1           1           1           0


                    SAMPLING      1.0000      2.80        0.00        0.00        0.00        0.00


************************************************************************************************************************************
      2  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 4


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  5.0000E-01     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925    -0.000     0.000     0.000            1
  A    1  1.0000    15.725     0.000     0.000     0.000            1.047    13.310    -0.266     0.000     0.000            2
  A    1  1.0000    10.125     0.000     0.000     0.000            1.047    11.749     0.246     0.000     0.000            3


                CONDITIONS  DE  MAXWELL  (       83.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.135349111     m ;  Time  (for ref. rigidity & particle) =   4.155202E-07 s 

************************************************************************************************************************************
      3  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 4

0                                             TRACE DU FAISCEAU
                                           (follows element #      2)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

O  1   1.0000    12.925     0.000     0.000     0.000      0.0000    0.0000   12.925   -0.000    0.000    0.000   1.353491E+01     1
A  1   1.0000    15.725     0.000     0.000     0.000      0.0000    0.0000   13.310 -266.020    0.000    0.000   1.578303E+01     2
B  1   1.0000    10.125     0.000     0.000     0.000      0.0000    0.0000   11.749  246.250    0.000    0.000   1.132622E+01     3


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   1.3180E-03   3.1557E+00   1.0506E-01   1.266128E-01  -6.590024E-03        3        3    1.000      (Y,T)         4
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        3        3    1.000      (Z,P)         4
   0.0000E+00   0.0000E+00   1.0000E+00   4.519144E-04   1.937392E+01        3        0    0.000      (t,K)         4

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   6.638841E-03
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   2.091855E-01

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   4.407421E-05  -1.323868E-03   0.000000E+00   0.000000E+00
  -1.323868E-03   4.375856E-02   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     4.195194E-04    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      4  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 4


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  5.0000E-01     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925    -0.000     0.000     0.000            1
  A    1  1.0000    15.725     0.000     0.000     0.000            1.047    10.338    -0.143     0.000     0.000            2
  A    1  1.0000    10.125     0.000     0.000     0.000            1.047    15.039     0.148     0.000     0.000            3


                CONDITIONS  DE  MAXWELL  (       83.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.270698221     m ;  Time  (for ref. rigidity & particle) =   4.373897E-07 s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 4

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

O  1   1.0000    12.925     0.000     0.000     0.000      0.0000    0.0000   12.925   -0.000    0.000    0.000   2.706982E+01     1
A  1   1.0000    15.725     0.000     0.000     0.000      0.0000    0.0000   10.338 -143.196    0.000    0.000   2.832486E+01     2
B  1   1.0000    10.125     0.000     0.000     0.000      0.0000    0.0000   15.039  147.946    0.000    0.000   2.579999E+01     3


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   4.8362E-04  -1.4809E+01   2.4004E+00   1.276728E-01   1.583153E-03        3        3    1.000      (Y,T)         4
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        3        3    1.000      (Z,P)         4
   0.0000E+00   0.0000E+00   1.0000E+00   9.027876E-04   1.937392E+01        3        0    0.000      (t,K)         4

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   1.922299E-02
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   1.188635E-01

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   3.695235E-04   2.279720E-03   0.000000E+00   0.000000E+00
   2.279720E-03   1.412853E-02   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     1.539420E-04    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 4


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  5.0000E-01     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925     0.000     0.000     0.000            1
  A    1  1.0000    15.725     0.000     0.000     0.000            1.047    10.496     0.169     0.000     0.000            2
  A    1  1.0000    10.125     0.000     0.000     0.000            1.047    14.803    -0.173     0.000     0.000            3


                CONDITIONS  DE  MAXWELL  (       83.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.406047332     m ;  Time  (for ref. rigidity & particle) =   4.592592E-07 s 

************************************************************************************************************************************
      7  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 4

0                                             TRACE DU FAISCEAU
                                           (follows element #      6)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

O  1   1.0000    12.925     0.000     0.000     0.000      0.0000    0.0000   12.925    0.000    0.000    0.000   4.060473E+01     1
A  1   1.0000    15.725     0.000     0.000     0.000      0.0000    0.0000   10.496  168.638    0.000    0.000   3.898031E+01     2
B  1   1.0000    10.125     0.000     0.000     0.000      0.0000    0.0000   14.803 -173.299    0.000    0.000   4.196193E+01     3


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   6.2957E-04   1.2240E+01   1.5509E+00   1.274144E-01  -1.553764E-03        3        3    1.000      (Y,T)         4
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        3        3    1.000      (Z,P)         4
   0.0000E+00   0.0000E+00   1.0000E+00   1.351457E-03   1.937392E+01        3        0    0.000      (t,K)         4

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   1.762942E-02
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   1.395993E-01

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   3.107963E-04  -2.452882E-03   0.000000E+00   0.000000E+00
  -2.452882E-03   1.948797E-02   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     2.003972E-04    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      8  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 4


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  5.0000E-01     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925     0.000     0.000     0.000            1
  A    1  1.0000    15.725     0.000     0.000     0.000            1.047    13.658     0.256     0.000     0.000            2
  A    1  1.0000    10.125     0.000     0.000     0.000            1.047    11.472    -0.234     0.000     0.000            3


                CONDITIONS  DE  MAXWELL  (       83.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.541396443     m ;  Time  (for ref. rigidity & particle) =   4.811286E-07 s 

************************************************************************************************************************************
      9  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 4

0                                             TRACE DU FAISCEAU
                                           (follows element #      8)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

O  1   1.0000    12.925     0.000     0.000     0.000      0.0000    0.0000   12.925    0.000    0.000    0.000   5.413964E+01     1
A  1   1.0000    15.725     0.000     0.000     0.000      0.0000    0.0000   13.658  256.036    0.000    0.000   5.185191E+01     2
B  1   1.0000    10.125     0.000     0.000     0.000      0.0000    0.0000   11.472 -234.334    0.000    0.000   5.612723E+01     3


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   1.2108E-03  -4.6139E+00   2.1420E-01   1.268488E-01   7.233793E-03        3        3    1.000      (Y,T)         4
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        3        3    1.000      (Z,P)         4
   0.0000E+00   0.0000E+00   1.0000E+00   1.802567E-03   1.937392E+01        3        0    0.000      (t,K)         4

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   9.085988E-03
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   2.002581E-01

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   8.255519E-05   1.778256E-03   0.000000E+00   0.000000E+00
   1.778256E-03   4.010331E-02   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     3.854136E-04    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     10  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 4


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  5.0000E-01     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925    -0.000     0.000     0.000            1
  A    1  1.0000    15.725     0.000     0.000     0.000            1.047    15.699    -0.034     0.000     0.000            2
  A    1  1.0000    10.125     0.000     0.000     0.000            1.047    10.139     0.029     0.000     0.000            3


                CONDITIONS  DE  MAXWELL  (       83.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.676745553     m ;  Time  (for ref. rigidity & particle) =   5.029981E-07 s 

************************************************************************************************************************************
     11  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 4

0                                             TRACE DU FAISCEAU
                                           (follows element #     10)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

O  1   1.0000    12.925     0.000     0.000     0.000      0.0000    0.0000   12.925   -0.000    0.000    0.000   6.767456E+01     1
A  1   1.0000    15.725     0.000     0.000     0.000      0.0000    0.0000   15.699  -33.560    0.000    0.000   6.780493E+01     2
B  1   1.0000    10.125     0.000     0.000     0.000      0.0000    0.0000   10.139   29.316    0.000    0.000   6.727602E+01     3


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   7.3422E-05   2.4929E+01   2.2045E+01   1.292122E-01  -1.414656E-03        3        3    1.000      (Y,T)         4
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        3        3    1.000      (Z,P)         4
   0.0000E+00   0.0000E+00   1.0000E+00   2.254399E-03   1.937392E+01        3        0    0.000      (t,K)         4

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   2.269817E-02
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   2.568873E-02

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   5.152070E-04  -5.826186E-04   0.000000E+00   0.000000E+00
  -5.826186E-04   6.599109E-04   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     2.337102E-05    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     12  Keyword, label(s) :  DIPOLE      #S_60DegSectorR200                                                           IPASS= 4


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  1.2925E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  5.0000E-01     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :  0.5000     cm   (i.e.,   3.8685E-02 rad  at mean radius RM =    12.92    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925    -0.000     0.000     0.000            1
  A    1  1.0000    15.725     0.000     0.000     0.000            1.047    12.960    -0.272     0.000     0.000            2
  A    1  1.0000    10.125     0.000     0.000     0.000            1.047    12.046     0.255     0.000     0.000            3


                CONDITIONS  DE  MAXWELL  (       84.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.812094664     m ;  Time  (for ref. rigidity & particle) =   5.248676E-07 s 

************************************************************************************************************************************
     13  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 4

0                                             TRACE DU FAISCEAU
                                           (follows element #     12)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

O  1   1.0000    12.925     0.000     0.000     0.000      0.0000    0.0000   12.925   -0.000    0.000    0.000   8.120947E+01     1
A  1   1.0000    15.725     0.000     0.000     0.000      0.0000    0.0000   12.960 -272.082    0.000    0.000   8.338577E+01     2
B  1   1.0000    10.125     0.000     0.000     0.000      0.0000    0.0000   12.046  254.784    0.000    0.000   7.881169E+01     3


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   1.3920E-03   1.7922E+00   4.0327E-02   1.264356E-01  -5.765823E-03        3        3    1.000      (Y,T)         4
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        3        3    1.000      (Z,P)         4
   0.0000E+00   0.0000E+00   1.0000E+00   2.706394E-03   1.937392E+01        3        0    0.000      (t,K)         4

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   4.227124E-03
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   2.151307E-01

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   1.786858E-05  -7.941333E-04   0.000000E+00   0.000000E+00
  -7.941333E-04   4.628121E-02   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     4.430935E-04    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     14  Keyword, label(s) :  REBELOTE                                                                                 IPASS= 4


                         >>>>  End  of  'REBELOTE'  procedure  <<<<

      There  has  been          4  passes  through  the  optical  structure 

                     Total of         12 particles have been launched

************************************************************************************************************************************
     15  Keyword, label(s) :  SYSTEM                                                                                   IPASS= 5

     Number  of  commands :   1,  as  follows : 

 /usr/bin/gnuplot < ./gnuplot_Zplt_vk.gnu &

************************************************************************************************************************************
     16  Keyword, label(s) :  END                                                                                      IPASS= 5


************************************************************************************************************************************
 Pgm zgoubi : Execution ended normally, upon keyword END or FIN
   
 File in:   cyclo_DIPOLE_varyIdx.INC.dat
 File out:  zgoubi.res

  Zgoubi, author's dvlpmnt version.
  Job  started  on  28-05-2020,  at  13:15:20 
  JOB  ENDED  ON    28-05-2020,  AT  13:15:20 

   CPU time, total :    0.17892699999999997     
