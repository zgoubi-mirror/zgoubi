unset xtics ;  unset ytics ; unset grid ; unset border; set polar ; set size ratio 1; cm2m = 0.01; 
unset colorbox; pi = 4.*atan(1.); DIPL1=2; dT=2 # number of 2nd DIPL & increment in zgoubi.plt listing
set palette defined ( 1 "red", 2 "blue", 3 "black" ) ; unset colorbox
pass1 =2 ; set xrange [-.2:.2]; set yrange [-.2:.2]; set key at graph .99, .85
plot \
for [it=1:3] "zgoubi.plt" u ($19==it && $41==pass1+1 ? $22 + ($42-DIPL1)/dT *pi/3 :1/0): \
($10 *cm2m ):($19) w l lw 2 lc palette tit (it==1? 'k=0' :'') ,\
for [it=2:3] "zgoubi.plt" u ($19==it && $41==pass1   && ($42==DIPL1 || $42==DIPL1+dT) ? $22 + ($42-DIPL1)/dT *pi/3 :1/0): \
($10 *cm2m ):($19) w p ps .7 pt 8 lc palette tit (it==2? 'k<0' :'') ,\
for [it=2:3] "zgoubi.plt" u ($19==it && $41==pass1+2 && ($42==DIPL1 || $42==DIPL1+dT) ? $22 + ($42-DIPL1)/dT *pi/3 :1/0): \
($10 *cm2m ):($19) w p ps .6 pt 4 lc palette  tit (it==2? 'k>0' :'') 

set label 1 at  .139, 0.011 'R_0'  center rotate by -90 front
set label 2 at  .085, 0.019 'R_0-{/Symbol d}R' center rotate by -90 front
set label 3 at  .17, 0.019 'R_0+{/Symbol d}R' center rotate by -90 front

pause 1

set terminal postscript eps blacktext color  enh  
set output "gnuplot_Zplt_cycloClass_vk.eps"  
replot  
set terminal X11  
unset output   
exit

