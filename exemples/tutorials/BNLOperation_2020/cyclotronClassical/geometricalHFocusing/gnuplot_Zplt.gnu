set xtics ;  set ytics ; set xlabel "X_{Lab} [m]" ; set ylabel "Y_{Lab} [m]"
set size ratio 1 ; set polar ; unset colorbox ; cm2m = 0.01 ; pi = 4.*atan(1.)
TOSCA1=4 ; TOSCA2=14 ; dT = 3  # number of 1st TOSCA in zgoubi.plt listing, of 4th TOSCA
plot for [i=TOSCA1:TOSCA1+2*dT:dT] "zgoubi.plt" u ($22 + (i/dT)  *pi/3.):($42==i ? $10*cm2m :1/0):19 w p ps .4 notit ,\
     for [i=TOSCA2:TOSCA2+2*dT:dT] "zgoubi.plt" u ($22 + (i-1)/dT*pi/3.):($42==i ? $10*cm2m :1/0):19 w p ps .4 notit 

set terminal postscript eps blacktext color  enh  
set output "gnuplot_Zplt_XYLab_cycloClass_HGFoc.eps"  
replot  
set terminal X11  
unset output   
pause 3
exit

