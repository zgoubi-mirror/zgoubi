Cyclotron, classical. Geometrical horizontal focusing
 
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./probCycloHGFoc.inc[ProbCycloHGFoc_S,*:Prob(n_inc, depth :  1 2)
'MARKER'  ProbCycloHGFoc_S                                                ! Just for edition purposes.       1
'OBJET'                                                                                                      2
64.62444403717985                      ! Reference Brho ("BORO" in the users' guide) -> 200keV proton.
2
3 1
12.9248888074 +100. 0. 0.  0.  1. 'm'      ! D=1 => 200keV proton. R=Brho/B=64.624444037[kG.cm]/5[kG].
12.9248888074    0. 0. 0.  0.  1. 'm'               ! three trajectories leave from the same position,
12.9248888074 -100. 0. 0.  0.  1. 'm'                    ! with three different angles: 0, +/-10 mrad.
1 1 1
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./60degSector.inc[#S_60degSectorUnifB,*:#E_6(n_inc, depth : *1 3)
'MARKER'    #S_60degSectorUnifB                               ! Label should not exceed 20 characters.       3
'DIPOLE'                                                   ! Analytical definition of a dipole field.        4
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                              ! Sector angle; reference radius.
30.  5. 0. 0. 0.                     ! Reference azimuthal angle; Bo field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                         ! EFB 3.
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
1.                                  ! Integration step size. The smaller, the better the orbits close.
2  0. 0. 0. 0.                  ! Magnet positionning RE, TE, RS, TS. Could be isntead non-zero, e.g.,
! 2 RE=50. 0. RS=50. 0., as long as Yo is amended accordingly in OBJET.
! Include_SegmentEnd : ./60degSector.inc                             (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorUnifB                               ! Label should not exceed 20 characters.       5
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./60degSector.inc[#S_60degSectorUnifB,*:#E_6(n_inc, depth : *1 3)
'MARKER'    #S_60degSectorUnifB                               ! Label should not exceed 20 characters.       6
'DIPOLE'                                                   ! Analytical definition of a dipole field.        7
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                              ! Sector angle; reference radius.
30.  5. 0. 0. 0.                     ! Reference azimuthal angle; Bo field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                         ! EFB 3.
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
1.                                  ! Integration step size. The smaller, the better the orbits close.
2  0. 0. 0. 0.                  ! Magnet positionning RE, TE, RS, TS. Could be isntead non-zero, e.g.,
! 2 RE=50. 0. RS=50. 0., as long as Yo is amended accordingly in OBJET.
! Include_SegmentEnd : ./60degSector.inc                             (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorUnifB                               ! Label should not exceed 20 characters.       8
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./60degSector.inc[#S_60degSectorUnifB,*:#E_6(n_inc, depth : *1 3)
'MARKER'    #S_60degSectorUnifB                               ! Label should not exceed 20 characters.       9
'DIPOLE'                                                   ! Analytical definition of a dipole field.       10
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                              ! Sector angle; reference radius.
30.  5. 0. 0. 0.                     ! Reference azimuthal angle; Bo field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                         ! EFB 3.
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
1.                                  ! Integration step size. The smaller, the better the orbits close.
2  0. 0. 0. 0.                  ! Magnet positionning RE, TE, RS, TS. Could be isntead non-zero, e.g.,
! 2 RE=50. 0. RS=50. 0., as long as Yo is amended accordingly in OBJET.
! Include_SegmentEnd : ./60degSector.inc                             (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorUnifB                               ! Label should not exceed 20 characters.      11
'FAISCEAU'                                         ! Placeholder, for further insertion of and RF gap.      12
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./60degSector.inc[#S_60degSectorUnifB,*:#E_6(n_inc, depth : *1 3)
'MARKER'    #S_60degSectorUnifB                               ! Label should not exceed 20 characters.      13
'DIPOLE'                                                   ! Analytical definition of a dipole field.       14
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                              ! Sector angle; reference radius.
30.  5. 0. 0. 0.                     ! Reference azimuthal angle; Bo field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                         ! EFB 3.
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
1.                                  ! Integration step size. The smaller, the better the orbits close.
2  0. 0. 0. 0.                  ! Magnet positionning RE, TE, RS, TS. Could be isntead non-zero, e.g.,
! 2 RE=50. 0. RS=50. 0., as long as Yo is amended accordingly in OBJET.
! Include_SegmentEnd : ./60degSector.inc                             (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorUnifB                               ! Label should not exceed 20 characters.      15
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./60degSector.inc[#S_60degSectorUnifB,*:#E_6(n_inc, depth : *1 3)
'MARKER'    #S_60degSectorUnifB                               ! Label should not exceed 20 characters.      16
'DIPOLE'                                                   ! Analytical definition of a dipole field.       17
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                              ! Sector angle; reference radius.
30.  5. 0. 0. 0.                     ! Reference azimuthal angle; Bo field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                         ! EFB 3.
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
1.                                  ! Integration step size. The smaller, the better the orbits close.
2  0. 0. 0. 0.                  ! Magnet positionning RE, TE, RS, TS. Could be isntead non-zero, e.g.,
! 2 RE=50. 0. RS=50. 0., as long as Yo is amended accordingly in OBJET.
! Include_SegmentEnd : ./60degSector.inc                             (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorUnifB                               ! Label should not exceed 20 characters.      18
! 'INCLUDE'                                                                                               
! Include_SegmentStart : ./60degSector.inc[#S_60degSectorUnifB,*:#E_6(n_inc, depth : *1 3)
'MARKER'    #S_60degSectorUnifB                               ! Label should not exceed 20 characters.      19
'DIPOLE'                                                   ! Analytical definition of a dipole field.       20
2                  ! IL=2, only purpose is to logged trajectories to zgoubi.plt, for further plotting.
60. 50.                                                              ! Sector angle; reference radius.
30.  5. 0. 0. 0.                     ! Reference azimuthal angle; Bo field at RM; indices, N, N', N''.
0.  0.                                                                          ! EFB 1 is  hard-edge,
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.             ! hard-edge only possible with sector magnet.
30. 0.  1.E6  -1.E6  1.E6  1.E6
0.   0.                                                                                       ! EFB 2.
4  .1455   2.2670  -.6395  1.1558  0. 0.  0.
-30. 0.  1.E6  -1.E6  1.E6  1.E6
0. 0.                                                                                         ! EFB 3.
0  0.      0.      0.      0.      0. 0.  0.
0. 0.  1.E6  -1.E6  1.E6  1.E6 0.
4   10.
1.                                  ! Integration step size. The smaller, the better the orbits close.
2  0. 0. 0. 0.                  ! Magnet positionning RE, TE, RS, TS. Could be isntead non-zero, e.g.,
! 2 RE=50. 0. RS=50. 0., as long as Yo is amended accordingly in OBJET.
! Include_SegmentEnd : ./60degSector.inc                             (had n_inc, depth :  1 3)
'MARKER'    #E_60degSectorUnifB                               ! Label should not exceed 20 characters.      21
'SYSTEM'                                                                                                    22
1                                                                          ! 1 SYSTEM command follows.
/usr/bin/gnuplot < ./gnuplot_Zplt.gnu &                                           ! Plot trajectories.
! Include_SegmentEnd : ./probCycloHGFoc.inc                          (had n_inc, depth :  1 2)
'MARKER'  ProbCycloHGFoc_E                                                ! Just for edition purposes.      23
 
'END'

************************************************************************************************************************************
      1  Keyword, label(s) :  MARKER      ProbCycloHGFoc_S                                                             IPASS= 1


************************************************************************************************************************************
      2  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      3  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
      4  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


                OPEN FILE zgoubi.plt                                                                      
                FOR PRINTING TRAJECTORIES

                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :   1.000     cm   (i.e.,   2.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925   100.000     0.000     0.000            1.047    14.061     0.046     0.000     0.000            1
  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925    -0.000     0.000     0.000            2
  A    1  1.0000    12.925  -100.000     0.000     0.000            1.047    11.821    -0.054     0.000     0.000            3


                CONDITIONS  DE  MAXWELL  (       44.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =   0.523598776     m ;  Time  (for ref. rigidity & particle) =            NaN s 

************************************************************************************************************************************
      5  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
      7  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :   1.000     cm   (i.e.,   2.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925   100.000     0.000     0.000            1.047    13.991    -0.054     0.000     0.000            1
  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925    -0.000     0.000     0.000            2
  A    1  1.0000    12.925  -100.000     0.000     0.000            1.047    11.762     0.046     0.000     0.000            3


                CONDITIONS  DE  MAXWELL  (       45.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    1.04719755     m ;  Time  (for ref. rigidity & particle) =            NaN s 

************************************************************************************************************************************
      8  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
      9  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     10  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :   1.000     cm   (i.e.,   2.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925   100.000     0.000     0.000            1.047    12.796    -0.100     0.000     0.000            1
  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925    -0.000     0.000     0.000            2
  A    1  1.0000    12.925  -100.000     0.000     0.000            1.047    12.796     0.100     0.000     0.000            3


                CONDITIONS  DE  MAXWELL  (       44.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    1.57079633     m ;  Time  (for ref. rigidity & particle) =            NaN s 

************************************************************************************************************************************
     11  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
     12  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #     11)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

m  1   1.0000    12.925   100.000     0.000     0.000      0.0000    0.0000   12.796 -100.000    0.000    0.000   4.318966E+01     1
m  1   1.0000    12.925     0.000     0.000     0.000      0.0000    0.0000   12.925   -0.000    0.000    0.000   4.060469E+01     2
m  1   1.0000    12.925  -100.000     0.000     0.000      0.0000    0.0000   12.796  100.000    0.000    0.000   3.801971E+01     3


---------------  Concentration ellipses : 
surface [pi*m]  alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   1.5616E-04   3.6961E-06   7.4559E-03   1.283876E-01  -1.151098E-07        3        3    1.000      (Y,T)         1
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        3        3    1.000      (Z,P)         1
   0.0000E+00   0.0000E+00   1.0000E+00   1.354427E-03   1.937392E+01        3        0    0.000      (t,K)         1

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   6.087728E-04
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   8.164964E-02

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   3.706044E-07  -1.837191E-10   0.000000E+00   0.000000E+00
  -1.837191E-10   6.666664E-03   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     4.970609E-05    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     13  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     14  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :   1.000     cm   (i.e.,   2.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925   100.000     0.000     0.000            1.047    11.762    -0.046     0.000     0.000            1
  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925     0.000     0.000     0.000            2
  A    1  1.0000    12.925  -100.000     0.000     0.000            1.047    13.991     0.054     0.000     0.000            3


                CONDITIONS  DE  MAXWELL  (       44.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    2.09439510     m ;  Time  (for ref. rigidity & particle) =            NaN s 

************************************************************************************************************************************
     15  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
     16  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     17  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :   1.000     cm   (i.e.,   2.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925   100.000     0.000     0.000            1.047    11.821     0.054     0.000     0.000            1
  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925     0.000     0.000     0.000            2
  A    1  1.0000    12.925  -100.000     0.000     0.000            1.047    14.061    -0.046     0.000     0.000            3


                CONDITIONS  DE  MAXWELL  (       45.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    2.61799388     m ;  Time  (for ref. rigidity & particle) =            NaN s 

************************************************************************************************************************************
     18  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
     19  Keyword, label(s) :  MARKER      #S_60degSectorUnifB                                                          IPASS= 1


************************************************************************************************************************************
     20  Keyword, label(s) :  DIPOLE                                                                                   IPASS= 1


     zgoubi.plt                                                                      
      already open...
                    Dipole  magnet

           ANGLES : A.TOTAL =  6.0000E+01 degrees     A.CENTRAL =  3.0000E+01 degrees
           RM =  5.0000E+01 cm
           HNORM =  5.0000E+00 kGauss     COEF.N =  0.0000E+00     COEF.B =  0.0000E+00     COEF.G=  0.0000E+00

     Entrance  EFB 
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA =  30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Exit  EFB     
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  4   0.14550   2.26700  -0.63950   1.15580   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

          OMEGA = -30.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

     Lateral  EFB  
          Fringe  field  : LAMBDA =   0.00 CM     QSI=   0.00
           COEFFICIENTS :  0   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
           Shift  of  EFB  =   0.0000E+00 CM

                     Face centred on direction ACENT+OMEGA, A   0.0000E+00 CM

          OMEGA =   0.00 deg.     Wedge  angle  =   0.00 deg.
           Radius 1 =  1.00E+06 CM
           Straight  segment 1 = -1.00E+06 CM
           Straight  segment 2 =  1.00E+06 CM
           Radius 2 =  1.00E+06 CM

                     Interpolation  option : 4
                    25-point  interpolation, size of flying mesh :   STEP /   10.0

                    Integration step :   1.000     cm   (i.e.,   2.0000E-02 rad  at mean radius RM =    50.00    )

                              AIMANT - KPOS = 2 ;  position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925   100.000     0.000     0.000            1.047    12.925     0.100     0.000     0.000            1
  A    1  1.0000    12.925     0.000     0.000     0.000            1.047    12.925     0.000     0.000     0.000            2
  A    1  1.0000    12.925  -100.000     0.000     0.000            1.047    12.925    -0.100     0.000     0.000            3


                CONDITIONS  DE  MAXWELL  (       44.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    3.14159265     m ;  Time  (for ref. rigidity & particle) =            NaN s 

************************************************************************************************************************************
     21  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
     22  Keyword, label(s) :  SYSTEM                                                                                   IPASS= 1

     Number  of  commands :   1,  as  follows : 

 /usr/bin/gnuplot < ./gnuplot_Zplt.gnu &

************************************************************************************************************************************
     23  Keyword, label(s) :  MARKER                                                                                   IPASS= 1


************************************************************************************************************************************
     24  Keyword, label(s) :  END                                                                                      IPASS= 1


                             3 particles have been launched
                     Made  it  to  the  end :      3

************************************************************************************************************************************
 Pgm zgoubi : Execution ended normally, upon keyword END or FIN
   
 File in:   cyclo_DIPOLE.INC.dat
 File out:  zgoubi.res

  Zgoubi, author's dvlpmnt version.
  Job  started  on  19-05-2020,  at  07:08:11 
  JOB  ENDED  ON    19-05-2020,  AT  07:08:12 

   CPU time, total :     1.7482000000000001E-002
