! This SCALING sets YO9_TH, _TV* and YI3_TH*, _TV* orbit kickers for local orbit bump at 
! snake current values as defined under TOSCA family elements.

'MARKER' scaling_S                                                                                
 
'SCALING'                                                                                         
1  22
BEND
-1
  7.9366778931E+01
1
MULTIPOL
-1
  7.9366778931E+01
1
MULTIPOL  *_QF*
-1
  7.9223617000E+01
1
MULTIPOL *_QD*
-1
  7.9417707000E+01
1
MULTIPOL  *_SXF*
-1
  4.9207402937E+01
1
MULTIPOL  *_SXD*
-1
  1.3889186313E+02
1
TOSCA    snk1LowB            ! snk1 = 9'o
-1
  1.0000000000E+02                                                    !  low-field coils current (A).
1
TOSCA    snk1HighB            ! snk1 = 9'o
-1
  3.2200000000E+02                                                   !  high-field coils current (A).
1
TOSCA    snk2LowB            ! snk2 = 3'o
-1
  1.0000000000E+02                                                    !  low-field coils current (A).
1
TOSCA    snk2HighB            ! snk2 = 3'o
-1
  3.2200000000E+02                                                   !  high-field coils current (A).
1
MULTIPOL YO9_TV5                                   ! Next 6 elements:  9 o'clock snake orbit kickers. 
-1
 -1.3648842000E+05
1
MULTIPOL YO9_TV7
-1
 -3.0003133064E+05
1
MULTIPOL YO9_TH8
-1
 -2.4864019368E+04
1
MULTIPOL YO9_TV9
-1
 -5.0324566128E+04
1
MULTIPOL YO9_TH10
-1
 -1.3085573629E+04
1
MULTIPOL YO9_TV11
-1
 -3.7782862905E+05
1
MULTIPOL YI3_TV4                                   ! Next 6 elements:  3 o'clock snake orbit kickers.
-1
  4.6924283222E+05
1
MULTIPOL YI3_TV6
-1
  2.2817388948E+05
1
MULTIPOL YI3_TV8
-1
  2.9194984956E+05
1
MULTIPOL YI3_TH9
-1
  3.4867674869E+03
1
MULTIPOL YI3_TV10
-1
  1.2477343675E+05
1
MULTIPOL YI3_TH11
-1
  3.4077370771E+04
1

'MARKER' scaling_E                                                                                  

'END'
