! Orbit kickers set for orbit bump at step 0 of radial polarization adjustment
'MARKER' scaling_S                                                                                           
 
'SCALING'                                                                                                    
1  23
BEND
-1
  7.9366778931E+01
1
MULTIPOL
-1
  7.9366778931E+01
1
MULTIPOL  *_QF*
-1
  7.9223617000E+01
1
MULTIPOL *_QD*
-1
  7.9417707000E+01
1
MULTIPOL  *_SXF*
-1
  4.9207402937E+01
1
MULTIPOL  *_SXD*
-1
  1.3889186313E+02
1
TOSCA    snk1LowB            ! snk1 = 9'o
11
184.08966 184.08967 184.08966 184.08966 184.08967 184.08966 178.43276 172.63753 166.69559 160.59563 154.32192 
1         501       1001      1501      2001      2501      3001      3501      4001      4501      5001
TOSCA    snk1HighB            ! snk1 = 9'o
11
199.07334 199.07334 199.07336 199.07335 199.07334 199.07334 200.72111 202.52287 204.46703 206.54226 208.73700 
1         501       1001      1501      2001      2501      3001      3501      4001      4501      5001
TOSCA    snk2LowB            ! snk2 = 3'o
11
164.16759 166.17474 168.19734 170.23614 172.29233 174.36709 174.36707 174.36709 174.36712 174.36710 174.36709 
1         501       1001      1501      2001      2501      3001      3501      4001      4501      5001
TOSCA    snk2HighB            ! snk2 = 3'o
11
220.45335 222.79739 225.14344 227.49256 229.84602 232.20533 232.20531 232.20531 232.20540 232.20535 232.20534 
1         501       1001      1501      2001      2501      3001      3501      4001      4501      5001
MULTIPOL YO9_TV5             ! Next 6 elements:  9 o'clock snake orbit kickers.
-1
  1.7905968411E+05
1
MULTIPOL YO9_TV7
-1
  3.6564632690E+05
1
MULTIPOL YO9_TH8
-1
  6.4252488673E+03
1
MULTIPOL YO9_TV9
-1
  5.2125941079E+04
1
MULTIPOL YO9_TH10
-1
  6.7996064758E+03
1
MULTIPOL YO9_TV11
-1
  5.0732776649E+05
1
MULTIPOL YI3_TV4             ! Next 6 elements:  3 o'clock snake orbit kickers.
-1
 -5.0364070518E+05
1
MULTIPOL YI3_TV6
-1
 -2.4489762740E+05
1
MULTIPOL YI3_TV8
-1
 -2.9093216368E+05
1
MULTIPOL YI3_TH9
-1
  1.9964797598E+03
1
MULTIPOL YI3_TV10
-1
 -1.3881781797E+05
1
MULTIPOL YI3_TH11
-1
  4.1869407887E+03
1
MULTIPOL  none ! *TH* *TV* 
-1
0.
1

'MARKER' scaling_E                                                                                 

'END'
