
set title "I_{out} and I_{in} current in 9 o'clock snake"

set xlabel "step"
set ylabel "{/Symbol m},  {/Symbol f} [deg],   Z_{co} [10^{-4}m],   I_{out, I_{in}} [A]"

set xtics
set ytics nomirror
set y2tics 

set key maxcol 1
set k t r
set grid

system "grep '4   2     4 ' zgoubi.FITVALS.out | cat > temp1"                   # I_out
system "grep '4   3     8 ' zgoubi.FITVALS.out | cat > temp2"                   # I_in
system "grep 'Fit reached penalty value' zgoubi.FITVALS.out | cat > temp3"      # penalty
system "grep '10   1   0    19 ' zgoubi.FITVALS.out | cat > temp4"              # phi
system "grep '10   1   2    19 ' zgoubi.FITVALS.out | cat > temp5"              # sin(mu)
system "grep '1   1    42 ' zgoubi.FITVALS.out | cat > temp6"                   # Z_co

r2d = 180. / (4.*atan(1.))

plot [:11.5] \
 "temp1" u 0:($6) w lp pt 4 tit "I_{out}" ,\
 "temp2" u 0:($6) w lp pt 5 tit "I_{in}"  ,\
 "temp6" u 0:($6 * 100.) w lp pt 10 tit "Z_{co}"  ,\
 "temp3" u 0:($5) axes x1y2 w lp pt 6 tit "penalty" ,\
 "temp4" u 0:($7 *r2d) w lp pt 7 tit "phi"  ,\
 "temp5" u 0:(asin($7) *r2d) w lp pt 8 tit "mu "  ,\

 set terminal postscript eps blacktext color enh 
 set output "gnuplot_FITVALS.eps"
 replot
 set terminal X11
 unset output

pause 2

exit
