set xtics mirror
set ytics mirror

set xlabel 'Y  [m]'
set ylabel 'Z  [m]'

set size ratio -1

set grid

cm2m = 0.01
kG2T= 0.1
MeV2eV = 1e6
am = 938.27203
c = 2.99792458e8


# A possibility, for plotting YZ through snake only:
snk1_1 = 47; snk1_2 = 49; snk1_3 = 52; snk1_4 = 54

plot \
for [IT=1:1]  'zgoubi.plt' u ($19==IT && ($42==snk1_1 || $42==snk1_2 || $42==snk1_3 || $42==snk1_4)  ? $10 : 1/0):($12) w lp ps .4 lc rgb "black" 

pause 1

# Another  possibility for a similar result: 
sMin = 37.e2 ; sMax = 49e2
plot \
for [IT=1:1]  'zgoubi.plt' u ($19==IT && ($14 > sMin && $14< sMax)  ? $10 : 1/0):($12) w lp ps .4 lc rgb "black" 

     set terminal postscript eps color  enh  
       set output "gnuplot_Zplt_YZ.eps"  
       replot  
       set terminal X11  
       unset output  
 
pause 1
exit

