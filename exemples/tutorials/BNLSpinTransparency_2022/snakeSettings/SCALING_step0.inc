! This SCALING sets YO9_TH, _TV* and YI3_TH*, _TV* orbit kickers for local orbit bump at 
! snake current values as defined under TOSCA family elements.

'MARKER' scaling_S                                                                                           5
 
'SCALING'                                                                                                    6
1  22
BEND
-1
  7.9366778931E+01
1
MULTIPOL
-1
  7.9366778931E+01
1
MULTIPOL  *_QF*
-1
  7.9223617000E+01
1
MULTIPOL *_QD*
-1
  7.9417707000E+01
1
MULTIPOL  *_SXF*
-1
  4.9207402937E+01
1
MULTIPOL  *_SXD*
-1
  1.3889186313E+02
1
TOSCA    snk1LowB            ! snk1 = 9'o
-1
184.08966                                                             !  low-field coils current (A).
1 
TOSCA    snk1HighB            ! snk1 = 9'o
-1
199.073334                                                           !  high-field coils current (A).
1
TOSCA    snk2LowB            ! snk2 = 3'o
-1
164.16759                                                             !  low-field coils current (A).
1
TOSCA    snk2HighB            ! snk2 = 3'o
-1
220.45335                                                            !  high-field coils current (A).
1
MULTIPOL YO9_TV5                                   ! Next 6 elements:  9 o'clock snake orbit kickers. 
-1
  1.7905968411E+05
1
MULTIPOL YO9_TV7
-1
  3.6564632690E+05
1
MULTIPOL YO9_TH8
-1
  6.4252488673E+03
1
MULTIPOL YO9_TV9
-1
  5.2125941079E+04
1
MULTIPOL YO9_TH10
-1
  6.7996064758E+03
1
MULTIPOL YO9_TV11
-1
  5.0732776649E+05
1
MULTIPOL YI3_TV4                                   ! Next 6 elements:  3 o'clock snake orbit kickers.
-1
 -5.0371183255E+05
1
MULTIPOL YI3_TV6
-1
 -2.4493149758E+05
1
MULTIPOL YI3_TV8
-1
 -2.9097619491E+05
1
MULTIPOL YI3_TH9
-1
  1.9930927422E+03
1
MULTIPOL YI3_TV10
-1
 -1.3883136604E+05
1
MULTIPOL YI3_TH11
-1
  4.1869407887E+03
1

'MARKER' scaling_E                                                                                           7
