set tit "Spin tune and spin vector versus turn number "
set xlabel "Turn"; set ylabel "n_{0,X},  n_{0,Y},  n_{0,Z}"; set y2label "Q_{sp}" 
set xtics;  set ytics nomirror ;  set y2tics nomirror 
set key maxcol 1
#set xrange [1:6000]; set yrange [-1.:1.]

plot \
       "spinTuneFromFai_iterate.Out"  u (10* $8):($2) w lp pt 11 tit "5000 turns; n_{0,X}" ,\
       "spinTuneFromFai_iterate.Out"  u (10* $8):($3) w lp pt 13 tit "n_{0,Y}" ,\
       "spinTuneFromFai_iterate.Out"  u (10* $8):($4) w lp pt 15 tit "n_{0,Z}" ,\
       "spinTuneFromFai_iterate.Out"  u (10* $8):($1) axes x1y2 w lp pt 17 ps .8  tit "Q_{sp}" 
 
      set terminal postscript eps blacktext color  enh 
       set output "gnuplot_Qspin.vs.turn.eps"  
       replot  
       set terminal X11  
       unset output  

      pause 1


 exit
