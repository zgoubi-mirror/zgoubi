! This SCALING sets YO9_TH, _TV* and YI3_TH*, _TV* orbit kickers for local orbit bump at 
! snake current values as defined under TOSCA family elements.
! TOSCA family currents are set to vary as a function of turn number,
! over step 0 through step 10 from turn 1 to turn 5001.

'MARKER' scaling_S                                                                                           
 
'SCALING'                                                                                                    
1  22
BEND
-1
  7.9366778931E+01
1
MULTIPOL
-1
  7.9366778931E+01
1
MULTIPOL  *_QF*
-1
  7.9223617000E+01
1
MULTIPOL *_QD*
-1
  7.9417707000E+01
1
MULTIPOL  *_SXF*
-1
  4.9207402937E+01
1
MULTIPOL  *_SXD*
-1
  1.3889186313E+02
1
TOSCA    snk1LowB            ! snk1 = 9'o
11
184.0896 184.0896 184.0896 184.0896 184.0896 184.0896 178.4327 172.6375 166.6955 160.5956 154.3219 
1        501      1001     1501     2001     2501     3001     3501     4001     4501     5001
TOSCA    snk1HighB            ! snk1 = 9'o
11
199.0733 199.0733 199.0733 199.0733 199.0733 199.0733 200.7211 202.5228 204.4670 206.5422 208.7370 
1        501      1001     1501     2001     2501     3001     3501     4001     4501     5001
TOSCA    snk2LowB            ! snk2 = 3'o
11
164.1675 166.1747 168.1973 170.2361 172.2923 174.3670 174.3670 174.3670 174.3671 174.3671 174.3670 
1        501      1001     1501     2001     2501     3001     3501     4001     4501     5001
TOSCA    snk2HighB            ! snk2 = 3'o
11
220.4533 222.7973 225.1434 227.4925 229.8460 232.2053 232.2053 232.2053 232.2054 232.2053 232.2053 
1        501      1001     1501     2001     2501     3001     3501     4001     4501     5001
MULTIPOL YO9_TV5                                   ! Next 6 elements:  9 o'clock snake orbit kickers.
-1
  1.7905968411E+05
1
MULTIPOL YO9_TV7
-1
  3.6564632690E+05
1
MULTIPOL YO9_TH8
-1
  6.4252488673E+03
1
MULTIPOL YO9_TV9
-1
  5.2125941079E+04
1
MULTIPOL YO9_TH10
-1
  6.7996064758E+03
1
MULTIPOL YO9_TV11
-1
  5.0732776649E+05
1
MULTIPOL YI3_TV4                                   ! Next 6 elements:  3 o'clock snake orbit kickers.
-1
 -5.0364070518E+05
1
MULTIPOL YI3_TV6
-1
 -2.4489762740E+05
1
MULTIPOL YI3_TV8
-1
 -2.9093216368E+05
1
MULTIPOL YI3_TH9
-1
  1.9964797598E+03
1
MULTIPOL YI3_TV10
-1
 -1.3881781797E+05
1
MULTIPOL YI3_TH11
-1
  4.1869407887E+03
1

'MARKER' scaling_E                                                                                 

'END'
