set tit "Spin components vs. turn number  \n from zgoubi.fai"

set key maxcol 1 spacin 1
set key c r

#set logscale y 

set xtics nomirror 
#set x2tics nomirror 
set ytics mirror 

set xlabel 'turn' 
# set x2label 'G{/Symbol g}' 
set ylabel 'S_X,  S_Y,  S_Z' 

# ELECTRON : 
# am = 0.511
# G = 1.1596e-3
# PROTON :
am = 938.27203e6
G = 1.79284735

m2cm = 100.
MeV2eV = 1e6
c = 2.99792458e8

V = 6000.
phs = 0.23635651
dEdt = V * sin(phs)
Ein = sqrt( (5018.67e-3*c)**2 + am**2)

plot \
   "zgoubi.fai" u ($38):($20) w p pt 5 ps .4 lc rgbcolor "red"    tit "S_X traj." ,\
   "zgoubi.fai" u ($38):($21) w p pt 6 ps .4 lc rgbcolor "green"  tit "S_Y" ,\
   "zgoubi.fai" u ($38):($22) w p pt 7 ps .4 lc rgbcolor "blue"   tit "S_Z"

#plot  for [i=1:3:1]  \
#   "zgoubi.fai" u ($26== i ? $38 : 1/0):($20) w p pt 2*i+1 ps .4 lc i  tit "S_X traj.".i ,\
#   "zgoubi.fai" u ($26== i ? $38 : 1/0):($21) w p pt 2*i+1 ps .4 lc i  tit "S_Y" ,\
#   "zgoubi.fai" u ($26== i ? $38 : 1/0):($22) w p pt 2*i+1 ps .4 lc i  tit "S_Z"

#,\
#   "zgoubi.fai" u ($26== i ? G*(Ein + ($38-1)*dEdt)/am : 1/0):($22) axes x2y1 w p pt 2*i+1 ps .4 lc i  notit

     set terminal postscript eps blacktext color  enh  "Times-Roman" 12  
       set output "gnuplot_Zfai_turn-Spin.eps"  
       replot  
       set terminal X11  
       unset output  

 
pause 1
exit

