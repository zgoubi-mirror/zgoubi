
set title "Plotted from file zgoubi.plt  \n From zgoubi's polar frame to lab frame \n u  " font "sans, 14"

set key maxcol 1
set key t r 

#set logscale y 

set xtics mirror font  "sans, 14"
set ytics mirror font  "sans, 14"

set xlabel 'B_Z [T]' font  "sans, 14"
set ylabel 'X=Angle [rd]' font  "sans, 14"

cm2m = 0.01
MeV2eV = 1e6
am = 938.27203
c = 2.99792458e8
pi = 4. * atan(1.)

plot  \
   'zgoubi.plt' u ($22):($25) w l lc rgb 'red' notit 

     set terminal postscript eps blacktext color  enh  "Times-Sans" 12  
       set output "gnuplot_zgoubi.plt_B.vs.X.eps"  
       replot  
       set terminal X11  
       unset output  

 
pause 2   # don't change this: needed for proper running of sector180deg
exit

