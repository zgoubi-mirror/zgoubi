150MeV FFAG at KEK. Field maps provided by Y. Mori and coll.
 'OBJET'                                                                                                      1
1839.090113 150MeV
2
1 1
444.15  0.    0.0  0. 0.   0.273042677097 'o'   12MeV  Brho=502.1500877
1
 
 'TOSCA'    #START                                                                                            2
0  0
-1.e-3   1. 1. 1.                     ! This yields:  B/kG, angle/rad, radius/cm, z/cm.
FFAG 150MeV HEADER_8. Map from Aiba    ! Tells that field map file header has 8 lines.
301 121 41   20                       ! Numb. of mesh nodes in  IX(angle) JY(radius) KZ.  MOD=20.
./fieldMap/k75v113my021f45500d2700.table    ! A binary format "./b_fieldMap/k75v113my021f45500d2700.table"
0 0 0 0                                   ! would save a lot of CPU time.
2
1.25                         ! Integration step size/cm.
2
0. 0. 0. 0.
 'DRIFT'                     ! A trick to get coordinates of particle #1 with high precision                  3
0.                          ! Another possibility is to look into   orbits4Obj3.fai
 'FAISCEAU'   #END                                                                                            4
 
 'FIT'                                                                                                        5
1                             ! 1 variable :
1 30 0 .5                     ! Y_0 in OBJET.
2      1e-10  500             ! 2 constraints. Optional: FIT will stop if penalty<1e-10 or numb of iterations>200.
3.1 1  2   #End 0. 1. 0       ! Constrt #1: equal in and out Y.
3   1  3   #End 0. 1. 0       ! Constrt #1: make sure final angle is nul (by symmetry).
 
 'FAISCEAU'                                                                                                   6
 
 'FAISTORE'                                                                                                   7
orbits4Obj3.fai
1
 
 'REBELOTE'                                                                                                   8
8 0.1 0 1
1
OBJET  35  2.73043000E-01:1.
 
 'FAISCEAU'                                                                                                   9
 
 'END'                                                                                                       10

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 

                          MAGNETIC  RIGIDITY =       1839.090 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  TOSCA       #START                                    


     NDIM =   3 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 20.0


          New field map(s) now used, polar mesh (MOD .ge. 20) ;  name(s) of map data file(s) are : 
          './fieldMap/k75v113my021f45500d2700.table'

   ----
   Map file number    1 ( of 1) :

     ./fieldMap/k75v113my021f45500d2700.table map,  FORMAT type : regular
 HEADER  (8 lines) : 
      430. 1  .1  .1   0  0  0                                                                                               
      1 X                                                                                                                    
      2 Y                                                                                                                    
      3 Z                                                                                                                    
      4 BX                                                                                                                   
      5 BY                                                                                                                   
      6 BZ                                                                                                                   
      0 [CGS]                                                                                                                
 
R_min (cm), DR (cm), DTTA (deg), DZ (cm) :   4.300000E+02  1.000000E+00  1.000000E-01  1.000000E-01



     Field map limits, angle :  min, max, max-min (rad) :  -0.26179939E+00   0.26179939E+00   0.52359878E+00
     Field map limits, radius :  min, max, max-min (cm) :   0.43000000E+03   0.55000000E+03   0.12000000E+03
      Min / max  fields  drawn  from  map  data :   1.78760E+07                 /  -2.13640E+07
       @  X-node, Y-node, z-node :               0.176      541.      1.70      /  0.101      547.      0.00    
     Normalisation  coeff.  BNORM   : -1.000000E-03
     Field  min/max  normalised  :               -17876.        /    21364.    
     Nbre  of  nodes  in  X/Y/Z :  301/ 121/  41
     Node  distance  in   X/Y/Z :   1.745329E-03/   1.00000    /  0.100000    

                     Option  for  interpolation :  3-D  grid    3*3*3  points,   interpolation  at  ordre  2

                    Integration step :   1.250     cm   (i.e.,   2.5510E-03 rad  at mean radius RM =    490.0    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.2730   444.150     0.000     0.000     0.000            0.262   444.152     0.000    -0.001    -0.000            1

 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      3  Keyword, label(s) :  DRIFT                                                 


                              Drift,  length =     0.00000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1 -7.269573E-01  4.441517E+02  1.712438E-02 -6.459928E-04 -4.369315E-03  2.4111986E+02  8.04289E-03

 Cumulative length of optical axis =    0.00000000     m   ;  Time  (for reference rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU    #END                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.2730   444.150     0.000     0.000     0.000      0.0000   -0.7270  444.152    0.017   -0.001   -0.004   2.411199E+02     1


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.441517E+00   1.712438E-05        1        0    0.000      (Y,T)         1
   0.0000E+00   0.0000E+00   1.0000E+00  -6.459928E-06  -4.369315E-06        1        0    0.000      (Z,P)         1
   0.0000E+00   0.0000E+00   1.0000E+00   8.042893E-03   1.505408E+02        1        0    0.000      (t,K)         1

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.420300E-31   0.000000E+00   1.025620E-37   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   1.025620E-37   0.000000E+00   7.406150E-44   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      5  Keyword, label(s) :  FIT                                                   

     FIT procedure launched.

           variable #            1       IR =            1 ,   ok.
           variable #            1       IP =           30 ,   ok.
           constraint #            1       IR =            4 ,   ok.
           constraint #            1       I  =            1 ,   ok.
           constraint #            2       IR =            4 ,   ok.
           constraint #            2       I  =            1 ,   ok.

                    FIT  variables  and  constraints  in  good  order,  FIT  will proceed. 

                    Final FIT status will NOT be saved. For so, use the 'save [FileName]' command

 STATUS OF VARIABLES  (Iteration #     0 /    500 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   1   1    30    222.        444.       444.15137       666.       0.00      OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-10)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     4    0.000000E+00    1.000E+00    1.960899E-04    9.88E-01 FAISCEAU   #END                 -                    0
  3   1   3     4    0.000000E+00    1.000E+00    2.120441E-05    1.16E-02 FAISCEAU   #END                 -                    0
 Fit reached penalty value   3.8901E-08

************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed.  Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 

                          MAGNETIC  RIGIDITY =       1839.090 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  TOSCA       #START                                    


     NDIM =   3 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 20.0

          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           ./fieldMap/k75v113my021f45500d2700.table
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : ./fieldMap/k75v113my021f45500d2700.table


     Field map limits, angle :  min, max, max-min (rad) :  -0.26179939E+00   0.26179939E+00   0.52359878E+00
     Field map limits, radius :  min, max, max-min (cm) :   0.43000000E+03   0.55000000E+03   0.12000000E+03
      Min / max  fields  drawn  from  map  data :   1.78760E+07                 /  -2.13640E+07
       @  X-node, Y-node, z-node :               0.176      541.      1.70      /  0.101      547.      0.00    
     Normalisation  coeff.  BNORM   : -1.000000E-03
     Field  min/max  normalised  :               -17876.        /    21364.    
     Nbre  of  nodes  in  X/Y/Z :  301/ 121/  41
     Node  distance  in   X/Y/Z :   1.745329E-03/   1.00000    /  0.100000    

                     Option  for  interpolation :  3-D  grid    3*3*3  points,   interpolation  at  ordre  2

                    Integration step :   1.250     cm   (i.e.,   2.5510E-03 rad  at mean radius RM =    490.0    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.2730   444.151     0.000     0.000     0.000            0.262   444.151     0.000    -0.001    -0.000            1

 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      3  Keyword, label(s) :  DRIFT                                                 


                              Drift,  length =     0.00000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1 -7.269573E-01  4.441512E+02  2.120441E-05 -6.455872E-04 -4.368019E-03  2.4112078E+02  8.04292E-03

 Cumulative length of optical axis =    0.00000000     m   ;  Time  (for reference rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU    #END                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.2730   444.151     0.000     0.000     0.000      0.0000   -0.7270  444.151    0.000   -0.001   -0.004   2.411208E+02     1


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.441512E+00   2.120441E-08        1        0    0.000      (Y,T)         1
   0.0000E+00   0.0000E+00   1.0000E+00  -6.455872E-06  -4.368019E-06        1        0    0.000      (Z,P)         1
   0.0000E+00   0.0000E+00   1.0000E+00   8.042923E-03   1.505408E+02        1        0    0.000      (t,K)         1

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.941458E-31   0.000000E+00  -5.467082E-40   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  -5.467082E-40   0.000000E+00   1.539512E-48   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************

      5   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      6  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      5)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.2730   444.151     0.000     0.000     0.000      0.0000   -0.7270  444.151    0.000   -0.001   -0.004   2.411208E+02     1


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.441512E+00   2.120441E-08        1        0    0.000      (Y,T)         1
   0.0000E+00   0.0000E+00   1.0000E+00  -6.455872E-06  -4.368019E-06        1        0    0.000      (Z,P)         1
   0.0000E+00   0.0000E+00   1.0000E+00   8.042923E-03   1.505408E+02        1        0    0.000      (t,K)         1

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.941458E-31   0.000000E+00  -5.467082E-40   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  -5.467082E-40   0.000000E+00   1.539512E-48   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      7  Keyword, label(s) :  FAISTORE                                              


                OPEN FILE orbits4Obj3.fai                                                                 
                FOR PRINTING COORDINATES 

               Print will occur at element[s] labeled : 


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #        1 through the optical structure 

                     Total of          1 particles have been launched

     Multiple pass, 
          from element #     1 : OBJET     /label1=                    /label2=                    
                             to  REBELOTE  /label1=                    /label2=                    
     ending at pass #       9 at element #     8 : REBELOTE  /label1=                    /label2=                    


 Pgm rebel. At pass #    1/   9.  In element #    1,  parameter # 35  changed to    2.73043000E-01   (was    2.73042677E-01)

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    500 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   1   1    30    222.        444.       444.15143       666.       0.00      OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-10)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     4    0.000000E+00    1.000E+00    1.960772E-04    9.88E-01 FAISCEAU   #END                 -                    0
  3   1   3     4    0.000000E+00    1.000E+00    2.122005E-05    1.16E-02 FAISCEAU   #END                 -                    0
 Fit reached penalty value   3.8897E-08

************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed.  Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 

                          MAGNETIC  RIGIDITY =       1839.090 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  TOSCA       #START                                    


     NDIM =   3 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 20.0

          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           ./fieldMap/k75v113my021f45500d2700.table
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : ./fieldMap/k75v113my021f45500d2700.table


     Field map limits, angle :  min, max, max-min (rad) :  -0.26179939E+00   0.26179939E+00   0.52359878E+00
     Field map limits, radius :  min, max, max-min (cm) :   0.43000000E+03   0.55000000E+03   0.12000000E+03
      Min / max  fields  drawn  from  map  data :   1.78760E+07                 /  -2.13640E+07
       @  X-node, Y-node, z-node :               0.176      541.      1.70      /  0.101      547.      0.00    
     Normalisation  coeff.  BNORM   : -1.000000E-03
     Field  min/max  normalised  :               -17876.        /    21364.    
     Nbre  of  nodes  in  X/Y/Z :  301/ 121/  41
     Node  distance  in   X/Y/Z :   1.745329E-03/   1.00000    /  0.100000    

                     Option  for  interpolation :  3-D  grid    3*3*3  points,   interpolation  at  ordre  2

                    Integration step :   1.250     cm   (i.e.,   2.5510E-03 rad  at mean radius RM =    490.0    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.2730   444.151     0.000     0.000     0.000            0.262   444.151     0.000    -0.001    -0.000            1

 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      3  Keyword, label(s) :  DRIFT                                                 


                              Drift,  length =     0.00000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1 -7.269570E-01  4.441512E+02  2.122005E-05 -6.455867E-04 -4.368015E-03  2.4112081E+02  8.04292E-03

 Cumulative length of optical axis =    0.00000000     m   ;  Time  (for reference rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU    #END                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.2730   444.151     0.000     0.000     0.000      0.0000   -0.7270  444.151    0.000   -0.001   -0.004   2.411208E+02     1


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.441512E+00   2.122005E-08        1        0    0.000      (Y,T)         2
   0.0000E+00   0.0000E+00   1.0000E+00  -6.455867E-06  -4.368015E-06        1        0    0.000      (Z,P)         2
   0.0000E+00   0.0000E+00   1.0000E+00   8.042925E-03   1.505410E+02        1        0    0.000      (t,K)         2

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.437505E-32   0.000000E+00  -1.078350E-38   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  -1.078350E-38   0.000000E+00   4.770607E-45   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************

      5   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      6  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      5)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.2730   444.151     0.000     0.000     0.000      0.0000   -0.7270  444.151    0.000   -0.001   -0.004   2.411208E+02     1


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.441512E+00   2.122005E-08        1        0    0.000      (Y,T)         2
   0.0000E+00   0.0000E+00   1.0000E+00  -6.455867E-06  -4.368015E-06        1        0    0.000      (Z,P)         2
   0.0000E+00   0.0000E+00   1.0000E+00   8.042925E-03   1.505410E+02        1        0    0.000      (t,K)         2

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.437505E-32   0.000000E+00  -1.078350E-38   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  -1.078350E-38   0.000000E+00   4.770607E-45   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      7  Keyword, label(s) :  FAISTORE                                              


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #        2 through the optical structure 

                     Total of          2 particles have been launched

 Pgm rebel. At pass #    2/   9.  In element #    1,  parameter # 35  changed to    3.76894000E-01   (was    2.73043000E-01)

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    500 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   1   1    30    222.        462.       461.74249       666.       0.00      OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-10)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     4    0.000000E+00    1.000E+00    5.938704E-04    9.87E-01 FAISCEAU   #END                 -                    0
  3   1   3     4    0.000000E+00    1.000E+00    6.757217E-05    1.28E-02 FAISCEAU   #END                 -                    0
 Fit reached penalty value   3.5725E-07

************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed.  Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 

                          MAGNETIC  RIGIDITY =       1839.090 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  TOSCA       #START                                    


     NDIM =   3 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 20.0

          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           ./fieldMap/k75v113my021f45500d2700.table
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : ./fieldMap/k75v113my021f45500d2700.table


     Field map limits, angle :  min, max, max-min (rad) :  -0.26179939E+00   0.26179939E+00   0.52359878E+00
     Field map limits, radius :  min, max, max-min (cm) :   0.43000000E+03   0.55000000E+03   0.12000000E+03
      Min / max  fields  drawn  from  map  data :   1.78760E+07                 /  -2.13640E+07
       @  X-node, Y-node, z-node :               0.176      541.      1.70      /  0.101      547.      0.00    
     Normalisation  coeff.  BNORM   : -1.000000E-03
     Field  min/max  normalised  :               -17876.        /    21364.    
     Nbre  of  nodes  in  X/Y/Z :  301/ 121/  41
     Node  distance  in   X/Y/Z :   1.745329E-03/   1.00000    /  0.100000    

                     Option  for  interpolation :  3-D  grid    3*3*3  points,   interpolation  at  ordre  2

                    Integration step :   1.250     cm   (i.e.,   2.5510E-03 rad  at mean radius RM =    490.0    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3769   461.742     0.000     0.000     0.000            0.262   461.742     0.000    -0.001    -0.000            1

 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      3  Keyword, label(s) :  DRIFT                                                 


                              Drift,  length =     0.00000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1 -6.231060E-01  4.617419E+02  6.757217E-05 -5.096003E-04 -3.367923E-03  2.5076424E+02  8.36459E-03

 Cumulative length of optical axis =    0.00000000     m   ;  Time  (for reference rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU    #END                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3769   461.742     0.000     0.000     0.000      0.0000   -0.6231  461.742    0.000   -0.001   -0.003   2.507642E+02     1


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.617419E+00   6.757217E-08        1        0    0.000      (Y,T)         3
   0.0000E+00   0.0000E+00   1.0000E+00  -5.096003E-06  -3.367923E-06        1        0    0.000      (Z,P)         3
   0.0000E+00   0.0000E+00   1.0000E+00   8.364595E-03   2.077988E+02        1        0    0.000      (t,K)         3

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.113257E-33   0.000000E+00   1.768192E-39   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   1.768192E-39   0.000000E+00   1.479471E-45   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************

      5   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      6  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      5)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3769   461.742     0.000     0.000     0.000      0.0000   -0.6231  461.742    0.000   -0.001   -0.003   2.507642E+02     1


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.617419E+00   6.757217E-08        1        0    0.000      (Y,T)         3
   0.0000E+00   0.0000E+00   1.0000E+00  -5.096003E-06  -3.367923E-06        1        0    0.000      (Z,P)         3
   0.0000E+00   0.0000E+00   1.0000E+00   8.364595E-03   2.077988E+02        1        0    0.000      (t,K)         3

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.113257E-33   0.000000E+00   1.768192E-39   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   1.768192E-39   0.000000E+00   1.479471E-45   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      7  Keyword, label(s) :  FAISTORE                                              


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #        3 through the optical structure 

                     Total of          3 particles have been launched

 Pgm rebel. At pass #    3/   9.  In element #    1,  parameter # 35  changed to    4.80745000E-01   (was    3.76894000E-01)

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    500 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   1   1    30    222.        475.       475.35736       666.       0.00      OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-10)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     4    0.000000E+00    1.000E+00    2.953666E-03    9.86E-01 FAISCEAU   #END                 -                    0
  3   1   3     4    0.000000E+00    1.000E+00    3.472734E-04    1.36E-02 FAISCEAU   #END                 -                    0
 Fit reached penalty value   8.8447E-06

************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed.  Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 

                          MAGNETIC  RIGIDITY =       1839.090 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  TOSCA       #START                                    


     NDIM =   3 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 20.0

          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           ./fieldMap/k75v113my021f45500d2700.table
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : ./fieldMap/k75v113my021f45500d2700.table


     Field map limits, angle :  min, max, max-min (rad) :  -0.26179939E+00   0.26179939E+00   0.52359878E+00
     Field map limits, radius :  min, max, max-min (cm) :   0.43000000E+03   0.55000000E+03   0.12000000E+03
      Min / max  fields  drawn  from  map  data :   1.78760E+07                 /  -2.13640E+07
       @  X-node, Y-node, z-node :               0.176      541.      1.70      /  0.101      547.      0.00    
     Normalisation  coeff.  BNORM   : -1.000000E-03
     Field  min/max  normalised  :               -17876.        /    21364.    
     Nbre  of  nodes  in  X/Y/Z :  301/ 121/  41
     Node  distance  in   X/Y/Z :   1.745329E-03/   1.00000    /  0.100000    

                     Option  for  interpolation :  3-D  grid    3*3*3  points,   interpolation  at  ordre  2

                    Integration step :   1.250     cm   (i.e.,   2.5510E-03 rad  at mean radius RM =    490.0    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.4807   475.357     0.000     0.000     0.000            0.262   475.354     0.000    -0.000    -0.000            1

 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      3  Keyword, label(s) :  DRIFT                                                 


                              Drift,  length =     0.00000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1 -5.192550E-01  4.753544E+02  3.472734E-04 -4.212439E-04 -2.704600E-03  2.5815561E+02  8.61114E-03

 Cumulative length of optical axis =    0.00000000     m   ;  Time  (for reference rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU    #END                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.4807   475.357     0.000     0.000     0.000      0.0000   -0.5193  475.354    0.000   -0.000   -0.003   2.581556E+02     1


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.753544E+00   3.472734E-07        1        0    0.000      (Y,T)         4
   0.0000E+00   0.0000E+00   1.0000E+00  -4.212439E-06  -2.704600E-06        1        0    0.000      (Z,P)         4
   0.0000E+00   0.0000E+00   1.0000E+00   8.611144E-03   2.650565E+02        1        0    0.000      (t,K)         4

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.469755E-31   0.000000E+00   1.222495E-37   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   1.222495E-37   0.000000E+00   1.016833E-43   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************

      5   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      6  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      5)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.4807   475.357     0.000     0.000     0.000      0.0000   -0.5193  475.354    0.000   -0.000   -0.003   2.581556E+02     1


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.753544E+00   3.472734E-07        1        0    0.000      (Y,T)         4
   0.0000E+00   0.0000E+00   1.0000E+00  -4.212439E-06  -2.704600E-06        1        0    0.000      (Z,P)         4
   0.0000E+00   0.0000E+00   1.0000E+00   8.611144E-03   2.650565E+02        1        0    0.000      (t,K)         4

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.469755E-31   0.000000E+00   1.222495E-37   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   1.222495E-37   0.000000E+00   1.016833E-43   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      7  Keyword, label(s) :  FAISTORE                                              


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #        4 through the optical structure 

                     Total of          4 particles have been launched

 Pgm rebel. At pass #    4/   9.  In element #    1,  parameter # 35  changed to    5.84596000E-01   (was    4.80745000E-01)

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    500 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   1   1    30    222.        487.       486.53726       666.       0.00      OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-10)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     4    0.000000E+00    1.000E+00    3.232131E-03    9.86E-01 FAISCEAU   #END                 -                    0
  3   1   3     4    0.000000E+00    1.000E+00   -3.914582E-04    1.45E-02 FAISCEAU   #END                 -                    0
 Fit reached penalty value   1.0600E-05

************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed.  Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 

                          MAGNETIC  RIGIDITY =       1839.090 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  TOSCA       #START                                    


     NDIM =   3 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 20.0

          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           ./fieldMap/k75v113my021f45500d2700.table
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : ./fieldMap/k75v113my021f45500d2700.table


     Field map limits, angle :  min, max, max-min (rad) :  -0.26179939E+00   0.26179939E+00   0.52359878E+00
     Field map limits, radius :  min, max, max-min (cm) :   0.43000000E+03   0.55000000E+03   0.12000000E+03
      Min / max  fields  drawn  from  map  data :   1.78760E+07                 /  -2.13640E+07
       @  X-node, Y-node, z-node :               0.176      541.      1.70      /  0.101      547.      0.00    
     Normalisation  coeff.  BNORM   : -1.000000E-03
     Field  min/max  normalised  :               -17876.        /    21364.    
     Nbre  of  nodes  in  X/Y/Z :  301/ 121/  41
     Node  distance  in   X/Y/Z :   1.745329E-03/   1.00000    /  0.100000    

                     Option  for  interpolation :  3-D  grid    3*3*3  points,   interpolation  at  ordre  2

                    Integration step :   1.250     cm   (i.e.,   2.5510E-03 rad  at mean radius RM =    490.0    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.5846   486.537     0.000     0.000     0.000            0.262   486.540    -0.000    -0.000    -0.000            1

 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      3  Keyword, label(s) :  DRIFT                                                 


                              Drift,  length =     0.00000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1 -4.154040E-01  4.865405E+02 -3.914582E-04 -3.434145E-04 -2.176062E-03  2.6421289E+02  8.81319E-03

 Cumulative length of optical axis =    0.00000000     m   ;  Time  (for reference rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU    #END                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.5846   486.537     0.000     0.000     0.000      0.0000   -0.4154  486.540   -0.000   -0.000   -0.002   2.642129E+02     1


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.865405E+00  -3.914582E-07        1        0    0.000      (Y,T)         5
   0.0000E+00   0.0000E+00   1.0000E+00  -3.434145E-06  -2.176062E-06        1        0    0.000      (Z,P)         5
   0.0000E+00   0.0000E+00   1.0000E+00   8.813193E-03   3.223143E+02        1        0    0.000      (t,K)         5

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.251930E-32   0.000000E+00  -7.172854E-39   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  -7.172854E-39   0.000000E+00   4.109643E-45   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************

      5   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      6  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      5)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.5846   486.537     0.000     0.000     0.000      0.0000   -0.4154  486.540   -0.000   -0.000   -0.002   2.642129E+02     1


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.865405E+00  -3.914582E-07        1        0    0.000      (Y,T)         5
   0.0000E+00   0.0000E+00   1.0000E+00  -3.434145E-06  -2.176062E-06        1        0    0.000      (Z,P)         5
   0.0000E+00   0.0000E+00   1.0000E+00   8.813193E-03   3.223143E+02        1        0    0.000      (t,K)         5

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.251930E-32   0.000000E+00  -7.172854E-39   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  -7.172854E-39   0.000000E+00   4.109643E-45   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      7  Keyword, label(s) :  FAISTORE                                              


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #        5 through the optical structure 

                     Total of          5 particles have been launched

 Pgm rebel. At pass #    5/   9.  In element #    1,  parameter # 35  changed to    6.88447000E-01   (was    5.84596000E-01)

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    500 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   1   1    30    222.        496.       496.01273       666.       0.00      OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-10)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     4    0.000000E+00    1.000E+00    4.493448E-03    9.85E-01 FAISCEAU   #END                 -                    0
  3   1   3     4    0.000000E+00    1.000E+00    5.546614E-04    1.50E-02 FAISCEAU   #END                 -                    0
 Fit reached penalty value   2.0499E-05

************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed.  Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 

                          MAGNETIC  RIGIDITY =       1839.090 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  TOSCA       #START                                    


     NDIM =   3 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 20.0

          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           ./fieldMap/k75v113my021f45500d2700.table
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : ./fieldMap/k75v113my021f45500d2700.table


     Field map limits, angle :  min, max, max-min (rad) :  -0.26179939E+00   0.26179939E+00   0.52359878E+00
     Field map limits, radius :  min, max, max-min (cm) :   0.43000000E+03   0.55000000E+03   0.12000000E+03
      Min / max  fields  drawn  from  map  data :   1.78760E+07                 /  -2.13640E+07
       @  X-node, Y-node, z-node :               0.176      541.      1.70      /  0.101      547.      0.00    
     Normalisation  coeff.  BNORM   : -1.000000E-03
     Field  min/max  normalised  :               -17876.        /    21364.    
     Nbre  of  nodes  in  X/Y/Z :  301/ 121/  41
     Node  distance  in   X/Y/Z :   1.745329E-03/   1.00000    /  0.100000    

                     Option  for  interpolation :  3-D  grid    3*3*3  points,   interpolation  at  ordre  2

                    Integration step :   1.250     cm   (i.e.,   2.5510E-03 rad  at mean radius RM =    490.0    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.6884   496.013     0.000     0.000     0.000            0.262   496.008     0.000    -0.000    -0.000            1

 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      3  Keyword, label(s) :  DRIFT                                                 


                              Drift,  length =     0.00000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1 -3.115530E-01  4.960082E+02  5.546614E-04 -2.826115E-04 -1.765822E-03  2.6931412E+02  8.98335E-03

 Cumulative length of optical axis =    0.00000000     m   ;  Time  (for reference rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU    #END                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.6884   496.013     0.000     0.000     0.000      0.0000   -0.3116  496.008    0.001   -0.000   -0.002   2.693141E+02     1


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.960082E+00   5.546614E-07        1        0    0.000      (Y,T)         6
   0.0000E+00   0.0000E+00   1.0000E+00  -2.826115E-06  -1.765822E-06        1        0    0.000      (Z,P)         6
   0.0000E+00   0.0000E+00   1.0000E+00   8.983352E-03   3.795720E+02        1        0    0.000      (t,K)         6

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   7.897516E-32   0.000000E+00  -4.033157E-38   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  -4.033157E-38   0.000000E+00   2.059680E-44   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************

      5   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      6  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      5)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.6884   496.013     0.000     0.000     0.000      0.0000   -0.3116  496.008    0.001   -0.000   -0.002   2.693141E+02     1


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.960082E+00   5.546614E-07        1        0    0.000      (Y,T)         6
   0.0000E+00   0.0000E+00   1.0000E+00  -2.826115E-06  -1.765822E-06        1        0    0.000      (Z,P)         6
   0.0000E+00   0.0000E+00   1.0000E+00   8.983352E-03   3.795720E+02        1        0    0.000      (t,K)         6

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   7.897516E-32   0.000000E+00  -4.033157E-38   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  -4.033157E-38   0.000000E+00   2.059680E-44   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      7  Keyword, label(s) :  FAISTORE                                              


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #        6 through the optical structure 

                     Total of          6 particles have been launched

 Pgm rebel. At pass #    6/   9.  In element #    1,  parameter # 35  changed to    7.92298000E-01   (was    6.88447000E-01)

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    500 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   1   1    30    222.        504.       504.23367       666.       0.00      OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-10)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     4    0.000000E+00    1.000E+00    9.675196E-04    9.84E-01 FAISCEAU   #END                 -                    0
  3   1   3     4    0.000000E+00    1.000E+00   -1.230831E-04    1.59E-02 FAISCEAU   #END                 -                    0
 Fit reached penalty value   9.5124E-07

************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed.  Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 

                          MAGNETIC  RIGIDITY =       1839.090 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  TOSCA       #START                                    


     NDIM =   3 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 20.0

          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           ./fieldMap/k75v113my021f45500d2700.table
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : ./fieldMap/k75v113my021f45500d2700.table


     Field map limits, angle :  min, max, max-min (rad) :  -0.26179939E+00   0.26179939E+00   0.52359878E+00
     Field map limits, radius :  min, max, max-min (cm) :   0.43000000E+03   0.55000000E+03   0.12000000E+03
      Min / max  fields  drawn  from  map  data :   1.78760E+07                 /  -2.13640E+07
       @  X-node, Y-node, z-node :               0.176      541.      1.70      /  0.101      547.      0.00    
     Normalisation  coeff.  BNORM   : -1.000000E-03
     Field  min/max  normalised  :               -17876.        /    21364.    
     Nbre  of  nodes  in  X/Y/Z :  301/ 121/  41
     Node  distance  in   X/Y/Z :   1.745329E-03/   1.00000    /  0.100000    

                     Option  for  interpolation :  3-D  grid    3*3*3  points,   interpolation  at  ordre  2

                    Integration step :   1.250     cm   (i.e.,   2.5510E-03 rad  at mean radius RM =    490.0    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.7923   504.234     0.000     0.000     0.000            0.262   504.235    -0.000    -0.000    -0.000            1

 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      3  Keyword, label(s) :  DRIFT                                                 


                              Drift,  length =     0.00000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1 -2.077020E-01  5.042346E+02 -1.230831E-04 -2.222840E-04 -1.380363E-03  2.7370484E+02  9.12981E-03

 Cumulative length of optical axis =    0.00000000     m   ;  Time  (for reference rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU    #END                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.7923   504.234     0.000     0.000     0.000      0.0000   -0.2077  504.235   -0.000   -0.000   -0.001   2.737048E+02     1


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   5.042346E+00  -1.230831E-07        1        0    0.000      (Y,T)         7
   0.0000E+00   0.0000E+00   1.0000E+00  -2.222840E-06  -1.380363E-06        1        0    0.000      (Z,P)         7
   0.0000E+00   0.0000E+00   1.0000E+00   9.129811E-03   4.368298E+02        1        0    0.000      (t,K)         7

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.060613E-32   0.000000E+00   1.395200E-38   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   1.395200E-38   0.000000E+00   9.446620E-45   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************

      5   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      6  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      5)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.7923   504.234     0.000     0.000     0.000      0.0000   -0.2077  504.235   -0.000   -0.000   -0.001   2.737048E+02     1


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   5.042346E+00  -1.230831E-07        1        0    0.000      (Y,T)         7
   0.0000E+00   0.0000E+00   1.0000E+00  -2.222840E-06  -1.380363E-06        1        0    0.000      (Z,P)         7
   0.0000E+00   0.0000E+00   1.0000E+00   9.129811E-03   4.368298E+02        1        0    0.000      (t,K)         7

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.060613E-32   0.000000E+00   1.395200E-38   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   1.395200E-38   0.000000E+00   9.446620E-45   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      7  Keyword, label(s) :  FAISTORE                                              


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #        7 through the optical structure 

                     Total of          7 particles have been launched

 Pgm rebel. At pass #    7/   9.  In element #    1,  parameter # 35  changed to    8.96149000E-01   (was    7.92298000E-01)

     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

 STATUS OF VARIABLES  (Iteration #     0 /    500 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   1   1    30    222.        512.       511.54690       666.       0.00      OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-10)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     4    0.000000E+00    1.000E+00    4.669790E-03    9.84E-01 FAISCEAU   #END                 -                    0
  3   1   3     4    0.000000E+00    1.000E+00   -6.033069E-04    1.64E-02 FAISCEAU   #END                 -                    0
 Fit reached penalty value   2.2171E-05

************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed.  Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 

                          MAGNETIC  RIGIDITY =       1839.090 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  TOSCA       #START                                    


     NDIM =   3 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 20.0

          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           ./fieldMap/k75v113my021f45500d2700.table
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : ./fieldMap/k75v113my021f45500d2700.table


     Field map limits, angle :  min, max, max-min (rad) :  -0.26179939E+00   0.26179939E+00   0.52359878E+00
     Field map limits, radius :  min, max, max-min (cm) :   0.43000000E+03   0.55000000E+03   0.12000000E+03
      Min / max  fields  drawn  from  map  data :   1.78760E+07                 /  -2.13640E+07
       @  X-node, Y-node, z-node :               0.176      541.      1.70      /  0.101      547.      0.00    
     Normalisation  coeff.  BNORM   : -1.000000E-03
     Field  min/max  normalised  :               -17876.        /    21364.    
     Nbre  of  nodes  in  X/Y/Z :  301/ 121/  41
     Node  distance  in   X/Y/Z :   1.745329E-03/   1.00000    /  0.100000    

                     Option  for  interpolation :  3-D  grid    3*3*3  points,   interpolation  at  ordre  2

                    Integration step :   1.250     cm   (i.e.,   2.5510E-03 rad  at mean radius RM =    490.0    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.8961   511.547     0.000     0.000     0.000            0.262   511.552    -0.000    -0.000    -0.000            1

 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      3  Keyword, label(s) :  DRIFT                                                 


                              Drift,  length =     0.00000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1 -1.038510E-01  5.115516E+02 -6.033069E-04 -1.738171E-04 -1.080216E-03  2.7758706E+02  9.25931E-03

 Cumulative length of optical axis =    0.00000000     m   ;  Time  (for reference rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU    #END                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.8961   511.547     0.000     0.000     0.000      0.0000   -0.1039  511.552   -0.001   -0.000   -0.001   2.775871E+02     1


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   5.115516E+00  -6.033069E-07        1        0    0.000      (Y,T)         8
   0.0000E+00   0.0000E+00   1.0000E+00  -1.738171E-06  -1.080216E-06        1        0    0.000      (Z,P)         8
   0.0000E+00   0.0000E+00   1.0000E+00   9.259308E-03   4.940876E+02        1        0    0.000      (t,K)         8

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.138179E-32   0.000000E+00  -4.765401E-39   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  -4.765401E-39   0.000000E+00   1.995208E-45   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************

      5   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      6  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      5)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.8961   511.547     0.000     0.000     0.000      0.0000   -0.1039  511.552   -0.001   -0.000   -0.001   2.775871E+02     1


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   5.115516E+00  -6.033069E-07        1        0    0.000      (Y,T)         8
   0.0000E+00   0.0000E+00   1.0000E+00  -1.738171E-06  -1.080216E-06        1        0    0.000      (Z,P)         8
   0.0000E+00   0.0000E+00   1.0000E+00   9.259308E-03   4.940876E+02        1        0    0.000      (t,K)         8

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.138179E-32   0.000000E+00  -4.765401E-39   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  -4.765401E-39   0.000000E+00   1.995208E-45   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      7  Keyword, label(s) :  FAISTORE                                              


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #        8 through the optical structure 

                     Total of          8 particles have been launched


      Next  pass  is  #     9 and  last  pass  through  the  optical  structure


 Pgm rebel. At pass #    8/   9.  In element #    1,  parameter # 35  changed to    1.00000000E+00   (was    8.96149000E-01)

     WRITE statements to zgoubi.res are re-established from now on.

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 

                          MAGNETIC  RIGIDITY =       1839.090 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  TOSCA       #START                                    


     NDIM =   3 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 20.0

          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           ./fieldMap/k75v113my021f45500d2700.table
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : ./fieldMap/k75v113my021f45500d2700.table


     Field map limits, angle :  min, max, max-min (rad) :  -0.26179939E+00   0.26179939E+00   0.52359878E+00
     Field map limits, radius :  min, max, max-min (cm) :   0.43000000E+03   0.55000000E+03   0.12000000E+03
      Min / max  fields  drawn  from  map  data :   1.78760E+07                 /  -2.13640E+07
       @  X-node, Y-node, z-node :               0.176      541.      1.70      /  0.101      547.      0.00    
     Normalisation  coeff.  BNORM   : -1.000000E-03
     Field  min/max  normalised  :               -17876.        /    21364.    
     Nbre  of  nodes  in  X/Y/Z :  301/ 121/  41
     Node  distance  in   X/Y/Z :   1.745329E-03/   1.00000    /  0.100000    

                     Option  for  interpolation :  3-D  grid    3*3*3  points,   interpolation  at  ordre  2

                    Integration step :   1.250     cm   (i.e.,   2.5510E-03 rad  at mean radius RM =    490.0    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000   511.547     0.000     0.000     0.000            0.262   520.023     0.065    -0.000    -0.000            1

 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      3  Keyword, label(s) :  DRIFT                                                 


                              Drift,  length =     0.00000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  5.200228E+02  6.546977E+01 -1.115355E-04 -6.483728E-04  2.7715868E+02  9.24502E-03

 Cumulative length of optical axis =    0.00000000     m   ;  Time  (for reference rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU    #END                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000   511.547     0.000     0.000     0.000      0.0000    0.0000  520.023   65.470   -0.000   -0.001   2.771587E+02     1


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   5.200228E+00   6.546977E-02        1        0    0.000      (Y,T)         9
   0.0000E+00   0.0000E+00   1.0000E+00  -1.115355E-06  -6.483728E-07        1        0    0.000      (Z,P)         9
   0.0000E+00   0.0000E+00   1.0000E+00   9.245019E-03   5.513453E+02        1        0    0.000      (t,K)         9

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.032707E-31   0.000000E+00   3.336050E-38   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   3.336050E-38   0.000000E+00   1.077676E-44   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      5  Keyword, label(s) :  FIT                                                   


     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 

                          MAGNETIC  RIGIDITY =       1839.090 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  TOSCA       #START                                    


     NDIM =   3 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 20.0

          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           ./fieldMap/k75v113my021f45500d2700.table
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : ./fieldMap/k75v113my021f45500d2700.table


     Field map limits, angle :  min, max, max-min (rad) :  -0.26179939E+00   0.26179939E+00   0.52359878E+00
     Field map limits, radius :  min, max, max-min (cm) :   0.43000000E+03   0.55000000E+03   0.12000000E+03
      Min / max  fields  drawn  from  map  data :   1.78760E+07                 /  -2.13640E+07
       @  X-node, Y-node, z-node :               0.176      541.      1.70      /  0.101      547.      0.00    
     Normalisation  coeff.  BNORM   : -1.000000E-03
     Field  min/max  normalised  :               -17876.        /    21364.    
     Nbre  of  nodes  in  X/Y/Z :  301/ 121/  41
     Node  distance  in   X/Y/Z :   1.745329E-03/   1.00000    /  0.100000    

                     Option  for  interpolation :  3-D  grid    3*3*3  points,   interpolation  at  ordre  2

                    Integration step :   1.250     cm   (i.e.,   2.5510E-03 rad  at mean radius RM =    490.0    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000   511.547     0.000     0.000     0.000            0.262   520.023     0.065    -0.000    -0.000            1

 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      3  Keyword, label(s) :  DRIFT                                                 


                              Drift,  length =     0.00000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  5.200228E+02  6.546977E+01 -1.115355E-04 -6.483728E-04  2.7715868E+02  9.24502E-03

 Cumulative length of optical axis =    0.00000000     m   ;  Time  (for reference rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU    #END                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000   511.547     0.000     0.000     0.000      0.0000    0.0000  520.023   65.470   -0.000   -0.001   2.771587E+02     1


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   5.200228E+00   6.546977E-02        1        0    0.000      (Y,T)         9
   0.0000E+00   0.0000E+00   1.0000E+00  -1.115355E-06  -6.483728E-07        1        0    0.000      (Z,P)         9
   0.0000E+00   0.0000E+00   1.0000E+00   9.245019E-03   5.513453E+02        1        0    0.000      (t,K)         9

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.032707E-31   0.000000E+00   3.336050E-38   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   3.336050E-38   0.000000E+00   1.077676E-44   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      5  Keyword, label(s) :  FIT                                                   

     FIT procedure launched.

           variable #            1       IR =            1 ,   ok.
           variable #            1       IP =           30 ,   ok.
           constraint #            1       IR =            4 ,   ok.
           constraint #            1       I  =            1 ,   ok.
           constraint #            2       IR =            4 ,   ok.
           constraint #            2       I  =            1 ,   ok.

                    FIT  variables  and  constraints  in  good  order,  FIT  will proceed. 

                    Final FIT status will NOT be saved. For so, use the 'save [FileName]' command

 STATUS OF VARIABLES  (Iteration #     0 /    500 max.)
LMNT VAR PARAM  MINIMUM    INITIAL         FINAL         MAXIMUM     STEP        NAME   LBL1                 LBL2
   1   1    30    222.        518.       518.41305       666.       0.00      OBJET      -                    -                   
 STATUS OF CONSTRAINTS (Target penalty =   1.0000E-10)
TYPE  I   J LMNT#     DESIRED          WEIGHT         REACHED         KI2     NAME   LBL1                 LBL2      Nb param. [value]
  3   1   2     4    0.000000E+00    1.000E+00    2.695026E-01    1.79E-02 FAISCEAU   #END                 -                    0
  3   1   3     4    0.000000E+00    1.000E+00    1.996905E+00    9.82E-01 FAISCEAU   #END                 -                    0
 Fit reached penalty value   4.0603E+00

************************************************************************************************************************************

           MAIN PROGRAM :  FIT completed.  Now doing a last run using variable values from FIT. 

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 

                          MAGNETIC  RIGIDITY =       1839.090 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  TOSCA       #START                                    


     NDIM =   3 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 20.0

          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           ./fieldMap/k75v113my021f45500d2700.table
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : ./fieldMap/k75v113my021f45500d2700.table


     Field map limits, angle :  min, max, max-min (rad) :  -0.26179939E+00   0.26179939E+00   0.52359878E+00
     Field map limits, radius :  min, max, max-min (cm) :   0.43000000E+03   0.55000000E+03   0.12000000E+03
      Min / max  fields  drawn  from  map  data :   1.78760E+07                 /  -2.13640E+07
       @  X-node, Y-node, z-node :               0.176      541.      1.70      /  0.101      547.      0.00    
     Normalisation  coeff.  BNORM   : -1.000000E-03
     Field  min/max  normalised  :               -17876.        /    21364.    
     Nbre  of  nodes  in  X/Y/Z :  301/ 121/  41
     Node  distance  in   X/Y/Z :   1.745329E-03/   1.00000    /  0.100000    

                     Option  for  interpolation :  3-D  grid    3*3*3  points,   interpolation  at  ordre  2

                    Integration step :   1.250     cm   (i.e.,   2.5510E-03 rad  at mean radius RM =    490.0    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000   518.413     0.000     0.000     0.000            0.262   518.683     0.002    -0.001    -0.000            1

 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      3  Keyword, label(s) :  DRIFT                                                 


                              Drift,  length =     0.00000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  5.186826E+02  1.996905E+00 -5.247424E-04 -3.205669E-03  2.8121989E+02  9.38049E-03

 Cumulative length of optical axis =    0.00000000     m   ;  Time  (for reference rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU    #END                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000   518.413     0.000     0.000     0.000      0.0000    0.0000  518.683    1.997   -0.001   -0.003   2.812199E+02     1


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   5.186826E+00   1.996905E-03        1        0    0.000      (Y,T)         9
   0.0000E+00   0.0000E+00   1.0000E+00  -5.247424E-06  -3.205669E-06        1        0    0.000      (Z,P)         9
   0.0000E+00   0.0000E+00   1.0000E+00   9.380486E-03   5.513453E+02        1        0    0.000      (t,K)         9

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   6.110690E-32   0.000000E+00   2.699103E-38   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   2.699103E-38   0.000000E+00   1.192198E-44   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************

      5   Keyword FIT[2] is skipped since this is the (end of) last run following the fitting procedure.

          Now carrying on beyond FIT keyword.


************************************************************************************************************************************
      6  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      5)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000   518.413     0.000     0.000     0.000      0.0000    0.0000  518.683    1.997   -0.001   -0.003   2.812199E+02     1


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   5.186826E+00   1.996905E-03        1        0    0.000      (Y,T)         9
   0.0000E+00   0.0000E+00   1.0000E+00  -5.247424E-06  -3.205669E-06        1        0    0.000      (Z,P)         9
   0.0000E+00   0.0000E+00   1.0000E+00   9.380486E-03   5.513453E+02        1        0    0.000      (t,K)         9

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   6.110690E-32   0.000000E+00   2.699103E-38   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   2.699103E-38   0.000000E+00   1.192198E-44   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      7  Keyword, label(s) :  FAISTORE                                              


************************************************************************************************************************************
      8  Keyword, label(s) :  REBELOTE                                              


                         ****  End  of  'REBELOTE'  procedure  ****

      There  has  been          9  passes  through  the  optical  structure 

                     Total of          9 particles have been launched

************************************************************************************************************************************
      9  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      8)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000   518.413     0.000     0.000     0.000      0.0000    0.0000  518.683    1.997   -0.001   -0.003   2.812199E+02     1


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   5.186826E+00   1.996905E-03        1        0    0.000      (Y,T)        10
   0.0000E+00   0.0000E+00   1.0000E+00  -5.247424E-06  -3.205669E-06        1        0    0.000      (Z,P)        10
   0.0000E+00   0.0000E+00   1.0000E+00   9.380486E-03   5.513453E+02        1        0    0.000      (t,K)        10

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   6.110690E-32   0.000000E+00   2.699103E-38   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   2.699103E-38   0.000000E+00   1.192198E-44   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     10  Keyword, label(s) :  END                                                   


************************************************************************************************************************************
 Pgm zgoubi : Execution ended normally, upon keyword END or FIN
   
            ZGOUBI RUN COMPLETED. 

  Zgoubi, author's dvlpmnt version.
  Job  started  on  28-09-2018,  at  09:55:31 
  JOB  ENDED  ON    28-09-2018,  AT  09:55:45 

   CPU time, total :     13.200824000000001     

     An updated version of the input data file, with variables in FIT'ed state, has been saved in zgoubi.FIT.out.dat.


************************************************************************************************************************************
