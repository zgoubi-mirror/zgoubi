
set title "Tune dependence on energy \n KEK 150 MeV scaling FFAG " font "roman,26"
 
set xlabel "kin. E [MeV] \n " font "roman,24"
set x2label "\n R [m] \n" font "roman,24"
set ylabel "{/Symbol n}_x \n " font "roman,24"
set y2label "\n {/Symbol n}_y \n" font "roman,24"

 set xtics nomirror font "roman,18"
 set xtics font "roman,18"
 set ytics nomirror  font "roman,18"
 set y2tics nomirror font "roman,18"


am = 938.27203e6
c = 2.99792458e8
BORO = 1839.090113     # kG.cm
BrhoRef = BORO *1e-3   # T.m
eV2MeV = 1e-6


plot "zgoubi.MATRIX.out" u ((sqrt(($47*BrhoRef*c)**2 + am*am)-am)*eV2MeV):($56) w l lt 1 lw 1 lc rgb "red" tit '{/Symbol n}_x', \
     "zgoubi.MATRIX.out" u ((sqrt(($47*BrhoRef*c)**2 + am*am)-am)*eV2MeV):($57) axes x1y2 w l lt 3 lw 1 lc rgb "blue" tit '{/Symbol n}_y'

 set terminal postscript eps blacktext color enh "Times-Roman" 12
 set output "gnuplot_MATRIX_Qxy.eps"
 replot
 set terminal X11
 unset output

pause 1

exit

