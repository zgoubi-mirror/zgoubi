Uniform field sector
 'OBJET'                                                                                                      1
64.6244440                       ! 200keV proton
2
8  1
12.9248888 0. 0. 0. 0.    1.0000000 o  ! p [MeV/c] =   19.373921, Brho [kG.cm] =   64.624444
22.3889497 0. 0. 0. 0.    1.7322354 o  ! p [MeV/c] =   33.560191, Brho [kG.cm] =  111.944748
28.9070891 0. 0. 0. 0.    2.2365445 o  ! p [MeV/c] =   43.330637, Brho [kG.cm] =  144.535446
34.2069723 0. 0. 0. 0.    2.6465971 o  ! p [MeV/c] =   51.274962, Brho [kG.cm] =  171.034862
38.7911914 0. 0. 0. 0.    3.0012786 o  ! p [MeV/c] =   58.146533, Brho [kG.cm] =  193.955957
42.8898417 0. 0. 0. 0.    3.3183916 o  ! p [MeV/c] =   64.290255, Brho [kG.cm] =  214.449209
46.6311370 0. 0. 0. 0.    3.6078560 o  ! p [MeV/c] =   69.898316, Brho [kG.cm] =  233.155685
50.0952070 0. 0. 0. 0.    3.8758715 o  ! p [MeV/c] =   75.090826, Brho [kG.cm] =  250.476035
1 1 1 1 1 1 1 1
 
 'PARTICUL'              ! This is required only because we want FAISCEAU to compute time-of-flight,          2
938.27203D0 1.602176487D-19  1.79284735D0 0. 0.               ! otherwise zgoubi only uses rigidity.
! 'PARTICUL'
! PROTON
 
 'TOSCA'                                                                                                      3
0  2
1.  1. 1. 1.
HEADER_8
315 151 1 22.1 1.                    !  IZ=1 -> 2D ; MOD=22 -> polar map ; .MOD2=.1 -> one map file.
geneSectorMap_180deg.out
0 0 0 0
2
1.
2
0. 0. 0. 0.
 'FAISCEAU'                                                                                                   4
 'TOSCA'                                                                                                      5
0  2
1.  1. 1. 1.
HEADER_8
315 151 1 22.3   .3  .25 .45         ! Just for the fun : field maps can be (linearly) superimposed.
geneSectorMap_180deg.out
geneSectorMap_180deg.out
geneSectorMap_180deg.out
0 0 0 0
2
1.
2
0. 0. 0. 0.
 
 'FAISCEAU'  #End                                                                                             6
 
 'SYSTEM'                                                                                                     7
5
cp gnuplot_zgoubi.plt.cmd gnuplot_zgoubi.plt_temp.cmd
sed -i 's@pause 2@pause 0@g' gnuplot_zgoubi.plt_temp.cmd
gnuplot < gnuplot_zgoubi.plt_temp.cmd
cp gnuplot_zgoubi.plt_XYLab.eps gnuplot_zgoubi.plt_XYLab_save.eps
okular gnuplot_zgoubi.plt_XYLab.eps &
 
 'END'                                                                                                        8

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       8 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


     Particle  properties :
      
                     Mass          =    938.272        MeV/c2
                     Charge        =   1.602176E-19    C     
                     G  factor     =    1.79285              

              Reference  data :
                    mag. rigidity (kG.cm)   :   64.624444      =p/q, such that dev.=B*L/rigidity
                    mass (MeV/c2)           :   938.27203    
                    momentum (MeV/c)        :   19.373921    
                    energy, total (MeV)     :   938.47203    
                    energy, kinetic (MeV)   :  0.20000000    
                    beta = v/c              :  2.0644111166E-02
                    gamma                   :   1.000213158    
                    beta*gamma              :  2.0648511619E-02
                    G*gamma                 :   1.793229509    
                    electric rigidity (MeV) :  0.3999573771    =T[eV]*(gamma+1)/gamma, such that dev.=E*L/rigidity
  
 I, AMQ(1,I), AMQ(2,I)/QE, P/Pref, v/c, time, s :
  
     1   9.38272030E+02  1.00000000E+00  1.00000000E+00  2.06441112E-02  0.00000000E+00  0.00000000E+00
     2   9.38272030E+02  1.00000000E+00  1.73223540E+00  3.57452247E-02  0.00000000E+00  0.00000000E+00
     3   9.38272030E+02  1.00000000E+00  2.23654450E+00  4.61321480E-02  0.00000000E+00  0.00000000E+00
     4   9.38272030E+02  1.00000000E+00  2.64659710E+00  5.45668715E-02  0.00000000E+00  0.00000000E+00
     5   9.38272030E+02  1.00000000E+00  3.00127860E+00  6.18532755E-02  0.00000000E+00  0.00000000E+00
     6   9.38272030E+02  1.00000000E+00  3.31839160E+00  6.83595624E-02  0.00000000E+00  0.00000000E+00
     7   9.38272030E+02  1.00000000E+00  3.60785600E+00  7.42909924E-02  0.00000000E+00  0.00000000E+00
     8   9.38272030E+02  1.00000000E+00  3.87587150E+00  7.97759048E-02  0.00000000E+00  0.00000000E+00

************************************************************************************************************************************
      3  Keyword, label(s) :  TOSCA                                                 


                OPEN FILE zgoubi.plt                                                                      
                FOR PRINTING TRAJECTORIES


     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00

          New field map(s) now used, polar mesh (MOD .ge. 20) ;  name(s) of map data file(s) are : 
          'geneSectorMap_180deg.out'

   ----
   Map file number    1 ( of 1) :

     geneSectorMap_180deg.out map,  FORMAT type : regular.  Field multiplication factor :  1.00000000E+00

 HEADER  (8 lines) : 
        1.0000000000000000       0.50000000000000000       0.57324840764331209       2.12199579145933796E-314      !  Rmi/cm,
      # Field map generated using geneSectorMap.f                                                                            
     # AT/rd,  AT/deg, Rmi/cm, Rma/cm, RM/cm, NR, dR/cm, NX, dX/cm, dA/rd :                                                  
     #   3.14159265E+00   1.80000000E+02   1.00000000E+00   7.60000000E+01   5.00000000E+01 151   5.00000000E-01 315   5.0025
      # For TOSCA:          315         151  1 22.1 1.  !IZ=1 -> 2D ; MOD=22 -> polar map ; .MOD2=.1 -> one map file         
      # R*cosA (A:0->360), Z==0, R*sinA, BY, BZ, BX                                                                          
      # cm                 cm    cm      kG  kG  kG                                                                          
      #                                                                                                                      
 
R_min (cm), DR (cm), DTTA (deg), DZ (cm) :   1.000000E+00  5.000000E-01  5.732484E-01  2.121996-314



     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.571    12.925    -0.000     0.000     0.000            1
  A    1  1.7322    22.389     0.000     0.000     0.000            1.571    22.389    -0.000     0.000     0.000            2
  A    1  2.2365    28.907     0.000     0.000     0.000            1.571    28.907    -0.000     0.000     0.000            3
  A    1  2.6466    34.207     0.000     0.000     0.000            1.571    34.207    -0.000     0.000     0.000            4
  A    1  3.0013    38.791     0.000     0.000     0.000            1.571    38.791    -0.000     0.000     0.000            5
  A    1  3.3184    42.890     0.000     0.000     0.000            1.571    42.890    -0.000     0.000     0.000            6
  A    1  3.6079    46.631     0.000     0.000     0.000            1.571    46.631    -0.000     0.000     0.000            7
  A    1  3.8759    50.095     0.000     0.000     0.000            1.571    50.095    -0.000     0.000     0.000            8


                CONDITIONS  DE  MAXWELL  (      875.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      3)
                                                  8 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000    12.925     0.000     0.000     0.000      0.0000    0.0000   12.925   -0.000    0.000    0.000   4.060469E+01     1
               Time of flight (mus) :  6.56083763E-02 mass (MeV/c2) :   938.272    
o  1   1.7322    22.389     0.000     0.000     0.000      0.0000    0.7322   22.389   -0.000    0.000    0.000   7.033695E+01     2
               Time of flight (mus) :  6.56364081E-02 mass (MeV/c2) :   938.272    
o  1   2.2365    28.907     0.000     0.000     0.000      0.0000    1.2365   28.907   -0.000    0.000    0.000   9.081429E+01     3
               Time of flight (mus) :  6.56643776E-02 mass (MeV/c2) :   938.272    
o  1   2.6466    34.207     0.000     0.000     0.000      0.0000    1.6466   34.207   -0.000    0.000    0.000   1.074644E+02     4
               Time of flight (mus) :  6.56923430E-02 mass (MeV/c2) :   938.272    
o  1   3.0013    38.791     0.000     0.000     0.000      0.0000    2.0013   38.791   -0.000    0.000    0.000   1.218661E+02     5
               Time of flight (mus) :  6.57203076E-02 mass (MeV/c2) :   938.272    
o  1   3.3184    42.890     0.000     0.000     0.000      0.0000    2.3184   42.890   -0.000    0.000    0.000   1.347424E+02     6
               Time of flight (mus) :  6.57482719E-02 mass (MeV/c2) :   938.272    
o  1   3.6079    46.631     0.000     0.000     0.000      0.0000    2.6079   46.631   -0.000    0.000    0.000   1.464960E+02     7
               Time of flight (mus) :  6.57762360E-02 mass (MeV/c2) :   938.272    
o  1   3.8759    50.095     0.000     0.000     0.000      0.0000    2.8759   50.095   -0.000    0.000    0.000   1.573787E+02     8
               Time of flight (mus) :  6.58042001E-02 mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   1.0536E-08  -9.9550E-01   4.1844E+06   3.460440E-01  -1.576428E-08        8        6   0.7500      (Y,T)         1
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        8        8    1.000      (Z,P)         1
   5.4476E-08  -3.3874E+03   2.3687E-01   6.570632E-02   1.600000E+00        8        7   0.8750      (t,K)         1

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   1.184601E-01
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   3.994610E-08

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   6.408933E-05
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   9.165152E-01


  Beam  sigma  matrix : 

   1.403279E-02   3.338492E-09   0.000000E+00   0.000000E+00
   3.338492E-09   1.595691E-15   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     3.353576E-09    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      5  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   3 ;  Stored in field array # IMAP =    4 ;  
     Value of MOD.MOD2 is 22.3

           3-D map. MOD=22 or 23.   Will sum up 3  field maps, with field coefficient values : 
                 3.000000E-01  2.500000E-01  4.500000E-01

          New field map(s) now used, polar mesh (MOD .ge. 20) ;  name(s) of map data file(s) are : 
          'geneSectorMap_180deg.out'
          'geneSectorMap_180deg.out'
          'geneSectorMap_180deg.out'

   ----
   Map file number    1 ( of 3) :

     geneSectorMap_180deg.out map,  FORMAT type : regular.  Field multiplication factor :  3.00000000E-01

 HEADER  (8 lines) : 
        1.0000000000000000       0.50000000000000000       0.57324840764331209       2.12199579145933796E-314      !  Rmi/cm,
      # Field map generated using geneSectorMap.f                                                                            
     # AT/rd,  AT/deg, Rmi/cm, Rma/cm, RM/cm, NR, dR/cm, NX, dX/cm, dA/rd :                                                  
     #   3.14159265E+00   1.80000000E+02   1.00000000E+00   7.60000000E+01   5.00000000E+01 151   5.00000000E-01 315   5.0025
      # For TOSCA:          315         151  1 22.1 1.  !IZ=1 -> 2D ; MOD=22 -> polar map ; .MOD2=.1 -> one map file         
      # R*cosA (A:0->360), Z==0, R*sinA, BY, BZ, BX                                                                          
      # cm                 cm    cm      kG  kG  kG                                                                          
      #                                                                                                                      
 
R_min (cm), DR (cm), DTTA (deg), DZ (cm) :   1.000000E+00  5.000000E-01  5.732484E-01  2.121996-314


   ----
   Map file number    2 ( of 3) :

     geneSectorMap_180deg.out map,  FORMAT type : regular.  Field multiplication factor :  2.50000000E-01

 HEADER  (8 lines) : 
        1.0000000000000000       0.50000000000000000       0.57324840764331209       2.12199579145933796E-314      !  Rmi/cm,
      # Field map generated using geneSectorMap.f                                                                            
     # AT/rd,  AT/deg, Rmi/cm, Rma/cm, RM/cm, NR, dR/cm, NX, dX/cm, dA/rd :                                                  
     #   3.14159265E+00   1.80000000E+02   1.00000000E+00   7.60000000E+01   5.00000000E+01 151   5.00000000E-01 315   5.0025
      # For TOSCA:          315         151  1 22.1 1.  !IZ=1 -> 2D ; MOD=22 -> polar map ; .MOD2=.1 -> one map file         
      # R*cosA (A:0->360), Z==0, R*sinA, BY, BZ, BX                                                                          
      # cm                 cm    cm      kG  kG  kG                                                                          
      #                                                                                                                      
 
R_min (cm), DR (cm), DTTA (deg), DZ (cm) :   1.000000E+00  5.000000E-01  5.732484E-01  2.121996-314


   ----
   Map file number    3 ( of 3) :

     geneSectorMap_180deg.out map,  FORMAT type : regular.  Field multiplication factor :  4.50000000E-01

 HEADER  (8 lines) : 
        1.0000000000000000       0.50000000000000000       0.57324840764331209       2.12199579145933796E-314      !  Rmi/cm,
      # Field map generated using geneSectorMap.f                                                                            
     # AT/rd,  AT/deg, Rmi/cm, Rma/cm, RM/cm, NR, dR/cm, NX, dX/cm, dA/rd :                                                  
     #   3.14159265E+00   1.80000000E+02   1.00000000E+00   7.60000000E+01   5.00000000E+01 151   5.00000000E-01 315   5.0025
      # For TOSCA:          315         151  1 22.1 1.  !IZ=1 -> 2D ; MOD=22 -> polar map ; .MOD2=.1 -> one map file         
      # R*cosA (A:0->360), Z==0, R*sinA, BY, BZ, BX                                                                          
      # cm                 cm    cm      kG  kG  kG                                                                          
      #                                                                                                                      
 
R_min (cm), DR (cm), DTTA (deg), DZ (cm) :   1.000000E+00  5.000000E-01  5.732484E-01  2.121996-314



     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  1.0000    12.925     0.000     0.000     0.000            1.571    12.925    -0.000     0.000     0.000            1
  A    1  1.7322    22.389     0.000     0.000     0.000            1.571    22.389    -0.000     0.000     0.000            2
  A    1  2.2365    28.907     0.000     0.000     0.000            1.571    28.907    -0.000     0.000     0.000            3
  A    1  2.6466    34.207     0.000     0.000     0.000            1.571    34.207     0.000     0.000     0.000            4
  A    1  3.0013    38.791     0.000     0.000     0.000            1.571    38.791    -0.000     0.000     0.000            5
  A    1  3.3184    42.890     0.000     0.000     0.000            1.571    42.890     0.000     0.000     0.000            6
  A    1  3.6079    46.631     0.000     0.000     0.000            1.571    46.631    -0.000     0.000     0.000            7
  A    1  3.8759    50.095     0.000     0.000     0.000            1.571    50.095    -0.000     0.000     0.000            8


                CONDITIONS  DE  MAXWELL  (      875.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      6  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      5)
                                                  8 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000    12.925     0.000     0.000     0.000      0.0000    0.0000   12.925   -0.000    0.000    0.000   8.120937E+01     1
               Time of flight (mus) :  0.13121675     mass (MeV/c2) :   938.272    
o  1   1.7322    22.389     0.000     0.000     0.000      0.0000    0.7322   22.389   -0.000    0.000    0.000   1.406739E+02     2
               Time of flight (mus) :  0.13127282     mass (MeV/c2) :   938.272    
o  1   2.2365    28.907     0.000     0.000     0.000      0.0000    1.2365   28.907   -0.000    0.000    0.000   1.816286E+02     3
               Time of flight (mus) :  0.13132876     mass (MeV/c2) :   938.272    
o  1   2.6466    34.207     0.000     0.000     0.000      0.0000    1.6466   34.207    0.000    0.000    0.000   2.149287E+02     4
               Time of flight (mus) :  0.13138469     mass (MeV/c2) :   938.272    
o  1   3.0013    38.791     0.000     0.000     0.000      0.0000    2.0013   38.791   -0.000    0.000    0.000   2.437322E+02     5
               Time of flight (mus) :  0.13144062     mass (MeV/c2) :   938.272    
o  1   3.3184    42.890     0.000     0.000     0.000      0.0000    2.3184   42.890    0.000    0.000    0.000   2.694848E+02     6
               Time of flight (mus) :  0.13149654     mass (MeV/c2) :   938.272    
o  1   3.6079    46.631     0.000     0.000     0.000      0.0000    2.6079   46.631   -0.000    0.000    0.000   2.929921E+02     7
               Time of flight (mus) :  0.13155247     mass (MeV/c2) :   938.272    
o  1   3.8759    50.095     0.000     0.000     0.000      0.0000    2.8759   50.095   -0.000    0.000    0.000   3.147575E+02     8
               Time of flight (mus) :  0.13160840     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   2.8176E-14  -9.6041E-01   1.5646E+12   3.460441E-01  -3.985091E-14        8        6   0.7500      (Y,T)         1
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        8        8    1.000      (Z,P)         1
   1.1281E-07  -3.2715E+03   4.5753E-01   1.314126E-01   1.600000E+00        8        7   0.8750      (t,K)         1

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   1.184600E-01
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   1.049737E-13

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   1.281796E-04
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   9.165152E-01


  Beam  sigma  matrix : 

   1.403276E-02   8.613675E-15   0.000000E+00   0.000000E+00
   8.613675E-15   1.101948E-26   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     8.968742E-15    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      7  Keyword, label(s) :  SYSTEM                                                

     Number  of  commands :   5,  as  follows : 

 cp gnuplot_zgoubi.plt.cmd gnuplot_zgoubi.plt_temp.cmd
 sed -i 's@pause 2@pause 0@g' gnuplot_zgoubi.plt_temp.cmd
 gnuplot < gnuplot_zgoubi.plt_temp.cmd
 cp gnuplot_zgoubi.plt_XYLab.eps gnuplot_zgoubi.plt_XYLab_save.eps
 okular gnuplot_zgoubi.plt_XYLab.eps &

************************************************************************************************************************************
      8  Keyword, label(s) :  END                                                   


                             8 particles have been launched
                     Made  it  to  the  end :      8

************************************************************************************************************************************
 Pgm zgoubi : Execution ended normally, upon keyword END or FIN
   
            ZGOUBI RUN COMPLETED. 

  Zgoubi, author's dvlpmnt version.
  Job  started  on  27-09-2018,  at  10:15:56 
  JOB  ENDED  ON    27-09-2018,  AT  10:15:59 

   CPU time, total :     2.3601470000000000     
