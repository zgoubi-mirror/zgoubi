
set title "Plotted from file zgoubi.plt  \n From zgoubi's polar frame to lab frame \n u ($10 *cm2m *cos($22)):($10 *cm2m *sin($22)) " font "sans, 14"

set key maxcol 1
set key t r 

#set logscale y 

set xtics mirror font  "sans, 14"
set ytics mirror font  "sans, 14"

set xlabel 'Y * cos(X)  [m]' font  "sans, 14"
set ylabel 'Y * sin(X)  [m]' font  "sans, 14"

cm2m = 0.01
MeV2eV = 1e6
am = 938.27203
c = 2.99792458e8
pi = 4. * atan(1.)

NOEL_1 = 4   #   number of 1st TOSCA in zgoubi,plt (col. 42)
NOEL_2 = 7   #   number of 2nd TOSCA in zgoubi,plt (col. 42)

set size ratio -1

plot  \
   'zgoubi.plt' u ($42==NOEL_1 ? $10 *cm2m *cos($22) :1/0):($10 *cm2m *sin($22)) w l lc rgb 'red' tit 'x\_lab, y\_lab' ,\
   'zgoubi.plt' u ($42==NOEL_2 ? $10 *cm2m *cos($22+pi) :1/0):($10 *cm2m *sin($22+pi)) w l lc rgb 'blue' notit

     set terminal postscript eps blacktext color  enh  "Times-Sans" 12  
       set output "gnuplot_zgoubi.plt_XYLab.eps"  
       replot  
       set terminal X11  
       unset output  

 
pause 2   # don't change this: needed for proper running of sector180deg
exit

