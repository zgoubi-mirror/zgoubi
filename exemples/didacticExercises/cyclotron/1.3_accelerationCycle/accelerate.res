Uniform field sector
 'OBJET'                                                                                                      1
64.62444403717985                                                                   ! 200keV proton.
2                                                                                         ! Generate
1 1                                                                                    ! 1 particle.
4.087013 0. 0. 0. 0. 0.3162126 'o' ! p[MeV/c]=  6.126278, Brho[kG.cm]= 20.435064, kin-E[MeV]=  0.0200
1
 
 'PARTICUL'              ! This is required only because we want to get the time-of-flight                    2
938.27203D0 1.602176487D-19  1.79284735D0 0. 0.  ! otherwise zgoubi only requires rigidity.
! 'PARTICUL'
! PROTON
 
 'FAISTORE'                                                                                                   3
zgoubi.fai   #End
1
 'TOSCA'                                                                                                      4
0  2
1.  1. 1. 1.
HEADER_8
315 151 1 22.1 1.                    !  IZ=1 -> 2D ; MOD=22 -> polar map ; .MOD2=.1 -> one map file.
geneSectorMap_180deg.out
0 0 0 0
2
1.
2
0. 0. 0. 0.
 'FAISCEAU'                                                                                                   5
 
 'CAVITE'                                                                                                     6
3                                    ! dW = qVsin(phi_s), no synch. motion
0. 0.
100e3  1.57079632679
 
 'TOSCA'                                                                                                      7
0  2
1.  1. 1. 1.
HEADER_8
315 151 1 22.1   1.                  ! Just for the fun : field maps can be (linearly) superimposed.
geneSectorMap_180deg.out
0 0 0 0
2
1.
2
0. 0. 0. 0.
 
 'FAISCEAU'  #End                                                                                             8
 
 'CAVITE'                                                                                                     9
3                                    ! dW = qVsin(phi_s), no synch. motion
0. 0.
100e3  1.57079632679
 
 'FAISCEAU'                                                                                                  10
 
 'REBELOTE'             ! K = 99 : coordinates at end of previous pass are used as initial                   11
60  1.1 99              ! coordinates for the next pass ; idem for spin components.
!                       ! Note that, (i) Y0 remains constant (due to the "microtron configuration"),
!                       ! (ii) is updated by passage through CAVITE
 'FAISCEAU'                                                                                                  12
 
 'SYSTEM'                                                                                                    13
6
cp gnuplot_zgoubi.plt.cmd gnuplot_zgoubi.plt_temp.cmd
sed -i 's@pause 2@pause 0@g' gnuplot_zgoubi.plt_temp.cmd
gnuplot < gnuplot_zgoubi.plt_temp.cmd
mv -f gnuplot_zgoubi.plt_XYLab.eps gnuplot_zgoubi.plt_XYLab_cyclotron.eps
okular gnuplot_zgoubi.plt_XYLab_cyclotron.eps &
rm -f gnuplot_zgoubi.plt_temp.cmd
 
 'END'                                                                                                       14

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 

                          MAGNETIC  RIGIDITY =         64.624 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


     Particle  properties :
      
                     Mass          =    938.272        MeV/c2
                     Charge        =   1.602176E-19    C     
                     G  factor     =    1.79285              

              Reference  data :
                    mag. rigidity (kG.cm)   :   64.624444      =p/q, such that dev.=B*L/rigidity
                    mass (MeV/c2)           :   938.27203    
                    momentum (MeV/c)        :   19.373921    
                    energy, total (MeV)     :   938.47203    
                    energy, kinetic (MeV)   :  0.20000000    
                    beta = v/c              :  2.0644111178E-02
                    gamma                   :   1.000213158    
                    beta*gamma              :  2.0648511631E-02
                    G*gamma                 :   1.793229509    
                    electric rigidity (MeV) :  0.3999573775    =T[eV]*(gamma+1)/gamma, such that dev.=E*L/rigidity
  
 I, AMQ(1,I), AMQ(2,I)/QE, P/Pref, v/c, time, s :
  
     1   9.38272030E+02  1.00000000E+00  3.16212600E-01  6.52918037E-03  0.00000000E+00  0.00000000E+00

************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


                OPEN FILE zgoubi.fai                                                                      
                FOR PRINTING COORDINATES 

               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


                OPEN FILE zgoubi.plt                                                                      
                FOR PRINTING TRAJECTORIES


     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00

          New field map(s) now used, polar mesh (MOD .ge. 20) ;  name(s) of map data file(s) are : 
          'geneSectorMap_180deg.out'

   ----
   Map file number    1 ( of 1) :

     geneSectorMap_180deg.out map,  FORMAT type : regular.  Field multiplication factor :  1.00000000E+00

 HEADER  (8 lines) : 
        1.0000000000000000       0.50000000000000000       0.57324840764331209       2.12199579145933796E-314      !  Rmi/cm,
      # Field map generated using geneSectorMap.f                                                                            
     # AT/rd,  AT/deg, Rmi/cm, Rma/cm, RM/cm, NR, dR/cm, NX, dX/cm, dA/rd :                                                  
     #   3.14159265E+00   1.80000000E+02   1.00000000E+00   7.60000000E+01   5.00000000E+01 151   5.00000000E-01 315   5.0025
      # For TOSCA:          315         151  1 22.1 1.  !IZ=1 -> 2D ; MOD=22 -> polar map ; .MOD2=.1 -> one map file         
      # R*cosA (A:0->360), Z==0, R*sinA, BY, BZ, BX                                                                          
      # cm                 cm    cm      kG  kG  kG                                                                          
      #                                                                                                                      
 
R_min (cm), DR (cm), DTTA (deg), DZ (cm) :   1.000000E+00  5.000000E-01  5.732484E-01  2.121996-314



     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571     4.086    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (       14.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000   -0.6838    4.086   -0.030    0.000    0.000   1.283831E+01     1
               Time of flight (mus) :  6.55886160E-02 mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.085806E-02  -2.998651E-05        1        0    0.000      (Y,T)         1
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)         1
   0.0000E+00   0.0000E+00   1.0000E+00   6.558862E-02   2.000000E-02        1        1    1.000      (t,K)         1

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.977046E-36   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00

          New field map(s) now used, polar mesh (MOD .ge. 20) ;  name(s) of map data file(s) are : 
          'geneSectorMap_180deg.out'

   ----
   Map file number    1 ( of 1) :

     geneSectorMap_180deg.out map,  FORMAT type : regular.  Field multiplication factor :  1.00000000E+00

 HEADER  (8 lines) : 
        1.0000000000000000       0.50000000000000000       0.57324840764331209       2.12199579145933796E-314      !  Rmi/cm,
      # Field map generated using geneSectorMap.f                                                                            
     # AT/rd,  AT/deg, Rmi/cm, Rma/cm, RM/cm, NR, dR/cm, NX, dX/cm, dA/rd :                                                  
     #   3.14159265E+00   1.80000000E+02   1.00000000E+00   7.60000000E+01   5.00000000E+01 151   5.00000000E-01 315   5.0025
      # For TOSCA:          315         151  1 22.1 1.  !IZ=1 -> 2D ; MOD=22 -> polar map ; .MOD2=.1 -> one map file         
      # R*cosA (A:0->360), Z==0, R*sinA, BY, BZ, BX                                                                          
      # cm                 cm    cm      kG  kG  kG                                                                          
      #                                                                                                                      
 
R_min (cm), DR (cm), DTTA (deg), DZ (cm) :   1.000000E+00  5.000000E-01  5.732484E-01  2.121996-314



     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    15.937     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (       33.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000   -0.2254   15.937    0.012    0.000    0.000   4.428959E+01     1
               Time of flight (mus) :  0.13119075     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   1.593684E-01   1.175977E-05        1        0    0.000      (Y,T)         1
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)         1
   0.0000E+00   0.0000E+00   1.0000E+00   1.311908E-01   1.200000E-01        1        1    1.000      (t,K)         1

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   6.051518E-35   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    0.0488   15.937    0.009    0.000    0.000   4.428959E+01     1
               Time of flight (mus) :  0.13119075     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   1.593684E-01   8.684931E-06        1        0    0.000      (Y,T)         1
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)         1
   0.0000E+00   0.0000E+00   1.0000E+00   1.311908E-01   2.200000E-01        1        1    1.000      (t,K)         1

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   6.051518E-35   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #        1 through the optical structure 

                     Total of          1 particles have been launched

     Multiple pass, 
          from element #     1 : OBJET     /label1=                    /label2=                    
                             to  REBELOTE  /label1=                    /label2=                    
     ending at pass #      61 at element #    11 : REBELOTE  /label1=                    /label2=                    


************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    11.175    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (       44.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    0.0488   11.175   -0.009    0.000    0.000   8.687661E+01     1
               Time of flight (mus) :  0.19680090     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   1.117475E-01  -8.782011E-06        1        0    0.000      (Y,T)         2
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)         2
   0.0000E+00   0.0000E+00   1.0000E+00   1.968009E-01   2.200000E-01        1        1    1.000      (t,K)         2

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.000177E-37   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    21.524     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (       52.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    0.2650   21.524    0.007    0.000    0.000   1.382394E+02     1
               Time of flight (mus) :  0.26241741     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   2.152395E-01   7.270407E-06        1        0    0.000      (Y,T)         2
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)         2
   0.0000E+00   0.0000E+00   1.0000E+00   2.624174E-01   3.200000E-01        1        1    1.000      (t,K)         2

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   4.391959E-35   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    0.4492   21.524    0.006    0.000    0.000   1.382394E+02     1
               Time of flight (mus) :  0.26241741     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   2.152395E-01   6.345963E-06        1        0    0.000      (Y,T)         2
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)         2
   0.0000E+00   0.0000E+00   1.0000E+00   2.624174E-01   4.200000E-01        1        1    1.000      (t,K)         2

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   4.391959E-35   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #        2 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    15.938    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (       60.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    0.4492   15.938   -0.006    0.000    0.000   1.970849E+02     1
               Time of flight (mus) :  0.32804149     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   1.593812E-01  -6.360793E-06        1        0    0.000      (Y,T)         3
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)         3
   0.0000E+00   0.0000E+00   1.0000E+00   3.280415E-01   4.200000E-01        1        1    1.000      (t,K)         3

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.778405E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    25.747     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (       66.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    0.6126   25.747    0.006    0.000    0.000   2.625634E+02     1
               Time of flight (mus) :  0.39367207     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   2.574693E-01   5.714143E-06        1        0    0.000      (Y,T)         3
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)         3
   0.0000E+00   0.0000E+00   1.0000E+00   3.936721E-01   5.200000E-01        1        1    1.000      (t,K)         3

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.024973E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    0.7609   25.747    0.005    0.000    0.000   2.625634E+02     1
               Time of flight (mus) :  0.39367207     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   2.574693E-01   5.232935E-06        1        0    0.000      (Y,T)         3
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)         3
   0.0000E+00   0.0000E+00   1.0000E+00   3.936721E-01   6.200000E-01        1        0    0.000      (t,K)         3

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.024973E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #        3 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    19.771    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (       72.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    0.7609   19.771   -0.005    0.000    0.000   3.340636E+02     1
               Time of flight (mus) :  0.45931009     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   1.977138E-01  -5.234284E-06        1        0    0.000      (Y,T)         4
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)         4
   0.0000E+00   0.0000E+00   1.0000E+00   4.593101E-01   6.200000E-01        1        0    0.000      (t,K)         4

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   6.178692E-37   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    29.282     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (       78.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    0.8976   29.282    0.005    0.000    0.000   4.111161E+02     1
               Time of flight (mus) :  0.52495469     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   2.928191E-01   4.854198E-06        1        0    0.000      (Y,T)         4
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)         4
   0.0000E+00   0.0000E+00   1.0000E+00   5.249547E-01   7.200000E-01        1        1    1.000      (t,K)         4

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   3.766401E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    1.0252   29.282    0.005    0.000    0.000   4.111161E+02     1
               Time of flight (mus) :  0.52495469     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   2.928191E-01   4.548469E-06        1        0    0.000      (Y,T)         4
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)         4
   0.0000E+00   0.0000E+00   1.0000E+00   5.249547E-01   8.200000E-01        1        1    1.000      (t,K)         4

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   3.766401E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #        4 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    23.069    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (       83.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    1.0252   23.069   -0.005    0.000    0.000   4.933483E+02     1
               Time of flight (mus) :  0.59060667     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   2.306854E-01  -4.549945E-06        1        0    0.000      (Y,T)         5
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)         5
   0.0000E+00   0.0000E+00   1.0000E+00   5.906067E-01   8.200000E-01        1        1    1.000      (t,K)         5

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.314563E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    32.384     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (       88.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    1.1452   32.384    0.004    0.000    0.000   5.804522E+02     1
               Time of flight (mus) :  0.65626528     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   3.238369E-01   4.294008E-06        1        0    0.000      (Y,T)         5
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)         5
   0.0000E+00   0.0000E+00   1.0000E+00   6.562653E-01   9.200000E-01        1        0    0.000      (t,K)         5

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   4.555114E-35   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    1.2588   32.384    0.004    0.000    0.000   5.804522E+02     1
               Time of flight (mus) :  0.65626528     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   3.238369E-01   4.077980E-06        1        0    0.000      (Y,T)         5
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)         5
   0.0000E+00   0.0000E+00   1.0000E+00   6.562653E-01   1.020000E+00        1        0    0.000      (t,K)         5

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   4.555114E-35   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #        5 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    26.006    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (       93.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    1.2588   26.006   -0.004    0.000    0.000   6.721709E+02     1
               Time of flight (mus) :  0.72193122     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   2.600608E-01  -4.079839E-06        1        0    0.000      (Y,T)         6
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)         6
   0.0000E+00   0.0000E+00   1.0000E+00   7.219312E-01   1.020000E+00        1        0    0.000      (t,K)         6

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   9.521504E-37   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    35.181     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (       97.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    1.3670   35.181    0.004    0.000    0.000   7.682825E+02     1
               Time of flight (mus) :  0.78760383     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   3.518065E-01   3.892479E-06        1        0    0.000      (Y,T)         6
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)         6
   0.0000E+00   0.0000E+00   1.0000E+00   7.876038E-01   1.120000E+00        1        0    0.000      (t,K)         6

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   6.745060E-35   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    1.4705   35.181    0.004    0.000    0.000   7.682825E+02     1
               Time of flight (mus) :  0.78760383     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   3.518065E-01   3.729442E-06        1        0    0.000      (Y,T)         6
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)         6
   0.0000E+00   0.0000E+00   1.0000E+00   7.876038E-01   1.220000E+00        1        0    0.000      (t,K)         6

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   6.745060E-35   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #        6 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    28.681    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      101.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    1.4705   28.681   -0.004    0.000    0.000   8.685963E+02     1
               Time of flight (mus) :  0.85328374     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   2.868094E-01  -3.729888E-06        1        0    0.000      (Y,T)         7
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)         7
   0.0000E+00   0.0000E+00   1.0000E+00   8.532837E-01   1.220000E+00        1        0    0.000      (t,K)         7

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   6.700612E-35   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    37.748     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      105.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    1.5698   37.748    0.004    0.000    0.000   9.729426E+02     1
               Time of flight (mus) :  0.91897034     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   3.774816E-01   3.585392E-06        1        0    0.000      (Y,T)         7
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)         7
   0.0000E+00   0.0000E+00   1.0000E+00   9.189703E-01   1.320000E+00        1        0    0.000      (t,K)         7

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.055947E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    1.6654   37.748    0.003    0.000    0.000   9.729426E+02     1
               Time of flight (mus) :  0.91897034     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   3.774816E-01   3.456749E-06        1        0    0.000      (Y,T)         7
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)         7
   0.0000E+00   0.0000E+00   1.0000E+00   9.189703E-01   1.420000E+00        1        0    0.000      (t,K)         7

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.055947E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #        7 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    31.153    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      109.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    1.6654   31.153   -0.003    0.000    0.000   1.081173E+03     1
               Time of flight (mus) :  0.98466423     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   3.115309E-01  -3.457124E-06        1        0    0.000      (Y,T)         8
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)         8
   0.0000E+00   0.0000E+00   1.0000E+00   9.846642E-01   1.420000E+00        1        0    0.000      (t,K)         8

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.193193E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    40.135     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      112.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    1.7578   40.135    0.003    0.000    0.000   1.193151E+03     1
               Time of flight (mus) :   1.0503648     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.013488E-01   3.340955E-06        1        0    0.000      (Y,T)         8
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)         8
   0.0000E+00   0.0000E+00   1.0000E+00   1.050365E+00   1.520000E+00        1        1    1.000      (t,K)         8

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.523441E-35   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    1.8471   40.135    0.003    0.000    0.000   1.193151E+03     1
               Time of flight (mus) :   1.0503648     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.013488E-01   3.236111E-06        1        0    0.000      (Y,T)         8
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)         8
   0.0000E+00   0.0000E+00   1.0000E+00   1.050365E+00   1.620000E+00        1        0    0.000      (t,K)         8

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.523441E-35   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #        8 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    33.463    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      116.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    1.8471   33.463   -0.003    0.000    0.000   1.308758E+03     1
               Time of flight (mus) :   1.1160727     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   3.346270E-01  -3.236199E-06        1        0    0.000      (Y,T)         9
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)         9
   0.0000E+00   0.0000E+00   1.0000E+00   1.116073E+00   1.620000E+00        1        0    0.000      (t,K)         9

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   3.268528E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    42.374     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      120.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    1.9338   42.374    0.003    0.000    0.000   1.427883E+03     1
               Time of flight (mus) :   1.1817873     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.237442E-01   3.140343E-06        1        0    0.000      (Y,T)         9
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)         9
   0.0000E+00   0.0000E+00   1.0000E+00   1.181787E+00   1.720000E+00        1        0    0.000      (t,K)         9

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   4.903935E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    2.0179   42.374    0.003    0.000    0.000   1.427883E+03     1
               Time of flight (mus) :   1.1817873     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.237442E-01   3.052769E-06        1        0    0.000      (Y,T)         9
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)         9
   0.0000E+00   0.0000E+00   1.0000E+00   1.181787E+00   1.820000E+00        1        1    1.000      (t,K)         9

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   4.903935E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #        9 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    35.638    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      123.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    2.0179   35.638   -0.003    0.000    0.000   1.550425E+03     1
               Time of flight (mus) :   1.2475091     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   3.563820E-01  -3.052848E-06        1        0    0.000      (Y,T)        10
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        10
   0.0000E+00   0.0000E+00   1.0000E+00   1.247509E+00   1.820000E+00        1        1    1.000      (t,K)        10

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   9.311091E-35   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    44.491     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      126.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    2.0998   44.491    0.003    0.000    0.000   1.676292E+03     1
               Time of flight (mus) :   1.3132377     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.449110E-01   2.972089E-06        1        0    0.000      (Y,T)        10
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        10
   0.0000E+00   0.0000E+00   1.0000E+00   1.313238E+00   1.920000E+00        1        0    0.000      (t,K)        10

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.327773E-35   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    2.1796   44.491    0.003    0.000    0.000   1.676292E+03     1
               Time of flight (mus) :   1.3132377     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.449110E-01   2.897511E-06        1        0    0.000      (Y,T)        10
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        10
   0.0000E+00   0.0000E+00   1.0000E+00   1.313238E+00   2.020000E+00        1        0    0.000      (t,K)        10

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.327773E-35   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       10 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    37.701    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      130.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    2.1796   37.701   -0.003    0.000    0.000   1.805398E+03     1
               Time of flight (mus) :   1.3789735     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   3.770060E-01  -2.897710E-06        1        0    0.000      (Y,T)        11
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        11
   0.0000E+00   0.0000E+00   1.0000E+00   1.378973E+00   2.020000E+00        1        0    0.000      (t,K)        11

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.063727E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    46.503     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      133.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    2.2574   46.503    0.003    0.000    0.000   1.937665E+03     1
               Time of flight (mus) :   1.4447160     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.650321E-01   2.828341E-06        1        0    0.000      (Y,T)        11
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        11
   0.0000E+00   0.0000E+00   1.0000E+00   1.444716E+00   2.120000E+00        1        0    0.000      (t,K)        11

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   9.052041E-36   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    2.3335   46.503    0.003    0.000    0.000   1.937665E+03     1
               Time of flight (mus) :   1.4447160     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.650321E-01   2.763832E-06        1        0    0.000      (Y,T)        11
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        11
   0.0000E+00   0.0000E+00   1.0000E+00   1.444716E+00   2.220000E+00        1        0    0.000      (t,K)        11

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   9.052041E-36   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       11 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    39.666    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      136.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    2.3335   39.666   -0.003    0.000    0.000   2.073019E+03     1
               Time of flight (mus) :   1.5104658     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   3.966595E-01  -2.763921E-06        1        0    0.000      (Y,T)        12
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        12
   0.0000E+00   0.0000E+00   1.0000E+00   1.510466E+00   2.220000E+00        1        0    0.000      (t,K)        12

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   3.653510E-35   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    48.425     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      139.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    2.4078   48.425    0.003    0.000    0.000   2.211392E+03     1
               Time of flight (mus) :   1.5762224     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.842493E-01   2.703550E-06        1        0    0.000      (Y,T)        12
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        12
   0.0000E+00   0.0000E+00   1.0000E+00   1.576222E+00   2.320000E+00        1        0    0.000      (t,K)        12

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.166384E-35   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    2.4806   48.425    0.003    0.000    0.000   2.211392E+03     1
               Time of flight (mus) :   1.5762224     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.842493E-01   2.647032E-06        1        0    0.000      (Y,T)        12
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        12
   0.0000E+00   0.0000E+00   1.0000E+00   1.576222E+00   2.420000E+00        1        0    0.000      (t,K)        12

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.166384E-35   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       12 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    41.547    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      142.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    2.4806   41.547   -0.003    0.000    0.000   2.352720E+03     1
               Time of flight (mus) :   1.6419861     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.154683E-01  -2.647109E-06        1        0    0.000      (Y,T)        13
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        13
   0.0000E+00   0.0000E+00   1.0000E+00   1.641986E+00   2.420000E+00        1        0    0.000      (t,K)        13

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   7.476218E-35   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    50.267     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      144.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    2.5518   50.267    0.003    0.000    0.000   2.496941E+03     1
               Time of flight (mus) :   1.7077567     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   5.026747E-01   2.593545E-06        1        0    0.000      (Y,T)        13
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        13
   0.0000E+00   0.0000E+00   1.0000E+00   1.707757E+00   2.520000E+00        1        0    0.000      (t,K)        13

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.094650E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    2.6217   50.267    0.003    0.000    0.000   2.496941E+03     1
               Time of flight (mus) :   1.7077567     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   5.026747E-01   2.543500E-06        1        0    0.000      (Y,T)        13
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        13
   0.0000E+00   0.0000E+00   1.0000E+00   1.707757E+00   2.620000E+00        1        1    1.000      (t,K)        13

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.094650E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       13 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    43.353    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      147.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    2.6217   43.353   -0.003    0.000    0.000   2.644000E+03     1
               Time of flight (mus) :   1.7735344     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.335331E-01  -2.543675E-06        1        0    0.000      (Y,T)        14
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        14
   0.0000E+00   0.0000E+00   1.0000E+00   1.773534E+00   2.620000E+00        1        1    1.000      (t,K)        14

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   7.617497E-35   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    52.040     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      150.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    2.6903   52.040    0.002    0.000    0.000   2.793844E+03     1
               Time of flight (mus) :   1.8393189     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   5.203994E-01   2.496370E-06        1        0    0.000      (Y,T)        14
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        14
   0.0000E+00   0.0000E+00   1.0000E+00   1.839319E+00   2.720000E+00        1        1    1.000      (t,K)        14

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.505684E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    2.7576   52.040    0.002    0.000    0.000   2.793844E+03     1
               Time of flight (mus) :   1.8393189     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   5.203994E-01   2.451643E-06        1        0    0.000      (Y,T)        14
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        14
   0.0000E+00   0.0000E+00   1.0000E+00   1.839319E+00   2.820000E+00        1        0    0.000      (t,K)        14

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.505684E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       14 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    45.094    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      153.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    2.7576   45.094   -0.002    0.000    0.000   2.946421E+03     1
               Time of flight (mus) :   1.9051106     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.509362E-01  -2.451667E-06        1        0    0.000      (Y,T)        15
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        15
   0.0000E+00   0.0000E+00   1.0000E+00   1.905111E+00   2.820000E+00        1        0    0.000      (t,K)        15

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   3.337493E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    53.750     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      155.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    2.8238   53.750    0.002    0.000    0.000   3.101683E+03     1
               Time of flight (mus) :   1.9709092     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   5.374979E-01   2.408887E-06        1        0    0.000      (Y,T)        15
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        15
   0.0000E+00   0.0000E+00   1.0000E+00   1.970909E+00   2.920000E+00        1        0    0.000      (t,K)        15

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   3.069463E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    2.8888   53.750    0.002    0.000    0.000   3.101683E+03     1
               Time of flight (mus) :   1.9709092     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   5.374979E-01   2.368606E-06        1        0    0.000      (Y,T)        15
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        15
   0.0000E+00   0.0000E+00   1.0000E+00   1.970909E+00   3.020000E+00        1        0    0.000      (t,K)        15

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   3.069463E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       15 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    46.775    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      158.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    2.8888   46.775   -0.002    0.000    0.000   3.259587E+03     1
               Time of flight (mus) :   2.0367149     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.677457E-01  -2.368654E-06        1        0    0.000      (Y,T)        16
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        16
   0.0000E+00   0.0000E+00   1.0000E+00   2.036715E+00   3.020000E+00        1        0    0.000      (t,K)        16

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.009574E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    55.403     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      161.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    2.9528   55.403    0.002    0.000    0.000   3.420087E+03     1
               Time of flight (mus) :   2.1025273     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   5.540327E-01   2.330300E-06        1        0    0.000      (Y,T)        16
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        16
   0.0000E+00   0.0000E+00   1.0000E+00   2.102527E+00   3.120000E+00        1        1    1.000      (t,K)        16

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   7.055905E-36   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    3.0157   55.403    0.002    0.000    0.000   3.420087E+03     1
               Time of flight (mus) :   2.1025273     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   5.540327E-01   2.293769E-06        1        0    0.000      (Y,T)        16
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        16
   0.0000E+00   0.0000E+00   1.0000E+00   2.102527E+00   3.220000E+00        1        0    0.000      (t,K)        16

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   7.055905E-36   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       16 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    48.402    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      163.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    3.0157   48.402   -0.002    0.000    0.000   3.583144E+03     1
               Time of flight (mus) :   2.1683470     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.840188E-01  -2.293871E-06        1        0    0.000      (Y,T)        17
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        17
   0.0000E+00   0.0000E+00   1.0000E+00   2.168347E+00   3.220000E+00        1        0    0.000      (t,K)        17

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   6.560912E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    57.006     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      166.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    3.0777   57.006    0.002    0.000    0.000   3.748718E+03     1
               Time of flight (mus) :   2.2341735     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   5.700563E-01   2.258985E-06        1        0    0.000      (Y,T)        17
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        17
   0.0000E+00   0.0000E+00   1.0000E+00   2.234173E+00   3.320000E+00        1        1    1.000      (t,K)        17

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.409447E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    3.1388   57.006    0.002    0.000    0.000   3.748718E+03     1
               Time of flight (mus) :   2.2341735     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   5.700563E-01   2.225655E-06        1        0    0.000      (Y,T)        17
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        17
   0.0000E+00   0.0000E+00   1.0000E+00   2.234173E+00   3.420000E+00        1        0    0.000      (t,K)        17

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.409447E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       17 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    49.980    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      168.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    3.1388   49.980   -0.002    0.000    0.000   3.916771E+03     1
               Time of flight (mus) :   2.3000072     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   4.998041E-01  -2.225741E-06        1        0    0.000      (Y,T)        18
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        18
   0.0000E+00   0.0000E+00   1.0000E+00   2.300007E+00   3.420000E+00        1        0    0.000      (t,K)        18

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   5.245827E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    58.561     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      170.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    3.1989   58.561    0.002    0.000    0.000   4.087268E+03     1
               Time of flight (mus) :   2.3658476     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   5.856137E-01   2.193248E-06        1        0    0.000      (Y,T)        18
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        18
   0.0000E+00   0.0000E+00   1.0000E+00   2.365848E+00   3.520000E+00        1        0    0.000      (t,K)        18

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.093504E-35   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    3.2583   58.561    0.002    0.000    0.000   4.087268E+03     1
               Time of flight (mus) :   2.3658476     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   5.856137E-01   2.162685E-06        1        0    0.000      (Y,T)        18
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        18
   0.0000E+00   0.0000E+00   1.0000E+00   2.365848E+00   3.620000E+00        1        0    0.000      (t,K)        18

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.093504E-35   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       18 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    51.514    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      173.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    3.2583   51.514   -0.002    0.000    0.000   4.260175E+03     1
               Time of flight (mus) :   2.4316952     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   5.151433E-01  -2.162716E-06        1        0    0.000      (Y,T)        19
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        19
   0.0000E+00   0.0000E+00   1.0000E+00   2.431695E+00   3.620000E+00        1        0    0.000      (t,K)        19

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   4.966464E-35   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    60.074     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      175.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    3.3168   60.074    0.002    0.000    0.000   4.435458E+03     1
               Time of flight (mus) :   2.4975497     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   6.007436E-01   2.133172E-06        1        0    0.000      (Y,T)        19
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        19
   0.0000E+00   0.0000E+00   1.0000E+00   2.497550E+00   3.720000E+00        1        0    0.000      (t,K)        19

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   9.150166E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    3.3746   60.074    0.002    0.000    0.000   4.435458E+03     1
               Time of flight (mus) :   2.4975497     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   6.007436E-01   2.105010E-06        1        0    0.000      (Y,T)        19
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        19
   0.0000E+00   0.0000E+00   1.0000E+00   2.497550E+00   3.820000E+00        1        0    0.000      (t,K)        19

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   9.150166E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       19 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    53.007    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      178.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    3.3746   53.007   -0.002    0.000    0.000   4.613086E+03     1
               Time of flight (mus) :   2.5634113     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   5.300724E-01  -2.105020E-06        1        0    0.000      (Y,T)        20
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        20
   0.0000E+00   0.0000E+00   1.0000E+00   2.563411E+00   3.820000E+00        1        0    0.000      (t,K)        20

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.168437E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    61.548     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      180.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    3.4316   61.548    0.002    0.000    0.000   4.793029E+03     1
               Time of flight (mus) :   2.6292797     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   6.154797E-01   2.077909E-06        1        0    0.000      (Y,T)        20
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        20
   0.0000E+00   0.0000E+00   1.0000E+00   2.629280E+00   3.920000E+00        1        1    1.000      (t,K)        20

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.639841E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    3.4879   61.548    0.002    0.000    0.000   4.793029E+03     1
               Time of flight (mus) :   2.6292797     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   6.154797E-01   2.051848E-06        1        0    0.000      (Y,T)        20
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        20
   0.0000E+00   0.0000E+00   1.0000E+00   2.629280E+00   4.020000E+00        1        0    0.000      (t,K)        20

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.639841E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       20 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    54.462    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      182.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    3.4879   54.462   -0.002    0.000    0.000   4.975257E+03     1
               Time of flight (mus) :   2.6951553     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   5.446228E-01  -2.051990E-06        1        0    0.000      (Y,T)        21
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        21
   0.0000E+00   0.0000E+00   1.0000E+00   2.695155E+00   4.020000E+00        1        0    0.000      (t,K)        21

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.283669E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    62.985     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      184.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    3.5435   62.985    0.002    0.000    0.000   5.159743E+03     1
               Time of flight (mus) :   2.7610377     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   6.298514E-01   2.026497E-06        1        0    0.000      (Y,T)        21
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        21
   0.0000E+00   0.0000E+00   1.0000E+00   2.761038E+00   4.120000E+00        1        0    0.000      (t,K)        21

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   8.294288E-35   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    3.5984   62.985    0.002    0.000    0.000   5.159743E+03     1
               Time of flight (mus) :   2.7610377     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   6.298514E-01   2.002289E-06        1        0    0.000      (Y,T)        21
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        21
   0.0000E+00   0.0000E+00   1.0000E+00   2.761038E+00   4.220000E+00        1        0    0.000      (t,K)        21

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   8.294288E-35   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       21 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    55.882    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      187.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    3.5984   55.882   -0.002    0.000    0.000   5.346460E+03     1
               Time of flight (mus) :   2.8269273     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   5.588223E-01  -2.002297E-06        1        0    0.000      (Y,T)        22
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        22
   0.0000E+00   0.0000E+00   1.0000E+00   2.826927E+00   4.220000E+00        1        0    0.000      (t,K)        22

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.592230E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    64.388     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      189.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    3.6527   64.388    0.002    0.000    0.000   5.535380E+03     1
               Time of flight (mus) :   2.8928237     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   6.438847E-01   1.978912E-06        1        0    0.000      (Y,T)        22
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        22
   0.0000E+00   0.0000E+00   1.0000E+00   2.892824E+00   4.320000E+00        1        0    0.000      (t,K)        22

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.396272E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    3.7063   64.388    0.002    0.000    0.000   5.535380E+03     1
               Time of flight (mus) :   2.8928237     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   6.438847E-01   1.956346E-06        1        0    0.000      (Y,T)        22
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        22
   0.0000E+00   0.0000E+00   1.0000E+00   2.892824E+00   4.420000E+00        1        0    0.000      (t,K)        22

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.396272E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       22 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    57.270    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      191.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    3.7063   57.270   -0.002    0.000    0.000   5.726481E+03     1
               Time of flight (mus) :   2.9587273     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   5.726952E-01  -1.956405E-06        1        0    0.000      (Y,T)        23
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        23
   0.0000E+00   0.0000E+00   1.0000E+00   2.958727E+00   4.420000E+00        1        0    0.000      (t,K)        23

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.485593E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    65.760     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      193.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    3.7594   65.760    0.002    0.000    0.000   5.919735E+03     1
               Time of flight (mus) :   3.0246377     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   6.576027E-01   1.934472E-06        1        0    0.000      (Y,T)        23
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        23
   0.0000E+00   0.0000E+00   1.0000E+00   3.024638E+00   4.520000E+00        1        0    0.000      (t,K)        23

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   9.885907E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    3.8119   65.760    0.002    0.000    0.000   5.919735E+03     1
               Time of flight (mus) :   3.0246377     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   6.576027E-01   1.913371E-06        1        0    0.000      (Y,T)        23
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        23
   0.0000E+00   0.0000E+00   1.0000E+00   3.024638E+00   4.620000E+00        1        0    0.000      (t,K)        23

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   9.885907E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       23 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    58.626    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      195.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    3.8119   58.626   -0.002    0.000    0.000   6.115121E+03     1
               Time of flight (mus) :   3.0905552     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   5.862633E-01  -1.913568E-06        1        0    0.000      (Y,T)        24
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        24
   0.0000E+00   0.0000E+00   1.0000E+00   3.090555E+00   4.620000E+00        1        0    0.000      (t,K)        24

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   3.117705E-35   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    67.103     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      197.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    3.8638   67.103    0.002    0.000    0.000   6.312616E+03     1
               Time of flight (mus) :   3.1564796     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   6.710258E-01   1.892857E-06        1        0    0.000      (Y,T)        24
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        24
   0.0000E+00   0.0000E+00   1.0000E+00   3.156480E+00   4.720000E+00        1        0    0.000      (t,K)        24

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.609134E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    3.9152   67.103    0.002    0.000    0.000   6.312616E+03     1
               Time of flight (mus) :   3.1564796     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   6.710258E-01   1.873069E-06        1        0    0.000      (Y,T)        24
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        24
   0.0000E+00   0.0000E+00   1.0000E+00   3.156480E+00   4.820000E+00        1        0    0.000      (t,K)        24

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.609134E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       24 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    59.955    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      200.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    3.9152   59.955   -0.002    0.000    0.000   6.512197E+03     1
               Time of flight (mus) :   3.2224111     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   5.995460E-01  -1.873075E-06        1        0    0.000      (Y,T)        25
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        25
   0.0000E+00   0.0000E+00   1.0000E+00   3.222411E+00   4.820000E+00        1        0    0.000      (t,K)        25

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.765057E-35   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    68.417     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      201.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    3.9661   68.417    0.002    0.000    0.000   6.713843E+03     1
               Time of flight (mus) :   3.2883494     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   6.841725E-01   1.853468E-06        1        0    0.000      (Y,T)        25
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        25
   0.0000E+00   0.0000E+00   1.0000E+00   3.288349E+00   4.920000E+00        1        0    0.000      (t,K)        25

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   9.084691E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    4.0164   68.417    0.002    0.000    0.000   6.713843E+03     1
               Time of flight (mus) :   3.2883494     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   6.841725E-01   1.834866E-06        1        0    0.000      (Y,T)        25
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        25
   0.0000E+00   0.0000E+00   1.0000E+00   3.288349E+00   5.020000E+00        1        0    0.000      (t,K)        25

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   9.084691E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       25 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    61.256    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      204.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    4.0164   61.256   -0.002    0.000    0.000   6.917533E+03     1
               Time of flight (mus) :   3.3542949     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   6.125608E-01  -1.834871E-06        1        0    0.000      (Y,T)        26
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        26
   0.0000E+00   0.0000E+00   1.0000E+00   3.354295E+00   5.020000E+00        1        0    0.000      (t,K)        26

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.333007E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    69.706     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      205.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    4.0663   69.706    0.002    0.000    0.000   7.123248E+03     1
               Time of flight (mus) :   3.4202473     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   6.970592E-01   1.816339E-06        1        0    0.000      (Y,T)        26
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        26
   0.0000E+00   0.0000E+00   1.0000E+00   3.420247E+00   5.120000E+00        1        0    0.000      (t,K)        26

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.502355E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    4.1156   69.706    0.002    0.000    0.000   7.123248E+03     1
               Time of flight (mus) :   3.4202473     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   6.970592E-01   1.798809E-06        1        0    0.000      (Y,T)        26
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        26
   0.0000E+00   0.0000E+00   1.0000E+00   3.420247E+00   5.220000E+00        1        0    0.000      (t,K)        26

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.502355E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       26 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    62.532    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      208.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    4.1156   62.532   -0.002    0.000    0.000   7.330967E+03     1
               Time of flight (mus) :   3.4862067     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   6.253234E-01  -1.798814E-06        1        0    0.000      (Y,T)        27
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        27
   0.0000E+00   0.0000E+00   1.0000E+00   3.486207E+00   5.220000E+00        1        0    0.000      (t,K)        27

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   5.061238E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    70.970     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      209.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    4.1645   70.970    0.002    0.000    0.000   7.540672E+03     1
               Time of flight (mus) :   3.5521730     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   7.097011E-01   1.781360E-06        1        0    0.000      (Y,T)        27
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        27
   0.0000E+00   0.0000E+00   1.0000E+00   3.552173E+00   5.320000E+00        1        0    0.000      (t,K)        27

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.072080E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    4.2130   70.970    0.002    0.000    0.000   7.540672E+03     1
               Time of flight (mus) :   3.5521730     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   7.097011E-01   1.764803E-06        1        0    0.000      (Y,T)        27
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        27
   0.0000E+00   0.0000E+00   1.0000E+00   3.552173E+00   5.420000E+00        1        0    0.000      (t,K)        27

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.072080E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       27 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    63.785    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      212.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    4.2130   63.785   -0.002    0.000    0.000   7.752345E+03     1
               Time of flight (mus) :   3.6181465     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   6.378480E-01  -1.764807E-06        1        0    0.000      (Y,T)        28
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        28
   0.0000E+00   0.0000E+00   1.0000E+00   3.618146E+00   5.420000E+00        1        0    0.000      (t,K)        28

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   4.915947E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    72.211     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      213.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    4.2610   72.211    0.002    0.000    0.000   7.965967E+03     1
               Time of flight (mus) :   3.6841268     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   7.221117E-01   1.748408E-06        1        0    0.000      (Y,T)        28
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        28
   0.0000E+00   0.0000E+00   1.0000E+00   3.684127E+00   5.520000E+00        1        0    0.000      (t,K)        28

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.128235E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    4.3086   72.211    0.002    0.000    0.000   7.965967E+03     1
               Time of flight (mus) :   3.6841268     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   7.221117E-01   1.732737E-06        1        0    0.000      (Y,T)        28
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        28
   0.0000E+00   0.0000E+00   1.0000E+00   3.684127E+00   5.620000E+00        1        0    0.000      (t,K)        28

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.128235E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       28 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    65.015    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      215.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    4.3086   65.015   -0.002    0.000    0.000   8.181521E+03     1
               Time of flight (mus) :   3.7501142     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   6.501476E-01  -1.732961E-06        1        0    0.000      (Y,T)        29
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        29
   0.0000E+00   0.0000E+00   1.0000E+00   3.750114E+00   5.620000E+00        1        0    0.000      (t,K)        29

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   4.844095E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    73.430     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      217.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    4.3558   73.430    0.002    0.000    0.000   8.398990E+03     1
               Time of flight (mus) :   3.8161085     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   7.343034E-01   1.717542E-06        1        0    0.000      (Y,T)        29
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        29
   0.0000E+00   0.0000E+00   1.0000E+00   3.816109E+00   5.720000E+00        1        0    0.000      (t,K)        29

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   8.474403E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    4.4025   73.430    0.002    0.000    0.000   8.398990E+03     1
               Time of flight (mus) :   3.8161085     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   7.343034E-01   1.702678E-06        1        0    0.000      (Y,T)        29
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        29
   0.0000E+00   0.0000E+00   1.0000E+00   3.816109E+00   5.820000E+00        1        0    0.000      (t,K)        29

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   8.474403E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       29 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    66.223    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      219.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    4.4025   66.223   -0.002    0.000    0.000   8.618358E+03     1
               Time of flight (mus) :   3.8821099     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   6.622341E-01  -1.702780E-06        1        0    0.000      (Y,T)        30
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        30
   0.0000E+00   0.0000E+00   1.0000E+00   3.882110E+00   5.820000E+00        1        0    0.000      (t,K)        30

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   4.725517E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    74.629     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      221.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    4.4489   74.629    0.002    0.000    0.000   8.839607E+03     1
               Time of flight (mus) :   3.9481182     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   7.462874E-01   1.688233E-06        1        0    0.000      (Y,T)        30
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        30
   0.0000E+00   0.0000E+00   1.0000E+00   3.948118E+00   5.920000E+00        1        0    0.000      (t,K)        30

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.173489E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    4.4948   74.629    0.002    0.000    0.000   8.839607E+03     1
               Time of flight (mus) :   3.9481182     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   7.462874E-01   1.674108E-06        1        0    0.000      (Y,T)        30
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        30
   0.0000E+00   0.0000E+00   1.0000E+00   3.948118E+00   6.020000E+00        1        0    0.000      (t,K)        30

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.173489E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       30 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    67.412    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      223.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    4.4948   67.412   -0.002    0.000    0.000   9.062724E+03     1
               Time of flight (mus) :   4.0141336     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   6.741183E-01  -1.674138E-06        1        0    0.000      (Y,T)        31
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        31
   0.0000E+00   0.0000E+00   1.0000E+00   4.014134E+00   6.020000E+00        1        0    0.000      (t,K)        31

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   4.608408E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    75.807     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      224.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    4.5404   75.807    0.002    0.000    0.000   9.287692E+03     1
               Time of flight (mus) :   4.0801558     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   7.580743E-01   1.659736E-06        1        0    0.000      (Y,T)        31
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        31
   0.0000E+00   0.0000E+00   1.0000E+00   4.080156E+00   6.120000E+00        1        0    0.000      (t,K)        31

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.801707E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    4.5857   75.807    0.002    0.000    0.000   9.287692E+03     1
               Time of flight (mus) :   4.0801558     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   7.580743E-01   1.646296E-06        1        0    0.000      (Y,T)        31
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        31
   0.0000E+00   0.0000E+00   1.0000E+00   4.080156E+00   6.220000E+00        1        0    0.000      (t,K)        31

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.801707E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       31 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad

  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    68.581    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      227.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    4.5857   68.581   -0.002    0.000    0.000   9.514497E+03     1
               Time of flight (mus) :   4.1461852     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   6.858102E-01  -1.646300E-06        1        0    0.000      (Y,T)        32
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        32
   0.0000E+00   0.0000E+00   1.0000E+00   4.146185E+00   6.220000E+00        1        0    0.000      (t,K)        32

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.296493E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   76.27 X =    1.00 T = 0.031 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    76.967     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      228.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    4.6305   76.967    0.002    0.000    0.000   9.743124E+03     1
               Time of flight (mus) :   4.2122215     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   7.696734E-01   1.632967E-06        1        0    0.000      (Y,T)        32
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        32
   0.0000E+00   0.0000E+00   1.0000E+00   4.212221E+00   6.320000E+00        1        0    0.000      (t,K)        32

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   4.176796E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    4.6751   76.967    0.002    0.000    0.000   9.743124E+03     1
               Time of flight (mus) :   4.2122215     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   7.696734E-01   1.620157E-06        1        0    0.000      (Y,T)        32
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        32
   0.0000E+00   0.0000E+00   1.0000E+00   4.212221E+00   6.420000E+00        1        0    0.000      (t,K)        32

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   4.176796E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       32 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   76.97 X =   -1.57 T = 0.000 Trajectory #       1 went out of field region.

    Y =   76.22 X =   -0.93 T =-0.029 Trajectory #       1 came back in. field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    69.732    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      230.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    4.6751   69.732   -0.002    0.000    0.000   9.973559E+03     1
               Time of flight (mus) :   4.2782648     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   6.973188E-01  -1.620261E-06        1        0    0.000      (Y,T)        33
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        33
   0.0000E+00   0.0000E+00   1.0000E+00   4.278265E+00   6.420000E+00        1        0    0.000      (t,K)        33

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   3.178537E-35   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   76.25 X =    0.61 T = 0.046 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    78.109     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      232.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    4.7192   78.109    0.002    0.000    0.000   1.020579E+04     1
               Time of flight (mus) :   4.3443150     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   7.810937E-01   1.607703E-06        1        0    0.000      (Y,T)        33
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        33
   0.0000E+00   0.0000E+00   1.0000E+00   4.344315E+00   6.520000E+00        1        0    0.000      (t,K)        33

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.061474E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    4.7631   78.109    0.002    0.000    0.000   1.020579E+04     1
               Time of flight (mus) :   4.3443150     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   7.810937E-01   1.595471E-06        1        0    0.000      (Y,T)        33
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        33
   0.0000E+00   0.0000E+00   1.0000E+00   4.344315E+00   6.620000E+00        1        1    1.000      (t,K)        33

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.061474E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       33 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   78.11 X =   -1.57 T = 0.000 Trajectory #       1 went out of field region.

    Y =   76.24 X =   -0.53 T =-0.042 Trajectory #       1 came back in. field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    70.865    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      234.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    4.7631   70.865   -0.002    0.000    0.000   1.043980E+04     1
               Time of flight (mus) :   4.4103724     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   7.086528E-01  -1.595484E-06        1        0    0.000      (Y,T)        34
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        34
   0.0000E+00   0.0000E+00   1.0000E+00   4.410372E+00   6.620000E+00        1        1    1.000      (t,K)        34

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.366877E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   76.26 X =    0.32 T = 0.053 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    79.234     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      235.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    4.8066   79.234    0.002    0.000    0.000   1.067557E+04     1
               Time of flight (mus) :   4.4764366     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   7.923432E-01   1.583237E-06        1        0    0.000      (Y,T)        34
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        34
   0.0000E+00   0.0000E+00   1.0000E+00   4.476437E+00   6.720000E+00        1        0    0.000      (t,K)        34

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.088410E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    4.8498   79.234    0.002    0.000    0.000   1.067557E+04     1
               Time of flight (mus) :   4.4764366     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   7.923432E-01   1.571545E-06        1        0    0.000      (Y,T)        34
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        34
   0.0000E+00   0.0000E+00   1.0000E+00   4.476437E+00   6.820000E+00        1        0    0.000      (t,K)        34

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.088410E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       34 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   79.23 X =   -1.57 T = 0.000 Trajectory #       1 went out of field region.

    Y =   76.21 X =   -0.19 T =-0.047 Trajectory #       1 came back in. field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    71.982    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      237.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    4.8498   71.982   -0.002    0.000    0.000   1.091310E+04     1
               Time of flight (mus) :   4.5425079     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   7.198198E-01  -1.571672E-06        1        0    0.000      (Y,T)        35
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        35
   0.0000E+00   0.0000E+00   1.0000E+00   4.542508E+00   6.820000E+00        1        0    0.000      (t,K)        35

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.829425E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   76.30 X =    0.06 T = 0.055 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    80.343     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      239.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    4.8927   80.343    0.002    0.000    0.000   1.115237E+04     1
               Time of flight (mus) :   4.6085861     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   8.034296E-01   1.560189E-06        1        0    0.000      (Y,T)        35
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        35
   0.0000E+00   0.0000E+00   1.0000E+00   4.608586E+00   6.920000E+00        1        0    0.000      (t,K)        35

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   9.019451E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    4.9353   80.343    0.002    0.000    0.000   1.115237E+04     1
               Time of flight (mus) :   4.6085861     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   8.034296E-01   1.548996E-06        1        0    0.000      (Y,T)        35
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        35
   0.0000E+00   0.0000E+00   1.0000E+00   4.608586E+00   7.020000E+00        1        0    0.000      (t,K)        35

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   9.019451E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       35 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   80.34 X =   -1.57 T = 0.000 Trajectory #       1 went out of field region.

    Y =   76.21 X =    0.12 T =-0.047 Trajectory #       1 came back in. field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    73.083    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      241.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    4.9353   73.083   -0.002    0.000    0.000   1.139337E+04     1
               Time of flight (mus) :   4.6746714     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   7.308272E-01  -1.549006E-06        1        0    0.000      (Y,T)        36
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        36
   0.0000E+00   0.0000E+00   1.0000E+00   4.674671E+00   7.020000E+00        1        0    0.000      (t,K)        36

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   4.022836E-35   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   76.28 X =   -0.21 T = 0.053 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    81.436     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      242.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    4.9776   81.436    0.002    0.000    0.000   1.163609E+04     1
               Time of flight (mus) :   4.7407635     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   8.143597E-01   1.537839E-06        1        0    0.000      (Y,T)        36
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        36
   0.0000E+00   0.0000E+00   1.0000E+00   4.740764E+00   7.120000E+00        1        0    0.000      (t,K)        36

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.531916E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    5.0196   81.436    0.002    0.000    0.000   1.163609E+04     1
               Time of flight (mus) :   4.7407635     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   8.143597E-01   1.527111E-06        1        0    0.000      (Y,T)        36
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        36
   0.0000E+00   0.0000E+00   1.0000E+00   4.740764E+00   7.220000E+00        1        0    0.000      (t,K)        36

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.531916E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       36 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   81.44 X =   -1.57 T = 0.000 Trajectory #       1 went out of field region.

    Y =   76.22 X =    0.43 T =-0.042 Trajectory #       1 came back in. field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    74.168    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      244.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    5.0196   74.168   -0.002    0.000    0.000   1.188051E+04     1
               Time of flight (mus) :   4.8068628     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   7.416817E-01  -1.527186E-06        1        0    0.000      (Y,T)        37
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        37
   0.0000E+00   0.0000E+00   1.0000E+00   4.806863E+00   7.220000E+00        1        0    0.000      (t,K)        37

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   4.044905E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   76.29 X =   -0.49 T = 0.047 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    82.514     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      245.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    5.0613   82.514    0.002    0.000    0.000   1.212663E+04     1
               Time of flight (mus) :   4.8729690     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   8.251401E-01   1.516100E-06        1        0    0.000      (Y,T)        37
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        37
   0.0000E+00   0.0000E+00   1.0000E+00   4.872969E+00   7.320000E+00        1        0    0.000      (t,K)        37

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.548937E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    5.1027   82.514    0.002    0.000    0.000   1.212663E+04     1
               Time of flight (mus) :   4.8729690     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   8.251401E-01   1.505809E-06        1        0    0.000      (Y,T)        37
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        37
   0.0000E+00   0.0000E+00   1.0000E+00   4.872969E+00   7.420000E+00        1        0    0.000      (t,K)        37

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.548937E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       37 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   82.51 X =   -1.57 T = 0.000 Trajectory #       1 went out of field region.

    Y =   76.24 X =    0.79 T =-0.032 Trajectory #       1 came back in. field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    75.239    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      247.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    5.1027   75.239   -0.002    0.000    0.000   1.237443E+04     1
               Time of flight (mus) :   4.9390822     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   7.523896E-01  -1.506050E-06        1        0    0.000      (Y,T)        38
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        38
   0.0000E+00   0.0000E+00   1.0000E+00   4.939082E+00   7.420000E+00        1        0    0.000      (t,K)        38

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.386988E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   76.28 X =   -0.83 T = 0.035 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    83.578     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      249.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    5.1438   83.578    0.001    0.000    0.000   1.262390E+04     1
               Time of flight (mus) :   5.0052024     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   8.357770E-01   1.495884E-06        1        0    0.000      (Y,T)        38
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        38
   0.0000E+00   0.0000E+00   1.0000E+00   5.005202E+00   7.520000E+00        1        1    1.000      (t,K)        38

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.799369E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    5.1847   83.578    0.001    0.000    0.000   1.262390E+04     1
               Time of flight (mus) :   5.0052024     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   8.357770E-01   1.485996E-06        1        0    0.000      (Y,T)        38
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        38
   0.0000E+00   0.0000E+00   1.0000E+00   5.005202E+00   7.620000E+00        1        0    0.000      (t,K)        38

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.799369E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       38 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   83.58 X =   -1.57 T = 0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    76.296    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      251.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    5.1847   76.296   -0.001    0.000    0.000   1.287502E+04     1
               Time of flight (mus) :   5.0713296     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   7.629567E-01  -1.486014E-06        1        0    0.000      (Y,T)        39
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        39
   0.0000E+00   0.0000E+00   1.0000E+00   5.071330E+00   7.620000E+00        1        0    0.000      (t,K)        39

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   3.599951E-36   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   76.30 X =   -1.57 T =-0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    84.628     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      252.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    5.2253   84.628    0.001    0.000    0.000   1.312780E+04     1
               Time of flight (mus) :   5.1374637     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   8.462759E-01   1.476113E-06        1        0    0.000      (Y,T)        39
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        39
   0.0000E+00   0.0000E+00   1.0000E+00   5.137464E+00   7.720000E+00        1        0    0.000      (t,K)        39

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   9.581484E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    5.2657   84.628    0.001    0.000    0.000   1.312780E+04     1
               Time of flight (mus) :   5.1374637     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   8.462759E-01   1.466606E-06        1        0    0.000      (Y,T)        39
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        39
   0.0000E+00   0.0000E+00   1.0000E+00   5.137464E+00   7.820000E+00        1        1    1.000      (t,K)        39

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   9.581484E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       39 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   84.63 X =   -1.57 T = 0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    77.339    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      254.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    5.2657   77.339   -0.001    0.000    0.000   1.338222E+04     1
               Time of flight (mus) :   5.2036050     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   7.733887E-01  -1.466665E-06        1        0    0.000      (Y,T)        40
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        40
   0.0000E+00   0.0000E+00   1.0000E+00   5.203605E+00   7.820000E+00        1        1    1.000      (t,K)        40

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.072098E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   77.34 X =   -1.57 T =-0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    85.664     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      255.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    5.3058   85.664    0.001    0.000    0.000   1.363826E+04     1
               Time of flight (mus) :   5.2697531     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   8.566421E-01   1.456940E-06        1        0    0.000      (Y,T)        40
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        40
   0.0000E+00   0.0000E+00   1.0000E+00   5.269753E+00   7.920000E+00        1        0    0.000      (t,K)        40

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   7.807927E-35   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    5.3456   85.664    0.001    0.000    0.000   1.363826E+04     1
               Time of flight (mus) :   5.2697531     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   8.566421E-01   1.447790E-06        1        0    0.000      (Y,T)        40
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        40
   0.0000E+00   0.0000E+00   1.0000E+00   5.269753E+00   8.020000E+00        1        0    0.000      (t,K)        40

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   7.807927E-35   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       40 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   85.66 X =   -1.57 T = 0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    78.369    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      257.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    5.3456   78.369   -0.001    0.000    0.000   1.389593E+04     1
               Time of flight (mus) :   5.3359083     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   7.836905E-01  -1.447922E-06        1        0    0.000      (Y,T)        41
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        41
   0.0000E+00   0.0000E+00   1.0000E+00   5.335908E+00   8.020000E+00        1        0    0.000      (t,K)        41

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   8.792962E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   78.37 X =   -1.57 T =-0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    86.688     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      259.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    5.3852   86.688    0.001    0.000    0.000   1.415520E+04     1
               Time of flight (mus) :   5.4020703     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   8.668807E-01   1.438911E-06        1        0    0.000      (Y,T)        41
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        41
   0.0000E+00   0.0000E+00   1.0000E+00   5.402070E+00   8.120000E+00        1        0    0.000      (t,K)        41

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   5.061238E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    5.4246   86.688    0.001    0.000    0.000   1.415520E+04     1
               Time of flight (mus) :   5.4020703     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   8.668807E-01   1.430094E-06        1        0    0.000      (Y,T)        41
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        41
   0.0000E+00   0.0000E+00   1.0000E+00   5.402070E+00   8.220000E+00        1        0    0.000      (t,K)        41

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   5.061238E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       41 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   86.69 X =   -1.57 T = 0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    79.387    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      260.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    5.4246   79.387   -0.001    0.000    0.000   1.441607E+04     1
               Time of flight (mus) :   5.4682395     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   7.938671E-01  -1.430322E-06        1        0    0.000      (Y,T)        42
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        42
   0.0000E+00   0.0000E+00   1.0000E+00   5.468240E+00   8.220000E+00        1        0    0.000      (t,K)        42

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.043886E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   79.39 X =   -1.57 T =-0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    87.700     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      262.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    5.4637   87.700    0.001    0.000    0.000   1.467853E+04     1
               Time of flight (mus) :   5.5344156     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   8.769964E-01   1.421603E-06        1        0    0.000      (Y,T)        42
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        42
   0.0000E+00   0.0000E+00   1.0000E+00   5.534416E+00   8.320000E+00        1        0    0.000      (t,K)        42

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.787908E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    5.5026   87.700    0.001    0.000    0.000   1.467853E+04     1
               Time of flight (mus) :   5.5344156     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   8.769964E-01   1.413099E-06        1        0    0.000      (Y,T)        42
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        42
   0.0000E+00   0.0000E+00   1.0000E+00   5.534416E+00   8.420000E+00        1        0    0.000      (t,K)        42

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.787908E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       42 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   87.70 X =   -1.57 T = 0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    80.392    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      263.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    5.5026   80.392   -0.001    0.000    0.000   1.494256E+04     1
               Time of flight (mus) :   5.6005988     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   8.039229E-01  -1.413435E-06        1        0    0.000      (Y,T)        43
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        43
   0.0000E+00   0.0000E+00   1.0000E+00   5.600599E+00   8.420000E+00        1        0    0.000      (t,K)        43

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.487346E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   80.39 X =   -1.57 T =-0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    88.699     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      265.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    5.5413   88.699    0.001    0.000    0.000   1.520817E+04     1
               Time of flight (mus) :   5.6667888     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   8.869935E-01   1.404984E-06        1        0    0.000      (Y,T)        43
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        43
   0.0000E+00   0.0000E+00   1.0000E+00   5.666789E+00   8.520000E+00        1        0    0.000      (t,K)        43

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.211241E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    5.5798   88.699    0.001    0.000    0.000   1.520817E+04     1
               Time of flight (mus) :   5.6667888     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   8.869935E-01   1.396773E-06        1        0    0.000      (Y,T)        43
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        43
   0.0000E+00   0.0000E+00   1.0000E+00   5.666789E+00   8.620000E+00        1        0    0.000      (t,K)        43

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.211241E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       43 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   88.70 X =   -1.57 T = 0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    81.386    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      267.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    5.5798   81.386   -0.001    0.000    0.000   1.547534E+04     1
               Time of flight (mus) :   5.7329860     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   8.138623E-01  -1.396789E-06        1        0    0.000      (Y,T)        44
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        44
   0.0000E+00   0.0000E+00   1.0000E+00   5.732986E+00   8.620000E+00        1        0    0.000      (t,K)        44

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   4.561976E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   81.39 X =   -1.57 T =-0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    89.688     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      268.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    5.6180   89.688    0.001    0.000    0.000   1.574406E+04     1
               Time of flight (mus) :   5.7991900     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   8.968762E-01   1.388592E-06        1        0    0.000      (Y,T)        44
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        44
   0.0000E+00   0.0000E+00   1.0000E+00   5.799190E+00   8.720000E+00        1        0    0.000      (t,K)        44

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.938736E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    5.6560   89.688    0.001    0.000    0.000   1.574406E+04     1
               Time of flight (mus) :   5.7991900     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   8.968762E-01   1.380661E-06        1        0    0.000      (Y,T)        44
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        44
   0.0000E+00   0.0000E+00   1.0000E+00   5.799190E+00   8.820000E+00        1        0    0.000      (t,K)        44

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.938736E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       44 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   89.69 X =   -1.57 T = 0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    82.369    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      270.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    5.6560   82.369   -0.001    0.000    0.000   1.601433E+04     1
               Time of flight (mus) :   5.8654011     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   8.236893E-01  -1.380684E-06        1        0    0.000      (Y,T)        45
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        45
   0.0000E+00   0.0000E+00   1.0000E+00   5.865401E+00   8.820000E+00        1        0    0.000      (t,K)        45

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.941006E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   82.37 X =   -1.57 T =-0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    90.665     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      271.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    5.6938   90.665    0.001    0.000    0.000   1.628613E+04     1
               Time of flight (mus) :   5.9316191     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   9.066483E-01   1.372734E-06        1        0    0.000      (Y,T)        45
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        45
   0.0000E+00   0.0000E+00   1.0000E+00   5.931619E+00   8.920000E+00        1        1    1.000      (t,K)        45

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.962293E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    5.7314   90.665    0.001    0.000    0.000   1.628613E+04     1
               Time of flight (mus) :   5.9316191     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   9.066483E-01   1.365067E-06        1        0    0.000      (Y,T)        45
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        45
   0.0000E+00   0.0000E+00   1.0000E+00   5.931619E+00   9.020000E+00        1        0    0.000      (t,K)        45

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.962293E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       45 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   90.66 X =   -1.57 T = 0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    83.341    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      273.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    5.7314   83.341   -0.001    0.000    0.000   1.655946E+04     1
               Time of flight (mus) :   5.9978442     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   8.334076E-01  -1.365096E-06        1        0    0.000      (Y,T)        46
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        46
   0.0000E+00   0.0000E+00   1.0000E+00   5.997844E+00   9.020000E+00        1        0    0.000      (t,K)        46

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.631072E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   83.34 X =   -1.57 T =-0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    91.631     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      274.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    5.7688   91.631    0.001    0.000    0.000   1.683430E+04     1
               Time of flight (mus) :   6.0640762     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   9.163137E-01   1.357391E-06        1        0    0.000      (Y,T)        46
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        46
   0.0000E+00   0.0000E+00   1.0000E+00   6.064076E+00   9.120000E+00        1        0    0.000      (t,K)        46

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   5.759922E-35   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    5.8060   91.631    0.001    0.000    0.000   1.683430E+04     1
               Time of flight (mus) :   6.0640762     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   9.163137E-01   1.349974E-06        1        0    0.000      (Y,T)        46
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        46
   0.0000E+00   0.0000E+00   1.0000E+00   6.064076E+00   9.220000E+00        1        0    0.000      (t,K)        46

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   5.759922E-35   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       46 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   91.63 X =   -1.57 T = 0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    84.302    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      276.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    5.8060   84.302   -0.001    0.000    0.000   1.711066E+04     1
               Time of flight (mus) :   6.1303153     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   8.430208E-01  -1.350005E-06        1        0    0.000      (Y,T)        47
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        47
   0.0000E+00   0.0000E+00   1.0000E+00   6.130315E+00   9.220000E+00        1        0    0.000      (t,K)        47

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.245439E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   84.30 X =   -1.57 T =-0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    92.588     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      277.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    5.8430   92.588    0.001    0.000    0.000   1.738852E+04     1
               Time of flight (mus) :   6.1965613     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   9.258757E-01   1.342546E-06        1        0    0.000      (Y,T)        47
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        47
   0.0000E+00   0.0000E+00   1.0000E+00   6.196561E+00   9.320000E+00        1        0    0.000      (t,K)        47

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   5.435486E-35   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    5.8798   92.588    0.001    0.000    0.000   1.738852E+04     1
               Time of flight (mus) :   6.1965613     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   9.258757E-01   1.335366E-06        1        0    0.000      (Y,T)        47
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        47
   0.0000E+00   0.0000E+00   1.0000E+00   6.196561E+00   9.420000E+00        1        0    0.000      (t,K)        47

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   5.435486E-35   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       47 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   92.59 X =   -1.57 T = 0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    85.253    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      279.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    5.8798   85.253   -0.001    0.000    0.000   1.766787E+04     1
               Time of flight (mus) :   6.2628144     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   8.525325E-01  -1.335394E-06        1        0    0.000      (Y,T)        48
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        48
   0.0000E+00   0.0000E+00   1.0000E+00   6.262814E+00   9.420000E+00        1        0    0.000      (t,K)        48

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.385669E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   85.25 X =   -1.57 T =-0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    93.534     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      280.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    5.9164   93.534    0.001    0.000    0.000   1.794871E+04     1
               Time of flight (mus) :   6.3290743     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   9.353377E-01   1.328183E-06        1        0    0.000      (Y,T)        48
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        48
   0.0000E+00   0.0000E+00   1.0000E+00   6.329074E+00   9.520000E+00        1        1    1.000      (t,K)        48

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   3.787472E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    5.9528   93.534    0.001    0.000    0.000   1.794871E+04     1
               Time of flight (mus) :   6.3290743     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   9.353377E-01   1.321226E-06        1        0    0.000      (Y,T)        48
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        48
   0.0000E+00   0.0000E+00   1.0000E+00   6.329074E+00   9.620000E+00        1        0    0.000      (t,K)        48

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   3.787472E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       48 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   93.53 X =   -1.57 T = 0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    86.195    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      282.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    5.9528   86.195   -0.001    0.000    0.000   1.823102E+04     1
               Time of flight (mus) :   6.3953414     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   8.619456E-01  -1.321250E-06        1        0    0.000      (Y,T)        49
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        49
   0.0000E+00   0.0000E+00   1.0000E+00   6.395341E+00   9.620000E+00        1        0    0.000      (t,K)        49

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   8.195840E-35   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   86.19 X =   -1.57 T =-0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    94.470     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      283.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    5.9890   94.470    0.001    0.000    0.000   1.851481E+04     1
               Time of flight (mus) :   6.4616153     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   9.447029E-01   1.314282E-06        1        0    0.000      (Y,T)        49
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        49
   0.0000E+00   0.0000E+00   1.0000E+00   6.461615E+00   9.720000E+00        1        0    0.000      (t,K)        49

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.320455E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    6.0251   94.470    0.001    0.000    0.000   1.851481E+04     1
               Time of flight (mus) :   6.4616153     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   9.447029E-01   1.307539E-06        1        0    0.000      (Y,T)        49
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        49
   0.0000E+00   0.0000E+00   1.0000E+00   6.461615E+00   9.820000E+00        1        0    0.000      (t,K)        49

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.320455E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       49 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   94.47 X =   -1.57 T = 0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    87.126    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      284.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    6.0251   87.126   -0.001    0.000    0.000   1.880006E+04     1
               Time of flight (mus) :   6.5278964     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   8.712635E-01  -1.307919E-06        1        0    0.000      (Y,T)        50
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        50
   0.0000E+00   0.0000E+00   1.0000E+00   6.527896E+00   9.820000E+00        1        0    0.000      (t,K)        50

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.566052E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   87.13 X =   -1.57 T =-0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    95.397     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      286.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    6.0609   95.397    0.001    0.000    0.000   1.908677E+04     1
               Time of flight (mus) :   6.5941843     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   9.539741E-01   1.301186E-06        1        0    0.000      (Y,T)        50
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        50
   0.0000E+00   0.0000E+00   1.0000E+00   6.594184E+00   9.920000E+00        1        1    1.000      (t,K)        50

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   4.044905E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    6.0966   95.397    0.001    0.000    0.000   1.908677E+04     1
               Time of flight (mus) :   6.5941843     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   9.539741E-01   1.294643E-06        1        0    0.000      (Y,T)        50
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        50
   0.0000E+00   0.0000E+00   1.0000E+00   6.594184E+00   1.002000E+01        1        0    0.000      (t,K)        50

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   4.044905E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       50 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   95.40 X =   -1.57 T = 0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    88.049    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      287.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    6.0966   88.049   -0.001    0.000    0.000   1.937493E+04     1
               Time of flight (mus) :   6.6604793     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   8.804888E-01  -1.294933E-06        1        0    0.000      (Y,T)        51
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        51
   0.0000E+00   0.0000E+00   1.0000E+00   6.660479E+00   1.002000E+01        1        0    0.000      (t,K)        51

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.444276E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   88.05 X =   -1.57 T =-0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    96.315     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      289.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    6.1321   96.315    0.001    0.000    0.000   1.966453E+04     1
               Time of flight (mus) :   6.7267812     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   9.631542E-01   1.288425E-06        1        0    0.000      (Y,T)        51
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        51
   0.0000E+00   0.0000E+00   1.0000E+00   6.726781E+00   1.012000E+01        1        0    0.000      (t,K)        51

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   4.518600E-35   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    6.1675   96.315    0.001    0.000    0.000   1.966453E+04     1
               Time of flight (mus) :   6.7267812     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   9.631542E-01   1.282073E-06        1        0    0.000      (Y,T)        51
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        51
   0.0000E+00   0.0000E+00   1.0000E+00   6.726781E+00   1.022000E+01        1        0    0.000      (t,K)        51

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   4.518600E-35   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       51 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   96.32 X =   -1.57 T = 0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    88.962    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      290.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    6.1675   88.962   -0.001    0.000    0.000   1.995556E+04     1
               Time of flight (mus) :   6.7930902     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   8.896244E-01  -1.282277E-06        1        0    0.000      (Y,T)        52
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        52
   0.0000E+00   0.0000E+00   1.0000E+00   6.793090E+00   1.022000E+01        1        0    0.000      (t,K)        52

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.312106E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   88.96 X =   -1.57 T =-0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    97.225     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      291.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    6.2027   97.225    0.001    0.000    0.000   2.024802E+04     1
               Time of flight (mus) :   6.8594061     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   9.722460E-01   1.275487E-06        1        0    0.000      (Y,T)        52
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        52
   0.0000E+00   0.0000E+00   1.0000E+00   6.859406E+00   1.032000E+01        1        0    0.000      (t,K)        52

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.580119E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    6.2377   97.225    0.001    0.000    0.000   2.024802E+04     1
               Time of flight (mus) :   6.8594061     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   9.722460E-01   1.269319E-06        1        0    0.000      (Y,T)        52
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        52
   0.0000E+00   0.0000E+00   1.0000E+00   6.859406E+00   1.042000E+01        1        0    0.000      (t,K)        52

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.580119E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       52 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   97.22 X =   -1.57 T = 0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    89.867    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      293.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    6.2377   89.867   -0.001    0.000    0.000   2.054190E+04     1
               Time of flight (mus) :   6.9257291     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   8.986729E-01  -1.269449E-06        1        0    0.000      (Y,T)        53
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        53
   0.0000E+00   0.0000E+00   1.0000E+00   6.925729E+00   1.042000E+01        1        0    0.000      (t,K)        53

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.331039E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   89.87 X =   -1.57 T =-0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    98.125     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      294.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    6.2725   98.125    0.001    0.000    0.000   2.083720E+04     1
               Time of flight (mus) :   6.9920590     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   9.812519E-01   1.263014E-06        1        0    0.000      (Y,T)        53
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        53
   0.0000E+00   0.0000E+00   1.0000E+00   6.992059E+00   1.052000E+01        1        0    0.000      (t,K)        53

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.140494E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    6.3072   98.125    0.001    0.000    0.000   2.083720E+04     1
               Time of flight (mus) :   6.9920590     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   9.812519E-01   1.257020E-06        1        0    0.000      (Y,T)        53
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        53
   0.0000E+00   0.0000E+00   1.0000E+00   6.992059E+00   1.062000E+01        1        0    0.000      (t,K)        53

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.140494E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       53 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   98.13 X =   -1.57 T = 0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    90.764    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      296.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    6.3072   90.764   -0.001    0.000    0.000   2.113391E+04     1
               Time of flight (mus) :   7.0583959     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   9.076368E-01  -1.257094E-06        1        0    0.000      (Y,T)        54
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        54
   0.0000E+00   0.0000E+00   1.0000E+00   7.058396E+00   1.062000E+01        1        0    0.000      (t,K)        54

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   7.703720E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   90.76 X =   -1.57 T =-0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    99.017     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      297.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    6.3417   99.017    0.001    0.000    0.000   2.143202E+04     1
               Time of flight (mus) :   7.1247398     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   9.901744E-01   1.250967E-06        1        0    0.000      (Y,T)        54
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        54
   0.0000E+00   0.0000E+00   1.0000E+00   7.124740E+00   1.072000E+01        1        0    0.000      (t,K)        54

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   3.009689E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    6.3761   99.017    0.001    0.000    0.000   2.143202E+04     1
               Time of flight (mus) :   7.1247398     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   9.901744E-01   1.245140E-06        1        0    0.000      (Y,T)        54
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        54
   0.0000E+00   0.0000E+00   1.0000E+00   7.124740E+00   1.082000E+01        1        0    0.000      (t,K)        54

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   3.009689E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       54 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   99.02 X =   -1.57 T = 0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    91.652    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      299.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    6.3761   91.652   -0.001    0.000    0.000   2.173152E+04     1
               Time of flight (mus) :   7.1910907     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   9.165185E-01  -1.245176E-06        1        0    0.000      (Y,T)        55
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        55
   0.0000E+00   0.0000E+00   1.0000E+00   7.191091E+00   1.082000E+01        1        0    0.000      (t,K)        55

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   2.142338E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   91.65 X =   -1.57 T =-0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    99.902     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      300.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    6.4103   99.902    0.001    0.000    0.000   2.203241E+04     1
               Time of flight (mus) :   7.2574486     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   9.990158E-01   1.239310E-06        1        0    0.000      (Y,T)        55
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        55
   0.0000E+00   0.0000E+00   1.0000E+00   7.257449E+00   1.092000E+01        1        0    0.000      (t,K)        55

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.485593E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    6.4443   99.902    0.001    0.000    0.000   2.203241E+04     1
               Time of flight (mus) :   7.2574486     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   9.990158E-01   1.233641E-06        1        0    0.000      (Y,T)        55
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        55
   0.0000E+00   0.0000E+00   1.0000E+00   7.257449E+00   1.102000E+01        1        0    0.000      (t,K)        55

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.485593E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       55 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   99.90 X =   -1.57 T = 0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    92.532    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      301.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    6.4443   92.532   -0.001    0.000    0.000   2.233468E+04     1
               Time of flight (mus) :   7.3238135     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   9.253203E-01  -1.233941E-06        1        0    0.000      (Y,T)        56
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        56
   0.0000E+00   0.0000E+00   1.0000E+00   7.323814E+00   1.102000E+01        1        0    0.000      (t,K)        56

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   4.288321E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   92.53 X =   -1.57 T =-0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571   100.778     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      303.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    6.4782  100.778    0.001    0.000    0.000   2.263833E+04     1
               Time of flight (mus) :   7.3901853     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   1.007778E+00   1.228292E-06        1        0    0.000      (Y,T)        56
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        56
   0.0000E+00   0.0000E+00   1.0000E+00   7.390185E+00   1.112000E+01        1        0    0.000      (t,K)        56

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.057945E-35   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    6.5120  100.778    0.001    0.000    0.000   2.263833E+04     1
               Time of flight (mus) :   7.3901853     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   1.007778E+00   1.222773E-06        1        0    0.000      (Y,T)        56
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        56
   0.0000E+00   0.0000E+00   1.0000E+00   7.390185E+00   1.122000E+01        1        0    0.000      (t,K)        56

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.057945E-35   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       56 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =  100.78 X =   -1.57 T = 0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    93.404    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      304.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    6.5120   93.404   -0.001    0.000    0.000   2.294335E+04     1
               Time of flight (mus) :   7.4565642     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   9.340443E-01  -1.222930E-06        1        0    0.000      (Y,T)        57
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        57
   0.0000E+00   0.0000E+00   1.0000E+00   7.456564E+00   1.122000E+01        1        0    0.000      (t,K)        57

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   3.787472E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   93.40 X =   -1.57 T =-0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571   101.646     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      305.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    6.5456  101.646    0.001    0.000    0.000   2.324974E+04     1
               Time of flight (mus) :   7.5229500     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   1.016464E+00   1.217127E-06        1        0    0.000      (Y,T)        57
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        57
   0.0000E+00   0.0000E+00   1.0000E+00   7.522950E+00   1.132000E+01        1        0    0.000      (t,K)        57

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   3.215318E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    6.5790  101.646    0.001    0.000    0.000   2.324974E+04     1
               Time of flight (mus) :   7.5229500     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   1.016464E+00   1.211755E-06        1        0    0.000      (Y,T)        57
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        57
   0.0000E+00   0.0000E+00   1.0000E+00   7.522950E+00   1.142000E+01        1        0    0.000      (t,K)        57

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   3.215318E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       57 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =  101.65 X =   -1.57 T = 0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    94.269    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      307.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    6.5790   94.269   -0.001    0.000    0.000   2.355748E+04     1
               Time of flight (mus) :   7.5893429     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   9.426925E-01  -1.211824E-06        1        0    0.000      (Y,T)        58
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        58
   0.0000E+00   0.0000E+00   1.0000E+00   7.589343E+00   1.142000E+01        1        0    0.000      (t,K)        58

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   3.851037E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   94.27 X =   -1.57 T =-0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571   102.508     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      308.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    6.6123  102.508    0.001    0.000    0.000   2.386658E+04     1
               Time of flight (mus) :   7.6557427     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   1.025075E+00   1.206345E-06        1        0    0.000      (Y,T)        58
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        58
   0.0000E+00   0.0000E+00   1.0000E+00   7.655743E+00   1.152000E+01        1        0    0.000      (t,K)        58

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.022974E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    6.6455  102.508    0.001    0.000    0.000   2.386658E+04     1
               Time of flight (mus) :   7.6557427     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   1.025075E+00   1.201111E-06        1        0    0.000      (Y,T)        58
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        58
   0.0000E+00   0.0000E+00   1.0000E+00   7.655743E+00   1.162000E+01        1        0    0.000      (t,K)        58

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.022974E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       58 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =  102.51 X =   -1.57 T = 0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    95.127    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      309.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    6.6455   95.127   -0.001    0.000    0.000   2.417702E+04     1
               Time of flight (mus) :   7.7221496     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   9.512671E-01  -1.201488E-06        1        0    0.000      (Y,T)        59
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        59
   0.0000E+00   0.0000E+00   1.0000E+00   7.722150E+00   1.162000E+01        1        0    0.000      (t,K)        59

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   4.243535E-36   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   95.13 X =   -1.57 T =-0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571   103.361     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      311.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    6.6785  103.361    0.001    0.000    0.000   2.448881E+04     1
               Time of flight (mus) :   7.7885633     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   1.033614E+00   1.196246E-06        1        0    0.000      (Y,T)        59
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        59
   0.0000E+00   0.0000E+00   1.0000E+00   7.788563E+00   1.172000E+01        1        0    0.000      (t,K)        59

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   3.415058E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    6.7114  103.361    0.001    0.000    0.000   2.448881E+04     1
               Time of flight (mus) :   7.7885633     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   1.033614E+00   1.191144E-06        1        0    0.000      (Y,T)        59
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        59
   0.0000E+00   0.0000E+00   1.0000E+00   7.788563E+00   1.182000E+01        1        1    1.000      (t,K)        59

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   3.415058E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       59 through the optical structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =  103.36 X =   -1.57 T = 0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    95.977    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      312.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    6.7114   95.977   -0.001    0.000    0.000   2.480193E+04     1
               Time of flight (mus) :   7.8549842     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   9.597698E-01  -1.191319E-06        1        0    0.000      (Y,T)        60
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        60
   0.0000E+00   0.0000E+00   1.0000E+00   7.854984E+00   1.182000E+01        1        1    1.000      (t,K)        60

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.107892E-33   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   95.98 X =   -1.57 T =-0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571   104.208     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      313.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    6.7442  104.208    0.001    0.000    0.000   2.511638E+04     1
               Time of flight (mus) :   7.9214119     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   1.042081E+00   1.185921E-06        1        0    0.000      (Y,T)        60
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        60
   0.0000E+00   0.0000E+00   1.0000E+00   7.921412E+00   1.192000E+01        1        0    0.000      (t,K)        60

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   9.348001E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    6.7768  104.208    0.001    0.000    0.000   2.511638E+04     1
               Time of flight (mus) :   7.9214119     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   1.042081E+00   1.180947E-06        1        0    0.000      (Y,T)        60
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        60
   0.0000E+00   0.0000E+00   1.0000E+00   7.921412E+00   1.202000E+01        1        0    0.000      (t,K)        60

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   9.348001E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                                -----  REBELOTE  -----

     End of pass #       60 through the optical structure 

                     Total of          1 particles have been launched


      Next  pass  is  #    61 and  last  pass  through  the  optical  structure


************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  FAISTORE                                              


     zgoubi.fai                                                                      
      already open...
               Print will occur at element[s] labeled : 
                    #End                

************************************************************************************************************************************
      4  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =  104.21 X =   -1.57 T = 0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571    96.820    -0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      315.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      5  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      4)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    6.7768   96.820   -0.001    0.000    0.000   2.543215E+04     1
               Time of flight (mus) :   7.9878468     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   9.682025E-01  -1.181014E-06        1        0    0.000      (Y,T)        61
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        61
   0.0000E+00   0.0000E+00   1.0000E+00   7.987847E+00   1.202000E+01        1        0    0.000      (t,K)        61

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.199122E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      6  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA                                                 


     zgoubi.plt                                                                      
      already open...

     NDIM =   2 ;  Number of data file sets used is   1 ;  Stored in field array # IMAP =    1 ;  
     Value of MOD.MOD2 is 22.1

           3-D map. MOD=22 or 23.  Single field map, with field coefficient value : 
                 1.000000E+00
          No  new  map  file  to  be  opened. Already  stored.
          Skip  reading  field  map  file :           geneSectorMap_180deg.out
 Pgm toscap,  restored mesh coordinates for field map #   1,  name : geneSectorMap_180deg.out


     Field map limits, angle :  min, max, max-min (rad) :  -0.15707963E+01   0.15707963E+01   0.31415927E+01
     Field map limits, radius :  min, max, max-min (cm) :   0.10000000E+01   0.76000000E+02   0.75000000E+02
      Min / max  fields  drawn  from  map  data :    0.0000                     /    5.0000    
       @  X-node, Y-node, z-node :                0.00      1.00      0.00      /   0.00      1.00      0.00    
     Normalisation  coeff.  BNORM   :   1.00000    
     Field  min/max  normalised  :                0.0000        /    5.0000    
     Nbre  of  nodes  in  X/Y/Z :  315/ 151/   1
     Node  distance  in   X/Y/Z :   1.000507E-02/  0.500000    /   0.00000    

                     Option  for  interpolation : 2
                     Smoothing  using  9  points 

                    Integration step :   1.000     cm   (i.e.,   2.5974E-02 rad  at mean radius RM =    38.50    )

                               KPOS = 2.  Position of reference orbit on mechanical  faces
                                         at entrance    RE =   0.00000     cm  TE =   0.00000     rad
                                         at exit        RS =   0.00000     cm  TS =   0.00000     rad


    Y =   96.82 X =   -1.57 T =-0.000 Trajectory #       1 went out of field region.
  A    1  0.3162     4.087     0.000     0.000     0.000            1.571   105.048     0.000     0.000     0.000            1


                CONDITIONS  DE  MAXWELL  (      316.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                      0.000            0.000             0.000    
                                       0.000             0.000    
                                       0.000             0.000    
                       LAPLACIEN SCALAIRE =   0.000    



 Cumulative length of optical axis =    0.00000000     m ;  Time  (for ref. rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      8  Keyword, label(s) :  FAISCEAU    #End                                      

0                                             TRACE DU FAISCEAU
                                           (follows element #      7)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    6.8093  105.048    0.001    0.000    0.000   2.574925E+04     1
               Time of flight (mus) :   8.0542885     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   1.050480E+00   1.175946E-06        1        0    0.000      (Y,T)        61
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        61
   0.0000E+00   0.0000E+00   1.0000E+00   8.054289E+00   1.212000E+01        1        0    0.000      (t,K)        61

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.866215E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
      9  Keyword, label(s) :  CAVITE                                                

                Accelerating cavity. Type is :   OPTION 3  


                    Synchronous  phase                 =     0.1571E+01 rad
                    Synchronous energy  gain           =     0.1000E+00 MeV
                    Cumulated  distance  from  origin  =     0.0000E+00 m
                    Synchronous  time                  =     0.0000E+00 s
                    Particle mass           =    0.93827E+03 MeV/c2
                             charge         =   0.160218E-18 C

************************************************************************************************************************************
     10  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #      9)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    6.8416  105.048    0.001    0.000    0.000   2.574925E+04     1
               Time of flight (mus) :   8.0542885     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   1.050480E+00   1.171094E-06        1        0    0.000      (Y,T)        61
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        61
   0.0000E+00   0.0000E+00   1.0000E+00   8.054289E+00   1.222000E+01        1        0    0.000      (t,K)        61

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.866215E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     11  Keyword, label(s) :  REBELOTE                                              


                         ****  End  of  'REBELOTE'  procedure  ****

      There  has  been         61  passes  through  the  optical  structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
     12  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #     11)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   0.3162     4.087     0.000     0.000     0.000      0.0000    6.8416  105.048    0.001    0.000    0.000   2.574925E+04     1
               Time of flight (mus) :   8.0542885     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface      alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                        in ellips,  out 
   0.0000E+00   0.0000E+00   1.0000E+00   1.050480E+00   1.171094E-06        1        0    0.000      (Y,T)        62
   0.0000E+00   0.0000E+00   1.0000E+00   0.000000E+00   0.000000E+00        1        1    1.000      (Z,P)        62
   0.0000E+00   0.0000E+00   1.0000E+00   8.054289E+00   1.222000E+01        1        0    0.000      (t,K)        62

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALF^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   1.866215E-34   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     13  Keyword, label(s) :  SYSTEM                                                

     Number  of  commands :   6,  as  follows : 

 cp gnuplot_zgoubi.plt.cmd gnuplot_zgoubi.plt_temp.cmd
 sed -i 's@pause 2@pause 0@g' gnuplot_zgoubi.plt_temp.cmd
 gnuplot < gnuplot_zgoubi.plt_temp.cmd
 mv -f gnuplot_zgoubi.plt_XYLab.eps gnuplot_zgoubi.plt_XYLab_cyclotron.eps
 okular gnuplot_zgoubi.plt_XYLab_cyclotron.eps &
 rm -f gnuplot_zgoubi.plt_temp.cmd

************************************************************************************************************************************
     14  Keyword, label(s) :  END                                                   


************************************************************************************************************************************
 Pgm zgoubi : Execution ended normally, upon keyword END or FIN
   
            ZGOUBI RUN COMPLETED. 

  Zgoubi, author's dvlpmnt version.
  Job  started  on  27-09-2018,  at  20:41:06 
  JOB  ENDED  ON    27-09-2018,  AT  20:41:11 

   CPU time, total :     3.1561970000000001     
