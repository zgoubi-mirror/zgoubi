SATURNE. CROSSING GammaG=7-NUz, NUz=3.60877(perturbed)
 'OBJET'                                                                                                      1
5018.67
2
1  1
0. 0.   .458  0.  0.  1.00   'o'
1
 
 'PARTICUL'                                                                                                   2
PROTON
 'SPNTRK'                                                                                                     3
3
 'FAISCNL'                                                                                                    4
turn3000.fai
 
 'SCALING'                                                                                                    5
1  3        PRINT
QUADRUPO
-1
5018.67E-3
1
MULTIPOL
-1
5018.67E-3
1
BEND
-1
5018.67E-3
1
 
 'DRIFT'                                                                                                      6
0.
 
'INCLUDE'
1
SATURNE.ring[#StartRing:#EndRing] 
 
 'CAVITE'                                                               85                                   89
2  PRINT
105.5556848673  3.
6000.   0.205303158892
 
 'REBELOTE'                                                             90                                   90
2999  0.2  99                     TOTAL NUMBER OF TURNS = 3000
 
 'FAISCEAU'                                                                                                  91
 'SPNPRT'                                                                                                    92
 
 'END'                                                                   91                                  93

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 

                          MAGNETIC  RIGIDITY =       5018.670 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       1 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


     Particle  properties :
     PROTON
                     Mass          =    938.272        MeV/c2
                     Charge        =   1.602176E-19    C     
                     G  factor     =    1.79285              
                     COM life-time =   1.000000E+99    s     

              Reference  data :
                    mag. rigidity (kG.cm)   :   5018.6700      =p/q, such that dev.=B*L/rigidity
                    mass (MeV/c2)           :   938.27203    
                    momentum (MeV/c)        :   1504.5594    
                    energy, total (MeV)     :   1773.1479    
                    energy, kinetic (MeV)   :   834.87586    
                    beta = v/c              :  0.8485244940    
                    gamma                   :   1.889801499    
                    beta*gamma              :   1.603542861    
                    G*gamma                 :   3.388125610    
                    electric rigidity (MeV) :   1276.655516    =T[eV]*(gamma+1)/gamma, such that dev.=E*L/rigidity
  
 I, AMQ(1,I), AMQ(2,I)/QE, P/Pref, v/c, time, s :
  
     1   9.38272030E+02  1.00000000E+00  1.00000000E+00  8.48524494E-01  0.00000000E+00  0.00000000E+00

************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                


                SPIN  TRACKING  REQUESTED  

                          Particle  mass          =    938.2720     MeV/c2
                          Gyromagnetic  factor  G =    1.792847    

                          Initial spin conditions type  3 :
                               All particles have spin parallel to  Z  AXIS


                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO   =      5018.670 KG*CM
                               BETA   =    0.848524
                               GAMMA*G =   3.388126


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        1  PARTICULES :
                               <SX> =     0.000000
                               <SY> =     0.000000
                               <SZ> =     1.000000
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCNL                                               


                OPEN FILE turn3000.fai                                                                    
                FOR PRINTING COORDINATES 

               Print will occur at element[s] labeled : 


************************************************************************************************************************************
      5  Keyword, label(s) :  SCALING                                               


               PRINT option is ON, 
               SCALING parameters will be logged in zgoubi.SCALING.Out.


               Scaling  request  on  3  families  of  optical  elements :
                                                      

     Family number   1
          Element [/label(s) ( 0)] to be scaled :          QUADRUPO
               Family not labeled ;  this scaling will apply to all (unlabeled) elements  "QUADRUPO  "
               Scaling of fields follows increase of rigidity taken from CAVITE, starting scaling value    5.01867000E+00

     Family number   2
          Element [/label(s) ( 0)] to be scaled :          MULTIPOL
               Family not labeled ;  this scaling will apply to all (unlabeled) elements  "MULTIPOL  "
               Scaling of fields follows increase of rigidity taken from CAVITE, starting scaling value    5.01867000E+00

     Family number   3
          Element [/label(s) ( 0)] to be scaled :          BEND
               Family not labeled ;  this scaling will apply to all (unlabeled) elements  "BEND      "
               Scaling of fields follows increase of rigidity taken from CAVITE, starting scaling value    5.01867000E+00

************************************************************************************************************************************
      6  Keyword, label(s) :  DRIFT                                                 


                              Drift,  length =     0.00000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  0.000000E+00  0.000000E+00  4.580000E-01  0.000000E+00   0.0000000E+00   0.00000E+00
TRAJ #1 SX, SY, SZ, |S| :  1    0.000000E+00   0.000000E+00   1.000000E+00   1.000000E+00

 Cumulative length of optical axis =    0.00000000     m   ;  Time  (for reference rigidity & particle) =    0.00000     s 

************************************************************************************************************************************
      7  Keyword, label(s) :  MARKER      #StartRing                                


************************************************************************************************************************************
      8  Keyword, label(s) :  QUADRUPO    QP                    1                   


      -----  QUADRUPOLE  : 
                Length  of  element  =    46.723000      cm
                Bore  radius      RO =    10.000      cm
               B-QUADRUPOLE  =  3.8327332E+00 kG   (i.e.,   7.6369500E-01 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0186700    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           46.723     0.000     0.000     0.497     0.002            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =   0.467230000     m ;  Time  (for ref. rigidity & particle) =   1.836731E-09 s 

************************************************************************************************************************************
      9  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  0.000000E+00  0.000000E+00  6.170451E-01  1.680031E+00   1.1834872E+02   4.65242E-03
TRAJ #1 SX, SY, SZ, |S| :  1   -7.372122E-03   0.000000E+00   9.999728E-01   1.000000E+00

 Cumulative length of optical axis =    1.18348600     m   ;  Time  (for reference rigidity & particle) =   4.652411E-09 s 

************************************************************************************************************************************
     10  Keyword, label(s) :  BEND        DIP                   3                   


      +++++        BEND  : 

                Length    =   2.473004E+02 cm
                Arc length    =   2.488966E+02 cm
                Deviation    =   2.250000E+01 deg.,    3.926991E-01 rad
                GAP   =   0.000000E+00 cm
                Gradient   =   0.000000E+00 kG/cm
                Grad-prime   =   0.000000E+00 kG/cm^2

                Field  =  7.9182568E+00  kG   (i.e.,   1.5777600E+00 * SCAL)
                Reference curvature radius (Brho/B) =   6.3380996E+02 cm
                Skew  angle  =   0.000000E+00  rad

               Entrance  face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

               Exit      face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

                    Field has been * by scaling factor    5.0186700    

     KPOS =  3 :  automatic positioning of element, 
        XCE, YCE, ALE = -4.2093663330E-03 -2.1161913603E-02 -0.1963495408     cm/cm/rad

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000          287.300     0.021    -0.196     1.026     0.002            1

     KPOS =  3.  Automatic  positionning  of  element.
          X = -4.2094E-03 CM   Y = -2.1162E-02 cm,  tilt  angle = -0.196350     RAD


 Cumulative length of optical axis =    3.67245201     m ;  Time  (for ref. rigidity & particle) =   1.443680E-08 s 

************************************************************************************************************************************
     11  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -5.974210E-04 -1.083683E-03  1.139611E+00  1.583413E+00   4.3885953E+02   1.72520E-02
TRAJ #1 SX, SY, SZ, |S| :  1    5.029909E-04   2.429045E-03   9.999969E-01   1.000000E+00

 Cumulative length of optical axis =    4.38870801     m   ;  Time  (for reference rigidity & particle) =   1.725248E-08 s 

************************************************************************************************************************************
     12  Keyword, label(s) :  MULTIPOL    QP                    5                   


      -----  MULTIPOLE   : 
                Length  of  element  =    48.627300      cm
                Bore  radius      RO =    10.000      cm
               B-DIPOLE      =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-QUADRUPOLE  = -3.8803855E+00 kG   (i.e.,  -7.7319000E-01 * SCAL)
               B-SEXTUPOLE   =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0186700    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           48.627    -0.001    -0.000     1.112    -0.003            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    4.87498101     m ;  Time  (for ref. rigidity & particle) =   1.916407E-08 s 

************************************************************************************************************************************
     13  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -9.578454E-04 -3.499543E-03  9.172552E-01 -2.714496E+00   5.5911274E+02   2.19793E-02
TRAJ #1 SX, SY, SZ, |S| :  1    1.936152E-02   2.428923E-03   9.998096E-01   1.000000E+00

 Cumulative length of optical axis =    5.59123701     m   ;  Time  (for reference rigidity & particle) =   2.197975E-08 s 

************************************************************************************************************************************
     14  Keyword, label(s) :  BEND        DIP                   3                   


      +++++        BEND  : 

                Length    =   2.473004E+02 cm
                Arc length    =   2.488966E+02 cm
                Deviation    =   2.250000E+01 deg.,    3.926991E-01 rad
                GAP   =   0.000000E+00 cm
                Gradient   =   0.000000E+00 kG/cm
                Grad-prime   =   0.000000E+00 kG/cm^2

                Field  =  7.9182568E+00  kG   (i.e.,   1.5777600E+00 * SCAL)
                Reference curvature radius (Brho/B) =   6.3380996E+02 cm
                Skew  angle  =   0.000000E+00  rad

               Entrance  face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

               Exit      face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

                    Field has been * by scaling factor    5.0186700    

     KPOS =  3 :  automatic positioning of element, 
        XCE, YCE, ALE = -4.2093663330E-03 -2.1161913603E-02 -0.1963495408     cm/cm/rad

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000          287.300     0.020    -0.196     0.228    -0.003            1

     KPOS =  3.  Automatic  positionning  of  element.
          X = -4.2094E-03 CM   Y = -2.1162E-02 cm,  tilt  angle = -0.196350     RAD


 Cumulative length of optical axis =    8.08020302     m ;  Time  (for ref. rigidity & particle) =   3.176415E-08 s 

************************************************************************************************************************************
     15  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -1.701263E-03 -5.067980E-03  2.892611E-02 -2.781809E+00   8.7962393E+02   3.45790E-02
TRAJ #1 SX, SY, SZ, |S| :  1    1.196024E-02  -1.716373E-02   9.997812E-01   1.000000E+00

 Cumulative length of optical axis =    8.79645902     m   ;  Time  (for reference rigidity & particle) =   3.457983E-08 s 

************************************************************************************************************************************
     16  Keyword, label(s) :  QUADRUPO    QP                    1                   


      -----  QUADRUPOLE  : 
                Length  of  element  =    46.723000      cm
                Bore  radius      RO =    10.000      cm
               B-QUADRUPOLE  =  3.8327332E+00 kG   (i.e.,   7.6369500E-01 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0186700    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           46.723    -0.002     0.000    -0.102    -0.003            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    9.26368902     m ;  Time  (for ref. rigidity & particle) =   3.641656E-08 s 

************************************************************************************************************************************
     17  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -1.702008E-03  1.251861E-03 -3.107362E-01 -2.910832E+00   9.9797301E+02   3.92314E-02
TRAJ #1 SX, SY, SZ, |S| :  1    1.252676E-02  -1.716343E-02   9.997742E-01   1.000000E+00

 Cumulative length of optical axis =    9.97994502     m   ;  Time  (for reference rigidity & particle) =   3.923224E-08 s 

************************************************************************************************************************************
     18  Keyword, label(s) :  BEND        DIP                   3                   


      +++++        BEND  : 

                Length    =   2.473004E+02 cm
                Arc length    =   2.488966E+02 cm
                Deviation    =   2.250000E+01 deg.,    3.926991E-01 rad
                GAP   =   0.000000E+00 cm
                Gradient   =   0.000000E+00 kG/cm
                Grad-prime   =   0.000000E+00 kG/cm^2

                Field  =  7.9182568E+00  kG   (i.e.,   1.5777600E+00 * SCAL)
                Reference curvature radius (Brho/B) =   6.3380996E+02 cm
                Skew  angle  =   0.000000E+00  rad

               Entrance  face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

               Exit      face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

                    Field has been * by scaling factor    5.0186700    

     KPOS =  3 :  automatic positioning of element, 
        XCE, YCE, ALE = -4.2093663330E-03 -2.1161913603E-02 -0.1963495408     cm/cm/rad

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000          287.300     0.019    -0.196    -1.031    -0.003            1

     KPOS =  3.  Automatic  positionning  of  element.
          X = -4.2094E-03 CM   Y = -2.1162E-02 cm,  tilt  angle = -0.196350     RAD


 Cumulative length of optical axis =    12.4689110     m ;  Time  (for ref. rigidity & particle) =   4.901663E-08 s 

************************************************************************************************************************************
     19  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -2.132406E-03  3.697137E-05 -1.233566E+00 -2.831950E+00   1.3184840E+03   5.18310E-02
TRAJ #1 SX, SY, SZ, |S| :  1   -1.417661E-02  -1.066214E-02   9.998427E-01   1.000000E+00

 Cumulative length of optical axis =    13.1851670     m   ;  Time  (for reference rigidity & particle) =   5.183231E-08 s 

************************************************************************************************************************************
     20  Keyword, label(s) :  MULTIPOL    QP                    5                   


      -----  MULTIPOLE   : 
                Length  of  element  =    48.627300      cm
                Bore  radius      RO =    10.000      cm
               B-DIPOLE      =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-QUADRUPOLE  = -3.8419575E+00 kG   (i.e.,  -7.6553300E-01 * SCAL)
               B-SEXTUPOLE   =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0186700    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           48.627    -0.002    -0.000    -1.257     0.002            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    13.6714400     m ;  Time  (for ref. rigidity & particle) =   5.374390E-08 s 

************************************************************************************************************************************
     21  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -2.909473E-03 -8.139366E-03 -1.122865E+00  1.875286E+00   1.4387371E+03   5.65583E-02
TRAJ #1 SX, SY, SZ, |S| :  1   -3.482519E-02  -1.066122E-02   9.993366E-01   1.000000E+00

 Cumulative length of optical axis =    14.3876960     m   ;  Time  (for reference rigidity & particle) =   5.655958E-08 s 

************************************************************************************************************************************
     22  Keyword, label(s) :  BEND        DIP                   3                   


      +++++        BEND  : 

                Length    =   2.473004E+02 cm
                Arc length    =   2.488966E+02 cm
                Deviation    =   2.250000E+01 deg.,    3.926991E-01 rad
                GAP   =   0.000000E+00 cm
                Gradient   =   0.000000E+00 kG/cm
                Grad-prime   =   0.000000E+00 kG/cm^2

                Field  =  7.9182568E+00  kG   (i.e.,   1.5777600E+00 * SCAL)
                Reference curvature radius (Brho/B) =   6.3380996E+02 cm
                Skew  angle  =   0.000000E+00  rad

               Entrance  face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

               Exit      face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

                    Field has been * by scaling factor    5.0186700    

     KPOS =  3 :  automatic positioning of element, 
        XCE, YCE, ALE = -4.2093663330E-03 -2.1161913603E-02 -0.1963495408     cm/cm/rad

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000          287.300     0.017    -0.196    -0.640     0.002            1

     KPOS =  3.  Automatic  positionning  of  element.
          X = -4.2094E-03 CM   Y = -2.1162E-02 cm,  tilt  angle = -0.196350     RAD


 Cumulative length of optical axis =    16.8766620     m ;  Time  (for ref. rigidity & particle) =   6.634397E-08 s 

************************************************************************************************************************************
     23  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -4.811680E-03 -8.171169E-03 -4.979180E-01  1.978914E+00   1.7592468E+03   6.91579E-02
TRAJ #1 SX, SY, SZ, |S| :  1   -2.429763E-02   3.223399E-02   9.991850E-01   1.000000E+00

 Cumulative length of optical axis =    17.5929180     m   ;  Time  (for reference rigidity & particle) =   6.915965E-08 s 

************************************************************************************************************************************
     24  Keyword, label(s) :  QUADRUPO    QP                    1                   


      -----  QUADRUPOLE  : 
                Length  of  element  =    46.723000      cm
                Bore  radius      RO =    10.000      cm
               B-QUADRUPOLE  =  3.8327332E+00 kG   (i.e.,   7.6369500E-01 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0186700    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           46.723    -0.005     0.000    -0.445     0.000            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    18.0601480     m ;  Time  (for ref. rigidity & particle) =   7.099638E-08 s 

************************************************************************************************************************************
     25  Keyword, label(s) :  ESL                                                   


                              Drift,  length =   392.14800  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -1.180972E-03  9.196566E-03 -3.195749E-01  3.197195E-01   2.1981179E+03   8.64104E-02
TRAJ #1 SX, SY, SZ, |S| :  1   -1.702468E-02   3.223246E-02   9.993354E-01   1.000000E+00

 Cumulative length of optical axis =    21.9816280     m   ;  Time  (for reference rigidity & particle) =   8.641214E-08 s 

************************************************************************************************************************************
     26  Keyword, label(s) :  MULTIPOL    QP                    5                   


      -----  MULTIPOLE   : 
                Length  of  element  =    48.627300      cm
                Bore  radius      RO =    10.000      cm
               B-DIPOLE      =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-QUADRUPOLE  = -3.8419575E+00 kG   (i.e.,  -7.6553300E-01 * SCAL)
               B-SEXTUPOLE   =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0186700    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           48.627    -0.001     0.000    -0.276     0.001            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    22.4679010     m ;  Time  (for ref. rigidity & particle) =   8.832373E-08 s 

************************************************************************************************************************************
     27  Keyword, label(s) :  ESL                                                   


                              Drift,  length =   392.14800  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  1.332651E-03  5.511478E-03  2.907668E-01  1.445292E+00   2.6388936E+03   1.03738E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -2.195980E-02   3.223275E-02   9.992391E-01   1.000000E+00

 Cumulative length of optical axis =    26.3893810     m   ;  Time  (for reference rigidity & particle) =   1.037395E-07 s 

************************************************************************************************************************************
     28  Keyword, label(s) :  QUADRUPO    QP                    1                   


      -----  QUADRUPOLE  : 
                Length  of  element  =    46.723000      cm
                Bore  radius      RO =    10.000      cm
               B-QUADRUPOLE  =  3.8327332E+00 kG   (i.e.,   7.6369500E-01 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0186700    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           46.723     0.001     0.000     0.385     0.003            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    26.8566110     m ;  Time  (for ref. rigidity & particle) =   1.055762E-07 s 

************************************************************************************************************************************
     29  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  1.504617E-03  4.342411E-04  5.734294E-01  2.634043E+00   2.7572426E+03   1.08390E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -2.717118E-02   3.223328E-02   9.991110E-01   1.000000E+00

 Cumulative length of optical axis =    27.5728670     m   ;  Time  (for reference rigidity & particle) =   1.083919E-07 s 

************************************************************************************************************************************
     30  Keyword, label(s) :  BEND        DIP                   3                   


      +++++        BEND  : 

                Length    =   2.473004E+02 cm
                Arc length    =   2.488966E+02 cm
                Deviation    =   2.250000E+01 deg.,    3.926991E-01 rad
                GAP   =   0.000000E+00 cm
                Gradient   =   0.000000E+00 kG/cm
                Grad-prime   =   0.000000E+00 kG/cm^2

                Field  =  7.9182568E+00  kG   (i.e.,   1.5777600E+00 * SCAL)
                Reference curvature radius (Brho/B) =   6.3380996E+02 cm
                Skew  angle  =   0.000000E+00  rad

               Entrance  face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

               Exit      face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

                    Field has been * by scaling factor    5.0186700    

     KPOS =  3 :  automatic positioning of element, 
        XCE, YCE, ALE = -4.2093663330E-03 -2.1161913603E-02 -0.1963495408     cm/cm/rad

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000          287.300     0.022    -0.196     1.221     0.003            1

     KPOS =  3.  Automatic  positionning  of  element.
          X = -4.2094E-03 CM   Y = -2.1162E-02 cm,  tilt  angle = -0.196350     RAD


 Cumulative length of optical axis =    30.0618330     m ;  Time  (for ref. rigidity & particle) =   1.181763E-07 s 

************************************************************************************************************************************
     31  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  3.695689E-04 -2.240402E-03  1.401790E+00  2.528503E+00   3.0777546E+03   1.20990E-01
TRAJ #1 SX, SY, SZ, |S| :  1    2.660070E-02   2.800271E-02   9.992538E-01   1.000000E+00

 Cumulative length of optical axis =    30.7780890     m   ;  Time  (for reference rigidity & particle) =   1.209920E-07 s 

************************************************************************************************************************************
     32  Keyword, label(s) :  MULTIPOL    QP                    5                   


      -----  MULTIPOLE   : 
                Length  of  element  =    48.627300      cm
                Bore  radius      RO =    10.000      cm
               B-DIPOLE      =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-QUADRUPOLE  = -3.8419575E+00 kG   (i.e.,  -7.6553300E-01 * SCAL)
               B-SEXTUPOLE   =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0186700    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           48.627     0.000    -0.000     1.396    -0.003            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    31.2643620     m ;  Time  (for ref. rigidity & particle) =   1.229036E-07 s 

************************************************************************************************************************************
     33  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  2.175894E-04 -1.028620E-03  1.198466E+00 -2.759185E+00   3.1980078E+03   1.25717E-01
TRAJ #1 SX, SY, SZ, |S| :  1    4.977703E-02   2.800290E-02   9.983677E-01   1.000000E+00

 Cumulative length of optical axis =    31.9806180     m   ;  Time  (for reference rigidity & particle) =   1.257192E-07 s 

************************************************************************************************************************************
     34  Keyword, label(s) :  BEND        DIP                   3                   


      +++++        BEND  : 

                Length    =   2.473004E+02 cm
                Arc length    =   2.488966E+02 cm
                Deviation    =   2.250000E+01 deg.,    3.926991E-01 rad
                GAP   =   0.000000E+00 cm
                Gradient   =   0.000000E+00 kG/cm
                Grad-prime   =   0.000000E+00 kG/cm^2

                Field  =  7.9182568E+00  kG   (i.e.,   1.5777600E+00 * SCAL)
                Reference curvature radius (Brho/B) =   6.3380996E+02 cm
                Skew  angle  =   0.000000E+00  rad

               Entrance  face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

               Exit      face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

                    Field has been * by scaling factor    5.0186700    

     KPOS =  3 :  automatic positioning of element, 
        XCE, YCE, ALE = -4.2093663330E-03 -2.1161913603E-02 -0.1963495408     cm/cm/rad

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000          287.300     0.022    -0.196     0.494    -0.003            1

     KPOS =  3.  Automatic  positionning  of  element.
          X = -4.2094E-03 CM   Y = -2.1162E-02 cm,  tilt  angle = -0.196350     RAD


 Cumulative length of optical axis =    34.4695841     m ;  Time  (for ref. rigidity & particle) =   1.355036E-07 s 

************************************************************************************************************************************
     35  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  2.863475E-04 -3.924908E-03  2.893743E-01 -2.858720E+00   3.5185198E+03   1.38317E-01
TRAJ #1 SX, SY, SZ, |S| :  1    4.531161E-02  -4.151732E-02   9.981098E-01   1.000000E+00

 Cumulative length of optical axis =    35.1858401     m   ;  Time  (for reference rigidity & particle) =   1.383193E-07 s 

************************************************************************************************************************************
     36  Keyword, label(s) :  QUADRUPO    QP                    1                   


      -----  QUADRUPOLE  : 
                Length  of  element  =    46.723000      cm
                Bore  radius      RO =    10.000      cm
               B-QUADRUPOLE  =  3.8327332E+00 kG   (i.e.,   7.6369500E-01 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0186700    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           46.723     0.000    -0.000     0.177    -0.002            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    35.6530701     m ;  Time  (for ref. rigidity & particle) =   1.401560E-07 s 

************************************************************************************************************************************
     37  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -2.447022E-04 -4.595840E-03  3.048735E-02 -2.038867E+00   3.6368687E+03   1.42969E-01
TRAJ #1 SX, SY, SZ, |S| :  1    4.172038E-02  -4.151744E-02   9.982664E-01   1.000000E+00

 Cumulative length of optical axis =    36.3693261     m   ;  Time  (for reference rigidity & particle) =   1.429717E-07 s 

************************************************************************************************************************************
     38  Keyword, label(s) :  BEND        DIP                   3                   


      +++++        BEND  : 

                Length    =   2.473004E+02 cm
                Arc length    =   2.488966E+02 cm
                Deviation    =   2.250000E+01 deg.,    3.926991E-01 rad
                GAP   =   0.000000E+00 cm
                Gradient   =   0.000000E+00 kG/cm
                Grad-prime   =   0.000000E+00 kG/cm^2

                Field  =  7.9182568E+00  kG   (i.e.,   1.5777600E+00 * SCAL)
                Reference curvature radius (Brho/B) =   6.3380996E+02 cm
                Skew  angle  =   0.000000E+00  rad

               Entrance  face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

               Exit      face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

                    Field has been * by scaling factor    5.0186700    

     KPOS =  3 :  automatic positioning of element, 
        XCE, YCE, ALE = -4.2093663330E-03 -2.1161913603E-02 -0.1963495408     cm/cm/rad

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000          287.300     0.020    -0.196    -0.477    -0.002            1

     KPOS =  3.  Automatic  positionning  of  element.
          X = -4.2094E-03 CM   Y = -2.1162E-02 cm,  tilt  angle = -0.196350     RAD


 Cumulative length of optical axis =    38.8582921     m ;  Time  (for ref. rigidity & particle) =   1.527561E-07 s 

************************************************************************************************************************************
     39  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -1.853264E-03 -5.071015E-03 -6.216036E-01 -2.012597E+00   3.9573793E+03   1.55569E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -2.961819E-02  -4.734447E-02   9.984394E-01   1.000000E+00

 Cumulative length of optical axis =    39.5745481     m   ;  Time  (for reference rigidity & particle) =   1.555718E-07 s 

************************************************************************************************************************************
     40  Keyword, label(s) :  MULTIPOL    QP                    5                   


      -----  MULTIPOLE   : 
                Length  of  element  =    48.627300      cm
                Bore  radius      RO =    10.000      cm
               B-DIPOLE      =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-QUADRUPOLE  = -3.8419575E+00 kG   (i.e.,  -7.6553300E-01 * SCAL)
               B-SEXTUPOLE   =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0186700    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           48.627    -0.002    -0.000    -0.661     0.000            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    40.0608211     m ;  Time  (for ref. rigidity & particle) =   1.574834E-07 s 

************************************************************************************************************************************
     41  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -3.183415E-03 -1.264590E-02 -6.316448E-01  4.116182E-01   4.0776322E+03   1.60296E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -4.023905E-02  -4.734326E-02   9.980679E-01   1.000000E+00

 Cumulative length of optical axis =    40.7770771     m   ;  Time  (for reference rigidity & particle) =   1.602991E-07 s 

************************************************************************************************************************************
     42  Keyword, label(s) :  BEND        DIP                   3                   


      +++++        BEND  : 

                Length    =   2.473004E+02 cm
                Arc length    =   2.488966E+02 cm
                Deviation    =   2.250000E+01 deg.,    3.926991E-01 rad
                GAP   =   0.000000E+00 cm
                Gradient   =   0.000000E+00 kG/cm
                Grad-prime   =   0.000000E+00 kG/cm^2

                Field  =  7.9182568E+00  kG   (i.e.,   1.5777600E+00 * SCAL)
                Reference curvature radius (Brho/B) =   6.3380996E+02 cm
                Skew  angle  =   0.000000E+00  rad

               Entrance  face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

               Exit      face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

                    Field has been * by scaling factor    5.0186700    

     KPOS =  3 :  automatic positioning of element, 
        XCE, YCE, ALE = -4.2093663330E-03 -2.1161913603E-02 -0.1963495408     cm/cm/rad

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000          287.300     0.016    -0.196    -0.520     0.000            1

     KPOS =  3.  Automatic  positionning  of  element.
          X = -4.2094E-03 CM   Y = -2.1162E-02 cm,  tilt  angle = -0.196350     RAD


 Cumulative length of optical axis =    43.2660431     m ;  Time  (for ref. rigidity & particle) =   1.700835E-07 s 

************************************************************************************************************************************
     43  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -6.675026E-03 -1.080976E-02 -4.856296E-01  4.792746E-01   4.3981408E+03   1.72896E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -5.853389E-02   2.934446E-02   9.978540E-01   1.000000E+00

 Cumulative length of optical axis =    43.9822991     m   ;  Time  (for reference rigidity & particle) =   1.728991E-07 s 

************************************************************************************************************************************
     44  Keyword, label(s) :  QUADRUPO    QP                    1                   


      -----  QUADRUPOLE  : 
                Length  of  element  =    46.723000      cm
                Bore  radius      RO =    10.000      cm
               B-QUADRUPOLE  =  3.8327332E+00 kG   (i.e.,   7.6369500E-01 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0186700    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           46.723    -0.007     0.000    -0.504    -0.001            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    44.4495291     m ;  Time  (for ref. rigidity & particle) =   1.747359E-07 s 

************************************************************************************************************************************
     45  Keyword, label(s) :  ESL                                                   


                              Drift,  length =   392.14800  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -1.425208E-03  1.324048E-02 -9.983892E-01 -1.261597E+00   4.8370121E+03   1.90148E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -5.091258E-02   2.933867E-02   9.982721E-01   1.000000E+00

 Cumulative length of optical axis =    48.3710091     m   ;  Time  (for reference rigidity & particle) =   1.901516E-07 s 

************************************************************************************************************************************
     46  Keyword, label(s) :  MULTIPOL    QP                    5                   


      -----  MULTIPOLE   : 
                Length  of  element  =    48.627300      cm
                Bore  radius      RO =    10.000      cm
               B-DIPOLE      =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-QUADRUPOLE  = -3.8419575E+00 kG   (i.e.,  -7.6553300E-01 * SCAL)
               B-SEXTUPOLE   =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0186700    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           48.627    -0.001     0.000    -0.969     0.002            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    48.8572821     m ;  Time  (for ref. rigidity & particle) =   1.920632E-07 s 

************************************************************************************************************************************
     47  Keyword, label(s) :  ESL                                                   


                              Drift,  length =   392.14800  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  2.632741E-03  8.990106E-03 -5.641249E-03  2.456346E+00   5.2777887E+03   2.07476E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -6.719115E-02   2.933970E-02   9.973086E-01   1.000000E+00

 Cumulative length of optical axis =    52.7787621     m   ;  Time  (for reference rigidity & particle) =   2.074790E-07 s 

************************************************************************************************************************************
     48  Keyword, label(s) :  QUADRUPO    QP                    1                   


      -----  QUADRUPOLE  : 
                Length  of  element  =    46.723000      cm
                Bore  radius      RO =    10.000      cm
               B-QUADRUPOLE  =  3.8327332E+00 kG   (i.e.,   7.6369500E-01 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0186700    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           46.723     0.003    -0.000     0.112     0.003            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    53.2459921     m ;  Time  (for ref. rigidity & particle) =   2.093157E-07 s 

************************************************************************************************************************************
     49  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  2.761443E-03 -8.842924E-04  3.011921E-01  2.643272E+00   5.3961377E+03   2.12128E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -6.800790E-02   2.934258E-02   9.972532E-01   1.000000E+00

 Cumulative length of optical axis =    53.9622481     m   ;  Time  (for reference rigidity & particle) =   2.121314E-07 s 

************************************************************************************************************************************
     50  Keyword, label(s) :  BEND        DIP                   3                   


      +++++        BEND  : 

                Length    =   2.473004E+02 cm
                Arc length    =   2.488966E+02 cm
                Deviation    =   2.250000E+01 deg.,    3.926991E-01 rad
                GAP   =   0.000000E+00 cm
                Gradient   =   0.000000E+00 kG/cm
                Grad-prime   =   0.000000E+00 kG/cm^2

                Field  =  7.9182568E+00  kG   (i.e.,   1.5777600E+00 * SCAL)
                Reference curvature radius (Brho/B) =   6.3380996E+02 cm
                Skew  angle  =   0.000000E+00  rad

               Entrance  face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

               Exit      face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

                    Field has been * by scaling factor    5.0186700    

     KPOS =  3 :  automatic positioning of element, 
        XCE, YCE, ALE = -4.2093663330E-03 -2.1161913603E-02 -0.1963495408     cm/cm/rad

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000          287.300     0.023    -0.196     0.955     0.003            1

     KPOS =  3.  Automatic  positionning  of  element.
          X = -4.2094E-03 CM   Y = -2.1162E-02 cm,  tilt  angle = -0.196350     RAD


 Cumulative length of optical axis =    56.4512141     m ;  Time  (for ref. rigidity & particle) =   2.219158E-07 s 

************************************************************************************************************************************
     51  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  1.423182E-03 -3.777102E-03  1.138765E+00  2.569428E+00   5.7166501E+03   2.24728E-01
TRAJ #1 SX, SY, SZ, |S| :  1    1.284240E-02   6.790823E-02   9.976089E-01   1.000000E+00

 Cumulative length of optical axis =    57.1674701     m   ;  Time  (for reference rigidity & particle) =   2.247315E-07 s 

************************************************************************************************************************************
     52  Keyword, label(s) :  MULTIPOL    QP                    5                   


      -----  MULTIPOLE   : 
                Length  of  element  =    48.627300      cm
                Bore  radius      RO =    10.000      cm
               B-DIPOLE      =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-QUADRUPOLE  = -3.8419575E+00 kG   (i.e.,  -7.6553300E-01 * SCAL)
               B-SEXTUPOLE   =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0186700    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           48.627     0.001     0.000     1.158    -0.002            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    57.6537431     m ;  Time  (for ref. rigidity & particle) =   2.266431E-07 s 

************************************************************************************************************************************
     53  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  1.460311E-03  1.335060E-03  1.031526E+00 -1.772047E+00   5.8369031E+03   2.29455E-01
TRAJ #1 SX, SY, SZ, |S| :  1    3.184278E-02   6.790873E-02   9.971833E-01   1.000000E+00

 Cumulative length of optical axis =    58.3699991     m   ;  Time  (for reference rigidity & particle) =   2.294587E-07 s 

************************************************************************************************************************************
     54  Keyword, label(s) :  BEND        DIP                   3                   


      +++++        BEND  : 

                Length    =   2.473004E+02 cm
                Arc length    =   2.488966E+02 cm
                Deviation    =   2.250000E+01 deg.,    3.926991E-01 rad
                GAP   =   0.000000E+00 cm
                Gradient   =   0.000000E+00 kG/cm
                Grad-prime   =   0.000000E+00 kG/cm^2

                Field  =  7.9182568E+00  kG   (i.e.,   1.5777600E+00 * SCAL)
                Reference curvature radius (Brho/B) =   6.3380996E+02 cm
                Skew  angle  =   0.000000E+00  rad

               Entrance  face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

               Exit      face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

                    Field has been * by scaling factor    5.0186700    

     KPOS =  3 :  automatic positioning of element, 
        XCE, YCE, ALE = -4.2093663330E-03 -2.1161913603E-02 -0.1963495408     cm/cm/rad

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000          287.300     0.024    -0.196     0.575    -0.002            1

     KPOS =  3.  Automatic  positionning  of  element.
          X = -4.2094E-03 CM   Y = -2.1162E-02 cm,  tilt  angle = -0.196350     RAD


 Cumulative length of optical axis =    60.8589651     m ;  Time  (for ref. rigidity & particle) =   2.392431E-07 s 

************************************************************************************************************************************
     55  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  2.054742E-03 -1.084154E-03  4.416597E-01 -1.866512E+00   6.1574149E+03   2.42055E-01
TRAJ #1 SX, SY, SZ, |S| :  1    7.873380E-02  -1.555841E-02   9.967743E-01   1.000000E+00

 Cumulative length of optical axis =    61.5752211     m   ;  Time  (for reference rigidity & particle) =   2.420588E-07 s 

************************************************************************************************************************************
     56  Keyword, label(s) :  QUADRUPO    QP                    1                   


      -----  QUADRUPOLE  : 
                Length  of  element  =    46.723000      cm
                Bore  radius      RO =    10.000      cm
               B-QUADRUPOLE  =  3.8327332E+00 kG   (i.e.,   7.6369500E-01 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0186700    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           46.723     0.002    -0.000     0.389    -0.000            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    62.0424511     m ;  Time  (for ref. rigidity & particle) =   2.438955E-07 s 

************************************************************************************************************************************
     57  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  1.254628E-03 -8.124751E-03  3.603879E-01 -4.041836E-01   6.2757635E+03   2.46707E-01
TRAJ #1 SX, SY, SZ, |S| :  1    7.233556E-02  -1.556072E-02   9.972590E-01   1.000000E+00

 Cumulative length of optical axis =    62.7587071     m   ;  Time  (for reference rigidity & particle) =   2.467112E-07 s 

************************************************************************************************************************************
     58  Keyword, label(s) :  BEND        DIP                   3                   


      +++++        BEND  : 

                Length    =   2.473004E+02 cm
                Arc length    =   2.488966E+02 cm
                Deviation    =   2.250000E+01 deg.,    3.926991E-01 rad
                GAP   =   0.000000E+00 cm
                Gradient   =   0.000000E+00 kG/cm
                Grad-prime   =   0.000000E+00 kG/cm^2

                Field  =  7.9182568E+00  kG   (i.e.,   1.5777600E+00 * SCAL)
                Reference curvature radius (Brho/B) =   6.3380996E+02 cm
                Skew  angle  =   0.000000E+00  rad

               Entrance  face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

               Exit      face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

                    Field has been * by scaling factor    5.0186700    

     KPOS =  3 :  automatic positioning of element, 
        XCE, YCE, ALE = -4.2093663330E-03 -2.1161913603E-02 -0.1963495408     cm/cm/rad

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000          287.300     0.021    -0.196     0.255    -0.000            1

     KPOS =  3.  Automatic  positionning  of  element.
          X = -4.2094E-03 CM   Y = -2.1162E-02 cm,  tilt  angle = -0.196350     RAD


 Cumulative length of optical axis =    65.2476731     m ;  Time  (for ref. rigidity & particle) =   2.564956E-07 s 

************************************************************************************************************************************
     59  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -1.235122E-03 -8.399514E-03  2.229802E-01 -4.402970E-01   6.5962739E+03   2.59307E-01
TRAJ #1 SX, SY, SZ, |S| :  1    3.849406E-03  -7.456965E-02   9.972084E-01   1.000000E+00

 Cumulative length of optical axis =    65.9639291     m   ;  Time  (for reference rigidity & particle) =   2.593113E-07 s 

************************************************************************************************************************************
     60  Keyword, label(s) :  MULTIPOL    QP                    5                   


      -----  MULTIPOLE   : 
                Length  of  element  =    48.627300      cm
                Bore  radius      RO =    10.000      cm
               B-DIPOLE      =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-QUADRUPOLE  = -3.8419575E+00 kG   (i.e.,  -7.6553300E-01 * SCAL)
               B-SEXTUPOLE   =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0186700    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           48.627    -0.002    -0.000     0.182    -0.001            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    66.4502021     m ;  Time  (for ref. rigidity & particle) =   2.612229E-07 s 

************************************************************************************************************************************
     61  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -2.765738E-03 -1.390912E-02  9.592951E-02 -1.206288E+00   6.7165269E+03   2.64034E-01
TRAJ #1 SX, SY, SZ, |S| :  1    7.199459E-03  -7.456979E-02   9.971898E-01   1.000000E+00

 Cumulative length of optical axis =    67.1664581     m   ;  Time  (for reference rigidity & particle) =   2.640386E-07 s 

************************************************************************************************************************************
     62  Keyword, label(s) :  BEND        DIP                   3                   


      +++++        BEND  : 

                Length    =   2.473004E+02 cm
                Arc length    =   2.488966E+02 cm
                Deviation    =   2.250000E+01 deg.,    3.926991E-01 rad
                GAP   =   0.000000E+00 cm
                Gradient   =   0.000000E+00 kG/cm
                Grad-prime   =   0.000000E+00 kG/cm^2

                Field  =  7.9182568E+00  kG   (i.e.,   1.5777600E+00 * SCAL)
                Reference curvature radius (Brho/B) =   6.3380996E+02 cm
                Skew  angle  =   0.000000E+00  rad

               Entrance  face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

               Exit      face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

                    Field has been * by scaling factor    5.0186700    

     KPOS =  3 :  automatic positioning of element, 
        XCE, YCE, ALE = -4.2093663330E-03 -2.1161913603E-02 -0.1963495408     cm/cm/rad

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000          287.300     0.016    -0.196    -0.206    -0.001            1

     KPOS =  3.  Automatic  positionning  of  element.
          X = -4.2094E-03 CM   Y = -2.1162E-02 cm,  tilt  angle = -0.196350     RAD


 Cumulative length of optical axis =    69.6554241     m ;  Time  (for ref. rigidity & particle) =   2.738230E-07 s 

************************************************************************************************************************************
     63  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -6.767883E-03 -1.206239E-02 -2.916641E-01 -1.199829E+00   7.0370357E+03   2.76633E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -6.990361E-02  -2.319923E-02   9.972840E-01   1.000000E+00

 Cumulative length of optical axis =    70.3716801     m   ;  Time  (for reference rigidity & particle) =   2.766386E-07 s 

************************************************************************************************************************************
     64  Keyword, label(s) :  QUADRUPO    QP                    1                   


      -----  QUADRUPOLE  : 
                Length  of  element  =    46.723000      cm
                Bore  radius      RO =    10.000      cm
               B-QUADRUPOLE  =  3.8327332E+00 kG   (i.e.,   7.6369500E-01 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0186700    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           46.723    -0.007     0.000    -0.374    -0.002            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    70.8389101     m ;  Time  (for ref. rigidity & particle) =   2.784754E-07 s 

************************************************************************************************************************************
     65  Keyword, label(s) :  ESL                                                   


                              Drift,  length =   392.14800  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -1.891819E-03  1.241306E-02 -1.303780E+00 -2.371123E+00   7.4759079E+03   2.93886E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -6.477439E-02  -2.320654E-02   9.976301E-01   1.000000E+00

 Cumulative length of optical axis =    74.7603901     m   ;  Time  (for reference rigidity & particle) =   2.938911E-07 s 

************************************************************************************************************************************
     66  Keyword, label(s) :  MULTIPOL    QP                    5                   


      -----  MULTIPOLE   : 
                Length  of  element  =    48.627300      cm
                Bore  radius      RO =    10.000      cm
               B-DIPOLE      =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-QUADRUPOLE  = -3.8419575E+00 kG   (i.e.,  -7.6553300E-01 * SCAL)
               B-SEXTUPOLE   =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0186700    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           48.627    -0.001     0.000    -1.299     0.003            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    75.2466631     m ;  Time  (for ref. rigidity & particle) =   2.958027E-07 s 

************************************************************************************************************************************
     67  Keyword, label(s) :  ESL                                                   


                              Drift,  length =   392.14800  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  1.025600E-03  6.296746E-03 -2.999717E-01  2.548591E+00   7.9166845E+03   3.11213E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -8.629541E-02  -2.320460E-02   9.959993E-01   1.000000E+00

 Cumulative length of optical axis =    79.1681431     m   ;  Time  (for reference rigidity & particle) =   3.112185E-07 s 

************************************************************************************************************************************
     68  Keyword, label(s) :  QUADRUPO    QP                    1                   


      -----  QUADRUPOLE  : 
                Length  of  element  =    46.723000      cm
                Bore  radius      RO =    10.000      cm
               B-QUADRUPOLE  =  3.8327332E+00 kG   (i.e.,   7.6369500E-01 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0186700    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           46.723     0.001     0.000    -0.203     0.002            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    79.6353731     m ;  Time  (for ref. rigidity & particle) =   3.130552E-07 s 

************************************************************************************************************************************
     69  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  1.386419E-03  2.220375E-03 -8.375187E-02  1.663651E+00   8.0350333E+03   3.15866E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -8.242749E-02  -2.320310E-02   9.963269E-01   1.000000E+00

 Cumulative length of optical axis =    80.3516291     m   ;  Time  (for reference rigidity & particle) =   3.158709E-07 s 

************************************************************************************************************************************
     70  Keyword, label(s) :  BEND        DIP                   3                   


      +++++        BEND  : 

                Length    =   2.473004E+02 cm
                Arc length    =   2.488966E+02 cm
                Deviation    =   2.250000E+01 deg.,    3.926991E-01 rad
                GAP   =   0.000000E+00 cm
                Gradient   =   0.000000E+00 kG/cm
                Grad-prime   =   0.000000E+00 kG/cm^2

                Field  =  7.9182568E+00  kG   (i.e.,   1.5777600E+00 * SCAL)
                Reference curvature radius (Brho/B) =   6.3380996E+02 cm
                Skew  angle  =   0.000000E+00  rad

               Entrance  face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

               Exit      face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

                    Field has been * by scaling factor    5.0186700    

     KPOS =  3 :  automatic positioning of element, 
        XCE, YCE, ALE = -4.2093663330E-03 -2.1161913603E-02 -0.1963495408     cm/cm/rad

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000          287.300     0.023    -0.196     0.332     0.002            1

     KPOS =  3.  Automatic  positionning  of  element.
          X = -4.2094E-03 CM   Y = -2.1162E-02 cm,  tilt  angle = -0.196350     RAD


 Cumulative length of optical axis =    82.8405951     m ;  Time  (for ref. rigidity & particle) =   3.256553E-07 s 

************************************************************************************************************************************
     71  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  1.886965E-03  8.499419E-04  4.496879E-01  1.649089E+00   8.3555447E+03   3.28465E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -4.305041E-02   7.224932E-02   9.964571E-01   1.000000E+00

 Cumulative length of optical axis =    83.5568511     m   ;  Time  (for reference rigidity & particle) =   3.284710E-07 s 

************************************************************************************************************************************
     72  Keyword, label(s) :  MULTIPOL    QP                    5                   


      -----  MULTIPOLE   : 
                Length  of  element  =    48.627300      cm
                Bore  radius      RO =    10.000      cm
               B-DIPOLE      =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-QUADRUPOLE  = -3.8419575E+00 kG   (i.e.,  -7.6553300E-01 * SCAL)
               B-SEXTUPOLE   =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0186700    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           48.627     0.002     0.000     0.487    -0.000            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    84.0431241     m ;  Time  (for ref. rigidity & particle) =   3.303826E-07 s 

************************************************************************************************************************************
     73  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  2.687848E-03  8.166267E-03  4.786604E-01 -1.218826E-01   8.4757976E+03   3.33193E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -3.530780E-02   7.224808E-02   9.967615E-01   1.000000E+00

 Cumulative length of optical axis =    84.7593801     m   ;  Time  (for reference rigidity & particle) =   3.331982E-07 s 

************************************************************************************************************************************
     74  Keyword, label(s) :  BEND        DIP                   3                   


      +++++        BEND  : 

                Length    =   2.473004E+02 cm
                Arc length    =   2.488966E+02 cm
                Deviation    =   2.250000E+01 deg.,    3.926991E-01 rad
                GAP   =   0.000000E+00 cm
                Gradient   =   0.000000E+00 kG/cm
                Grad-prime   =   0.000000E+00 kG/cm^2

                Field  =  7.9182568E+00  kG   (i.e.,   1.5777600E+00 * SCAL)
                Reference curvature radius (Brho/B) =   6.3380996E+02 cm
                Skew  angle  =   0.000000E+00  rad

               Entrance  face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

               Exit      face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

                    Field has been * by scaling factor    5.0186700    

     KPOS =  3 :  automatic positioning of element, 
        XCE, YCE, ALE = -4.2093663330E-03 -2.1161913603E-02 -0.1963495408     cm/cm/rad

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000          287.300     0.026    -0.196     0.441    -0.000            1

     KPOS =  3.  Automatic  positionning  of  element.
          X = -4.2094E-03 CM   Y = -2.1162E-02 cm,  tilt  angle = -0.196350     RAD


 Cumulative length of optical axis =    87.2483461     m ;  Time  (for ref. rigidity & particle) =   3.429826E-07 s 

************************************************************************************************************************************
     75  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  5.068871E-03  6.151927E-03  4.287231E-01 -1.759276E-01   8.7963094E+03   3.45792E-01
TRAJ #1 SX, SY, SZ, |S| :  1    6.396196E-02   5.003515E-02   9.966972E-01   1.000000E+00

 Cumulative length of optical axis =    87.9646021     m   ;  Time  (for reference rigidity & particle) =   3.457983E-07 s 

************************************************************************************************************************************
     76  Keyword, label(s) :  QUADRUPO    QP                    1                   


      -----  QUADRUPOLE  : 
                Length  of  element  =    46.723000      cm
                Bore  radius      RO =    10.000      cm
               B-QUADRUPOLE  =  3.8327332E+00 kG   (i.e.,   7.6369500E-01 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0186700    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           46.723     0.005    -0.000     0.457     0.001            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    88.4318321     m ;  Time  (for ref. rigidity & particle) =   3.476350E-07 s 

************************************************************************************************************************************
     77  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  4.076320E-03 -1.194223E-02  5.554853E-01  1.381840E+00   8.9146581E+03   3.50445E-01
TRAJ #1 SX, SY, SZ, |S| :  1    5.715139E-02   5.003033E-02   9.971112E-01   1.000000E+00

 Cumulative length of optical axis =    89.1480881     m   ;  Time  (for reference rigidity & particle) =   3.504507E-07 s 

************************************************************************************************************************************
     78  Keyword, label(s) :  BEND        DIP                   3                   


      +++++        BEND  : 

                Length    =   2.473004E+02 cm
                Arc length    =   2.488966E+02 cm
                Deviation    =   2.250000E+01 deg.,    3.926991E-01 rad
                GAP   =   0.000000E+00 cm
                Gradient   =   0.000000E+00 kG/cm
                Grad-prime   =   0.000000E+00 kG/cm^2

                Field  =  7.9182568E+00  kG   (i.e.,   1.5777600E+00 * SCAL)
                Reference curvature radius (Brho/B) =   6.3380996E+02 cm
                Skew  angle  =   0.000000E+00  rad

               Entrance  face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

               Exit      face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

                    Field has been * by scaling factor    5.0186700    

     KPOS =  3 :  automatic positioning of element, 
        XCE, YCE, ALE = -4.2093663330E-03 -2.1161913603E-02 -0.1963495408     cm/cm/rad

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000          287.300     0.022    -0.196     0.891     0.001            1

     KPOS =  3.  Automatic  positionning  of  element.
          X = -4.2094E-03 CM   Y = -2.1162E-02 cm,  tilt  angle = -0.196350     RAD


 Cumulative length of optical axis =    91.6370541     m ;  Time  (for ref. rigidity & particle) =   3.602351E-07 s 

************************************************************************************************************************************
     79  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -4.050124E-04 -1.396919E-02  9.842035E-01  1.296788E+00   9.2351697E+03   3.63044E-01
TRAJ #1 SX, SY, SZ, |S| :  1    6.426036E-02  -4.764751E-02   9.967950E-01   1.000000E+00

 Cumulative length of optical axis =    92.3533101     m   ;  Time  (for reference rigidity & particle) =   3.630508E-07 s 

************************************************************************************************************************************
     80  Keyword, label(s) :  MULTIPOL    QP                    5                   


      -----  MULTIPOLE   : 
                Length  of  element  =    48.627300      cm
                Bore  radius      RO =    10.000      cm
               B-DIPOLE      =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-QUADRUPOLE  = -3.8419575E+00 kG   (i.e.,  -7.6553300E-01 * SCAL)
               B-SEXTUPOLE   =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0186700    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           48.627    -0.001    -0.000     0.958    -0.002            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    92.8395831     m ;  Time  (for ref. rigidity & particle) =   3.649624E-07 s 

************************************************************************************************************************************
     81  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -2.345955E-03 -1.680635E-02  7.876608E-01 -2.373064E+00   9.3554229E+03   3.67772E-01
TRAJ #1 SX, SY, SZ, |S| :  1    8.030290E-02  -4.764852E-02   9.956310E-01   1.000000E+00

 Cumulative length of optical axis =    93.5558391     m   ;  Time  (for reference rigidity & particle) =   3.677781E-07 s 

************************************************************************************************************************************
     82  Keyword, label(s) :  BEND        DIP                   3                   


      +++++        BEND  : 

                Length    =   2.473004E+02 cm
                Arc length    =   2.488966E+02 cm
                Deviation    =   2.250000E+01 deg.,    3.926991E-01 rad
                GAP   =   0.000000E+00 cm
                Gradient   =   0.000000E+00 kG/cm
                Grad-prime   =   0.000000E+00 kG/cm^2

                Field  =  7.9182568E+00  kG   (i.e.,   1.5777600E+00 * SCAL)
                Reference curvature radius (Brho/B) =   6.3380996E+02 cm
                Skew  angle  =   0.000000E+00  rad

               Entrance  face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

               Exit      face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

                    Field has been * by scaling factor    5.0186700    

     KPOS =  3 :  automatic positioning of element, 
        XCE, YCE, ALE = -4.2093663330E-03 -2.1161913603E-02 -0.1963495408     cm/cm/rad

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000          287.300     0.016    -0.196     0.185    -0.002            1

     KPOS =  3.  Automatic  positionning  of  element.
          X = -4.2094E-03 CM   Y = -2.1162E-02 cm,  tilt  angle = -0.196350     RAD


 Cumulative length of optical axis =    96.0448052     m ;  Time  (for ref. rigidity & particle) =   3.775624E-07 s 

************************************************************************************************************************************
     83  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -7.117987E-03 -1.636881E-02  1.140633E-02 -2.430225E+00   9.6759325E+03   3.80371E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -2.288477E-02  -8.836069E-02   9.958256E-01   1.000000E+00

 Cumulative length of optical axis =    96.7610612     m   ;  Time  (for reference rigidity & particle) =   3.803781E-07 s 

************************************************************************************************************************************
     84  Keyword, label(s) :  QUADRUPO    QP                    1                   


      -----  QUADRUPOLE  : 
                Length  of  element  =    46.723000      cm
                Bore  radius      RO =    10.000      cm
               B-QUADRUPOLE  =  3.8327332E+00 kG   (i.e.,   7.6369500E-01 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0186700    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           46.723    -0.007     0.000    -0.104    -0.003            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    97.2282912     m ;  Time  (for ref. rigidity & particle) =   3.822149E-07 s 

************************************************************************************************************************************
     85  Keyword, label(s) :  ESL                                                   


                              Drift,  length =   392.14800  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -3.482326E-03  9.675521E-03 -1.121513E+00 -2.593796E+00   1.0114805E+04   3.97624E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -2.215990E-02  -8.836338E-02   9.958418E-01   1.000000E+00

 Cumulative length of optical axis =    101.149771     m   ;  Time  (for reference rigidity & particle) =   3.976306E-07 s 

************************************************************************************************************************************
     86  Keyword, label(s) :  MULTIPOL    QP                    5                   


      -----  MULTIPOLE   : 
                Length  of  element  =    48.627300      cm
                Bore  radius      RO =    10.000      cm
               B-DIPOLE      =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-QUADRUPOLE  = -3.8419575E+00 kG   (i.e.,  -7.6553300E-01 * SCAL)
               B-SEXTUPOLE   =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0186700    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           48.627    -0.003    -0.000    -1.144     0.002            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    101.636044     m ;  Time  (for ref. rigidity & particle) =   3.995422E-07 s 

************************************************************************************************************************************
     87  Keyword, label(s) :  ESL                                                   


                              Drift,  length =   392.14800  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -4.412898E-03 -2.793391E-03 -4.821151E-01  1.687551E+00   1.0555581E+04   4.14951E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -4.086868E-02  -8.836167E-02   9.952497E-01   1.000000E+00

 Cumulative length of optical axis =    105.557524     m   ;  Time  (for reference rigidity & particle) =   4.149580E-07 s 

************************************************************************************************************************************
     88  Keyword, label(s) :  MARKER      #EndRing                                  


************************************************************************************************************************************
     89  Keyword, label(s) :  CAVITE      85                                        

                Accelerating cavity. Type is :   OPTION 2  

                Cavite parameters saved in zgoubi.CAVITE.Out


                    Orbit  length           =     1.05555685E+02 m
                    RF  harmonic            =     3.00000000E+00
                    Peak  voltage           =     6.00000000E+03 V
                    RF  frequency           =     7.22977386E+06 Hz
                    Synchronous  phase      =     2.05303159E-01 rd
                    Isochronous  time       =     4.14950738E-07 s
                    qV.sin(phi_s)           =     1.22318377E-03 MeV
                    cos(phi_s)              =     9.78999226E-01 
                    Nu_s/sqrt(alpha)        =     1.25766468E-03  
                    dp-acc*sqrt(alpha)      =     7.09291985E-04  
                    dgamma/dt               =     3.70255923E+00 /s 
                    rho*dB/dt               =     1.15880426E+01 T.m/s 
                    SR loss, this pass      =     0.00000000E+00 MeV 


************************************************************************************************************************************
     90  Keyword, label(s) :  REBELOTE    90                                        


                                -----  REBELOTE  -----

     End of pass #        1 through the optical structure 

                     Total of          1 particles have been launched

     Multiple pass, 
          from element #     1 : OBJET     /label1=                    /label2=                    
                             to  REBELOTE  /label1=                    /label2=                    
     ending at pass #    3000 at element #    90 : REBELOTE  /label1=90                  /label2=                    


     WRITE statements to zgoubi.res are inhibeted from now on.

************************************************************************************************************************************

                                -----  REBELOTE  -----

     End of pass #        2 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #        3 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #        4 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #        5 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #        6 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #        7 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #        8 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #        9 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       10 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       11 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       12 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       13 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       14 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       15 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       16 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       17 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       18 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       19 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       20 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       21 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       22 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       23 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       24 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       25 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       26 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       27 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       28 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       29 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       30 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       31 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       32 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       33 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       34 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       35 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       36 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       37 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       38 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       39 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       40 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       41 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       42 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       43 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       44 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       45 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       46 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       47 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       48 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       49 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       50 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       51 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       52 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       53 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       54 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       55 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       56 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       57 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       58 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       59 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       60 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       61 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       62 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       63 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       64 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       65 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       66 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       67 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       68 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       69 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       70 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       71 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       72 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       73 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       74 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       75 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       76 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       77 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       78 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       79 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       80 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       81 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       82 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       83 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       84 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       85 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       86 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       87 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       88 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       89 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       90 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       91 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       92 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       93 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       94 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       95 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       96 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       97 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       98 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #       99 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      100 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      101 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      102 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      103 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      104 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      105 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      106 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      107 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      108 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      109 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      110 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      111 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      112 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      113 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      114 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      115 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      116 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      117 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      118 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      119 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      120 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      121 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      122 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      123 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      124 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      125 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      126 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      127 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      128 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      129 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      130 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      131 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      132 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      133 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      134 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      135 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      136 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      137 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      138 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      139 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      140 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      141 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      142 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      143 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      144 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      145 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      146 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      147 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      148 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      149 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      150 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      151 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      152 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      153 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      154 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      155 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      156 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      157 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      158 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      159 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      160 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      161 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      162 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      163 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      164 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      165 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      166 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      167 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      168 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      169 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      170 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      171 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      172 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      173 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      174 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      175 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      176 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      177 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      178 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      179 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      180 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      181 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      182 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      183 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      184 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      185 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      186 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      187 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      188 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      189 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      190 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      191 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      192 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      193 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      194 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      195 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      196 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      197 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      198 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      199 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      200 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      201 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      202 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      203 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      204 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      205 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      206 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      207 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      208 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      209 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      210 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      211 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      212 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      213 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      214 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      215 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      216 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      217 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      218 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      219 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      220 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      221 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      222 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      223 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      224 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      225 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      226 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      227 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      228 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      229 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      230 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      231 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      232 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      233 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      234 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      235 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      236 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      237 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      238 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      239 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      240 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      241 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      242 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      243 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      244 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      245 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      246 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      247 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      248 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      249 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      250 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      251 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      252 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      253 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      254 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      255 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      256 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      257 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      258 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      259 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      260 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      261 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      262 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      263 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      264 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      265 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      266 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      267 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      268 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      269 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      270 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      271 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      272 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      273 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      274 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      275 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      276 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      277 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      278 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      279 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      280 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      281 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      282 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      283 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      284 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      285 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      286 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      287 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      288 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      289 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      290 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      291 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      292 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      293 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      294 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      295 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      296 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      297 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      298 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      299 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      300 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      301 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      302 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      303 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      304 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      305 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      306 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      307 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      308 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      309 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      310 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      311 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      312 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      313 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      314 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      315 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      316 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      317 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      318 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      319 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      320 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      321 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      322 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      323 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      324 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      325 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      326 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      327 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      328 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      329 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      330 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      331 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      332 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      333 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      334 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      335 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      336 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      337 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      338 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      339 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      340 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      341 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      342 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      343 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      344 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      345 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      346 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      347 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      348 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      349 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      350 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      351 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      352 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      353 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      354 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      355 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      356 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      357 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      358 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      359 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      360 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      361 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      362 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      363 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      364 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      365 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      366 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      367 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      368 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      369 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      370 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      371 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      372 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      373 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      374 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      375 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      376 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      377 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      378 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      379 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      380 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      381 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      382 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      383 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      384 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      385 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      386 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      387 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      388 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      389 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      390 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      391 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      392 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      393 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      394 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      395 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      396 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      397 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      398 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      399 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      400 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      401 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      402 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      403 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      404 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      405 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      406 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      407 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      408 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      409 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      410 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      411 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      412 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      413 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      414 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      415 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      416 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      417 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      418 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      419 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      420 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      421 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      422 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      423 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      424 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      425 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      426 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      427 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      428 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      429 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      430 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      431 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      432 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      433 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      434 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      435 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      436 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      437 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      438 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      439 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      440 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      441 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      442 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      443 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      444 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      445 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      446 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      447 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      448 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      449 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      450 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      451 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      452 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      453 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      454 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      455 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      456 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      457 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      458 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      459 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      460 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      461 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      462 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      463 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      464 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      465 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      466 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      467 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      468 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      469 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      470 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      471 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      472 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      473 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      474 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      475 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      476 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      477 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      478 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      479 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      480 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      481 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      482 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      483 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      484 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      485 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      486 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      487 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      488 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      489 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      490 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      491 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      492 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      493 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      494 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      495 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      496 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      497 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      498 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      499 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      500 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      501 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      502 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      503 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      504 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      505 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      506 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      507 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      508 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      509 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      510 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      511 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      512 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      513 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      514 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      515 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      516 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      517 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      518 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      519 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      520 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      521 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      522 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      523 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      524 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      525 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      526 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      527 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      528 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      529 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      530 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      531 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      532 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      533 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      534 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      535 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      536 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      537 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      538 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      539 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      540 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      541 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      542 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      543 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      544 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      545 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      546 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      547 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      548 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      549 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      550 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      551 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      552 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      553 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      554 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      555 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      556 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      557 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      558 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      559 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      560 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      561 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      562 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      563 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      564 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      565 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      566 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      567 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      568 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      569 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      570 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      571 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      572 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      573 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      574 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      575 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      576 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      577 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      578 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      579 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      580 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      581 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      582 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      583 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      584 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      585 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      586 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      587 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      588 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      589 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      590 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      591 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      592 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      593 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      594 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      595 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      596 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      597 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      598 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      599 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      600 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      601 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      602 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      603 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      604 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      605 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      606 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      607 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      608 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      609 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      610 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      611 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      612 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      613 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      614 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      615 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      616 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      617 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      618 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      619 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      620 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      621 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      622 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      623 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      624 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      625 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      626 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      627 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      628 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      629 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      630 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      631 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      632 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      633 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      634 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      635 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      636 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      637 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      638 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      639 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      640 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      641 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      642 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      643 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      644 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      645 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      646 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      647 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      648 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      649 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      650 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      651 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      652 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      653 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      654 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      655 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      656 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      657 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      658 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      659 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      660 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      661 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      662 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      663 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      664 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      665 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      666 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      667 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      668 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      669 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      670 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      671 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      672 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      673 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      674 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      675 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      676 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      677 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      678 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      679 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      680 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      681 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      682 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      683 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      684 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      685 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      686 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      687 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      688 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      689 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      690 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      691 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      692 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      693 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      694 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      695 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      696 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      697 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      698 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      699 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      700 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      701 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      702 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      703 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      704 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      705 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      706 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      707 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      708 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      709 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      710 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      711 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      712 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      713 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      714 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      715 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      716 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      717 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      718 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      719 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      720 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      721 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      722 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      723 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      724 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      725 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      726 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      727 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      728 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      729 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      730 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      731 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      732 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      733 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      734 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      735 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      736 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      737 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      738 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      739 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      740 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      741 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      742 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      743 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      744 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      745 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      746 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      747 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      748 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      749 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      750 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      751 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      752 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      753 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      754 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      755 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      756 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      757 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      758 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      759 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      760 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      761 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      762 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      763 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      764 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      765 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      766 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      767 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      768 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      769 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      770 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      771 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      772 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      773 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      774 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      775 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      776 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      777 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      778 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      779 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      780 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      781 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      782 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      783 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      784 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      785 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      786 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      787 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      788 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      789 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      790 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      791 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      792 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      793 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      794 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      795 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      796 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      797 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      798 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      799 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      800 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      801 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      802 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      803 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      804 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      805 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      806 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      807 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      808 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      809 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      810 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      811 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      812 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      813 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      814 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      815 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      816 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      817 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      818 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      819 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      820 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      821 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      822 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      823 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      824 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      825 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      826 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      827 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      828 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      829 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      830 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      831 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      832 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      833 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      834 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      835 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      836 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      837 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      838 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      839 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      840 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      841 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      842 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      843 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      844 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      845 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      846 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      847 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      848 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      849 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      850 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      851 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      852 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      853 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      854 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      855 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      856 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      857 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      858 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      859 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      860 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      861 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      862 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      863 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      864 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      865 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      866 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      867 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      868 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      869 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      870 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      871 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      872 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      873 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      874 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      875 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      876 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      877 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      878 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      879 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      880 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      881 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      882 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      883 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      884 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      885 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      886 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      887 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      888 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      889 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      890 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      891 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      892 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      893 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      894 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      895 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      896 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      897 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      898 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      899 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      900 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      901 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      902 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      903 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      904 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      905 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      906 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      907 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      908 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      909 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      910 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      911 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      912 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      913 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      914 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      915 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      916 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      917 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      918 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      919 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      920 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      921 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      922 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      923 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      924 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      925 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      926 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      927 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      928 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      929 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      930 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      931 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      932 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      933 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      934 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      935 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      936 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      937 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      938 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      939 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      940 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      941 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      942 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      943 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      944 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      945 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      946 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      947 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      948 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      949 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      950 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      951 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      952 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      953 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      954 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      955 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      956 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      957 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      958 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      959 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      960 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      961 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      962 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      963 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      964 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      965 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      966 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      967 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      968 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      969 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      970 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      971 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      972 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      973 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      974 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      975 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      976 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      977 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      978 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      979 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      980 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      981 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      982 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      983 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      984 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      985 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      986 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      987 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      988 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      989 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      990 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      991 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      992 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      993 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      994 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      995 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      996 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      997 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      998 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #      999 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1000 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1001 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1002 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1003 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1004 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1005 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1006 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1007 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1008 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1009 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1010 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1011 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1012 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1013 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1014 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1015 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1016 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1017 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1018 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1019 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1020 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1021 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1022 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1023 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1024 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1025 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1026 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1027 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1028 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1029 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1030 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1031 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1032 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1033 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1034 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1035 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1036 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1037 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1038 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1039 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1040 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1041 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1042 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1043 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1044 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1045 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1046 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1047 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1048 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1049 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1050 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1051 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1052 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1053 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1054 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1055 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1056 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1057 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1058 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1059 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1060 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1061 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1062 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1063 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1064 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1065 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1066 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1067 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1068 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1069 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1070 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1071 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1072 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1073 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1074 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1075 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1076 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1077 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1078 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1079 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1080 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1081 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1082 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1083 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1084 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1085 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1086 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1087 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1088 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1089 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1090 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1091 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1092 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1093 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1094 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1095 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1096 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1097 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1098 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1099 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1100 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1101 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1102 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1103 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1104 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1105 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1106 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1107 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1108 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1109 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1110 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1111 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1112 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1113 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1114 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1115 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1116 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1117 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1118 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1119 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1120 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1121 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1122 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1123 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1124 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1125 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1126 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1127 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1128 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1129 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1130 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1131 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1132 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1133 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1134 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1135 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1136 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1137 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1138 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1139 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1140 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1141 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1142 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1143 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1144 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1145 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1146 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1147 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1148 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1149 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1150 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1151 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1152 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1153 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1154 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1155 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1156 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1157 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1158 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1159 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1160 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1161 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1162 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1163 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1164 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1165 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1166 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1167 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1168 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1169 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1170 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1171 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1172 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1173 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1174 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1175 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1176 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1177 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1178 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1179 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1180 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1181 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1182 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1183 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1184 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1185 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1186 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1187 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1188 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1189 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1190 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1191 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1192 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1193 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1194 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1195 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1196 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1197 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1198 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1199 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1200 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1201 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1202 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1203 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1204 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1205 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1206 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1207 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1208 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1209 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1210 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1211 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1212 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1213 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1214 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1215 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1216 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1217 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1218 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1219 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1220 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1221 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1222 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1223 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1224 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1225 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1226 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1227 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1228 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1229 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1230 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1231 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1232 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1233 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1234 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1235 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1236 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1237 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1238 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1239 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1240 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1241 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1242 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1243 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1244 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1245 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1246 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1247 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1248 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1249 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1250 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1251 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1252 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1253 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1254 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1255 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1256 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1257 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1258 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1259 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1260 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1261 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1262 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1263 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1264 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1265 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1266 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1267 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1268 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1269 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1270 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1271 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1272 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1273 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1274 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1275 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1276 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1277 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1278 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1279 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1280 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1281 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1282 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1283 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1284 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1285 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1286 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1287 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1288 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1289 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1290 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1291 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1292 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1293 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1294 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1295 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1296 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1297 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1298 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1299 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1300 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1301 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1302 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1303 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1304 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1305 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1306 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1307 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1308 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1309 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1310 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1311 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1312 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1313 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1314 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1315 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1316 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1317 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1318 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1319 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1320 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1321 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1322 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1323 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1324 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1325 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1326 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1327 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1328 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1329 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1330 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1331 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1332 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1333 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1334 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1335 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1336 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1337 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1338 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1339 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1340 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1341 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1342 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1343 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1344 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1345 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1346 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1347 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1348 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1349 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1350 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1351 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1352 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1353 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1354 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1355 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1356 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1357 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1358 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1359 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1360 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1361 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1362 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1363 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1364 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1365 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1366 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1367 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1368 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1369 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1370 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1371 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1372 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1373 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1374 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1375 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1376 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1377 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1378 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1379 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1380 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1381 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1382 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1383 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1384 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1385 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1386 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1387 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1388 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1389 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1390 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1391 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1392 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1393 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1394 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1395 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1396 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1397 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1398 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1399 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1400 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1401 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1402 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1403 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1404 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1405 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1406 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1407 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1408 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1409 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1410 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1411 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1412 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1413 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1414 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1415 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1416 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1417 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1418 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1419 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1420 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1421 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1422 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1423 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1424 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1425 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1426 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1427 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1428 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1429 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1430 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1431 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1432 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1433 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1434 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1435 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1436 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1437 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1438 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1439 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1440 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1441 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1442 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1443 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1444 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1445 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1446 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1447 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1448 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1449 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1450 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1451 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1452 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1453 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1454 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1455 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1456 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1457 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1458 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1459 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1460 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1461 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1462 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1463 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1464 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1465 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1466 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1467 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1468 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1469 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1470 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1471 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1472 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1473 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1474 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1475 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1476 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1477 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1478 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1479 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1480 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1481 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1482 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1483 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1484 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1485 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1486 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1487 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1488 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1489 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1490 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1491 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1492 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1493 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1494 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1495 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1496 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1497 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1498 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1499 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1500 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1501 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1502 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1503 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1504 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1505 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1506 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1507 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1508 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1509 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1510 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1511 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1512 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1513 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1514 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1515 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1516 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1517 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1518 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1519 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1520 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1521 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1522 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1523 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1524 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1525 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1526 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1527 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1528 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1529 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1530 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1531 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1532 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1533 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1534 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1535 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1536 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1537 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1538 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1539 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1540 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1541 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1542 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1543 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1544 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1545 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1546 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1547 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1548 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1549 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1550 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1551 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1552 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1553 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1554 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1555 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1556 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1557 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1558 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1559 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1560 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1561 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1562 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1563 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1564 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1565 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1566 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1567 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1568 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1569 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1570 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1571 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1572 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1573 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1574 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1575 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1576 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1577 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1578 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1579 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1580 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1581 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1582 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1583 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1584 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1585 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1586 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1587 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1588 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1589 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1590 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1591 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1592 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1593 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1594 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1595 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1596 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1597 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1598 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1599 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1600 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1601 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1602 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1603 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1604 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1605 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1606 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1607 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1608 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1609 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1610 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1611 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1612 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1613 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1614 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1615 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1616 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1617 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1618 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1619 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1620 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1621 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1622 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1623 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1624 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1625 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1626 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1627 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1628 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1629 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1630 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1631 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1632 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1633 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1634 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1635 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1636 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1637 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1638 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1639 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1640 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1641 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1642 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1643 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1644 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1645 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1646 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1647 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1648 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1649 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1650 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1651 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1652 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1653 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1654 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1655 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1656 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1657 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1658 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1659 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1660 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1661 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1662 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1663 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1664 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1665 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1666 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1667 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1668 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1669 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1670 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1671 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1672 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1673 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1674 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1675 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1676 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1677 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1678 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1679 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1680 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1681 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1682 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1683 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1684 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1685 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1686 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1687 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1688 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1689 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1690 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1691 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1692 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1693 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1694 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1695 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1696 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1697 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1698 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1699 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1700 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1701 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1702 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1703 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1704 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1705 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1706 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1707 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1708 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1709 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1710 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1711 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1712 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1713 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1714 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1715 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1716 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1717 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1718 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1719 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1720 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1721 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1722 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1723 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1724 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1725 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1726 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1727 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1728 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1729 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1730 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1731 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1732 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1733 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1734 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1735 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1736 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1737 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1738 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1739 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1740 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1741 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1742 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1743 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1744 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1745 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1746 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1747 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1748 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1749 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1750 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1751 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1752 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1753 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1754 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1755 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1756 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1757 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1758 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1759 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1760 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1761 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1762 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1763 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1764 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1765 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1766 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1767 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1768 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1769 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1770 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1771 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1772 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1773 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1774 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1775 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1776 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1777 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1778 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1779 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1780 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1781 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1782 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1783 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1784 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1785 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1786 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1787 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1788 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1789 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1790 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1791 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1792 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1793 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1794 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1795 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1796 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1797 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1798 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1799 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1800 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1801 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1802 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1803 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1804 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1805 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1806 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1807 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1808 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1809 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1810 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1811 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1812 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1813 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1814 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1815 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1816 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1817 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1818 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1819 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1820 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1821 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1822 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1823 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1824 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1825 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1826 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1827 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1828 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1829 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1830 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1831 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1832 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1833 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1834 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1835 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1836 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1837 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1838 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1839 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1840 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1841 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1842 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1843 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1844 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1845 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1846 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1847 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1848 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1849 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1850 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1851 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1852 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1853 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1854 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1855 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1856 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1857 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1858 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1859 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1860 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1861 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1862 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1863 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1864 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1865 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1866 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1867 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1868 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1869 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1870 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1871 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1872 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1873 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1874 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1875 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1876 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1877 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1878 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1879 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1880 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1881 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1882 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1883 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1884 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1885 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1886 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1887 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1888 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1889 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1890 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1891 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1892 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1893 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1894 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1895 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1896 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1897 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1898 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1899 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1900 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1901 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1902 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1903 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1904 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1905 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1906 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1907 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1908 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1909 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1910 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1911 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1912 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1913 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1914 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1915 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1916 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1917 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1918 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1919 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1920 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1921 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1922 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1923 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1924 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1925 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1926 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1927 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1928 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1929 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1930 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1931 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1932 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1933 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1934 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1935 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1936 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1937 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1938 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1939 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1940 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1941 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1942 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1943 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1944 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1945 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1946 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1947 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1948 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1949 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1950 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1951 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1952 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1953 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1954 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1955 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1956 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1957 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1958 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1959 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1960 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1961 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1962 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1963 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1964 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1965 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1966 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1967 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1968 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1969 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1970 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1971 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1972 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1973 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1974 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1975 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1976 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1977 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1978 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1979 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1980 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1981 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1982 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1983 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1984 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1985 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1986 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1987 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1988 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1989 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1990 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1991 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1992 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1993 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1994 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1995 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1996 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1997 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1998 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     1999 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2000 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2001 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2002 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2003 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2004 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2005 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2006 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2007 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2008 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2009 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2010 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2011 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2012 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2013 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2014 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2015 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2016 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2017 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2018 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2019 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2020 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2021 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2022 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2023 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2024 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2025 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2026 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2027 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2028 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2029 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2030 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2031 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2032 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2033 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2034 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2035 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2036 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2037 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2038 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2039 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2040 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2041 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2042 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2043 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2044 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2045 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2046 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2047 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2048 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2049 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2050 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2051 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2052 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2053 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2054 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2055 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2056 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2057 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2058 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2059 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2060 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2061 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2062 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2063 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2064 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2065 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2066 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2067 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2068 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2069 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2070 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2071 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2072 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2073 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2074 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2075 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2076 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2077 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2078 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2079 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2080 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2081 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2082 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2083 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2084 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2085 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2086 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2087 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2088 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2089 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2090 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2091 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2092 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2093 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2094 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2095 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2096 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2097 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2098 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2099 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2100 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2101 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2102 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2103 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2104 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2105 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2106 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2107 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2108 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2109 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2110 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2111 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2112 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2113 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2114 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2115 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2116 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2117 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2118 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2119 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2120 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2121 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2122 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2123 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2124 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2125 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2126 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2127 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2128 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2129 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2130 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2131 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2132 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2133 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2134 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2135 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2136 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2137 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2138 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2139 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2140 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2141 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2142 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2143 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2144 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2145 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2146 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2147 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2148 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2149 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2150 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2151 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2152 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2153 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2154 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2155 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2156 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2157 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2158 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2159 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2160 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2161 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2162 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2163 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2164 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2165 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2166 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2167 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2168 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2169 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2170 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2171 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2172 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2173 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2174 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2175 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2176 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2177 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2178 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2179 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2180 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2181 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2182 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2183 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2184 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2185 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2186 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2187 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2188 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2189 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2190 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2191 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2192 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2193 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2194 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2195 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2196 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2197 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2198 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2199 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2200 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2201 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2202 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2203 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2204 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2205 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2206 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2207 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2208 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2209 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2210 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2211 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2212 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2213 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2214 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2215 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2216 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2217 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2218 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2219 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2220 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2221 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2222 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2223 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2224 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2225 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2226 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2227 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2228 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2229 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2230 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2231 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2232 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2233 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2234 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2235 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2236 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2237 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2238 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2239 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2240 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2241 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2242 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2243 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2244 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2245 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2246 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2247 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2248 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2249 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2250 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2251 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2252 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2253 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2254 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2255 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2256 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2257 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2258 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2259 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2260 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2261 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2262 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2263 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2264 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2265 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2266 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2267 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2268 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2269 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2270 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2271 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2272 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2273 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2274 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2275 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2276 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2277 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2278 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2279 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2280 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2281 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2282 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2283 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2284 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2285 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2286 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2287 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2288 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2289 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2290 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2291 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2292 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2293 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2294 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2295 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2296 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2297 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2298 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2299 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2300 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2301 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2302 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2303 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2304 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2305 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2306 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2307 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2308 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2309 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2310 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2311 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2312 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2313 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2314 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2315 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2316 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2317 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2318 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2319 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2320 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2321 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2322 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2323 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2324 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2325 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2326 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2327 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2328 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2329 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2330 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2331 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2332 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2333 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2334 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2335 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2336 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2337 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2338 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2339 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2340 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2341 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2342 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2343 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2344 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2345 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2346 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2347 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2348 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2349 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2350 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2351 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2352 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2353 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2354 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2355 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2356 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2357 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2358 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2359 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2360 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2361 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2362 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2363 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2364 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2365 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2366 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2367 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2368 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2369 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2370 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2371 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2372 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2373 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2374 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2375 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2376 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2377 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2378 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2379 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2380 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2381 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2382 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2383 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2384 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2385 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2386 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2387 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2388 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2389 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2390 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2391 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2392 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2393 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2394 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2395 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2396 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2397 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2398 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2399 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2400 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2401 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2402 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2403 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2404 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2405 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2406 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2407 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2408 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2409 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2410 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2411 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2412 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2413 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2414 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2415 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2416 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2417 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2418 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2419 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2420 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2421 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2422 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2423 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2424 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2425 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2426 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2427 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2428 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2429 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2430 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2431 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2432 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2433 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2434 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2435 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2436 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2437 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2438 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2439 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2440 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2441 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2442 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2443 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2444 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2445 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2446 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2447 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2448 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2449 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2450 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2451 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2452 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2453 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2454 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2455 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2456 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2457 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2458 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2459 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2460 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2461 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2462 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2463 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2464 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2465 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2466 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2467 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2468 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2469 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2470 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2471 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2472 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2473 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2474 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2475 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2476 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2477 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2478 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2479 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2480 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2481 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2482 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2483 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2484 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2485 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2486 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2487 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2488 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2489 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2490 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2491 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2492 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2493 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2494 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2495 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2496 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2497 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2498 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2499 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2500 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2501 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2502 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2503 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2504 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2505 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2506 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2507 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2508 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2509 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2510 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2511 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2512 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2513 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2514 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2515 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2516 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2517 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2518 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2519 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2520 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2521 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2522 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2523 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2524 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2525 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2526 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2527 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2528 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2529 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2530 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2531 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2532 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2533 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2534 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2535 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2536 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2537 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2538 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2539 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2540 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2541 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2542 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2543 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2544 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2545 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2546 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2547 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2548 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2549 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2550 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2551 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2552 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2553 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2554 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2555 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2556 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2557 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2558 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2559 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2560 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2561 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2562 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2563 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2564 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2565 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2566 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2567 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2568 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2569 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2570 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2571 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2572 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2573 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2574 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2575 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2576 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2577 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2578 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2579 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2580 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2581 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2582 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2583 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2584 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2585 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2586 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2587 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2588 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2589 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2590 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2591 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2592 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2593 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2594 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2595 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2596 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2597 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2598 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2599 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2600 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2601 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2602 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2603 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2604 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2605 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2606 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2607 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2608 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2609 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2610 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2611 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2612 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2613 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2614 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2615 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2616 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2617 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2618 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2619 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2620 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2621 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2622 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2623 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2624 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2625 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2626 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2627 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2628 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2629 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2630 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2631 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2632 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2633 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2634 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2635 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2636 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2637 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2638 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2639 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2640 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2641 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2642 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2643 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2644 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2645 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2646 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2647 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2648 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2649 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2650 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2651 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2652 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2653 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2654 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2655 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2656 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2657 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2658 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2659 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2660 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2661 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2662 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2663 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2664 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2665 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2666 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2667 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2668 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2669 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2670 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2671 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2672 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2673 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2674 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2675 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2676 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2677 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2678 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2679 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2680 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2681 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2682 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2683 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2684 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2685 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2686 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2687 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2688 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2689 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2690 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2691 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2692 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2693 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2694 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2695 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2696 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2697 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2698 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2699 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2700 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2701 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2702 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2703 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2704 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2705 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2706 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2707 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2708 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2709 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2710 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2711 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2712 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2713 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2714 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2715 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2716 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2717 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2718 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2719 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2720 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2721 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2722 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2723 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2724 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2725 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2726 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2727 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2728 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2729 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2730 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2731 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2732 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2733 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2734 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2735 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2736 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2737 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2738 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2739 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2740 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2741 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2742 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2743 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2744 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2745 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2746 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2747 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2748 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2749 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2750 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2751 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2752 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2753 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2754 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2755 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2756 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2757 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2758 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2759 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2760 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2761 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2762 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2763 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2764 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2765 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2766 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2767 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2768 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2769 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2770 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2771 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2772 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2773 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2774 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2775 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2776 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2777 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2778 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2779 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2780 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2781 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2782 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2783 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2784 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2785 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2786 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2787 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2788 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2789 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2790 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2791 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2792 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2793 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2794 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2795 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2796 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2797 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2798 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2799 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2800 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2801 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2802 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2803 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2804 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2805 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2806 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2807 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2808 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2809 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2810 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2811 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2812 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2813 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2814 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2815 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2816 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2817 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2818 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2819 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2820 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2821 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2822 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2823 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2824 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2825 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2826 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2827 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2828 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2829 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2830 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2831 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2832 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2833 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2834 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2835 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2836 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2837 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2838 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2839 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2840 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2841 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2842 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2843 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2844 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2845 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2846 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2847 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2848 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2849 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2850 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2851 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2852 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2853 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2854 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2855 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2856 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2857 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2858 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2859 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2860 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2861 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2862 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2863 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2864 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2865 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2866 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2867 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2868 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2869 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2870 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2871 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2872 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2873 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2874 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2875 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2876 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2877 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2878 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2879 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2880 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2881 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2882 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2883 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2884 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2885 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2886 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2887 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2888 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2889 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2890 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2891 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2892 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2893 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2894 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2895 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2896 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2897 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2898 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2899 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2900 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2901 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2902 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2903 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2904 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2905 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2906 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2907 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2908 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2909 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2910 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2911 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2912 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2913 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2914 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2915 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2916 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2917 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2918 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2919 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2920 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2921 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2922 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2923 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2924 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2925 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2926 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2927 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2928 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2929 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2930 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2931 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2932 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2933 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2934 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2935 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2936 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2937 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2938 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2939 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2940 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2941 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2942 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2943 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2944 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2945 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2946 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2947 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2948 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2949 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2950 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2951 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2952 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2953 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2954 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2955 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2956 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2957 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2958 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2959 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2960 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2961 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2962 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2963 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2964 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2965 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2966 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2967 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2968 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2969 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2970 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2971 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2972 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2973 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2974 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2975 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2976 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2977 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2978 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2979 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2980 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2981 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2982 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2983 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2984 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2985 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2986 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2987 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2988 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2989 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2990 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2991 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2992 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2993 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2994 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2995 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2996 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2997 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2998 through the optical structure 

                     Total of          1 particles have been launched

                                -----  REBELOTE  -----

     End of pass #     2999 through the optical structure 

                     Total of          1 particles have been launched


      Next  pass  is  #  3000 and  last  pass  through  the  optical  structure


************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                 



               Final  coordinates  of  previous  run  taken  as  initial  coordinates ;         1 particles

************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                              


************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                



               Final  spins  of  last  run  taken  as  initial  spins.

************************************************************************************************************************************
      4  Keyword, label(s) :  FAISCNL                                               


     turn3000.fai                                                                    
      already open...
               Print will occur at element[s] labeled : 


************************************************************************************************************************************
      5  Keyword, label(s) :  SCALING                                               


               PRINT option is ON, 
               SCALING parameters will be logged in zgoubi.SCALING.Out.


               Scaling  request  on  3  families  of  optical  elements :
                                                      

     Family number   1
          Element [/label(s) ( 0)] to be scaled :          QUADRUPO
               Family not labeled ;  this scaling will apply to all (unlabeled) elements  "QUADRUPO  "
               Scaling of fields follows increase of rigidity taken from CAVITE, starting scaling value    5.01867000E+00

     Family number   2
          Element [/label(s) ( 0)] to be scaled :          MULTIPOL
               Family not labeled ;  this scaling will apply to all (unlabeled) elements  "MULTIPOL  "
               Scaling of fields follows increase of rigidity taken from CAVITE, starting scaling value    5.01867000E+00

     Family number   3
          Element [/label(s) ( 0)] to be scaled :          BEND
               Family not labeled ;  this scaling will apply to all (unlabeled) elements  "BEND      "
               Scaling of fields follows increase of rigidity taken from CAVITE, starting scaling value    5.01867000E+00

************************************************************************************************************************************
      6  Keyword, label(s) :  DRIFT                                                 


                              Drift,  length =     0.00000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  2.877804E-03 -8.830708E-03 -1.801425E-02 -4.106049E-01 -5.036205E-01   0.0000000E+00   1.24394E+03
TRAJ #1 SX, SY, SZ, |S| :  1    6.433592E-01  -6.957151E-01   3.194831E-01   1.000000E+00

 Cumulative length of optical axis =    0.00000000     m   ;  Time  (for reference rigidity & particle) =   1.243960E-03 s 

************************************************************************************************************************************
      7  Keyword, label(s) :  MARKER      #StartRing                                


************************************************************************************************************************************
      8  Keyword, label(s) :  QUADRUPO    QP                    1                   


      -----  QUADRUPOLE  : 
                Length  of  element  =    46.723000      cm
                Bore  radius      RO =    10.000      cm
               B-QUADRUPOLE  =  3.8437417E+00 kG   (i.e.,   7.6369500E-01 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0330848    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           46.723    -0.009     0.000    -0.470    -0.002            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =   0.467230000     m ;  Time  (for ref. rigidity & particle) =   1.243962E-03 s 

************************************************************************************************************************************
      9  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  2.877804E-03 -7.912752E-03  1.410808E-02 -6.165020E-01 -2.052357E+00   1.1834879E+02   1.24394E+03
TRAJ #1 SX, SY, SZ, |S| :  1    6.456172E-01  -6.956241E-01   3.150960E-01   1.000000E+00

 Cumulative length of optical axis =    1.18348600     m   ;  Time  (for reference rigidity & particle) =   1.243965E-03 s 

************************************************************************************************************************************
     10  Keyword, label(s) :  BEND        DIP                   3                   


      +++++        BEND  : 

                Length    =   2.473004E+02 cm
                Arc length    =   2.481837E+02 cm
                Deviation    =   2.250000E+01 deg.,    3.926991E-01 rad
                GAP   =   0.000000E+00 cm
                Gradient   =   0.000000E+00 kG/cm
                Grad-prime   =   0.000000E+00 kG/cm^2

                Field  =  7.9409999E+00  kG   (i.e.,   1.5777600E+00 * SCAL)
                Reference curvature radius (Brho/B) =   6.3199472E+02 cm
                Skew  angle  =   0.000000E+00  rad

               Entrance  face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

               Exit      face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

                    Field has been * by scaling factor    5.0330848    

     KPOS =  3 :  automatic positioning of element, 
        XCE, YCE, ALE = -4.2093663330E-03 -2.1161913603E-02 -0.1963495408     cm/cm/rad

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000          287.300     0.017    -0.196    -1.118    -0.002            1

     KPOS =  3.  Automatic  positionning  of  element.
          X = -4.2094E-03 CM   Y = -2.1162E-02 cm,  tilt  angle = -0.196350     RAD


 Cumulative length of optical axis =    3.67245201     m ;  Time  (for ref. rigidity & particle) =   1.243974E-03 s 

************************************************************************************************************************************
     11  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  2.877804E-03 -3.183474E-03  1.779454E-02 -1.258025E+00 -1.950354E+00   4.3885747E+02   1.24396E+03
TRAJ #1 SX, SY, SZ, |S| :  1   -5.248351E-01  -7.895000E-01   3.181790E-01   1.000000E+00

 Cumulative length of optical axis =    4.38870801     m   ;  Time  (for reference rigidity & particle) =   1.243977E-03 s 

************************************************************************************************************************************
     12  Keyword, label(s) :  MULTIPOL    QP                    5                   


      -----  MULTIPOLE   : 
                Length  of  element  =    48.627300      cm
                Bore  radius      RO =    10.000      cm
               B-DIPOLE      =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-QUADRUPOLE  = -3.8915308E+00 kG   (i.e.,  -7.7319000E-01 * SCAL)
               B-SEXTUPOLE   =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0330848    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           48.627    -0.003     0.000    -1.237     0.003            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    4.87498101     m ;  Time  (for ref. rigidity & particle) =   1.243979E-03 s 

************************************************************************************************************************************
     13  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  2.877804E-03 -2.077865E-03  7.108868E-03 -1.035306E+00  2.812340E+00   5.5911071E+02   1.24396E+03
TRAJ #1 SX, SY, SZ, |S| :  1   -5.314170E-01  -7.894752E-01   3.071234E-01   1.000000E+00

 Cumulative length of optical axis =    5.59123701     m   ;  Time  (for reference rigidity & particle) =   1.243982E-03 s 

************************************************************************************************************************************
     14  Keyword, label(s) :  BEND        DIP                   3                   


      +++++        BEND  : 

                Length    =   2.473004E+02 cm
                Arc length    =   2.481837E+02 cm
                Deviation    =   2.250000E+01 deg.,    3.926991E-01 rad
                GAP   =   0.000000E+00 cm
                Gradient   =   0.000000E+00 kG/cm
                Grad-prime   =   0.000000E+00 kG/cm^2

                Field  =  7.9409999E+00  kG   (i.e.,   1.5777600E+00 * SCAL)
                Reference curvature radius (Brho/B) =   6.3199472E+02 cm
                Skew  angle  =   0.000000E+00  rad

               Entrance  face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

               Exit      face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

                    Field has been * by scaling factor    5.0330848    

     KPOS =  3 :  automatic positioning of element, 
        XCE, YCE, ALE = -4.2093663330E-03 -2.1161913603E-02 -0.1963495408     cm/cm/rad

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000          287.300     0.022    -0.196    -0.320     0.003            1

     KPOS =  3.  Automatic  positionning  of  element.
          X = -4.2094E-03 CM   Y = -2.1162E-02 cm,  tilt  angle = -0.196350     RAD


 Cumulative length of optical axis =    8.08020302     m ;  Time  (for ref. rigidity & particle) =   1.243992E-03 s 

************************************************************************************************************************************
     15  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  2.877804E-03  1.054483E-03  7.259928E-03 -1.129917E-01  2.892026E+00   8.7962216E+02   1.24397E+03
TRAJ #1 SX, SY, SZ, |S| :  1   -8.940523E-01   3.304770E-01   3.024159E-01   1.000000E+00

 Cumulative length of optical axis =    8.79645902     m   ;  Time  (for reference rigidity & particle) =   1.243994E-03 s 

************************************************************************************************************************************
     16  Keyword, label(s) :  QUADRUPO    QP                    1                   


      -----  QUADRUPOLE  : 
                Length  of  element  =    46.723000      cm
                Bore  radius      RO =    10.000      cm
               B-QUADRUPOLE  =  3.8437417E+00 kG   (i.e.,   7.6369500E-01 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0330848    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           46.723     0.001     0.000     0.016     0.003            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    9.26368902     m ;  Time  (for ref. rigidity & particle) =   1.243996E-03 s 

************************************************************************************************************************************
     17  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  2.877804E-03  1.512832E-03  3.004175E-03  2.113334E-01  2.721995E+00   9.9797121E+02   1.24398E+03
TRAJ #1 SX, SY, SZ, |S| :  1   -8.938199E-01   3.304937E-01   3.030840E-01   1.000000E+00

 Cumulative length of optical axis =    9.97994502     m   ;  Time  (for reference rigidity & particle) =   1.243999E-03 s 

************************************************************************************************************************************
     18  Keyword, label(s) :  BEND        DIP                   3                   


      +++++        BEND  : 

                Length    =   2.473004E+02 cm
                Arc length    =   2.481837E+02 cm
                Deviation    =   2.250000E+01 deg.,    3.926991E-01 rad
                GAP   =   0.000000E+00 cm
                Gradient   =   0.000000E+00 kG/cm
                Grad-prime   =   0.000000E+00 kG/cm^2

                Field  =  7.9409999E+00  kG   (i.e.,   1.5777600E+00 * SCAL)
                Reference curvature radius (Brho/B) =   6.3199472E+02 cm
                Skew  angle  =   0.000000E+00  rad

               Entrance  face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

               Exit      face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

                    Field has been * by scaling factor    5.0330848    

     KPOS =  3 :  automatic positioning of element, 
        XCE, YCE, ALE = -4.2093663330E-03 -2.1161913603E-02 -0.1963495408     cm/cm/rad

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000          287.300     0.023    -0.196     0.886     0.003            1

     KPOS =  3.  Automatic  positionning  of  element.
          X = -4.2094E-03 CM   Y = -2.1162E-02 cm,  tilt  angle = -0.196350     RAD


 Cumulative length of optical axis =    12.4689110     m ;  Time  (for ref. rigidity & particle) =   1.244009E-03 s 

************************************************************************************************************************************
     19  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  2.877804E-03  1.976063E-03  2.597013E-03  1.076122E+00  2.657502E+00   1.3184834E+03   1.24399E+03
TRAJ #1 SX, SY, SZ, |S| :  1    1.109193E-01   9.449746E-01   3.077660E-01   1.000000E+00

 Cumulative length of optical axis =    13.1851670     m   ;  Time  (for reference rigidity & particle) =   1.244012E-03 s 

************************************************************************************************************************************
     20  Keyword, label(s) :  MULTIPOL    QP                    5                   


      -----  MULTIPOLE   : 
                Length  of  element  =    48.627300      cm
                Bore  radius      RO =    10.000      cm
               B-DIPOLE      =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-QUADRUPOLE  = -3.8529925E+00 kG   (i.e.,  -7.6553300E-01 * SCAL)
               B-SEXTUPOLE   =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0330848    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           48.627     0.002     0.000     1.106    -0.001            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    13.6714400     m ;  Time  (for ref. rigidity & particle) =   1.244014E-03 s 

************************************************************************************************************************************
     21  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  2.877804E-03  3.033782E-03  1.041559E-02  1.000574E+00 -1.465583E+00   1.4387364E+03   1.24399E+03
TRAJ #1 SX, SY, SZ, |S| :  1    1.164456E-01   9.449785E-01   3.057058E-01   1.000000E+00

 Cumulative length of optical axis =    14.3876960     m   ;  Time  (for reference rigidity & particle) =   1.244016E-03 s 

************************************************************************************************************************************
     22  Keyword, label(s) :  BEND        DIP                   3                   


      +++++        BEND  : 

                Length    =   2.473004E+02 cm
                Arc length    =   2.481837E+02 cm
                Deviation    =   2.250000E+01 deg.,    3.926991E-01 rad
                GAP   =   0.000000E+00 cm
                Gradient   =   0.000000E+00 kG/cm
                Grad-prime   =   0.000000E+00 kG/cm^2

                Field  =  7.9409999E+00  kG   (i.e.,   1.5777600E+00 * SCAL)
                Reference curvature radius (Brho/B) =   6.3199472E+02 cm
                Skew  angle  =   0.000000E+00  rad

               Entrance  face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

               Exit      face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

                    Field has been * by scaling factor    5.0330848    

     KPOS =  3 :  automatic positioning of element, 
        XCE, YCE, ALE = -4.2093663330E-03 -2.1161913603E-02 -0.1963495408     cm/cm/rad

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000          287.300     0.028    -0.196     0.621    -0.002            1

     KPOS =  3.  Automatic  positionning  of  element.
          X = -4.2094E-03 CM   Y = -2.1162E-02 cm,  tilt  angle = -0.196350     RAD


 Cumulative length of optical axis =    16.8766620     m ;  Time  (for ref. rigidity & particle) =   1.244026E-03 s 

************************************************************************************************************************************
     23  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  2.877804E-03  6.678320E-03  9.089840E-03  5.093269E-01 -1.560921E+00   1.7592491E+03   1.24401E+03
TRAJ #1 SX, SY, SZ, |S| :  1    9.473578E-01   1.088219E-01   3.011162E-01   1.000000E+00

 Cumulative length of optical axis =    17.5929180     m   ;  Time  (for reference rigidity & particle) =   1.244029E-03 s 

************************************************************************************************************************************
     24  Keyword, label(s) :  QUADRUPO    QP                    1                   


      -----  QUADRUPOLE  : 
                Length  of  element  =    46.723000      cm
                Bore  radius      RO =    10.000      cm
               B-QUADRUPOLE  =  3.8437417E+00 kG   (i.e.,   7.6369500E-01 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0330848    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           46.723     0.007    -0.000     0.477     0.000            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    18.0601480     m ;  Time  (for ref. rigidity & particle) =   1.244031E-03 s 

************************************************************************************************************************************
     25  Keyword, label(s) :  ESL                                                   


                              Drift,  length =   392.14800  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  2.877804E-03  7.266411E-04 -1.483031E-02  5.462020E-01  1.754430E-01   2.1981201E+03   1.24402E+03
TRAJ #1 SX, SY, SZ, |S| :  1    9.450437E-01   1.087225E-01   3.083372E-01   1.000000E+00

 Cumulative length of optical axis =    21.9816280     m   ;  Time  (for reference rigidity & particle) =   1.244046E-03 s 

************************************************************************************************************************************
     26  Keyword, label(s) :  MULTIPOL    QP                    5                   


      -----  MULTIPOLE   : 
                Length  of  element  =    48.627300      cm
                Bore  radius      RO =    10.000      cm
               B-DIPOLE      =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-QUADRUPOLE  = -3.8529925E+00 kG   (i.e.,  -7.6553300E-01 * SCAL)
               B-SEXTUPOLE   =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0330848    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           48.627     0.000    -0.000     0.506    -0.002            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    22.4679010     m ;  Time  (for ref. rigidity & particle) =   1.244048E-03 s 

************************************************************************************************************************************
     27  Keyword, label(s) :  ESL                                                   


                              Drift,  length =   392.14800  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  2.877804E-03 -5.206713E-03 -1.340567E-02 -2.050557E-01 -1.812678E+00   2.6388961E+03   1.24404E+03
TRAJ #1 SX, SY, SZ, |S| :  1    9.477012E-01   1.087284E-01   3.000677E-01   1.000000E+00

 Cumulative length of optical axis =    26.3893810     m   ;  Time  (for reference rigidity & particle) =   1.244064E-03 s 

************************************************************************************************************************************
     28  Keyword, label(s) :  QUADRUPO    QP                    1                   


      -----  QUADRUPOLE  : 
                Length  of  element  =    46.723000      cm
                Bore  radius      RO =    10.000      cm
               B-QUADRUPOLE  =  3.8437417E+00 kG   (i.e.,   7.6369500E-01 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0330848    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           46.723    -0.005     0.000    -0.309    -0.003            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    26.8566110     m ;  Time  (for ref. rigidity & particle) =   1.244065E-03 s 

************************************************************************************************************************************
     29  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  2.877804E-03 -4.975000E-03  5.763022E-03 -5.041386E-01 -2.718073E+00   2.7572451E+03   1.24405E+03
TRAJ #1 SX, SY, SZ, |S| :  1    9.488786E-01   1.088083E-01   2.962941E-01   1.000000E+00

 Cumulative length of optical axis =    27.5728670     m   ;  Time  (for reference rigidity & particle) =   1.244068E-03 s 

************************************************************************************************************************************
     30  Keyword, label(s) :  BEND        DIP                   3                   


      +++++        BEND  : 

                Length    =   2.473004E+02 cm
                Arc length    =   2.481837E+02 cm
                Deviation    =   2.250000E+01 deg.,    3.926991E-01 rad
                GAP   =   0.000000E+00 cm
                Gradient   =   0.000000E+00 kG/cm
                Grad-prime   =   0.000000E+00 kG/cm^2

                Field  =  7.9409999E+00  kG   (i.e.,   1.5777600E+00 * SCAL)
                Reference curvature radius (Brho/B) =   6.3199472E+02 cm
                Skew  angle  =   0.000000E+00  rad

               Entrance  face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

               Exit      face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

                    Field has been * by scaling factor    5.0330848    

     KPOS =  3 :  automatic positioning of element, 
        XCE, YCE, ALE = -4.2093663330E-03 -2.1161913603E-02 -0.1963495408     cm/cm/rad

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000          287.300     0.018    -0.196    -1.173    -0.003            1

     KPOS =  3.  Automatic  positionning  of  element.
          X = -4.2094E-03 CM   Y = -2.1162E-02 cm,  tilt  angle = -0.196350     RAD


 Cumulative length of optical axis =    30.0618330     m ;  Time  (for ref. rigidity & particle) =   1.244078E-03 s 

************************************************************************************************************************************
     31  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  2.877804E-03 -3.397888E-03  8.004905E-03 -1.360934E+00 -2.619421E+00   3.0777549E+03   1.24406E+03
TRAJ #1 SX, SY, SZ, |S| :  1    3.286210E-01  -8.948416E-01   3.021040E-01   1.000000E+00

 Cumulative length of optical axis =    30.7780890     m   ;  Time  (for reference rigidity & particle) =   1.244081E-03 s 

************************************************************************************************************************************
     32  Keyword, label(s) :  MULTIPOL    QP                    5                   


      -----  MULTIPOLE   : 
                Length  of  element  =    48.627300      cm
                Bore  radius      RO =    10.000      cm
               B-DIPOLE      =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-QUADRUPOLE  = -3.8529925E+00 kG   (i.e.,  -7.6553300E-01 * SCAL)
               B-SEXTUPOLE   =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0330848    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           48.627    -0.003    -0.000    -1.363     0.003            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    31.2643620     m ;  Time  (for ref. rigidity & particle) =   1.244083E-03 s 

************************************************************************************************************************************
     33  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  2.877804E-03 -3.616516E-03 -4.293546E-03 -1.182044E+00  2.528803E+00   3.1980081E+03   1.24406E+03
TRAJ #1 SX, SY, SZ, |S| :  1    3.216533E-01  -8.948592E-01   3.094612E-01   1.000000E+00

 Cumulative length of optical axis =    31.9806180     m   ;  Time  (for reference rigidity & particle) =   1.244086E-03 s 

************************************************************************************************************************************
     34  Keyword, label(s) :  BEND        DIP                   3                   


      +++++        BEND  : 

                Length    =   2.473004E+02 cm
                Arc length    =   2.481837E+02 cm
                Deviation    =   2.250000E+01 deg.,    3.926991E-01 rad
                GAP   =   0.000000E+00 cm
                Gradient   =   0.000000E+00 kG/cm
                Grad-prime   =   0.000000E+00 kG/cm^2

                Field  =  7.9409999E+00  kG   (i.e.,   1.5777600E+00 * SCAL)
                Reference curvature radius (Brho/B) =   6.3199472E+02 cm
                Skew  angle  =   0.000000E+00  rad

               Entrance  face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

               Exit      face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

                    Field has been * by scaling factor    5.0330848    

     KPOS =  3 :  automatic positioning of element, 
        XCE, YCE, ALE = -4.2093663330E-03 -2.1161913603E-02 -0.1963495408     cm/cm/rad

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000          287.300     0.018    -0.196    -0.535     0.003            1

     KPOS =  3.  Automatic  positionning  of  element.
          X = -4.2094E-03 CM   Y = -2.1162E-02 cm,  tilt  angle = -0.196350     RAD


 Cumulative length of optical axis =    34.4695841     m ;  Time  (for ref. rigidity & particle) =   1.244095E-03 s 

************************************************************************************************************************************
     35  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  2.877804E-03 -3.784126E-03 -2.750168E-03 -3.469413E-01  2.629783E+00   3.5185183E+03   1.24408E+03
TRAJ #1 SX, SY, SZ, |S| :  1   -7.959240E-01  -5.231533E-01   3.046565E-01   1.000000E+00

 Cumulative length of optical axis =    35.1858401     m   ;  Time  (for reference rigidity & particle) =   1.244098E-03 s 

************************************************************************************************************************************
     36  Keyword, label(s) :  QUADRUPO    QP                    1                   


      -----  QUADRUPOLE  : 
                Length  of  element  =    46.723000      cm
                Bore  radius      RO =    10.000      cm
               B-QUADRUPOLE  =  3.8437417E+00 kG   (i.e.,   7.6369500E-01 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0330848    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           46.723    -0.004     0.000    -0.250     0.002            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    35.6530701     m ;  Time  (for ref. rigidity & particle) =   1.244100E-03 s 

************************************************************************************************************************************
     37  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  2.877804E-03 -2.838313E-03  1.060636E-02 -1.368247E-01  1.579419E+00   3.6368671E+03   1.24408E+03
TRAJ #1 SX, SY, SZ, |S| :  1   -7.944784E-01  -5.232000E-01   3.083276E-01   1.000000E+00

 Cumulative length of optical axis =    36.3693261     m   ;  Time  (for reference rigidity & particle) =   1.244103E-03 s 

************************************************************************************************************************************
     38  Keyword, label(s) :  BEND        DIP                   3                   


      +++++        BEND  : 

                Length    =   2.473004E+02 cm
                Arc length    =   2.481837E+02 cm
                Deviation    =   2.250000E+01 deg.,    3.926991E-01 rad
                GAP   =   0.000000E+00 cm
                Gradient   =   0.000000E+00 kG/cm
                Grad-prime   =   0.000000E+00 kG/cm^2

                Field  =  7.9409999E+00  kG   (i.e.,   1.5777600E+00 * SCAL)
                Reference curvature radius (Brho/B) =   6.3199472E+02 cm
                Skew  angle  =   0.000000E+00  rad

               Entrance  face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

               Exit      face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

                    Field has been * by scaling factor    5.0330848    

     KPOS =  3 :  automatic positioning of element, 
        XCE, YCE, ALE = -4.2093663330E-03 -2.1161913603E-02 -0.1963495408     cm/cm/rad

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000          287.300     0.022    -0.196     0.258     0.002            1

     KPOS =  3.  Automatic  positionning  of  element.
          X = -4.2094E-03 CM   Y = -2.1162E-02 cm,  tilt  angle = -0.196350     RAD


 Cumulative length of optical axis =    38.8582921     m ;  Time  (for ref. rigidity & particle) =   1.244113E-03 s 

************************************************************************************************************************************
     39  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  2.877804E-03  1.129550E-03  1.294652E-02  3.709217E-01  1.572284E+00   3.9573772E+03   1.24409E+03
TRAJ #1 SX, SY, SZ, |S| :  1   -6.958029E-01   6.484428E-01   3.088369E-01   1.000000E+00

 Cumulative length of optical axis =    39.5745481     m   ;  Time  (for reference rigidity & particle) =   1.244115E-03 s 

************************************************************************************************************************************
     40  Keyword, label(s) :  MULTIPOL    QP                    5                   


      -----  MULTIPOLE   : 
                Length  of  element  =    48.627300      cm
                Bore  radius      RO =    10.000      cm
               B-DIPOLE      =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-QUADRUPOLE  = -3.8529925E+00 kG   (i.e.,  -7.6553300E-01 * SCAL)
               B-SEXTUPOLE   =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0330848    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           48.627     0.002     0.000     0.412     0.000            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    40.0608211     m ;  Time  (for ref. rigidity & particle) =   1.244117E-03 s 

************************************************************************************************************************************
     41  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  2.877804E-03  3.204901E-03  1.846889E-02  4.186570E-01  9.261528E-02   4.0776301E+03   1.24410E+03
TRAJ #1 SX, SY, SZ, |S| :  1   -6.937955E-01   6.484260E-01   3.133554E-01   1.000000E+00

 Cumulative length of optical axis =    40.7770771     m   ;  Time  (for reference rigidity & particle) =   1.244120E-03 s 

************************************************************************************************************************************
     42  Keyword, label(s) :  BEND        DIP                   3                   


      +++++        BEND  : 

                Length    =   2.473004E+02 cm
                Arc length    =   2.481837E+02 cm
                Deviation    =   2.250000E+01 deg.,    3.926991E-01 rad
                GAP   =   0.000000E+00 cm
                Gradient   =   0.000000E+00 kG/cm
                Grad-prime   =   0.000000E+00 kG/cm^2

                Field  =  7.9409999E+00  kG   (i.e.,   1.5777600E+00 * SCAL)
                Reference curvature radius (Brho/B) =   6.3199472E+02 cm
                Skew  angle  =   0.000000E+00  rad

               Entrance  face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

               Exit      face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

                    Field has been * by scaling factor    5.0330848    

     KPOS =  3 :  automatic positioning of element, 
        XCE, YCE, ALE = -4.2093663330E-03 -2.1161913603E-02 -0.1963495408     cm/cm/rad

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000          287.300     0.030    -0.196     0.436     0.000            1

     KPOS =  3.  Automatic  positionning  of  element.
          X = -4.2094E-03 CM   Y = -2.1162E-02 cm,  tilt  angle = -0.196350     RAD


 Cumulative length of optical axis =    43.2660431     m ;  Time  (for ref. rigidity & particle) =   1.244130E-03 s 

************************************************************************************************************************************
     43  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  2.877804E-03  9.126729E-03  1.778537E-02  4.386269E-01  4.243541E-02   4.3981426E+03   1.24411E+03
TRAJ #1 SX, SY, SZ, |S| :  1    4.675569E-01   8.263904E-01   3.137984E-01   1.000000E+00

 Cumulative length of optical axis =    43.9822991     m   ;  Time  (for reference rigidity & particle) =   1.244133E-03 s 

************************************************************************************************************************************
     44  Keyword, label(s) :  QUADRUPO    QP                    1                   


      -----  QUADRUPOLE  : 
                Length  of  element  =    46.723000      cm
                Bore  radius      RO =    10.000      cm
               B-QUADRUPOLE  =  3.8437417E+00 kG   (i.e.,   7.6369500E-01 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0330848    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           46.723     0.009    -0.000     0.478     0.002            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    44.4495291     m ;  Time  (for ref. rigidity & particle) =   1.244135E-03 s 

************************************************************************************************************************************
     45  Keyword, label(s) :  ESL                                                   


                              Drift,  length =   392.14800  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  2.877804E-03  3.166918E-03 -1.534528E-02  1.126737E+00  1.654980E+00   4.8370142E+03   1.24413E+03
TRAJ #1 SX, SY, SZ, |S| :  1    4.654415E-01   8.263224E-01   3.171047E-01   1.000000E+00

 Cumulative length of optical axis =    48.3710091     m   ;  Time  (for reference rigidity & particle) =   1.244150E-03 s 

************************************************************************************************************************************
     46  Keyword, label(s) :  MULTIPOL    QP                    5                   


      -----  MULTIPOLE   : 
                Length  of  element  =    48.627300      cm
                Bore  radius      RO =    10.000      cm
               B-DIPOLE      =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-QUADRUPOLE  = -3.8529925E+00 kG   (i.e.,  -7.6553300E-01 * SCAL)
               B-SEXTUPOLE   =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0330848    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           48.627     0.003    -0.000     1.104    -0.003            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    48.8572821     m ;  Time  (for ref. rigidity & particle) =   1.244152E-03 s 

************************************************************************************************************************************
     47  Keyword, label(s) :  ESL                                                   


                              Drift,  length =   392.14800  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  2.877804E-03  8.822473E-04 -4.607290E-03  9.986332E-02 -2.561512E+00   5.2777908E+03   1.24415E+03
TRAJ #1 SX, SY, SZ, |S| :  1    4.711989E-01   8.263445E-01   3.084256E-01   1.000000E+00

 Cumulative length of optical axis =    52.7787621     m   ;  Time  (for reference rigidity & particle) =   1.244167E-03 s 

************************************************************************************************************************************
     48  Keyword, label(s) :  QUADRUPO    QP                    1                   


      -----  QUADRUPOLE  : 
                Length  of  element  =    46.723000      cm
                Bore  radius      RO =    10.000      cm
               B-QUADRUPOLE  =  3.8437417E+00 kG   (i.e.,   7.6369500E-01 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0330848    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           46.723     0.001    -0.000    -0.015    -0.002            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    53.2459921     m ;  Time  (for ref. rigidity & particle) =   1.244169E-03 s 

************************************************************************************************************************************
     49  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  2.877804E-03  7.824753E-05 -7.289829E-03 -1.874710E-01 -2.411703E+00   5.3961398E+03   1.24415E+03
TRAJ #1 SX, SY, SZ, |S| :  1    4.710054E-01   8.263390E-01   3.087358E-01   1.000000E+00

 Cumulative length of optical axis =    53.9622481     m   ;  Time  (for reference rigidity & particle) =   1.244172E-03 s 

************************************************************************************************************************************
     50  Keyword, label(s) :  BEND        DIP                   3                   


      +++++        BEND  : 

                Length    =   2.473004E+02 cm
                Arc length    =   2.481837E+02 cm
                Deviation    =   2.250000E+01 deg.,    3.926991E-01 rad
                GAP   =   0.000000E+00 cm
                Gradient   =   0.000000E+00 kG/cm
                Grad-prime   =   0.000000E+00 kG/cm^2

                Field  =  7.9409999E+00  kG   (i.e.,   1.5777600E+00 * SCAL)
                Reference curvature radius (Brho/B) =   6.3199472E+02 cm
                Skew  angle  =   0.000000E+00  rad

               Entrance  face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

               Exit      face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

                    Field has been * by scaling factor    5.0330848    

     KPOS =  3 :  automatic positioning of element, 
        XCE, YCE, ALE = -4.2093663330E-03 -2.1161913603E-02 -0.1963495408     cm/cm/rad

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000          287.300     0.020    -0.196    -0.785    -0.002            1

     KPOS =  3.  Automatic  positionning  of  element.
          X = -4.2094E-03 CM   Y = -2.1162E-02 cm,  tilt  angle = -0.196350     RAD


 Cumulative length of optical axis =    56.4512141     m ;  Time  (for ref. rigidity & particle) =   1.244182E-03 s 

************************************************************************************************************************************
     51  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  2.877804E-03 -2.324621E-03 -6.042815E-03 -9.536713E-01 -2.354537E+00   5.7166507E+03   1.24416E+03
TRAJ #1 SX, SY, SZ, |S| :  1    9.139428E-01  -2.619717E-01   3.099667E-01   1.000000E+00

 Cumulative length of optical axis =    57.1674701     m   ;  Time  (for reference rigidity & particle) =   1.244184E-03 s 

************************************************************************************************************************************
     52  Keyword, label(s) :  MULTIPOL    QP                    5                   


      -----  MULTIPOLE   : 
                Length  of  element  =    48.627300      cm
                Bore  radius      RO =    10.000      cm
               B-DIPOLE      =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-QUADRUPOLE  = -3.8529925E+00 kG   (i.e.,  -7.6553300E-01 * SCAL)
               B-SEXTUPOLE   =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0330848    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           48.627    -0.003    -0.000    -0.980     0.001            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    57.6537431     m ;  Time  (for ref. rigidity & particle) =   1.244186E-03 s 

************************************************************************************************************************************
     53  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  2.877804E-03 -3.952278E-03 -1.551503E-02 -8.866556E-01  1.299335E+00   5.8369037E+03   1.24417E+03
TRAJ #1 SX, SY, SZ, |S| :  1    9.088364E-01  -2.620096E-01   3.246033E-01   1.000000E+00

 Cumulative length of optical axis =    58.3699991     m   ;  Time  (for reference rigidity & particle) =   1.244189E-03 s 

************************************************************************************************************************************
     54  Keyword, label(s) :  BEND        DIP                   3                   


      +++++        BEND  : 

                Length    =   2.473004E+02 cm
                Arc length    =   2.481837E+02 cm
                Deviation    =   2.250000E+01 deg.,    3.926991E-01 rad
                GAP   =   0.000000E+00 cm
                Gradient   =   0.000000E+00 kG/cm
                Grad-prime   =   0.000000E+00 kG/cm^2

                Field  =  7.9409999E+00  kG   (i.e.,   1.5777600E+00 * SCAL)
                Reference curvature radius (Brho/B) =   6.3199472E+02 cm
                Skew  angle  =   0.000000E+00  rad

               Entrance  face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

               Exit      face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

                    Field has been * by scaling factor    5.0330848    

     KPOS =  3 :  automatic positioning of element, 
        XCE, YCE, ALE = -4.2093663330E-03 -2.1161913603E-02 -0.1963495408     cm/cm/rad

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000          287.300     0.015    -0.196    -0.550     0.001            1

     KPOS =  3.  Automatic  positionning  of  element.
          X = -4.2094E-03 CM   Y = -2.1162E-02 cm,  tilt  angle = -0.196350     RAD


 Cumulative length of optical axis =    60.8589651     m ;  Time  (for ref. rigidity & particle) =   1.244199E-03 s 

************************************************************************************************************************************
     55  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  2.877804E-03 -7.718498E-03 -1.165525E-02 -4.511637E-01  1.383773E+00   6.1574122E+03   1.24418E+03
TRAJ #1 SX, SY, SZ, |S| :  1   -4.221895E-02  -9.446331E-01   3.254012E-01   1.000000E+00

 Cumulative length of optical axis =    61.5752211     m   ;  Time  (for reference rigidity & particle) =   1.244202E-03 s 

************************************************************************************************************************************
     56  Keyword, label(s) :  QUADRUPO    QP                    1                   


      -----  QUADRUPOLE  : 
                Length  of  element  =    46.723000      cm
                Bore  radius      RO =    10.000      cm
               B-QUADRUPOLE  =  3.8437417E+00 kG   (i.e.,   7.6369500E-01 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0330848    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           46.723    -0.008     0.000    -0.423    -0.000            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    62.0424511     m ;  Time  (for ref. rigidity & particle) =   1.244204E-03 s 

************************************************************************************************************************************
     57  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  2.877804E-03 -6.461445E-03  1.608508E-02 -4.338773E-01 -1.542111E-01   6.2757608E+03   1.24418E+03
TRAJ #1 SX, SY, SZ, |S| :  1   -3.990323E-02  -9.446380E-01   3.256788E-01   1.000000E+00

 Cumulative length of optical axis =    62.7587071     m   ;  Time  (for reference rigidity & particle) =   1.244206E-03 s 

************************************************************************************************************************************
     58  Keyword, label(s) :  BEND        DIP                   3                   


      +++++        BEND  : 

                Length    =   2.473004E+02 cm
                Arc length    =   2.481837E+02 cm
                Deviation    =   2.250000E+01 deg.,    3.926991E-01 rad
                GAP   =   0.000000E+00 cm
                Gradient   =   0.000000E+00 kG/cm
                Grad-prime   =   0.000000E+00 kG/cm^2

                Field  =  7.9409999E+00  kG   (i.e.,   1.5777600E+00 * SCAL)
                Reference curvature radius (Brho/B) =   6.3199472E+02 cm
                Skew  angle  =   0.000000E+00  rad

               Entrance  face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

               Exit      face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

                    Field has been * by scaling factor    5.0330848    

     KPOS =  3 :  automatic positioning of element, 
        XCE, YCE, ALE = -4.2093663330E-03 -2.1161913603E-02 -0.1963495408     cm/cm/rad

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000          287.300     0.020    -0.196    -0.466    -0.000            1

     KPOS =  3.  Automatic  positionning  of  element.
          X = -4.2094E-03 CM   Y = -2.1162E-02 cm,  tilt  angle = -0.196350     RAD


 Cumulative length of optical axis =    65.2476731     m ;  Time  (for ref. rigidity & particle) =   1.244216E-03 s 

************************************************************************************************************************************
     59  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  2.877804E-03 -3.857343E-04  2.014176E-02 -4.731766E-01 -1.013575E-01   6.5962695E+03   1.24420E+03
TRAJ #1 SX, SY, SZ, |S| :  1   -9.281222E-01  -1.829421E-01   3.242242E-01   1.000000E+00

 Cumulative length of optical axis =    65.9639291     m   ;  Time  (for reference rigidity & particle) =   1.244219E-03 s 

************************************************************************************************************************************
     60  Keyword, label(s) :  MULTIPOL    QP                    5                   


      -----  MULTIPOLE   : 
                Length  of  element  =    48.627300      cm
                Bore  radius      RO =    10.000      cm
               B-DIPOLE      =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-QUADRUPOLE  = -3.8529925E+00 kG   (i.e.,  -7.6553300E-01 * SCAL)
               B-SEXTUPOLE   =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0330848    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           48.627     0.001     0.000    -0.436     0.002            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    66.4502021     m ;  Time  (for ref. rigidity & particle) =   1.244221E-03 s 

************************************************************************************************************************************
     61  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  2.877804E-03  2.057325E-03  2.051283E-02 -3.199944E-01  1.616445E+00   6.7165225E+03   1.24420E+03
TRAJ #1 SX, SY, SZ, |S| :  1   -9.305433E-01  -1.829436E-01   3.172077E-01   1.000000E+00

 Cumulative length of optical axis =    67.1664581     m   ;  Time  (for reference rigidity & particle) =   1.244224E-03 s 

************************************************************************************************************************************
     62  Keyword, label(s) :  BEND        DIP                   3                   


      +++++        BEND  : 

                Length    =   2.473004E+02 cm
                Arc length    =   2.481837E+02 cm
                Deviation    =   2.250000E+01 deg.,    3.926991E-01 rad
                GAP   =   0.000000E+00 cm
                Gradient   =   0.000000E+00 kG/cm
                Grad-prime   =   0.000000E+00 kG/cm^2

                Field  =  7.9409999E+00  kG   (i.e.,   1.5777600E+00 * SCAL)
                Reference curvature radius (Brho/B) =   6.3199472E+02 cm
                Skew  angle  =   0.000000E+00  rad

               Entrance  face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

               Exit      face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

                    Field has been * by scaling factor    5.0330848    

     KPOS =  3 :  automatic positioning of element, 
        XCE, YCE, ALE = -4.2093663330E-03 -2.1161913603E-02 -0.1963495408     cm/cm/rad

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000          287.300     0.029    -0.196     0.087     0.002            1

     KPOS =  3.  Automatic  positionning  of  element.
          X = -4.2094E-03 CM   Y = -2.1162E-02 cm,  tilt  angle = -0.196350     RAD


 Cumulative length of optical axis =    69.6554241     m ;  Time  (for ref. rigidity & particle) =   1.244234E-03 s 

************************************************************************************************************************************
     63  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  2.877804E-03  8.721425E-03  1.983383E-02  2.037948E-01  1.630134E+00   7.0370351E+03   1.24421E+03
TRAJ #1 SX, SY, SZ, |S| :  1   -3.973834E-01   8.609230E-01   3.176445E-01   1.000000E+00

 Cumulative length of optical axis =    70.3716801     m   ;  Time  (for reference rigidity & particle) =   1.244236E-03 s 

************************************************************************************************************************************
     64  Keyword, label(s) :  QUADRUPO    QP                    1                   


      -----  QUADRUPOLE  : 
                Length  of  element  =    46.723000      cm
                Bore  radius      RO =    10.000      cm
               B-QUADRUPOLE  =  3.8437417E+00 kG   (i.e.,   7.6369500E-01 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0330848    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           46.723     0.009    -0.000     0.299     0.003            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    70.8389101     m ;  Time  (for ref. rigidity & particle) =   1.244238E-03 s 

************************************************************************************************************************************
     65  Keyword, label(s) :  ESL                                                   


                              Drift,  length =   392.14800  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  2.877804E-03  4.176748E-03 -1.205891E-02  1.285759E+00  2.515473E+00   7.4759074E+03   1.24423E+03
TRAJ #1 SX, SY, SZ, |S| :  1   -3.984957E-01   8.609787E-01   3.160962E-01   1.000000E+00

 Cumulative length of optical axis =    74.7603901     m   ;  Time  (for reference rigidity & particle) =   1.244254E-03 s 

************************************************************************************************************************************
     66  Keyword, label(s) :  MULTIPOL    QP                    5                   


      -----  MULTIPOLE   : 
                Length  of  element  =    48.627300      cm
                Bore  radius      RO =    10.000      cm
               B-DIPOLE      =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-QUADRUPOLE  = -3.8529925E+00 kG   (i.e.,  -7.6553300E-01 * SCAL)
               B-SEXTUPOLE   =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0330848    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           48.627     0.004     0.000     1.290    -0.002            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    75.2466631     m ;  Time  (for ref. rigidity & particle) =   1.244255E-03 s 

************************************************************************************************************************************
     67  Keyword, label(s) :  ESL                                                   


                              Drift,  length =   392.14800  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  2.877804E-03  5.075730E-03  2.854674E-03  3.674588E-01 -2.352005E+00   7.9166838E+03   1.24425E+03
TRAJ #1 SX, SY, SZ, |S| :  1   -3.916992E-01   8.609528E-01   3.245490E-01   1.000000E+00

 Cumulative length of optical axis =    79.1681431     m   ;  Time  (for reference rigidity & particle) =   1.244271E-03 s 

************************************************************************************************************************************
     68  Keyword, label(s) :  QUADRUPO    QP                    1                   


      -----  QUADRUPOLE  : 
                Length  of  element  =    46.723000      cm
                Bore  radius      RO =    10.000      cm
               B-QUADRUPOLE  =  3.8437417E+00 kG   (i.e.,   7.6369500E-01 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0330848    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           46.723     0.005    -0.000     0.286    -0.001            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    79.6353731     m ;  Time  (for ref. rigidity & particle) =   1.244273E-03 s 

************************************************************************************************************************************
     69  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  2.877804E-03  3.714353E-03 -1.499214E-02  1.993871E-01 -1.202901E+00   8.0350326E+03   1.24425E+03
TRAJ #1 SX, SY, SZ, |S| :  1   -3.932658E-01   8.609836E-01   3.225667E-01   1.000000E+00

 Cumulative length of optical axis =    80.3516291     m   ;  Time  (for reference rigidity & particle) =   1.244276E-03 s 

************************************************************************************************************************************
     70  Keyword, label(s) :  BEND        DIP                   3                   


      +++++        BEND  : 

                Length    =   2.473004E+02 cm
                Arc length    =   2.481837E+02 cm
                Deviation    =   2.250000E+01 deg.,    3.926991E-01 rad
                GAP   =   0.000000E+00 cm
                Gradient   =   0.000000E+00 kG/cm
                Grad-prime   =   0.000000E+00 kG/cm^2

                Field  =  7.9409999E+00  kG   (i.e.,   1.5777600E+00 * SCAL)
                Reference curvature radius (Brho/B) =   6.3199472E+02 cm
                Skew  angle  =   0.000000E+00  rad

               Entrance  face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

               Exit      face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

                    Field has been * by scaling factor    5.0330848    

     KPOS =  3 :  automatic positioning of element, 
        XCE, YCE, ALE = -4.2093663330E-03 -2.1161913603E-02 -0.1963495408     cm/cm/rad

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000          287.300     0.022    -0.196    -0.103    -0.001            1

     KPOS =  3.  Automatic  positionning  of  element.
          X = -4.2094E-03 CM   Y = -2.1162E-02 cm,  tilt  angle = -0.196350     RAD


 Cumulative length of optical axis =    82.8405951     m ;  Time  (for ref. rigidity & particle) =   1.244285E-03 s 

************************************************************************************************************************************
     71  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  2.877804E-03 -7.692954E-04 -1.405413E-02 -1.895025E-01 -1.208553E+00   8.3555438E+03   1.24427E+03
TRAJ #1 SX, SY, SZ, |S| :  1    7.446844E-01   5.851965E-01   3.209208E-01   1.000000E+00

 Cumulative length of optical axis =    83.5568511     m   ;  Time  (for reference rigidity & particle) =   1.244288E-03 s 

************************************************************************************************************************************
     72  Keyword, label(s) :  MULTIPOL    QP                    5                   


      -----  MULTIPOLE   : 
                Length  of  element  =    48.627300      cm
                Bore  radius      RO =    10.000      cm
               B-DIPOLE      =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-QUADRUPOLE  = -3.8529925E+00 kG   (i.e.,  -7.6553300E-01 * SCAL)
               B-SEXTUPOLE   =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0330848    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           48.627    -0.002    -0.000    -0.230    -0.000            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    84.0431241     m ;  Time  (for ref. rigidity & particle) =   1.244290E-03 s 

************************************************************************************************************************************
     73  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  2.877804E-03 -2.854689E-03 -1.829638E-02 -2.594491E-01 -4.164647E-01   8.4757968E+03   1.24427E+03
TRAJ #1 SX, SY, SZ, |S| :  1    7.435736E-01   5.851826E-01   3.235114E-01   1.000000E+00

 Cumulative length of optical axis =    84.7593801     m   ;  Time  (for reference rigidity & particle) =   1.244293E-03 s 

************************************************************************************************************************************
     74  Keyword, label(s) :  BEND        DIP                   3                   


      +++++        BEND  : 

                Length    =   2.473004E+02 cm
                Arc length    =   2.481837E+02 cm
                Deviation    =   2.250000E+01 deg.,    3.926991E-01 rad
                GAP   =   0.000000E+00 cm
                Gradient   =   0.000000E+00 kG/cm
                Grad-prime   =   0.000000E+00 kG/cm^2

                Field  =  7.9409999E+00  kG   (i.e.,   1.5777600E+00 * SCAL)
                Reference curvature radius (Brho/B) =   6.3199472E+02 cm
                Skew  angle  =   0.000000E+00  rad

               Entrance  face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

               Exit      face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

                    Field has been * by scaling factor    5.0330848    

     KPOS =  3 :  automatic positioning of element, 
        XCE, YCE, ALE = -4.2093663330E-03 -2.1161913603E-02 -0.1963495408     cm/cm/rad

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000          287.300     0.015    -0.196    -0.359    -0.000            1

     KPOS =  3.  Automatic  positionning  of  element.
          X = -4.2094E-03 CM   Y = -2.1162E-02 cm,  tilt  angle = -0.196350     RAD


 Cumulative length of optical axis =    87.2483461     m ;  Time  (for ref. rigidity & particle) =   1.244303E-03 s 

************************************************************************************************************************************
     75  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  2.877804E-03 -7.773240E-03 -1.381490E-02 -3.865464E-01 -3.801192E-01   8.7963052E+03   1.24428E+03
TRAJ #1 SX, SY, SZ, |S| :  1    7.433885E-01  -5.844889E-01   3.251865E-01   1.000000E+00

 Cumulative length of optical axis =    87.9646021     m   ;  Time  (for reference rigidity & particle) =   1.244305E-03 s 

************************************************************************************************************************************
     76  Keyword, label(s) :  QUADRUPO    QP                    1                   


      -----  QUADRUPOLE  : 
                Length  of  element  =    46.723000      cm
                Bore  radius      RO =    10.000      cm
               B-QUADRUPOLE  =  3.8437417E+00 kG   (i.e.,   7.6369500E-01 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0330848    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           46.723    -0.008     0.000    -0.437    -0.002            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    88.4318321     m ;  Time  (for ref. rigidity & particle) =   1.244307E-03 s 

************************************************************************************************************************************
     77  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  2.877804E-03 -6.738176E-03  1.429293E-02 -5.685633E-01 -1.830166E+00   8.9146539E+03   1.24429E+03
TRAJ #1 SX, SY, SZ, |S| :  1    7.455181E-01  -5.843969E-01   3.204419E-01   1.000000E+00

 Cumulative length of optical axis =    89.1480881     m   ;  Time  (for reference rigidity & particle) =   1.244310E-03 s 

************************************************************************************************************************************
     78  Keyword, label(s) :  BEND        DIP                   3                   


      +++++        BEND  : 

                Length    =   2.473004E+02 cm
                Arc length    =   2.481837E+02 cm
                Deviation    =   2.250000E+01 deg.,    3.926991E-01 rad
                GAP   =   0.000000E+00 cm
                Gradient   =   0.000000E+00 kG/cm
                Grad-prime   =   0.000000E+00 kG/cm^2

                Field  =  7.9409999E+00  kG   (i.e.,   1.5777600E+00 * SCAL)
                Reference curvature radius (Brho/B) =   6.3199472E+02 cm
                Skew  angle  =   0.000000E+00  rad

               Entrance  face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

               Exit      face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

                    Field has been * by scaling factor    5.0330848    

     KPOS =  3 :  automatic positioning of element, 
        XCE, YCE, ALE = -4.2093663330E-03 -2.1161913603E-02 -0.1963495408     cm/cm/rad

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000          287.300     0.018    -0.196    -1.016    -0.002            1

     KPOS =  3.  Automatic  positionning  of  element.
          X = -4.2094E-03 CM   Y = -2.1162E-02 cm,  tilt  angle = -0.196350     RAD


 Cumulative length of optical axis =    91.6370541     m ;  Time  (for ref. rigidity & particle) =   1.244320E-03 s 

************************************************************************************************************************************
     79  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  2.877804E-03 -1.887263E-03  1.766727E-02 -1.140205E+00 -1.737031E+00   9.2351629E+03   1.24430E+03
TRAJ #1 SX, SY, SZ, |S| :  1   -3.931899E-01  -8.605548E-01   3.238010E-01   1.000000E+00

 Cumulative length of optical axis =    92.3533101     m   ;  Time  (for reference rigidity & particle) =   1.244323E-03 s 

************************************************************************************************************************************
     80  Keyword, label(s) :  MULTIPOL    QP                    5                   


      -----  MULTIPOLE   : 
                Length  of  element  =    48.627300      cm
                Bore  radius      RO =    10.000      cm
               B-DIPOLE      =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-QUADRUPOLE  = -3.8529925E+00 kG   (i.e.,  -7.6553300E-01 * SCAL)
               B-SEXTUPOLE   =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0330848    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           48.627    -0.001     0.000    -1.120     0.003            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    92.8395831     m ;  Time  (for ref. rigidity & particle) =   1.244325E-03 s 

************************************************************************************************************************************
     81  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  2.877804E-03 -3.122251E-04  1.205126E-02 -9.388942E-01  2.535412E+00   9.3554161E+03   1.24431E+03
TRAJ #1 SX, SY, SZ, |S| :  1   -3.992218E-01  -8.605451E-01   3.163607E-01   1.000000E+00

 Cumulative length of optical axis =    93.5558391     m   ;  Time  (for reference rigidity & particle) =   1.244327E-03 s 

************************************************************************************************************************************
     82  Keyword, label(s) :  BEND        DIP                   3                   


      +++++        BEND  : 

                Length    =   2.473004E+02 cm
                Arc length    =   2.481837E+02 cm
                Deviation    =   2.250000E+01 deg.,    3.926991E-01 rad
                GAP   =   0.000000E+00 cm
                Gradient   =   0.000000E+00 kG/cm
                Grad-prime   =   0.000000E+00 kG/cm^2

                Field  =  7.9409999E+00  kG   (i.e.,   1.5777600E+00 * SCAL)
                Reference curvature radius (Brho/B) =   6.3199472E+02 cm
                Skew  angle  =   0.000000E+00  rad

               Entrance  face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

               Exit      face  
                DX =     20.000    LAMBDA =      8.000
                Wedge  angle  =  0.042761 RD
                Fringe  field  coefficients :
                  0.24010  1.86390 -0.55720  0.39040  0.00000  0.00000

                    Field has been * by scaling factor    5.0330848    

     KPOS =  3 :  automatic positioning of element, 
        XCE, YCE, ALE = -4.2093663330E-03 -2.1161913603E-02 -0.1963495408     cm/cm/rad

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000          287.300     0.025    -0.196    -0.294     0.003            1

     KPOS =  3.  Automatic  positionning  of  element.
          X = -4.2094E-03 CM   Y = -2.1162E-02 cm,  tilt  angle = -0.196350     RAD


 Cumulative length of optical axis =    96.0448052     m ;  Time  (for ref. rigidity & particle) =   1.244337E-03 s 

************************************************************************************************************************************
     83  Keyword, label(s) :  ESL         SD                    2                   


                              Drift,  length =    71.62560  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  2.877804E-03  4.155301E-03  1.153309E-02 -1.072726E-01  2.607887E+00   9.6759282E+03   1.24432E+03
TRAJ #1 SX, SY, SZ, |S| :  1   -9.319057E-01   1.852988E-01   3.117950E-01   1.000000E+00

 Cumulative length of optical axis =    96.7610612     m   ;  Time  (for reference rigidity & particle) =   1.244340E-03 s 

************************************************************************************************************************************
     84  Keyword, label(s) :  QUADRUPO    QP                    1                   


      -----  QUADRUPOLE  : 
                Length  of  element  =    46.723000      cm
                Bore  radius      RO =    10.000      cm
               B-QUADRUPOLE  =  3.8437417E+00 kG   (i.e.,   7.6369500E-01 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0330848    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           46.723     0.004    -0.000     0.009     0.002            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    97.2282912     m ;  Time  (for ref. rigidity & particle) =   1.244342E-03 s 

************************************************************************************************************************************
     85  Keyword, label(s) :  ESL                                                   


                              Drift,  length =   392.14800  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  2.877804E-03  2.834454E-03 -3.833410E-03  9.637343E-01  2.434819E+00   1.0114800E+04   1.24434E+03
TRAJ #1 SX, SY, SZ, |S| :  1   -9.316558E-01   1.853617E-01   3.125038E-01   1.000000E+00

 Cumulative length of optical axis =    101.149771     m   ;  Time  (for reference rigidity & particle) =   1.244357E-03 s 

************************************************************************************************************************************
     86  Keyword, label(s) :  MULTIPOL    QP                    5                   


      -----  MULTIPOLE   : 
                Length  of  element  =    48.627300      cm
                Bore  radius      RO =    10.000      cm
               B-DIPOLE      =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-QUADRUPOLE  = -3.8529925E+00 kG   (i.e.,  -7.6553300E-01 * SCAL)
               B-SEXTUPOLE   =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    5.0330848    

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.458     0.000           48.627     0.003     0.000     0.993    -0.001            1

     KPOS =  1.  Change  of  frame  at  exit  of  element.
          X =   0.000     CM   Y =   0.000     cm,  tilt  angle =   0.00000     RAD


 Cumulative length of optical axis =    101.636044     m ;  Time  (for ref. rigidity & particle) =   1.244359E-03 s 

************************************************************************************************************************************
     87  Keyword, label(s) :  ESL                                                   


                              Drift,  length =   392.14800  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  2.877804E-03  5.525115E-03  6.687035E-03  4.975673E-01 -1.262551E+00   1.0555576E+04   1.24435E+03
TRAJ #1 SX, SY, SZ, |S| :  1   -9.264632E-01   1.853187E-01   3.276018E-01   1.000000E+00

 Cumulative length of optical axis =    105.557524     m   ;  Time  (for reference rigidity & particle) =   1.244375E-03 s 

************************************************************************************************************************************
     88  Keyword, label(s) :  MARKER      #EndRing                                  


************************************************************************************************************************************
     89  Keyword, label(s) :  CAVITE      85                                        

                Accelerating cavity. Type is :   OPTION 2  

                Cavite parameters saved in zgoubi.CAVITE.Out


                    Orbit  length           =     1.05555685E+02 m
                    RF  harmonic            =     3.00000000E+00
                    Peak  voltage           =     6.00000000E+03 V
                    RF  frequency           =     7.23557037E+06 Hz
                    Synchronous  phase      =     2.05303159E-01 rd
                    Isochronous  time       =     4.14618316E-07 s
                    qV.sin(phi_s)           =     1.22318377E-03 MeV
                    cos(phi_s)              =     9.78999226E-01 
                    Nu_s/sqrt(alpha)        =     1.25636575E-03  
                    dp-acc*sqrt(alpha)      =     7.08559423E-04  
                    dgamma/dt               =     3.70255923E+00 /s 
                    rho*dB/dt               =     1.15880426E+01 T.m/s 
                    SR loss, this pass      =     0.00000000E+00 MeV 


************************************************************************************************************************************
     90  Keyword, label(s) :  REBELOTE    90                                        


                         ****  End  of  'REBELOTE'  procedure  ****

      There  has  been       3000  passes  through  the  optical  structure 

                     Total of          1 particles have been launched

************************************************************************************************************************************
     91  Keyword, label(s) :  FAISCEAU                                              

0                                             TRACE DU FAISCEAU
                                           (follows element #     90)
                                                  1 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)       D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o  1   1.0000     0.000     0.000     0.458     0.000       0.0000    0.0029    0.006    0.007    0.498   -1.263   0.000000E+00     1
               Time of flight (mus) :   1244.3530     mass (MeV/c2) :   938.272    


------
  Characteristics of concentration ellipse (Surface, ALP, BET, <X>, <XP>, #prtcls, #prtcls inside ellips, ratio, space, pass#) : 

   0.0000E+00   0.0000E+00   1.0000E+00   5.525115E-05   6.687028E-06        1        1    1.000      (Y,T)      3001
   0.0000E+00   0.0000E+00   1.0000E+00   4.975673E-03  -1.262549E-03        1        1    1.000      (Z,P)      3001
   0.0000E+00   0.0000E+00   1.0000E+00   1.244353E+03   8.385526E+02        1        1    1.000      (t,K)      3001

(Y,T)  space (units : (cm,rd)   ) :  
      sigma_Y = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_T = sqrt(Surface/pi * (1+ALP^2)/BET) =   0.000000E+00

(Z,P)  space (units : (cm,rd)   ) :  
      sigma_Z = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_P = sqrt(Surface/pi * (1+ALP^2)/BET) =   0.000000E+00

(t,K)  space (units : (mu_s,MeV)) :  
      sigma_t = sqrt(Surface/pi * BET) =   0.000000E+00
      sigma_K = sqrt(Surface/pi * (1+ALP^2)/BET) =   0.000000E+00


  Beam  sigma  matrix : 

   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)

************************************************************************************************************************************
     92  Keyword, label(s) :  SPNPRT                                                



                         Momentum  group  #1 ; average  over 1 particles at this pass : 

                   INITIAL                                           FINAL

      <SX>       <SY>       <SZ>       <|S|>            <SX>       <SY>       <SZ>      <|S|>    <G.gma>    <(SI,SF)>  sigma_(SI,SF)
                                                                                                               (deg)       (deg)
   0.000000   0.000000   1.000000   1.000000        -0.926463   0.185319   0.327602   1.000000   3.395151  70.876723   0.000000


                Spin  components  of  each  of  the      1  particles,  and  rotation  angle :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   (Si,Sf)   (Z,Sf_X)  (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_X : projection of Sf on YZ plane)

 o  1  0.000000  0.000000  1.000000  1.000000    -0.926463  0.185319  0.327602  1.000000      1.8937   70.877   29.496   70.877    1



                Min/Max  components  of  each  of  the      1  particles :

  SX_mi       SX_ma       SY_mi       SY_ma       SZ_mi       SZ_ma       |S|_mi      |S|_ma      p/p_0        GAMMA          I  IEX

 -9.2646E-01 -9.2646E-01  1.8532E-01  1.8532E-01  3.2760E-01  3.2760E-01  1.0000E+00  1.0000E+00  1.00288E+00  1.89372E+00      1   1

************************************************************************************************************************************
     93  Keyword, label(s) :  END                                                   


************************************************************************************************************************************
 Pgm zgoubi : Execution ended normally, upon keyword END or FIN
   
            ZGOUBI RUN COMPLETED. 

  Zgoubi, author's dvlpmnt version.
  Job  started  on  06-03-2018,  at  16:23:23 
  JOB  ENDED  ON    06-03-2018,  AT  16:23:32 

   CPU time, total :     9.0554199999999998     
