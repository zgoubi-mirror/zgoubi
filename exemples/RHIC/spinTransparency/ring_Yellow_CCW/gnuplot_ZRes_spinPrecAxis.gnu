
set title "n0 at IP6"

set xtics
set ytics nomirror
set y2tics nomirror

set logscale y2

set key maxcol 1
set k t r
set grid
              
system "grep 'Precession axis :  (' zgoubi.res | cat > temp1"           # n0_X
system "sed -i 's@Precession axis :  (@ @g' temp1"
system "sed -i 's@) @  @g' temp1"
system "grep 'Spin precession/2pi (or Qs' zgoubi.res | cat > temp2"     # spin tune
system "grep 'Fit reached penalty value' zgoubi.res | cat > temp3"      # spin tune

plot [:14] \
 "temp1" u 0:($1) w lp pt 4 tit "n_{0,X}" ,\
 "temp1" u 0:($2) w lp pt 5 tit "n_{0,Y}" ,\
 "temp1" u 0:($3) w lp pt 6 tit "n_{0,Z}"  ,\
 "temp2" u 0:($7) w lp pt 8 lc rgb "black" tit "Q_{sp}"  

 set terminal postscript eps blacktext color enh 
 set output "gnuplot_ZRes_spinPrecAxis.eps"
 replot
 set terminal X11
 unset output

pause 2

exit
