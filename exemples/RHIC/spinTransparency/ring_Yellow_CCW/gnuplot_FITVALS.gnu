
set title "n0 at IP6"

set xtics
set ytics nomirror
set y2tics 

set logscale  y2

set key maxcol 1
set k t r
set grid
              
system "grep '3   1    40 ' fitVals_n0_IP6 | cat > temp1"                   # n0_X
system "grep '3   2    41 ' fitVals_n0_IP6 | cat > temp2"                   # n0_Y
system "grep '3   3    42 ' fitVals_n0_IP6 | cat > temp3"                   # n0_Z
system "grep 'Fit reached penalty value' fitVals_n0_IP6 | cat > temp4"      # penalty

r2d = 180. / (4.*atan(1.))

plot \
 "temp1" u 0:($6) w lp pt 4 tit "n_{0,X}" ,\
 "temp2" u 0:($6) w lp pt 5 tit "n_{0,Y}" ,\
 "temp3" u 0:($6) w lp pt 6 tit "n_{0,Z}" ,\
 "temp4" u 0:($5) axes x1y2 w lp pt 7 tit "penalty" 

 set terminal postscript eps blacktext color enh 
 set output "gnuplot_FITVALS_n0_IP6.eps"
 replot
 set terminal X11
 unset output

pause 2

exit
