'MARKER'   scaling_S

'SCALING'                                                                                     
1  10
BEND
-1
79.366778931425273
1
MULTIPOL
-1
79.366778931425273
1
MULTIPOL  *_QF*
-1
79.366778931425273 * 0.99844414
1
MULTIPOL *_QD*
-1
79.366778931425273 * 1.0014380
1
MULTIPOL  *_SXF*
-1
79.366778931425273 * .62
1
MULTIPOL  *_SXD*
-1
79.366778931425273 * 1.75
1
TOSCA    snk1LowB
-1
184.47838    !  low-field coils current (A)
1
TOSCA    snk1HighB
-1
198.70954    !  high-field coils current (A)
1
TOSCA    snk2LowB
-1
164.43807    !  low-field coils current (A)
1
TOSCA    snk2HighB
-1
220.65730    !  high-field coils current (A)
1

'MARKER'   scaling_E
'END'
