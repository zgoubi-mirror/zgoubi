
set tit "Spin component(s) vs. s  \n from zgoubi.fai"  

set xtics 
set ytics 

set xlabel 's  [m]' 
set ylabel 'n_{0,X},   n_{0,Y},   n_{0,Z}' 

set key maxrow 1
set key t l

# ELECTRON : 
# am = 0.511
# G = 1.1596e-3
# PROTON :
am = 938.27203e6
G = 1.79284735

m2cm = 100.
MeV2eV = 1e6
c = 2.99792458e8

set yrange [-1.15:1.15]

traj = 4   # Trajectory used for spin orbit FIT
# $53==1 flag tells that FIT has been completed.
# every is used in order to allow 'w line'

ym1 = -1
yp1 = 1

plot  [0:3384.] \
   "zgoubi.fai"  every 6::3 u ($14/m2cm):($26==traj && $53==1 ? $20 :1/0) w lp pt 5 ps .4 lc rgb "red" tit "n_{0,X}" ,\
   "zgoubi.fai"  every 6::3 u ($14/m2cm):($26==traj && $53==1 ? $21 :1/0) w lp pt 7 ps .4 lc rgb "green" tit "n_{0,Y}" ,\
   "zgoubi.fai"  every 6::3 u ($14/m2cm):($26==traj && $53==1 ? $22 :1/0) w lp pt 9 ps .4 lc rgb "blue" tit "n_{0,Z}" ,\
   ym1 w l lw .2 lc rgb "black" notit,  yp1 w l lw .2 lc rgb "black" notit

     set terminal postscript eps blacktext color  enh  
       set output "gnuplot_Zfai_s-SXYZ.eps"  
       replot  
       set terminal X11  
       unset output  

 
pause 3
exit

