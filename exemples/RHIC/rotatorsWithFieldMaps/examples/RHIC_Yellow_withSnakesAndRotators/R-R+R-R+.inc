R-R+R-R+ snake: 3'O:
!  Yell 9 o'clock CCW, or Blue 3 o'clock, or Yell 3 o'clock CW.
'OBJET'                                                                                                      1
851.249816894531 * 1.d3
2
3 1
0. 0. 0. 0. 0. 1. 'o'
0. 0. 0. 0. 0. 1. 'o'
0. 0. 0. 0. 0. 1. 'o'
1 1 1
'PARTICUL'                                                                                                   2
PROTON
'SPNTRK'                                                                                                     3
4
1. 0. 0.
0. 1. 0.
0. 0. 1.
 
'MARKER'  R-R+R-R+_S    ! axis +45deg.  240.+21.2+240.+22.4+22.4+240.+21.2+240. = 1047.2cm                   4
 
'DRIFT'                                                                                                      5
-78.200000
 
'DRIFT'                                                                                                      6
3.600000
'DRIFT'                                                                                                      7
16.400000
 
'TOSCA'      snk2LowB                                                                                        8
0  000  ! .plt
-1.00000000E+02  1.00000000E+00  1.00000000E+00  1.00000000E+00
HEADER_8   RHIC_helix
361 81 81  15.1  0.748502994012e-4     ! = 1/1.3360e4
b_model3a2a-x-4_4_y-4_4_z-180_180-integral.table
0 0 0 0
2
.3
2   0.00000000E+00  0.00000000E+00  0.00000000E+00
 
'DRIFT'    2.162m_C1toC2                                                                                     9
-98.800000
 
'TOSCA'      snk2HighB                                                                                      10
0  000  ! .plt
3.22000000E+02  1.00000000E+00  1.00000000E+00  1.00000000E+00
HEADER_8   RHIC_helix
361 81 81  15.1  0.246305418719e-4   ! = 1/4.060e4
b_model3a2a322a-x-4_4_y-4_4_z-180_180-integral.table
0 0 0 0
2
.3
2   0.00000000E+00  0.00000000E+00  0.00000000E+00
 
'DRIFT'    YI3_B7.1  VMON  ! center of 3 oclock snake                                                       11
-37.6
'DRIFT'    2.162m_C2toC3                                                                                    12
-37.6
 
'TOSCA'      snk2HighB                                                                                      13
0  000  ! .plt
-3.22000000E+02  1.00000000E+00  1.00000000E+00  1.00000000E+00
HEADER_8   RHIC_helix
361 81 81  15.1  0.246305418719e-4   ! = 1/4.060e4
b_model3a2a322a-x-4_4_y-4_4_z-180_180-integral.table
0 0 0 0
2
.3
2   0.00000000E+00  0.00000000E+00  0.00000000E+00
 
'DRIFT'    2.162m_C3toC4                                                                                    14
-98.800000
 
'TOSCA'      snk2LowB                                                                                       15
0  000  ! .plt
1.00000000E+02  1.00000000E+00  1.00000000E+00  1.00000000E+00
HEADER_8   RHIC_helix
361 81 81  15.1  0.748502994012e-4     ! = 1/1.3360e4
b_model3a2a-x-4_4_y-4_4_z-180_180-integral.table
0 0 0 0
2
.3
2   0.00000000E+00  0.00000000E+00  0.00000000E+00
 
'DRIFT'                                                                                                     16
16.400000
 
'DRIFT'                                                                                                     17
-78.200000
 
'MARKER'  R-R+R-R+_E                                                                                        18
 
'FAISCEAU'                                                                                                  19
'SPNPRT' MATRIX                                                                                             20
 
'END'

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =     851249.817 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       3 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 1

     Particle  properties :
     PROTON
                     Mass          =    938.272        MeV/c2
                     Charge        =   1.602176E-19    C     
                     G  factor     =    1.79285              
                     COM life-time =   1.000000E+99    s     

              Reference  data :
                    mag. rigidity (kG.cm)   :   851249.82      =p/q, such that dev.=B*L/rigidity
                    mass (MeV/c2)           :   938.27208    
                    momentum (MeV/c)        :   255198.27    
                    energy, total (MeV)     :   255200.00    
                    energy, kinetic (MeV)   :   254261.73    
                    beta = v/c              :  0.9999932412    
                    gamma                   :   271.9893354    
                    beta*gamma              :   271.9874971    
                    G*gamma                 :   487.6353592    
                    m / G                   :   523.3418681    
                    electric rigidity (MeV) :   255196.5502    =T[eV]*(gamma+1)/gamma, such that dev.=E*L/rigidity
  
 I, AMQ(1,I), AMQ(2,I)/QE, P/Pref, v/c, time, s :
  
     1   9.38272081E+02  1.00000000E+00  1.00000000E+00  9.99993241E-01  0.00000000E+00  0.00000000E+00
     2   9.38272081E+02  1.00000000E+00  1.00000000E+00  9.99993241E-01  0.00000000E+00  0.00000000E+00
     3   9.38272081E+02  1.00000000E+00  1.00000000E+00  9.99993241E-01  0.00000000E+00  0.00000000E+00

************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 1


                Spin  tracking  requested.


                          Particle  mass          =    938.2721     MeV/c2
                          Gyromagnetic  factor  G =    1.792847    

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 3 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     851249.82     kG*cm
                               beta      =    0.99999324    
                               gamma     =     271.98934    
                               G*gamma   =     487.63536    
                               M / G     =     523.3418681    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        3  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  MARKER      R-R+R-R+_S                                                                   IPASS= 1


************************************************************************************************************************************
      5  Keyword, label(s) :  DRIFT                                                                                    IPASS= 1


                              Drift,  length =   -78.20000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00 -7.8200000E+01 -2.60849E-03
TRAJ #1 SX, SY, SZ, |S| :  1    1.000000E+00   0.000000E+00   0.000000E+00   1.000000E+00

 Cumulative length of optical axis =  -0.782000000     m   ;  Time  (for reference rigidity & particle) =  -2.608489E-09 s 

************************************************************************************************************************************
      6  Keyword, label(s) :  DRIFT                                                                                    IPASS= 1


                              Drift,  length =     3.60000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00 -7.4600000E+01 -2.48840E-03
TRAJ #1 SX, SY, SZ, |S| :  1    1.000000E+00   0.000000E+00   0.000000E+00   1.000000E+00

 Cumulative length of optical axis =  -0.746000000     m   ;  Time  (for reference rigidity & particle) =  -2.488405E-09 s 

************************************************************************************************************************************
      7  Keyword, label(s) :  DRIFT                                                                                    IPASS= 1


                              Drift,  length =    16.40000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00 -5.8200000E+01 -1.94136E-03
TRAJ #1 SX, SY, SZ, |S| :  1    1.000000E+00   0.000000E+00   0.000000E+00   1.000000E+00

 Cumulative length of optical axis =  -0.582000000     m   ;  Time  (for reference rigidity & particle) =  -1.941356E-09 s 

************************************************************************************************************************************
      8  Keyword, label(s) :  TOSCA       snk2LowB                                                                     IPASS= 1


     Status of MOD.MOD2 is 15.1 ; NDIM = 3 ; number of field data files used is   1.
     Field map set stored in field array HC(IMAP = 1)
     2-D map. MOD=15.  Single field map, with field coefficient value : 
                 7.485030E-05

     ZroBXY OPTION :  absent from map title. BX and BY values in Z=0 plane left as read.

           New field map(s) opened, cartesian mesh (MOD.le.19) ; 
           name(s) of map data file(s) : 

          'b_model3a2a-x-4_4_y-4_4_z-180_180-integral.table'

   ----
   Map file number    1 ( of 1 ) (unit # 24 ) :
     b_model3a2a-x-4_4_y-4_4_z-180_180-integral.table field map,
     FORMAT type : regular,
     field multiplication factor :  7.48502994E-05.

 HEADER  (8 lines) : 
               81          81         361           1                                                                        
      1 X [LENGU]                                                                                                            
      2 Y [LENGU]                                                                                                            
      3 Z [LENGU]                                                                                                            
      4 BX [FLUXU]                                                                                                           
      5 BY [FLUXU]                                                                                                           
      6 BZ [FLUXU]                                                                                                           
      0                                                                                                                      
R_min (cm), DR (cm), DTTA (deg), DZ (cm) :    0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00

  Sbr fmapw/fmapr3 : completed job of reading map.
     Field map scaled by factor  a(1) =   7.485030E-05

     Min/max fields found in map (series) read   :   1.073060E+03  /  -1.073124E+03
       @  X,  Y, Z :  -52.0      -4.00      -2.90                  /   52.0      -4.00       2.90    
     Given normalisation coeffs on field, x, y, z :  -1.336759E+01   1.000000E+00   1.000000E+00   1.000000E+00
     this yields min/max normalised fields (kG) : -1.434422E+04                      1.434508E+04
       @  X (cm),  Y (cm), Z (cm) :  -52.0      -4.00      -2.90   /   52.0      -4.00       2.90    

     Length of element,  XL =  3.600000E+02 cm 
                                               from  XI =  -1.800000E+02 cm 
                                               to    XF =   1.800000E+02 cm 

     Nbr of nodes in X = 361;  nbr of nodes in Y =   81
     X-size of mesh =  1.000000E+00 cm ; Y-size =  1.000000E-01 cm
     nber of mesh nodes in Z =   81 ; Step in Z =  0.100000E+00 cm

                     OPTION  DE  CALCUL  :  GRILLE  3-D  A  3*3*3  POINTS , INTERPOLATION  A  L'ORDRE  2

                    Integration step :  0.3000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000          180.000    -0.001    -0.000     0.144    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000    -0.001    -0.000     0.144    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000    -0.001    -0.000     0.144    -0.000            3

     QUASEX - KPOS =  2 :  Element  is  mis-aligned  wrt.  the  optical  axis.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    3.01800000     m ;  Time  (for ref. rigidity & particle) =   1.006703E-08 s 

************************************************************************************************************************************
      9  Keyword, label(s) :  DRIFT       2.162m_C1toC2                                                                IPASS= 1


                              Drift,  length =   -98.80000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -3.596255E-04 -4.533832E-03  1.436315E-01 -3.348251E-05  2.0300009E+02  6.77140E-03
TRAJ #1 SX, SY, SZ, |S| :  1    9.974753E-01   7.045371E-02  -8.903594E-03   1.000000E+00

 Cumulative length of optical axis =    2.03000000     m   ;  Time  (for reference rigidity & particle) =   6.771397E-09 s 

************************************************************************************************************************************
     10  Keyword, label(s) :  TOSCA       snk2HighB                                                                    IPASS= 1


     Status of MOD.MOD2 is 15.1 ; NDIM = 3 ; number of field data files used is   1.
     Field map set stored in field array HC(IMAP = 2)
     2-D map. MOD=15.  Single field map, with field coefficient value : 
                 2.463054E-05

     ZroBXY OPTION :  absent from map title. BX and BY values in Z=0 plane left as read.

           New field map(s) opened, cartesian mesh (MOD.le.19) ; 
           name(s) of map data file(s) : 

          'b_model3a2a322a-x-4_4_y-4_4_z-180_180-integral.table'

   ----
   Map file number    1 ( of 1 ) (unit # 25 ) :
     b_model3a2a322a-x-4_4_y-4_4_z-180_180-integral.table field map,
     FORMAT type : regular,
     field multiplication factor :  2.46305419E-05.

 HEADER  (8 lines) : 
               81          81         361           1                                                                        
      1 X [LENGU]                                                                                                            
      2 Y [LENGU]                                                                                                            
      3 Z [LENGU]                                                                                                            
      4 BX [FLUXU]                                                                                                           
      5 BY [FLUXU]                                                                                                           
      6 BZ [FLUXU]                                                                                                           
      0                                                                                                                      
R_min (cm), DR (cm), DTTA (deg), DZ (cm) :    0.00000000E+00   0.00000000E+00   0.00000000E+00   1.00000000E-01

  Sbr fmapw/fmapr3 : completed job of reading map.
     Field map scaled by factor  a(1) =   2.463054E-05

     Min/max fields found in map (series) read   :  -1.083566E+03  /   1.083781E+03
       @  X,  Y, Z :  -52.0      -4.00      -2.90                  /   52.0      -4.00       2.90    
     Given normalisation coeffs on field, x, y, z :   4.065586E+01   1.000000E+00   1.000000E+00   1.000000E+00
     this yields min/max normalised fields (kG) : -4.405332E+04                      4.406205E+04
       @  X (cm),  Y (cm), Z (cm) :  -52.0      -4.00      -2.90   /   52.0      -4.00       2.90    

     Length of element,  XL =  3.600000E+02 cm 
                                               from  XI =  -1.800000E+02 cm 
                                               to    XF =   1.800000E+02 cm 

     Nbr of nodes in X = 361;  nbr of nodes in Y =   81
     X-size of mesh =  1.000000E+00 cm ; Y-size =  1.000000E-01 cm
     nber of mesh nodes in Z =   81 ; Step in Z =  0.100000E+00 cm

                     OPTION  DE  CALCUL  :  GRILLE  3-D  A  3*3*3  POINTS , INTERPOLATION  A  L'ORDRE  2

                    Integration step :  0.3000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000          180.000     0.005     0.000    -0.290     0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000     0.005     0.000    -0.290     0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000     0.005     0.000    -0.290     0.000            3

     QUASEX - KPOS =  2 :  Element  is  mis-aligned  wrt.  the  optical  axis.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    5.63000000     m ;  Time  (for ref. rigidity & particle) =   1.877979E-08 s 

************************************************************************************************************************************
     11  Keyword, label(s) :  DRIFT       YI3_B7.1              VMON                                                   IPASS= 1


                              Drift,  length =   -37.60000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  3.673105E-03  3.412066E-02 -2.900210E-01  2.480341E-04  5.2540087E+02  1.75256E-02
TRAJ #1 SX, SY, SZ, |S| :  1    3.788703E-01  -6.102483E-01   6.957402E-01   1.000000E+00

 Cumulative length of optical axis =    5.25400000     m   ;  Time  (for reference rigidity & particle) =   1.752558E-08 s 

************************************************************************************************************************************
     12  Keyword, label(s) :  DRIFT       2.162m_C2toC3                                                                IPASS= 1


                              Drift,  length =   -37.60000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  2.390168E-03  3.412066E-02 -2.900304E-01  2.480341E-04  4.8780087E+02  1.62714E-02
TRAJ #1 SX, SY, SZ, |S| :  1    3.788703E-01  -6.102483E-01   6.957402E-01   1.000000E+00

 Cumulative length of optical axis =    4.87800000     m   ;  Time  (for reference rigidity & particle) =   1.627137E-08 s 

************************************************************************************************************************************
     13  Keyword, label(s) :  TOSCA       snk2HighB                                                                    IPASS= 1


     Status of MOD.MOD2 is 15.1 ; NDIM = 3 ; number of field data files used is   1.
     Field map set stored in field array HC(IMAP = 2)
     2-D map. MOD=15.  Single field map, with field coefficient value : 
                 2.463054E-05

     ZroBXY OPTION :  absent from map title. BX and BY values in Z=0 plane left as read.

     No  map  file  opened :  was  already  stored.
     Skip  reading  field  map  file :           b_model3a2a322a-x-4_4_y-4_4_z-180_180-integral.table
     Restored mesh coordinates for field map #IMAP=   2,  name : b_model3a2a322a-x-4_4_y-4_4_z-180_180-integral.table
     Field map scaled by factor  a(1) =   2.463054E-05

     Min/max fields found in map (series) read   :   1.083566E+03  /  -1.083781E+03
       @  X,  Y, Z :  -52.0      -4.00      -2.90                  /   52.0      -4.00       2.90    
     Given normalisation coeffs on field, x, y, z :  -4.065586E+01   1.000000E+00   1.000000E+00   1.000000E+00
     this yields min/max normalised fields (kG) : -4.405332E+04                      4.406205E+04
       @  X (cm),  Y (cm), Z (cm) :  -52.0      -4.00      -2.90   /   52.0      -4.00       2.90    

     Length of element,  XL =  3.600000E+02 cm 
                                               from  XI =  -1.800000E+02 cm 
                                               to    XF =   1.800000E+02 cm 

     Nbr of nodes in X = 361;  nbr of nodes in Y =   81
     X-size of mesh =  1.000000E+00 cm ; Y-size =  1.000000E-01 cm
     nber of mesh nodes in Z =   81 ; Step in Z =  0.100000E+00 cm

                     OPTION  DE  CALCUL  :  GRILLE  3-D  A  3*3*3  POINTS , INTERPOLATION  A  L'ORDRE  2

                    Integration step :  0.3000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000          180.000     0.008    -0.000     0.144     0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000     0.008    -0.000     0.144     0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000     0.008    -0.000     0.144     0.000            3

     QUASEX - KPOS =  2 :  Element  is  mis-aligned  wrt.  the  optical  axis.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    8.47800000     m ;  Time  (for ref. rigidity & particle) =   2.827976E-08 s 

************************************************************************************************************************************
     14  Keyword, label(s) :  DRIFT       2.162m_C3toC4                                                                IPASS= 1


                              Drift,  length =   -98.80000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  8.176428E-03 -4.476344E-03  1.437003E-01  6.218757E-05  7.4900166E+02  2.49842E-02
TRAJ #1 SX, SY, SZ, |S| :  1   -3.866854E-02   9.747036E-01   2.201309E-01   1.000000E+00

 Cumulative length of optical axis =    7.49000000     m   ;  Time  (for reference rigidity & particle) =   2.498412E-08 s 

************************************************************************************************************************************
     15  Keyword, label(s) :  TOSCA       snk2LowB                                                                     IPASS= 1


     Status of MOD.MOD2 is 15.1 ; NDIM = 3 ; number of field data files used is   1.
     Field map set stored in field array HC(IMAP = 1)
     2-D map. MOD=15.  Single field map, with field coefficient value : 
                 7.485030E-05

     ZroBXY OPTION :  absent from map title. BX and BY values in Z=0 plane left as read.

     No  map  file  opened :  was  already  stored.
     Skip  reading  field  map  file :           b_model3a2a-x-4_4_y-4_4_z-180_180-integral.table
     Restored mesh coordinates for field map #IMAP=   1,  name : b_model3a2a-x-4_4_y-4_4_z-180_180-integral.table
     Field map scaled by factor  a(1) =   7.485030E-05

     Min/max fields found in map (series) read   :  -1.073060E+03  /   1.073124E+03
       @  X,  Y, Z :  -52.0      -4.00      -2.90                  /   52.0      -4.00       2.90    
     Given normalisation coeffs on field, x, y, z :   1.336759E+01   1.000000E+00   1.000000E+00   1.000000E+00
     this yields min/max normalised fields (kG) : -1.434422E+04                      1.434508E+04
       @  X (cm),  Y (cm), Z (cm) :  -52.0      -4.00      -2.90   /   52.0      -4.00       2.90    

     Length of element,  XL =  3.600000E+02 cm 
                                               from  XI =  -1.800000E+02 cm 
                                               to    XF =   1.800000E+02 cm 

     Nbr of nodes in X = 361;  nbr of nodes in Y =   81
     X-size of mesh =  1.000000E+00 cm ; Y-size =  1.000000E-01 cm
     nber of mesh nodes in Z =   81 ; Step in Z =  0.100000E+00 cm

                     OPTION  DE  CALCUL  :  GRILLE  3-D  A  3*3*3  POINTS , INTERPOLATION  A  L'ORDRE  2

                    Integration step :  0.3000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000          180.000     0.007     0.000     0.000     0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000     0.007     0.000     0.000     0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000     0.007     0.000     0.000     0.000            3

     QUASEX - KPOS =  2 :  Element  is  mis-aligned  wrt.  the  optical  axis.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    11.0900000     m ;  Time  (for ref. rigidity & particle) =   3.699251E-08 s 

************************************************************************************************************************************
     16  Keyword, label(s) :  DRIFT                                                                                    IPASS= 1


                              Drift,  length =    16.40000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  7.373651E-03  5.909795E-05  9.445299E-05  8.740418E-05  1.1254017E+03  3.75396E-02
TRAJ #1 SX, SY, SZ, |S| :  1    3.205666E-02   9.990751E-01  -2.865927E-02   1.000000E+00

 Cumulative length of optical axis =    11.2540000     m   ;  Time  (for reference rigidity & particle) =   3.753956E-08 s 

************************************************************************************************************************************
     17  Keyword, label(s) :  DRIFT                                                                                    IPASS= 1


                              Drift,  length =   -78.20000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  7.369030E-03  5.909795E-05  8.761798E-05  8.740418E-05  1.0472017E+03  3.49311E-02
TRAJ #1 SX, SY, SZ, |S| :  1    3.205666E-02   9.990751E-01  -2.865927E-02   1.000000E+00

 Cumulative length of optical axis =    10.4720000     m   ;  Time  (for reference rigidity & particle) =   3.493107E-08 s 

************************************************************************************************************************************
     18  Keyword, label(s) :  MARKER      R-R+R-R+_E                                                                   IPASS= 1


************************************************************************************************************************************
     19  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #     18)
                                                  3 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o   1   1.0000     0.000     0.000     0.000     0.000      0.0000   0.0000    0.007    0.000    0.000    0.000   1.047202E+03     1
               Time of flight (mus) :  3.49311264E-02 mass (MeV/c2) :   938.272    
o   1   1.0000     0.000     0.000     0.000     0.000      0.0000   0.0000    0.007    0.000    0.000    0.000   1.047202E+03     2
               Time of flight (mus) :  3.49311264E-02 mass (MeV/c2) :   938.272    
o   1   1.0000     0.000     0.000     0.000     0.000      0.0000   0.0000    0.007    0.000    0.000    0.000   1.047202E+03     3
               Time of flight (mus) :  3.49311264E-02 mass (MeV/c2) :   938.272    

************************************************************************************************************************************
     20  Keyword, label(s) :  SPNPRT      MATRIX                                                                       IPASS= 1

                          -- 1  GROUPS  OF  MOMENTA  FOLLOW   --

                    ----------------------------------------------------------------------------------
                    Momentum  group  #1 (D=   1.000000E+00;  particles  1  to 3 ; 


                Spin  components  of  the      3  particles,  spin  angles :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   |Si,Sf|   (Z,Sf_yz) (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_yz : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     0.032057  0.999075 -0.028659  1.000000    271.9893   88.163   91.642   91.642    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.999080 -0.031213  0.029400  1.000000    271.9893  -91.789   55.564   88.315    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.028478 -0.029575 -0.999157  1.000000    271.9893 -177.647  134.987  177.647    3


                  Spin transfer matrix, momentum group # 1 :

          3.205666E-02    0.999080        2.847785E-02
          0.999075       -3.121347E-02   -2.957536E-02
         -2.865927E-02    2.939959E-02   -0.999157    

     Determinant =     1.0000000000
     Trace =    -0.9983136154;  spin precession acos((trace-1)/2) =   177.6469482794 deg
     Precession axis :  ( 0.7182,  0.6958, -0.0001)  ->  angle to (X,Y) plane, to X axis :    -0.0036,    44.0932 deg
     Spin precession/2pi (or Qs, fractional) :    4.9346E-01

************************************************************************************************************************************
     21  Keyword, label(s) :  END                                                                                      IPASS= 1

                             3 particles have been launched
                     Made  it  to  the  end :      3

************************************************************************************************************************************
 Pgm zgoubi : Execution ended normally, upon keyword END or FIN
   
 File in:   R-R+R-R+.inc
 File out:  zgoubi.res

  Zgoubi, author's Revision: 1670.
  Job  started  on  12-05-2022,  at  06:28:45 
  JOB  ENDED  ON    12-05-2022,  AT  06:28:46 

   CPU time, total :    0.76737400000000000     
