R-L-R-L- rotator:
! Arc to IP, V to H spin, along  Yell 5 o'clock CW, or Blue 5 o'clock.
'OBJET'                                                                                                      1
851.249816894531 * 1.d3
2
4 1
0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00  1.00000000E+00 'o'
0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00  1.00000000E+00 'o'
0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00  1.00000000E+00 'o'
0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00  1.00000000E+00 'o'
1 1 1 1
'PARTICUL'                                                                                                   2
PROTON
'SPNTRK'                                                                                                     3
4
1.00000000E+00  0.00000000E+00  0.00000000E+00
0.00000000E+00  1.00000000E+00  0.00000000E+00
0.00000000E+00  0.00000000E+00  1.00000000E+00
0.00000000E+00  0.00000000E+00  1.00000000E+00
 
'SCALING'                                                                                                    4
1  2
TOSCA  rotatorV2H_o*
-1
2.6361177603E+02
1.
TOSCA  rotatorV2H_i*
-1
2.1349643782E+02
1.
 
'OPTIONS'                                                                                                    5
1 1
.plt 2
 
'MARKER'  R-L-R-L-_S                                                                                         6
 
'DRIFT' rotator_adjL                                                                                         7
71.836840
 
'TOSCA'   rotatorV2H_out_RH                                                                                  8
0  0000  ! .plt
-1.00000000E+00  1.00000000E+00  1.00000000E+00  1.00000000E+00
HEADER_8   RHIC_helix
361 81 81  15.1  0.748502994012e-4     ! = 1/1.3360e4
b_rotatorModule_rightHanded.table
0 0 0 0
2
.3
2   0.00000000E+00  0.00000000E+00  0.00000000E+00
 
'DRIFT'                                                                                                      9
-98.800000
 
'TOSCA'   rotatorV2H_in_LH                                                                                  10
0  0000  ! .plt
-1.00000000E+00  1.00000000E+00  1.00000000E+00  1.00000000E+00
HEADER_8   RHIC_helix
361 81 81  15.1  0.748502994012e-4     ! = 1/1.3360e4
b_rotatorModule_leftHanded.table
0 0 0 0
2
.3
2   0.00000000E+00  0.00000000E+00  0.00000000E+00
 
'DRIFT'                                                                                                     11
-75.200000
'DRIFT'                                                                                                     12
35.889200
'DRIFT'                                                                                                     13
-35.889200
 
'TOSCA'  rotatorV2H_in_RH                                                                                   14
0  0000  ! .plt
-1.00000000E+00  1.00000000E+00  1.00000000E+00  1.00000000E+00
HEADER_8   RHIC_helix
361 81 81  15.1  0.748502994012e-4     ! = 1/1.3360e4
b_rotatorModule_rightHanded.table
0 0 0 0
2
.3
2   0.00000000E+00  0.00000000E+00  0.00000000E+00
 
'DRIFT'                                                                                                     15
-98.800000
 
'TOSCA'   rotatorV2H_out_LH                                                                                 16
0  0000  ! .plt
-1.00000000E+00  1.00000000E+00  1.00000000E+00  1.00000000E+00
HEADER_8   RHIC_helix
361 81 81  15.1  0.748502994012e-4     ! = 1/1.3360e4
b_rotatorModule_leftHanded.table
0 0 0 0
2
.3
2   0.00000000E+00  0.00000000E+00  0.00000000E+00
 
'DRIFT' rotator_adjL                                                                                        17
71.836840
 
'MARKER'  R-L-R-L-_E                                                                                        18
 
! 'FIT2'                                                                                                      19
! 2
! 4 4 0 3.
! 4 8 0 3.
! 2  1.e-12
! 10 4 3 #End 0. 1. 0
! 10 4 4 #End 1. .1 0
 
'FAISCEAU'                                                                                                  19
'SPNPRT' MATRIX                                                                                             20
 
'END'

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =     851249.817 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       4 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 1

     Particle  properties :
     PROTON
                     Mass          =    938.272        MeV/c2
                     Charge        =   1.602176E-19    C     
                     G  factor     =    1.79285              
                     COM life-time =   1.000000E+99    s     

              Reference  data :
                    mag. rigidity (kG.cm)   :   851249.82      =p/q, such that dev.=B*L/rigidity
                    mass (MeV/c2)           :   938.27208    
                    momentum (MeV/c)        :   255198.27    
                    energy, total (MeV)     :   255200.00    
                    energy, kinetic (MeV)   :   254261.73    
                    beta = v/c              :  0.9999932412    
                    gamma                   :   271.9893354    
                    beta*gamma              :   271.9874971    
                    G*gamma                 :   487.6353592    
                    m / G                   :   523.3418681    
                    electric rigidity (MeV) :   255196.5502    =T[eV]*(gamma+1)/gamma, such that dev.=E*L/rigidity
  
 I, AMQ(1,I), AMQ(2,I)/QE, P/Pref, v/c, time, s :
  
     1   9.38272081E+02  1.00000000E+00  1.00000000E+00  9.99993241E-01  0.00000000E+00  0.00000000E+00
     2   9.38272081E+02  1.00000000E+00  1.00000000E+00  9.99993241E-01  0.00000000E+00  0.00000000E+00
     3   9.38272081E+02  1.00000000E+00  1.00000000E+00  9.99993241E-01  0.00000000E+00  0.00000000E+00
     4   9.38272081E+02  1.00000000E+00  1.00000000E+00  9.99993241E-01  0.00000000E+00  0.00000000E+00

************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 1


                Spin  tracking  requested.


                          Particle  mass          =    938.2721     MeV/c2
                          Gyromagnetic  factor  G =    1.792847    

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 4 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     851249.82     kG*cm
                               beta      =    0.99999324    
                               gamma     =     271.98934    
                               G*gamma   =     487.63536    
                               M / G     =     523.3418681    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        4  PARTICULES :
                               <SX> =     0.408248
                               <SY> =     0.408248
                               <SZ> =     0.816497
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  SCALING                                                                                  IPASS= 1


               PRINT option is OFF.

               Scaling  request  on  2  families  of  optical  elements :
                                                      

     Family number   1
          Element [/label(s) ( 1)] to be scaled :          TOSCA
                                                           /rotatorV2H_o*
               Scaling of fields follows increase of rigidity taken from CAVITE, starting scaling value    2.63611776E+02

     Family number   2
          Element [/label(s) ( 1)] to be scaled :          TOSCA
                                                           /rotatorV2H_i*
               Scaling of fields follows increase of rigidity taken from CAVITE, starting scaling value    2.13496438E+02

************************************************************************************************************************************
      5  Keyword, label(s) :  OPTIONS                                                                                  IPASS= 1

         A list of 1 option(s) is expected.  List and actions taken are as follows :


    Option #  1 : .plt 2

     - IL is forced to  2 in all optical elements. Output file is: (0) none; (1) zgoubi.res; (2) zgoubi.plt; (7) zgoubi.impdev.out.

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      R-L-R-L-_S                                                                   IPASS= 1


************************************************************************************************************************************
      7  Keyword, label(s) :  DRIFT       rotator_adjL                                                                 IPASS= 1


                              Drift,  length =    71.83684  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  7.1836840E+01  2.39624E-03
TRAJ #1 SX, SY, SZ, |S| :  1    1.000000E+00   0.000000E+00   0.000000E+00   1.000000E+00

 Cumulative length of optical axis =   0.718368400     m   ;  Time  (for reference rigidity & particle) =   2.396235E-09 s 

************************************************************************************************************************************
      8  Keyword, label(s) :  TOSCA       rotatorV2H_out_RH                                                            IPASS= 1


                OPEN FILE zgoubi.plt                                                                      
                FOR PRINTING TRAJECTORIES


     Status of MOD.MOD2 is 15.1 ; NDIM = 3 ; number of field data files used is   1.
     Field map set stored in field array HC(IMAP = 1)
     2-D map. MOD=15.  Single field map, with field coefficient value : 
                 7.485030E-05

     ZroBXY OPTION :  absent from map title. BX and BY values in Z=0 plane left as read.

           New field map(s) opened, cartesian mesh (MOD.le.19) ; 
           name(s) of map data file(s) : 

          'b_rotatorModule_rightHanded.table'

   ----
   Map file number    1 ( of 1 ) (unit # 25 ) :
     b_rotatorModule_rightHanded.table field map,
     FORMAT type : regular,
     field multiplication factor :  7.48502994E-05.

 HEADER  (8 lines) : 
                81          81         361           1                                                                       
       1 X [LENGU]                                                                                                           
       2 Y [LENGU]                                                                                                           
       3 Z [LENGU]                                                                                                           
       4 BX [FLUXU]                                                                                                          
       5 BY [FLUXU]                                                                                                          
       6 BZ [FLUXU]                                                                                                          
       0                                                                                                                     
R_min (cm), DR (cm), DTTA (deg), DZ (cm) :    0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00

  Sbr fmapw/fmapr3 : completed job of reading map.
     Field map scaled by factor  a(1) =   7.485030E-05

     Min/max fields found in map (series) read   :   1.098551E+05  /  -1.098486E+05
       @  X,  Y, Z :   52.0      -4.00       2.90                  /  -52.0      -4.00      -2.90    
     Given normalisation coeffs on field, x, y, z :  -1.305818E-01   1.000000E+00   1.000000E+00   1.000000E+00
     this yields min/max normalised fields (kG) : -1.434508E+04                      1.434422E+04
       @  X (cm),  Y (cm), Z (cm) :   52.0      -4.00       2.90   /  -52.0      -4.00      -2.90    

     Length of element,  XL =  3.600000E+02 cm 
                                               from  XI =  -1.800000E+02 cm 
                                               to    XF =   1.800000E+02 cm 

     Nbr of nodes in X = 361;  nbr of nodes in Y =   81
     X-size of mesh =  1.000000E+00 cm ; Y-size =  1.000000E-01 cm
     nber of mesh nodes in Z =   81 ; Step in Z =  0.100000E+00 cm

                     OPTION  DE  CALCUL  :  GRILLE  3-D  A  3*3*3  POINTS , INTERPOLATION  A  L'ORDRE  2

                    Field has been * by scaling factor    263.61178       (Brho_ref =    851249.82    )

                    Integration step :  0.3000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000          180.000     0.370    -0.000    -0.002    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000     0.370    -0.000    -0.002    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000     0.370    -0.000    -0.002    -0.000            3
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000     0.370    -0.000    -0.002    -0.000            4


                CONDITIONS  DE  MAXWELL  (     4804.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                    -1.8577E-16       2.4852E-17        1.8513E-16
                                     -8.2678E-16       -1.2192E-14
                                      3.1538E-16       -1.2190E-14
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  2 :  Element  is  mis-aligned  wrt.  the  optical  axis.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    4.31836840     m ;  Time  (for ref. rigidity & particle) =   1.440462E-08 s 

************************************************************************************************************************************
      9  Keyword, label(s) :  DRIFT                                                                                    IPASS= 1


     zgoubi.plt                                                                      
      already open...

                              Drift,  length =   -98.80000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  3.698616E-01 -6.225002E-05 -9.285923E-04 -1.168620E-02  3.3303741E+02  1.11090E-02
TRAJ #1 SX, SY, SZ, |S| :  1    6.389633E-01  -4.794031E-01   6.015800E-01   1.000000E+00

 Cumulative length of optical axis =    3.33036840     m   ;  Time  (for reference rigidity & particle) =   1.110899E-08 s 

************************************************************************************************************************************
     10  Keyword, label(s) :  TOSCA       rotatorV2H_in_LH                                                             IPASS= 1


     zgoubi.plt                                                                      
      already open...

     Status of MOD.MOD2 is 15.1 ; NDIM = 3 ; number of field data files used is   1.
     Field map set stored in field array HC(IMAP = 2)
     2-D map. MOD=15.  Single field map, with field coefficient value : 
                 7.485030E-05

     ZroBXY OPTION :  absent from map title. BX and BY values in Z=0 plane left as read.

           New field map(s) opened, cartesian mesh (MOD.le.19) ; 
           name(s) of map data file(s) : 

          'b_rotatorModule_leftHanded.table'

   ----
   Map file number    1 ( of 1 ) (unit # 26 ) :
     b_rotatorModule_leftHanded.table field map,
     FORMAT type : regular,
     field multiplication factor :  7.48502994E-05.

 HEADER  (8 lines) : 
                81          81         361           1                                                                       
       0                                                                                                                     
       6 BZ [FLUXU]                                                                                                          
       5 BY [FLUXU]                                                                                                          
       4 BX [FLUXU]                                                                                                          
       3 Z [LENGU]                                                                                                           
       2 Y [LENGU]                                                                                                           
       1 X [LENGU]                                                                                                           
R_min (cm), DR (cm), DTTA (deg), DZ (cm) :    0.00000000E+00   0.00000000E+00   0.00000000E+00   1.00000000E-01

  Sbr fmapw/fmapr3 : completed job of reading map.
     Field map scaled by factor  a(1) =   7.485030E-05

     Min/max fields found in map (series) read   :   1.080799E+05  /  -1.080734E+05
       @  X,  Y, Z :  -52.0       4.00      -2.90                  /   52.0       4.00       2.90    
     Given normalisation coeffs on field, x, y, z :  -1.327266E-01   1.000000E+00   1.000000E+00   1.000000E+00
     this yields min/max normalised fields (kG) : -1.434508E+04                      1.434422E+04
       @  X (cm),  Y (cm), Z (cm) :  -52.0       4.00      -2.90   /   52.0       4.00       2.90    

     Length of element,  XL =  3.600000E+02 cm 
                                               from  XI =  -1.800000E+02 cm 
                                               to    XF =   1.800000E+02 cm 

     Nbr of nodes in X = 361;  nbr of nodes in Y =   81
     X-size of mesh =  1.000000E+00 cm ; Y-size =  1.000000E-01 cm
     nber of mesh nodes in Z =   81 ; Step in Z =  0.100000E+00 cm

                     OPTION  DE  CALCUL  :  GRILLE  3-D  A  3*3*3  POINTS , INTERPOLATION  A  L'ORDRE  2

                    Field has been * by scaling factor    213.49644       (Brho_ref =    851249.82    )

                    Integration step :  0.3000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000          180.000     0.065    -0.000    -0.007    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000     0.065    -0.000    -0.007    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000     0.065    -0.000    -0.007    -0.000            3
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000     0.065    -0.000    -0.007    -0.000            4


                CONDITIONS  DE  MAXWELL  (     4804.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                    -5.9734E-17       2.5933E-19        5.2068E-17
                                      8.0392E-16        1.4012E-14
                                     -4.6233E-16        1.3999E-14
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  2 :  Element  is  mis-aligned  wrt.  the  optical  axis.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    6.93036840     m ;  Time  (for ref. rigidity & particle) =   2.311738E-08 s 

************************************************************************************************************************************
     11  Keyword, label(s) :  DRIFT                                                                                    IPASS= 1


                              Drift,  length =   -75.20000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  6.536607E-02 -1.134150E-04 -5.282224E-03 -2.130878E-02  6.1783779E+02  2.06090E-02
TRAJ #1 SX, SY, SZ, |S| :  1    1.440121E-01   3.217446E-01   9.358103E-01   1.000000E+00

 Cumulative length of optical axis =    6.17836840     m   ;  Time  (for reference rigidity & particle) =   2.060896E-08 s 

************************************************************************************************************************************
     12  Keyword, label(s) :  DRIFT                                                                                    IPASS= 1


                              Drift,  length =    35.88920  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  6.536200E-02 -1.134150E-04 -6.046979E-03 -2.130878E-02  6.5372699E+02  2.18061E-02
TRAJ #1 SX, SY, SZ, |S| :  1    1.440121E-01   3.217446E-01   9.358103E-01   1.000000E+00

 Cumulative length of optical axis =    6.53726040     m   ;  Time  (for reference rigidity & particle) =   2.180610E-08 s 

************************************************************************************************************************************
     13  Keyword, label(s) :  DRIFT                                                                                    IPASS= 1


                              Drift,  length =   -35.88920  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  6.536607E-02 -1.134150E-04 -5.282224E-03 -2.130878E-02  6.1783779E+02  2.06090E-02
TRAJ #1 SX, SY, SZ, |S| :  1    1.440121E-01   3.217446E-01   9.358103E-01   1.000000E+00

 Cumulative length of optical axis =    6.17836840     m   ;  Time  (for reference rigidity & particle) =   2.060896E-08 s 

************************************************************************************************************************************
     14  Keyword, label(s) :  TOSCA       rotatorV2H_in_RH                                                             IPASS= 1


     zgoubi.plt                                                                      
      already open...

     Status of MOD.MOD2 is 15.1 ; NDIM = 3 ; number of field data files used is   1.
     Field map set stored in field array HC(IMAP = 1)
     2-D map. MOD=15.  Single field map, with field coefficient value : 
                 7.485030E-05

     ZroBXY OPTION :  absent from map title. BX and BY values in Z=0 plane left as read.

     No  map  file  opened :  was  already  stored.
     Skip  reading  field  map  file :           b_rotatorModule_rightHanded.table
     Restored mesh coordinates for field map #IMAP=   1,  name : b_rotatorModule_rightHanded.table
     Field map scaled by factor  a(1) =   7.485030E-05

     Min/max fields found in map (series) read   :   1.080799E+05  /  -1.080734E+05
       @  X,  Y, Z :   52.0      -4.00       2.90                  /  -52.0      -4.00      -2.90    
     Given normalisation coeffs on field, x, y, z :  -1.327266E-01   1.000000E+00   1.000000E+00   1.000000E+00
     this yields min/max normalised fields (kG) : -1.434508E+04                      1.434422E+04
       @  X (cm),  Y (cm), Z (cm) :   52.0      -4.00       2.90   /  -52.0      -4.00      -2.90    

     Length of element,  XL =  3.600000E+02 cm 
                                               from  XI =  -1.800000E+02 cm 
                                               to    XF =   1.800000E+02 cm 

     Nbr of nodes in X = 361;  nbr of nodes in Y =   81
     X-size of mesh =  1.000000E+00 cm ; Y-size =  1.000000E-01 cm
     nber of mesh nodes in Z =   81 ; Step in Z =  0.100000E+00 cm

                     OPTION  DE  CALCUL  :  GRILLE  3-D  A  3*3*3  POINTS , INTERPOLATION  A  L'ORDRE  2

                    Field has been * by scaling factor    213.49644       (Brho_ref =    851249.82    )

                    Integration step :  0.3000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000          180.000     0.370    -0.000    -0.015    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000     0.370    -0.000    -0.015    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000     0.370    -0.000    -0.015    -0.000            3
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000     0.370    -0.000    -0.015    -0.000            4


                CONDITIONS  DE  MAXWELL  (     4804.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                    -1.5614E-16       2.0458E-17        1.5282E-16
                                     -6.8060E-16       -1.0036E-14
                                      2.5962E-16       -1.0035E-14
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  2 :  Element  is  mis-aligned  wrt.  the  optical  axis.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    9.77836840     m ;  Time  (for ref. rigidity & particle) =   3.261735E-08 s 

************************************************************************************************************************************
     15  Keyword, label(s) :  DRIFT                                                                                    IPASS= 1


                              Drift,  length =   -98.80000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  3.698034E-01 -1.623805E-04 -1.161235E-02 -3.092841E-02  8.7903818E+02  2.93218E-02
TRAJ #1 SX, SY, SZ, |S| :  1   -3.943563E-01  -5.496818E-01   7.364327E-01   1.000000E+00

 Cumulative length of optical axis =    8.79036840     m   ;  Time  (for reference rigidity & particle) =   2.932171E-08 s 

************************************************************************************************************************************
     16  Keyword, label(s) :  TOSCA       rotatorV2H_out_LH                                                            IPASS= 1


     zgoubi.plt                                                                      
      already open...

     Status of MOD.MOD2 is 15.1 ; NDIM = 3 ; number of field data files used is   1.
     Field map set stored in field array HC(IMAP = 2)
     2-D map. MOD=15.  Single field map, with field coefficient value : 
                 7.485030E-05

     ZroBXY OPTION :  absent from map title. BX and BY values in Z=0 plane left as read.

     No  map  file  opened :  was  already  stored.
     Skip  reading  field  map  file :           b_rotatorModule_leftHanded.table
     Restored mesh coordinates for field map #IMAP=   2,  name : b_rotatorModule_leftHanded.table
     Field map scaled by factor  a(1) =   7.485030E-05

     Min/max fields found in map (series) read   :   1.098551E+05  /  -1.098486E+05
       @  X,  Y, Z :  -52.0       4.00      -2.90                  /   52.0       4.00       2.90    
     Given normalisation coeffs on field, x, y, z :  -1.305818E-01   1.000000E+00   1.000000E+00   1.000000E+00
     this yields min/max normalised fields (kG) : -1.434508E+04                      1.434422E+04
       @  X (cm),  Y (cm), Z (cm) :  -52.0       4.00      -2.90   /   52.0       4.00       2.90    

     Length of element,  XL =  3.600000E+02 cm 
                                               from  XI =  -1.800000E+02 cm 
                                               to    XF =   1.800000E+02 cm 

     Nbr of nodes in X = 361;  nbr of nodes in Y =   81
     X-size of mesh =  1.000000E+00 cm ; Y-size =  1.000000E-01 cm
     nber of mesh nodes in Z =   81 ; Step in Z =  0.100000E+00 cm

                     OPTION  DE  CALCUL  :  GRILLE  3-D  A  3*3*3  POINTS , INTERPOLATION  A  L'ORDRE  2

                    Field has been * by scaling factor    263.61178       (Brho_ref =    851249.82    )

                    Integration step :  0.3000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000          180.000    -0.000    -0.000    -0.025    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000    -0.000    -0.000    -0.025    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000    -0.000    -0.000    -0.025    -0.000            3
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000    -0.000    -0.000    -0.025    -0.000            4


                CONDITIONS  DE  MAXWELL  (     4804.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                    -1.1916E-16       5.7055E-18        9.1071E-17
                                      9.3861E-16        1.7021E-14
                                     -5.5515E-16        1.7005E-14
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  2 :  Element  is  mis-aligned  wrt.  the  optical  axis.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    12.3903684     m ;  Time  (for ref. rigidity & particle) =   4.133010E-08 s 

************************************************************************************************************************************
     17  Keyword, label(s) :  DRIFT       rotator_adjL                                                                 IPASS= 1


                              Drift,  length =    71.83684  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -1.504195E-04 -2.240456E-04 -2.793293E-02 -4.261739E-02  1.3108756E+03  4.37264E-02
TRAJ #1 SX, SY, SZ, |S| :  1   -9.585240E-01   1.993674E-01   2.036774E-01   1.000000E+00

 Cumulative length of optical axis =    13.1087368     m   ;  Time  (for reference rigidity & particle) =   4.372633E-08 s 

************************************************************************************************************************************
     18  Keyword, label(s) :  MARKER      R-L-R-L-_E                                                                   IPASS= 1


************************************************************************************************************************************
     19  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #     18)
                                                  4 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o   1   1.0000     0.000     0.000     0.000     0.000      0.0000   0.0000   -0.000   -0.000   -0.028   -0.043   1.310876E+03     1
               Time of flight (mus) :  4.37263985E-02 mass (MeV/c2) :   938.272    
o   1   1.0000     0.000     0.000     0.000     0.000      0.0000   0.0000   -0.000   -0.000   -0.028   -0.043   1.310876E+03     2
               Time of flight (mus) :  4.37263985E-02 mass (MeV/c2) :   938.272    
o   1   1.0000     0.000     0.000     0.000     0.000      0.0000   0.0000   -0.000   -0.000   -0.028   -0.043   1.310876E+03     3
               Time of flight (mus) :  4.37263985E-02 mass (MeV/c2) :   938.272    
o   1   1.0000     0.000     0.000     0.000     0.000      0.0000   0.0000   -0.000   -0.000   -0.028   -0.043   1.310876E+03     4
               Time of flight (mus) :  4.37263985E-02 mass (MeV/c2) :   938.272    

************************************************************************************************************************************
     20  Keyword, label(s) :  SPNPRT      MATRIX                                                                       IPASS= 1

                          -- 1  GROUPS  OF  MOMENTA  FOLLOW   --

                    ----------------------------------------------------------------------------------
                    Momentum  group  #1 (D=   1.000000E+00;  particles  1  to 3 ; 


                Spin  components  of  the      3  particles,  spin  angles :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   |Si,Sf|   (Z,Sf_yz) (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_yz : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000    -0.958524  0.199367  0.203677  1.000000    271.9893 -163.440   54.449   78.248    1
 o  1  0.000000  1.000000  0.000000  1.000000    -0.199410  0.041476 -0.979038  1.000000    271.9893   87.623  134.974  168.248    2
 o  1  0.000000  0.000000  1.000000  1.000000    -0.203636 -0.979047 -0.000000  1.000000    271.9893  -90.000   90.000   90.000    3


                  Spin transfer matrix, momentum group # 1 :

         -0.958524       -0.199410       -0.203636    
          0.199367        4.147615E-02   -0.979047    
          0.203677       -0.979038       -1.082302E-07

     Determinant =     1.0000000000
     Trace =    -0.9170479211;  spin precession acos((trace-1)/2) =   163.4404365323 deg
     Precession axis :  ( 0.0000, -0.7146,  0.6996)  ->  angle to (X,Y) plane, to X axis :    44.3933,    89.9991 deg
     Spin precession/2pi (or Qs, fractional) :    4.5400E-01

                    ----------------------------------------------------------------------------------
                    Remaining  1  particles  ; 


                Spin  components  of  the      1  particles,  spin  angles :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   |Si,Sf|   (Z,Sf_yz) (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_yz : projection of Sf on YZ plane)

 o  1  0.000000  0.000000  1.000000  1.000000    -0.203636 -0.979047 -0.000000  1.000000    271.9893  -90.000   90.000   90.000    4

************************************************************************************************************************************
     21  Keyword, label(s) :  END                                                                                      IPASS= 1

                             4 particles have been launched
                     Made  it  to  the  end :      4

************************************************************************************************************************************
 Pgm zgoubi : Execution ended normally, upon keyword END or FIN
   
 File in:   R-L-R-L-.inc
 File out:  zgoubi.res

  Zgoubi, author's Revision: 1670.
  Job  started  on  12-05-2022,  at  10:08:08 
  JOB  ENDED  ON    12-05-2022,  AT  10:08:10 

   CPU time, total :     1.1881529999999998     
