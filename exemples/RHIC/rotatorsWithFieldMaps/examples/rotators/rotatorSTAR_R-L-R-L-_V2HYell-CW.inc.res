rotatorSTAR_R-L-R-L-_V2HYell-CW.inc
'OBJET'                                                                                                      1
853.71244451829796 * 1.e3  ! E_kin=255   Gg=489.046059786
2
4   3
0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00  1.00000000E+00 'o'
0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00  1.00000000E+00 'o'
0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00  1.00000000E+00 'o'
0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00  1.00000000E+00 'o'
1 1 1 1
 
'PARTICUL'                                                                                                   2
PROTON
'SPNTRK'                                                                                                     3
4
1.00000000E+00  0.00000000E+00  0.00000000E+00
0.00000000E+00  1.00000000E+00  0.00000000E+00
0.00000000E+00  0.00000000E+00  1.00000000E+00
0.00000000E+00  0.00000000E+00  1.00000000E+00
 
! PAC03 paper, Waldo's: nominal is Iout=221, Iin=184 at 100GeV.
 
'SCALING'                                                                                                    4
1  10
BEND
-1
8.5124981689E+02
1
MULTIPOL
-1
8.5124981689E+02
1
MULTIPOL *QF*
-1
8.5105235000E+02
1
MULTIPOL *QD*
-1
8.5144285000E+02
1
MULTIPOL *SXF*
-1
8.0000000000E+02
1
MULTIPOL *SXD*
-1
1.0000000000E+03
1
TOSCA  snk1*
-1
1.0000000000E+00
1
TOSCA  snk2*
-1
1.0000000000E+00
1
TOSCA  rotatorV2H_out*
-1
2.6307770330E+02
1.
TOSCA  rotatorV2H_in*
-1
2.1236465919E+02
1.
 
! 'INCLUDE'
! Include_SegmentStart : R-L-R-L-.inc[R-L-R-L-_S,*:R-L-R-L-_E,*]     (n_inc, depth :  1 2)
'MARKER'  R-L-R-L-_S                                                                                         5
 
'DRIFT' rotator_adjL                                                                                         6
71.836840
 
'TOSCA'   rotatorV2H_out_RH                                                                                  7
0  0000  ! .plt
-1.00000000E+00  1.00000000E+00  1.00000000E+00  1.00000000E+00
HEADER_8   RHIC_helix
361 81 81  15.1  0.748502994012e-4     ! = 1/1.3360e4
b_rotatorModule_rightHanded.table
0 0 0 0
2
.3
2   0.00000000E+00  0.00000000E+00  0.00000000E+00
 
'DRIFT'                                                                                                      8
-98.800000
 
'TOSCA'   rotatorV2H_in_LH                                                                                   9
0  0000  ! .plt
-1.00000000E+00  1.00000000E+00  1.00000000E+00  1.00000000E+00
HEADER_8   RHIC_helix
361 81 81  15.1  0.748502994012e-4     ! = 1/1.3360e4
b_rotatorModule_leftHanded.table
0 0 0 0
2
.3
2   0.00000000E+00  0.00000000E+00  0.00000000E+00
 
'DRIFT'                                                                                                     10
-75.200000
'DRIFT'                                                                                                     11
35.889200
'DRIFT'                                                                                                     12
-35.889200
 
'TOSCA'  rotatorV2H_in_RH                                                                                   13
0  0000  ! .plt
-1.00000000E+00  1.00000000E+00  1.00000000E+00  1.00000000E+00
HEADER_8   RHIC_helix
361 81 81  15.1  0.748502994012e-4     ! = 1/1.3360e4
b_rotatorModule_rightHanded.table
0 0 0 0
2
.3
2   0.00000000E+00  0.00000000E+00  0.00000000E+00
 
'DRIFT'                                                                                                     14
-98.800000
 
'TOSCA'   rotatorV2H_out_LH                                                                                 15
0  0000  ! .plt
-1.00000000E+00  1.00000000E+00  1.00000000E+00  1.00000000E+00
HEADER_8   RHIC_helix
361 81 81  15.1  0.748502994012e-4     ! = 1/1.3360e4
b_rotatorModule_leftHanded.table
0 0 0 0
2
.3
2   0.00000000E+00  0.00000000E+00  0.00000000E+00
 
'DRIFT' rotator_adjL                                                                                        16
71.836840
 
! Include_SegmentEnd : R-L-R-L-.inc                                  (had n_inc, depth :  1 2)
'MARKER'  R-L-R-L-_E                                                                                        17
 
!'DRIFT'    DRIFT_1201DRIF
!    655.436840
!'DRIFT'    YO5_B3.1  VMON
!      0.0000
!'DRIFT'    DRIFT_1202DRIF
!    867.970400
 
'DRIFT'    DRIFT_1202DRIF                                                                                   18
212.533560
'MARKER'   YO5_DMPT3.INST                                                                                   19
'DRIFT'    DRIFT_1203DRIF                                                                                   20
224.713800
'MARKER'   YO5_DMPT3.INST                                                                                   21
'DRIFT'    DRIFT_1204DRIF                                                                                   22
1671.560800
'MARKER'   YO5_KFBH3 TKIC                                                                                   23
'DRIFT'    DRIFT_1205DRIF                                                                                   24
210.231202
'DRIFT'    YO5_B3    MONI                                                                                   25
0.000000
'DRIFT'    DRIFT_1206DRIF                                                                                   26
47.142826
'MULTIPOL' YO5_TV3   VKIC                                                                                   27
0  .kicker
0.000100  10.00   7.56987000E+02   0.00000000E+00   0.00000000E+00  0.0  0.0  0.0  0.0  0.0  0.0  0.0
0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
4   1.45500E-01   2.26700E+00  -6.39500E-01   1.15580E+00   0.00000E+00   0.00000E+00
0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
4   1.45500E-01   2.26700E+00  -6.39500E-01   1.15580E+00   0.00000E+00   0.00000E+00
1.57080E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#20|4|20  Kick
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'DRIFT'    DRIFT_1207DRIF                                                                                   28
59.796800
'MULTIPOL' YO5_QD3   QUAD                                                                                   29
0  .Quad
210.048400  10.00   0.00000000E+00  -5.62616245E-02   0.00000000E+00  0.0  0.0  0.0  0.0  0.0  0.0  0.0
0.0000   0.0000   6.00   3.00   1.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00
0.0000   0.0000   6.00   3.00   1.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00
0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#30|420|30   Quad  YO5_QD3
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'DRIFT'    DRIFT_1208DRIF                                                                                   30
48.977800
'MARKER'   YO5_QS3   MULT                                                                                   31
'DRIFT'    DRIFT_1209DRIF                                                                                   32
137.096150
'MULTIPOL' YO5_QF2   QUAD                                                                                   33
0  .Quad
339.163300  10.00   0.00000000E+00   5.59802553E-02   0.00000000E+00  0.0  0.0  0.0  0.0  0.0  0.0  0.0
0.0000   0.0000   6.00   3.00   1.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00
0.0000   0.0000   6.00   3.00   1.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00
0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#30|678|30   Quad  YO5_QF2
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'DRIFT'    DRIFT_1210DRIF                                                                                   34
49.420350
'MULTIPOL' YO5_TH2   HKIC                                                                                   35
0  .kicker
0.000100  10.00  -0.00000000E+00   0.00000000E+00   0.00000000E+00  0.0  0.0  0.0  0.0  0.0  0.0  0.0
0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
4   1.45500E-01   2.26700E+00  -6.39500E-01   1.15580E+00   0.00000E+00   0.00000E+00
0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
4   1.45500E-01   2.26700E+00  -6.39500E-01   1.15580E+00   0.00000E+00   0.00000E+00
0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#20|4|20  Kick
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'DRIFT'    DRIFT_1211DRIF                                                                                   36
118.242800
'MULTIPOL' YO5_QD1   QUAD                                                                                   37
0  .Quad
144.000000  10.00   0.00000000E+00  -5.67389273E-02   0.00000000E+00  0.0  0.0  0.0  0.0  0.0  0.0  0.0
0.0000   0.0000   6.00   3.00   1.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00
0.0000   0.0000   6.00   3.00   1.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00
0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#30|288|30   Quad  YO5_QD1
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'DRIFT'    DRIFT_1212DRIF                                                                                   38
33.694480
'DRIFT'    YO5_B1    MONI                                                                                   39
0.000000
'DRIFT'    DRIFT_1213DRIF                                                                                   40
92.770376
'MULTIPOL' YO5_DH0_E1MULT                                                                                   41
0  .Mult
0.001000  10.00   0.00000000E+00  -0.00000000E+00   5.78170000E+01  0.0  0.0  0.0  0.0  0.0  0.0  0.0
0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00
0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00
0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#30|9|30   MultYO5_DH0_E1
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'BEND'     YO5_DH0   SBEN                                                                                   42
0  .Bend
3.588931727E+02   0.000000000E+00   4.232419840E-02
0.00  0.00   0.00000000
4 .2401  1.8639  -.5572  .3904 0. 0. 0.
0.00  0.00   0.00000000
4 .2401  1.8639  -.5572  .3904 0. 0. 0.
1.  !  Bend YO5_DH0
3 0. 0.    -7.595006E-03
'MULTIPOL' YO5_DH0_E2MULT                                                                                   43
0  .Mult
0.001000  10.00   0.00000000E+00  -0.00000000E+00   1.06030000E+01  0.0  0.0  0.0  0.0  0.0  0.0  0.0
0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00
0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00
0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#30|9|30   MultYO5_DH0_E2
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'DRIFT'    DRIFT_1214DRIF                                                                                   44
292.311317
'MARKER'   YO5_RP0.2 INST                                                                                   45
'DRIFT'    DRIFT_1215DRIF                                                                                   46
180.200000
'MARKER'   YO5_RP0.1 INST                                                                                   47
'DRIFT'    DRIFT_1216DRIF                                                                                   48
228.178063
'MULTIPOL' ERDXMP05  MULT                                                                                   49
0  .Mult
0.001000  10.00   0.00000000E+00  -9.61490000E+00   0.00000000E+00  0.0  0.0  0.0  0.0  0.0  0.0  0.0
0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00
0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00
0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#30|9|30   MultERDXMP05
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'MULTIPOL' G5_DHX_E1 MULT                                                                                   50
0  .Mult
0.001000  10.00   0.00000000E+00  -3.36750000E+00   2.41270000E+00  0.0  0.0  0.0  0.0  0.0  0.0  0.0
0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00
0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00
0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#30|9|30   MultG5_DHX_E1
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'BEND'     G5_DHX    SBEN                                                                                   51
0  .Bend
3.700164531E+02   0.000000000E+00  -5.097208850E-02
0.00  0.00   0.00000000
4 .2401  1.8639  -.5572  .3904 0. 0. 0.
0.00  0.00   0.00000000
4 .2401  1.8639  -.5572  .3904 0. 0. 0.
1.  !  Bend G5_DHX
3 0. 0.     9.430395E-03
'MARKER'   ELDXMP05  MULT                                                                                   52
'MULTIPOL' G5_DHX_E2 MULT                                                                                   53
0  .Mult
0.001000  10.00   0.00000000E+00  -8.71000000E-16  -6.38850000E+00  0.0  0.0  0.0  0.0  0.0  0.0  0.0
0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00
0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00
0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#30|9|30   MultG5_DHX_E2
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'DRIFT'    DRIFT_1217DRIF                                                                                   54
147.366200
'DRIFT'    G5_BX     MONI                                                                                   55
0.000000
'DRIFT'    DRIFT_1218DRIF                                                                                   56
832.633800
'MARKER'   RHIC$END  MARK                                                                                   57
 
'SPNPRT'  MATRIX                                                                                            58
 
! 'FIT2'                                                                                                      59
! 2
! 4 36 0 2.                                                          ! vary scaling factors rotatorH2V_out*
! 4 40 0 2.                                                           ! vary scaling factors rotatorH2V_in*
! 2  1e-10
! 10 4 1 #End 1. .1 0                                                    ! requset SZ=0 at end of sequence.
! 10 4 4 #End 1. .1 0                                                   ! requset |S|=1 at end of sequence.
 
!10.2 1 0 #End 3.1415926 1. 0   ! spin rotation= pi
!10.3 1 1 #End 0. 1. 0          ! X component of rotation axis is 0
 
'FAISCEAU'                                                                                                  59
'SPNPRT'  MATRIX                                                                                            60
 
'END'                                                                                                       17

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =     853712.445 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       4 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 1

     Particle  properties :
     PROTON
                     Mass          =    938.272        MeV/c2
                     Charge        =   1.602176E-19    C     
                     G  factor     =    1.79285              
                     COM life-time =   1.000000E+99    s     

              Reference  data :
                    mag. rigidity (kG.cm)   :   853712.44      =p/q, such that dev.=B*L/rigidity
                    mass (MeV/c2)           :   938.27208    
                    momentum (MeV/c)        :   255936.55    
                    energy, total (MeV)     :   255938.27    
                    energy, kinetic (MeV)   :   255000.00    
                    beta = v/c              :  0.9999932802    
                    gamma                   :   272.7761777    
                    beta*gamma              :   272.7743447    
                    G*gamma                 :   489.0460474    
                    m / G                   :   523.3418681    
                    electric rigidity (MeV) :   255934.8323    =T[eV]*(gamma+1)/gamma, such that dev.=E*L/rigidity
  
 I, AMQ(1,I), AMQ(2,I)/QE, P/Pref, v/c, time, s :
  
     1   9.38272081E+02  1.00000000E+00  1.00000000E+00  9.99993280E-01  0.00000000E+00  0.00000000E+00
     2   9.38272081E+02  1.00000000E+00  1.00000000E+00  9.99993280E-01  0.00000000E+00  0.00000000E+00
     3   9.38272081E+02  1.00000000E+00  1.00000000E+00  9.99993280E-01  0.00000000E+00  0.00000000E+00
     4   9.38272081E+02  1.00000000E+00  1.00000000E+00  9.99993280E-01  0.00000000E+00  0.00000000E+00

************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 1


                Spin  tracking  requested.


                          Particle  mass          =    938.2721     MeV/c2
                          Gyromagnetic  factor  G =    1.792847    

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 4 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     853712.44     kG*cm
                               beta      =    0.99999328    
                               gamma     =     272.77618    
                               G*gamma   =     489.04605    
                               M / G     =     523.3418681    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        4  PARTICULES :
                               <SX> =     0.408248
                               <SY> =     0.408248
                               <SZ> =     0.816497
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  SCALING                                                                                  IPASS= 1


               PRINT option is OFF.

               Scaling  request  on 10  families  of  optical  elements :
                                                      

     Family number   1
          Element [/label(s) ( 0)] to be scaled :          BEND
               Family not labeled ;  this scaling will apply to all (unlabeled) elements  "BEND      "
               Scaling of fields follows increase of rigidity taken from CAVITE, starting scaling value    8.51249817E+02

     Family number   2
          Element [/label(s) ( 0)] to be scaled :          MULTIPOL
               Family not labeled ;  this scaling will apply to all (unlabeled) elements  "MULTIPOL  "
               Scaling of fields follows increase of rigidity taken from CAVITE, starting scaling value    8.51249817E+02

     Family number   3
          Element [/label(s) ( 1)] to be scaled :          MULTIPOL
                                                           /*QF*
               Scaling of fields follows increase of rigidity taken from CAVITE, starting scaling value    8.51052350E+02

     Family number   4
          Element [/label(s) ( 1)] to be scaled :          MULTIPOL
                                                           /*QD*
               Scaling of fields follows increase of rigidity taken from CAVITE, starting scaling value    8.51442850E+02

     Family number   5
          Element [/label(s) ( 1)] to be scaled :          MULTIPOL
                                                           /*SXF*
               Scaling of fields follows increase of rigidity taken from CAVITE, starting scaling value    8.00000000E+02

     Family number   6
          Element [/label(s) ( 1)] to be scaled :          MULTIPOL
                                                           /*SXD*
               Scaling of fields follows increase of rigidity taken from CAVITE, starting scaling value    1.00000000E+03

     Family number   7
          Element [/label(s) ( 1)] to be scaled :          TOSCA
                                                           /snk1*
               Scaling of fields follows increase of rigidity taken from CAVITE, starting scaling value    1.00000000E+00

     Family number   8
          Element [/label(s) ( 1)] to be scaled :          TOSCA
                                                           /snk2*
               Scaling of fields follows increase of rigidity taken from CAVITE, starting scaling value    1.00000000E+00

     Family number   9
          Element [/label(s) ( 1)] to be scaled :          TOSCA
                                                           /rotatorV2H_out*
               Scaling of fields follows increase of rigidity taken from CAVITE, starting scaling value    2.63077703E+02

     Family number  10
          Element [/label(s) ( 1)] to be scaled :          TOSCA
                                                           /rotatorV2H_in*
               Scaling of fields follows increase of rigidity taken from CAVITE, starting scaling value    2.12364659E+02

************************************************************************************************************************************
      5  Keyword, label(s) :  MARKER      R-L-R-L-_S                                                                   IPASS= 1


************************************************************************************************************************************
      6  Keyword, label(s) :  DRIFT       rotator_adjL                                                                 IPASS= 1


                              Drift,  length =    71.83684  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  7.1836840E+01  2.39624E-03
TRAJ #1 SX, SY, SZ, |S| :  1    1.000000E+00   0.000000E+00   0.000000E+00   1.000000E+00

 Cumulative length of optical axis =   0.718368400     m   ;  Time  (for reference rigidity & particle) =   2.396235E-09 s 

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA       rotatorV2H_out_RH                                                            IPASS= 1


     Status of MOD.MOD2 is 15.1 ; NDIM = 3 ; number of field data files used is   1.
     Field map set stored in field array HC(IMAP = 1)
     2-D map. MOD=15.  Single field map, with field coefficient value : 
                 7.485030E-05

     ZroBXY OPTION :  absent from map title. BX and BY values in Z=0 plane left as read.

           New field map(s) opened, cartesian mesh (MOD.le.19) ; 
           name(s) of map data file(s) : 

          'b_rotatorModule_rightHanded.table'

   ----
   Map file number    1 ( of 1 ) (unit # 24 ) :
     b_rotatorModule_rightHanded.table field map,
     FORMAT type : regular,
     field multiplication factor :  7.48502994E-05.

 HEADER  (8 lines) : 
                81          81         361           1                                                                       
       1 X [LENGU]                                                                                                           
       2 Y [LENGU]                                                                                                           
       3 Z [LENGU]                                                                                                           
       4 BX [FLUXU]                                                                                                          
       5 BY [FLUXU]                                                                                                          
       6 BZ [FLUXU]                                                                                                          
       0                                                                                                                     
R_min (cm), DR (cm), DTTA (deg), DZ (cm) :    0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00

  Sbr fmapw/fmapr3 : completed job of reading map.
     Field map scaled by factor  a(1) =   7.485030E-05

     Min/max fields found in map (series) read   :   1.098269E+05  /  -1.098203E+05
       @  X,  Y, Z :   52.0      -4.00       2.90                  /  -52.0      -4.00      -2.90    
     Given normalisation coeffs on field, x, y, z :  -1.306153E-01   1.000000E+00   1.000000E+00   1.000000E+00
     this yields min/max normalised fields (kG) : -1.434508E+04                      1.434422E+04
       @  X (cm),  Y (cm), Z (cm) :   52.0      -4.00       2.90   /  -52.0      -4.00      -2.90    

     Length of element,  XL =  3.600000E+02 cm 
                                               from  XI =  -1.800000E+02 cm 
                                               to    XF =   1.800000E+02 cm 

     Nbr of nodes in X = 361;  nbr of nodes in Y =   81
     X-size of mesh =  1.000000E+00 cm ; Y-size =  1.000000E-01 cm
     nber of mesh nodes in Z =   81 ; Step in Z =  0.100000E+00 cm

                     OPTION  DE  CALCUL  :  GRILLE  3-D  A  3*3*3  POINTS , INTERPOLATION  A  L'ORDRE  2

                    Field has been * by scaling factor    263.07770       (Brho_ref =    853712.44    )

                    Integration step :  0.3000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000          180.000     0.368    -0.000    -0.002    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000     0.368    -0.000    -0.002    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000     0.368    -0.000    -0.002    -0.000            3
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000     0.368    -0.000    -0.002    -0.000            4

     QUASEX - KPOS =  2 :  Element  is  mis-aligned  wrt.  the  optical  axis.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    4.31836840     m ;  Time  (for ref. rigidity & particle) =   1.440462E-08 s 

************************************************************************************************************************************
      8  Keyword, label(s) :  DRIFT                                                                                    IPASS= 1


                              Drift,  length =   -98.80000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  3.681422E-01 -6.205908E-05 -9.242433E-04 -1.163171E-02  3.3303740E+02  1.11090E-02
TRAJ #1 SX, SY, SZ, |S| :  1    6.415968E-01  -4.767653E-01   6.008730E-01   1.000000E+00

 Cumulative length of optical axis =    3.33036840     m   ;  Time  (for reference rigidity & particle) =   1.110899E-08 s 

************************************************************************************************************************************
      9  Keyword, label(s) :  TOSCA       rotatorV2H_in_LH                                                             IPASS= 1


     Status of MOD.MOD2 is 15.1 ; NDIM = 3 ; number of field data files used is   1.
     Field map set stored in field array HC(IMAP = 2)
     2-D map. MOD=15.  Single field map, with field coefficient value : 
                 7.485030E-05

     ZroBXY OPTION :  absent from map title. BX and BY values in Z=0 plane left as read.

           New field map(s) opened, cartesian mesh (MOD.le.19) ; 
           name(s) of map data file(s) : 

          'b_rotatorModule_leftHanded.table'

   ----
   Map file number    1 ( of 1 ) (unit # 25 ) :
     b_rotatorModule_leftHanded.table field map,
     FORMAT type : regular,
     field multiplication factor :  7.48502994E-05.

 HEADER  (8 lines) : 
                81          81         361           1                                                                       
       0                                                                                                                     
       6 BZ [FLUXU]                                                                                                          
       5 BY [FLUXU]                                                                                                          
       4 BX [FLUXU]                                                                                                          
       3 Z [LENGU]                                                                                                           
       2 Y [LENGU]                                                                                                           
       1 X [LENGU]                                                                                                           
R_min (cm), DR (cm), DTTA (deg), DZ (cm) :    0.00000000E+00   0.00000000E+00   0.00000000E+00   1.00000000E-01

  Sbr fmapw/fmapr3 : completed job of reading map.
     Field map scaled by factor  a(1) =   7.485030E-05

     Min/max fields found in map (series) read   :   1.080574E+05  /  -1.080509E+05
       @  X,  Y, Z :  -52.0       4.00      -2.90                  /   52.0       4.00       2.90    
     Given normalisation coeffs on field, x, y, z :  -1.327543E-01   1.000000E+00   1.000000E+00   1.000000E+00
     this yields min/max normalised fields (kG) : -1.434508E+04                      1.434422E+04
       @  X (cm),  Y (cm), Z (cm) :  -52.0       4.00      -2.90   /   52.0       4.00       2.90    

     Length of element,  XL =  3.600000E+02 cm 
                                               from  XI =  -1.800000E+02 cm 
                                               to    XF =   1.800000E+02 cm 

     Nbr of nodes in X = 361;  nbr of nodes in Y =   81
     X-size of mesh =  1.000000E+00 cm ; Y-size =  1.000000E-01 cm
     nber of mesh nodes in Z =   81 ; Step in Z =  0.100000E+00 cm

                     OPTION  DE  CALCUL  :  GRILLE  3-D  A  3*3*3  POINTS , INTERPOLATION  A  L'ORDRE  2

                    Field has been * by scaling factor    212.36466       (Brho_ref =    853712.44    )

                    Integration step :  0.3000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000          180.000     0.066    -0.000    -0.007    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000     0.066    -0.000    -0.007    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000     0.066    -0.000    -0.007    -0.000            3
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000     0.066    -0.000    -0.007    -0.000            4

     QUASEX - KPOS =  2 :  Element  is  mis-aligned  wrt.  the  optical  axis.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    6.93036840     m ;  Time  (for ref. rigidity & particle) =   2.311738E-08 s 

************************************************************************************************************************************
     10  Keyword, label(s) :  DRIFT                                                                                    IPASS= 1


                              Drift,  length =   -75.20000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  6.607145E-02 -1.128792E-04 -5.254190E-03 -2.117752E-02  6.1783778E+02  2.06090E-02
TRAJ #1 SX, SY, SZ, |S| :  1    1.543468E-01   3.160320E-01   9.361094E-01   1.000000E+00

 Cumulative length of optical axis =    6.17836840     m   ;  Time  (for reference rigidity & particle) =   2.060896E-08 s 

************************************************************************************************************************************
     11  Keyword, label(s) :  DRIFT                                                                                    IPASS= 1


                              Drift,  length =    35.88920  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  6.606740E-02 -1.128792E-04 -6.014235E-03 -2.117752E-02  6.5372698E+02  2.18061E-02
TRAJ #1 SX, SY, SZ, |S| :  1    1.543468E-01   3.160320E-01   9.361094E-01   1.000000E+00

 Cumulative length of optical axis =    6.53726040     m   ;  Time  (for reference rigidity & particle) =   2.180610E-08 s 

************************************************************************************************************************************
     12  Keyword, label(s) :  DRIFT                                                                                    IPASS= 1


                              Drift,  length =   -35.88920  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  6.607145E-02 -1.128792E-04 -5.254190E-03 -2.117752E-02  6.1783778E+02  2.06090E-02
TRAJ #1 SX, SY, SZ, |S| :  1    1.543468E-01   3.160320E-01   9.361094E-01   1.000000E+00

 Cumulative length of optical axis =    6.17836840     m   ;  Time  (for reference rigidity & particle) =   2.060896E-08 s 

************************************************************************************************************************************
     13  Keyword, label(s) :  TOSCA       rotatorV2H_in_RH                                                             IPASS= 1


     Status of MOD.MOD2 is 15.1 ; NDIM = 3 ; number of field data files used is   1.
     Field map set stored in field array HC(IMAP = 1)
     2-D map. MOD=15.  Single field map, with field coefficient value : 
                 7.485030E-05

     ZroBXY OPTION :  absent from map title. BX and BY values in Z=0 plane left as read.

     No  map  file  opened :  was  already  stored.
     Skip  reading  field  map  file :           b_rotatorModule_rightHanded.table
     Restored mesh coordinates for field map #IMAP=   1,  name : b_rotatorModule_rightHanded.table
     Field map scaled by factor  a(1) =   7.485030E-05

     Min/max fields found in map (series) read   :   1.080574E+05  /  -1.080509E+05
       @  X,  Y, Z :   52.0      -4.00       2.90                  /  -52.0      -4.00      -2.90    
     Given normalisation coeffs on field, x, y, z :  -1.327543E-01   1.000000E+00   1.000000E+00   1.000000E+00
     this yields min/max normalised fields (kG) : -1.434508E+04                      1.434422E+04
       @  X (cm),  Y (cm), Z (cm) :   52.0      -4.00       2.90   /  -52.0      -4.00      -2.90    

     Length of element,  XL =  3.600000E+02 cm 
                                               from  XI =  -1.800000E+02 cm 
                                               to    XF =   1.800000E+02 cm 

     Nbr of nodes in X = 361;  nbr of nodes in Y =   81
     X-size of mesh =  1.000000E+00 cm ; Y-size =  1.000000E-01 cm
     nber of mesh nodes in Z =   81 ; Step in Z =  0.100000E+00 cm

                     OPTION  DE  CALCUL  :  GRILLE  3-D  A  3*3*3  POINTS , INTERPOLATION  A  L'ORDRE  2

                    Field has been * by scaling factor    212.36466       (Brho_ref =    853712.44    )

                    Integration step :  0.3000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000          180.000     0.368    -0.000    -0.015    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000     0.368    -0.000    -0.015    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000     0.368    -0.000    -0.015    -0.000            3
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000     0.368    -0.000    -0.015    -0.000            4

     QUASEX - KPOS =  2 :  Element  is  mis-aligned  wrt.  the  optical  axis.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    9.77836840     m ;  Time  (for ref. rigidity & particle) =   3.261735E-08 s 

************************************************************************************************************************************
     14  Keyword, label(s) :  DRIFT                                                                                    IPASS= 1


                              Drift,  length =   -98.80000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  3.680842E-01 -1.615742E-04 -1.154391E-02 -3.072037E-02  8.7903816E+02  2.93218E-02
TRAJ #1 SX, SY, SZ, |S| :  1   -3.783084E-01  -5.506465E-01   7.440909E-01   1.000000E+00

 Cumulative length of optical axis =    8.79036840     m   ;  Time  (for reference rigidity & particle) =   2.932171E-08 s 

************************************************************************************************************************************
     15  Keyword, label(s) :  TOSCA       rotatorV2H_out_LH                                                            IPASS= 1


     Status of MOD.MOD2 is 15.1 ; NDIM = 3 ; number of field data files used is   1.
     Field map set stored in field array HC(IMAP = 2)
     2-D map. MOD=15.  Single field map, with field coefficient value : 
                 7.485030E-05

     ZroBXY OPTION :  absent from map title. BX and BY values in Z=0 plane left as read.

     No  map  file  opened :  was  already  stored.
     Skip  reading  field  map  file :           b_rotatorModule_leftHanded.table
     Restored mesh coordinates for field map #IMAP=   2,  name : b_rotatorModule_leftHanded.table
     Field map scaled by factor  a(1) =   7.485030E-05

     Min/max fields found in map (series) read   :   1.098269E+05  /  -1.098203E+05
       @  X,  Y, Z :  -52.0       4.00      -2.90                  /   52.0       4.00       2.90    
     Given normalisation coeffs on field, x, y, z :  -1.306153E-01   1.000000E+00   1.000000E+00   1.000000E+00
     this yields min/max normalised fields (kG) : -1.434508E+04                      1.434422E+04
       @  X (cm),  Y (cm), Z (cm) :  -52.0       4.00      -2.90   /   52.0       4.00       2.90    

     Length of element,  XL =  3.600000E+02 cm 
                                               from  XI =  -1.800000E+02 cm 
                                               to    XF =   1.800000E+02 cm 

     Nbr of nodes in X = 361;  nbr of nodes in Y =   81
     X-size of mesh =  1.000000E+00 cm ; Y-size =  1.000000E-01 cm
     nber of mesh nodes in Z =   81 ; Step in Z =  0.100000E+00 cm

                     OPTION  DE  CALCUL  :  GRILLE  3-D  A  3*3*3  POINTS , INTERPOLATION  A  L'ORDRE  2

                    Field has been * by scaling factor    263.07770       (Brho_ref =    853712.44    )

                    Integration step :  0.3000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000          180.000    -0.000    -0.000    -0.025    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000    -0.000    -0.000    -0.025    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000    -0.000    -0.000    -0.025    -0.000            3
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000    -0.000    -0.000    -0.025    -0.000            4

     QUASEX - KPOS =  2 :  Element  is  mis-aligned  wrt.  the  optical  axis.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    12.3903684     m ;  Time  (for ref. rigidity & particle) =   4.133010E-08 s 

************************************************************************************************************************************
     16  Keyword, label(s) :  DRIFT       rotator_adjL                                                                 IPASS= 1


                              Drift,  length =    71.83684  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -1.497293E-04 -2.230750E-04 -2.776084E-02 -4.235482E-02  1.3108756E+03  4.37264E-02
TRAJ #1 SX, SY, SZ, |S| :  1   -9.523573E-01   2.128968E-01   2.183817E-01   1.000000E+00

 Cumulative length of optical axis =    13.1087368     m   ;  Time  (for reference rigidity & particle) =   4.372633E-08 s 

************************************************************************************************************************************
     17  Keyword, label(s) :  MARKER      R-L-R-L-_E                                                                   IPASS= 1


************************************************************************************************************************************
     18  Keyword, label(s) :  DRIFT       DRIFT_1202DRIF                                                               IPASS= 1


                              Drift,  length =   212.53356  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -1.971403E-04 -2.230750E-04 -3.676266E-02 -4.235482E-02  1.5234091E+03  5.08158E-02
TRAJ #1 SX, SY, SZ, |S| :  1   -9.523573E-01   2.128968E-01   2.183817E-01   1.000000E+00

 Cumulative length of optical axis =    15.2340724     m   ;  Time  (for reference rigidity & particle) =   5.081574E-08 s 

************************************************************************************************************************************
     19  Keyword, label(s) :  MARKER      YO5_DMPT3.INST                                                               IPASS= 1


************************************************************************************************************************************
     20  Keyword, label(s) :  DRIFT       DRIFT_1203DRIF                                                               IPASS= 1


                              Drift,  length =   224.71380  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -2.472683E-04 -2.230750E-04 -4.628037E-02 -4.235482E-02  1.7481229E+03  5.83115E-02
TRAJ #1 SX, SY, SZ, |S| :  1   -9.523573E-01   2.128968E-01   2.183817E-01   1.000000E+00

 Cumulative length of optical axis =    17.4812104     m   ;  Time  (for reference rigidity & particle) =   5.831143E-08 s 

************************************************************************************************************************************
     21  Keyword, label(s) :  MARKER      YO5_DMPT3.INST                                                               IPASS= 1


************************************************************************************************************************************
     22  Keyword, label(s) :  DRIFT       DRIFT_1204DRIF                                                               IPASS= 1


                              Drift,  length =  1671.56080  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -6.201518E-04 -2.230750E-04 -1.170790E-01 -4.235482E-02  3.4196837E+03  1.14069E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -9.523573E-01   2.128968E-01   2.183817E-01   1.000000E+00

 Cumulative length of optical axis =    34.1968184     m   ;  Time  (for reference rigidity & particle) =   1.140691E-07 s 

************************************************************************************************************************************
     23  Keyword, label(s) :  MARKER      YO5_KFBH3             TKIC                                                   IPASS= 1


************************************************************************************************************************************
     24  Keyword, label(s) :  DRIFT       DRIFT_1205DRIF                                                               IPASS= 1


                              Drift,  length =   210.23120  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -6.670491E-04 -2.230750E-04 -1.259833E-01 -4.235482E-02  3.6299149E+03  1.21082E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -9.523573E-01   2.128968E-01   2.183817E-01   1.000000E+00

 Cumulative length of optical axis =    36.2991304     m   ;  Time  (for reference rigidity & particle) =   1.210817E-07 s 

************************************************************************************************************************************
     25  Keyword, label(s) :  DRIFT       YO5_B3                MONI                                                   IPASS= 1


                              Drift,  length =     0.00000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -6.670491E-04 -2.230750E-04 -1.259833E-01 -4.235482E-02  3.6299149E+03  1.21082E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -9.523573E-01   2.128968E-01   2.183817E-01   1.000000E+00

 Cumulative length of optical axis =    36.2991304     m   ;  Time  (for reference rigidity & particle) =   1.210817E-07 s 

************************************************************************************************************************************
     26  Keyword, label(s) :  DRIFT       DRIFT_1206DRIF                                                               IPASS= 1


                              Drift,  length =    47.14283  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -6.775655E-04 -2.230750E-04 -1.279801E-01 -4.235482E-02  3.6770578E+03  1.22654E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -9.523573E-01   2.128968E-01   2.183817E-01   1.000000E+00

 Cumulative length of optical axis =    36.7705587     m   ;  Time  (for reference rigidity & particle) =   1.226542E-07 s 

************************************************************************************************************************************
     27  Keyword, label(s) :  MULTIPOL    YO5_TV3               VKIC                                                   IPASS= 1


      -----  MULTIPOLE   : 
                Length  of  element  =   1.00000000E-04  cm
                Bore  radius      RO =    10.000      cm                              B/RO^IM/Brho (strength)
               B-DIPOLE      =  6.4438505E+05 kG   (i.e.,   7.5698700E+02 * SCAL)     7.5480339E+01
               B-QUADRUPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-SEXTUPOLE   =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               DIPOLE      Skew  angle =  1.5708000E+00 rd
               QUADRUPOLE  Skew  angle =  0.0000000E+00 rd
               SEXTUPOLE   Skew  angle =  0.0000000E+00 rd
               OCTUPOLE    Skew  angle =  0.0000000E+00 rd
               DECAPOLE    Skew  angle =  0.0000000E+00 rd
               DODECAPOLE  Skew  angle =  0.0000000E+00 rd
               14-POLE     Skew  angle =  0.0000000E+00 rd
               16-POLE     Skew  angle =  0.0000000E+00 rd
               18-POLE     Skew  angle =  0.0000000E+00 rd
               20-POLE     Skew  angle =  0.0000000E+00 rd

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   1.0000E+01

  ***  Warning : sharp edge model, vertical wedge focusing approximated with first order kick. FINT at entrance =    0.000    

  ***  Warning : sharp edge model,  vertical wedge focusing approximated with first order kick. FINT at exit =    0.000    

                    Field has been * by scaling factor    851.24982       (Brho_ref =    853712.44    )

                    Integration steps inside entrance|body|exit region : 
                                      0.00      2.500E-05   0.00     (cm)
                                    20             4      20

  A    1  1.0000     0.000     0.000     0.000     0.000            0.000    -0.001    -0.000    -0.128    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000            0.000    -0.001    -0.000    -0.128    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000            0.000    -0.001    -0.000    -0.128    -0.000            3
  A    1  1.0000     0.000     0.000     0.000     0.000            0.000    -0.001    -0.000    -0.128    -0.000            4

     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    36.7705597     m ;  Time  (for ref. rigidity & particle) =   1.226542E-07 s 

************************************************************************************************************************************
     28  Keyword, label(s) :  DRIFT       DRIFT_1207DRIF                                                               IPASS= 1


                              Drift,  length =    59.79680  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -6.908881E-04 -2.227978E-04 -1.350262E-01 -1.178352E-01  3.7368547E+03  1.24649E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -9.436300E-01   2.128967E-01   2.534509E-01   1.000000E+00

 Cumulative length of optical axis =    37.3685277     m   ;  Time  (for reference rigidity & particle) =   1.246488E-07 s 

************************************************************************************************************************************
     29  Keyword, label(s) :  MULTIPOL    YO5_QD3               QUAD                                                   IPASS= 1


      -----  MULTIPOLE   : 
                Length  of  element  =    210.04840      cm
                Bore  radius      RO =    10.000      cm                              B/RO^IM/Brho (strength)
               B-DIPOLE      =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-QUADRUPOLE  = -4.7903558E+01 kG   (i.e.,  -5.6261625E-02 * SCAL)    -5.6112053E-02
               B-SEXTUPOLE   =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    851.44285       (Brho_ref =    853712.44    )

                    Integration steps inside entrance|body|exit region : 
                                      0.00      0.500       0.00     (cm)
                                    30           420      30

  A    1  1.0000     0.000     0.000     0.000     0.000          210.048    -0.001    -0.000    -0.142     0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000          210.048    -0.001    -0.000    -0.142     0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000          210.048    -0.001    -0.000    -0.142     0.000            3
  A    1  1.0000     0.000     0.000     0.000     0.000          210.048    -0.001    -0.000    -0.142     0.000            4

     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    39.4690117     m ;  Time  (for ref. rigidity & particle) =   1.316553E-07 s 

************************************************************************************************************************************
     30  Keyword, label(s) :  DRIFT       DRIFT_1208DRIF                                                               IPASS= 1


                              Drift,  length =    48.97780  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -8.807810E-04 -1.099265E-03 -1.399911E-01  4.911202E-02  3.9958809E+03  1.33289E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -9.610946E-01   2.133061E-01   1.754928E-01   1.000000E+00

 Cumulative length of optical axis =    39.9587897     m   ;  Time  (for reference rigidity & particle) =   1.332891E-07 s 

************************************************************************************************************************************
     31  Keyword, label(s) :  MARKER      YO5_QS3               MULT                                                   IPASS= 1


************************************************************************************************************************************
     32  Keyword, label(s) :  DRIFT       DRIFT_1209DRIF                                                               IPASS= 1


                              Drift,  length =   137.09615  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -1.031486E-03 -1.099265E-03 -1.332580E-01  4.911202E-02  4.1329770E+03  1.37862E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -9.610946E-01   2.133061E-01   1.754928E-01   1.000000E+00

 Cumulative length of optical axis =    41.3297512     m   ;  Time  (for reference rigidity & particle) =   1.378621E-07 s 

************************************************************************************************************************************
     33  Keyword, label(s) :  MULTIPOL    YO5_QF2               QUAD                                                   IPASS= 1


      -----  MULTIPOLE   : 
                Length  of  element  =    339.16330      cm
                Bore  radius      RO =    10.000      cm                              B/RO^IM/Brho (strength)
               B-DIPOLE      =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-QUADRUPOLE  =  4.7642128E+01 kG   (i.e.,   5.5980255E-02 * SCAL)     5.5805826E-02
               B-SEXTUPOLE   =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    851.05235       (Brho_ref =    853712.44    )

                    Integration steps inside entrance|body|exit region : 
                                      0.00      0.500       0.00     (cm)
                                    30           678      30

  A    1  1.0000     0.000     0.000     0.000     0.000          339.163    -0.001     0.000    -0.160    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000          339.163    -0.001     0.000    -0.160    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000          339.163    -0.001     0.000    -0.160    -0.000            3
  A    1  1.0000     0.000     0.000     0.000     0.000          339.163    -0.001     0.000    -0.160    -0.000            4

     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    44.7213842     m ;  Time  (for ref. rigidity & particle) =   1.491755E-07 s 

************************************************************************************************************************************
     34  Keyword, label(s) :  DRIFT       DRIFT_1210DRIF                                                               IPASS= 1


                              Drift,  length =    49.42035  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -1.003261E-03  9.851383E-04 -1.704638E-01 -2.143490E-01  4.5215607E+03  1.50824E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -9.307176E-01   2.123380E-01   2.977872E-01   1.000000E+00

 Cumulative length of optical axis =    45.2155877     m   ;  Time  (for reference rigidity & particle) =   1.508240E-07 s 

************************************************************************************************************************************
     35  Keyword, label(s) :  MULTIPOL    YO5_TH2               HKIC                                                   IPASS= 1


      -----  MULTIPOLE   : 
                Length  of  element  =   1.00000000E-04  cm
                Bore  radius      RO =    10.000      cm                              B/RO^IM/Brho (strength)
               B-DIPOLE      = -0.0000000E+00 kG   (i.e.,  -0.0000000E+00 * SCAL)    -0.0000000E+00
               B-QUADRUPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-SEXTUPOLE   =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   0.0000E+00

                    Field has been * by scaling factor    851.24982       (Brho_ref =    853712.44    )

                    Integration steps inside entrance|body|exit region : 
                                      0.00      2.500E-05   0.00     (cm)
                                    20             4      20


                          ++++++++  ZERO  FIELD  ++++++++++
               Element  is  equivalent  to  drift  of  length      0.00010 cm

 Cumulative length of optical axis =    45.2155887     m   ;  Time  (for reference rigidity & particle) =   1.508240E-07 s 

************************************************************************************************************************************
     36  Keyword, label(s) :  DRIFT       DRIFT_1211DRIF                                                               IPASS= 1


                              Drift,  length =   118.24280  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -8.867749E-04  9.851383E-04 -1.958090E-01 -2.143490E-01  4.6398036E+03  1.54768E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -9.307176E-01   2.123380E-01   2.977872E-01   1.000000E+00

 Cumulative length of optical axis =    46.3980167     m   ;  Time  (for reference rigidity & particle) =   1.547682E-07 s 

************************************************************************************************************************************
     37  Keyword, label(s) :  MULTIPOL    YO5_QD1               QUAD                                                   IPASS= 1


      -----  MULTIPOLE   : 
                Length  of  element  =    144.00000      cm
                Bore  radius      RO =    10.000      cm                              B/RO^IM/Brho (strength)
               B-DIPOLE      =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-QUADRUPOLE  = -4.8309954E+01 kG   (i.e.,  -5.6738927E-02 * SCAL)    -5.6588087E-02
               B-SEXTUPOLE   =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    851.44285       (Brho_ref =    853712.44    )

                    Integration steps inside entrance|body|exit region : 
                                      0.00      0.500       0.00     (cm)
                                    30           288      30

  A    1  1.0000     0.000     0.000     0.000     0.000          144.000    -0.001     0.000    -0.215    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000          144.000    -0.001     0.000    -0.215    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000          144.000    -0.001     0.000    -0.215    -0.000            3
  A    1  1.0000     0.000     0.000     0.000     0.000          144.000    -0.001     0.000    -0.215    -0.000            4

     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    47.8380167     m ;  Time  (for ref. rigidity & particle) =   1.595715E-07 s 

************************************************************************************************************************************
     38  Keyword, label(s) :  DRIFT       DRIFT_1212DRIF                                                               IPASS= 1


                              Drift,  length =    33.69448  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -7.843291E-04  3.066843E-04 -2.162299E-01 -4.543917E-02  4.8174980E+03  1.60696E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -9.520813E-01   2.126511E-01   2.198198E-01   1.000000E+00

 Cumulative length of optical axis =    48.1749615     m   ;  Time  (for reference rigidity & particle) =   1.606955E-07 s 

************************************************************************************************************************************
     39  Keyword, label(s) :  DRIFT       YO5_B1                MONI                                                   IPASS= 1


                              Drift,  length =     0.00000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -7.843291E-04  3.066843E-04 -2.162299E-01 -4.543917E-02  4.8174980E+03  1.60696E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -9.520813E-01   2.126511E-01   2.198198E-01   1.000000E+00

 Cumulative length of optical axis =    48.1749615     m   ;  Time  (for reference rigidity & particle) =   1.606955E-07 s 

************************************************************************************************************************************
     40  Keyword, label(s) :  DRIFT       DRIFT_1213DRIF                                                               IPASS= 1


                              Drift,  length =    92.77038  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -7.558778E-04  3.066843E-04 -2.204453E-01 -4.543917E-02  4.9102684E+03  1.63790E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -9.520813E-01   2.126511E-01   2.198198E-01   1.000000E+00

 Cumulative length of optical axis =    49.1026652     m   ;  Time  (for reference rigidity & particle) =   1.637900E-07 s 

************************************************************************************************************************************
     41  Keyword, label(s) :  MULTIPOL    YO5_DH0_E1MULT                                                               IPASS= 1


      -----  MULTIPOLE   : 
                Length  of  element  =   1.00000000E-03  cm
                Bore  radius      RO =    10.000      cm                              B/RO^IM/Brho (strength)
               B-DIPOLE      =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-QUADRUPOLE  = -0.0000000E+00 kG   (i.e.,  -0.0000000E+00 * SCAL)    -0.0000000E+00
               B-SEXTUPOLE   =  4.9216711E+04 kG   (i.e.,   5.7817000E+01 * SCAL)     5.7650220E+02
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   3.3333E+00

                    Field has been * by scaling factor    851.24982       (Brho_ref =    853712.44    )

                    Integration steps inside entrance|body|exit region : 
                                      0.00      1.111E-04   0.00     (cm)
                                    30             9      30

  A    1  1.0000     0.000     0.000     0.000     0.000            0.001    -0.001     0.000    -0.220    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000            0.001    -0.001     0.000    -0.220    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000            0.001    -0.001     0.000    -0.220    -0.000            3
  A    1  1.0000     0.000     0.000     0.000     0.000            0.001    -0.001     0.000    -0.220    -0.000            4

     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    49.1026752     m ;  Time  (for ref. rigidity & particle) =   1.637900E-07 s 

************************************************************************************************************************************
     42  Keyword, label(s) :  BEND        YO5_DH0               SBEN                                                   IPASS= 1


      +++++        BEND  : 

                Length    =   3.588932E+02 cm
                Arc length    =   3.599349E+02 cm
                Deviation    =   8.703236E-01 deg.,    1.519001E-02 rad
                GAP   =   0.000000E+00 cm
                Gradient   =   0.000000E+00 kG/cm
                Grad-prime   =   0.000000E+00 kG/cm^2

                Field  =  3.6028466E+01  kG   (i.e.,   4.2324198E-02 * SCAL)
                Reference curvature radius (Brho/B) =   2.3695498E+04 cm
                Skew  angle  =   0.000000E+00  rad

               Entrance  face  
                DX =      0.000    LAMBDA =      0.000
                Wedge  angle  =  0.000000 RD

               Exit      face  
                DX =      0.038    LAMBDA =      0.000
                Wedge  angle  =  0.000000 RD

  ***  Warning : entrance sharp edge entails vertical wedge focusing approximated with first order kick, FINT values entr/exit :    0.000    

  ***  Warning : exit sharp edge entails vertical wedge focusing approximated with first order kick, FINT values entr/exit :    0.000    

                    Field has been * by scaling factor    851.24982       (Brho_ref =    853712.44    )

     CHXC - KPOS =  3 :  automatic positioning of element, 
        XCE, YCE, ALE =   0.000000000       0.000000000     -7.5950060000E-03 cm/cm/rad

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000          358.931     0.007    -0.008    -0.237    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000          358.931     0.007    -0.008    -0.237    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000          358.931     0.007    -0.008    -0.237    -0.000            3
  A    1  1.0000     0.000     0.000     0.000     0.000          358.931     0.007    -0.008    -0.237    -0.000            4

     QUASEX - KPOS =  3 :  Automatic  positionning  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle = -7.59500600E-03 rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    52.6916415     m ;  Time  (for ref. rigidity & particle) =   1.757616E-07 s 

************************************************************************************************************************************
     43  Keyword, label(s) :  MULTIPOL    YO5_DH0_E2MULT                                                               IPASS= 1


      -----  MULTIPOLE   : 
                Length  of  element  =   1.00000000E-03  cm
                Bore  radius      RO =    10.000      cm                              B/RO^IM/Brho (strength)
               B-DIPOLE      =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-QUADRUPOLE  = -0.0000000E+00 kG   (i.e.,  -0.0000000E+00 * SCAL)    -0.0000000E+00
               B-SEXTUPOLE   =  9.0258018E+03 kG   (i.e.,   1.0603000E+01 * SCAL)     1.0572414E+02
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   3.3333E+00

                    Field has been * by scaling factor    851.24982       (Brho_ref =    853712.44    )

                    Integration steps inside entrance|body|exit region : 
                                      0.00      1.111E-04   0.00     (cm)
                                    30             9      30

  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.007     0.000    -0.237    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.007     0.000    -0.237    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.007     0.000    -0.237    -0.000            3
  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.007     0.000    -0.237    -0.000            4

     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    52.6916515     m ;  Time  (for ref. rigidity & particle) =   1.757616E-07 s 

************************************************************************************************************************************
     44  Keyword, label(s) :  DRIFT       DRIFT_1214DRIF                                                               IPASS= 1


                              Drift,  length =   292.31132  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  2.013469E-02  4.415675E-02 -2.500359E-01 -4.543978E-02  5.5614784E+03  1.85512E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -2.196475E-01   9.504993E-01   2.197866E-01   1.000000E+00

 Cumulative length of optical axis =    55.6147646     m   ;  Time  (for reference rigidity & particle) =   1.855121E-07 s 

************************************************************************************************************************************
     45  Keyword, label(s) :  MARKER      YO5_RP0.2             INST                                                   IPASS= 1


************************************************************************************************************************************
     46  Keyword, label(s) :  DRIFT       DRIFT_1215DRIF                                                               IPASS= 1


                              Drift,  length =   180.20000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  2.809174E-02  4.415675E-02 -2.582241E-01 -4.543978E-02  5.7416784E+03  1.91523E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -2.196475E-01   9.504993E-01   2.197866E-01   1.000000E+00

 Cumulative length of optical axis =    57.4167646     m   ;  Time  (for reference rigidity & particle) =   1.915230E-07 s 

************************************************************************************************************************************
     47  Keyword, label(s) :  MARKER      YO5_RP0.1             INST                                                   IPASS= 1


************************************************************************************************************************************
     48  Keyword, label(s) :  DRIFT       DRIFT_1216DRIF                                                               IPASS= 1


                              Drift,  length =   228.17806  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  3.816734E-02  4.415675E-02 -2.685925E-01 -4.543978E-02  5.9698565E+03  1.99134E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -2.196475E-01   9.504993E-01   2.197866E-01   1.000000E+00

 Cumulative length of optical axis =    59.6985453     m   ;  Time  (for reference rigidity & particle) =   1.991343E-07 s 

************************************************************************************************************************************
     49  Keyword, label(s) :  MULTIPOL    ERDXMP05              MULT                                                   IPASS= 1


      -----  MULTIPOLE   : 
                Length  of  element  =   1.00000000E-03  cm
                Bore  radius      RO =    10.000      cm                              B/RO^IM/Brho (strength)
               B-DIPOLE      =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-QUADRUPOLE  = -8.1846819E+03 kG   (i.e.,  -9.6149000E+00 * SCAL)    -9.5871648E+00
               B-SEXTUPOLE   =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    851.24982       (Brho_ref =    853712.44    )

                    Integration steps inside entrance|body|exit region : 
                                      0.00      1.111E-04   0.00     (cm)
                                    30             9      30

  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.038     0.000    -0.269    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.038     0.000    -0.269    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.038     0.000    -0.269    -0.000            3
  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.038     0.000    -0.269    -0.000            4

     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    59.6985553     m ;  Time  (for ref. rigidity & particle) =   1.991343E-07 s 

************************************************************************************************************************************
     50  Keyword, label(s) :  MULTIPOL    G5_DHX_E1             MULT                                                   IPASS= 1


      -----  MULTIPOLE   : 
                Length  of  element  =   1.00000000E-03  cm
                Bore  radius      RO =    10.000      cm                              B/RO^IM/Brho (strength)
               B-DIPOLE      =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-QUADRUPOLE  = -2.8665838E+03 kG   (i.e.,  -3.3675000E+00 * SCAL)    -3.3577861E+00
               B-SEXTUPOLE   =  2.0538104E+03 kG   (i.e.,   2.4127000E+00 * SCAL)     2.4057403E+01
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    851.24982       (Brho_ref =    853712.44    )

                    Integration steps inside entrance|body|exit region : 
                                      0.00      1.111E-04   0.00     (cm)
                                    30             9      30

  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.038     0.000    -0.269    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.038     0.000    -0.269    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.038     0.000    -0.269    -0.000            3
  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.038     0.000    -0.269    -0.000            4

     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    59.6985653     m ;  Time  (for ref. rigidity & particle) =   1.991343E-07 s 

************************************************************************************************************************************
     51  Keyword, label(s) :  BEND        G5_DHX                SBEN                                                   IPASS= 1


      +++++        BEND  : 

                Length    =   3.700165E+02 cm
                Arc length    =   3.710924E+02 cm
                Deviation    =  -1.080644E+00 deg.,   -1.886079E-02 rad
                GAP   =   0.000000E+00 cm
                Gradient   =   0.000000E+00 kG/cm
                Grad-prime   =   0.000000E+00 kG/cm^2

                Field  = -4.3389981E+01  kG   (i.e.,  -5.0972089E-02 * SCAL)
                Reference curvature radius (Brho/B) =  -1.9675336E+04 cm
                Skew  angle  =   0.000000E+00  rad

               Entrance  face  
                DX =      0.000    LAMBDA =      0.000
                Wedge  angle  =  0.000000 RD

               Exit      face  
                DX =      0.047    LAMBDA =      0.000
                Wedge  angle  =  0.000000 RD

  ***  Warning : entrance sharp edge entails vertical wedge focusing approximated with first order kick, FINT values entr/exit :    0.000    

  ***  Warning : exit sharp edge entails vertical wedge focusing approximated with first order kick, FINT values entr/exit :    0.000    

                    Field has been * by scaling factor    851.24982       (Brho_ref =    853712.44    )

     CHXC - KPOS =  3 :  automatic positioning of element, 
        XCE, YCE, ALE =  -0.000000000       0.000000000      9.4303950000E-03 cm/cm/rad

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000          370.064     0.044     0.009    -0.285    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000          370.064     0.044     0.009    -0.285    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000          370.064     0.044     0.009    -0.285    -0.000            3
  A    1  1.0000     0.000     0.000     0.000     0.000          370.064     0.044     0.009    -0.285    -0.000            4

     QUASEX - KPOS =  3 :  Automatic  positionning  of  element.
          X =  -0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =  9.43039500E-03 rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    63.3987846     m ;  Time  (for ref. rigidity & particle) =   2.114770E-07 s 

************************************************************************************************************************************
     52  Keyword, label(s) :  MARKER      ELDXMP05              MULT                                                   IPASS= 1


************************************************************************************************************************************
     53  Keyword, label(s) :  MULTIPOL    G5_DHX_E2             MULT                                                   IPASS= 1


      -----  MULTIPOLE   : 
                Length  of  element  =   1.00000000E-03  cm
                Bore  radius      RO =    10.000      cm                              B/RO^IM/Brho (strength)
               B-DIPOLE      =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-QUADRUPOLE  = -7.4143859E-13 kG   (i.e.,  -8.7100000E-16 * SCAL)    -8.6848750E-16
               B-SEXTUPOLE   = -5.4382095E+03 kG   (i.e.,  -6.3885000E+00 * SCAL)    -6.3700717E+01
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    851.24982       (Brho_ref =    853712.44    )

                    Integration steps inside entrance|body|exit region : 
                                      0.00      1.111E-04   0.00     (cm)
                                    30             9      30

  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.044    -0.000    -0.285    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.044    -0.000    -0.285    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.044    -0.000    -0.285    -0.000            3
  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.044    -0.000    -0.285    -0.000            4

     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    63.3987946     m ;  Time  (for ref. rigidity & particle) =   2.114770E-07 s 

************************************************************************************************************************************
     54  Keyword, label(s) :  DRIFT       DRIFT_1217DRIF                                                               IPASS= 1


                              Drift,  length =   147.36620  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  4.294277E-02 -1.024347E-02 -2.919230E-01 -4.509172E-02  6.4872468E+03  2.16393E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -4.728091E-04  -9.755585E-01   2.197394E-01   1.000000E+00

 Cumulative length of optical axis =    64.8724566     m   ;  Time  (for reference rigidity & particle) =   2.163927E-07 s 

************************************************************************************************************************************
     55  Keyword, label(s) :  DRIFT       G5_BX                 MONI                                                   IPASS= 1


                              Drift,  length =     0.00000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  4.294277E-02 -1.024347E-02 -2.919230E-01 -4.509172E-02  6.4872468E+03  2.16393E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -4.728091E-04  -9.755585E-01   2.197394E-01   1.000000E+00

 Cumulative length of optical axis =    64.8724566     m   ;  Time  (for reference rigidity & particle) =   2.163927E-07 s 

************************************************************************************************************************************
     56  Keyword, label(s) :  DRIFT       DRIFT_1218DRIF                                                               IPASS= 1


                              Drift,  length =   832.63380  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  3.441371E-02 -1.024347E-02 -3.294679E-01 -4.509172E-02  7.3198806E+03  2.44167E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -4.728091E-04  -9.755585E-01   2.197394E-01   1.000000E+00

 Cumulative length of optical axis =    73.1987946     m   ;  Time  (for reference rigidity & particle) =   2.441665E-07 s 

************************************************************************************************************************************
     57  Keyword, label(s) :  MARKER      RHIC$END              MARK                                                   IPASS= 1


************************************************************************************************************************************
     58  Keyword, label(s) :  SPNPRT      MATRIX                                                                       IPASS= 1

                          -- 1  GROUPS  OF  MOMENTA  FOLLOW   --

                    ----------------------------------------------------------------------------------
                    Momentum  group  #1 (D=   1.000000E+00;  particles  1  to 3 ; 


                Spin  components  of  the      3  particles,  spin  angles :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   |Si,Sf|   (Z,Sf_yz) (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_yz : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000    -0.000473 -0.975559  0.219739  1.000000    272.7762  -90.027   77.607   77.306    1
 o  1  0.000000  1.000000  0.000000  1.000000    -0.000826 -0.219739 -0.975558  1.000000    272.7762 -102.694  134.291  167.306    2
 o  1  0.000000  0.000000  1.000000  1.000000     1.000000 -0.000643 -0.000702  1.000000    272.7762  -90.040  126.404   90.040    3


                  Spin transfer matrix, momentum group # 1 :

         -4.728091E-04   -8.256603E-04     1.00000    
         -0.975559       -0.219739       -6.426829E-04
          0.219739       -0.975558       -7.015854E-04

     Determinant =     1.0000000000
     Trace =    -0.2209133253;  spin precession acos((trace-1)/2) =   127.6225299779 deg
     Precession axis :  (-0.6154,  0.4926, -0.6153)  ->  angle to (X,Y) plane, to X axis :   -37.9754,   127.9838 deg
     Spin precession/2pi (or Qs, fractional) :    3.5451E-01

                    ----------------------------------------------------------------------------------
                    Remaining  1  particles  ; 


                Spin  components  of  the      1  particles,  spin  angles :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   |Si,Sf|   (Z,Sf_yz) (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_yz : projection of Sf on YZ plane)

 o  1  0.000000  0.000000  1.000000  1.000000     1.000000 -0.000643 -0.000702  1.000000    272.7762  -90.040  126.404   90.040    4

************************************************************************************************************************************
     59  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #     58)
                                                  4 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o   1   1.0000     0.000     0.000     0.000     0.000      0.0000   0.0000    0.034   -0.010   -0.329   -0.045   7.319881E+03     1
               Time of flight (mus) :  0.24416656     mass (MeV/c2) :   938.272    
o   1   1.0000     0.000     0.000     0.000     0.000      0.0000   0.0000    0.034   -0.010   -0.329   -0.045   7.319881E+03     2
               Time of flight (mus) :  0.24416656     mass (MeV/c2) :   938.272    
o   1   1.0000     0.000     0.000     0.000     0.000      0.0000   0.0000    0.034   -0.010   -0.329   -0.045   7.319881E+03     3
               Time of flight (mus) :  0.24416656     mass (MeV/c2) :   938.272    
o   1   1.0000     0.000     0.000     0.000     0.000      0.0000   0.0000    0.034   -0.010   -0.329   -0.045   7.319881E+03     4
               Time of flight (mus) :  0.24416656     mass (MeV/c2) :   938.272    

************************************************************************************************************************************
     60  Keyword, label(s) :  SPNPRT      MATRIX                                                                       IPASS= 1

                          -- 1  GROUPS  OF  MOMENTA  FOLLOW   --

                    ----------------------------------------------------------------------------------
                    Momentum  group  #1 (D=   1.000000E+00;  particles  1  to 3 ; 


                Spin  components  of  the      3  particles,  spin  angles :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   |Si,Sf|   (Z,Sf_yz) (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_yz : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000    -0.000473 -0.975559  0.219739  1.000000    272.7762  -90.027   77.607   77.306    1
 o  1  0.000000  1.000000  0.000000  1.000000    -0.000826 -0.219739 -0.975558  1.000000    272.7762 -102.694  134.291  167.306    2
 o  1  0.000000  0.000000  1.000000  1.000000     1.000000 -0.000643 -0.000702  1.000000    272.7762  -90.040  126.404   90.040    3


                  Spin transfer matrix, momentum group # 1 :

         -4.728091E-04   -8.256603E-04     1.00000    
         -0.975559       -0.219739       -6.426829E-04
          0.219739       -0.975558       -7.015854E-04

     Determinant =     1.0000000000
     Trace =    -0.2209133253;  spin precession acos((trace-1)/2) =   127.6225299779 deg
     Precession axis :  (-0.6154,  0.4926, -0.6153)  ->  angle to (X,Y) plane, to X axis :   -37.9754,   127.9838 deg
     Spin precession/2pi (or Qs, fractional) :    3.5451E-01

                    ----------------------------------------------------------------------------------
                    Remaining  1  particles  ; 


                Spin  components  of  the      1  particles,  spin  angles :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   |Si,Sf|   (Z,Sf_yz) (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_yz : projection of Sf on YZ plane)

 o  1  0.000000  0.000000  1.000000  1.000000     1.000000 -0.000643 -0.000702  1.000000    272.7762  -90.040  126.404   90.040    4

************************************************************************************************************************************
     61  Keyword, label(s) :  END                                                                                      IPASS= 1

                             4 particles have been launched
                     Made  it  to  the  end :      4

************************************************************************************************************************************
 Pgm zgoubi : Execution ended normally, upon keyword END or FIN
   
 File in:   zgoubi.FIT.out.dat
 File out:  zgoubi.res

  Zgoubi, author's Revision: 1670.
  Job  started  on  12-05-2022,  at  10:19:57 
  JOB  ENDED  ON    12-05-2022,  AT  10:19:58 

   CPU time, total :    0.81222799999999995     
