rotatorSTAR_R+L+R+L+_H2VYell-CW.inc
'OBJET'                                                                                                      1
853.71244451829796 * 1.e3  ! E_kin=255   Gg=489.046059786
2
6   2
0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00  1.00000000E+00 'o'
0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00  1.00000000E+00 'o'
0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00  1.00000000E+00 'o'
0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00  1.00000000E+00 'o'
0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00  1.00000000E+00 'o'
0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00  1.00000000E+00 'o'
1 1 1 1 1 1
 
'PARTICUL'                                                                                                   2
PROTON
'SPNTRK'                                                                                                     3
4
1.00000000E+00  0.00000000E+00  0.00000000E+00
0.00000000E+00  1.00000000E+00  0.00000000E+00
0.00000000E+00  0.00000000E+00  1.00000000E+00
1.00000000E+00  0.00000000E+00  0.00000000E+00
0.00000000E+00  1.00000000E+00  0.00000000E+00
0.00000000E+00  0.00000000E+00  1.00000000E+00
 
'SCALING'                                                                                                    4
1  10
BEND
-1
8.5124981689E+02
1
MULTIPOL
-1
8.5124981689E+02
1
MULTIPOL *QF*
-1
8.5105235000E+02
1
MULTIPOL *QD*
-1
8.5144285000E+02
1
MULTIPOL *SXF*
-1
8.0000000000E+02
1
MULTIPOL *SXD*
-1
1.0000000000E+03
1
TOSCA  snk1*
-1
1.0000000000E+00
1
TOSCA  snk2*
-1
1.0000000000E+00
1
TOSCA  rotatorH2V_out*   ! goes horiz to vert
-1
2.6370126331E+02
1.
TOSCA  rotatorH2V_in*
-1
2.0970920063E+02
1.
 
'OPTIONS'                                                                                                    5
1 1
WRITE ON
 
'MARKER'   RHIC$STARTMARK                                                                                    6
'MARKER'   G6_MARKX  MARK                                                                                    7
'SPNPRT'   Clock6    PRINT                                                                                   8
'DRIFT'    DRIFT_0   DRIF                                                                                    9
799.309000
'DRIFT'    G6_BX     MONI                                                                                   10
0.000000
'DRIFT'    DRIFT_1   DRIF                                                                                   11
180.691000
'MARKER'   ELDXPM06  MULT                                                                                   12
'MULTIPOL' G6_DHX_E1 MULT                                                                                   13
0  .Mult
0.001000  10.00   0.00000000E+00  -1.13660000E-15   8.33680000E+00  0.0  0.0  0.0  0.0  0.0  0.0  0.0
0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00
0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00
0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#30|9|30   MultG6_DHX_E1
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'BEND'     G6_DHX    SBEN                                                                                   14
0  .Bend
3.700164548E+02   0.000000000E+00   5.096457494E-02
0.00  0.00   0.00000000
4 .2401  1.8639  -.5572  .3904 0. 0. 0.
0.00  0.00   0.00000000
4 .2401  1.8639  -.5572  .3904 0. 0. 0.
1.  !  Bend G6_DHX
3 0. 0.    -9.429005E-03
'MULTIPOL' ERDXPM06  MULT                                                                                   15
0  .Mult
0.001000  10.00   0.00000000E+00  -9.61490000E+00   0.00000000E+00  0.0  0.0  0.0  0.0  0.0  0.0  0.0
0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00
0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00
0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#30|9|30   MultERDXPM06
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'MULTIPOL' G6_DHX_E2 MULT                                                                                   16
0  .Mult
0.001000  10.00   0.00000000E+00   5.12240000E+00   3.67000000E+00  0.0  0.0  0.0  0.0  0.0  0.0  0.0
0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00
0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00
0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#30|9|30   MultG6_DHX_E2
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'DRIFT'    DRIFT_2   DRIF                                                                                   17
700.689380
'MULTIPOL' YI6_DH0_E1MULT                                                                                   18
0  .Mult
0.001000  10.00   0.00000000E+00   0.00000000E+00  -1.27290000E+01  0.0  0.0  0.0  0.0  0.0  0.0  0.0
0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00
0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00
0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#30|9|30   MultYI6_DH0_E1
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'BEND'     YI6_DH0   SBEN                                                                                   19
0  .Bend
3.588931744E+02   0.000000000E+00  -4.231372201E-02
0.00  0.00   0.00000000
4 .2401  1.8639  -.5572  .3904 0. 0. 0.
0.00  0.00   0.00000000
4 .2401  1.8639  -.5572  .3904 0. 0. 0.
1.  !  Bend YI6_DH0
3 0. 0.     7.593126E-03
'MULTIPOL' YI6_DH0_E2MULT                                                                                   20
0  .Mult
0.001000  10.00   0.00000000E+00   0.00000000E+00  -6.22510000E+01  0.0  0.0  0.0  0.0  0.0  0.0  0.0
0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00
0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00
0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#30|9|30   MultYI6_DH0_E2
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'DRIFT'    DRIFT_3   DRIF                                                                                   21
92.770376
'DRIFT'    YI6_B1    MONI                                                                                   22
0.000000
'DRIFT'    DRIFT_4   DRIF                                                                                   23
33.694480
'MULTIPOL' YI6_QF1   QUAD                                                                                   24
0  .Quad
144.000000  10.00   0.00000000E+00   5.62677735E-02   0.00000000E+00  0.0  0.0  0.0  0.0  0.0  0.0  0.0
0.0000   0.0000   6.00   3.00   1.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00
0.0000   0.0000   6.00   3.00   1.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00
0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#30|288|30   Quad  YI6_QF1
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'DRIFT'    DRIFT_5   DRIF                                                                                   25
118.242800
'MULTIPOL' YI6_TV2   VKIC                                                                                   26
0  .kicker
0.000100  10.00   1.07238000E+03   0.00000000E+00   0.00000000E+00  0.0  0.0  0.0  0.0  0.0  0.0  0.0
0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
4   1.45500E-01   2.26700E+00  -6.39500E-01   1.15580E+00   0.00000E+00   0.00000E+00
0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
4   1.45500E-01   2.26700E+00  -6.39500E-01   1.15580E+00   0.00000E+00   0.00000E+00
1.57080E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#20|4|20  Kick
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'DRIFT'    DRIFT_6   DRIF                                                                                   27
49.420350
'MULTIPOL' YI6_QD2   QUAD                                                                                   28
0  .Quad
339.163300  10.00   0.00000000E+00  -5.58893036E-02   0.00000000E+00  0.0  0.0  0.0  0.0  0.0  0.0  0.0
0.0000   0.0000   6.00   3.00   1.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00
0.0000   0.0000   6.00   3.00   1.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00
0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#30|678|30   Quad  YI6_QD2
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'DRIFT'    DRIFT_7   DRIF                                                                                   29
137.096150
'MARKER'   YI6_QS3   MULT                                                                                   30
'DRIFT'    DRIFT_8   DRIF                                                                                   31
48.977800
'MULTIPOL' YI6_QF3   QUAD                                                                                   32
0  .Quad
210.048400  10.00   0.00000000E+00   5.62903698E-02   0.00000000E+00  0.0  0.0  0.0  0.0  0.0  0.0  0.0
0.0000   0.0000   6.00   3.00   1.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00
0.0000   0.0000   6.00   3.00   1.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00
0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#30|420|30   Quad  YI6_QF3
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'DRIFT'    DRIFT_9   DRIF                                                                                   33
59.796800
'MULTIPOL' YI6_TH3   HKIC                                                                                   34
0  .kicker
0.000100  10.00  -0.00000000E+00   0.00000000E+00   0.00000000E+00  0.0  0.0  0.0  0.0  0.0  0.0  0.0
0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
4   1.45500E-01   2.26700E+00  -6.39500E-01   1.15580E+00   0.00000E+00   0.00000E+00
0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
4   1.45500E-01   2.26700E+00  -6.39500E-01   1.15580E+00   0.00000E+00   0.00000E+00
0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#20|4|20  Kick
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'DRIFT'    DRIFT_10  DRIF                                                                                   35
47.142826
'DRIFT'    YI6_B3    MONI                                                                                   36
0.000000
'DRIFT'    DRIFT_11  DRIF                                                                                   37
210.236100
'MARKER'   YI6_KFBH3 TKIC                                                                                   38
'DRIFT'    DRIFT_12  DRIF                                                                                   39
2109.239902
 
'MARKER'  rotSTAR_H2V_S                                                                                     40
 
! 'INCLUDE'
! Include_SegmentStart : R+L+R+L+.inc[R+L+R+L+_S,*:R+L+R+L+_E,*]     (n_inc, depth :  1 2)
'MARKER'  R+L+R+L+_S                                                                                        41
 
'DRIFT' rotator_adjL                                                                                        42
71.836840
 
'TOSCA'   rotatorH2V_out_RH                                                                                 43
0  0020  ! .plt
1.00000000E+00  1.00000000E+00  1.00000000E+00  1.00000000E+00
HEADER_8   RHIC_helix
361 81 81  15.1  0.748502994012e-4     ! = 1/1.3360e4
b_rotatorModule_rightHanded.table
0 0 0 0
2
.3
2   0.00000000E+00  0.00000000E+00  0.00000000E+00
 
'DRIFT'                                                                                                     44
-98.800000
 
'TOSCA'   rotatorH2V_in_LH                                                                                  45
0  0020  ! .plt
1.00000000E+00  1.00000000E+00  1.00000000E+00  1.00000000E+00
HEADER_8   RHIC_helix
361 81 81  15.1  0.748502994012e-4     ! = 1/1.3360e4
b_rotatorModule_leftHanded.table
0 0 0 0
2
.3
2   0.00000000E+00  0.00000000E+00  0.00000000E+00
 
'DRIFT'                                                                                                     46
-75.200000
'DRIFT'                                                                                                     47
35.889200
'DRIFT'                                                                                                     48
-35.889200
 
'TOSCA'  rotatorH2V_in_RH                                                                                   49
0  0020  ! .plt
1.00000000E+00  1.00000000E+00  1.00000000E+00  1.00000000E+00
HEADER_8   RHIC_helix
361 81 81  15.1  0.748502994012e-4     ! = 1/1.3360e4
b_rotatorModule_rightHanded.table
0 0 0 0
2
.3
2   0.00000000E+00  0.00000000E+00  0.00000000E+00
 
'DRIFT'                                                                                                     50
-98.800000
 
'TOSCA'   rotatorH2V_out_LH                                                                                 51
0  0020  ! .plt
1.00000000E+00  1.00000000E+00  1.00000000E+00  1.00000000E+00
HEADER_8   RHIC_helix
361 81 81  15.1  0.748502994012e-4     ! = 1/1.3360e4
b_rotatorModule_leftHanded.table
0 0 0 0
2
.3
2   0.00000000E+00  0.00000000E+00  0.00000000E+00
 
'DRIFT' rotator_adjL                                                                                        52
71.836840
 
! Include_SegmentEnd : R+L+R+L+.inc                                  (had n_inc, depth :  1 2)
'MARKER'  R+L+R+L+_E                                                                                        53
 
! 'FIT2'                                                                                                      54
! 2
! 4 36 0 .99
! 4 40 0 .99
! 2  1e-10
! 10 1 3 #End 1. .1 0
! 10 1 4 #End 1. .1 0
 
!10.2 1 0 #End 3.1415926 1. 0   ! spin rotation= pi
!10.3 1 1 #End 0. 1. 0          ! X component of rotation axis is 0
 
'FAISCEAU'                                                                                                  54
'SPNPRT'  MATRIX                                                                                            55
 
'END'                                                                                                       17

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =     853712.445 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       6 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 1

     Particle  properties :
     PROTON
                     Mass          =    938.272        MeV/c2
                     Charge        =   1.602176E-19    C     
                     G  factor     =    1.79285              
                     COM life-time =   1.000000E+99    s     

              Reference  data :
                    mag. rigidity (kG.cm)   :   853712.44      =p/q, such that dev.=B*L/rigidity
                    mass (MeV/c2)           :   938.27208    
                    momentum (MeV/c)        :   255936.55    
                    energy, total (MeV)     :   255938.27    
                    energy, kinetic (MeV)   :   255000.00    
                    beta = v/c              :  0.9999932802    
                    gamma                   :   272.7761777    
                    beta*gamma              :   272.7743447    
                    G*gamma                 :   489.0460474    
                    m / G                   :   523.3418681    
                    electric rigidity (MeV) :   255934.8323    =T[eV]*(gamma+1)/gamma, such that dev.=E*L/rigidity
  
 I, AMQ(1,I), AMQ(2,I)/QE, P/Pref, v/c, time, s :
  
     1   9.38272081E+02  1.00000000E+00  1.00000000E+00  9.99993280E-01  0.00000000E+00  0.00000000E+00
     2   9.38272081E+02  1.00000000E+00  1.00000000E+00  9.99993280E-01  0.00000000E+00  0.00000000E+00
     3   9.38272081E+02  1.00000000E+00  1.00000000E+00  9.99993280E-01  0.00000000E+00  0.00000000E+00
     4   9.38272081E+02  1.00000000E+00  1.00000000E+00  9.99993280E-01  0.00000000E+00  0.00000000E+00
     5   9.38272081E+02  1.00000000E+00  1.00000000E+00  9.99993280E-01  0.00000000E+00  0.00000000E+00
     6   9.38272081E+02  1.00000000E+00  1.00000000E+00  9.99993280E-01  0.00000000E+00  0.00000000E+00

************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 1


                Spin  tracking  requested.


                          Particle  mass          =    938.2721     MeV/c2
                          Gyromagnetic  factor  G =    1.792847    

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 6 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     853712.44     kG*cm
                               beta      =    0.99999328    
                               gamma     =     272.77618    
                               G*gamma   =     489.04605    
                               M / G     =     523.3418681    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        6  PARTICULES :
                               <SX> =     0.577350
                               <SY> =     0.577350
                               <SZ> =     0.577350
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  SCALING                                                                                  IPASS= 1


               PRINT option is OFF.

               Scaling  request  on 10  families  of  optical  elements :
                                                      

     Family number   1
          Element [/label(s) ( 0)] to be scaled :          BEND
               Family not labeled ;  this scaling will apply to all (unlabeled) elements  "BEND      "
               Scaling of fields follows increase of rigidity taken from CAVITE, starting scaling value    8.51249817E+02

     Family number   2
          Element [/label(s) ( 0)] to be scaled :          MULTIPOL
               Family not labeled ;  this scaling will apply to all (unlabeled) elements  "MULTIPOL  "
               Scaling of fields follows increase of rigidity taken from CAVITE, starting scaling value    8.51249817E+02

     Family number   3
          Element [/label(s) ( 1)] to be scaled :          MULTIPOL
                                                           /*QF*
               Scaling of fields follows increase of rigidity taken from CAVITE, starting scaling value    8.51052350E+02

     Family number   4
          Element [/label(s) ( 1)] to be scaled :          MULTIPOL
                                                           /*QD*
               Scaling of fields follows increase of rigidity taken from CAVITE, starting scaling value    8.51442850E+02

     Family number   5
          Element [/label(s) ( 1)] to be scaled :          MULTIPOL
                                                           /*SXF*
               Scaling of fields follows increase of rigidity taken from CAVITE, starting scaling value    8.00000000E+02

     Family number   6
          Element [/label(s) ( 1)] to be scaled :          MULTIPOL
                                                           /*SXD*
               Scaling of fields follows increase of rigidity taken from CAVITE, starting scaling value    1.00000000E+03

     Family number   7
          Element [/label(s) ( 1)] to be scaled :          TOSCA
                                                           /snk1*
               Scaling of fields follows increase of rigidity taken from CAVITE, starting scaling value    1.00000000E+00

     Family number   8
          Element [/label(s) ( 1)] to be scaled :          TOSCA
                                                           /snk2*
               Scaling of fields follows increase of rigidity taken from CAVITE, starting scaling value    1.00000000E+00

     Family number   9
          Element [/label(s) ( 1)] to be scaled :          TOSCA
                                                           /rotatorH2V_out*
               Scaling of fields follows increase of rigidity taken from CAVITE, starting scaling value    2.63701263E+02

     Family number  10
          Element [/label(s) ( 1)] to be scaled :          TOSCA
                                                           /rotatorH2V_in*
               Scaling of fields follows increase of rigidity taken from CAVITE, starting scaling value    2.09709201E+02

************************************************************************************************************************************
      5  Keyword, label(s) :  OPTIONS                                                                                  IPASS= 1

         A list of 1 option(s) is expected.  List and actions taken are as follows :


    Option #  1 : WRITE ON

    WRITE ON -> 'WRITE' bit in 'OPTIONS' set to 1.

************************************************************************************************************************************
      6  Keyword, label(s) :  MARKER      RHIC$STARTMARK                                                               IPASS= 1


************************************************************************************************************************************
      7  Keyword, label(s) :  MARKER      G6_MARKX              MARK                                                   IPASS= 1


************************************************************************************************************************************
      8  Keyword, label(s) :  SPNPRT      Clock6                PRINT                                                  IPASS= 1



                Spin  components  of  the      6  particles,  spin  angles :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   |Si,Sf|   (Z,Sf_yz) (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_yz : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000     1.000000  0.000000  0.000000  1.000000    272.7762    0.000    0.000   90.000    1
 o  1  0.000000  1.000000  0.000000  1.000000     0.000000  1.000000  0.000000  1.000000    272.7762    0.000   90.000   90.000    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000  0.000000  1.000000  1.000000    272.7762    0.000   45.000    0.000    3
 o  1  1.000000  0.000000  0.000000  1.000000     1.000000  0.000000  0.000000  1.000000    272.7762    0.000    0.000   90.000    4
 o  1  0.000000  1.000000  0.000000  1.000000     0.000000  1.000000  0.000000  1.000000    272.7762    0.000   90.000   90.000    5
 o  1  0.000000  0.000000  1.000000  1.000000     0.000000  0.000000  1.000000  1.000000    272.7762    0.000   45.000    0.000    6

************************************************************************************************************************************
      9  Keyword, label(s) :  DRIFT       DRIFT_0               DRIF                                                   IPASS= 1


                              Drift,  length =   799.30900  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  7.9930900E+02  2.66623E-02
TRAJ #1 SX, SY, SZ, |S| :  1    1.000000E+00   0.000000E+00   0.000000E+00   1.000000E+00

 Cumulative length of optical axis =    7.99309000     m   ;  Time  (for reference rigidity & particle) =   2.666226E-08 s 

************************************************************************************************************************************
     10  Keyword, label(s) :  DRIFT       G6_BX                 MONI                                                   IPASS= 1


                              Drift,  length =     0.00000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  7.9930900E+02  2.66623E-02
TRAJ #1 SX, SY, SZ, |S| :  1    1.000000E+00   0.000000E+00   0.000000E+00   1.000000E+00

 Cumulative length of optical axis =    7.99309000     m   ;  Time  (for reference rigidity & particle) =   2.666226E-08 s 

************************************************************************************************************************************
     11  Keyword, label(s) :  DRIFT       DRIFT_1               DRIF                                                   IPASS= 1


                              Drift,  length =   180.69100  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  9.8000000E+02  3.26895E-02
TRAJ #1 SX, SY, SZ, |S| :  1    1.000000E+00   0.000000E+00   0.000000E+00   1.000000E+00

 Cumulative length of optical axis =    9.80000000     m   ;  Time  (for reference rigidity & particle) =   3.268950E-08 s 

************************************************************************************************************************************
     12  Keyword, label(s) :  MARKER      ELDXPM06              MULT                                                   IPASS= 1


************************************************************************************************************************************
     13  Keyword, label(s) :  MULTIPOL    G6_DHX_E1             MULT                                                   IPASS= 1


      -----  MULTIPOLE   : 
                Length  of  element  =   1.00000000E-03  cm
                Bore  radius      RO =    10.000      cm                              B/RO^IM/Brho (strength)
               B-DIPOLE      =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-QUADRUPOLE  = -9.6753054E-13 kG   (i.e.,  -1.1366000E-15 * SCAL)    -1.1333214E-15
               B-SEXTUPOLE   =  7.0966995E+03 kG   (i.e.,   8.3368000E+00 * SCAL)     8.3127516E+01
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    851.24982       (Brho_ref =    853712.44    )

                    Integration steps inside entrance|body|exit region : 
                                      0.00      1.111E-04   0.00     (cm)
                                    30             9      30

  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.000     0.000     0.000     0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.000     0.000     0.000     0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.000     0.000     0.000     0.000            3
  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.000     0.000     0.000     0.000            4
  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.000     0.000     0.000     0.000            5
  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.000     0.000     0.000     0.000            6

     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    9.80001000     m ;  Time  (for ref. rigidity & particle) =   3.268953E-08 s 

************************************************************************************************************************************
     14  Keyword, label(s) :  BEND        G6_DHX                SBEN                                                   IPASS= 1


      +++++        BEND  : 

                Length    =   3.700165E+02 cm
                Arc length    =   3.710924E+02 cm
                Deviation    =   1.080484E+00 deg.,    1.885801E-02 rad
                GAP   =   0.000000E+00 cm
                Gradient   =   0.000000E+00 kG/cm
                Grad-prime   =   0.000000E+00 kG/cm^2

                Field  =  4.3383585E+01  kG   (i.e.,   5.0964575E-02 * SCAL)
                Reference curvature radius (Brho/B) =   1.9678236E+04 cm
                Skew  angle  =   0.000000E+00  rad

               Entrance  face  
                DX =      0.000    LAMBDA =      0.000
                Wedge  angle  =  0.000000 RD

               Exit      face  
                DX =      0.047    LAMBDA =      0.000
                Wedge  angle  =  0.000000 RD

  ***  Warning : entrance sharp edge entails vertical wedge focusing approximated with first order kick, FINT values entr/exit :    0.000    

  ***  Warning : exit sharp edge entails vertical wedge focusing approximated with first order kick, FINT values entr/exit :    0.000    

                    Field has been * by scaling factor    851.24982       (Brho_ref =    853712.44    )

     CHXC - KPOS =  3 :  automatic positioning of element, 
        XCE, YCE, ALE =   0.000000000       0.000000000     -9.4290050000E-03 cm/cm/rad

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000          370.064     0.010    -0.009     0.000     0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000          370.064     0.010    -0.009     0.000     0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000          370.064     0.010    -0.009     0.000     0.000            3
  A    1  1.0000     0.000     0.000     0.000     0.000          370.064     0.010    -0.009     0.000     0.000            4
  A    1  1.0000     0.000     0.000     0.000     0.000          370.064     0.010    -0.009     0.000     0.000            5
  A    1  1.0000     0.000     0.000     0.000     0.000          370.064     0.010    -0.009     0.000     0.000            6

     QUASEX - KPOS =  3 :  Automatic  positionning  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle = -9.42900500E-03 rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    13.5002294     m ;  Time  (for ref. rigidity & particle) =   4.503222E-08 s 

************************************************************************************************************************************
     15  Keyword, label(s) :  MULTIPOL    ERDXPM06              MULT                                                   IPASS= 1


      -----  MULTIPOLE   : 
                Length  of  element  =   1.00000000E-03  cm
                Bore  radius      RO =    10.000      cm                              B/RO^IM/Brho (strength)
               B-DIPOLE      =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-QUADRUPOLE  = -8.1846819E+03 kG   (i.e.,  -9.6149000E+00 * SCAL)    -9.5871648E+00
               B-SEXTUPOLE   =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    851.24982       (Brho_ref =    853712.44    )

                    Integration steps inside entrance|body|exit region : 
                                      0.00      1.111E-04   0.00     (cm)
                                    30             9      30

  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.010     0.000     0.000     0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.010     0.000     0.000     0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.010     0.000     0.000     0.000            3
  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.010     0.000     0.000     0.000            4
  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.010     0.000     0.000     0.000            5
  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.010     0.000     0.000     0.000            6

     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    13.5002394     m ;  Time  (for ref. rigidity & particle) =   4.503225E-08 s 

************************************************************************************************************************************
     16  Keyword, label(s) :  MULTIPOL    G6_DHX_E2             MULT                                                   IPASS= 1


      -----  MULTIPOLE   : 
                Length  of  element  =   1.00000000E-03  cm
                Bore  radius      RO =    10.000      cm                              B/RO^IM/Brho (strength)
               B-DIPOLE      =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-QUADRUPOLE  =  4.3604421E+03 kG   (i.e.,   5.1224000E+00 * SCAL)     5.1076239E+00
               B-SEXTUPOLE   =  3.1240868E+03 kG   (i.e.,   3.6700000E+00 * SCAL)     3.6594135E+01
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    851.24982       (Brho_ref =    853712.44    )

                    Integration steps inside entrance|body|exit region : 
                                      0.00      1.111E-04   0.00     (cm)
                                    30             9      30

  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.010     0.000     0.000     0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.010     0.000     0.000     0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.010     0.000     0.000     0.000            3
  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.010     0.000     0.000     0.000            4
  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.010     0.000     0.000     0.000            5
  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.010     0.000     0.000     0.000            6

     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    13.5002494     m ;  Time  (for ref. rigidity & particle) =   4.503229E-08 s 

************************************************************************************************************************************
     17  Keyword, label(s) :  DRIFT       DRIFT_2               DRIF                                                   IPASS= 1


                              Drift,  length =   700.68938  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  4.818036E-02  5.439852E-02  0.000000E+00  0.000000E+00  2.0507144E+03  6.84049E-02
TRAJ #1 SX, SY, SZ, |S| :  1   -9.738938E-01  -2.270042E-01   0.000000E+00   1.000000E+00

 Cumulative length of optical axis =    20.5071432     m   ;  Time  (for reference rigidity & particle) =   6.840493E-08 s 

************************************************************************************************************************************
     18  Keyword, label(s) :  MULTIPOL    YI6_DH0_E1MULT                                                               IPASS= 1


      -----  MULTIPOLE   : 
                Length  of  element  =   1.00000000E-03  cm
                Bore  radius      RO =    10.000      cm                              B/RO^IM/Brho (strength)
               B-DIPOLE      =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-QUADRUPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-SEXTUPOLE   = -1.0835559E+04 kG   (i.e.,  -1.2729000E+01 * SCAL)    -1.2692282E+02
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   3.3333E+00

                    Field has been * by scaling factor    851.24982       (Brho_ref =    853712.44    )

                    Integration steps inside entrance|body|exit region : 
                                      0.00      1.111E-04   0.00     (cm)
                                    30             9      30

  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.048     0.000     0.000     0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.048     0.000     0.000     0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.048     0.000     0.000     0.000            3
  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.048     0.000     0.000     0.000            4
  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.048     0.000     0.000     0.000            5
  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.048     0.000     0.000     0.000            6

     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    20.5071532     m ;  Time  (for ref. rigidity & particle) =   6.840496E-08 s 

************************************************************************************************************************************
     19  Keyword, label(s) :  BEND        YI6_DH0               SBEN                                                   IPASS= 1


      +++++        BEND  : 

                Length    =   3.588932E+02 cm
                Arc length    =   3.599349E+02 cm
                Deviation    =  -8.701081E-01 deg.,   -1.518625E-02 rad
                GAP   =   0.000000E+00 cm
                Gradient   =   0.000000E+00 kG/cm
                Grad-prime   =   0.000000E+00 kG/cm^2

                Field  = -3.6019548E+01  kG   (i.e.,  -4.2313722E-02 * SCAL)
                Reference curvature radius (Brho/B) =  -2.3701365E+04 cm
                Skew  angle  =   0.000000E+00  rad

               Entrance  face  
                DX =      0.000    LAMBDA =      0.000
                Wedge  angle  =  0.000000 RD

               Exit      face  
                DX =      0.038    LAMBDA =      0.000
                Wedge  angle  =  0.000000 RD

  ***  Warning : entrance sharp edge entails vertical wedge focusing approximated with first order kick, FINT values entr/exit :    0.000    

  ***  Warning : exit sharp edge entails vertical wedge focusing approximated with first order kick, FINT values entr/exit :    0.000    

                    Field has been * by scaling factor    851.24982       (Brho_ref =    853712.44    )

     CHXC - KPOS =  3 :  automatic positioning of element, 
        XCE, YCE, ALE =  -0.000000000       0.000000000      7.5931260000E-03 cm/cm/rad

                    Integration step :   1.000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000          358.931     0.060     0.008     0.000     0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000          358.931     0.060     0.008     0.000     0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000          358.931     0.060     0.008     0.000     0.000            3
  A    1  1.0000     0.000     0.000     0.000     0.000          358.931     0.060     0.008     0.000     0.000            4
  A    1  1.0000     0.000     0.000     0.000     0.000          358.931     0.060     0.008     0.000     0.000            5
  A    1  1.0000     0.000     0.000     0.000     0.000          358.931     0.060     0.008     0.000     0.000            6

     QUASEX - KPOS =  3 :  Automatic  positionning  of  element.
          X =  -0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =  7.59312600E-03 rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    24.0961194     m ;  Time  (for ref. rigidity & particle) =   8.037654E-08 s 

************************************************************************************************************************************
     20  Keyword, label(s) :  MULTIPOL    YI6_DH0_E2MULT                                                               IPASS= 1


      -----  MULTIPOLE   : 
                Length  of  element  =   1.00000000E-03  cm
                Bore  radius      RO =    10.000      cm                              B/RO^IM/Brho (strength)
               B-DIPOLE      =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-QUADRUPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-SEXTUPOLE   = -5.2991152E+04 kG   (i.e.,  -6.2251000E+01 * SCAL)    -6.2071430E+02
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   3.3333E+00

                    Field has been * by scaling factor    851.24982       (Brho_ref =    853712.44    )

                    Integration steps inside entrance|body|exit region : 
                                      0.00      1.111E-04   0.00     (cm)
                                    30             9      30

  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.060     0.000     0.000     0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.060     0.000     0.000     0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.060     0.000     0.000     0.000            3
  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.060     0.000     0.000     0.000            4
  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.060     0.000     0.000     0.000            5
  A    1  1.0000     0.000     0.000     0.000     0.000            0.001     0.060     0.000     0.000     0.000            6

     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    24.0961294     m ;  Time  (for ref. rigidity & particle) =   8.037658E-08 s 

************************************************************************************************************************************
     21  Keyword, label(s) :  DRIFT       DRIFT_3               DRIF                                                   IPASS= 1


                              Drift,  length =    92.77038  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  6.081639E-02  1.055912E-02  0.000000E+00  0.000000E+00  2.5023825E+03  8.34710E-02
TRAJ #1 SX, SY, SZ, |S| :  1   -2.179251E-01  -9.759655E-01   0.000000E+00   1.000000E+00

 Cumulative length of optical axis =    25.0238332     m   ;  Time  (for reference rigidity & particle) =   8.347108E-08 s 

************************************************************************************************************************************
     22  Keyword, label(s) :  DRIFT       YI6_B1                MONI                                                   IPASS= 1


                              Drift,  length =     0.00000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  6.081639E-02  1.055912E-02  0.000000E+00  0.000000E+00  2.5023825E+03  8.34710E-02
TRAJ #1 SX, SY, SZ, |S| :  1   -2.179251E-01  -9.759655E-01   0.000000E+00   1.000000E+00

 Cumulative length of optical axis =    25.0238332     m   ;  Time  (for reference rigidity & particle) =   8.347108E-08 s 

************************************************************************************************************************************
     23  Keyword, label(s) :  DRIFT       DRIFT_4               DRIF                                                   IPASS= 1


                              Drift,  length =    33.69448  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  6.117218E-02  1.055912E-02  0.000000E+00  0.000000E+00  2.5360770E+03  8.45950E-02
TRAJ #1 SX, SY, SZ, |S| :  1   -2.179251E-01  -9.759655E-01   0.000000E+00   1.000000E+00

 Cumulative length of optical axis =    25.3607780     m   ;  Time  (for reference rigidity & particle) =   8.459502E-08 s 

************************************************************************************************************************************
     24  Keyword, label(s) :  MULTIPOL    YI6_QF1               QUAD                                                   IPASS= 1


      -----  MULTIPOLE   : 
                Length  of  element  =    144.00000      cm
                Bore  radius      RO =    10.000      cm                              B/RO^IM/Brho (strength)
               B-DIPOLE      =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-QUADRUPOLE  =  4.7886821E+01 kG   (i.e.,   5.6267773E-02 * SCAL)     5.6092448E-02
               B-SEXTUPOLE   =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    851.05235       (Brho_ref =    853712.44    )

                    Integration steps inside entrance|body|exit region : 
                                      0.00      0.500       0.00     (cm)
                                    30           288      30

  A    1  1.0000     0.000     0.000     0.000     0.000          144.000     0.059    -0.000     0.000     0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000          144.000     0.059    -0.000     0.000     0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000          144.000     0.059    -0.000     0.000     0.000            3
  A    1  1.0000     0.000     0.000     0.000     0.000          144.000     0.059    -0.000     0.000     0.000            4
  A    1  1.0000     0.000     0.000     0.000     0.000          144.000     0.059    -0.000     0.000     0.000            5
  A    1  1.0000     0.000     0.000     0.000     0.000          144.000     0.059    -0.000     0.000     0.000            6

     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    26.8007780     m ;  Time  (for ref. rigidity & particle) =   8.939837E-08 s 

************************************************************************************************************************************
     25  Keyword, label(s) :  DRIFT       DRIFT_5               DRIF                                                   IPASS= 1


                              Drift,  length =   118.24280  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  5.458694E-02 -3.850741E-02  0.000000E+00  0.000000E+00  2.7983198E+03  9.33425E-02
TRAJ #1 SX, SY, SZ, |S| :  1   -2.413268E-01  -9.704439E-01   0.000000E+00   1.000000E+00

 Cumulative length of optical axis =    27.9832060     m   ;  Time  (for reference rigidity & particle) =   9.334256E-08 s 

************************************************************************************************************************************
     26  Keyword, label(s) :  MULTIPOL    YI6_TV2               VKIC                                                   IPASS= 1


      -----  MULTIPOLE   : 
                Length  of  element  =   1.00000000E-04  cm
                Bore  radius      RO =    10.000      cm                              B/RO^IM/Brho (strength)
               B-DIPOLE      =  9.1286328E+05 kG   (i.e.,   1.0723800E+03 * SCAL)     1.0692866E+02
               B-QUADRUPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-SEXTUPOLE   =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               DIPOLE      Skew  angle =  1.5708000E+00 rd
               QUADRUPOLE  Skew  angle =  0.0000000E+00 rd
               SEXTUPOLE   Skew  angle =  0.0000000E+00 rd
               OCTUPOLE    Skew  angle =  0.0000000E+00 rd
               DECAPOLE    Skew  angle =  0.0000000E+00 rd
               DODECAPOLE  Skew  angle =  0.0000000E+00 rd
               14-POLE     Skew  angle =  0.0000000E+00 rd
               16-POLE     Skew  angle =  0.0000000E+00 rd
               18-POLE     Skew  angle =  0.0000000E+00 rd
               20-POLE     Skew  angle =  0.0000000E+00 rd

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   1.0000E+01

  ***  Warning : sharp edge model, vertical wedge focusing approximated with first order kick. FINT at entrance =    0.000    

  ***  Warning : sharp edge model,  vertical wedge focusing approximated with first order kick. FINT at exit =    0.000    

                    Field has been * by scaling factor    851.24982       (Brho_ref =    853712.44    )

                    Integration steps inside entrance|body|exit region : 
                                      0.00      2.500E-05   0.00     (cm)
                                    20             4      20

  A    1  1.0000     0.000     0.000     0.000     0.000            0.000     0.055    -0.000    -0.000    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000            0.000     0.055    -0.000    -0.000    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000            0.000     0.055    -0.000    -0.000    -0.000            3
  A    1  1.0000     0.000     0.000     0.000     0.000            0.000     0.055    -0.000    -0.000    -0.000            4
  A    1  1.0000     0.000     0.000     0.000     0.000            0.000     0.055    -0.000    -0.000    -0.000            5
  A    1  1.0000     0.000     0.000     0.000     0.000            0.000     0.055    -0.000    -0.000    -0.000            6

     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    27.9832070     m ;  Time  (for ref. rigidity & particle) =   9.334256E-08 s 

************************************************************************************************************************************
     27  Keyword, label(s) :  DRIFT       DRIFT_6               DRIF                                                   IPASS= 1


                              Drift,  length =    49.42035  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  5.268391E-02 -3.850702E-02 -5.284457E-03 -1.069287E-01  2.8477403E+03  9.49910E-02
TRAJ #1 SX, SY, SZ, |S| :  1   -2.409954E-01  -9.704440E-01   1.263778E-02   1.000000E+00

 Cumulative length of optical axis =    28.4774105     m   ;  Time  (for reference rigidity & particle) =   9.499105E-08 s 

************************************************************************************************************************************
     28  Keyword, label(s) :  MULTIPOL    YI6_QD2               QUAD                                                   IPASS= 1


      -----  MULTIPOLE   : 
                Length  of  element  =    339.16330      cm
                Bore  radius      RO =    10.000      cm                              B/RO^IM/Brho (strength)
               B-DIPOLE      =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-QUADRUPOLE  = -4.7586548E+01 kG   (i.e.,  -5.5889304E-02 * SCAL)    -5.5740722E-02
               B-SEXTUPOLE   =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    851.44285       (Brho_ref =    853712.44    )

                    Integration steps inside entrance|body|exit region : 
                                      0.00      0.500       0.00     (cm)
                                    30           678      30

  A    1  1.0000     0.000     0.000     0.000     0.000          339.163     0.056     0.000    -0.036    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000          339.163     0.056     0.000    -0.036    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000          339.163     0.056     0.000    -0.036    -0.000            3
  A    1  1.0000     0.000     0.000     0.000     0.000          339.163     0.056     0.000    -0.036    -0.000            4
  A    1  1.0000     0.000     0.000     0.000     0.000          339.163     0.056     0.000    -0.036    -0.000            5
  A    1  1.0000     0.000     0.000     0.000     0.000          339.163     0.056     0.000    -0.036    -0.000            6

     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    31.8690435     m ;  Time  (for ref. rigidity & particle) =   1.063044E-07 s 

************************************************************************************************************************************
     29  Keyword, label(s) :  DRIFT       DRIFT_7               DRIF                                                   IPASS= 1


                              Drift,  length =   137.09615  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  6.409244E-02  5.906418E-02 -4.516953E-02 -6.548416E-02  3.3239997E+03  1.10877E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -1.945478E-01  -9.808578E-01   8.315546E-03   1.000000E+00

 Cumulative length of optical axis =    33.2400050     m   ;  Time  (for reference rigidity & particle) =   1.108775E-07 s 

************************************************************************************************************************************
     30  Keyword, label(s) :  MARKER      YI6_QS3               MULT                                                   IPASS= 1


************************************************************************************************************************************
     31  Keyword, label(s) :  DRIFT       DRIFT_8               DRIF                                                   IPASS= 1


                              Drift,  length =    48.97780  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  6.698527E-02  5.906418E-02 -4.837680E-02 -6.548416E-02  3.3729775E+03  1.12511E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -1.945478E-01  -9.808578E-01   8.315546E-03   1.000000E+00

 Cumulative length of optical axis =    33.7297830     m   ;  Time  (for reference rigidity & particle) =   1.125112E-07 s 

************************************************************************************************************************************
     32  Keyword, label(s) :  MULTIPOL    YI6_QF3               QUAD                                                   IPASS= 1


      -----  MULTIPOLE   : 
                Length  of  element  =    210.04840      cm
                Bore  radius      RO =    10.000      cm                              B/RO^IM/Brho (strength)
               B-DIPOLE      =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-QUADRUPOLE  =  4.7906052E+01 kG   (i.e.,   5.6290370E-02 * SCAL)     5.6114974E-02
               B-SEXTUPOLE   =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   5.0000E+00

                    Field has been * by scaling factor    851.05235       (Brho_ref =    853712.44    )

                    Integration steps inside entrance|body|exit region : 
                                      0.00      0.500       0.00     (cm)
                                    30           420      30

  A    1  1.0000     0.000     0.000     0.000     0.000          210.048     0.071    -0.000    -0.069    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000          210.048     0.071    -0.000    -0.069    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000          210.048     0.071    -0.000    -0.069    -0.000            3
  A    1  1.0000     0.000     0.000     0.000     0.000          210.048     0.071    -0.000    -0.069    -0.000            4
  A    1  1.0000     0.000     0.000     0.000     0.000          210.048     0.071    -0.000    -0.069    -0.000            5
  A    1  1.0000     0.000     0.000     0.000     0.000          210.048     0.071    -0.000    -0.069    -0.000            6

     QUASEX - KPOS =  1 :  Change  of  frame  at  exit  of  element.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    35.8302670     m ;  Time  (for ref. rigidity & particle) =   1.195177E-07 s 

************************************************************************************************************************************
     33  Keyword, label(s) :  DRIFT       DRIFT_9               DRIF                                                   IPASS= 1


                              Drift,  length =    59.79680  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  6.933828E-02 -2.383455E-02 -7.678214E-02 -1.331624E-01  3.6428227E+03  1.21512E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -2.338323E-01  -9.721540E-01   1.545838E-02   1.000000E+00

 Cumulative length of optical axis =    36.4282350     m   ;  Time  (for reference rigidity & particle) =   1.215123E-07 s 

************************************************************************************************************************************
     34  Keyword, label(s) :  MULTIPOL    YI6_TH3               HKIC                                                   IPASS= 1


      -----  MULTIPOLE   : 
                Length  of  element  =   1.00000000E-04  cm
                Bore  radius      RO =    10.000      cm                              B/RO^IM/Brho (strength)
               B-DIPOLE      = -0.0000000E+00 kG   (i.e.,  -0.0000000E+00 * SCAL)    -0.0000000E+00
               B-QUADRUPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-SEXTUPOLE   =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-OCTUPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-DECAPOLE    =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-DODECAPOLE  =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-14-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-16-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-18-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00
               B-20-POLE     =  0.0000000E+00 kG   (i.e.,   0.0000000E+00 * SCAL)     0.0000000E+00

               Entrance/exit field models are sharp edge
               FINTE, FINTS, gap :    0.0000E+00   0.0000E+00   0.0000E+00

                    Field has been * by scaling factor    851.24982       (Brho_ref =    853712.44    )

                    Integration steps inside entrance|body|exit region : 
                                      0.00      2.500E-05   0.00     (cm)
                                    20             4      20


                          ++++++++  ZERO  FIELD  ++++++++++
               Element  is  equivalent  to  drift  of  length      0.00010 cm

 Cumulative length of optical axis =    36.4282360     m   ;  Time  (for reference rigidity & particle) =   1.215123E-07 s 

************************************************************************************************************************************
     35  Keyword, label(s) :  DRIFT       DRIFT_10              DRIF                                                   IPASS= 1


                              Drift,  length =    47.14283  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  6.821465E-02 -2.383455E-02 -8.305981E-02 -1.331624E-01  3.6899657E+03  1.23085E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -2.338323E-01  -9.721540E-01   1.545838E-02   1.000000E+00

 Cumulative length of optical axis =    36.8996642     m   ;  Time  (for reference rigidity & particle) =   1.230849E-07 s 

************************************************************************************************************************************
     36  Keyword, label(s) :  DRIFT       YI6_B3                MONI                                                   IPASS= 1


                              Drift,  length =     0.00000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  6.821465E-02 -2.383455E-02 -8.305981E-02 -1.331624E-01  3.6899657E+03  1.23085E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -2.338323E-01  -9.721540E-01   1.545838E-02   1.000000E+00

 Cumulative length of optical axis =    36.8996642     m   ;  Time  (for reference rigidity & particle) =   1.230849E-07 s 

************************************************************************************************************************************
     37  Keyword, label(s) :  DRIFT       DRIFT_11              DRIF                                                   IPASS= 1


                              Drift,  length =   210.23610  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  6.320377E-02 -2.383455E-02 -1.110553E-01 -1.331624E-01  3.9002018E+03  1.30098E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -2.338323E-01  -9.721540E-01   1.545838E-02   1.000000E+00

 Cumulative length of optical axis =    39.0020252     m   ;  Time  (for reference rigidity & particle) =   1.300976E-07 s 

************************************************************************************************************************************
     38  Keyword, label(s) :  MARKER      YI6_KFBH3             TKIC                                                   IPASS= 1


************************************************************************************************************************************
     39  Keyword, label(s) :  DRIFT       DRIFT_12              DRIF                                                   IPASS= 1


                              Drift,  length =  2109.23990  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  1.293098E-02 -2.383455E-02 -3.919267E-01 -1.331624E-01  6.0094417E+03  2.00455E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -2.338323E-01  -9.721540E-01   1.545838E-02   1.000000E+00

 Cumulative length of optical axis =    60.0944242     m   ;  Time  (for reference rigidity & particle) =   2.004548E-07 s 

************************************************************************************************************************************
     40  Keyword, label(s) :  MARKER      rotSTAR_H2V_S                                                                IPASS= 1


************************************************************************************************************************************
     41  Keyword, label(s) :  MARKER      R+L+R+L+_S                                                                   IPASS= 1


************************************************************************************************************************************
     42  Keyword, label(s) :  DRIFT       rotator_adjL                                                                 IPASS= 1


                              Drift,  length =    71.83684  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  1.121878E-02 -2.383455E-02 -4.014927E-01 -1.331624E-01  6.0812785E+03  2.02851E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -2.338323E-01  -9.721540E-01   1.545838E-02   1.000000E+00

 Cumulative length of optical axis =    60.8127926     m   ;  Time  (for reference rigidity & particle) =   2.028510E-07 s 

************************************************************************************************************************************
     43  Keyword, label(s) :  TOSCA       rotatorH2V_out_RH                                                            IPASS= 1


                OPEN FILE zgoubi.plt                                                                      
                FOR PRINTING TRAJECTORIES


     Status of MOD.MOD2 is 15.1 ; NDIM = 3 ; number of field data files used is   1.
     Field map set stored in field array HC(IMAP = 1)
     2-D map. MOD=15.  Single field map, with field coefficient value : 
                 7.485030E-05

     ZroBXY OPTION :  absent from map title. BX and BY values in Z=0 plane left as read.

           New field map(s) opened, cartesian mesh (MOD.le.19) ; 
           name(s) of map data file(s) : 

          'b_rotatorModule_rightHanded.table'

   ----
   Map file number    1 ( of 1 ) (unit # 26 ) :
     b_rotatorModule_rightHanded.table field map,
     FORMAT type : regular,
     field multiplication factor :  7.48502994E-05.

 HEADER  (8 lines) : 
                81          81         361           1                                                                       
       1 X [LENGU]                                                                                                           
       2 Y [LENGU]                                                                                                           
       3 Z [LENGU]                                                                                                           
       4 BX [FLUXU]                                                                                                          
       5 BY [FLUXU]                                                                                                          
       6 BZ [FLUXU]                                                                                                          
       0                                                                                                                     
R_min (cm), DR (cm), DTTA (deg), DZ (cm) :    0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00

  Sbr fmapw/fmapr3 : completed job of reading map.
     Field map scaled by factor  a(1) =   7.485030E-05

     Min/max fields found in map (series) read   :  -1.098599E+05  /   1.098533E+05
       @  X,  Y, Z :   52.0      -4.00       2.90                  /  -52.0      -4.00      -2.90    
     Given normalisation coeffs on field, x, y, z :   1.305761E-01   1.000000E+00   1.000000E+00   1.000000E+00
     this yields min/max normalised fields (kG) : -1.434508E+04                      1.434422E+04
       @  X (cm),  Y (cm), Z (cm) :   52.0      -4.00       2.90   /  -52.0      -4.00      -2.90    

     Length of element,  XL =  3.600000E+02 cm 
                                               from  XI =  -1.800000E+02 cm 
                                               to    XF =   1.800000E+02 cm 

     Nbr of nodes in X = 361;  nbr of nodes in Y =   81
     X-size of mesh =  1.000000E+00 cm ; Y-size =  1.000000E-01 cm
     nber of mesh nodes in Z =   81 ; Step in Z =  0.100000E+00 cm

                     OPTION  DE  CALCUL  :  GRILLE  3-D  A  3*3*3  POINTS , INTERPOLATION  A  L'ORDRE  2

                    Field has been * by scaling factor    263.70126       (Brho_ref =    853712.44    )

                    Integration step :  0.3000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000          180.000    -0.366    -0.000    -0.447    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000    -0.366    -0.000    -0.447    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000    -0.366    -0.000    -0.447    -0.000            3
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000    -0.366    -0.000    -0.447    -0.000            4
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000    -0.366    -0.000    -0.447    -0.000            5
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000    -0.366    -0.000    -0.447    -0.000            6


                CONDITIONS  DE  MAXWELL  (     7206.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                    -1.6411E-16       9.7755E-18        6.2282E-17
                                      5.3393E-16        8.1096E-15
                                     -2.1204E-16        8.1081E-15
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  2 :  Element  is  mis-aligned  wrt.  the  optical  axis.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    64.4127926     m ;  Time  (for ref. rigidity & particle) =   2.148594E-07 s 

************************************************************************************************************************************
     44  Keyword, label(s) :  DRIFT                                                                                    IPASS= 1


                              Drift,  length =   -98.80000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -3.639211E-01 -2.366603E-02 -4.353473E-01 -1.215217E-01  6.3424791E+03  2.11564E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -6.064121E-01  -4.775871E-01  -6.357476E-01   1.000000E+00

 Cumulative length of optical axis =    63.4247926     m   ;  Time  (for reference rigidity & particle) =   2.115638E-07 s 

************************************************************************************************************************************
     45  Keyword, label(s) :  TOSCA       rotatorH2V_in_LH                                                             IPASS= 1


     zgoubi.plt                                                                      
      already open...

     Status of MOD.MOD2 is 15.1 ; NDIM = 3 ; number of field data files used is   1.
     Field map set stored in field array HC(IMAP = 2)
     2-D map. MOD=15.  Single field map, with field coefficient value : 
                 7.485030E-05

     ZroBXY OPTION :  absent from map title. BX and BY values in Z=0 plane left as read.

           New field map(s) opened, cartesian mesh (MOD.le.19) ; 
           name(s) of map data file(s) : 

          'b_rotatorModule_leftHanded.table'

   ----
   Map file number    1 ( of 1 ) (unit # 27 ) :
     b_rotatorModule_leftHanded.table field map,
     FORMAT type : regular,
     field multiplication factor :  7.48502994E-05.

 HEADER  (8 lines) : 
                81          81         361           1                                                                       
       0                                                                                                                     
       6 BZ [FLUXU]                                                                                                          
       5 BY [FLUXU]                                                                                                          
       4 BX [FLUXU]                                                                                                          
       3 Z [LENGU]                                                                                                           
       2 Y [LENGU]                                                                                                           
       1 X [LENGU]                                                                                                           
R_min (cm), DR (cm), DTTA (deg), DZ (cm) :    0.00000000E+00   0.00000000E+00   0.00000000E+00   1.00000000E-01

  Sbr fmapw/fmapr3 : completed job of reading map.
     Field map scaled by factor  a(1) =   7.485030E-05

     Min/max fields found in map (series) read   :  -1.080067E+05  /   1.080003E+05
       @  X,  Y, Z :  -52.0       4.00      -2.90                  /   52.0       4.00       2.90    
     Given normalisation coeffs on field, x, y, z :   1.328165E-01   1.000000E+00   1.000000E+00   1.000000E+00
     this yields min/max normalised fields (kG) : -1.434508E+04                      1.434422E+04
       @  X (cm),  Y (cm), Z (cm) :  -52.0       4.00      -2.90   /   52.0       4.00       2.90    

     Length of element,  XL =  3.600000E+02 cm 
                                               from  XI =  -1.800000E+02 cm 
                                               to    XF =   1.800000E+02 cm 

     Nbr of nodes in X = 361;  nbr of nodes in Y =   81
     X-size of mesh =  1.000000E+00 cm ; Y-size =  1.000000E-01 cm
     nber of mesh nodes in Z =   81 ; Step in Z =  0.100000E+00 cm

                     OPTION  DE  CALCUL  :  GRILLE  3-D  A  3*3*3  POINTS , INTERPOLATION  A  L'ORDRE  2

                    Field has been * by scaling factor    209.70920       (Brho_ref =    853712.44    )

                    Integration step :  0.3000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000          180.000    -0.074    -0.000    -0.477    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000    -0.074    -0.000    -0.477    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000    -0.074    -0.000    -0.477    -0.000            3
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000    -0.074    -0.000    -0.477    -0.000            4
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000    -0.074    -0.000    -0.477    -0.000            5
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000    -0.074    -0.000    -0.477    -0.000            6


                CONDITIONS  DE  MAXWELL  (     7206.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                    -2.7759E-17       1.0543E-18       -7.6647E-17
                                     -5.2556E-16       -9.1532E-15
                                      2.9860E-16       -9.1448E-15
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  2 :  Element  is  mis-aligned  wrt.  the  optical  axis.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    67.0247926     m ;  Time  (for ref. rigidity & particle) =   2.235721E-07 s 

************************************************************************************************************************************
     46  Keyword, label(s) :  DRIFT                                                                                    IPASS= 1


                              Drift,  length =   -75.20000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -7.222296E-02 -2.352724E-02 -4.689586E-01 -1.120841E-01  6.6272795E+03  2.21064E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -7.014899E-01  -6.346672E-01   3.242061E-01   1.000000E+00

 Cumulative length of optical axis =    66.2727926     m   ;  Time  (for reference rigidity & particle) =   2.210637E-07 s 

************************************************************************************************************************************
     47  Keyword, label(s) :  DRIFT                                                                                    IPASS= 1


                              Drift,  length =    35.88920  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -7.306733E-02 -2.352724E-02 -4.729812E-01 -1.120841E-01  6.6631687E+03  2.22261E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -7.014899E-01  -6.346672E-01   3.242061E-01   1.000000E+00

 Cumulative length of optical axis =    66.6316846     m   ;  Time  (for reference rigidity & particle) =   2.222609E-07 s 

************************************************************************************************************************************
     48  Keyword, label(s) :  DRIFT                                                                                    IPASS= 1


                              Drift,  length =   -35.88920  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -7.222296E-02 -2.352724E-02 -4.689586E-01 -1.120841E-01  6.6272795E+03  2.21064E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -7.014899E-01  -6.346672E-01   3.242061E-01   1.000000E+00

 Cumulative length of optical axis =    66.2727926     m   ;  Time  (for reference rigidity & particle) =   2.210637E-07 s 

************************************************************************************************************************************
     49  Keyword, label(s) :  TOSCA       rotatorH2V_in_RH                                                             IPASS= 1


     zgoubi.plt                                                                      
      already open...

     Status of MOD.MOD2 is 15.1 ; NDIM = 3 ; number of field data files used is   1.
     Field map set stored in field array HC(IMAP = 1)
     2-D map. MOD=15.  Single field map, with field coefficient value : 
                 7.485030E-05

     ZroBXY OPTION :  absent from map title. BX and BY values in Z=0 plane left as read.

     No  map  file  opened :  was  already  stored.
     Skip  reading  field  map  file :           b_rotatorModule_rightHanded.table
     Restored mesh coordinates for field map #IMAP=   1,  name : b_rotatorModule_rightHanded.table
     Field map scaled by factor  a(1) =   7.485030E-05

     Min/max fields found in map (series) read   :  -1.080067E+05  /   1.080003E+05
       @  X,  Y, Z :   52.0      -4.00       2.90                  /  -52.0      -4.00      -2.90    
     Given normalisation coeffs on field, x, y, z :   1.328165E-01   1.000000E+00   1.000000E+00   1.000000E+00
     this yields min/max normalised fields (kG) : -1.434508E+04                      1.434422E+04
       @  X (cm),  Y (cm), Z (cm) :   52.0      -4.00       2.90   /  -52.0      -4.00      -2.90    

     Length of element,  XL =  3.600000E+02 cm 
                                               from  XI =  -1.800000E+02 cm 
                                               to    XF =   1.800000E+02 cm 

     Nbr of nodes in X = 361;  nbr of nodes in Y =   81
     X-size of mesh =  1.000000E+00 cm ; Y-size =  1.000000E-01 cm
     nber of mesh nodes in Z =   81 ; Step in Z =  0.100000E+00 cm

                     OPTION  DE  CALCUL  :  GRILLE  3-D  A  3*3*3  POINTS , INTERPOLATION  A  L'ORDRE  2

                    Field has been * by scaling factor    209.70920       (Brho_ref =    853712.44    )

                    Integration step :  0.3000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000          180.000    -0.379    -0.000    -0.508    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000    -0.379    -0.000    -0.508    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000    -0.379    -0.000    -0.508    -0.000            3
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000    -0.379    -0.000    -0.508    -0.000            4
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000    -0.379    -0.000    -0.508    -0.000            5
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000    -0.379    -0.000    -0.508    -0.000            6


                CONDITIONS  DE  MAXWELL  (     7206.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                    -1.2921E-16       6.2506E-18        5.0020E-17
                                      4.3095E-16        6.5601E-15
                                     -1.7716E-16        6.5585E-15
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  2 :  Element  is  mis-aligned  wrt.  the  optical  axis.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    69.8727926     m ;  Time  (for ref. rigidity & particle) =   2.330721E-07 s 

************************************************************************************************************************************
     50  Keyword, label(s) :  DRIFT                                                                                    IPASS= 1


                              Drift,  length =   -98.80000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -3.767841E-01 -2.339039E-02 -4.974800E-01 -1.026556E-01  6.8884798E+03  2.29776E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -6.020264E-01  -7.984756E-01   9.666880E-04   1.000000E+00

 Cumulative length of optical axis =    68.8847926     m   ;  Time  (for reference rigidity & particle) =   2.297765E-07 s 

************************************************************************************************************************************
     51  Keyword, label(s) :  TOSCA       rotatorH2V_out_LH                                                            IPASS= 1


     zgoubi.plt                                                                      
      already open...

     Status of MOD.MOD2 is 15.1 ; NDIM = 3 ; number of field data files used is   1.
     Field map set stored in field array HC(IMAP = 2)
     2-D map. MOD=15.  Single field map, with field coefficient value : 
                 7.485030E-05

     ZroBXY OPTION :  absent from map title. BX and BY values in Z=0 plane left as read.

     No  map  file  opened :  was  already  stored.
     Skip  reading  field  map  file :           b_rotatorModule_leftHanded.table
     Restored mesh coordinates for field map #IMAP=   2,  name : b_rotatorModule_leftHanded.table
     Field map scaled by factor  a(1) =   7.485030E-05

     Min/max fields found in map (series) read   :  -1.098599E+05  /   1.098533E+05
       @  X,  Y, Z :  -52.0       4.00      -2.90                  /   52.0       4.00       2.90    
     Given normalisation coeffs on field, x, y, z :   1.305761E-01   1.000000E+00   1.000000E+00   1.000000E+00
     this yields min/max normalised fields (kG) : -1.434508E+04                      1.434422E+04
       @  X (cm),  Y (cm), Z (cm) :  -52.0       4.00      -2.90   /   52.0       4.00       2.90    

     Length of element,  XL =  3.600000E+02 cm 
                                               from  XI =  -1.800000E+02 cm 
                                               to    XF =   1.800000E+02 cm 

     Nbr of nodes in X = 361;  nbr of nodes in Y =   81
     X-size of mesh =  1.000000E+00 cm ; Y-size =  1.000000E-01 cm
     nber of mesh nodes in Z =   81 ; Step in Z =  0.100000E+00 cm

                     OPTION  DE  CALCUL  :  GRILLE  3-D  A  3*3*3  POINTS , INTERPOLATION  A  L'ORDRE  2

                    Field has been * by scaling factor    263.70126       (Brho_ref =    853712.44    )

                    Integration step :  0.3000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000          180.000    -0.016    -0.000    -0.532    -0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000    -0.016    -0.000    -0.532    -0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000    -0.016    -0.000    -0.532    -0.000            3
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000    -0.016    -0.000    -0.532    -0.000            4
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000    -0.016    -0.000    -0.532    -0.000            5
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000    -0.016    -0.000    -0.532    -0.000            6


                CONDITIONS  DE  MAXWELL  (     7206.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                    -8.6313E-17       6.4239E-18       -8.1208E-17
                                     -6.3692E-16       -1.1316E-14
                                      3.6332E-16       -1.1306E-14
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  2 :  Element  is  mis-aligned  wrt.  the  optical  axis.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    72.4847926     m ;  Time  (for ref. rigidity & particle) =   2.417849E-07 s 

************************************************************************************************************************************
     52  Keyword, label(s) :  DRIFT       rotator_adjL                                                                 IPASS= 1


                              Drift,  length =    71.83684  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -1.790267E-02 -2.319004E-02 -5.388599E-01 -9.098887E-02  7.3203172E+03  2.44181E-01
TRAJ #1 SX, SY, SZ, |S| :  1   -3.878918E-04  -4.462844E-04   9.999998E-01   1.000000E+00

 Cumulative length of optical axis =    73.2031610     m   ;  Time  (for reference rigidity & particle) =   2.441811E-07 s 

************************************************************************************************************************************
     53  Keyword, label(s) :  MARKER      R+L+R+L+_E                                                                   IPASS= 1


************************************************************************************************************************************
     54  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #     53)
                                                  6 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o   1   1.0000     0.000     0.000     0.000     0.000      0.0000   0.0000   -0.018   -0.023   -0.539   -0.091   7.320317E+03     1
               Time of flight (mus) :  0.24418113     mass (MeV/c2) :   938.272    
o   1   1.0000     0.000     0.000     0.000     0.000      0.0000   0.0000   -0.018   -0.023   -0.539   -0.091   7.320317E+03     2
               Time of flight (mus) :  0.24418113     mass (MeV/c2) :   938.272    
o   1   1.0000     0.000     0.000     0.000     0.000      0.0000   0.0000   -0.018   -0.023   -0.539   -0.091   7.320317E+03     3
               Time of flight (mus) :  0.24418113     mass (MeV/c2) :   938.272    
o   1   1.0000     0.000     0.000     0.000     0.000      0.0000   0.0000   -0.018   -0.023   -0.539   -0.091   7.320317E+03     4
               Time of flight (mus) :  0.24418113     mass (MeV/c2) :   938.272    
o   1   1.0000     0.000     0.000     0.000     0.000      0.0000   0.0000   -0.018   -0.023   -0.539   -0.091   7.320317E+03     5
               Time of flight (mus) :  0.24418113     mass (MeV/c2) :   938.272    
o   1   1.0000     0.000     0.000     0.000     0.000      0.0000   0.0000   -0.018   -0.023   -0.539   -0.091   7.320317E+03     6
               Time of flight (mus) :  0.24418113     mass (MeV/c2) :   938.272    


---------------  Concentration ellipses : 
   surface/pi              alpha        beta         <X>            <XP>           numb. of prtcls   ratio      space      pass# 
                                                                                   in ellips,  out 
   0.0000E+00 [m.rad]      0.0000E+00   1.0000E+00  -1.790267E-04  -2.319004E-05        6        0    0.000      (Y,T)         1
   0.0000E+00 [m.rad]      0.0000E+00   1.0000E+00  -5.388599E-03  -9.098887E-05        6        0    0.000      (Z,P)         1
   0.0000E+00 [mu_s.MeV]   0.0000E+00   1.0000E+00   2.441811E-01   2.550000E+05        6        6    1.000      (t,K)         1

(Y,T)  space (units : m, rad   ) :  
      sigma_Y = sqrt(surface/pi * beta) =   0.000000E+00
      sigma_T = sqrt(surface/pi * (1+alpha^2)/beta) =   0.000000E+00

(Z,P)  space (units : m, rad   ) :  
      sigma_Z = sqrt(surface/pi * beta) =   0.000000E+00
      sigma_P = sqrt(surface/pi * (1+alpha^2)/beta) =   0.000000E+00

(t,K)  space (units : mu_s, MeV) :  
      sigma_t = sqrt(surface/pi * beta) =   0.000000E+00
      sigma_K = sqrt(surface/pi * (1+alpha^2)/beta) =   0.000000E+00


  Beam  sigma  matrix  and  determinants : 

   7.346840E-40   9.183550E-41   0.000000E+00  -3.673420E-40
   9.183550E-41   1.147944E-41   0.000000E+00  -4.591775E-41
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  -3.673420E-40  -4.591775E-41   0.000000E+00   1.836710E-40

      sqrt(det_Y), sqrt(det_Z) :     0.000000E+00    0.000000E+00    (Note :  sqrt(determinant) = ellipse surface / pi)
      normalized (*beta*gamma) :     0.000000E+00    0.000000E+00

************************************************************************************************************************************
     55  Keyword, label(s) :  SPNPRT      MATRIX                                                                       IPASS= 1

                          -- 2  GROUPS  OF  MOMENTA  FOLLOW   --

                    ----------------------------------------------------------------------------------
                    Momentum  group  #1 (D=   1.000000E+00;  particles  1  to 3 ; 


                Spin  components  of  the      3  particles,  spin  angles :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   |Si,Sf|   (Z,Sf_yz) (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_yz : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000    -0.000388 -0.000446  1.000000  1.000000    272.7762  -90.022   45.000    0.034    1
 o  1  0.000000  1.000000  0.000000  1.000000    -0.985009 -0.172502 -0.000459  1.000000    272.7762  -99.933   90.152   90.026    2
 o  1  0.000000  0.000000  1.000000  1.000000     0.172502 -0.985009 -0.000373  1.000000    272.7762  -90.021   90.022   90.021    3


                  Spin transfer matrix, momentum group # 1 :

         -3.878918E-04   -0.985009        0.172502    
         -4.462844E-04   -0.172502       -0.985009    
           1.00000       -4.590619E-04   -3.726822E-04

     Determinant =     1.0000000000
     Trace =    -0.1732624077;  spin precession acos((trace-1)/2) =   125.9183108638 deg
     Precession axis :  ( 0.6079, -0.5109,  0.6079)  ->  angle to (X,Y) plane, to X axis :    37.4352,    52.5653 deg
     Spin precession/2pi (or Qs, fractional) :    3.4977E-01

                    ----------------------------------------------------------------------------------
                    Momentum  group  #2 (D=   1.000000E+00;  particles  4  to 6 ; 


                Spin  components  of  the      3  particles,  spin  angles :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   |Si,Sf|   (Z,Sf_yz) (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_yz : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000    -0.000388 -0.000446  1.000000  1.000000    272.7762  -90.022   45.000    0.034    4
 o  1  0.000000  1.000000  0.000000  1.000000    -0.985009 -0.172502 -0.000459  1.000000    272.7762  -99.933   90.152   90.026    5
 o  1  0.000000  0.000000  1.000000  1.000000     0.172502 -0.985009 -0.000373  1.000000    272.7762  -90.021   90.022   90.021    6


                  Spin transfer matrix, momentum group # 2 :

         -3.878918E-04   -0.985009        0.172502    
         -4.462844E-04   -0.172502       -0.985009    
           1.00000       -4.590619E-04   -3.726822E-04

     Determinant =     1.0000000000
     Trace =    -0.1732624077;  spin precession acos((trace-1)/2) =   125.9183108638 deg
     Precession axis :  ( 0.6079, -0.5109,  0.6079)  ->  angle to (X,Y) plane, to X axis :    37.4352,    52.5653 deg
     Spin precession/2pi (or Qs, fractional) :    3.4977E-01

************************************************************************************************************************************
     56  Keyword, label(s) :  END                                                                                      IPASS= 1

                             6 particles have been launched
                     Made  it  to  the  end :      6

************************************************************************************************************************************
 Pgm zgoubi : Execution ended normally, upon keyword END or FIN
   
 File in:   zgoubi.FIT.out.dat
 File out:  zgoubi.res

  Zgoubi, author's Revision: 1670.
  Job  started  on  12-05-2022,  at  10:19:06 
  JOB  ENDED  ON    12-05-2022,  AT  10:19:07 

   CPU time, total :    0.90225900000000003     
