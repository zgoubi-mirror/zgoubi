R+L+R+L+ rotator:
! IP to arc, H to V spin, along Yell 7 o'clock CW, or Blue 7 o'clock.
'OBJET'                                                                                                      1
851.249816894531 * 1.d3
2
4 1
0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00  1.00000000E+00 'o'
0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00  1.00000000E+00 'o'
0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00  1.00000000E+00 'o'
0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00  1.00000000E+00 'o'
1 1 1 1
'PARTICUL'                                                                                                   2
PROTON
'SPNTRK'                                                                                                     3
4
1.00000000E+00  0.00000000E+00  0.00000000E+00
0.00000000E+00  1.00000000E+00  0.00000000E+00
0.00000000E+00  0.00000000E+00  1.00000000E+00
1.00000000E+00  0.00000000E+00  0.00000000E+00
 
'SCALING'                                                                                                    4
1  2
TOSCA  rotatorH2V_o*   ! goes horiz to vert
-1
2.7743861062E+02
1.
TOSCA  rotatorH2V_i*
-1
3.7036876962E+02
1.
 
'MARKER'  R+L+R+L+_S                                                                                         5
 
'DRIFT' rotator_adjL                                                                                         6
71.836840
 
'TOSCA'   rotatorH2V_out_RH                                                                                  7
0  0020  ! .plt
1.00000000E+00  1.00000000E+00  1.00000000E+00  1.00000000E+00
HEADER_8   RHIC_helix
361 81 81  15.1  0.748502994012e-4     ! = 1/1.3360e4
b_rotatorModule_rightHanded.table
0 0 0 0
2
.3
2   0.00000000E+00  0.00000000E+00  0.00000000E+00
 
'DRIFT'                                                                                                      8
-98.800000
 
'TOSCA'   rotatorH2V_in_LH                                                                                   9
0  0020  ! .plt
1.00000000E+00  1.00000000E+00  1.00000000E+00  1.00000000E+00
HEADER_8   RHIC_helix
361 81 81  15.1  0.748502994012e-4     ! = 1/1.3360e4
b_rotatorModule_leftHanded.table
0 0 0 0
2
.3
2   0.00000000E+00  0.00000000E+00  0.00000000E+00
 
'DRIFT'                                                                                                     10
-75.200000
'DRIFT'                                                                                                     11
35.889200
'DRIFT'                                                                                                     12
-35.889200
 
'TOSCA'  rotatorH2V_in_RH                                                                                   13
0  0020  ! .plt
1.00000000E+00  1.00000000E+00  1.00000000E+00  1.00000000E+00
HEADER_8   RHIC_helix
361 81 81  15.1  0.748502994012e-4     ! = 1/1.3360e4
b_rotatorModule_rightHanded.table
0 0 0 0
2
.3
2   0.00000000E+00  0.00000000E+00  0.00000000E+00
 
'DRIFT'                                                                                                     14
-98.800000
 
'TOSCA'   rotatorH2V_out_LH                                                                                 15
0  0020  ! .plt
1.00000000E+00  1.00000000E+00  1.00000000E+00  1.00000000E+00
HEADER_8   RHIC_helix
361 81 81  15.1  0.748502994012e-4     ! = 1/1.3360e4
b_rotatorModule_leftHanded.table
0 0 0 0
2
.3
2   0.00000000E+00  0.00000000E+00  0.00000000E+00
 
'DRIFT' rotator_adjL                                                                                        16
71.836840
 
'MARKER'  R+L+R+L+_E                                                                                        17
 
! 'FIT'                                                                                                       18
! 2
! 4 4 0 2.
! 4 8 0 2.
! 4  1.6e-4
! 10 4 1 #End 0. 10. 0
! 10 4 2 #End 0. 10. 0
! 10 4 3 #End 1. 1. 0
! 10 4 4 #End 1. .1 0
 
'FAISCEAU'                                                                                                  18
'SPNPRT' MATRIX                                                                                             19
 
'END'

************************************************************************************************************************************
      1  Keyword, label(s) :  OBJET                                                                                    IPASS= 1

                          MAGNETIC  RIGIDITY =     851249.817 kG*cm

                                         TRAJECTOIRY SETTING UP

                              OBJET  (2)  BUILT  UP  FROM       4 POINTS 



************************************************************************************************************************************
      2  Keyword, label(s) :  PARTICUL                                                                                 IPASS= 1

     Particle  properties :
     PROTON
                     Mass          =    938.272        MeV/c2
                     Charge        =   1.602176E-19    C     
                     G  factor     =    1.79285              
                     COM life-time =   1.000000E+99    s     

              Reference  data :
                    mag. rigidity (kG.cm)   :   851249.82      =p/q, such that dev.=B*L/rigidity
                    mass (MeV/c2)           :   938.27208    
                    momentum (MeV/c)        :   255198.27    
                    energy, total (MeV)     :   255200.00    
                    energy, kinetic (MeV)   :   254261.73    
                    beta = v/c              :  0.9999932412    
                    gamma                   :   271.9893354    
                    beta*gamma              :   271.9874971    
                    G*gamma                 :   487.6353592    
                    m / G                   :   523.3418681    
                    electric rigidity (MeV) :   255196.5502    =T[eV]*(gamma+1)/gamma, such that dev.=E*L/rigidity
  
 I, AMQ(1,I), AMQ(2,I)/QE, P/Pref, v/c, time, s :
  
     1   9.38272081E+02  1.00000000E+00  1.00000000E+00  9.99993241E-01  0.00000000E+00  0.00000000E+00
     2   9.38272081E+02  1.00000000E+00  1.00000000E+00  9.99993241E-01  0.00000000E+00  0.00000000E+00
     3   9.38272081E+02  1.00000000E+00  1.00000000E+00  9.99993241E-01  0.00000000E+00  0.00000000E+00
     4   9.38272081E+02  1.00000000E+00  1.00000000E+00  9.99993241E-01  0.00000000E+00  0.00000000E+00

************************************************************************************************************************************
      3  Keyword, label(s) :  SPNTRK                                                                                   IPASS= 1


                Spin  tracking  requested.


                          Particle  mass          =    938.2721     MeV/c2
                          Gyromagnetic  factor  G =    1.792847    

                          Initial spin conditions type  4 :
                              All spins entered particle by particle
                              Particles # 1 to 4 can be subjected to spin matching using FIT procedure

                          PARAMETRES  DYNAMIQUES  DE  REFERENCE :
                               BORO      =     851249.82     kG*cm
                               beta      =    0.99999324    
                               gamma     =     271.98934    
                               G*gamma   =     487.63536    
                               M / G     =     523.3418681    


                          POLARISATION  INITIALE  MOYENNE  DU  FAISCEAU  DE        4  PARTICULES :
                               <SX> =     0.816497
                               <SY> =     0.408248
                               <SZ> =     0.408248
                               <S>  =     1.000000

************************************************************************************************************************************
      4  Keyword, label(s) :  SCALING                                                                                  IPASS= 1


               PRINT option is OFF.

               Scaling  request  on  2  families  of  optical  elements :
                                                      

     Family number   1
          Element [/label(s) ( 1)] to be scaled :          TOSCA
                                                           /rotatorH2V_o*
               Scaling of fields follows increase of rigidity taken from CAVITE, starting scaling value    2.77438611E+02

     Family number   2
          Element [/label(s) ( 1)] to be scaled :          TOSCA
                                                           /rotatorH2V_i*
               Scaling of fields follows increase of rigidity taken from CAVITE, starting scaling value    3.70368770E+02

************************************************************************************************************************************
      5  Keyword, label(s) :  MARKER      R+L+R+L+_S                                                                   IPASS= 1


************************************************************************************************************************************
      6  Keyword, label(s) :  DRIFT       rotator_adjL                                                                 IPASS= 1


                              Drift,  length =    71.83684  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  0.000000E+00  7.1836840E+01  2.39624E-03
TRAJ #1 SX, SY, SZ, |S| :  1    1.000000E+00   0.000000E+00   0.000000E+00   1.000000E+00

 Cumulative length of optical axis =   0.718368400     m   ;  Time  (for reference rigidity & particle) =   2.396235E-09 s 

************************************************************************************************************************************
      7  Keyword, label(s) :  TOSCA       rotatorH2V_out_RH                                                            IPASS= 1


                OPEN FILE zgoubi.plt                                                                      
                FOR PRINTING TRAJECTORIES


     Status of MOD.MOD2 is 15.1 ; NDIM = 3 ; number of field data files used is   1.
     Field map set stored in field array HC(IMAP = 1)
     2-D map. MOD=15.  Single field map, with field coefficient value : 
                 7.485030E-05

     ZroBXY OPTION :  absent from map title. BX and BY values in Z=0 plane left as read.

           New field map(s) opened, cartesian mesh (MOD.le.19) ; 
           name(s) of map data file(s) : 

          'b_rotatorModule_rightHanded.table'

   ----
   Map file number    1 ( of 1 ) (unit # 25 ) :
     b_rotatorModule_rightHanded.table field map,
     FORMAT type : regular,
     field multiplication factor :  7.48502994E-05.

 HEADER  (8 lines) : 
                81          81         361           1                                                                       
       1 X [LENGU]                                                                                                           
       2 Y [LENGU]                                                                                                           
       3 Z [LENGU]                                                                                                           
       4 BX [FLUXU]                                                                                                          
       5 BY [FLUXU]                                                                                                          
       6 BZ [FLUXU]                                                                                                          
       0                                                                                                                     
R_min (cm), DR (cm), DTTA (deg), DZ (cm) :    0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00

  Sbr fmapw/fmapr3 : completed job of reading map.
     Field map scaled by factor  a(1) =   7.485030E-05

     Min/max fields found in map (series) read   :  -1.107021E+05  /   1.106955E+05
       @  X,  Y, Z :   52.0      -4.00       2.90                  /  -52.0      -4.00      -2.90    
     Given normalisation coeffs on field, x, y, z :   1.295827E-01   1.000000E+00   1.000000E+00   1.000000E+00
     this yields min/max normalised fields (kG) : -1.434508E+04                      1.434422E+04
       @  X (cm),  Y (cm), Z (cm) :   52.0      -4.00       2.90   /  -52.0      -4.00      -2.90    

     Length of element,  XL =  3.600000E+02 cm 
                                               from  XI =  -1.800000E+02 cm 
                                               to    XF =   1.800000E+02 cm 

     Nbr of nodes in X = 361;  nbr of nodes in Y =   81
     X-size of mesh =  1.000000E+00 cm ; Y-size =  1.000000E-01 cm
     nber of mesh nodes in Z =   81 ; Step in Z =  0.100000E+00 cm

                     OPTION  DE  CALCUL  :  GRILLE  3-D  A  3*3*3  POINTS , INTERPOLATION  A  L'ORDRE  2

                    Field has been * by scaling factor    277.43861       (Brho_ref =    851249.82    )

                    Integration step :  0.3000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000          180.000    -0.386     0.000     0.002     0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000    -0.386     0.000     0.002     0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000    -0.386     0.000     0.002     0.000            3
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000    -0.386     0.000     0.002     0.000            4


                CONDITIONS  DE  MAXWELL  (     4804.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                    -4.1379E-16       2.7199E-17        1.3498E-16
                                      9.0973E-16        1.2734E-14
                                     -3.3304E-16        1.2731E-14
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  2 :  Element  is  mis-aligned  wrt.  the  optical  axis.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    4.31836840     m ;  Time  (for ref. rigidity & particle) =   1.440462E-08 s 

************************************************************************************************************************************
      8  Keyword, label(s) :  DRIFT                                                                                    IPASS= 1


                              Drift,  length =   -98.80000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -3.862843E-01  6.707277E-05  9.699020E-04  1.220833E-02  3.3303746E+02  1.11090E-02
TRAJ #1 SX, SY, SZ, |S| :  1    5.698206E-01   5.464788E-01  -6.137307E-01   1.000000E+00

 Cumulative length of optical axis =    3.33036840     m   ;  Time  (for reference rigidity & particle) =   1.110899E-08 s 

************************************************************************************************************************************
      9  Keyword, label(s) :  TOSCA       rotatorH2V_in_LH                                                             IPASS= 1


     zgoubi.plt                                                                      
      already open...

     Status of MOD.MOD2 is 15.1 ; NDIM = 3 ; number of field data files used is   1.
     Field map set stored in field array HC(IMAP = 2)
     2-D map. MOD=15.  Single field map, with field coefficient value : 
                 7.485030E-05

     ZroBXY OPTION :  absent from map title. BX and BY values in Z=0 plane left as read.

           New field map(s) opened, cartesian mesh (MOD.le.19) ; 
           name(s) of map data file(s) : 

          'b_rotatorModule_leftHanded.table'

   ----
   Map file number    1 ( of 1 ) (unit # 26 ) :
     b_rotatorModule_leftHanded.table field map,
     FORMAT type : regular,
     field multiplication factor :  7.48502994E-05.

 HEADER  (8 lines) : 
                81          81         361           1                                                                       
       0                                                                                                                     
       6 BZ [FLUXU]                                                                                                          
       5 BY [FLUXU]                                                                                                          
       4 BX [FLUXU]                                                                                                          
       3 Z [LENGU]                                                                                                           
       2 Y [LENGU]                                                                                                           
       1 X [LENGU]                                                                                                           
R_min (cm), DR (cm), DTTA (deg), DZ (cm) :    0.00000000E+00   0.00000000E+00   0.00000000E+00   1.00000000E-01

  Sbr fmapw/fmapr3 : completed job of reading map.
     Field map scaled by factor  a(1) =   7.485030E-05

     Min/max fields found in map (series) read   :  -1.170518E+05  /   1.170448E+05
       @  X,  Y, Z :  -52.0       4.00      -2.90                  /   52.0       4.00       2.90    
     Given normalisation coeffs on field, x, y, z :   1.225532E-01   1.000000E+00   1.000000E+00   1.000000E+00
     this yields min/max normalised fields (kG) : -1.434508E+04                      1.434422E+04
       @  X (cm),  Y (cm), Z (cm) :  -52.0       4.00      -2.90   /   52.0       4.00       2.90    

     Length of element,  XL =  3.600000E+02 cm 
                                               from  XI =  -1.800000E+02 cm 
                                               to    XF =   1.800000E+02 cm 

     Nbr of nodes in X = 361;  nbr of nodes in Y =   81
     X-size of mesh =  1.000000E+00 cm ; Y-size =  1.000000E-01 cm
     nber of mesh nodes in Z =   81 ; Step in Z =  0.100000E+00 cm

                     OPTION  DE  CALCUL  :  GRILLE  3-D  A  3*3*3  POINTS , INTERPOLATION  A  L'ORDRE  2

                    Field has been * by scaling factor    370.36877       (Brho_ref =    851249.82    )

                    Integration step :  0.3000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000          180.000     0.101     0.000     0.008     0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000     0.101     0.000     0.008     0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000     0.101     0.000     0.008     0.000            3
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000     0.101     0.000     0.008     0.000            4


                CONDITIONS  DE  MAXWELL  (     4804.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                     6.0339E-17      -4.1540E-19       -6.0620E-17
                                     -1.2877E-15       -2.2444E-14
                                      7.4056E-16       -2.2425E-14
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  2 :  Element  is  mis-aligned  wrt.  the  optical  axis.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    6.93036840     m ;  Time  (for ref. rigidity & particle) =   2.311738E-08 s 

************************************************************************************************************************************
     10  Keyword, label(s) :  DRIFT                                                                                    IPASS= 1


                              Drift,  length =   -75.20000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  1.014480E-01  1.510616E-04  6.088649E-03  2.761295E-02  6.1783845E+02  2.06090E-02
TRAJ #1 SX, SY, SZ, |S| :  1   -6.929084E-01  -7.177286E-01   6.887390E-02   1.000000E+00

 Cumulative length of optical axis =    6.17836840     m   ;  Time  (for reference rigidity & particle) =   2.060896E-08 s 

************************************************************************************************************************************
     11  Keyword, label(s) :  DRIFT                                                                                    IPASS= 1


                              Drift,  length =    35.88920  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  1.014534E-01  1.510616E-04  7.079656E-03  2.761295E-02  6.5372765E+02  2.18062E-02
TRAJ #1 SX, SY, SZ, |S| :  1   -6.929084E-01  -7.177286E-01   6.887390E-02   1.000000E+00

 Cumulative length of optical axis =    6.53726040     m   ;  Time  (for reference rigidity & particle) =   2.180610E-08 s 

************************************************************************************************************************************
     12  Keyword, label(s) :  DRIFT                                                                                    IPASS= 1


                              Drift,  length =   -35.88920  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  1.014480E-01  1.510616E-04  6.088649E-03  2.761295E-02  6.1783845E+02  2.06090E-02
TRAJ #1 SX, SY, SZ, |S| :  1   -6.929084E-01  -7.177286E-01   6.887390E-02   1.000000E+00

 Cumulative length of optical axis =    6.17836840     m   ;  Time  (for reference rigidity & particle) =   2.060896E-08 s 

************************************************************************************************************************************
     13  Keyword, label(s) :  TOSCA       rotatorH2V_in_RH                                                             IPASS= 1


     zgoubi.plt                                                                      
      already open...

     Status of MOD.MOD2 is 15.1 ; NDIM = 3 ; number of field data files used is   1.
     Field map set stored in field array HC(IMAP = 1)
     2-D map. MOD=15.  Single field map, with field coefficient value : 
                 7.485030E-05

     ZroBXY OPTION :  absent from map title. BX and BY values in Z=0 plane left as read.

     No  map  file  opened :  was  already  stored.
     Skip  reading  field  map  file :           b_rotatorModule_rightHanded.table
     Restored mesh coordinates for field map #IMAP=   1,  name : b_rotatorModule_rightHanded.table
     Field map scaled by factor  a(1) =   7.485030E-05

     Min/max fields found in map (series) read   :  -1.170518E+05  /   1.170448E+05
       @  X,  Y, Z :   52.0      -4.00       2.90                  /  -52.0      -4.00      -2.90    
     Given normalisation coeffs on field, x, y, z :   1.225532E-01   1.000000E+00   1.000000E+00   1.000000E+00
     this yields min/max normalised fields (kG) : -1.434508E+04                      1.434422E+04
       @  X (cm),  Y (cm), Z (cm) :   52.0      -4.00       2.90   /  -52.0      -4.00      -2.90    

     Length of element,  XL =  3.600000E+02 cm 
                                               from  XI =  -1.800000E+02 cm 
                                               to    XF =   1.800000E+02 cm 

     Nbr of nodes in X = 361;  nbr of nodes in Y =   81
     X-size of mesh =  1.000000E+00 cm ; Y-size =  1.000000E-01 cm
     nber of mesh nodes in Z =   81 ; Step in Z =  0.100000E+00 cm

                     OPTION  DE  CALCUL  :  GRILLE  3-D  A  3*3*3  POINTS , INTERPOLATION  A  L'ORDRE  2

                    Field has been * by scaling factor    370.36877       (Brho_ref =    851249.82    )

                    Integration step :  0.3000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000          180.000    -0.386     0.000     0.019     0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000    -0.386     0.000     0.019     0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000    -0.386     0.000     0.019     0.000            3
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000    -0.386     0.000     0.019     0.000            4


                CONDITIONS  DE  MAXWELL  (     4804.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                    -5.2931E-16       3.4339E-17        1.7198E-16
                                      1.1486E-15        1.6077E-14
                                     -4.2047E-16        1.6073E-14
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  2 :  Element  is  mis-aligned  wrt.  the  optical  axis.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    9.77836840     m ;  Time  (for ref. rigidity & particle) =   3.261735E-08 s 

************************************************************************************************************************************
     14  Keyword, label(s) :  DRIFT                                                                                    IPASS= 1


                              Drift,  length =   -98.80000  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00 -3.862114E-01  2.398364E-04  1.452373E-02  4.301505E-02  8.7903944E+02  2.93218E-02
TRAJ #1 SX, SY, SZ, |S| :  1   -6.890992E-01  -7.215199E-01  -6.746400E-02   1.000000E+00

 Cumulative length of optical axis =    8.79036840     m   ;  Time  (for reference rigidity & particle) =   2.932171E-08 s 

************************************************************************************************************************************
     15  Keyword, label(s) :  TOSCA       rotatorH2V_out_LH                                                            IPASS= 1


     zgoubi.plt                                                                      
      already open...

     Status of MOD.MOD2 is 15.1 ; NDIM = 3 ; number of field data files used is   1.
     Field map set stored in field array HC(IMAP = 2)
     2-D map. MOD=15.  Single field map, with field coefficient value : 
                 7.485030E-05

     ZroBXY OPTION :  absent from map title. BX and BY values in Z=0 plane left as read.

     No  map  file  opened :  was  already  stored.
     Skip  reading  field  map  file :           b_rotatorModule_leftHanded.table
     Restored mesh coordinates for field map #IMAP=   2,  name : b_rotatorModule_leftHanded.table
     Field map scaled by factor  a(1) =   7.485030E-05

     Min/max fields found in map (series) read   :  -1.107021E+05  /   1.106955E+05
       @  X,  Y, Z :  -52.0       4.00      -2.90                  /   52.0       4.00       2.90    
     Given normalisation coeffs on field, x, y, z :   1.295827E-01   1.000000E+00   1.000000E+00   1.000000E+00
     this yields min/max normalised fields (kG) : -1.434508E+04                      1.434422E+04
       @  X (cm),  Y (cm), Z (cm) :  -52.0       4.00      -2.90   /   52.0       4.00       2.90    

     Length of element,  XL =  3.600000E+02 cm 
                                               from  XI =  -1.800000E+02 cm 
                                               to    XF =   1.800000E+02 cm 

     Nbr of nodes in X = 361;  nbr of nodes in Y =   81
     X-size of mesh =  1.000000E+00 cm ; Y-size =  1.000000E-01 cm
     nber of mesh nodes in Z =   81 ; Step in Z =  0.100000E+00 cm

                     OPTION  DE  CALCUL  :  GRILLE  3-D  A  3*3*3  POINTS , INTERPOLATION  A  L'ORDRE  2

                    Field has been * by scaling factor    277.43861       (Brho_ref =    851249.82    )

                    Integration step :  0.3000     cm

  A    1  1.0000     0.000     0.000     0.000     0.000          180.000     0.000     0.000     0.032     0.000            1
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000     0.000     0.000     0.032     0.000            2
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000     0.000     0.000     0.032     0.000            3
  A    1  1.0000     0.000     0.000     0.000     0.000          180.000     0.000     0.000     0.032     0.000            4


                CONDITIONS  DE  MAXWELL  (     4804.  PAS )  :
                       DIV(B)        LAPLACIEN(B)     ROTATIONNEL(B)
                     1.5725E-16      -5.9589E-18       -9.2880E-17
                                     -9.8029E-16       -1.7775E-14
                                      5.7980E-16       -1.7760E-14
                       LAPLACIEN SCALAIRE =   0.000    



     QUASEX - KPOS =  2 :  Element  is  mis-aligned  wrt.  the  optical  axis.
          X =   0.0000000     cm,   Y =   0.0000000     cm,  tilt  angle =   0.0000000     rad,   Z =   0.0000000     cm


 Cumulative length of optical axis =    12.3903684     m ;  Time  (for ref. rigidity & particle) =   4.133010E-08 s 

************************************************************************************************************************************
     16  Keyword, label(s) :  DRIFT       rotator_adjL                                                                 IPASS= 1


                              Drift,  length =    71.83684  cm

TRAJ #1 IEX,D,Y,T,Z,P,S,time :  1  0.000000E+00  1.945474E-04  2.978019E-04  3.619490E-02  5.522268E-02  1.3108769E+03  4.37264E-02
TRAJ #1 SX, SY, SZ, |S| :  1   -3.981790E-02   1.031612E-01   9.938673E-01   1.000000E+00

 Cumulative length of optical axis =    13.1087368     m   ;  Time  (for reference rigidity & particle) =   4.372633E-08 s 

************************************************************************************************************************************
     17  Keyword, label(s) :  MARKER      R+L+R+L+_E                                                                   IPASS= 1


************************************************************************************************************************************
     18  Keyword, label(s) :  FAISCEAU                                                                                 IPASS= 1

0                                             TRACE DU FAISCEAU
                                           (follows element #     17)
                                                  4 TRAJECTOIRES

                                   OBJET                                                  FAISCEAU

          D       Y(cm)     T(mr)     Z(cm)     P(mr)       S(cm)      D-1     Y(cm)    T(mr)    Z(cm)    P(mr)      S(cm)

o   1   1.0000     0.000     0.000     0.000     0.000      0.0000   0.0000    0.000    0.000    0.036    0.055   1.310877E+03     1
               Time of flight (mus) :  4.37264423E-02 mass (MeV/c2) :   938.272    
o   1   1.0000     0.000     0.000     0.000     0.000      0.0000   0.0000    0.000    0.000    0.036    0.055   1.310877E+03     2
               Time of flight (mus) :  4.37264423E-02 mass (MeV/c2) :   938.272    
o   1   1.0000     0.000     0.000     0.000     0.000      0.0000   0.0000    0.000    0.000    0.036    0.055   1.310877E+03     3
               Time of flight (mus) :  4.37264423E-02 mass (MeV/c2) :   938.272    
o   1   1.0000     0.000     0.000     0.000     0.000      0.0000   0.0000    0.000    0.000    0.036    0.055   1.310877E+03     4
               Time of flight (mus) :  4.37264423E-02 mass (MeV/c2) :   938.272    

************************************************************************************************************************************
     19  Keyword, label(s) :  SPNPRT      MATRIX                                                                       IPASS= 1

                          -- 1  GROUPS  OF  MOMENTA  FOLLOW   --

                    ----------------------------------------------------------------------------------
                    Momentum  group  #1 (D=   1.000000E+00;  particles  1  to 3 ; 


                Spin  components  of  the      3  particles,  spin  angles :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   |Si,Sf|   (Z,Sf_yz) (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_yz : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000    -0.039818  0.103161  0.993867  1.000000    271.9893  -92.282   45.153    6.349    1
 o  1  0.000000  1.000000  0.000000  1.000000    -0.103154  0.988917 -0.106780  1.000000    271.9893    8.538   96.127   96.130    2
 o  1  0.000000  0.000000  1.000000  1.000000    -0.993868 -0.106773 -0.028735  1.000000    271.9893  -91.647  104.568   91.647    3


                  Spin transfer matrix, momentum group # 1 :

         -3.981790E-02   -0.103154       -0.993868    
          0.103161        0.988917       -0.106773    
          0.993867       -0.106780       -2.873516E-02

     Determinant =     1.0000000000
     Trace =     0.9203641925;  spin precession acos((trace-1)/2) =    92.2820011103 deg
     Precession axis :  (-0.0000, -0.9947,  0.1032)  ->  angle to (X,Y) plane, to X axis :     5.9257,    90.0002 deg
     Spin precession/2pi (or Qs, fractional) :    2.5634E-01

                    ----------------------------------------------------------------------------------
                    Remaining  1  particles  ; 


                Spin  components  of  the      1  particles,  spin  angles :

                   INITIAL                                           FINAL

           SX        SY        SZ        |S|         SX        SY        SZ        |S|       GAMMA   |Si,Sf|   (Z,Sf_yz) (Z,Sf)
                                                                                                      (deg.)    (deg.)    (deg.)
                                                                                           (Sf_yz : projection of Sf on YZ plane)

 o  1  1.000000  0.000000  0.000000  1.000000    -0.039818  0.103161  0.993867  1.000000    271.9893  -92.282   45.153    6.349    4

************************************************************************************************************************************
     20  Keyword, label(s) :  END                                                                                      IPASS= 1

                             4 particles have been launched
                     Made  it  to  the  end :      4

************************************************************************************************************************************
 Pgm zgoubi : Execution ended normally, upon keyword END or FIN
   
 File in:   R+L+R+L+.inc
 File out:  zgoubi.res

  Zgoubi, author's Revision: 1670.
  Job  started  on  12-05-2022,  at  09:55:22 
  JOB  ENDED  ON    12-05-2022,  AT  09:55:23 

   CPU time, total :    0.86752799999999997     
