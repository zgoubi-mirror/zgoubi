Snake 2 going CCW. 3 o'clock snake
'OBJET'                                                                                                      1
79.366778931425273d3
1
3  1 1 1 1 1
   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.00  0.000000E+00 
   0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00000000E+00   0.00  1.00000000E+00 
 
'PARTICUL'                                                                                                   2
PROTON
 
'SPNTRK'                                                                                                     3
4
  1.00000000E+00  0.00000000E+00  0.00000000E+00
  0.00000000E+00  1.00000000E+00  0.00000000E+00
  0.00000000E+00  0.00000000E+00  1.00000000E+00
 
'SCALING'                                                                                                    4
1  10
BEND
-1
79.366778931425273
1
MULTIPOL
-1
79.366778931425273
1
MULTIPOL  *_QF*
-1
79.366778931425273 * 0.99844414
1
MULTIPOL *_QD*
-1
79.366778931425273 * 1.0014380
1
MULTIPOL  *_SXF*
-1
79.366778931425273 * .62
1
MULTIPOL  *_SXD*
-1
79.366778931425273 * 1.75
1
TOSCA    snk1LowB
-1
100.         !  low-field coils current (A)
1
TOSCA    snk1HighB
-1
322.         !  high-field coils current (A)
1
TOSCA    snk2LowB
-1
100.         !  low-field coils current (A)
1
TOSCA    snk2HighB
-1
322.         !  high-field coils current (A)
1
 
'MARKER'   snk2CCWSeg_S                                                                                      5
 
'MULTIPOL' YI3_TV4   VKIC                                                                                    6
0  .kicker
   0.000100  10.00   7.46081230E+03   0.00000000E+00   0.00000000E+00  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
4   1.45500E-01   2.26700E+00  -6.39500E-01   1.15580E+00   0.00000E+00   0.00000E+00 
  0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
4   1.45500E-01   2.26700E+00  -6.39500E-01   1.15580E+00   0.00000E+00   0.00000E+00 
 1.57080E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#20|4|20  Kick
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'DRIFT'    DRIFT_221 DRIF                                                                                    7
 436.747240
'DRIFT'    YI3_BH5   HMON                                                                                    8
   0.000000
'DRIFT'    DRIFT_222 DRIF                                                                                    9
  22.622760
'MULTIPOL' YI3_TQ5   QUAD                                                                                   10
0  .Quad
  75.000000  10.00   0.00000000E+00   9.38666530E-03   0.00000000E+00  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0000   0.0000   6.00   3.00   1.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00 
  0.0000   0.0000   6.00   3.00   1.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00 
 0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#30|150|30   Quad  YI3_TQ5
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'DRIFT'    DRIFT_223 DRIF                                                                                   11
  13.105000
'MULTIPOL' YI3_QF5   QUAD                                                                                   12
0  .Quad
 111.000000  10.00   0.00000000E+00   9.26784014E-02   0.00000000E+00  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0000   0.0000   6.00   3.00   1.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00 
  0.0000   0.0000   6.00   3.00   1.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00 
 0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#30|222|30   Quad  YI3_QF5
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'DRIFT'    DRIFT_224 DRIF                                                                                   13
  54.525000
'MARKER'   YI3_QGT5  MULT                                                                                   14
'MULTIPOL' YI3_TH5   HKIC                                                                                   15
0  .kicker
   0.000100  10.00  -0.00000000E+00   0.00000000E+00   0.00000000E+00  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
4   1.45500E-01   2.26700E+00  -6.39500E-01   1.15580E+00   0.00000E+00   0.00000E+00 
  0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
4   1.45500E-01   2.26700E+00  -6.39500E-01   1.15580E+00   0.00000E+00   0.00000E+00 
 0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#20|4|20  Kick
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'DRIFT'    DRIFT_225 DRIF                                                                                   16
 283.979600
'MULTIPOL' YI3_DH5_E2MULT                                                                                   17
0  .Mult
   0.001000  10.00   0.00000000E+00  -0.00000000E+00   1.17670000E+02  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00 
  0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00 
 0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#30|9|30   MultYI3_DH5_E2
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'BEND'     YI3_DH5   SBEN                                                                                   18
0  .Bend
6.915765707E+02   0.000000000E+00   4.121876942E-02
0.00  0.00   0.00000000
4 .2401  1.8639  -.5572  .3904 0. 0. 0.
0.00  0.00   0.00000000
4 .2401  1.8639  -.5572  .3904 0. 0. 0.
1.  !  Bend YI3_DH5
3 0. 0.    -1.425345E-02
'MULTIPOL' YI3_DH5_E1MULT                                                                                   19
0  .Mult
   0.001000  10.00   0.00000000E+00  -0.00000000E+00   2.28510000E+01  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00 
  0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00 
 0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#30|9|30   MultYI3_DH5_E1
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'DRIFT'    DRIFT_226 DRIF                                                                                   20
 227.776845
'DRIFT'    YI3_BV6   VMON                                                                                   21
   0.000000
'DRIFT'    DRIFT_227 DRIF                                                                                   22
  22.622760
'MULTIPOL' YI3_TQ6   QUAD                                                                                   23
0  .Quad
  75.000000  10.00   0.00000000E+00  -9.00825760E-03   0.00000000E+00  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0000   0.0000   6.00   3.00   1.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00 
  0.0000   0.0000   6.00   3.00   1.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00 
 0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#30|150|30   Quad  YI3_TQ6
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'DRIFT'    DRIFT_228 DRIF                                                                                   24
  13.105000
'MULTIPOL' YI3_QD6   QUAD                                                                                   25
0  .Quad
 111.000000  10.00   0.00000000E+00  -9.27660140E-02   0.00000000E+00  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0000   0.0000   6.00   3.00   1.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00 
  0.0000   0.0000   6.00   3.00   1.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00 
 0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#30|222|30   Quad  YI3_QD6
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'DRIFT'    DRIFT_229 DRIF                                                                                   26
  54.525000
'MARKER'   YI3_QS6   MULT                                                                                   27
'MULTIPOL' YI3_TV6   VKIC                                                                                   28
0  .kicker
   0.000100  10.00   3.64555946E+03   0.00000000E+00   0.00000000E+00  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
4   1.45500E-01   2.26700E+00  -6.39500E-01   1.15580E+00   0.00000E+00   0.00000E+00 
  0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
4   1.45500E-01   2.26700E+00  -6.39500E-01   1.15580E+00   0.00000E+00   0.00000E+00 
 1.57080E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#20|4|20  Kick
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'DRIFT'    DRIFT_230 DRIF                                                                                   29
 807.665093
'MULTIPOL' YI3_DH6_E2MULT                                                                                   30
0  .Mult
   0.001000  10.00   0.00000000E+00  -0.00000000E+00   1.10990000E+02  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00 
  0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00 
 0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#30|9|30   MultYI3_DH6_E2
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'BEND'     YI3_DH6   SBEN                                                                                   31
0  .Bend
2.949408455E+02   0.000000000E+00   4.149191281E-02
0.00  0.00   0.00000000
4 .2401  1.8639  -.5572  .3904 0. 0. 0.
0.00  0.00   0.00000000
4 .2401  1.8639  -.5572  .3904 0. 0. 0.
1.  !  Bend YI3_DH6
3 0. 0.    -6.118868E-03
'MULTIPOL' YI3_DH6_E1MULT                                                                                   32
0  .Mult
   0.001000  10.00   0.00000000E+00   0.00000000E+00  -5.21150000E-01  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00 
  0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00 
 0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#30|9|30   MultYI3_DH6_E1
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'DRIFT'    DRIFT_231 DRIF                                                                                   33
 157.899703
'MARKER'   YI3_QGT7  MULT                                                                                   34
'MULTIPOL' YI3_TH7   HKIC                                                                                   35
0  .kicker
   0.000100  10.00  -0.00000000E+00   0.00000000E+00   0.00000000E+00  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
4   1.45500E-01   2.26700E+00  -6.39500E-01   1.15580E+00   0.00000E+00   0.00000E+00 
  0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
4   1.45500E-01   2.26700E+00  -6.39500E-01   1.15580E+00   0.00000E+00   0.00000E+00 
 0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#20|4|20  Kick
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'DRIFT'    DRIFT_232 DRIF                                                                                   36
  54.537800
'MULTIPOL' YI3_QF7   QUAD                                                                                   37
0  .Quad
  92.974400  10.00   0.00000000E+00   8.95344540E-02   0.00000000E+00  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0000   0.0000   6.00   3.00   1.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00 
  0.0000   0.0000   6.00   3.00   1.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00 
 0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#30|186|30   Quad  YI3_QF7
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'DRIFT'    DRIFT_233 DRIF                                                                                   38
  29.623360
'DRIFT'    YI3_B7    MONI                                                                                   39
   0.000000
 
'DRIFT'    snak2SS_up                                                                                       40
 131.741795
 
'MARKER'  SNK2_S                                                                                            41
 
'DRIFT'                                                                                                     42
 -78.200000
!! 'DRIFT'  CCW here -> moved to down end
!!3.600000
 
'DRIFT'                                                                                                     43
  16.400000
 
'TOSCA'      snk2LowB                                                                                       44
0  0020  ! .plt
  1.00000000E+00  1.00000000E+00  1.00000000E+00  1.00000000E+00
HEADER_8   RHIC_helix
361 81 81  15.1  0.748502994012e-4     ! = 1/1.3360e4
b_model3a2a-x-4_4_y-4_4_z-180_180-integral.table
0 0 0 0
2
.3
2   0.00000000E+00  0.00000000E+00  0.00000000E+00
 
'DRIFT'    2.162m_C1toC2                                                                                    45
 -98.800000
 
'TOSCA'      snk2HighB                                                                                      46
0  0020  ! .plt
 -1.00000000E+00  1.00000000E+00  1.00000000E+00  1.00000000E+00
HEADER_8   RHIC_helix
361 81 81  15.1  0.246305418719e-4   ! = 1/4.060e4
b_model3a2a322a-x-4_4_y-4_4_z-180_180-integral.table
0 0 0 0
2
.3
2   0.00000000E+00  0.00000000E+00  0.00000000E+00
 
'DRIFT'    YI3_B7.1  VMON  ! center of 3 oclock snake                                                       47
 -37.600000
'DRIFT'    2.162m_C2toC3                                                                                    48
 -37.600000
 
'TOSCA'      snk2HighB                                                                                      49
0  0020  ! .plt
  1.00000000E+00  1.00000000E+00  1.00000000E+00  1.00000000E+00
HEADER_8   RHIC_helix
361 81 81  15.1  0.246305418719e-4   ! = 1/4.060e4
b_model3a2a322a-x-4_4_y-4_4_z-180_180-integral.table
0 0 0 0
2
.3
2   0.00000000E+00  0.00000000E+00  0.00000000E+00
 
'DRIFT'    2.162m_C3toC4                                                                                    50
 -98.800000
 
'TOSCA'      snk2LowB                                                                                       51
0  0020  ! .plt
 -1.00000000E+00  1.00000000E+00  1.00000000E+00  1.00000000E+00
HEADER_8   RHIC_helix
361 81 81  15.1  0.748502994012e-4     ! = 1/1.3360e4
b_model3a2a-x-4_4_y-4_4_z-180_180-integral.table
0 0 0 0
2
.3
2   0.00000000E+00  0.00000000E+00  0.00000000E+00
 
'DRIFT'                                                                                                     52
  16.400000
'DRIFT'                                                                                                     53
   3.600000
'DRIFT'                                                                                                     54
 -78.200000
 
'MARKER'  SNK2_E                                                                                            55
 
'DRIFT'    snak2SS_dw                                                                                       56
 131.741795
 
!'DRIFT'    DRIFT_234 DRIF                                                                                  586
!655.441795
!'DRIFT'    YI3_B7.1  VMON                                                                                  587
!0.0000
!'DRIFT'    DRIFT_235 DRIF                                                                                  588
!655.441795
 
'DRIFT'    YI3_B8    MONI                                                                                   57
   0.000000
'DRIFT'    DRIFT_236 DRIF                                                                                   58
  29.610560
'MULTIPOL' YI3_QD8   QUAD                                                                                   59
0  .Quad
 111.000000  10.00   0.00000000E+00  -8.23299515E-02   0.00000000E+00  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0000   0.0000   6.00   3.00   1.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00 
  0.0000   0.0000   6.00   3.00   1.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00 
 0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#30|222|30   Quad  YI3_QD8
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'DRIFT'    DRIFT_237 DRIF                                                                                   60
  54.525000
'MARKER'   YI3_QS8   MULT                                                                                   61
'MULTIPOL' YI3_TV8   VKIC                                                                                   62
0  .kicker
   0.000100  10.00   4.82766265E+03   0.00000000E+00   0.00000000E+00  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
4   1.45500E-01   2.26700E+00  -6.39500E-01   1.15580E+00   0.00000E+00   0.00000E+00 
  0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
4   1.45500E-01   2.26700E+00  -6.39500E-01   1.15580E+00   0.00000E+00   0.00000E+00 
 1.57080E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#20|4|20  Kick
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'DRIFT'    DRIFT_238 DRIF                                                                                   63
 157.618713
'MULTIPOL' YI3_DH8_E2MULT                                                                                   64
0  .Mult
   0.001000  10.00   0.00000000E+00  -0.00000000E+00   1.31470000E+02  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00 
  0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00 
 0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#30|9|30   MultYI3_DH8_E2
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'BEND'     YI3_DH8   SBEN                                                                                   65
0  .Bend
9.440061541E+02   0.000000000E+00   4.117822024E-02
0.00  0.00   0.00000000
4 .2401  1.8639  -.5572  .3904 0. 0. 0.
0.00  0.00   0.00000000
4 .2401  1.8639  -.5572  .3904 0. 0. 0.
1.  !  Bend YI3_DH8
3 0. 0.    -1.943747E-02
'MULTIPOL' YI3_DH8_E1MULT                                                                                   66
0  .Mult
   0.001000  10.00   0.00000000E+00  -0.00000000E+00   3.04840000E+01  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00 
  0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00 
 0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#30|9|30   MultYI3_DH8_E1
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'DRIFT'    DRIFT_239 DRIF                                                                                   67
 157.618712
'MARKER'   YI3_QS9   MULT                                                                                   68
'MULTIPOL' YI3_TH9   HKIC                                                                                   69
0  .kicker
   0.000100  10.00   4.49245784E+01   0.00000000E+00   0.00000000E+00  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
4   1.45500E-01   2.26700E+00  -6.39500E-01   1.15580E+00   0.00000E+00   0.00000E+00 
  0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
4   1.45500E-01   2.26700E+00  -6.39500E-01   1.15580E+00   0.00000E+00   0.00000E+00 
 0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#20|4|20  Kick
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'DRIFT'    DRIFT_240 DRIF                                                                                   70
  54.525000
'MULTIPOL' YI3_QF9   QUAD                                                                                   71
0  .Quad
 111.000000  10.00   0.00000000E+00   8.08755978E-02   0.00000000E+00  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0000   0.0000   6.00   3.00   1.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00 
  0.0000   0.0000   6.00   3.00   1.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00 
 0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#30|222|30   Quad  YI3_QF9
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'DRIFT'    DRIFT_241 DRIF                                                                                   72
  13.105000
'MULTIPOL' YI3_SXF9  SEXT                                                                                   73
0  .Sext
  75.000000  10.00   0.00000000E+00   0.00000000E+00   1.16207049E-02  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00 
  0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00 
 0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#30|150|30   Sext  YI3_SXF9
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'DRIFT'    DRIFT_242 DRIF                                                                                   74
  22.622760
'DRIFT'    YI3_BH9   HMON                                                                                   75
   0.000000
'DRIFT'    DRIFT_243 DRIF                                                                                   76
 751.462333
'MULTIPOL' YI3_DH9_E2MULT                                                                                   77
0  .Mult
   0.001000  10.00   0.00000000E+00  -0.00000000E+00   1.07780000E+02  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00 
  0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00 
 0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#30|9|30   MultYI3_DH9_E2
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'BEND'     YI3_DH9   SBEN                                                                                   78
0  .Bend
2.949408353E+02   0.000000000E+00   4.160680160E-02
0.00  0.00   0.00000000
4 .2401  1.8639  -.5572  .3904 0. 0. 0.
0.00  0.00   0.00000000
4 .2401  1.8639  -.5572  .3904 0. 0. 0.
1.  !  Bend YI3_DH9
3 0. 0.    -6.135811E-03
'MULTIPOL' YI3_DH9_E1MULT                                                                                   79
0  .Mult
   0.001000  10.00   0.00000000E+00   0.00000000E+00  -2.90260000E+00  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00 
  0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00 
 0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#30|9|30   MultYI3_DH9_E1
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'DRIFT'    DRIFT_244 DRIF                                                                                   80
 157.899702
'MARKER'   YI3_QS10  MULT                                                                                   81
'MULTIPOL' YI3_TV10  VKIC                                                                                   82
0  .kicker
   0.000100  10.00   1.93951941E+03   0.00000000E+00   0.00000000E+00  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
4   1.45500E-01   2.26700E+00  -6.39500E-01   1.15580E+00   0.00000E+00   0.00000E+00 
  0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
4   1.45500E-01   2.26700E+00  -6.39500E-01   1.15580E+00   0.00000E+00   0.00000E+00 
 1.57080E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#20|4|20  Kick
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'DRIFT'    DRIFT_245 DRIF                                                                                   83
  54.525000
'MULTIPOL' YI3_QD10  QUAD                                                                                   84
0  .Quad
 111.000000  10.00   0.00000000E+00  -8.65166526E-02   0.00000000E+00  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0000   0.0000   6.00   3.00   1.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00 
  0.0000   0.0000   6.00   3.00   1.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00 
 0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#30|222|30   Quad  YI3_QD10
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'DRIFT'    DRIFT_246 DRIF                                                                                   85
  13.105000
'MULTIPOL' YI3_SXD10 SEXT                                                                                   86
0  .Sext
  75.000000  10.00   0.00000000E+00   0.00000000E+00  -1.05147408E-02  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00 
  0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00 
 0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#30|150|30   Sext  YI3_SXD10
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'DRIFT'    DRIFT_247 DRIF                                                                                   87
  22.622760
'DRIFT'    YI3_BV10  VMON                                                                                   88
   0.000000
'DRIFT'    DRIFT_248 DRIF                                                                                   89
 101.415953
'MULTIPOL' YI3_DH10_EMULT                                                                                   90
0  .Mult
   0.001000  10.00   0.00000000E+00  -0.00000000E+00   1.29970000E+02  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00 
  0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00 
 0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#30|9|30   MultYI3_DH10_E2
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'BEND'     YI3_DH10  SBEN                                                                                   91
0  .Bend
9.440060757E+02   0.000000000E+00   4.120534210E-02
0.00  0.00   0.00000000
4 .2401  1.8639  -.5572  .3904 0. 0. 0.
0.00  0.00   0.00000000
4 .2401  1.8639  -.5572  .3904 0. 0. 0.
1.  !  Bend YI3_DH10
3 0. 0.    -1.945027E-02
'MULTIPOL' YI3_DH10_EMULT                                                                                   92
0  .Mult
   0.001000  10.00   0.00000000E+00  -0.00000000E+00   3.51810000E+01  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00 
  0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
6   1.12200E-01   6.26710E+00  -1.49820E+00   3.58820E+00  -2.12090E+00   1.72300E+00 
 0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#30|9|30   MultYI3_DH10_E1
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
'DRIFT'    DRIFT_249 DRIF                                                                                   93
 157.618712
'MARKER'   YI3_QGT11 MULT                                                                                   94
'MULTIPOL' YI3_TH11  HKIC                                                                                   95
0  .kicker
   0.000100  10.00   4.18492731E+02   0.00000000E+00   0.00000000E+00  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
4   1.45500E-01   2.26700E+00  -6.39500E-01   1.15580E+00   0.00000E+00   0.00000E+00 
  0.0000   0.0000   1.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
4   1.45500E-01   2.26700E+00  -6.39500E-01   1.15580E+00   0.00000E+00   0.00000E+00 
 0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00
#20|4|20  Kick
1   0.00000000E+00  0.00000000E+00  0.00000000E+00
 
! Include_SegmentEnd : snake3OCCW_Segment.inc                        (had n_inc, depth :  1 2)
'MARKER'   snk2CCWSeg_E                                                                                     96
 
 ! 'FIT2'                                                                                                      97
 ! 6
 ! 6   4 0 [-10000.,10000.]					
 ! 28   4 0 [-10000.,10000.]					
 ! 62   4 0 [-10000.,10000.]					
 ! 69   4 0 [-10000.,10000.]					
 ! 82   4 0 [-10000.,10000.]					
 ! 95   4 0 [-10000.,10000.]					
 ! 6  1e-8
 ! 3 1 4 40   -1.   1.  0      !!      Z entry snake
 ! 3 1 5 40    0.  .2  0      !!      P entry snake
 ! 3 1 2 #End  0.  1.  0
 ! 3 1 3 #End  0.  1.  0
 ! 3 1 4 #End  0.  1.  0
 ! 3 1 5 #End  0.  1.  0
 
'FAISCEAU'                                                                                                  98
'SPNPRT'  MATRIX                                                                                            99
 
'END'                                                                                                      103
