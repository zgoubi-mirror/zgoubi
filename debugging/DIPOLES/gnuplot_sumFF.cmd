          dec = 0.01  # just to distinguish various curves on the graph

set key c r

dec = 0.1 # just to distinguish the various curves

plot \
'fort.77' u 1:2 w l lw 2 lc rgb "red" tit "fs" ,\
'fort.77' u 1:3 w l lw 2 lc rgb "blue" tit "fm" ,\
'fort.77' u 1:($4+dec) w l lw 2 lc rgb "black" tit "fs+fm" 

 set samples 10000
 set terminal postscript eps blacktext color enh size 8cm,5cm 
 set output "gnuplot_sumFF.eps"
 replot
 set terminal X11
 unset output

pause 1
exit
