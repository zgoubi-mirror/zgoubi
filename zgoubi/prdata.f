C  ZGOUBI, a program for computing the trajectories of charged particles
C  in electric and magnetic fields
C  Copyright (C) 1988-2007  François Méot
C
C  This program is free software; you can redistribute it and/or modify
C  it under the terms of the GNU General Public License as published by
C  the Free Software Foundation; either version 2 of the License, or
C  (at your option) any later version.
C
C  This program is distributed in the hope that it will be useful,
C  but WITHOUT ANY WARRANTY; without even the implied warranty of
C  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C  GNU General Public License for more details.
C
C  You should have received a copy of the GNU General Public License
C  along with this program; if not, write to the Free Software
C  Foundation, Inc., 51 Franklin Street, Fifth Floor,
C  Boston, MA  02110-1301  USA
C
C  François Méot <fmeot@bnl.gov>
C  Brookhaven National Laboratory
C  C-AD, Bldg 911
C  Upton, NY, 11973, USA
C  -------
      SUBROUTINE PRDATA(NLIN,FLIN,FDAT,MFDA,NRES,
     >                                    LABEL,NOEL)  !,NDAT)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      CHARACTER(*) FLIN
      CHARACTER(MFDA) FDAT
      INCLUDE 'MXLD.H'
      PARAMETER (LBLSIZ=20)
      CHARACTER(LBLSIZ) LABEL(MXL,2)
C----------------------------------------------------------
C     Copy zgoubi.dat into zgoubi.res, and a few more stuff
C----------------------------------------------------------
      PARAMETER (I2000=2000)
      CHARACTER(I2000) TEXT
      PARAMETER (I6=6)
      CHARACTER(I6) TXT6
      INTEGER DEBSTR, FINSTR
      LOGICAL EMPTY
      LOGICAL OK
C      LOGICAL IDLUNI

      PARAMETER (KSIZ=10)
      PARAMETER(MXFIL=1)
      PARAMETER(MXDEEP=10)              ! Max. file depth is 10 (depth 1 is initial data file) 
      DIMENSION LUNR(MXDEEP)
      CHARACTER(MFDA+40) CMMND
      LOGICAL YINC, YINC2(MXDEEP), STRCON
      CHARACTER(LBLSIZ) LBL1A(MXFIL,MXDEEP),LBL2A(MXFIL,MXDEEP),
     >LBL1B(MXFIL,MXDEEP),LBL2B(MXFIL,MXDEEP)
      SAVE LBL1A,LBL2A,LBL1B,LBL2B
      CHARACTER(LBLSIZ) L1A,L2A,L1B,L2B
      CHARACTER(LBLSIZ) L1AP,L2AP,L1BP,L2BP
      SAVE L1AP,L2AP,L1BP,L2BP
      LOGICAL LBAVU(MXDEEP), LBBVU(MXDEEP), EXS
      CHARACTER(LBLSIZ) LABEL1, LABEL2
      DIMENSION NINC(MXFIL,MXDEEP)

      CHARACTER(132) FINC(MXFIL,MXDEEP)
      PARAMETER (I104=104)
      
      PARAMETER(MFD = MXFIL*MXDEEP)

C      CHARACTER(MFDA) FNAME(MXFIL,MXDEEP)
      CHARACTER(MFDA) FNAM

      logical lbbv1
      
      DATA YINC, YINC2 / .FALSE. , MXDEEP*.FALSE. /
      DATA EXS / .FALSE. /
      DATA LBAVU, LBBVU / MXDEEP*.FALSE., MXDEEP*.FALSE. /
      DATA IDEEP / 1 /
      DATA L1AP,L2AP,L1BP,L2BP / '*','*','neverMeetMe!', 'neverMeetMe!'/  
      DATA NINC /  MFD * 0 /
      
      WRITE(6,*) '  '
      WRITE(6,*) ' Copy/expand  input  .dat  file  to  zgoubi.res,'
      WRITE(6,*) ' number  and  label  elements,  record  labels, ...'

      IDA = 1              ! input data file number
      LUNR(IDA) = NLIN     ! Normally NLIN = NDAT
      IDEEP = 1  ! depth of main file is 1. Depth of INCLUDE is 2 and up to MXDEEP,
      
C----- Read zgoubi.dat title (1st data line)
      READ(LUNR(IDA),FMT='(A)',ERR=10,END=95) TEXT
      IF(NRES.GT.0)
     >WRITE(NRES,FMT='(A)') TEXT(DEBSTR(TEXT):FINSTR(TEXT))

      NOEL=1
      L1A = L1AP       ! Occurence of INCLUDE, and only that, will change L1-2,A-B. 
      L2A = L2AP
      L1B = L1BP 
      L2B = L2BP 
      IFL = 1
      LBL1A(IFL,IDEEP) = L1A    ! These data are reset if INCLUDE is met
      LBL2A(IFL,IDEEP) = L2A
      LBL1B(IFL,IDEEP) = L1B
      LBL2B(IFL,IDEEP) = L2B
      LBAVU(IDEEP) = .TRUE.
      LBBVU(IDEEP) = .FALSE.
 10   CONTINUE
      
      READ (LUNR(IDA),FMT='(A)',ERR=10,END=95) TEXT
      TEXT = TEXT(DEBSTR(TEXT):FINSTR(TEXT))

c      write(*,*) 'ifl,ideep,ida  ',ifl,ideep,ida,trim( TEXT)
c     > ,' ///// ',L1A,L2A,L1B,L2B,lbavu(ideep),lbbvu(ideep)
C      read(*,*)
      
      
      IF( LBAVU(IDEEP) .AND. .NOT.LBBVU(IDEEP)) THEN    ! reading within INCLUDE range
          
        IF( .NOT. EMPTY(TEXT) ) THEN
            
c        write(*,*) 'aaa ',TEXT(DEBSTR(TEXT):FINSTR(TEXT)),ideep,ida,ifl
            
          IDEB = 1
          IF(TEXT(IDEB:IDEB+5) .EQ. '''FIN'''
     >    .OR. TEXT(IDEB:IDEB+5) .EQ. '''END''') THEN
            IF(IDEEP .EQ. 1) THEN        ! Means READ is from parent .dat.
              WRITE(NRES,FMT='(A)') TEXT(IDEB:FINSTR(TEXT))
              GOTO 98      ! RETURN
            ELSE           ! Means 'END' is from an INCLUDE file. Goes wrong: should have left it upon label1==end sequence
              GOTO 90      ! End job.
            ENDIF
          ELSEIF(TEXT(IDEB:IDEB) .EQ. '!') THEN       ! Means this line is a comment
            WRITE(NRES,FMT='(A)') TEXT(IDEB:FINSTR(TEXT))
            GOTO 10        
          ENDIF
        ELSE
          WRITE(NRES,FMT='(A)') ' '
          GOTO 10
        ENDIF

      ENDIF   
      
C Meet a keyword      
      IF( TEXT(IDEB:IDEB) .EQ. '''' ) THEN !  Meet a keyword
           
C        NOEL=NOEL+1  ! if INCLUDE, will be replaced by MARKER
        TEXT = TEXT(DEBSTR(TEXT):I104)
        OK = STRCON(TEXT(IDEB+1:IDEB+KSIZ+1),'''',
     >                                            I)

        IF(TEXT(IDEB+1:I) .NE. 'INCLUDE') THEN     ! INCLUDE is not a keyword!
        
C     Return keyword labels, lbavu, lbbvu. 
C     Updates l1a, l2a, l1b, l2b if end of include segment is met
C       LBAVU is necessary to determine whether WRTITE to NRES or not
C       LBBVU is necessary to determine end of INCLUDE segment
C     If lbavu & .not. lbbvu, then this keyword is within INCLUDE segment,
C     will continue read(nadt)/written(nres) 
C     If lbavu & lbbvu, then this keyword is the end of INCLUDE, will 
C     stop read(ndat)/write(nres) 
C     CALL LBLGET(NRES,TEXT(I+1:),FINC,
          LBBV1 = LBBVU(IDEEP)
c               write(*,*) ' prdata lbbv1, ideep ',lbbv1, ideep
           
          CALL LBLGET(NRES,TEXT,FINC,IFL,
     >              IDEEP,L1A,L2A,L1B,L2B,LBL1A,LBL2A,LBL1B,LBL2B,
     >              IDA,LUNR,LABEL1,LABEL2,LBAVU,LBBVU)

          LABEL(NOEL,1) = LABEL1
          LABEL(NOEL,2) = LABEL2
          
          IF((.NOT. LBAVU(IDEEP)) .OR. LBBVU(IDEEP)) THEN

          ELSEIF(LBAVU(IDEEP) .AND. LBBVU(IDEEP+1)) THEN
C     just met end of include sequence

             IF(NINC(IFL,IDEEP+1) .NE. 0) THEN

                DO JJ = 1,3
                   BACKSPACE(LUNR(IDA))
                ENDDO
                
             ENDIF
            
          ELSE

            LABEL(NOEL,1) = LABEL1
            LABEL(NOEL,2) = LABEL2
                  
          ENDIF

          

        ELSE  ! Case TEXT = INCLUDE

          IF( LBAVU(IDEEP) .AND. .NOT. LBBVU(IDEEP)) THEN
            
            WRITE(NRES,FMT='(A)') '! '//TEXT(IDEB:I104)        ! Write "! 'INCLUDE'  etc. ".
            IF(FINSTR(FLIN)-9 .GE. 1) THEN
              IF(FLIN(FINSTR(FLIN)-9:FINSTR(FLIN)) .EQ. 'zgoubi.dat')
     >        CALL ENDJOB('Pgm prdata. Job includes INCLUDE keyword, '//
     >        '"zgoubi.dat" is reserved in this case => cannot be '//
     >        'used as input file name. Please use different input '//
     >        'file name and "zgoubi -in filename" command.'
     >        ,-99)
            ENDIF

            LINE =LINE+1
            READ(LUNR(IDA),FMT='(A)',ERR=10,END=93) TEXT
            READ(TEXT,*,ERR=93) NBFIL
            
            IF(NBFIL .GT. 1) CALL ENDJOB('Pgm prdata. '//
     >      'INCLUDE is limited to 1 file, '//
     >      '(functionality of NBFIL>1 needs be checked). '// 
     >      'Use several INCLUDEs instead. Occured at LINE=',LINE)
            NBFIL = 1
          
            IFL = 1
            LINE = LINE + 1
            READ(LUNR(IDA),FMT='(A)',ERR=10,END=93) TEXT
C TEXT is of the form   ninc(ifl,ideep) * filename[LBL1a,LBLl2a:LBL1b,LBL2b].
C     Only text within range Any_KEYWORD LABEL1=lbl1a&LABEL2=lbl2a : Any_KEYWORD
C          LABEL1=lbl1b&LABEL2=lbl2b will be included.
C Open the INCLUDE file and updates ideep, ida, LBL1A,LBL2A,LBL1B,LBL2B,FINC,ninc
            CALL INCRAN(NRES,IDEEP,IFL,TEXT,
     >               L1A,L2A,L1B,L2B,
     >               IDA,LUNR,LBL1A,LBL2A,LBL1B,LBL2B,FINC,NINC)
            TEXT = '! '//TEXT(DEBSTR(TEXT):)

          ENDIF 
          
        ENDIF  ! TEXT(IDEB+1:I-1) .EQ. 'INCLUDE') 

c        write(*,*)
c     >       '/// prdata, keyword case  '
c     >   //trim(text(1:100)),ideep,LBAVU(IDEEP),LBBVU(IDEEP)
c        write(*,*)
c     >       '/// prdata, l1a,l1b,labelNoel1,labelNoel2 :  '
c     >       //L1A,L1B,LBL1a,LBL1B,
c     >   ' **'//label(noel,1)//'**  **'//label(noel,2)//'**  ',noel


        LBAVU(IDEEP+1) = .FALSE.
          
      ENDIF                     !  case meet a keyword

      IF(NRES.GT.0) THEN
        IF(LBAVU(IDEEP).AND. (.NOT.LBBVU(IDEEP))) THEN
          IF( TEXT(IDEB:IDEB) .EQ. '''' ) THEN !  Case keyword
            WRITE(TXT6,FMT='(I6)') NOEL
            TEXT = TEXT(1:I104)//TXT6
          ENDIF
          WRITE(NRES,FMT='(A)') TEXT(DEBSTR(TEXT):FINSTR(TEXT))
          IF( TEXT(IDEB:IDEB) .EQ. '''' ) NOEL = NOEL +1
        ENDIF
C        IF(LBAVU(IDEEP).AND. (.NOT.LBBVU(IDEEP))) 
C     >  WRITE(NRES,FMT='(A)') TEXT(DEBSTR(TEXT):FINSTR(TEXT))
        CALL FLUSH2(NRES,.FALSE.)
      ENDIF

      GOTO 10
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

 98   CONTINUE
c          write(*,*) ' ndat ',ndat, trim(fdat)
c          write(*,*) ' nlin, lunr(1) ',nlin,lunr(1), trim(flin),nres
c           read(*,*)
c           read(*,*)
      CLOSE(LUNR(1))
      CALL FLUSH2(NRES,.FALSE.)
C Now, zgoubi.res contains the full sequence, fully expanded.
Copy zgoubi.res to zgoubi.dat. The latter will be run by zgoubi, 
C execution oucomes will be logged to zgoubi.res.
      CMMND = 'cp zgoubi.res '//FDAT(DEBSTR(FDAT):FINSTR(FDAT))
      CALL SYSTEM(CMMND)
      
C      OK=IDLUNI(
C     >          NDAT)      
C      OPEN(UNIT=NDAT,FILE=FDAT)
CC      FNAME(IFL,IDEEP) = FDAT(DEBSTR(FDAT):FINSTR(FDAT))

      RETURN

 93   CONTINUE
Comes here upon read EOF
      WRITE(ABS(NRES),FMT=992) TRIM(TEXT),IFL,IDEEP,IDA,LUNR(IDA)
      WRITE(6,FMT=992) TRIM(TEXT),IFL,IDEEP,IDA,LUNR(IDA)
 992  FORMAT(//,'   Pgm prdata. Last line read was: ',A,/
     >,'file number and depth were: ',2(I0,1X),/
     >,'IDA,  LUNR(IDA) : ',2(I0,1X))
      WRITE(TEXT,*) ' Pgm prdata. Exit on 93. '
     >//'Input data error, at line #',LINE
      GOTO 999
      
 95   CONTINUE
Comes here upon read EOF
      TEXT='Pgm prdata. Exit on 95.'
      IF    (IDEEP .EQ. 1) THEN
        TEXT=TRIM(TEXT)//' Error during read from parent file'
     >  //' (missing ''FIN'' or ''END'' ?).'
      ELSEIF(IDEEP .LE. 0) THEN
        TEXT=TRIM(TEXT)//' Error: found IDEEP .le. 0'
      ENDIF
      
 90   CONTINUE
      TEXT='Pgm prdata. Problem w/ INCLUDE. Exit on 90,'
      IF(IDEEP .GT. 1) THEN
        inquire(unit=lunr(ida),name=fnam)
        TEXT=TRIM(TEXT)//' from file '//trim(fnam)
     >  //' (downward boundary label1[, label2] not met ?).'
      ELSE
        inquire(unit=lunr(ida),name=fnam)
        TEXT=TRIM(TEXT)//' from parent file.'
     >  //' Missing ''FIN'' or ''END'' ?'
      ENDIF

 999  CONTINUE
      WRITE(ABS(NRES),FMT='(/,A)') TEXT(DEBSTR(TEXT):FINSTR(TEXT))
      WRITE(6,FMT='(/,A)') TEXT(DEBSTR(TEXT):FINSTR(TEXT))
      
      CALL ENDJOB('*** Pgm prdata : '//
     >'input data error, exit! ',-99)
      RETURN
      END
