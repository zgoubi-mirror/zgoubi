C  ZGOUBI, a program for computing the trajectories of charged particles
C  in electric and magnetic fields
C  Copyright (C) 1988-2007  François Méot
C
C  This program is free software; you can redistribute it and/or modify
C  it under the terms of the GNU General Public License as published by
C  the Free Software Foundation; either version 2 of the License, or
C  (at your option) any later version.
C
C  This program is distributed in the hope that it will be useful,
C  but WITHOUT ANY WARRANTY; without even the implied warranty of
C  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C  GNU General Public License for more details.
C
C  You should have received a copy of the GNU General Public License
C  along with this program; if not, write to the Free Software
C  Foundation, Inc., 51 Franklin Street, Fifth Floor,
C  Boston, MA  02110-1301  USA
C
C  François Méot <fmeot@bnl.gov>
C  Brookhaven National Laboratory
C  C-AD, Bldg 911
C  Upton, NY, 11973, USA
C  -------
      SUBROUTINE SVDPR(LSVD,TXFMT,NLM,NLMC,MR,NC,
     >                 KLE,IPASS,LABEL,HKIC,VKIC,
     >                                            AA)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C      PARAMETER (MXCO=1000)
C      DIMENSION AA(MXPU,MXCO)
C      PARAMETER (MR=1000,NC=1000)
      DIMENSION AA(MR,NC),AAO(MR,NC)
      CHARACTER(*) KLE(*), TXFMT

      INCLUDE 'MXLD.H'
      CHARACTER(*) LABEL(MXL,2)
      PARAMETER (MXPUD=9,MXPU=1000)
      INCLUDE 'C.CO.H'     ! COMMON/CO/ FPU(MXPUD,MXPU),KCO,NPUF,NFPU,IPU
      INCLUDE "C.DON.H"     ! COMMON/DON/ A(MXL,MXD),IQ(MXL),IP(MXL),NB,NOEL

      DIMENSION KHV(MXPU), KHVI(MXPU)     ! 1, 2, 3 FOR H V, HV
      SAVE NPUS,NPUH,NPUV,NPUHV,KHV
      SAVE NCOS,NCOH,NCOV
      DIMENSION TMP(MXPU)

C Specific svdpr3
      CHARACTER(80) NAMFIL
      DIMENSION U(MR,MR),SD(NC),V(NC,NC)
      DOUBLE PRECISION S(MR,NC),VT(NC,NC)
      DOUBLE PRECISION USVT(MR,NC),VSUT(NC,MR)
      DOUBLE PRECISION UT(MR,MR),SI(NC,MR),VV(NC,NC)
      LOGICAL IDLUNI
      CHARACTER(30) TXM, TXN, TXI, TXFM

      JPU = 0
      DO I = 1, NPUS
        IF    (KHV(I) .EQ. 1) THEN
          JPU = JPU + 1
          TMP(JPU) = FPU(2,I)/HKIC
        ELSEIF(KHV(I) .EQ. 2) THEN
          JPU = JPU + 1
          TMP(JPU) = FPU(4,I)/VKIC
        ELSE                           ! KHV(I)=3: HV pickup
          JPU = JPU + 2
          TMP(JPU-1) = FPU(2,I)/HKIC
          TMP(JPU) = FPU(4,I)/VKIC
        ENDIF
      ENDDO

c      do i = 1, NPUS
c          write(*,*) ' svdpr ',i,khv(i)
c          enddo
c          read(*,*)

      IF(JPU .NE. NPUS) CALL ENDJOB(
     >'Pgm  svdpr. Problem with numbering of PU I/O',-99)

C      IF(NLMC .GT. NC) CALL ENDJOB('Pgm svdpr. Error. '
C     >//'Number of corrector families cannot exceed ',NC)


C                       write(*,*) ' svdpr 2',lsvd,nlmc,mr,nc
C               read(*,*)

      DO JP = 1, NPUS
c              write(*,*) ' svdpr ',jp,nlmc,tmp(jp),mr,nc
        AA(JP,NLMC) = TMP(JP)
c              write(*,*) ' svdpr a ',AA(JP,NLMC)
        IF(JP .GT. MR) CALL ENDJOB('Pgm svdpr. Error. '
     >     //'Number of PUs cannot exceed ',MR)
      ENDDO


C                       write(*,*) ' svdpr 3'



c      WRITE(LSVD,FMT='(1p,24(e12.4,1x),i0,1x)')
c     >(aa(jp,nlmc),jp=1,npus),nlmc

      WRITE(LSVD,FMT=TXFMT)
     >(TMP(J),J=1,NPUS),NLMC,IPASS-1,NLM,
     >TRIM(KLE(IQ(NLM)))//'['//TRIM(LABEL(NLM,1))//']',A(NLM,4)
      CALL FLUSH2(LSVD,.FALSE.)

c      write(*,*) 'svdpr. flushed in zgoubi.SVDtmp.out'
c      read(*,*)

      RETURN

      ENTRY SVDPR2(NPUHI,NPUVI,NPUHVI,KHVI)
      NPUH = NPUHI
      NPUV = NPUVI
      NPUHV = NPUHVI
      NPUS = NPUH + NPUV + NPUHV
      KHV = KHVI
      RETURN

      ENTRY SVDPR4(NCOHI,NCOVI)
      NCOH = NCOHI
      NCOV = NCOVI
      NCOS = NCOH + NCOV
      RETURN

      ENTRY SVDPR5(MRI,NCI,
     >                    AAO)
C     AAO(:,:) = AA(:,:)

           write(*,*) ' svdpr5   1'

      AAO(1:MRI,1:NCI) = AA(1:MRI,1:NCI)
           write(*,*) ' svdpr5   2'
      RETURN

C      ENTRY SVDPR3(NAMFIL)
C           write(*,*) ' svdpr3   1'
C      IF(IDLUNI(
C     >          LW)) OPEN(UNIT=LW,FILE=NAMFIL)    ! default is zgoubi.SVD.out
C      U(:,:)=0.D0   ! ZZZZZZZZ   Check this notation :,:
C      V(:,:)=0.D0
C      SD(:)=0.D0
C           write(*,*) ' svdpr3   2'
C      REWIND(LSVD)
c      DO I = 1, 8
c        READ(LSVD,*)
c      ENDDO

c      DO I=1,MR
c          READ(LSVD,FMT='(A)',END=10,ERR=10) TXT8
c          READ(TXT8,*,END=10,ERR=10) (AA(I,J),J=1,NC)
c      END DO

c      WRITE(LSVD,*) '# svdinv. A = '
c      do ii = 1, mr
c      WRITE(LSVD,FMT='(1p,24(e12.4,1x),2(i0,1x))')
c     >     (AA(ii,jj),jj=1,nc),ii,nc
c      enddo


      write(*,*) ' svdpr3   1'


c      WRITE(LSVD,*) '# Numb row = ',MR,', Numb cols = ',NC,'.  A ='
c      WRITE(LSVD,FMT='(24(1X,E14.6))') ((AA(I,J),J=1,NC),I=1,MR)

      CALL SVD(A,U,SD,V,MR,NC,'A','O')

           write(*,*) ' svdpr3   2'


       WRITE(TXM,FMT='(I0)') MR
       TXM = '(1P,'//TRIM(TXM)//'(E14.6,1X))'
       WRITE(TXN,FMT='(I0)') NC
       TXN = '(1P,'//TRIM(TXN)//'(E14.6,1X))'
       WRITE(TXI,FMT='(I0)') NC
       TXI = '('//TRIM(TXI)//'(F12.6,1X))'

      S(:,:) = 0.D0
      DO J = 1,MIN(MR,NC)
        S(J,J) = SD(J)
       ENDDO
      WRITE(LSVD,*) '# svdinv. S ='
      WRITE(LSVD,FMT=TXN) ((S(I,J),J=1,NC),I=1,MR)

      DO I=1, NC
        DO J = 1,NC
          VT(I,J) = AA(I,J)
        ENDDO
      ENDDO
C       VT = A
      WRITE(LSVD,*) '# svdinv. VT ='        ! V is an N by N orthogonal matrix. The routine returns VT, not V.
      WRITE(LSVD,FMT=TXN) ((VT(I,J),J=1,NC),I=1,NC)

      DO I=1, MR
        DO J = 1,MR
          UT(I,J) = U(J,I)
        ENDDO
      ENDDO

      DO I=1, NC
        DO J = 1,NC
          V(I,J) = VT(J,I)
        ENDDO
      ENDDO

      SI(:,:) = 0.D0
      DO J = 1,MIN(MR,NC)
        IF(SD(J).GT.1E-8) SI(J,J) = 1.D0/SD(J)
      ENDDO

      WRITE(LSVD,*) '# svdinv. UT ='
      WRITE(LSVD,FMT=TXM) ((UT(I,J),J=1,MR),I=1,MR)

      WRITE(LSVD,*) '# svdinv. INV(S) ='
      WRITE(LSVD,FMT=TXM) ((SI(I,J),J=1,MR),I=1,NC)

      USVT = MATMUL(U,S)
      USVT = MATMUL(USVT,VT)
      WRITE(LSVD,*) '# svdinv. U * S * VT (expected = A) : '
      WRITE(LSVD,FMT=TXN) ((USVT(I,J),J=1,NC),I=1,MR)
      VSUT = MATMUL(V,SI)
      VSUT = MATMUL(VSUT,UT)
      WRITE(LSVD,*) '# svdinv. V * S * UT (expected = A^-1) : '
      WRITE(LSVD,FMT=TXM) ((VSUT(I,J),J=1,MR),I=1,NC)
      WRITE(TXFM,*) MR
      WRITE(LW,FMT='(A)') '# V * S * UT = A^-1 : '
      WRITE(LW,FMT='(A,'//TRIM(TXFM)//'(I4,6X))') '#  ',(I,I=1,MR)
      WRITE(TXFM,*) MR
      TXFM = '(1P,'//TRIM(TXFM)//'(E14.6,1X),I4)'
C      TXFM = '('//TRIM(TXFM)//'(F14.6,1X),I4)'
      WRITE(LW,FMT=TXFM) ((VSUT(I,J),J=1,MR),I,I=1,NC)
      IF(MR.GE.NC) THEN
        VV = MATMUL(VSUT,USVT)
        WRITE(LSVD,FMT='(A)') '# svdinv. Expected = A^-1 * A)  = 1_nn :'
        WRITE(LSVD,FMT=TXI) ((VV(I,J),J=1,NC),I=1,NC)
        WRITE(LW  ,FMT='(A)') '# Expected = A^-1 * A)  = 1_nn : '
        WRITE(LW  ,FMT=TXI) ((VV(I,J),J=1,NC),I=1,NC)
      ENDIF

      USVT = MATMUL(U,S)
      USVT = MATMUL(USVT,VT)
      WRITE(LSVD,*) '# svdinv. U * S * VT (expected = A) : '
      WRITE(LSVD,FMT=TXN) ((USVT(I,J),J=1,NC),I=1,MR)
      VSUT = MATMUL(V,SI)
      VSUT = MATMUL(VSUT,UT)
      WRITE(LSVD,*) '# svdinv. V * S * UT (expected = A^-1) : '
      WRITE(LSVD,FMT=TXM) ((VSUT(I,J),J=1,MR),I=1,NC)
      IF(MR.LT.NC) THEN
        U = MATMUL(USVT,VSUT)
        WRITE(LSVD,FMT='(A)') '# svdinv. Expected = A^-1 * A)  = 1_mm :'
        WRITE(LSVD,FMT=TXM) ((U(I,J),J=1,MR),I=1,MR)
        WRITE(LW,FMT='(A)') '# svdinv.  Expected = A^-1 * A)  = 1_mm : '
        WRITE(LW,FMT=TXM) ((U(I,J),J=1,MR),I=1,MR)
      ENDIF

      CLOSE(LW)

      RETURN
      END


