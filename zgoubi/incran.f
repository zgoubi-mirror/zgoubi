C  ZGOUBI, a program for computing the trajectories of charged particles
C  in electric and magnetic fields
C  Copyright (C) 1988-2007  François Méot
C
C  This program is free software; you can redistribute it and/or modify
C  it under the terms of the GNU General Public License as published by
C  the Free Software Foundation; either version 2 of the License, or
C  (at your option) any later version.
C
C  This program is distributed in the hope that it will be useful,
C  but WITHOUT ANY WARRANTY; without even the implied warranty of
C  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C  GNU General Public License for more details.
C
C  You should have received a copy of the GNU General Public License
C  along with this program; if not, write to the Free Software
C  Foundation, Inc., 51 Franklin Street, Fifth Floor,
C  Boston, MA  02110-1301  USA
C
C  François Méot <fmeot@bnl.gov>
C  Brookhaven National Laboratory  
C  C-AD, Bldg 911
C  Upton, NY, 11973, USA
C  -------
      SUBROUTINE INCRAN(NRES,IDEEP,IFL,TEXT,
     >              L1A,L2A,L1B,L2B,
     >              IDA,LUNR,LBL1A,LBL2A,LBL1B,LBL2B,FINC,NINC)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      CHARACTER(*) TEXT
      PARAMETER(MXDEEP=10)
      DIMENSION LUNR(MXDEEP)
      PARAMETER (LBLSIZ=20)
      PARAMETER(MXFIL=1)
      CHARACTER(LBLSIZ) LBL1A(MXFIL,MXDEEP),LBL2A(MXFIL,MXDEEP),
     >LBL1B(MXFIL,MXDEEP),LBL2B(MXFIL,MXDEEP)
      CHARACTER(LBLSIZ) L1A,L2A,L1B,L2B
      DIMENSION NINC(MXFIL,MXDEEP)
      CHARACTER(*) FINC(MXFIL,MXDEEP)
C     --------------------------------------------------------------------------------------
C     Update LBL1A(MXFIL,MXDEEP),LBL2A(MXFIL,MXDEEP),LBL1B(MXFIL,MXDEEP),LBL2B(MXFIL,MXDEEP)
C     and inout data file numb IDA.
C     --------------------------------------------------------------------------------------
      LOGICAL STRCON, OK
      INTEGER DEBSTR, FINSTR
      LOGICAL IDLUNI, EXS, OPN
      CHARACTER(132) TXT
C      LOGICAL LBAVU(MXDEEP), LBBVU(MXDEEP), EXS
      
C TEXT is of the form    ninc(ifl,ideep) * filename[LBL1a,LBLl2a:LBL1b,LBL2b].
C Only text within range [Any_KEYWORD LABEL1=lbl1a&LABEL2=lbl2a : Any_KEYWORD LABEL1=lbl1b&LABEL2=lbl2b]
C will be included
      TXT = TEXT
      IDEEP = IDEEP + 1
      
              IF(STRCON(TXT,'*',
     >                          ISA)) THEN
                IF(NINC(IFL,IDEEP).EQ.0)
     >          READ(TXT(1:ISA-1),*) NINC(IFL,IDEEP)
                TXT = TXT(ISA+1:)
              ELSE
                NINC(IFL,IDEEP) = 1   
              ENDIF
             
              OK = STRCON(TXT,'[',
     >                             ISA)
              IF(OK) THEN
                FINC(IFL,IDEEP) = TXT(DEBSTR(TXT):ISA-1)      ! NAME OF FILE TO INCLUDE
              ELSE
                FINC(IFL,IDEEP) = TXT(DEBSTR(TXT):FINSTR(TXT))
              ENDIF

c              write(*,*) 'incran - define finc, ifl,ideep : '
c     >         ,ifl,ideep,'  **',trim(finc(ifl,ideep)),'** '
c              read(*,*)
              
              IF(OK) THEN
                OK = STRCON(TXT,']',
     >                                     ISB)
                IF(.NOT. OK) CALL ENDJOB
     >          ('Sbr incran. Keyword INCLUDE. Formatting error '//
     >          ' in INCLUDE[lbl1a,lbl2a:lbl1b,lbl2b] (missing '//
     >          ' closing square braket ?)',-99 )

                OK = STRCON(TXT,':',
     >                               ISC)
                IF(.NOT. OK) CALL ENDJOB
     >          ('Sbr incran. Keyword INCLUDE. Formatting error '//
     >          ' in INCLUDE[lbl1a,lbl2a:lbl1b,lbl2b] (missing '//
     >          ' ":" ?)',-99 )

                OK = STRCON(TXT(1:ISC-1),',',
     >                                        ISAC)

                IF(OK) THEN

                  IF(ISA+1 .LE. ISAC-1) THEN
C TXT is of the form FILENAME[,lbll2a: NOT YET KNOWN]
                    L1A = TXT(ISA+1:ISAC-1)
                  ELSE
                    L1A = '*'
                  ENDIF
                 
                  IF(ISAC+1 .LE. ISC-1) THEN
                    L2A = TXT(ISAC+1:ISC-1)
                  ELSE
                    L2A = '*'
                  ENDIF

                ELSE

                  IF(ISA+1 .LE. ISC-1) THEN
                    L1A = TXT(ISA+1:ISC-1)
                  ELSE
                    L1A = '*'
                  ENDIF
                  L2A = '*'
                  
                ENDIF

                OK = STRCON(TXT(ISC+1:ISB-1),',',
     >                                            ISCB)
                ISCB = ISCB + ISC

                IF(OK) THEN

                  IF(ISC+1 .LE. ISCB-1) THEN
                    L1B = TXT(ISC+1:ISCB-1)
                  ELSE
                    L1B = '*'
                  ENDIF
                 
                  IF(ISCB+1 .LE. ISC-1) THEN
                    L2B = TXT(ISCB+1:ISC-1)
                  ELSE
                    L2B = '*'
                  ENDIF

                ELSE

c              write(*,*) ' incran ISC+1:ISB-1 ',ISC+1,ISB-1,ifl,ideep
                   
                  IF(ISC+1 .LE. ISB-1) THEN
                    L1B = TXT(ISC+1:ISB-1)
                  ELSE
                    L1B = '*'
                  ENDIF
                  L2B = '*'
                  
                ENDIF

              ELSE

                L1A = '*' ; L2A = '*' ; L1B = '*' ; L2B = '*'

              ENDIF

              LBL1A(IFL,IDEEP) = L1A ; LBL2A(IFL,IDEEP) = L2A
              LBL1B(IFL,IDEEP) = L1B ; LBL2B(IFL,IDEEP) = L2B

c              write(*,*) ifl,ideep,  LBL1A(IFL,IDEEP), LBL2A(IFL,IDEEP),
c     >           LBL1B(IFL,IDEEP),   LBL2B(IFL,IDEEP)

              
            INQUIRE(FILE=FINC(IFL,IDEEP),EXIST=EXS,OPENED=OPN)
            IF(.NOT. EXS) THEN
              WRITE(ABS(NRES),*)'Pgm incran, keyword INCLUDE :  '//
     >        'could not include file # ',IFL,
     >        ' :  ''',TRIM(FINC(ifl,ideep))//''' '
              WRITE(6,*)'Pgm incran, keyword INCLUDE :  '//
     >        'could not include file # ',IFL,
     >        ' :  ''',TRIM(FINC(ifl,ideep))//''' '
              CALL ENDJOB('does it exist ? ',-99)
            ENDIF
           
            IF(.NOT. OPN) THEN
              IDA = IDA + 1
              OK = IDLUNI(
     >                    LUNR(IDA))
              OPEN(UNIT=LUNR(IDA),FILE=FINC(IFL,IDEEP))

c              write(*,*)'incan @open ida, lunr, ifl ideep :',
c     >         ida, lunr(ida), ifl,ideep,
c     >         FINC(ifl,ideep)
c         read(*,*)
         
            ELSE
C              REWIND(LUNR(IDA))
            ENDIF

            TEXT = '! '//TEXT(DEBSTR(TEXT):FINSTR(TEXT))
                        
C            write(*,*) ' '
c            write(*,*) 'incran - text ',trim(text)
c            write(*,*) 'incran - IFL,IDEEP :',IFL,IDEEP
c            write(*,*) ' '
c            read(*,*)

            NINC(IFL,IDEEP) = NINC(IFL,IDEEP) - 1
      
      RETURN
      END
            
