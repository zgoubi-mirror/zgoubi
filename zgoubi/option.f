C  ZGOUBI, a program for computing the trajectories of charged particles
C  in electric and magnetic fields
C  Copyright (C) 1988-2007  François Méot
C
C  This program is free software; you can redistribute it and/or modify
C  it under the terms of the GNU General Public License as published by
C  the Free Software Foundation; either version 2 of the License, or
C  (at your option) any later version.
C
C  This program is distributed in the hope that it will be useful,
C  but WITHOUT ANY WARRANTY; without even the implied warranty of
C  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C  GNU General Public License for more details.
C
C  You should have received a copy of the GNU General Public License
C  along with this program; if not, write to the Free Software
C  Foundation, Inc., 51 Franklin Street, Fifth Floor,
C  Boston, MA  02110-1301  USA
C
C  François Méot <fmeot@bnl.gov>
C  Brookhaven National Laboratory
C  C-AD, Bldg 911
C  Upton, NY, 11973
C  -------
      SUBROUTINE OPTION
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE "C.CDF.H"     ! COMMON/CDF/ IES,LF,LST,NDAT,NRES,NPLT,NFAI,NMAP,NSPN,NLOG
      INCLUDE 'MXLD.H'
      INCLUDE "C.DON.H"     ! COMMON/DON/ A(MXL,MXD),IQ(MXL),IP(MXL),NB,NOEL
C      PARAMETER (LNTA=132) ; CHARACTER(LNTA) TA
C      PARAMETER (MXTA=45)
      INCLUDE "C.DONT.H"     ! COMMON/DONT/ TA(MXL,MXTA)
      CHARACTER(40) TXTA, TXTB, TXTC
      INTEGER DEBSTR, FINSTR
      SAVE NRSAV
      SAVE KWROFF
      LOGICAL FITING
      LOGICAL CONSTY
      LOGICAL AGSMDL

C===================
C Vincent special. BNL, June 2019
      PARAMETER (LBLSIZ=20)
      PARAMETER (NBLB=3)
      CHARACTER(LBLSIZ) LBLIST(NBLB), LBLISO(NBLB)
C===================

      LOGICAL OK, IDLUN2

      SAVE ILI
      
      DATA NRSAV / -11111 /
      DATA KWROFF /  0 /
      DATA CONSTY / .FALSE. /
      DATA AGSMDL / .FALSE. /
      DATA ILI / -1 /
      
C Numb of options. NBOP lines should follow
      NY = NINT(A(NOEL,1))
      NBOP = NINT(A(NOEL,2))

      IF(NY*NBOP.EQ.0) THEN
        IF(NRSAV .EQ. -11111) THEN
          IF(NRES.GT.0) WRITE(ABS(NRES),FMT='(/,T25,A)')
     >    ' ''OPTIONS''  is  inhibited,  no  option  will  be  set.'
        ENDIF
        GOTO 99
      ENDIF

      IF(NBOP.GT.40)
     >CALL ENDJOB('SBR option : nmbr of options exceded ; max is ',40)

      IF(NRSAV .EQ. -11111) THEN
        IF(NRES.GT.0) WRITE(ABS(NRES),FMT='(T10,A,I0,A,/)')
     >  'A list of ',NBOP,' option(s) is expected.  '//
     >  'List and actions taken are as follows :'
      ENDIF

      DO I = 1, NBOP

        READ(TA(NOEL,I),*,ERR=88,END=88) TXTA
      
        IF(NRES.GT.0) THEN
          IF(NRSAV .EQ. -11111) WRITE(ABS(NRES),FMT='(/,T5,A,I2,2A)')
     >    'Option # ',I,' : ',
     >    TA(NOEL,I)(DEBSTR(TA(NOEL,I)):FINSTR(TA(NOEL,I)))
        ENDIF

        IF(TXTA(DEBSTR(TXTA):FINSTR(TXTA)) .EQ. 'WRITE') THEN

          READ(TA(NOEL,I),*) TXTA, TXTB

          IF    (TXTB(DEBSTR(TXTB):FINSTR(TXTB)) .EQ. 'OFF') THEN
            IF(NRSAV .EQ. -11111) THEN
              IF(NRES.GT.0) THEN
                WRITE(ABS(NRES),FMT='(/,T5,A)')
     >          'WRITE OFF -> A lot of (almost all) '//
     >          'WRITE statements will be inhibited !'
                WRITE(ABS(NRES),FMT='(/,132(''*''))')
              ENDIF
            ENDIF
            KWROFF = 1
            NRSAV = NRES
            NRES = -ABS(NRES)

          ELSEIF(TXTB(DEBSTR(TXTB):FINSTR(TXTB)) .EQ. 'ON') THEN
            CALL FITSTA(5,
     >                  FITING)
            IF(.NOT. FITING) THEN
C Yann, 14-03-07. NRES>0 here is necessary for the online model to work
              CALL REBEL1(
     >                   KWRT)
              CALL ZGAGSM(
     >                   AGSMDL)

c             IF(KWRT.NE.0 .OR. AGSMDL) THEN
                KWROFF = 0
                NRSAV = NRES
                NRES = ABS(NRES)
                WRITE(NRES,FMT='(/,T5,A)') 'WRITE ON -> '//
     >         '''WRITE'' bit in ''OPTIONS'' set to 1.'

c             ELSE
c             ENDIF

            ENDIF
          ELSE
            CALL ENDJOB('Pgm option. No such option'//
     >      TXTB(DEBSTR(TXTB):FINSTR(TXTB)),-99)
          ENDIF

        ELSEIF(TXTA(DEBSTR(TXTA):FINSTR(TXTA)) .EQ. 'CONSTY') THEN
 
          READ(TA(NOEL,I),*) TXTA, TXTB

          CONSTY = TXTB(DEBSTR(TXTB):FINSTR(TXTB)) .EQ. 'ON'
          CALL INTEGA(CONSTY)
          CALL TRANS2(CONSTY)

          IF(ABS(NRES).GT.0) THEN
            NRES = ABS(NRES)
            WRITE(ABS(NRES),FMT='(5X,T5,A)') '- rays will be forced to'
     >      //' constant Y and Z in pgm integr.'
            WRITE(ABS(NRES),FMT='(5X,T5,A)') '- mid-plane symmetry'
     >      //' test in pgm transf (i.e., ''dejaca'' procedure) '
     >      //'is inhibited.'
            WRITE(ABS(NRES),FMT='(5X,T5,A)') '- coordinates and field '
     >      //'may be checked using IL = 1, 2 OR 7 '
c     >      //'will be stored in file zgoubi.consty.out '
          ENDIF

        ELSEIF(TXTA(DEBSTR(TXTA):FINSTR(TXTA)) .EQ. '.plt') THEN
 
          READ(TA(NOEL,I),*) TXTA, TXTB
          READ(TXTB,*,ERR=98,END=98) ILI
          
          IF(ABS(NRES).GT.0) THEN
C            NRES = ABS(NRES)
            IF(ILI .EQ. -1) THEN  
              WRITE(ABS(NRES),FMT='(/,5X,A,2X,I0,A)')
     >        'IL=-1 :  this  option  is  not  active,  ignored.'
            ELSE
             WRITE(ABS(NRES),FMT='(/,5X,A,2X,I0,A)')'- IL is forced to'
     >       ,ILI,' in all optical elements. Output file is: (0) none;'
     >       //' (1) zgoubi.res; (2) zgoubi.plt; (7) zgoubi.impdev.out.'
            ENDIF  
          ENDIF

        ELSEIF(TXTA(DEBSTR(TXTA):FINSTR(TXTA)) .EQ. 'AGSQUADS') THEN

          READ(TA(NOEL,I),*) TXTA

          CALL AGSQU1(TRIM(TXTA) .EQ. 'ON',
     >                                    LBLISO)
          LBLIST = LBLISO

          IF(ABS(NRES).GT.0) THEN
            NRES = ABS(NRES)
            WRITE(ABS(NRES),FMT='(5X,T5,A)') 'AGS, winding currents '
     >      //' in various quads are modified in agsquf.f.'
            WRITE(ABS(NRES),FMT='(10X,A)') 'Concerns quads with'
     >      //' the following labels1/2 : '
            WRITE(ABS(NRES),FMT='(15X,I4,3X,A)')
     >           (IL, LBLIST(IL),IL=1,NBLB)

          ENDIF

        ELSEIF(TXTA(DEBSTR(TXTA):FINSTR(TXTA)) .EQ. 'MONITOR') THEN

          READ(TA(NOEL,I),*) TXTA, TXTB, TXTC
      
          IF(TXTB(DEBSTR(TXTB):FINSTR(TXTB)) .EQ. 'IDLUNI')
     >    OK = IDLUN2(TXTC)           ! TXTC='ON' will monitor
           
         ENDIF
      
      ENDDO

 99   RETURN

 98   CONTINUE
      CALL ENDJOB('SBR option, input data error.',-99)
      RETURN
      
      ENTRY OPTIO1(
     >             KWROFO)
      KWROFO = KWROFF
      RETURN
      
      ENTRY OPTIO3(
     >             ILIO)
      ILIO = ILI
      RETURN

      
 88   RETURN
      END


