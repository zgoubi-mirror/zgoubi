C  ZGOUBI, a program for computing the trajectories of charged particles
C  in electric and magnetic fields
C  Copyright (C) 1988-2007  François Méot
C
C  This program is free software; you can redistribute it and/or modify
C  it under the terms of the GNU General Public License as published by
C  the Free Software Foundation; either version 2 of the License, or
C  (at your option) any later version.
C
C  This program is distributed in the hope that it will be useful,
C  but WITHOUT ANY WARRANTY; without even the implied warranty of
C  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C  GNU General Public License for more details.
C
C  You should have received a copy of the GNU General Public License
C  along with this program; if not, write to the Free Software
C  Foundation, Inc., 51 Franklin Street, Fifth Floor,
C  Boston, MA  02110-1301  USA
C
C  François Méot <fmeot@bnl.gov>
C  Brookhaven National Laboratory
C  C-AD, Bldg 911
C  Upton, NY, 11973, USA
C  -------
      SUBROUTINE RESET(IP,IRST)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE "C.CDF.H"     ! COMMON/CDF/ IES,LF,LST,NDAT,NRES,NPLT,NFAI,NMAP,NSPN,NLOG
      INCLUDE "MAXTRA.H"
      INCLUDE "C.CHAMBR.H"     ! COMMON/CHAMBR/ LIMIT,IFORM,YLIM2,ZLIM2,SORT(MXT),FMAG,YCH,ZCH

      PARAMETER (MXPUD=9,MXPU=1000)
       INCLUDE "C.CO.H"     ! COMMON/CO/ FPU(MXPUD,MXPU),KCO,NPUF,NFPU,IPU
      INCLUDE "C.DESIN.H"     ! COMMON/DESIN/ FDES(7,MXT),IFDES,KINFO,IRSAR,IRTET,IRPHI,NDES
C     >,AMS,AMP,AM3,TDVM,TETPHI(2,MXT)
      INCLUDE "MAXCOO.H"
      LOGICAL AMQLU(5),PABSLU
      INCLUDE "C.FAISC.H"     ! COMMON/FAISC/ F(MXJ,MXT),AMQ(5,MXT),DP0(MXT),IMAX,IEX(MXT),
C     $     IREP(MXT),AMQLU,PABSLU
      INCLUDE "C.HISTO.H"     ! COMMON/HISTO/ ICTOT(JH,KH),MOYC(JH,KH) ,CMOY(JH,KH),JMAX(JH,KH)
      INCLUDE "C.HISTOG.H"     ! COMMON/HISTOG/NC(JH,120,KH),NH,XMI(JH,KH),XMO(JH,KH),XMA(JH,KH)
      INCLUDE "C.INTEG.H"     ! COMMON/INTEG/ PAS,DXI,XLIM,XCE,YCE,ALE,XCS,YCS,ALS,KP
      PARAMETER (LBLSIZ=20)
      INCLUDE 'MXLD.H'
      CHARACTER(LBLSIZ) LABEL
      INCLUDE "C.LABEL.H"     ! COMMON/LABEL/ LABEL(MXL,2)
      INCLUDE "C.OBJET.H"     ! COMMON/OBJET/ FO(MXJ,MXT),KOBJ,IDMAX,IMAXT,KZOB
C      LOGICAL ZSYM
      INCLUDE "C.TYPFLD.H"     ! COMMON/TYPFLD/ KFLD,MG,LC,ML,ZSYM
      INCLUDE "C.ORDRES.H"     ! COMMON/ORDRES/ KORD,IRD,IDS,IDB,IDE,IDZ
      INCLUDE "C.REBELO.H"   ! COMMON/REBELO/ NRBLT,IPASS,KWRT,NNDES,STDVM
      INCLUDE 'MXFS.H'
      INCLUDE 'MXSCL.H'
      INCLUDE "C.SCAL.H"     ! COMMON/SCAL/ SCL(MXF,MXS,MXSCL),TIM(MXF,MXS),NTIM(MXF),KSCL
      PARAMETER (KSIZ=10)
      CHARACTER(KSIZ) FAM
      CHARACTER(LBLSIZ) LBF
      INCLUDE "C.SCALT.H"     ! COMMON/SCALT/ FAM(MXF),LBF(MXF,MLF)
      INCLUDE "C.SPIN.H"     ! COMMON/SPIN/ KSPN,KSO,SI(4,MXT),SF(4,MXT)

      DO I=1,MXF
        FAM(I) = ' '
        DO J=1,MLF
          LBF(I,J) = ' '
        ENDDO
      ENDDO
      IF(IP.EQ.1) WRITE(NRES,FMT='
     >(10X,''FAM and LBF reset to void'')'')')

C----- Pick-up signal calculation switched off
      IF(KCO .EQ. 1) KCO = 0    ! KCO=2 and has to stay so if SVD
      IF(IP.EQ.1) WRITE(NRES,FMT='
     >(10X,''Pick-up signal calculation switched off'')')

C     ....  Defaults: ORDRE=2 when KALC=3;
C                     fields have z-symmetry when KALC=1
      KORD=2
      ZSYM=.TRUE.
      IF(IP.EQ.1) WRITE(NRES,FMT='
     >(10X,''KORD=2; ZSYM=.TRUE.'')')

C     ....  REBELOTE
      KOBJ = 0
      IF(IRST.EQ.1) THEN
        IF(IP.EQ.1) WRITE(NRES,FMT='
     >  (10X,''IPASS  reset  to  1  (was  '',I0,'')'')')
     >  IPASS
        IPASS = 1
      ENDIF
C     .... COMPTEUR DE PASSAGE PAR 'REBELOTE'
      NRBLT = 0
      IF(IP.EQ.1) WRITE(NRES,FMT='
     >(10X,''COMPTEUR DE PASSAGE PAR REBELOTE NRBLT = 0'')')

C----- Decay simulation switched off
      IFDES = 0
C     ... COMPTEUR DE DESINTEGRATION
      NDES = 0
C     ... COORD AU POINT DE DESINTEGRATION
      FDES(1:6,1:MXT) = 0.D0
      IF(IP.EQ.1) WRITE(NRES,FMT='
     >(10X,''Decay simulation switched off'')')      

C     ... TEMPS DE VOL AU POINT DE SORTIE
      SORT(1:MXT) = 0.D0
      IF(IP.EQ.1) WRITE(NRES,FMT='
     >(10X,''TEMPS DE VOL AU POINT DE SORTIE RAZ'')')

C       ... RAZ LES HISTOS
        NC(1:JH,1:120,1:KH) = 0
        JMAX(1:JH,1:KH) = 0
      IF(IP.EQ.1) WRITE(NRES,FMT='
     >(10X,''RAZ LES HISTOS'')')
        
C       ... RESET LES SOMMES
        XMO(1:JH,1:KH) = 0.D0
        DO K=1,KH
          DO J=1,JH
            XMI(J,K)=1.D10
            XMA(J,K)=-1.D10
          ENDDO
        ENDDO
      IF(IP.EQ.1) WRITE(NRES,FMT='
     >(10X,''RESET LES SOMMES'')')

C     ....  SPIN TRACKING
      KSPN = 0
      IF(IP.EQ.1) WRITE(NRES,FMT='
     >(10X,''SPIN TRACKING off'')')

C     .... SCALING
      KSCL=0
      NTIM(1:MXF) = 0
      IF(IP.EQ.1) WRITE(NRES,FMT='
     >(10X,''SCALING off'')')      

      F (1:MXJ,1:MXT) = 0.D0
      FO(1:MXJ,1:MXT) = 0.D0
      IF(IP.EQ.1) WRITE(NRES,FMT='
     >(10X,''RAZ FAISCEAU'')')

      IF(IP.EQ.1) WRITE(NRES,FMT='
     >(10X,''and more (see routine reset.f) ...'')')
      
      ENTRY RESET2
C     ... # part. rejected in SBR INTEG
C     ... # part. out of acceptance (collimator, chamber)
C      NRJ = 0
C      NOUT = 0
      CALL CNTRST

C     .... OPTION CHAMBRE => LIMIT=1
      LIMIT=0

C     .... FIELD MAP FILE NAMES
      CALL TOSCA1

C Field map counter reset to 0
      CALL KSMAP0

C-----  structure length ------------
      CALL SCUMS(ZERO)
      CALL SCUMT(ZERO)
C------------------------------------------------

C SR loss -----------------------
      CALL RAYSY4(IMAX)

      RETURN
      END
