C  ZGOUBI, a program for computing the trajectories of charged particles
C  in electric and magnetic fields
C  Copyright (C) 1988-2007  François Méot
C
C  This program is free software; you can redistribute it and/or modify
C  it under the terms of the GNU General Public License as published by
C  the Free Software Foundation; either version 2 of the License, or
C  (at your option) any later version.
C
C  This program is distributed in the hope that it will be useful,
C  but WITHOUT ANY WARRANTY; without even the implied warranty of
C  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C  GNU General Public License for more details.
C
C  You should have received a copy of the GNU General Public License
C  along with this program; if not, write to the Free Software
C  Foundation, Inc., 51 Franklin Street, Fifth Floor,
C  Boston, MA  02110-1301  USA
C
C  François Méot <fmeot@bnl.gov>
C  Brookhaven National Laboratory
C  C-AD, Bldg 911
C  Upton, NY, 11973, USA
C  -------
      FUNCTION IDLUNI(
     >                LN)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL IDLUNI

      LOGICAL OPN
      SAVE II, IMA
      LOGICAL FIRST, OPND, MONITOR, IDLUN2

      CHARACTER(3) ONOF
            
      SAVE FIRST, OPND, MONITOR, LW
      INTEGER DEBSTR, FINSTR
      
      DATA II / 20 /  
      DATA IMA / 9999 /
      DATA FIRST, OPND, MONITOR / .TRUE., .FALSE. , .FALSE. /

      IDLUNI =.FALSE.
      
      I = II
 1    CONTINUE
        INQUIRE(UNIT=I,ERR=90,IOSTAT=IOS,OPENED=OPN)
        I = I+1
        IF(I .GT. IMA) GOTO 91
      IF(OPN) GOTO 1
C      IF(IOS .GT. 0) GOTO 1

      LN = I-1
      II = II + 1
      IDLUNI = .TRUE.
      GOTO 99

 90   CONTINUE
      LN = 0
      IDLUNI = .FALSE.
      WRITE(NRES,*) ' '
      WRITE(NRES,*) 'Pgm idluni. Error status in OPEN. IDLUNI = '
     >,IDLUNI
      WRITE(NRES,*) ' '
      WRITE(*,*) ' '
      WRITE(*,*) 'Pgm idluni. Error status in OPEN. IDLUNI = '
     >,IDLUNI
      WRITE(*,*) ' '
      GOTO 99

 91   CONTINUE
      LN = 0
      IDLUNI = .FALSE.
      WRITE(NRES,*) ' '
      WRITE(NRES,*) 'Pgm idluni. Exceeded max. unit number  (',
     >IMA,').  ',IDLUNI
      WRITE(NRES,*) ' '
      WRITE(*,*) ' '
      WRITE(*,*) 'Pgm idluni. Exceeded max. unit number  (',
     >IMA,').  ',IDLUNI
      WRITE(*,*) ' '

 99   CONTINUE

      IF(MONITOR) THEN
        IF(FIRST) THEN
          FIRST = .FALSE.
          IF(.NOT. OPND) THEN
            LW = II
            II = II+1
            OPEN(UNIT=LW,FILE='zgoubi.IDLUNI.out')
            OPND = .TRUE.
            WRITE(LW,*) '   opened unit # ',LW,'  Monitor_idluni'
          ENDIF
        ENDIF
        IF(OPND) THEN
           WRITE(LW,*) '   opened unit # ',LN
C           WRITE(*,*) '   opened unit # ',LN
C           READ(*,*)
        ENDIF
      ENDIF
      
      RETURN

      ENTRY IDLUN2(ONOF)
      IDLUN2 = ONOF(DEBSTR(ONOF):FINSTR(ONOF)) .EQ. 'ON'
      MONITOR = IDLUN2
      write(NRES,*) ' Monitor idluni ', ONOF(DEBSTR(ONOF):FINSTR(ONOF))
c     >     ,monitor
c      read(*,*)
      
      RETURN
      
      END
