C  ZGOUBI, a program for computing the trajectories of charged particles
C  in electric and magnetic fields
C  Copyright (C) 1988-2007  François Méot
C
C  This program is free software; you can redistribute it and/or modify
C  it under the terms of the GNU General Public License as published by
C  the Free Software Foundation; either version 2 of the License, or
C  (at your option) any later version.
C
C  This program is distributed in the hope that it will be useful,
C  but WITHOUT ANY WARRANTY; without even the implied warranty of
C  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C  GNU General Public License for more details.
C
C  You should have received a copy of the GNU General Public License
C  along with this program; if not, write to the Free Software
C  Foundation, Inc., 51 Franklin Street, Fifth Floor,
C  Boston, MA  02110-1301  USA
C
C  François Méot <fmeot@bnl.gov>
C  Brookhaven National Laboratory
C  C-AD, Bldg 911
C  Upton, NY, 11973
C  USA
C  -------
      SUBROUTINE RTWISS(NDAT,
     >                       KTW)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'MXLD.H'
      INCLUDE "C.DON.H"     ! COMMON/DON/ A(MXL,MXD),IQ(MXL),IP(MXL),NB,NOEL
C      PARAMETER (MXTA=45) ; PARAMETER (LNTA=132) ; CHARACTER(LNTA) TA
      INCLUDE "C.DONT.H"     ! COMMON/DONT/ TA(MXL,MXTA)

      CHARACTER(132) TXT
      LOGICAL STRCON
      PARAMETER (KSIZ=10)
      CHARACTER(KSIZ) KLE

c           write(*,*) ' rtwiss ndat ',ndat
      
      LINE = 1
      READ(NDAT,FMT='(A)',END=90,ERR=90) TXT
      IF(STRCON(TXT,'!',
     >                  IS)) TXT = TXT(1:IS-1)

      TA(NOEL,1) = ''
      IF(STRCON(TXT,'coupled',
     >                        IS)) THEN
        TA(NOEL,1) = 'coupled'
      ELSE
        TA(NOEL,1) = 'uncoupled'
      ENDIF
      IF(STRCON(TXT,'SRINT',
     >                      IS)) 
     >TA(NOEL,1) = TRIM(TA(NOEL,1))//' '//'SRINT'
      IF(STRCON(TXT,'DKINT',
     >                      IS)) 
     >TA(NOEL,1) = TRIM(TA(NOEL,1))//' '//'DKINT'
      
      READ(TXT,*,END=90,ERR=90) A(NOEL,1),A(NOEL,2),A(NOEL,3)
      KTW = NINT(A(NOEL,1))

      IF(KTW.GT.2) CALL ENDJOB('Sbr rtwiss. Option KTW=3 is '
     >//'under development. Try later. ',-99)
      
      RETURN

 90   CONTINUE
      CALL ZGKLEY(
     >            KLE)
      CALL ENDJOB('*** Pgm rtwiss, keyword '//KLE//' : '//
     >'input data error, at line #',LINE)
      RETURN
      END
