C  ZGOUBI, a program for computing the trajectories of charged particles
C  in electric and magnetic fields
C  Copyright (C) 1988-2007  François Méot
C
C  This program is free software; you can redistribute it and/or modify
C  it under the terms of the GNU General Public License as published by
C  the Free Software Foundation; either version 2 of the License, or
C  (at your option) any later version.
C
C  This program is distributed in the hope that it will be useful,
C  but WITHOUT ANY WARRANTY; without even the implied warranty of
C  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C  GNU General Public License for more details.
C
C  You should have received a copy of the GNU General Public License
C  along with this program; if not, write to the Free Software
C  Foundation, Inc., 51 Franklin Street, Fifth Floor,
C  Boston, MA  02110-1301  USA
C
C  François Méot <fmeot@bnl.gov>
C  Brookhaven National Laboratory  
C  C-AD, Bldg 911
C  Upton, NY, 11973, USA
C  -------
      SUBROUTINE LBLGET(NRES,TEXT,FINC,IFL,
     >          IDEEP,L1A,L2A,L1B,L2B,LBL1A,LBL2A,LBL1B,LBL2B,
     >          IDA,LUNR,LABEL1,LABEL2,LBAVU,LBBVU)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      CHARACTER(*) TEXT
      PARAMETER(MXDEEP=10) ! Max. file depth is 10 (depth 1 is initial data file) 
      PARAMETER(MXFIL=1)
      CHARACTER(*) FINC(MXFIL,MXDEEP)
      CHARACTER(*) L1A,L2A,L1B,L2B
      INCLUDE 'MXLD.H'
      PARAMETER (LBLSIZ=20)
      CHARACTER(LBLSIZ) LABEL1, LABEL2
      DIMENSION LUNR(MXDEEP)
      LOGICAL LBAVU(MXDEEP),LBBVU(MXDEEP)
      CHARACTER(LBLSIZ) LBL1A(MXFIL,MXDEEP),LBL2A(MXFIL,MXDEEP),
     >LBL1B(MXFIL,MXDEEP),LBL2B(MXFIL,MXDEEP)
C     -----------------------------------------------------------
C     Extract LABEL1 and LABEL2 from TEXT, determine lbavu, lbbvu
C     -----------------------------------------------------------
      PARAMETER (MXSTR=3)
      CHARACTER(LBLSIZ) STRA(MXSTR)
C      CHARACTER(110) TXT110
      LOGICAL EMPTY
      INTEGER DEBSTR, FINSTR
      PARAMETER (I6=6)
C      CHARACTER(I6) TXT6
      CHARACTER(132) TXT
      LOGICAL STRCON
      PARAMETER (I104=104)

      DATA STRA / MXSTR* ' ' /
      
      TXT = TEXT
      IF(STRCON(TXT,'!',
     >                  IS)) TXT=TXT(DEBSTR(TXT):IS-1)
      
C Get labels of the current keyword      
      LABEL1 = ' '
      LABEL2 = ' '
      IF( .NOT. EMPTY(TXT) ) THEN ! TXT is a keyword line, full line as read from .dat         
C     stra(1) is expected to be a KEYWORD, stra(1) and stra(2) its label1, label2
        CALL STRGET(TXT,MXSTR,
     >                        NST,STRA)

c              write(*,*) ' nst, stra 1-nst : ',(stra(i),i=1,nst)
c              read(*,*)
              
        IF(NST.GE.2) THEN
c          IF(.NOT. EMPTY(STRA(2))) THEN
            LABEL1 = STRA(2)
c          ELSE
c            LABEL1 = ' '
c          ENDIF
          IF(NST.GE.3) THEN
c            IF(.NOT. EMPTY(STRA(3))) THEN
              LABEL2 = STRA(3)
c            ELSE
c              LABEL2 = ' '
c            ENDIF
          ENDIF
        ELSE
          LABEL1 = ' ' ; LABEL2 = ' '
        ENDIF
c      ELSE
c        LABEL1 = ' ' ; LABEL2 = ' '
      ENDIF

C Any keyword may indicate start or end of INCLUDE segment. This is managed here:
      
C     Detemrine whether this keyword is within readable range (in case read(ndat) is not parent file,
C      that range is reduced)
      IF(LBAVU(IDEEP)) THEN        ! Within include segment to be read
C Always T if no INCLUDE met, 
C otherwise:  LableA, i.e. start of Include, is known already; expecting labelB, i.e. end of INCLUDE section
        LBBVU(IDEEP) = ( L1B .EQ. '*' .AND.                       ! Always F if no INCLUDE met
     >  (L2B .EQ. '*' .OR. L2B .EQ. LABEL2) )
     >        .OR.
     >  ( L1B .EQ. LABEL1 .AND.
     >  (L2B .EQ. '*' .OR. L2B .EQ. LABEL2) )

        IF(LBBVU(IDEEP)) THEN
C LableB, = end of Include section, just met

          WRITE(NRES,FMT='(A,T70,A,2(1X,i0),A)') 
     >    '! Include_SegmentEnd : '//FINC(IFL,IDEEP)(
     >    DEBSTR(FINC(IFL,IDEEP)):FINSTR(FINC(IFL,IDEEP)))
     >    ,'(had n_inc, depth : ',ifl,ideep,')'

c       write(*,fmt='(t2,A,4i6,4a)') 'lblget -  ! Include_SegmentEnd '
c     >         // ' ifl,ideep, ida, lunR : ',
c     >   ifl,ideep,IDA ,lunr(ida), l1a,l1b,l2a,l2b
C           write(*,*) 
           
          CLOSE(LUNR(IDA))
          IDA = IDA -1
          IDEEP = IDEEP -1
          
C Reset to lower depth value
          l1a=LBL1A(IFL,IDEEP)
          l2a=LBL2A(IFL,IDEEP)
          l1b=LBL1B(IFL,IDEEP)
          l2b=LBL2B(IFL,IDEEP)

c       write(*,fmt='(t2,A,4i6,4a)') 'lblget -  ! Include_SegmentEnd.'
c     >         // ' Updated ifl,ideep, ida, lunR : ',
c     >   ifl,ideep,IDA ,lunr(ida), l1a,l1b,l2a,l2b
c          write(*,*) ' lblget - @close ida, lunr, ifl, ideep ',
c     >         ida, lunr(ida), ifl, ideep ,finc(ifl,ideep)
c          read(*,*)
c     write(*,*)
          

c       write(*,fmt='(t2,A,3i6,4a)') 'lblget -  ! Include_SegmentEnd '
c     >     // ' ifl,ideep, ida : ',ifl,ideep,IDA ,l1a,l1b,l2a,l2b
c           write(*,*) 
c           write(*,*) 
c           write(*,*) 
c       read(*,*)
          
        ENDIF        ! LBBVU(IDEEP)
        
      ELSE  ! not LBAVU(IDEEP)
                
        LBAVU(IDEEP) = ( L1A .EQ. '*' .AND. 
     >  (L2A .EQ. '*' .OR. L2A .EQ. LABEL2) )
     >      .OR.
     >  ( L1A .EQ. LABEL1 .AND.
     >  (L2A .EQ. '*' .OR. L2A .EQ. LABEL2) )

        IF(LBAVU(IDEEP)) THEN
C     LableA, = start of Include section, just met

            IF(IDEEP .GT. 1) THEN
              WRITE(NRES,FMT='(A,T70,A,2(1X,I0),A)') 
     >        '! Include_SegmentStart : '//FINC(IFL,IDEEP)(
     >        DEBSTR(FINC(IFL,IDEEP)):FINSTR(FINC(IFL,IDEEP)))
     >        //'['//TRIM(LBL1A(IFL,IDEEP))
     >        //','//TRIM(LBL2A(IFL,IDEEP))
     >        //':'//TRIM(LBL1B(IFL,IDEEP))
     >        //','//TRIM(LBL2B(IFL,IDEEP))
     >        //']','(n_inc, depth : ',ifl,ideep,')'

c                write(*,fmt='(a,2x,2i6,1x,5a,1x,i0)')
c     >           'lblget -  ! Include_SegmentStart,  ifl, ideep : '
c     >        , ifl, ideep,l1a,l2a,l1a,l2b,' nres = ',nres
c                 read(*,*) 

              lbbvu(ideep) = .false.
              
            ENDIF

        ENDIF      ! LBAVU(IDEEP)
      ENDIF      ! LBAVU(IDEEP)
      
      RETURN
      END
