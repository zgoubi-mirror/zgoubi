C  ZGOUBI, a program for computing the trajectories of charged particles
C  in electric and magnetic fields
C  Copyright (C) 1988-2007  François Méot
C
C  This program is free software; you can redistribute it and/or modify
C  it under the terms of the GNU General Public License as published by
C  the Free Software Foundation; either version 2 of the License, or
C  (at your option) any later version.
C
C  This program is distributed in the hope that it will be useful,
C  but WITHOUT ANY WARRANTY; without even the implied warranty of
C  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C  GNU General Public License for more details.
C
C  You should have received a copy of the GNU General Public License
C  along with this program; if not, write to the Free Software
C  Foundation, Inc., 51 Franklin Street, Fifth Floor,
C  Boston, MA  02110-1301  USA
C
C  François Méot <fmeot@bnl.gov>
C  Brookhaven National Laboratory
C  C-AD, Bldg 911
C  Upton, NY, 11973,  USA
C  -------
      SUBROUTINE ROPTIC(NDAT)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'MXLD.H'
      INCLUDE "C.DON.H"     ! COMMON/DON/ A(MXL,MXD),IQ(MXL),IP(MXL),NB,NOEL
C      PARAMETER (MXTA=45) ; PARAMETER (LNTA=132) ; CHARACTER(LNTA) TA
      INCLUDE "C.DONT.H"     ! COMMON/DONT/ TA(MXL,MXTA)

      CHARACTER(LNTA) TXT
      LOGICAL STRCON
      PARAMETER (KSIZ=10)
      CHARACTER(KSIZ) KLE
      INTEGER DEBSTR, FINSTR

      PARAMETER(MLB=10)
      PARAMETER (I30=30)
      CHARACTER(I30) STRA(MLB+1)
      
      LINE = 1
      READ(NDAT,FMT='(A)',END=90,ERR=90) TXT
      TXT = TXT(DEBSTR(TXT):FINSTR(TXT))      
      IF(STRCON(TXT,'!',
     >                  IS)) TXT = TXT(1:IS-1)

      IS = FINSTR(TXT)
      IIS = IS
      IF(STRCON(TXT,'PRINT',
     >                        IS)) THEN
        TA(NOEL,1) = 'PRINT'
      ELSE
        TA(NOEL,1) = ' '
      ENDIF
      IF(STRCON(TXT,'coupled',
     >                        IIS)) THEN
        TA(NOEL,2) = 'coupled'
      ELSE
        TA(NOEL,2) = ' '
      ENDIF

      TXT = TXT(1:MIN(IS,IIS)-1)
      CALL STRGET(TXT,MLB+1
     >                     ,MSTR,STRA)
      IF(MSTR .GT. MLB+1) CALL ENDJOB('*** Pgm zgoubi, keyword '
     >//'OPTICS :  too many labels, max. is  ',MLB)
      READ(STRA(1),*,END=90,ERR=90) XOPT
      A(NOEL,1) = XOPT
      IF( STRCON(STRA(1),'.',
     >                       IS)) THEN
        IF(FINSTR(STRA(1)) .GT. IS) THEN
          READ(STRA(1)(IS+1:FINSTR(STRA(1))),*) NLBL
        ELSE
          NLBL = 1
        ENDIF
      ELSE
        NLBL = 1
      ENDIF
      A(NOEL,2) = NLBL
      
      DO I = 1, NLBL
        TA(NOEL,2+I) = TRIM(STRA(I+1))
      ENDDO

      RETURN

 90   CONTINUE
      CALL ZGKLEY(
     >            KLE)
      CALL ENDJOB('*** Pgm roptic, keyword '//KLE//' : '//
     >'input data error, at line #',LINE)
      RETURN
      END
