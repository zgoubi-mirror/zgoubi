C  ZGOUBI, a program for computing the trajectories of charged particles
C  in electric and magnetic fields
C  Copyright (C) 1988-2007  François Méot
C
C  This program is free software; you can redistribute it and/or modify
C  it under the terms of the GNU General Public License as published by
C  the Free Software Foundation; either version 2 of the License, or
C  (at your option) any later version.
C
C  This program is distributed in the hope that it will be useful,
C  but WITHOUT ANY WARRANTY; without even the implied warranty of
C  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C  GNU General Public License for more details.
C
C  You should have received a copy of the GNU General Public License
C  along with this program; if not, write to the Free Software
C  Foundation, Inc., 51 Franklin Street, Fifth Floor,
C  Boston, MA  02110-1301  USA
C
C  François Méot <fmeot@bnl.gov>
C  Brookhaven National Laboratory
C  C-AD, Bldg 911
C  Upton, NY, 11973, USA
C  -------
      SUBROUTINE ZGOUBI(NL1,NL2,READAT,NBLMI)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL READAT

      INCLUDE "C.CDF.H"     ! COMMON/CDF/ IES,LF,LST,NDAT,NRES,NPLT,NFAI,NMAP,NSPN,NLOG
      INCLUDE "C.CONST.H"     ! COMMON/CONST/ CL9,CL ,PI,RAD,DEG,QE ,AMPROT, CM2M
      INCLUDE "C.CONST2.H"     ! COMMON/CONST2/ ZERO, UN
      INCLUDE 'MXLD.H'
      INCLUDE "C.DON.H"     ! COMMON/DON/ A(MXL,MXD),IQ(MXL),IP(MXL),NB,NOEL
C      PARAMETER (LNTA=132) ; CHARACTER(LNTA) TA
C      PARAMETER (MXTA=45)
      INCLUDE "C.DONT.H"     ! COMMON/DONT/ TA(MXL,MXTA)
      INCLUDE "MAXTRA.H"
      INCLUDE "MAXCOO.H"
      LOGICAL AMQLU(5),PABSLU
      INCLUDE "C.FAISC.H"     ! COMMON/FAISC/ F(MXJ,MXT),AMQ(5,MXT),DP0(MXT),IMAX,IEX(MXT),
      PARAMETER (LBLSIZ=20)
      CHARACTER(LBLSIZ) LABEL
      INCLUDE "C.LABEL.H"     ! COMMON/LABEL/ LABEL(MXL,2)
      INCLUDE "C.MARK.H"     ! COMMON/MARK/ KART,KALC,KERK,KUASEX
      INCLUDE "C.TYPFLD.H"     ! COMMON/TYPFLD/ KFLD,MG,LC,ML,ZSYM
      INCLUDE "C.REBELO.H"   ! COMMON/REBELO/ NRBLT,IPASS,KWRT,NNDES,STDVM
      INCLUDE 'MXFS.H'
      INCLUDE 'MXSCL.H'
      INCLUDE "C.SCAL.H"     ! COMMON/SCAL/ SCL(MXF,MXS,MXSCL),TIM(MXF,MXS),NTIM(MXF),KSCL

      PARAMETER (KSIZ=10)
      CHARACTER(KSIZ) KLEY
      SAVE KLEY
      CHARACTER(80) TITRE, TITRO
C      COMMON/TITR/ TITRE
      SAVE TITRE

      PARAMETER(MPOL=10)
      DIMENSION ND(MXL)

C----- For space charge computaton
      PARAMETER(MLBSC=10)
      CHARACTER(LBLSIZ) LBLSC(MLBSC)
      LOGICAL TSPCH
C      COMMON/SPACECHA/ TSPCH,TLAMBDA,Rbeam,Xave,Emitt,Tave,Bunch_len,
      COMMON/SPACECHA/ TLAMBDA,RBEAM(2),XAVE(2),EMITT(2),TAVE,BUNCH_LEN,
     >                EMITTZ, BTAG, SCKX, SCKY, TSPCH
CC----- FOR SPACE CHARGE COMPUTATON
C      PARAMETER(MLBSC=10)
C      CHARACTER(LBLSIZ) LBLSC(MLBSC)
C      LOGICAL OKSPCH
C      SAVE OKSPCH, LBLSC, NLBSC

C----- For printing after occurence of pre-defined labels
      PARAMETER(MLB=10)
      CHARACTER(LBLSIZ) LBLST(MLB), LBLSP(MLB), LBLOPT(MLB)
      LOGICAL PRLB, PRLBSP
      SAVE KPRT, PRLB, LBLST, KPRTSP, PRLBSP, LBLSP, NLB, NLBSP
      SAVE LBLOPT, NLBOPT

C----- Pick-up signal
      PARAMETER (MXPUD=9,MXPU=1000)
      INCLUDE "C.CO.H"     ! COMMON/CO/ FPU(MXPUD,MXPU),KCO,NPUF,NFPU,IPU
      PARAMETER (MPUF=6)
      CHARACTER(LBLSIZ) PULAB
      INCLUDE "C.COT.H"     ! COMMON/COT/ PULAB(MPUF)

C----- Set to true by REBELOTE if last turn to be stopped at NOELB<MAX_NOEL
      LOGICAL REBFLG
      SAVE REBFLG, NOELRB

C----- Tells whether FIT is active or not
      LOGICAL FITING
C----- To get values into A(), from earlier FIT
      LOGICAL FITGET
      SAVE FITGET

      LOGICAL TOMANY, STRACO, STRWLD
      CHARACTER(LNTA) TXTEMP
      INTEGER DEBSTR,FINSTR

      CHARACTER(80) TXTELT, TXTELO
      SAVE TXTELT

      CHARACTER(300) SYSCMD

      INCLUDE 'PARIZ.H'
      INCLUDE 'FILPLT.H'

      PARAMETER (J0=0, J1=1, J2=2, J3=3, J5=5, J6=6)

      LOGICAL OPTIX
      SAVE KOPTCS, OPTIX

      LOGICAL EMPTY, KCPLD

      SAVE PNLTGT, ITRMA, ICPTMA

C This INCLUDE must stay located right before the first statement
      CHARACTER(KSIZ) KLEO

      LOGICAL PRDIC

      LOGICAL FITFNL, FITLST
      SAVE FITFNL

C      CHARACTER(132) TXT132
      LOGICAL STRCON

      LOGICAL FITBYD, FITRBL
      LOGICAL OKPRLB

      SAVE NBLMN

      LOGICAL FAIFIT
      SAVE FAIFIT

      PARAMETER (ITRMA0=999)
      SAVE IRET

      LOGICAL AGSMDL, AGSMDLO

      CHARACTER(LBLSIZ) LBL1, LBL2
      PARAMETER (I1=1)

      DATA PNLTGT, ITRMA, ICPTMA / 1D-10, ITRMA0, 1000 /

      DATA PRDIC / .FALSE. /
      DATA IRET / 0 /
      DATA FAIFIT / .FALSE. /
      DATA AGSMDL / .FALSE. /
      DATA KCPLD / .FALSE. /
      DATA OPTIX / .FALSE. /
      
Complete list of keys      
      INCLUDE 'LSTKEY.H'

      NBLMN = NBLMI

C .T. if FIT has been completed, and pgm executing beyond keyword FIT[2}
      CALL FITST3(
     >            FITBYD)         ! Execution pointer goes beyond FIT

      IF(FITBYD) GOTO 998

      IF(NL2 .GT. MXL) CALL ENDJOB(
     >      'Too  many  elements  in  the  structure, max is',MXL)

      IF(READAT) THEN
        CALL FITSTA(J5,
     >                 FITING)
        CALL FITST2(NBLMN)
        IF(.NOT.FITING) NL2=NBLMN
        CALL LINGUA(LNG)
        CALL RESET(0,0)
C------- Print after defined labels. Switched on by FAISTORE.
        PRLB = .FALSE.
C------- Print after defined labels. Switched on by SPNSTORE.
        PRLBSP = .FALSE.
      ENDIF

C----- Get FIT status
      CALL FITSTA(J5,
     >               FITING)
      IF(FITING) CALL RESET2

      IF(READAT) READ(NDAT,503) TITRE
 503  FORMAT(A)

C------- space charge initially off
        TSPCH= .FALSE.

CCCCCCCCCCCCfor LHC : do    REWIND(4)

      IF(.NOT. FITING) THEN
        IF(NRES .GT. 0) WRITE(6,905) TITRE
 905    FORMAT(/,1X,'Title : ',A80,//)
      ENDIF

      TOMANY = .FALSE.
      NOEL = NL1-1

 998  CONTINUE

C YD FM. 28/01/2014
      IF(NOEL .GT. 0) THEN

C------- Compute space charge kick and apply to bunch
       IF(TSPCH) THEN
         IF( STRACO(NLBSC,LBLSC,LABEL(NOEL,1),
     >                                       IL)
     >   .OR. LBLSC(1).EQ.'all' .OR. LBLSC(1).EQ.'ALL')
     >   CALL SPACH(
     >                  SCKX, SCKY )
       ENDIF
       IF(PRLB) THEN
C------- From Keyword FAISTORE: Print after Lmnt with defined LABEL.
C        LBLST contains the LABEL['s] after which print shall occur
C        IF( STRACO(NLB,LBLST,LABEL(NOEL,1),
C     >                                   IL)
C     >    .OR. LBLST(1).EQ.'all' .OR. LBLST(1).EQ.'ALL')
         CALL FITST9(
     >               FITLST)
         IF(FAIFIT .AND. (.NOT.FITLST)) THEN ! skip if label1='finalFIT' and not at last pass after FIT

         ELSE
           IF( OKPRLB(NLB,LBLST,LABEL(NOEL,1))
     >     .OR.STRWLD(NLB,LBLST,LABEL(NOEL,1),
     >                                       IS))
     >     CALL IMPFAI(KPRT,NOEL,KLE(IQ(NOEL)),LABEL(NOEL,1),
     >                                              LABEL(NOEL,2))
         ENDIF
       ENDIF
       IF(PRLBSP) THEN
C------- Print after Lmnt with defined LABEL - from Keyword SPNSTORE
C        LBLSP contains the LABEL['s] after which print shall occur
        IF( STRACO(NLBSP,LBLSP,LABEL(NOEL,1),
     >                                     IL) )
     >    CALL SPNPRN(KPRTSP,NOEL,KLE(IQ(NOEL)),LABEL(NOEL,1),
     >                                              LABEL(NOEL,2))
       ENDIF

       IF(KCO .EQ. 1) THEN      ! From PICKUP
C------- Calculate pick-up signal
C         PULAB contains the NPUF LABEL's at which CO is calculated

         IF(.NOT. EMPTY(LABEL(NOEL,1))) THEN

           IF( STRACO(NPUF,PULAB,LABEL(NOEL,1),
     >                                       IL)
     >     .OR.STRWLD(NPUF,PULAB,LABEL(NOEL,1),
     >                                       IS))
     >     CALL PCKUP

         ENDIF
       ELSEIF(KCO .EQ. 2) THEN    ! From SVDOC

         CALL FITST9(
     >               FITLST)  ! A final pass after FIT

         IF(FITLST) THEN
C------- Store pickup signal, at PUs belonging in svdpus list

           IF(.NOT. EMPTY(LABEL(NOEL,1))) THEN

             IF( STRACO(NPUF,PULAB,LABEL(NOEL,1),
     >                                         IL)
     >       .OR.STRWLD(NPUF,PULAB,LABEL(NOEL,1),
     >                                         IS))
     >       CALL SVDPCK

           ENDIF
         ENDIF
       ENDIF

       IF(KOPTCS .EQ. 1) THEN
C------- Transport beam matrix and print at element ends. Set by OPTICS or by TWISS keywords.

         IF(KUASEX .NE. -99) THEN
C OPTICC may not be desired for some elements (non-optical, or else.)

           IF(OKPRLB(NLBOPT,LBLOPT,LABEL(NOEL,1))
     >     .OR.STRWLD(NLBOPT,LBLOPT,LABEL(NOEL,1),
     >                                            IS))
     >     CALL OPTICC(F,NOEL,PRDIC,KCPLD,IMAX)

         ENDIF
       ENDIF

C This was introduced so to restrain OPTICS to optics-type keywords.  Any additional desired
C location should be assigned KUASEX=0 for OPTICC to operate (e.g., DRIFT, MARKER...)
       KUASEX = -99

       IF(REBFLG) THEN
C----- Set to true by REBELOTE : last turn to be stopped at NOELB<MAX_NOEL
        IF(IPASS.EQ.NRBLT+1) THEN
C          CALL REBEL7(
C     >                NOELB)
          CALL REBLT7(
     >                NOELB,KLERB)

          IF(NOEL.EQ.NOELB) THEN
            IKLE = KLERB
            KLEY = KLE(IKLE)   ! 'REBELOTE' OR 'SVDOC'
            NOEL = NOELRB
            IQ(NOEL) = IKLE
            GOTO 187
          ENDIF
        ENDIF
       ENDIF

      ENDIF ! NOEL.GT.0

      IF(READAT) THEN

 188    READ(NDAT,*,ERR=996,END=997) KLEY

        IF(KLEY(DEBSTR(KLEY):DEBSTR(KLEY)) .EQ. '!' .OR.
     >  FINSTR(KLEY) .LE. DEBSTR(KLEY) ! Takes care of empty lines and of possible crap single-character
     >  ) GOTO 188

        DO IKLE=1,MXKLE
          IF(KLEY .EQ. KLE(IKLE)) THEN
            NOEL = NOEL+1
C            IF( NOEL .EQ. MXL+1) THEN
            IF( NOEL .GT. MXL) THEN
              TOMANY=.TRUE.
              IF(NRES .GT. 0) THEN
                WRITE(NRES,*) ' PROCEDURE STOPPED: too many elements'
                WRITE(NRES,*) ' (number of elements should not exceed '
     >                                                       ,MXL,').'
                WRITE(NRES,*) ' Increase  MXL  in   MXLD.H'
              ENDIF
              CALL ENDJOB(' Increase  MXL  in   MXLD.H',-99)
            ENDIF
            IQ(NOEL) =  IKLE
            GOTO 187
          ENDIF
        ENDDO
        GOTO 999

      ELSE
C------- Gets here in case of "FIT"

        IF (NOEL .EQ. NL2 ) RETURN
        NOEL = NOEL+1
        IKLE = IQ(NOEL)
        KLEY = KLE(IKLE)

      ENDIF

 187  CONTINUE
      
      IF(NRES.GT.0) THEN
        WRITE(NRES,201)
 201    FORMAT(/,132('*'))
        WRITE(NRES,334) NOEL,'Keyword, label(s) :',
     >  KLEY,LABEL(NOEL,1),LABEL(NOEL,2),IPASS
 334    FORMAT(2X,I5,4(2X,A),T120,'IPASS= ',I0,/)
        CALL FLUSH2(NRES,.FALSE.)
        WRITE(TXTELT,FMT='(I5,A1,I5,1X,A10,2(A1,A))')
     >  NOEL,'/',NBLMN,KLEY,'/',LABEL(NOEL,1),'/',LABEL(NOEL,2)
        IF(IPASS.EQ.1) CALL ARRIER(TXTELT)
      ENDIF

      KFLD=MG

CCCCCCCCCCCCfor LHC ; do REWIND(4).
C      IF(1000*(NOEL/1000) .EQ. NOEL) REWIND(4)
C            write(88,*) ' zgoubi ',NDAT,NRES,NPLT,NFAI,NMAP,NSPN,ipass

C Go to "GOTO(...) IKLE" ---------------------------
      GOTO 1001
  999 CONTINUE
C---------------------------------------------------

      IF(NRES.GT.0) THEN
        WRITE(NRES,201)
        IF(KLEY.EQ.'END' .OR. KLEY.EQ.'FIN') THEN
          WRITE(6,*)
          WRITE(6,*)
     >     'Pgm zgoubi : Execution ended normally, '
     >     //'upon keyword END or FIN.'
          WRITE(NRES,*)
     >     'Pgm zgoubi : Execution ended normally, '
     >     //'upon keyword END or FIN'
        ELSE
          WRITE(6,200) 'Unknown keyword  /',KLEY,'/'
          WRITE(ABS(NRES),200) 'Unknown keyword /',KLEY,'/'
 200      FORMAT(/,10X,'Pgm zgoubi. ',3A,/)
          CALL ENDJOB(
     >    'Pgm zgoubi : Execution ended upon unexpected keyword.',-99)
        ENDIF
      ENDIF
      
      IF(KLEY.EQ.'END' .OR. KLEY.EQ.'FIN') IRET=1
      RETURN

 996  CONTINUE
      CALL ENDJOB(
     >'Pgm zgoubi : Execution stopped due to ERRor '//
     >'while reading Key from input .dat file.',-99)
      RETURN

 997  CONTINUE
      IF(NRES.GT.0) WRITE(NRES,fmt='(a)')
     >'Pgm zgoubi : Execution stopped due to END-of-file '//
     >'while reading Key from input .dat file.'
      RETURN

C----- DRIFT, ESL. Free space.
 1    CONTINUE
      KUASEX = 0
C      IF(READAT) READ(NDAT,*) A(NOEL,1)
      IF(READAT) CALL RESL(NDAT)
      IF(FITGET) CALL FITGT1
C     CALL ESL(READAT,1,1,IMAX)
C      LST = LSTSET(NINT(A(NOEL,3)))
      CALL ESLI(NINT(A(NOEL,3)))
C              IPR        # split NDL     XL
      CALL ESL(I1,I1,IMAX,NINT(A(NOEL,2)),A(NOEL,1))
      GOTO 998
C----- AIMANT. Dipole with computed field map in cylindrical coordinates
 2    CONTINUE
      IF(READAT) CALL RAIMAN(NDAT,NOEL,MXL,A,
     >                                       ND(NOEL))
      IF(FITGET) CALL FITGT1
      KALC = 2
      KUASEX = 20
      CALL AIMANT(READAT,ND(NOEL))
      GOTO 998
C----- QUADRUPO. B quadrupolaire et derivees calcules en tout point (X,Y,Z)
 3    CONTINUE
      KALC = 3
      KUASEX = 2
      IF(READAT) THEN
        CALL RQSOD(NDAT,NOEL,MXL,A,
     >                             ND(NOEL))
      ELSE
        CALL STPSI1(NOEL)
      ENDIF
      IF(FITGET) CALL FITGT1
      CALL QUASEX(ND(NOEL))
      GOTO 998
C----- SEXTUPOL. B SEXTUPOLAIRE  ET DERIVEES CALCULES EN TOUT POINT (X,Y,Z)
4     CONTINUE
      KALC = 3
      KUASEX = 3
      IF(READAT) THEN
        CALL RQSOD(NDAT,NOEL,MXL,A,
     >                        ND(NOEL))
      ELSE
        CALL STPSI1(NOEL)
      ENDIF
      IF(FITGET) CALL FITGT1
      CALL QUASEX(ND(NOEL))
      GOTO 998
C----- IMAGE. RECHERCHE DU PLAN IMAGE ET DIMENSIONS D'IMAGE HORIZONTAUX
C----- Monochromatic beam
 5    CALL FOCALE(1)
      GOTO 998
C----- IMAGES. RECHERCHE DU PLAN IMAGE ET DIMENSIONS D'IMAGE HORIZONTAUX
C----- Polychrome beam
 6    CALL FOCALE(2)
      GOTO 998
C----- FAISCEAU. Print current beam in zgoubi.res
 7    CONTINUE
      IF(NRES.GT.0 .OR. LABEL(NOEL,1).EQ.'FORCE')
     >CALL IMPTRA(1,IMAX,ABS(NRES))
      GOTO 998
C----- FAISCNL. Stores beam at current position (in .fai type file)
 8    CONTINUE
      IF(READAT) CALL RFAIST(J0,LABEL(NOEL,1),
     >                         PRLB,KPRT,LBLST,NLB,FAIFIT)
      IF(TA(NOEL,1).NE.'none') THEN
        NLB = 0
        TXTEMP = TA(NOEL,1)
        TXTEMP=TXTEMP(DEBSTR(TXTEMP):FINSTR(TXTEMP))
        CALL IMPFAW(TXTEMP,LBLST,NLB)
        CALL IMPFAI(J0,NOEL-1,KLE(IQ(NOEL-1)),LABEL(NOEL-1,1),
     >                                             LABEL(NOEL-1,2))
      ENDIF
      GOTO 998
C----- TRAVERSEE D'UNE CIBLE
 9    CONTINUE
      IF(READAT) CALL RCIBLE
      IF(FITGET) CALL FITGT1
      CALL CIBLE
      GOTO 998
C----- FOCALE. DIMENSIONS DU FAISCEAU @ XI
 10   CONTINUE
      IF(READAT) READ(NDAT,*) A(NOEL,1)
      IF(FITGET) CALL FITGT1
      CALL FOCALE(3)
      GOTO 998
C----- REBELOTE. Passes NRBLT more times thru the structure
 11   CONTINUE
      IF(READAT) CALL RREBEL(LABEL,KLE)
      IF(FITBYD) THEN                ! Execution pointer goes beyond FIT
        FITRBL = NRBLT .EQ.0 .OR. IPASS .LE. NRBLT
        CALL FITST8(FITRBL)          ! Allows FIT embeded in REBELOTE in zgoubi_main
      ENDIF
      CALL REBEL(READAT,KLE,LABEL,
     >                            REBFLG,NOELRB)
      FITBYD = .FALSE.
      CALL FITST4(FITBYD)
      IF(IPASS.EQ.NRBLT+2) THEN        ! Means that REBELOTE series is completed
        FITRBL = .FALSE.
        CALL FITST8(FITRBL)
      ENDIF
      CALL KSMAP0
      CALL REBLT4(11)   !  11 = REBELOTE
      GOTO 998
C----- QUADISEX. Champ creneau B = B0(1+N.Y+B.Y2+G.Y3) plan median
 12   CONTINUE
      KALC =1
      KUASEX = 3
      IF(READAT) CALL RSIMB(
     >                      ND(NOEL))
      IF(FITGET) CALL FITGT1
      CALL QUASEX(ND(NOEL))
      GOTO 998
C----- CHANGREF. Translation X,Y et rotation du referentiel courant
 13   CONTINUE
      KUASEX = 0
      IF(READAT) CALL RCHANG
      IF(FITGET) CALL FITGT1
      CALL CHREFE
      GOTO 998
C----- SEXQUAD. Champ CRENEAU B = B0(N.Y+B.Y2+G.Y3) PLAN MEDIAN
 14   CONTINUE
      KALC =1
      KUASEX = 4
      IF(READAT) CALL RSIMB(
     >                        ND(NOEL))
      IF(FITGET) CALL FITGT1
      CALL QUASEX(ND(NOEL))
      GOTO 998
C----- AIMANT TOROIDAL
 15   CONTINUE
      KALC =1
      KUASEX = 7
      IF(READAT) CALL RSIMB(
     >                        ND(NOEL))
      IF(FITGET) CALL FITGT1
      CALL QUASEX(ND(NOEL))
      GOTO 998
C----- OBJETA. MONTE-CARLO GENERATION OF MU OR PI FROM ETA DECAY
C          ( D'APRES Benjamin MAYER , FEVRIER 1990 )
 17   CONTINUE
      IF(READAT) CALL ROBJTA
      CALL OBJETA
      GOTO 998
C----- MATRIX. COEFFICIENTS D'ABERRATION A L'ABSCISSE COURANTE
 18   CONTINUE
      IF(READAT) CALL RMATRX(
     >                       IORD,IFOC,KWR)
      IF(FITGET) CALL FITGT1
      CALL MATRIC(F,IORD,IFOC,KWR,IMAX,
     >                                 IER)
      GOTO 998
C----- CHAMBR. Stops and records trajectories out of chamber limits
 19   CONTINUE
      IF(READAT) CALL RCOLLI
      IF(FITGET) CALL FITGT1
      CALL CHAMB
      GOTO 998
C----- Champ Q-POLE SPECIAL PROJET SPES2
 20   CONTINUE
      KALC =1
      KUASEX = 1
C ............... ADD   IF(READAT) CALL ............
      IF(FITGET) CALL FITGT1
      CALL QUASEX(ND(NOEL))
      GOTO 998
C----- CARTEMES. CARTE DE Champ CARTESIENNE MESUREE DU SPES2
 21   CONTINUE
      KALC =2
      KUASEX = 1
      IF(READAT) CALL RCARTE(J1,J2,
     >                                ND(NOEL))
      IF(FITGET) CALL FITGT1
      CALL QUASEX(ND(NOEL))
      GOTO 998
C----- YMY. CHANGE LE FAISCEAU Y,T,Z,P  ->  -Y,-T,-Z,-P
C       ( EQUIVALENT A CHANGEMENT DE SIGNE DU Champ DIPOLAIRE }
 22   CONTINUE
      KUASEX = 0
      CALL YMOINY
      GOTO 998
C----- B OCTUPOLAIRE ET DERIVEES CALCULES EN TOUT POINT (X,Y,Z)
 23   CONTINUE
      KALC = 3
      KUASEX = 4
      IF(READAT) THEN
        CALL RQSOD(NDAT,NOEL,MXL,A,
     >                        ND(NOEL))
      ELSE
        CALL STPSI1(NOEL)
      ENDIF
      IF(FITGET) CALL FITGT1
      CALL QUASEX(ND(NOEL))
      GOTO 998
C----- OBJET.
24    CONTINUE
      KUASEX = 0
      IF(READAT) CALL ROBJET
      IF(FITGET) CALL FITGT1
      CALL OBJETS
      GOTO 998
C----- MCOBJET. Object defined by Monte-Carlo
 25   CONTINUE
      KUASEX = 0
      IF(READAT) CALL RMCOBJ
      IF(FITGET) CALL FITGT1
C FM July 2014 - Inhibited call to mcobja for eRHIC, to allow varying Z0,P0.
C      IF(.NOT. FITING) THEN
        CALL MCOBJ
C      ELSE
C        CALL MCOBJA
C        CALL CNTRST
C      ENDIF
      GOTO 998
C----- DESINTEGRATION EN COURS DE VOL
26    CONTINUE
      IF(READAT) CALL RMCDES
      IF(.NOT. FITING)  CALL MCDESI
      IF(FITGET) CALL FITGT1
      GOTO 998
C----- HISTOGRAMME DE Y T Z P D
27    CONTINUE
      IF(READAT) CALL RHIST(NDAT,NOEL,MXL,A,TA)
      IF(FITGET) CALL FITGT1
      CALL HIST
      GOTO 998
C----- TRANSMAT. TRANSFERT MATRICIEL AU SECOND ORDRE
 28   CONTINUE
      KUASEX = 0
      IF(READAT) CALL RTRANS
      IF(FITGET) CALL FITGT1
      CALL TRANSM(READAT)
      GOTO 998
C----- AIMANT VENUS (Champ CONSTANT DANS UN RECTANGLE)
 29   CONTINUE
      KALC =1
      KUASEX = 5
      IF(READAT) CALL RSIMB(
     >                        ND(NOEL))
      IF(FITGET) CALL FITGT1
      CALL QUASEX(ND(NOEL))
      GOTO 998
C----- AIMANT PS170 (Champ CONSTANT DANS UN CERCLE)
 30   CONTINUE
      KALC =1
      KUASEX = 6
      IF(READAT) CALL RSIMB(
     >                        ND(NOEL))
      IF(FITGET) CALL FITGT1
      CALL QUASEX(ND(NOEL))
      GOTO 998
C----- TOSCA. Read  2-D or 3-D field map (e.g., as obtained from TOSCA code),
C      with mesh either cartesian (KART=1) or cylindrical (KART=2).
 31   CONTINUE
      KALC = 2
      IDIM = J3
C      IF(READAT) CALL RCARTE(KART,J3,
      IF(READAT) CALL RTOSCA(IDIM,
     >                            ND(NOEL))
      IF    (A(NOEL,22) .EQ. 1) THEN
        KUASEX = 2
      ELSEIF(A(NOEL,22) .GT. 1) THEN
        KUASEX = 7
        IF(IZ.LE.1) CALL ENDJOB('Pgm zgoubi. Cannot use a 3-D map. '
     >  //'Change IZ in PARIZ.H to .ge. the '
     >  //'number of vertical mesh nodes and re-compile',-99)
      ENDIF
      IF(FITGET) CALL FITGT1
      IF    (NINT(A(NOEL,23)) .LT. 20) THEN
Cartesian coordinates, KART=1 -> chxc -> toscac
        CALL QUASEX(ND(NOEL))
      ELSE
Cylindrical coordinates, KART=2 -> chxp -> toscap
        CALL AIMANT(READAT,ND(NOEL))
      ENDIF
      GOTO 998
C----- AUTOREF. CHGNT RFRNTIEL AUTMTIQ => PLAN NORMAL A LA TRAJ. DE REFERENCE
 32   CONTINUE
      IF(READAT) CALL RAUTOR
      IF(FITGET) CALL FITGT1
      CALL AUTORF
      GOTO 998
C----- COLLIMA. Collimator ( particules out => IEX = -4 )
 33   CONTINUE
      IF(READAT) CALL RCOLLI
      IF(FITGET) CALL FITGT1
      CALL COLLIM
      GOTO 998
C----- MULTIPOL. B ET DERIVEES CALCULES EN TOUT POINT (X,Y,Z)
 34   CONTINUE
      KALC = 3
      KUASEX = MPOL+1
      IF(READAT) THEN
        CALL RMULTI(NDAT,NOEL,MXL,A,MPOL,
     >                                   ND(NOEL))
      ELSE
        CALL STPSI1(NOEL)
      ENDIF
      IF(FITGET) CALL FITGT1
      CALL QUASEX(ND(NOEL))
      GOTO 998
C----- SEPARA. SEPARATEUR ELECTROSTATIQUE ANALYTIQUE
 35   CONTINUE
      IF(READAT) CALL RSEPAR
      IF(FITGET) CALL FITGT1
      CALL SEPARA
      GOTO 998
C----- RESET. RAZ LES COMPTEURS ( PEUT S'INTERCALER ENTRE PB CONSECUTIFS)
 36   CONTINUE
      IRST=1
C IRST=1 (default when stack problems using 'RESET' keyword): set ipass=1. Otheriwse ipass=as left by possible earlier REBELOTE
      CALL RESET(1,IRST)
      GOTO 998
C----- ORDRE. IMPOSE  L'ORDRE  DE CALCUL  POUR LES ELMNTS KALC = 3
C       ORDRE IS 2 BY DEFAUT)
 37   CONTINUE
      IF(READAT) READ(NDAT,*) A(NOEL,1)
      IF(FITGET) CALL FITGT1
      CALL MODORD
      GOTO 998
C----- DIPOLE-M. 2-D mid plane fabricated dipole map. Polar coordinates.
 39   CONTINUE
      IF(READAT) CALL RAIMAN(NDAT,NOEL,MXL,A,
     >                        ND(NOEL))
      KALC = 2
      KUASEX = 21
      IF(FITGET) CALL FITGT1
      CALL AIMANT(READAT,ND(NOEL))
      GOTO 998
C----- IMAGEZ. RECHERCHE DU PLAN IMAGE ET DIMENSIONS D'IMAGE VERTICAUX
 41   CALL FOCALE(-1)
      GOTO 998
C----- IMAGESZ. RECHERCHE DU PLAN IMAGE ET DIMENSIONS D'IMAGE VERTICAUX
C----- FAISCEAU DISPERSE EN V
 42   CALL FOCALE(-2)
      GOTO 998
C----- FOCALEZ. DIMENSIONS ET POSITION VERTICALES DU FAISCEAU @ XI
 43   CONTINUE
      IF(READAT) READ(NDAT,*) A(NOEL,1)
      IF(FITGET) CALL FITGT1
      CALL FOCALE(-3)
      GOTO 998
C----- PLOTDATA PURPOSE STUFF
 44   CONTINUE
      IF(READAT) READ(NDAT,*) A(NOEL,1)
      IF(FITGET) CALL FITGT1
      CALL PLTDAT
      GOTO 998
C----- BINARY.
C          READ  THE  FORMATTED  FILE  'MYFILE' AND
C          COPIES  IT  INTO  THE  BINARY  FILE  'B_MYFILE',
C          OR RECIPROCAL.
 45   CONTINUE
      IF(READAT) CALL RBINAR
      CALL BINARY
      GOTO 998
C----- FIT. FIT2. Two methods are available
 102  MTHOD = 2
      GOTO 461
 46   CONTINUE
      MTHOD = 1
 461  CONTINUE
      CALL FITNU2(MTHOD)
      IF(READAT) CALL RFIT(KLEY,ITRMA,
     >                         PNLTGT,ITRMA,ICPTMA,FITFNL)
      CALL FITST6(FITFNL)     !  True if request for last run with variables following from FIT[2}
      IF(MTHOD.EQ.1) CALL MINO12(PNLTGT,ITRMA,ICPTMA)
      IF(MTHOD.EQ.2) CALL NMMIN2(PNLTGT,ITRMA,ICPTMA)
      CALL CPTFC1(ICPTMA)
      FITING = .TRUE.
      CALL FITSTA(J6,FITING)
      CALL FITST2(NOEL)
      FITGET = .FALSE.
      IF(FITING) RETURN
      GOTO 998
C----- SPES3. CARTE DE CHAMP CARTESIENNE MESUREE DU SPES3
C          D'APRES W. ROSCH, 1991
 47   CONTINUE
      KALC =2
      KUASEX = 3
      IF(READAT) CALL RCARTE(J1,J2,
     >                        ND(NOEL))
      IF(FITGET) CALL FITGT1
      CALL QUASEX(ND(NOEL))
      GOTO 998
C----- CARTE DE Champ CARTESIENNE 2-D DE CHALUT
 48   CONTINUE
      KALC =2
      KUASEX = 4
      IF(READAT) CALL RCARTE(J1,J2,
     >                        ND(NOEL))
      IF(FITGET) CALL FITGT1
      CALL QUASEX(ND(NOEL))
      GOTO 998
C-----  SPNTRK. Switch spin tracking
 49   CONTINUE
      IF(READAT) CALL RSPN
      IF(FITGET) CALL FITGT1
      CALL SPN
      GOTO 998
C----- SPNPRT. PRINT SPIN STATES
 50   CONTINUE
      CALL FITSTA(J5,
     >               FITING)
      IF(.NOT. FITING)
     >CALL SPNPRT(LABEL(NOEL,1),LABEL(NOEL,2))
      GOTO 998
C----- BEND. Dipole magnet
 51   CONTINUE
      KALC =1
      KUASEX = 8
      IF(READAT) THEN
        CALL RBEND(
     >             ND(NOEL))
      ELSE
        CALL STPSI1(NOEL)
      ENDIF
      IF(FITGET) CALL FITGT1
      CALL QUASEX(ND(NOEL))
      GOTO 998
C----- SOLENOID
 52   CONTINUE
      KALC = 3
      KUASEX = 20
      IF(READAT) THEN
        CALL RSOLEN(NDAT,NOEL,MXL,A,
     >                        ND(NOEL))
      ELSE
        CALL STPSI1(NOEL)
      ENDIF
      IF(FITGET) CALL FITGT1
      CALL QUASEX(ND(NOEL))
      GOTO 998
C----- Plot transverse coordinates (for use on a workstation)
 53   CONTINUE
      GOTO 998
C----- SPNPRNL. Store  state of spins in logical unit
 54   CONTINUE
      IF(READAT) CALL RSPNST(J0,
     >                         PRLBSP,KPRTSP,LBLSP,NLBSP)
      IF(TA(NOEL,1).NE.'none') THEN
        NLBSP = 0
        TXTEMP = TA(NOEL,1)
        TXTEMP=TXTEMP(DEBSTR(TXTEMP):FINSTR(TXTEMP))
        CALL SPNPRW(TXTEMP,LBLSP,NLBSP)
        CALL SPNPRN(J0,NOEL-1,KLE(IQ(NOEL-1)),LABEL(NOEL-1,1),
     >                                             LABEL(NOEL-1,2))
      ENDIF
      GOTO 998
C----- CAVITE ACCELERATRICE
 55   CONTINUE
      KUASEX = 0   ! This is to allow KOPTCS
      IF(READAT) CALL RCAVIT
      IF(FITGET) CALL FITGT1
      CALL CAVITE
      GOTO 998
C----- PARTICUL.  DATA PARTICLE
 56   CONTINUE
C     .... MASSE(MeV/c2), CHARGE(C), G-yromagn., com life time(s), Free
      IF(READAT) CALL RPARTI(NDAT,NOEL,
     >                                 A)
      IF(FITGET) CALL FITGT1
      CALL PARTIC
      GOTO 998
C----- SCALING. COMMANDE DES SCALINGS ( B, FREQ...)
 57   CONTINUE
      IF(READAT) CALL RSCAL
      IF(FITGET) CALL FITGT1
      CALL SCALIN
      GOTO 998
C----- ELREVOL.  ELCTROSTATIQ  FIELD  Ex(R=0,X) 1-D MEASURED.  CYLINDRICAL SYMM.
 68   CONTINUE
      KFLD=LC
C----- BREVOL. 1-D field B(r=0,x) on-axis field map, with cylindrical symmetry.
 58   CONTINUE
      KALC =2
      KUASEX = 8
      IF(READAT) CALL RCARTE(J1,J1,
     >                        ND(NOEL))
      IF(FITGET) CALL FITGT1
      CALL QUASEX(ND(NOEL))
      GOTO 998
C----- POISSON. CARTE DE Champ CARTESIENNE 2-D FABRIQUEE PAR POISSON
 59   CONTINUE
      KALC =2
      KUASEX = 5
      IF(READAT) CALL RCARTE(J1,J2,
     >                        ND(NOEL))
      IF(FITGET) CALL FITGT1
      CALL QUASEX(ND(NOEL))
      GOTO 998
C----- DIPOLE. Similar to DIPOLES, but with a single magnet
 60   CONTINUE
      KALC =1
      KUASEX = 31
      IF(READAT) CALL RDIP(
     >                        ND(NOEL))
      IF(FITGET) CALL FITGT1
      CALL AIMANT(READAT,ND(NOEL))
      GOTO 998
C----- CARTE MESUREE SPECTRO KAON GSI (DANFISICS)
 61   CONTINUE
      KALC =2
      KUASEX = 6
      IF(READAT) CALL RCARTE(J1,J2,
     >                        ND(NOEL))
      IF(FITGET) CALL FITGT1
      CALL QUASEX(ND(NOEL))
      GOTO 998
C----- MAP2D. 2D B-FIELD MAP, NO SPECIAL SYMMETRY (P. Akishin, 07/1992)
 62   CONTINUE
      IF(ID.LT.3)
     >   CALL ENDJOB('Use of MAP2D :  you need ID=3 in PARIZ.H',-99)
      KALC =2
      KUASEX = 9
      IF(READAT) CALL RCARTE(J1,J2,
     >                        ND(NOEL))
      IF(FITGET) CALL FITGT1
      CALL QUASEX(ND(NOEL))
      GOTO 998
C----- B DECAPOLE ET DERIVEES CALCULES EN TOUT POINT (X,Y,Z)
 63   CONTINUE
      KALC = 3
      KUASEX = 5
      IF(READAT) THEN
        CALL RQSOD(NDAT,NOEL,MXL,A,
     >                        ND(NOEL))
      ELSE
        CALL STPSI1(NOEL)
      ENDIF
      IF(FITGET) CALL FITGT1
      CALL QUASEX(ND(NOEL))
      GOTO 998
C----- B DODECAPOLE ET DERIVEES CALCULES EN TOUT POINT (X,Y,Z)
 64   CONTINUE
      KALC = 3
      KUASEX = 6
      IF(READAT) THEN
        CALL RQSOD(NDAT,NOEL,MXL,A,
     >                        ND(NOEL))
      ELSE
        CALL STPSI1(NOEL)
      ENDIF
      IF(FITGET) CALL FITGT1
      CALL QUASEX(ND(NOEL))
      GOTO 998
C----- WIENFILT. INTEGR NUMERIQ.
 65   CONTINUE
      KALC = 3
      KUASEX = 21
      KFLD=ML
      IF(READAT) CALL RWIENF(NDAT,NOEL,MXL,A,
     >                                       ND(NOEL))
      IF(FITGET) CALL FITGT1
      CALL QUASEX(ND(NOEL))
      GOTO 998
C----- FAISTORE. Similar to FAISCNL, with additional options:
C        - Print after LABEL'ed elements
C        - Print every other IPASS = mutltiple of IA
 66   CONTINUE
      IF(READAT) CALL RFAIST(MLB,LABEL(NOEL,1),
     >                           PRLB,KPRT,LBLST,NLB,FAIFIT)
      TXTEMP = TA(NOEL,1)
      TXTEMP=TXTEMP(DEBSTR(TXTEMP):FINSTR(TXTEMP))
      IF(FAIFIT .AND. (.NOT.FITLST)) THEN ! skip if label1='AtFITfinal' and not at last pass after FIT
      ELSE
        IF(TXTEMP.NE.'none') THEN
          IF(TA(NOEL,2).NE.'none') THEN
            CALL IMPFAW(TXTEMP,LBLST,NLB)
            IF(.NOT. PRLB) CALL IMPFAI(KPRT,NOEL-1,KLE(IQ(NOEL-1)),
     >                           LABEL(NOEL-1,1), LABEL(NOEL-1,2))
          ENDIF
        ENDIF
      ENDIF
      GOTO 998
C----- SPNSTORE. Similar to SPNPRNL, with additional options:
C        - Print after LABEL'ed elements
C        - Print every other IPASS = mutltiple of IA
 67   CONTINUE
      IF(READAT) CALL RSPNST(MLB,
     >                           PRLBSP,KPRTSP,LBLSP,NLBSP)
      TXTEMP = TA(NOEL,1)
      TXTEMP=TXTEMP(DEBSTR(TXTEMP):FINSTR(TXTEMP))
      IF(TXTEMP.NE.'none') THEN
        IF(TA(NOEL,2).NE.'none') THEN
          CALL SPNPRW(TXTEMP,LBLSP,NLBSP)
          IF(.NOT. PRLBSP) CALL SPNPRN(KPRTSP,NOEL-1,KLE(IQ(NOEL-1)),
     >                           LABEL(NOEL-1,1), LABEL(NOEL-1,2))
        ENDIF
      ENDIF
      GOTO 998
C----- EL2TUB. LENTILLE ELECTROSTATIQ A 2 TUBES OU DIAPHRAG.
 69   CONTINUE
      KALC = 3
      KUASEX = 22
      KFLD=LC
      IF(READAT) CALL REL2TU(
     >                        ND(NOEL))
      IF(FITGET) CALL FITGT1
      CALL QUASEX(ND(NOEL))
      GOTO 998
C----- UNIPOT. LENTILLE ELECTROSTATIQ A 3 TUBES OU DIAPHRAG.
 70   CONTINUE
      KALC = 3
      KUASEX = 23
      KFLD=LC
      IF(READAT) CALL RUNIPO(
     >                        ND(NOEL))
      IF(FITGET) CALL FITGT1
      CALL QUASEX(ND(NOEL))
      GOTO 998
C----- ELMULT. E MULTIPOLAIRE ET DERIVEES CALCULES EN TOUT POINT (X,Y,Z)
 71   CONTINUE
      KALC = 3
      KUASEX = MPOL+1
      KFLD=LC
      IF(READAT) THEN
        CALL RMULTI(NDAT,NOEL,MXL,A,MPOL,
     >                        ND(NOEL))
      ELSE
        CALL STPSI1(NOEL)
      ENDIF
      IF(FITGET) CALL FITGT1
      CALL QUASEX(ND(NOEL))
      GOTO 998
C----- EBMULT. E+B MULTIPOLAIRES ET DERIVEES CALCULES EN TOUT POINT (X,Y,Z)
 72   CONTINUE
      KALC = 3
      KUASEX = MPOL+1
      KFLD=ML
      IF(READAT) CALL REBMUL(
     >                        ND(NOEL))
      IF(FITGET) CALL FITGT1
      CALL QUASEX(ND(NOEL))
      GOTO 998
C----- ESPACE LIBRE VIRTUEL
 73   CONTINUE
      IF(READAT) READ(NDAT,*) A(NOEL,1),A(NOEL,2)
      IF(FITGET) CALL FITGT1
      CALL ESLVIR
      GOTO 998
C----- TRANSFORMATION FO(I,n)= A(,3)*FO(I,n) + A(,4)*FO(J,n) + A(,5)
 74   CONTINUE
      IF(READAT) READ(NDAT,*) A(NOEL,1),A(NOEL,2),
     >           A(NOEL,3),A(NOEL,4),A(NOEL,5)
      IF(FITGET) CALL FITGT1
      CALL TROBJ(1)
      GOTO 998
C----- TRANSLATION  F(2,n)=F(2,n)+A(,1) et F(4,n)=F(4,n)+A(,2)
 75   CONTINUE
      IF(READAT) READ(NDAT,*) A(NOEL,1),A(NOEL,2)
      IF(FITGET) CALL FITGT1
      CALL TRAOBJ(1)
      GOTO 998
C----- POLARMES. CARTE DE Champ POLAIRE. Field map, cylindrical coordinates.
 76   CONTINUE
      KALC =2
      KUASEX = 22
      IF(READAT) CALL RCARTE(J2,J2,
     >                        ND(NOEL))
      IF(FITGET) CALL FITGT1
      CALL AIMANT(READAT,ND(NOEL))
      GOTO 998
C----- TRAROT. Reference frame translation X,Y,Z & rotation RX,RY,RZ
 77   CONTINUE
      KUASEX = 0
      IF(READAT) READ(NDAT,*) (A(NOEL,II),II=1,6)
      IF(FITGET) CALL FITGT1
      CALL TRAROT(A(NOEL,1),A(NOEL,2),A(NOEL,3)
     >  ,A(NOEL,4),A(NOEL,5),A(NOEL,6))
      GOTO 998
C----- SRLOSS. Switch synchrotron ligth on
 78   CONTINUE
      IF(READAT) CALL RSRLOS
      IF(FITGET) CALL FITGT1
      CALL SRLOSS(IMAX,IPASS)
      GOTO 998
C----- PICKUPS.
 79   CONTINUE
      IF(READAT) CALL RPCKUP
      IF(FITGET) CALL FITGT1
      CALL PICKUP
      GOTO 998
C----- OPTICS. Transport the beam matrix and print/store it after keyword[s].
 80   CONTINUE
      IF(READAT) CALL ROPTIC(NDAT)
      CALL OPTICS(
     >            KOPTCS,LBLOPT,KCPLD)
      OPTIX = KOPTCS .EQ. 1                 ! Flag to preclude concurrent TWISS
      GOTO 998
C-----  GASCAT. Switch gas-scattering
 81   CONTINUE
      IF(READAT) CALL RGASCA
      IF(FITGET) CALL FITGT1
      CALL GASINI
      GOTO 998
C----- UNDULATO.  UNDULATOR
 82   CONTINUE
      KALC =3
      KUASEX = 30
      IF(READAT) CALL RUNDUL(
     >                        ND(NOEL))
      IF(FITGET) CALL FITGT1
      CALL QUASEX(ND(NOEL))
      GOTO 998
C----- ELCYLDEF
 83   CONTINUE
      IF(READAT) CALL RELCYL(
     >                        ND(NOEL))
      IF(FITGET) CALL FITGT1
      KALC = 3
      KUASEX = 24
      KFLD=LC
      CALL AIMANT(READAT,ND(NOEL))
      GOTO 998
C----- ELMIR
 84   CONTINUE
      KALC = 3
      KUASEX = 25
      KFLD=LC
      IF(READAT) THEN
        CALL RELMIR(
     >                        ND(NOEL))
      ELSE
        CALL STPSI1(NOEL)
      ENDIF
      IF(FITGET) CALL FITGT1
      CALL QUASEX(ND(NOEL))
      GOTO 998
C----- ELCMIR
 85   CONTINUE
      KALC = 3
      KUASEX = 26
      KFLD=LC
      IF(READAT) THEN
        CALL RELCMI(
     >                        ND(NOEL))
      ELSE
        CALL STPSI1(NOEL)
      ENDIF
      IF(FITGET) CALL FITGT1
      CALL AIMANT(READAT,ND(NOEL))
      GOTO 998
C----- MAP2D-E: 2D E-FIELD MAP
 86   CONTINUE
      KFLD=LC
      GOTO 62
C----- SRPRNT. Print/Store S.R. loss tracking statistics into file
 87   CONTINUE
      CALL SRPRN(J1,NRES,IMAX)
      GOTO 998
C----- BETATRON. Betatron core
 88   CONTINUE
      IF(READAT) READ(NDAT,*) DPKCK
      IF(FITGET) CALL FITGT1
      CALL DPKICK(DPKCK)
      GOTO 998
C----- TWISS. Compute linear lattice functions, chromaticities, etc.
C      Also prints out periodic beta functions to zgoubi.TWISS.out (it sets KOPTCS to 1).
 89   CONTINUE
      IF(OPTIX) CALL ENDJOB('Pgm zgoubi. Sorry, ''OPTICS'' is'
     >//' not compatible with ''TWISS''. Comment one or the other.',-99)
      IF(READAT) CALL RTWISS(NDAT,
     >                            KTW)
      NRBLT = 2*KTW -2  ! for ktw = 1, 2 or 3
      IF(FITBYD) THEN                               ! Execution pointer is beyond FIT
        FITRBL = NRBLT .EQ.0 .OR. IPASS .LE. NRBLT  !  NRBLT is zero at 1st pass, set to MCOH+MCOV+2 by SVDOC below
        CALL FITST8(FITRBL)                         ! Allows FIT embeded in SVDOC
      ENDIF
      CALL TWISS(
     >           KOPTCS,READAT,KTW,LBLOPT(1),PRDIC,KCPLD)
      IF(IPASS.EQ.NRBLT+2) THEN ! Means that loop is completed
        FITRBL = .FALSE.
        CALL FITST8(FITRBL)
      ENDIF            
      IF(IPASS .EQ. 2*KTW+1) READAT = .TRUE.
      GOTO 998
C----- END. End of data list, end of execution.
 90   CONTINUE
      CALL END(
     >         READAT,NOEL,KCSR)
      IF(KCSR.EQ.1) GOTO 998
      GOTO 999
C----- FFAG. FFAG Sector multi-dipole.
 91   CONTINUE
      KALC = 1
      KUASEX = 27
      IF(READAT) THEN
        CALL RFFAG(
     >                        ND(NOEL))
      ELSE
        CALL STPSI1(NOEL)
      ENDIF
      IF(FITGET) CALL FITGT1
      CALL AIMANT(READAT,ND(NOEL))
      GOTO 998
C----- HELIX. Helical field (twisted dipole)
 92   CONTINUE
      KALC = 3
      KUASEX = 28
      IF(READAT) THEN
        CALL RHELIX(
     >                        ND(NOEL))
      ELSE
        CALL STPSI1(NOEL)
      ENDIF
      IF(FITGET) CALL FITGT1
      CALL QUASEX(ND(NOEL))
      GOTO 998
C-----  CSR. Switch  coherent SR interaction
 93   CONTINUE
      IF(READAT) CALL RCSR
      IF(FITGET) CALL FITGT1
      CALL CSRI
      GOTO 998
C----- Each particle is pushed a Delta-S distance
 94   CONTINUE
      IF(READAT) READ(NDAT,*) A(NOEL,1)
      IF(FITGET) CALL FITGT1
      CALL PATH(A(NOEL,1))
      GOTO 998
C----- COILS.
 95   CONTINUE
      KALC = 3
      KUASEX = 29
      IF(READAT) THEN
        CALL RCOILS(NDAT,NOEL,MXL,A,
     >                        ND(NOEL))
      ELSE
        CALL STPSI1(NOEL)
      ENDIF
      IF(FITGET) CALL FITGT1
      CALL QUASEX(ND(NOEL))
      GOTO 998
C----- GETFITVAL.  Get parameter values resulting from FIT, stored in TA(NOEL,1)
 96   CONTINUE
      CALL FITSTA(J5,
     >               FITING)
      IF(.NOT.FITING) THEN
        IF(IPASS.EQ.1) CALL RFITGT
      ENDIF
      CALL FITGTV(FITING,TA(NOEL,1),
     >                              FITGET)
      GOTO 998
C----- SUPERPOSE.  To superimpose magnets. 2B developped
 97   CONTINUE
C      IF(READAT) CALL RSUPER(NDAT,NOEL,MXL,A)
      KUASEX = 31
      IF(FITGET) CALL FITGT1
C      CALL SUPERP
      GOTO 998
C----- MARKER.
 98   CONTINUE
      KUASEX = 0
      IF(LABEL(NOEL,2).EQ.'.plt' .OR. LABEL(NOEL,2).EQ.'.PLT') THEN
        CALL OPEN2('MAIN',NPLT,FILPLT)
        DO IT = 1, IMAX
          TRAD = F(3,IT)*1.D-3
          PRAD = F(5,IT)*1.D-3
          CALL IMPPLB(NPLT,F(2,IT),TRAD,F(4,IT),PRAD,ZERO,
     >    F(6,IT),F(7,IT),ZERO,AMQ(1,IT),AMQ(2,IT),IEX(IT),IT)
        ENDDO
      ENDIF
C FM - Jun 2020. Useless? In any case, problem as BORO not initialized if MARKER prior to [MC]OBJET
C      IF(IPASS.EQ.1 .AND. READAT) CALL SCUMW(NOEL,0.D0)
C      IF(KCO .EQ. 2) CALL SCUMW(NOEL,0.D0)       ! case SVDOC, to locate possible PUs at MARKERs
      CALL SCUMW(NOEL,0.D0)       ! case SVDOC, to locate possible PUs at MARKERs
      GOTO 998
C----- DIPOLES. A set of neiboring or overlapping dipoles.
 99   CONTINUE
      KALC =1
      KUASEX = 32
      IF(READAT) CALL RDIPS(
     >                        ND(NOEL))
      IF(FITGET) CALL FITGT1
      CALL AIMANT(READAT,ND(NOEL))
      GOTO 998
C----- TRACKING.
 100  CONTINUE
        READ(NDAT,*) NLMA, NLMB
        IF(NRES.GT.0)
     >  WRITE(NRES,*) ' Tracking,  NLM_A  ->  NLM_B : ',NLMA,' -> ',NLMB
        CALL TRACK(NLMA,NLMB)
        CALL ENDJOB(' End of job after TRACKING',-99)
      GOTO 998
C----- FFAG-SPI. FFAG, spiral.
 101  CONTINUE
      KALC = 1
      KUASEX = 33
      IF(READAT) THEN
        CALL RFFAG(
     >                        ND(NOEL))
      ELSE
        CALL STPSI1(NOEL)
      ENDIF
      IF(FITGET) CALL FITGT1
      CALL AIMANT(READAT,ND(NOEL))
      GOTO 998
C----- EMMA. Read  2-D or 3-D field map,
C      with mesh either cartesian (KART=1) or cylindrical (KART=2).
 103  CONTINUE
      KALC = 2
      IF(READAT) CALL REMMA(J3,
     >                        ND(NOEL))
      IF    (A(NOEL,22) .EQ. 1) THEN
C------- 2-D map, KZMA = 1
        KUASEX = 34
      ELSEIF(A(NOEL,22) .GT. 1) THEN
C------- 3-D map, KZMA > 1
        KUASEX = 35
        IF(IZ.LE.1) CALL ENDJOB(' *** ERROR ; cannot use a 3-D map, need
     >  recompile zgoubi, using IZ>1 in PARIZ.H',-99)
      ENDIF
      IF(FITGET) CALL FITGT1
      IF    (NINT(A(NOEL,23)) .LT. 20) THEN
Cartesian coordinates, KART=1 -> chxc -> emmac
        CALL QUASEX(ND(NOEL))
      ELSE
Cylindrical coordinates, KART=2 -> chxp -> emmap
        CALL AIMANT(READAT,ND(NOEL))
      ENDIF
      GOTO 998
C----- DIPOLEC. Like DIPOLES, with cartesian coordinates
 104  CONTINUE
      KALC =1
      KUASEX = 36
      IF(READAT) CALL RDIPC(
     >                        ND(NOEL))
      IF(FITGET) CALL FITGT1
      CALL QUASEX(ND(NOEL))
      GOTO 998
C----- REVERSE.
 105  CONTINUE
      CALL REVERS
      GOTO 998
C----- SYSTEM. System call
 106  CONTINUE
C        stop '***************'
C      IF(READAT) READ(NDAT,*) A(NOEL,1)
      LINE = 1
      READ(NDAT,*,END=1069,ERR=1069) A(NOEL,1)
      NCMD = NINT(A(NOEL,1))
      IF(NRES.GT.0) WRITE(NRES,FMT='(5X,''Number  of  commands : '',I3,
     >'',  as  follows : '',/)') NCMD
      DO I = 1, NCMD
        LINE = LINE + 1
        READ(NDAT,FMT='(A)',END=1069,ERR=1069) SYSCMD
        IF(STRCON(SYSCMD,'!'
     >                      ,IS)) SYSCMD = SYSCMD(1:IS-1)
        WRITE(6,*) 'Executing  "',TRIM(SYSCMD),'"'
        CALL SYSTEM(TRIM(SYSCMD))
        IF(NRES.GT.0) WRITE(NRES,*) TRIM(SYSCMD)
      ENDDO
      WRITE(6,*) ' ' 
      GOTO 998
 1069 CALL ENDJOB('*** Pgm zgoubi, keyword '//KLEY//' : '//
     >'input data error, at line #',LINE)
C-----SPINR. Spin rotator
 107  CONTINUE
      IF(READAT) CALL RSPINR
      IF(FITGET) CALL FITGT1
      CALL SPINR
      GOTO 998
C----- BENDTH. Pure dipole field, analytical push.
 108  CONTINUE
      IF(READAT) CALL RBNDTH(
     >                        ND(NOEL))
      IF(FITGET) CALL FITGT1
      CALL BNDTHI(READAT,
     >                   ND(NOEL))
      GOTO 998
C----- AGSMM. AGS main magnet. Works like MULTIPOL + various refinements or specificities.
 109  CONTINUE
      KALC = 3
      KUASEX = 37
      IF(READAT) THEN
        CALL RAGSMM(NDAT,NOEL,MXL,A,
     >                              ND(NOEL))
        AGSMDL = .TRUE.
      ELSE
        CALL STPSI1(NOEL)
      ENDIF
      IF(FITGET) CALL FITGT1
      CALL QUASEX(ND(NOEL))

      GOTO 998
C----- BEAMBEAM.
 110  CONTINUE
      IF(READAT) CALL RBB
      IF(FITGET) CALL FITGT1
      CALL BB
      GOTO 998
C----- AGSQUAD. AGS quadrupole. It has 2 windings, works otherwise like QUADRUPO.
 111  CONTINUE
      KALC = 3
      KUASEX = 38
      IF(READAT) THEN
        CALL RAGSQU(NDAT,NOEL,MXL,A,
     >                        ND(NOEL))
        AGSMDL = .TRUE.
      ELSE
        CALL STPSI1(NOEL)
      ENDIF
      IF(FITGET) CALL FITGT1
      CALL QUASEX(ND(NOEL))
      GOTO 998
C----- SYNRAD.
 112  CONTINUE
      CALL SYNTRK
      GOTO 998
C----- OPTIONS.
 113  CONTINUE
      IF(READAT) CALL ROPTIO(NDAT)
      CALL OPTION
      GOTO 998
C----- EPLATES.
 114  CONTINUE
      KALC = 3
      KUASEX = 39
      IF(READAT) THEN
        CALL REPLAT(
     >                        ND(NOEL))
      ELSE
        CALL STPSI1(NOEL)
      ENDIF
      IF(FITGET) CALL FITGT1
      CALL QUASEX(ND(NOEL))
      GOTO 998
C----- DAMPER.
 115  CONTINUE
      IF(READAT) CALL RDAMPE
      IF(FITGET) CALL FITGT1
      CALL DAMPER
      GOTO 998
C----- CYCLOTRON.
 116  CONTINUE
      KALC =1
      KUASEX = 40
      IF(READAT) CALL RCYCLO(
     >                        ND(NOEL))
      IF(FITGET) CALL FITGT1
      CALL AIMANT(READAT,ND(NOEL))
      GOTO 998
C----- ERRORS.
 117  CONTINUE
      IF(READAT) CALL RERROR
      IF(FITGET) CALL FITGT1
      CALL ERRORS
      GOTO 998
C----- SPACECHARG.
 118  CONTINUE
C      IF(READAT) CALL RSPACH(
C     >                       KSPCH,LBLSC,NLBSC)
C      OKSPCH = KSPCH .EQ. 1
C      CALL SPACH(KSPCH,LBLSC,NLBSC)
       IF(READAT) CALL  RSPACH(MLBSC,
     >                         TSPCH,TLAMBDA,LBLSC,NLBSC)

       IF(NRES .GT. 0) THEN
         IF (TSPCH) THEN
           WRITE(NRES,FMT='(15X,
     >    ''Space charge effect will occur at element[s] labeled : '')')
           WRITE(NRES,FMT='(20X,A)') (LBLSC(I),I=1,NLBSC)

         ELSE
           WRITE(NRES,FMT='(15X,
     >    ''Space charge effect will not occur '')')
         ENDIF
       ENDIF
      GOTO 998
C----- GOTO.
 119  CONTINUE
      IF(READAT) CALL RGOTO(NOEL)
      CALL GOTOL(IPASS,MXKLE,KLE)
      READAT = .TRUE.  ! As READAT may have been set to F, e.g. by REBELOTE.
      GOTO 998
C----- ELLIPTIC.
 120  CONTINUE
      KALC = 3
      KUASEX = 41
      IF(READAT) THEN
        CALL RELLIP(NDAT,NOEL,MXL,A,MPOL,
     >                                   ND(NOEL))
      ELSE
        CALL STPSI1(NOEL)
      ENDIF
      IF(FITGET) CALL FITGT1
      CALL QUASEX(ND(NOEL))
      GOTO 998
C----- SVDOC. Compute SVD matrix. Requires to be preceded by FIT to find orbit
 121  CONTINUE
      IF(READAT) CALL RSVDOC
      IF(FITBYD) THEN                               ! Execution pointer is beyond FIT
        FITRBL = NRBLT .EQ.0 .OR. IPASS .LE. NRBLT  !  NRBLT is zero at 1st pass, set to MCOH+MCOV+2 by SVDOC below
        CALL FITST8(FITRBL)                         ! Allows FIT embeded in SVDOC
      ENDIF
      CALL SVDOC(KLE,LABEL,
     >                     READAT)
      FITBYD = .FALSE.
      CALL FITST4(FITBYD)
      IF(IPASS.EQ.NRBLT+1) THEN   ! Means that loop is completed
        FITRBL = .FALSE.
        CALL FITST8(FITRBL)
      ENDIF
C      CALL REBLT4(121)   !  121 = SVDOC - Unused ???? check. If so: suppress
      GOTO 998           !  could be reminiscence from early devlopmnts of svd in REBELOTE

C-------------------------
C-------------------------
C-------------------------

      ENTRY ZGLMNT(
     >             TXTELO)
      TXTELO = TXTELT
      RETURN
      ENTRY ZGKLEY(
     >             KLEO)
C Current KLEY
      KLEO = KLEY(DEBSTR(KLEY):FINSTR(KLEY))
      RETURN
      ENTRY ZGLBL(
     >            LBL1,LBL2)
C Current LABEL_1, _2
      LBL1 = LABEL(NOEL,1)
      LBL2 = LABEL(NOEL,2)
      RETURN
      ENTRY ZGMXKL(
     >             MXKLEO)
C Size of KLE array
      MXKLEO = MXKLE
      RETURN
      ENTRY ZGNOEL(
     >             NOELO)
C Current elmnt #
      NOELO = NOEL
      RETURN
      ENTRY ZGKLE(IKL,
     >                KLEO)
C KLEY[IKL]
      IF(IKL.LE.0) THEN
        KLEO = 'UnknownKLE'
      ELSE
        KLEO = KLE(IKL)(DEBSTR(KLE(IKL)):FINSTR(KLE(IKL)))
      ENDIF
C      KLEO = KLE(IKL)
      RETURN
      ENTRY ZGNBLM(
     >             NBLMNO)
C Number of elements in the sequence
      NBLMNO = NBLMN
      RETURN
      ENTRY ZGPNLT(
     >             PNLTGO)
C Target penalty in FIT[2]
      PNLTGO = PNLTGT
      RETURN
      ENTRY ZGIPAS(
     >             IPASSO,NRBLTO)
C Current pass #
      IPASSO = IPASS
      NRBLTO = NRBLT
      RETURN
      ENTRY ZGTITR(
     >             TITRO)
      TITRO = TITRE
      RETURN
      ENTRY ZGIMAX(
     >             IMAXO)
C Number of particles being tracked
      IMAXO = IMAX
      RETURN
      ENTRY ZGIRET(
     >             IRETO)
C Number of particles being tracked
      IRETO = IRET
      RETURN
      ENTRY ZGAGSM(
     >             AGSMDLO)
C Flip-fop TWISS on/off
      AGSMDLO = AGSMDL
      RETURN

      END
