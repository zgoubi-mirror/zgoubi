C  ZGOUBI, a program for computing the trajectories of charged particles
C  in electric and magnetic fields
C  Copyright (C) 1988-2007  François Méot
C
C  This program is free software; you can redistribute it and/or modify
C  it under the terms of the GNU General Public License as published by
C  the Free Software Foundation; either version 2 of the License, or
C  (at your option) any later version.
C
C  This program is distributed in the hope that it will be useful,
C  but WITHOUT ANY WARRANTY; without even the implied warranty of
C  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C  GNU General Public License for more details.
C
C  You should have received a copy of the GNU General Public License
C  along with this program; if not, write to the Free Software
C  Foundation, Inc., 51 Franklin Street, Fifth Floor,
C  Boston, MA  02110-1301  USA
C
C  François Méot <fmeot@bnl.gov>
C  Brookhaven National Laboratory
C  C-AD, Bldg 911
C  Upton, NY, 11973, USA
C  -------
      SUBROUTINE SVDPUS(NBLM,HPNA,VPNA,HVPNA,
     >                              NAMPU,LPUF,NPUH,NPUV,NPUHV,KHV)
C     >                                 MPUL,MPUH,MPUV,MPUHV)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C     -----------------------------------------------------
C     Find pickups from A() list. Log their labels in PULAB
C     -----------------------------------------------------
      CHARACTER(*) HPNA(*), VPNA(*), HVPNA(*)
      CHARACTER(*) NAMPU(*)
      INCLUDE 'C.CDF.H'         ! COMMON/CDF/ IES,LF,LST,NDAT,NRES,NPLT,NFAI,NMAP,NSPN,NLOG
      PARAMETER (MXPUD=9,MXPU=1000)
      INCLUDE 'C.CO.H'     ! COMMON/CO/ FPU(MXPUD,MXPU),KCO,NPUF,NFPU,IPU
      PARAMETER (MPUF=6)
      PARAMETER (LBLSIZ=20)
      CHARACTER(LBLSIZ) PULAB
      INCLUDE 'C.COT.H'     ! COMMON/COT/ PULAB(MPUF)
      INCLUDE 'MXLD.H'
      INCLUDE 'C.DON.H'     ! COMMON/DON/ A(MXL,MXD),IQ(MXL),IP(MXL),NB,NOEL
      CHARACTER(LBLSIZ) LABEL
      INCLUDE 'C.LABEL.H'     ! COMMON/LABEL/ LABEL(MXL,2)
      INCLUDE 'C.REBELO.H'   ! COMMON/REBELO/ NRBLT,IPASS,KWRT,NNDES,STDVM

      LOGICAL OKPU
      PARAMETER (IMON=MPUF/3)
      PARAMETER(MXPUH =IMON, MXPUV =IMON, MXPUHV =IMON)
      LOGICAL DEJA
      DIMENSION KHV(MXPU)    ! 1, 2, 3 FOR H V, HV

      PARAMETER (KSIZ=10)
      CHARACTER(KSIZ) KLE
      CHARACTER(6) TXT

      CHARACTER(LBLSIZ) LBL2
      LOGICAL EMPTY

      DATA OKPU / .FALSE. /

      OKPU = .FALSE.
      LPUF = 0      ! Numb of PU families/labels
      NPUH = 0      ! Numb of H PUs
      NPUV = 0
      NPUHV = 0
      JPU = 0       ! total numb of PUs
      NLM = 1

      DO WHILE ((.NOT. OKPU) .AND. NLM .LE. NBLM)
C Move to next element in sequence. Test whether its label identifies w/ PU label.
        I = 1
        DO WHILE((.NOT. OKPU) .AND. I.LE.MXPUH)
          OKPU = OKPU .OR. (LABEL(NLM,1).EQ.HPNA(I))
     >                .OR. (LABEL(NLM,1).EQ.VPNA(I))
     >                .OR. (LABEL(NLM,1).EQ.HVPNA(I))
          IF(OKPU) THEN
            JPU = JPU + 1
            CALL ZGKLE(IQ(NLM),
     >                         KLE)
            WRITE(TXT,FMT='(I0)') NLM
            LBL2 = LABEL(NLM,2)
            IF(EMPTY(LBL2)) LBL2 = '.'
            NAMPU(JPU)=KLE//' '//LABEL(NLM,1)//' '//LBL2//' '
     >      //TXT
            IF    (LABEL(NLM,1).EQ.HPNA(I)) THEN
              KHV(JPU) = 1
              NPUH = NPUH + 1
            ELSEIF(LABEL(NLM,1).EQ.VPNA(I)) THEN
              KHV(JPU) = 2
              NPUV = NPUV + 1
            ELSEIF(LABEL(NLM,1).EQ.HVPNA(I)) THEN
              KHV(JPU) = 3
              NPUHV = NPUHV + 1
            ENDIF
          ENDIF
          I = I + 1
        ENDDO

        IF(OKPU) THEN
          J = 1
          DEJA = .FALSE.
          DO WHILE (.NOT. DEJA .AND. J.LE. MPUF)
            DEJA = DEJA .OR. (PULAB(J) .EQ. LABEL(NLM,1))
            J = J+1
          ENDDO

          IF(.NOT. DEJA ) THEN
            LPUF = LPUF + 1
            IF(LPUF .GT. MPUF) CALL ENDJOB(
     >      'Pgm svdpus.  Too many PU families: ',MPUF)
            PULAB(LPUF) = LABEL(NLM,1)
          ENDIF
          OKPU = .FALSE.

        ENDIF

        NLM = NLM + 1
      ENDDO

      OKPU=.FALSE.

      IF(LPUF.LE.0) CALL ENDJOB('Pgm svdpus.  None of the'
     >//' PU families listed under SVDOC appears in the sequence. '
     >//'Check PU family names and existence of corresponding label_1.'
     >,-99)
      IF(JPU.GT.MXPU)
     >CALL ENDJOB('Pgm svdpus. Too many PUs.'
     >//' Need re-size FPU.  Max allowed is ',MXPU)

      CALL SVDPR2(NPUH,NPUV,NPUHV,KHV)
      RETURN
      END
