      implicit double precision (a-h,o-z)
      character(100) cmnd
      character(132) txt1, txt6, txtSnk(6)
      dimension snk(6), ds(6)
      parameter (cm2m=1.d-2)
      
      data snk /  125.20250d2, 763.29700d2, 1404.02110d2,
     >     2041.69370d2, 2680.65130d2, 3319.64227d2/

C  insertSnakes.in       should be zgoubi.res
      cmnd = 'grep -B5 '//'''TRAJ #1 IEX,D,Y,T,'''//
     >' insertSnakes.in | cat > grepS.out'
      write(*,*) ' Now doing ',trim(cmnd)
      call system(cmnd)

      open(unit=1,file='grepS.out')

      isn = 1
      il = 0
 1    continue
      read(1,fmt='(a)',err=99,end=99) txt1 ; il = il+1
      read(1,*); read(1,*); read(1,*); read(1,*) ; il = il+4
      read(1,fmt='(a)') txt6 ; il = il+1
      write(*,fmt='(a)') txt6
      read(txt6(104:),*) s
      if(s.gt. snk(isn)) then
        if(isn .le. 6) then
          ds(isn) = s - snk(isn)
          write(*,*) 'Snake #',isn,'. Next DRIFT ends at  s=',s,' cm'
          write(*,*) trim(txt1)
          write(*,*) 'Snake installed ',ds(isn),' cm upstream,'
     >    ,' at   s = ',s - ds(isn),' cm       (',snk(isn),')'
C          read(*,*)
          txtSnk(isn)=trim(txt1)
          write(*,*) ' Prune : ',txt1(29:)
          isn = isn +1
        endif
      endif
      if(isn .eq. 7) goto 88
      read(1,*,err=99,end=99) ; il = il+1

      goto 1

 88   continue
      write(*,*) ' Done counting snakes!'
      write(*,*) ' Read il ',il,' lines from grepS.out'

      write(*,*) ' Snake list: '
      write(*,fmt='(a,/,f10.4)') (trim(txtSnk(isn)),ds(isn),isn=1,6)
      read(*,*)
      
      call install(ds,txtSnk)
      
 2    continue
        read(3,fmt='(a)',end=89,err=89) txt1
        write(2,fmt='(a)') trim(txt1)
      goto 2
      
 89   continue
      write(*,*) ' Dome installing snakes !'
      stop

      
 99   continue
      write(*,*) ' Problem !'
      stop
       end

      subroutine install(ds,txt)
      implicit double precision (a-h,o-z)
      dimension ds(*)
      character(*) txt(*)
      character(200) txtR, txtR2
      integer debstr, finstr
      character(20) txtNum,txtb,name
      logical strcon
      
      open(unit=3,file='insertSnakes.in')
      open(unit=2,file='insertSnakes.out')
      
      do isn = 1, 6

        read(txt(isn),*) txtNum
        read(txt(isn)(29:),*) txtb,txtb,name
        write(*,fmt='(2a)') ' txtNum : ',txtNum
        read(*,*)
        
 1      continue
          read(3,fmt='(a)',err=99,end=99) txtR
          write(*,fmt='(a)') trim(txtR(50:))//' '//trim(txtNum)
C          read(*,*)
          if(strcon(txtR(50:),trim(txtNum),jj)) then
             write(*,*) 
             write(*,*) txt(isn)
             write(*,*) ' Snake #',isn,'  here !   ds= ',ds(isn)
             read(3,fmt='(a)') txtR2
             read(txtR2,*) xl
             write(2,*) '''DRIFT''  SnakeFollows'
             write(2,*) ds(isn)
             write(2,fmt='(a12,i0)') '''SPINR''  SNK',isn
             write(2,fmt='(a)') '1'
             write(2,fmt='(1p,e12.4,a)') (-1)**isn * 45., '  180. '
             write(2,*) '''DRIFT'''
             write(2,*) xl - ds(isn),'   ! ',xl
C             write(2,*) txtR
C             write(2,*) txtR2
             read(*,*)
             goto 2
          endif
          write(2,fmt='(a)') trim(txtR)
          goto 1
          
 2      continue
        
      enddo
      
      return

 99   continue
      close(3) ; close(2)
      stop
      end
      
      FUNCTION DEBSTR(STRING)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INTEGER DEBSTR
      CHARACTER(*) STRING
C     --------------------------------------
C     RENVOIE DANS DEBSTR LE RANG DU
C     1-ER CHARACTER NON BLANC DE STRING,
C     OU BIEN 0 SI STRING EST VIDE ou BLANC.
C     --------------------------------------
      II=0
      LENGTH=LEN(STRING)
      IF(LENGTH.LE.0) GOTO 99
1     CONTINUE
        II=II+1
        IF (STRING(II:II) .EQ. ' ') THEN
          IF(II .GE. LENGTH) THEN
            II = 0
            GOTO 99
          ELSE
            GOTO 1
          ENDIF
        ENDIF
 99   CONTINUE
      IF(II .EQ. 0) THEN
        STRING = ' '
        II = 1
      ENDIF
      DEBSTR = II
      RETURN
      END

      FUNCTION FINSTR(STRING)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INTEGER FINSTR
      CHARACTER(*) STRING
C     --------------------------------------
C     RENVOIE DANS FINSTR LE RANG DU
C     DERNIER CHARACTER NON BLANC DE STRING,
C     OU BIEN 0 SI STRING EST VIDE ou BLANC.
C     --------------------------------------

      FINSTR=LEN(STRING)+1
1     CONTINUE
        FINSTR=FINSTR-1
        IF(FINSTR .EQ. 0) GOTO 99
        IF (STRING(FINSTR:FINSTR) .EQ. ' ') GOTO 1

 99   CONTINUE

      IF(FINSTR.EQ.0) THEN
        FINSTR = 1
        STRING = ' '
      ENDIF
      RETURN
      END

      FUNCTION STRCON(STR,STR2,
     >                         IS)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL STRCON
      CHARACTER(*) STR, STR2
C     ---------------------------------------------------------------
C     .TRUE. if the string STR contains the string STR2 at least once
C     IS = position of first occurence of STR2 in STR 
C     (i.e.,STR(IS:IS+LEN(STR2)-1)=STR2)
C     ---------------------------------------------------------------
      INTEGER DEBSTR,FINSTR
      LNG2 = LEN(STR2(DEBSTR(STR2):FINSTR(STR2)))
      IF(LEN(STR).LT.LNG2 .OR.
     >   (DEBSTR(STR).EQ.0 .AND. FINSTR(STR).EQ.0)
     >     ) GOTO 1
      DO I = DEBSTR(STR), FINSTR(STR)-LNG2+1
        IF( STR(I:I+LNG2-1) .EQ. STR2 ) THEN
          IS = I 
          STRCON = .TRUE.
          RETURN
        ENDIF
      ENDDO
 1    CONTINUE
      STRCON = .FALSE.
      RETURN
      END
      
