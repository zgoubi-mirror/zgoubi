reset
 
set xlabel "Turn"
set ylabel "{/Symbol bg e}_x  [{/Symbol p}m]" 
set y2label "{/Symbol bg e}_y  [{/Symbol p}m]" 

set title "Normalized emittance" 
 
set xtics nomirror  # font "roman,11"
set ytics nomirror  # font "roman,11"
set y2tics nomirror  # font "roman,11"
 
set key t r 

set logscale y 10 
set logscale y2 10 

set format y "%.2t*10^{%+03T}"
set format y2 "%.2t*10^{%+03T}"

M = 938.27208

plot [2:] \
    "./lipsFitFromFai_iterate.Out" every 9 u 11:($3 *sqrt($13**2-M**2)/M) axes x1y1 w lp ps .6 pt 4 lc rgb "red" tit '{/Symbol e}_x' ,\
    "./lipsFitFromFai_iterate.Out" every 9 u 11:($4 *sqrt($13**2-M**2)/M) axes x1y2 w lp ps .6 pt 5 lc rgb "blue" tit '{/Symbol e}_y' 

set terminal postscript eps blacktext color enh 
 set output "gnuplot_lipsFromFai_e_xy.eps" 
 replot 
 set terminal X11 
 unset output 

pause 1

set ylabel '{/Symbol s}_x,  {/Symbol s}_y' 
set y2label '{/Symbol s}_l'

set key c r 

plot [2:] \
    "./lipsFitFromFai_iterate.Out"  every 9 u 11:($19) axes x1y1 w lp ps .6 pt 4 lc rgb "red"  tit '{/Symbol s}_x' ,\
    "./lipsFitFromFai_iterate.Out"  every 9 u 11:($20) axes x1y1 w lp ps .6 pt 5 lc rgb "blue"  tit '{/Symbol s}_y' ,\
    "./lipsFitFromFai_iterate.Out"  every 9 u 11:($21) axes x1y2 w lp ps .6 pt 6 lc rgb "green"  tit '{/Symbol s}_l' 

set terminal postscript eps blacktext color enh 
 set output "gnuplot_lipsFromFai_sig_xyl.eps" 
 replot 
 set terminal X11 
 unset output 

pause 1
exit
