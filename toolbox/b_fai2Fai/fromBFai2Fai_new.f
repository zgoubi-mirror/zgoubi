      implicit double precision (a-h,o-z)
      CHARACTER(270) TXTN
      dimension FO(7), f(7), SI(4), SF(4)
      PARAMETER (KSIZ=10)
      CHARACTER(ksiz) KLEY
      parameter (LBLSIZ=20)
      CHARACTER(lblsiz) LBL1,LBL2
      CHARACTER(1) LET
      CHARACTER(1) TX1
      PARAMETER (TX1=' ')
      CHARACTER(10) DMY
      CHARACTER(9) HMS
      
      parameter (lr = 1 , lw = 2 )

      OPEN(UNIT=lr,file='b_zgoubi.fai',FORM='UNFORMATTED',ERR=97)
      open(unit=lw,file='fromBFai2Fai.out')

C read/write header      
      DO I=1, 4
        READ(lr,ERR=88,END=89) TXTN
        WRITE(lw,FMT='(A)') TRIM(TXTN)
        WRITE(6,FMT='(A)') TRIM(TXTN)
      ENDDO

      write(*,*) ' '
      write(*,*) ' wait - now busy copy-pasting...'

      line = 0
 1    continue
      
            READ(lr,ERR=99,END=10) 
     >      KEX,(FO(J),J=1,7),
     >      (F(J),J=1,7), 
     >      (SI(J),J=1,4),(SF(J),J=1,4),
     >      ENEKI, ENERG, 
     >      IT, IREP, SORT, AMQ1,AMQ2,AMQ3,AMQ4,AMQ5, RET, DPR, PS,
     >      BORO, IPASS, NOEL ,KLEY,LBL1,LBL2,LET,
     9      SRLT, DPREF,HDPRF,DMY,HMS
        
          WRITE(lw,110)
     >      KEX,(FO(J),J=1,7),
     >      (F(J),J=1,7), 
     >      (SI(J),J=1,4),(SF(J),J=1,4),
     >      ENEKI, ENERG, 
     >      IT, IREP, SORT, AMQ1,AMQ2,AMQ3,AMQ4,AMQ5, RET, DPR, PS,
     >      BORO, IPASS, NOEL,
     8    TX1,KLEY,TX1,TX1,LBL1,TX1,TX1,LBL2,TX1,TX1,LET,TX1,
     9    SRLT, DPREF,HDPRF,DMY,HMS

          line = line + 1
          if(100000*(line/100000) .eq. line) write(*,*)
     >    '# of lines read from  b_zgoubi.fai file : ',line
          
          INCLUDE "FRMFAI.H"
          
      goto 1

 10   continue

      write(*,*) 'Read ',line,' lines from b_zgoubi.fai file.'
      stop ' End of job. Output in fromBFai2Fai.out'

 97   stop ' Error open b_zgoubi.fai'

 99   stop ' Error during read'

 88   stop ' EOF while reading header'

 89   stop ' Error while reading header'
      end
      
