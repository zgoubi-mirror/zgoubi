
set title "Tune dependence on energy, from zgoubi.MATRIX.out"  # font "roman,26"
 
set xlabel "kin. E [MeV] \n "  # font "roman,24"
set ylabel "{/Symbol n}_x, ({/Symbol n}_x^2+{/Symbol n}_y^2)^{1/2} "  # font "roman,24"
set y2label "{/Symbol n}_y"  # font "roman,24"

set key maxrow 1

 set xtics nomirror  # font "roman,18"
 set ytics nomirror   # font "roman,18"
 set y2tics nomirror  # font "roman,18"

BORO = 64.62444403717985     # kG.cm
am = 938.27203e6
c = 2.99792458e8
BrhoRef = BORO *1e-3   # T.m
eV2MeV = 1e-6

plot "zgoubi.MATRIX.out" u ((sqrt(($47*BrhoRef*c)**2 + am*am)-am)*eV2MeV):($56) w lp pt 5 lt 1 lw .5 lc rgb "red" tit "{/Symbol n}_x " ,\
"zgoubi.MATRIX.out" u ((sqrt(($47*BrhoRef*c)**2 + am*am)-am)*eV2MeV):($57) axes x1y2 w lp pt 6 lt 3 lw .5 lc rgb "blue" tit "{/Symbol n}_y " ,\
"zgoubi.MATRIX.out" u ((sqrt(($47*BrhoRef*c)**2 + am*am)-am)*eV2MeV):(sqrt($56**2+$57**2)) w lp pt 7 lt 1 lw .5 lc rgb "black" tit "    ({/Symbol n}_x^2+{/Symbol n}_y^2)^{1/2}"

 set terminal postscript eps blacktext color enh   # "Times-Roman" 12
 set output "gnuplot_MATRIX_Qxy.eps"
 replot
 set terminal X11
 unset output

pause 1

exit

