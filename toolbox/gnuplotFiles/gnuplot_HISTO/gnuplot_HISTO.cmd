
reset
 
set xlabel "x-axis" font "roman,18"
set ylabel "count" font "roman,18"

set title "Histogram"
 
set xtics mirror font "roman,11"
set ytics mirror font "roman,11"
 
set key  t c font "roman,10"  

plot \
    'zgoubi.HISTO.out'  u ($1):($2) axes x1y1 w l notit 

set terminal postscript eps blacktext color enh size 8cm,5cm "Times-Roman" 12 
 set output "gnuplot_HISTO.eps" 
 replot 
 set terminal X11 
 unset output 

pause 1
exit


