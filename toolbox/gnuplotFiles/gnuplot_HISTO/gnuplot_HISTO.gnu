
reset
 
set xlabel "x-axis" 
# set xlabel "Y [cm]" 
set ylabel "count" 

set title "Histogram"
 
set xtics mirror 
set ytics mirror 
 
set key  t r 

#set xrange [-7:7]
set xrange [*:*]

plot for [i=1:3] \
    'zgoubi.HISTO.out'  u ($9==i? $1 :1/0):($2) axes x1y1 w lp ps .8 tit 'Histo '.i

set terminal postscript eps blacktext color enh 
 set output "BLine_gnuplot_HISTO.eps" 
 replot 
 set terminal X11 
 unset output 

pause 8
exit


