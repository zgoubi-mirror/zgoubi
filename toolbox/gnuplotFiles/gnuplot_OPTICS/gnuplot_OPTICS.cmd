
 set title "Orbits, from zgoubi.OPTICS.out"   # font "roman,16"   # offset 0,+.7    

 set xlabel "s [m]"       # font "roman,16"   # offset +4,-.5 rotate by +20  
 set ylabel "Y, Z [m]"            # font "roman,13"   #offset -0,-1 rotate by -20 
# set y2label "{/Symbol h}_Y, {/Symbol h}_Z"         # font "roman,13"   #offset -0,-1 rotate by -20 

 set xtics mirror # font "roman,12"
 set ytics mirror # font "roman,12"    #offset 0,-.6

set key t l maxrows 1 width 4
set key samplen 1  

Circ = 0. 

unset grid

#plot [:7300] [-.04:.04] \

plot [:] [:] \
     'zgoubi.OPTICS.out'  u ($13 + ($30 -1)*Circ):15  w l lc rgb "red"  tit "Y orbit"  ,\
     'zgoubi.OPTICS.out'  u ($13 + ($30 -1)*Circ):17  w lp ps .6 lc rgb "blue"  tit "Z orbit"

set terminal postscript eps blacktext color enh  # size 8.3cm,4cm "Times-Roman" 12 
 set output "gnuplot_OPTICS_xy.eps" 
 replot 
 set terminal X11 
 unset output 

pause .5

 set title "{/Symbol b}_{Y,Z}, {/Symbol h}_{Y,Z}, from zgoubi.OPTICS.out"   # font "roman,16"   # offset 0,+.7    

 set xlabel "s [m]"       # font "roman,16"   # offset +4,-.5 rotate by +20  
 set ylabel "{/Symbol b}_Y, {/Symbol b}_Z  [m]"         # font "roman,13"   #offset -0,-1 rotate by -20 
 set y2label "{/Symbol h}_Y, {/Symbol h}_Z [m]"         # font "roman,13"   #offset -0,-1 rotate by -20 

 set xtics # font "roman,12"   
 set ytics  nomirror    # font "roman,12"
 set y2tics  nomirror   # font "roman,12"     #offset 0,-.6

set key t c maxrows 1 width 4
set key   samplen 1  

unset grid
 
# "every 3" in this plot assumes there are 3 different label classes stored in zgoubi.OPTICS.out
plot [:] \
     'zgoubi.OPTICS.out' every 1  u ($13 + ($30 -1)*Circ):($2)  w l lc rgb "red" lt 1 lw 2. tit "{/Symbol b}_Y"  ,\
     'zgoubi.OPTICS.out' every 1  u ($13 + ($30 -1)*Circ):($4)  w lp ps .6 lc rgb "blue"  lt 1 lw 2. tit "{/Symbol b}_Z"  ,\
     'zgoubi.OPTICS.out' every 1  u ($13 + ($30 -1)*Circ):7  axes x1y2 w l lc rgb "black" lt 2  tit "{/Symbol h}_Y"  ,\
     'zgoubi.OPTICS.out' every 1  u ($13 + ($30 -1)*Circ):9  axes x1y2 w lp ps .6 lc rgb "green" lt 2 tit "{/Symbol h}_Z"

set terminal postscript eps blacktext color enh # size 8.3cm,4cm "Times-Roman" 12 
 set output "gnuplot_OPTICS_bxyDxy.eps" 
 replot 
 set terminal X11 
 unset output 

pause .5

 set title "H and V determinants, from zgoubi.OPTICS.out"   # font "roman,16"   # offset 0,+.7    
# set title "H and V determinants and momentum \n from zgoubi.OPTICS.out"   # font "roman,16"   # offset 0,+.7    

 set xlabel "s [m]"       # font "roman,16"   # offset +4,-.5 rotate by +20  
 set ylabel "Y, Z determinants"            # font "roman,13"   #offset -0,-1 rotate by -20 
 unset y2label # "p [MeV/c]"         # font "roman,13"   #offset -0,-1 rotate by -20 

 set xtics  nomirror # font "roman,12"   
# set x2tics # font "roman,12" 
 set ytics  mirror # font "roman,12"   
 unset y2tics

set key t c maxrows 1 width 4
set key   samplen 1  
set key maxrow 1

unset grid

p0 = 123.

#plot [2000:2250] \

plot [:] \
     'zgoubi.OPTICS.out' u ($13/100. + ($30 -1)*Circ):(1.-($33*$36-$34*$35))  w l lc rgb "red"  tit '1-Det_Y'  ,\
     'zgoubi.OPTICS.out' u ($13/100. + ($30 -1)*Circ):(1.-($37*$40-$38*$39))  w lp ps .6 lc rgb "blue"  tit '1-Det_Z' ,\

set terminal postscript eps blacktext color enh   # size 8.3cm,4cm "Times-Roman" 12 
 set output "gnuplot_OPTICS_DetYZ.eps" 
 replot 
 set terminal X11 
 unset output 

pause .5

exit 
