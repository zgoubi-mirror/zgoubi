
 set title "S_y at end of AtR, from zgoubi.SPNPRT.Out \n Energy scan using REBELOTE"    font "roman,16"   # offset 0,+.7    

 set xlabel "G{/Symbol g}"        font "roman,16"   # offset +4,-.5 rotate by +20  
 set x2label "Kin. E [GeV/u]"        font "roman,16"   # offset +4,-.5 rotate by +20  
 set ylabel "S_y"             font "roman,13"   #offset -0,-1 rotate by -20 

 set xtics  font "roman,12"   nomirror
 set x2tics  font "roman,12"   nomirror
 set ytics  font "roman,12"   mirror      #offset 0,-.6

set key c l maxrows 1 width 4
set key font "roman, 12"  samplen 1  

am = 938.27203e6
A = 1.    
amu = am/A
G = 1.79283735
eV2GeV = 1e-9

#Ek1u= 10. ; Ek2u=14.  # GeV/u
#Gg1 = G*(Ek1u + amu*eV2GeV)/(amu*eV2GeV) ; Gg2 = G*(Ek2u + amu*eV2GeV)/(amu*eV2GeV)

Gg1 = 40. ; Gg2 = 50.
Ek1u = (Gg1/G -1.)* am *eV2GeV ; Ek2u = (Gg2/G -1.)* am *eV2GeV

set xrange [Gg1:Gg2]
set x2range [Ek1u:Ek2u]

plot \
     'zgoubi.SPNPRT.Out'  u 18:15  w p pt 7 ps .4 lc rgb "red"  tit "S_y"  ,\
     'zgoubi.SPNPRT.Out'  u (($18/G-1.)*amu *eV2GeV):15 axes x2y1  w p pt 7 ps .4 lc rgb "red"  notit

set terminal postscript eps blacktext color enh size 8.3cm,4cm "Times-Roman" 12 
 set output "gnuplot_Gg-SZ.eps" 
 replot 
 set terminal X11 
 unset output 

pause 2

exit
