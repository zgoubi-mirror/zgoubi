
set key maxcol 1
set key t c

set tit "Spin component(s) vs. s  \n from Zfai"  

set xtics 
set ytics 

set xlabel 's' 
set ylabel 'dn_X,  dn_Y,  dn_Z' 

# ELECTRON : 
# am = 0.511
# G = 1.1596e-3
# PROTON :
am = 938.27203e6
G = 1.79284735

m2cm = 100.
MeV2eV = 1e6
c = 2.99792458e8

dp = 1e-4

plot  \
   "< paste  n_dp0.fai  n_dp1e-4.fai n_dp-1e-4.fai" u ($14):( ($73 - $126)/(2.*dp)) w lp pt 5 ps .1  tit "{/Symbol d}n_s" ,\
   "< paste  n_dp0.fai  n_dp1e-4.fai n_dp-1e-4.fai" u ($14):( ($74 - $127)/(2.*dp)) w lp pt 5 ps .1  tit "{/Symbol d}n_x" ,\
   "< paste  n_dp0.fai  n_dp1e-4.fai n_dp-1e-4.fai" u ($14):( ($75 - $128)/(2.*dp)) w lp pt 5 ps .1  tit "{/Symbol d}n_y"

     set terminal postscript eps blacktext color  enh  
       set output "gnuplot_Zfai_s-SXYZ.eps"  
       replot  
       set terminal X11  
       unset output  

 
pause 1
exit

