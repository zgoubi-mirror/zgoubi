
set key maxcol 1
set key t l

#set logscale y 

set tit 'Y(s), from zgoubi.fai'

set xtics 
set ytics 

set xlabel 's  [m]'
set ylabel 'Y  [m]'

#! electron : 
#am = 0.511
#! proton :
am = 938.27203
m2cm = 100.
cm2m = 1/100.
MeV2eV = 1e6
c = 2.99792458e8
B = 0.5   # [T]
rho(x) = m2cm * x/B    # [cm]

IT = 1

plot  \
   'zgoubi.fai' u ($26==IT ? $14*cm2m :1/0):($10*cm2m) w lp pt 5 ps .6 lc rgb "red"  tit "Y(s)" ,\
   'zgoubi.fai' u ($26==IT ? $14*cm2m :1/0):($12*cm2m) w lp pt 5 ps .6 lc rgb "blue"  tit "Z(s)"

# 'zgoubi.fai' u ($26==1 ? $14*cm2m :1/0):($10*cm2m) w lp pt 5 ps .8 lc 1  tit "Y(s)"
#'zgoubi.fai' u ($26==11? $14*cm2m :1/0):($10*cm2m) w lp pt 6 ps 1.4 lc 2  tit "D_x dp/p"

     set terminal postscript eps blacktext color  enh
       set output "gnuplot_Zfai_YZ-s.eps"  
       replot  
       set terminal X11  
       unset output  
 
pause 3
exit

