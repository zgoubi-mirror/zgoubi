# set key maxcol 1
set key t l

#set logscale y 

set tit "l-dp space, from Zfai."

set xtics 
set x2tics 
set ytics 

set xlabel '{/Symbol d}t [ns]'
set x2label '{/Symbol d}l [m]'
set ylabel '{/Symbol d}p/p'

#plot   \
# do  for [i=0:4] { 

pi = 4. * atan(1.)
fRF = 9.e6
omga = 2. *pi * fRF
s2ns = 1e9
c = 2.99792458e8

set xrange [-20:20]
set x2range [-20/s2ns*c:20/s2ns*c]

plot \
   'Run0/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run1/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run2/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run3/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run4/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run5/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run6/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run7/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run8/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run9/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run10/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run11/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run12/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run13/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run14/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run15/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run16/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run17/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run18/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run19/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run20/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run21/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run22/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run23/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run24/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run25/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run26/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run27/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run28/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run29/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run30/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run31/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run32/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run33/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run34/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run35/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run36/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run37/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run38/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run39/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run40/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run41/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run42/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run43/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run44/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run45/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run46/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run47/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run48/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run49/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run50/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run51/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run52/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run53/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run54/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run55/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run56/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run57/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run58/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit ,\
   'Run59/Zfai' u (int($38 /7)*7 == $38 ? asin(sin($34))/omga *s2ns : 1/0):($35) w p pt 5 ps .1 lc 1  notit 


     set terminal postscript eps blacktext color  enh size 8.3cm,4cm "Times-Roman" 12  
       set output "gnuplot_fai_ldp.eps"  
       replot  
       set terminal X11  
       unset output  

 
pause 8
exit

