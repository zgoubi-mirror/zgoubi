
set tit "Horizontal phase-space, from Zfai. \n Qx/Qy .73/.28 - 1000-turn DA \n [pathTo]/200116_BD15p40555_BF15_BDH7p09445/searchDA_HighEn/searchDA_HighEn_200205_Qx.73Qy.28/stabLim \n Field map: gap3mm_BD15p40555_BF15p0_BDH7p09445_mesh0p20.table"  font "roman, 10"
set tit "Horizontal phase-space, from Zfai. \n Qx/Qy .73/.28 - 1000-turn DA \n [pathTo]/200116_BD15p40555_BF15_BDH7p09445/searchDA_HighEn/searchDA_HighEn_200205_Qx.73Qy.28/stabLim \n Field map: gap3mm_BD15p40555_BF15p0_BDH7p09445_mesh0p20.table"  font "roman, 10"

set xtics 
set ytics 

set xlabel 'x /m'
set ylabel "x' /rad"

set key maxcol 1
#set key maxrow 1
set key t r

#set logscale y 

#plot   \
# do  for [i=0:4] { 

cm2m = 1e-2
mrd2rd = 1e-3

nPart = 5
mxPass = 300

plot for [i=1:nPart]\
 './Zfai' u ($26==i && $38 < mxPass ? $10 *cm2m :1/0):($11 *mrd2rd):(i) w p ps .9 pt i+5 tit "dp/p=".(i-3)."{/Symbol \264} 10^{-3}"

     set terminal postscript eps blacktext color  enh    # size 8.3cm,4cm "Times-Roman" 12  
       set output "gnuplot_fai_YT.eps"  
       replot  
       set terminal X11  
       unset output  
 
pause .8

plot for [i=1:nPart]\
 './Zfai' u ($26==i && $38 < mxPass ? $12 *cm2m :1/0):($13 *mrd2rd):(i) w p ps .9 pt i+5 tit "dp/p=".(i-3)."{/Symbol \264} 10^{-3}"

     set terminal postscript eps blacktext color  enh    # size 8.3cm,4cm "Times-Roman" 12  
       set output "gnuplot_fai_ZP.eps"  
       replot  
       set terminal X11  
       unset output  
 
pause .8

plot for [i=1:nPart]\
 './Zfai' u ($26==i && $38 < mxPass ? $34 :1/0):($35):(i) w p ps .9 pt i+5 tit "dp/p=".(i-3)."{/Symbol \264} 10^{-3}"

     set terminal postscript eps blacktext color  enh    # size 8.3cm,4cm "Times-Roman" 12  
       set output "gnuplot_fai_phD.eps"  
       replot  
       set terminal X11  
       unset output  
 
pause .8

exit

