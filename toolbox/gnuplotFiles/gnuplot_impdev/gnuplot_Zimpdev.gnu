
Brho = 3.3658103258583867e3 *1e-3  # T.m

      set xtics     mirror # font "roman,11" 
      set ytics     mirror # font "roman,11" 

      set key # font "roman,15" t l
      set key maxcol 1
      set grid


#     set title "Field and derivatives across magnet  \n BZ  "  # font "roman,16"  


      unset ytics 
      set ytics    mirror # font "roman,11" 
      set  nogrid

set xlabel "distance [cm]" # font "roman,14" 
set ylabel "B_Z /B{/Symbol r} [m^{-1}]" # font "roman,14" 

plot \
     'zgoubi.impdev.out' u ($410 ==1? $7 :1/0):($10) w lp ps .04   tit "B_Z Y=1" ,\
     'zgoubi.impdev.out' u ($410 ==2? $7 :1/0):($10) w lp ps .04   tit "    Z=1"

       set terminal postscript eps blacktext color  enh #  size 8.3cm,4.4cm "Times-Roman" 14  
       set output "gnuplot_impdev_BZ.eps"  
       replot  
       set terminal X11  
       unset output  

      unset ytics 
      set ytics     mirror # font "roman,11" 
      unset y2tics
      unset y2label 

pause .5



set ylabel "E_X /B{/Symbol r}" # font "roman,14" 

plot \
     'zgoubi.impdev.out' u ($410 ==1? $7 :1/0):($371) w lp ps .04   tit "E_X Y=1" ,\
     'zgoubi.impdev.out' u ($410 ==2? $7 :1/0):($371) w lp ps .04   tit "    Z=1"

       set terminal postscript eps blacktext color  enh #  size 8.3cm,4.4cm "Times-Roman" 14  
       set output "gnuplot_impdev_EX.eps"  
       replot  
       set terminal X11  
       unset output  

      unset ytics 
      set ytics     mirror # font "roman,11" 
      unset y2tics
      unset y2label 

pause .5



set ylabel "E_Y /B{/Symbol r}" # font "roman,14" 

plot \
     'zgoubi.impdev.out' u ($410 ==1? $7 :1/0):($372) w lp ps .04   tit "E_Y Y=1" ,\
     'zgoubi.impdev.out' u ($410 ==2? $7 :1/0):($372) w lp ps .04   tit "    Z=1"

       set terminal postscript eps blacktext color  enh #  size 8.3cm,4.4cm "Times-Roman" 14  
       set output "gnuplot_impdev_EY.eps"  
       replot  
       set terminal X11  
       unset output  

      unset ytics 
      set ytics     mirror # font "roman,11" 
      unset y2tics
      unset y2label 

pause .5


set ylabel "E_Z /B{/Symbol r}" # font "roman,14" 

plot \
     'zgoubi.impdev.out' u ($410 ==1? $7 :1/0):($373) w lp ps .04   tit "E_Z Y=1" ,\
     'zgoubi.impdev.out' u ($410 ==2? $7 :1/0):($373) w lp ps .04   tit "    Z=1"

       set terminal postscript eps blacktext color  enh #  size 8.3cm,4.4cm "Times-Roman" 14  
       set output "gnuplot_impdev_EZ.eps"  
       replot  
       set terminal X11  
       unset output  

      unset ytics 
      set ytics     mirror # font "roman,11" 
      unset y2tics
      unset y2label 

pause .5



set ylabel "dB_X/dX /B{/Symbol r} [m^{-2}]" # font "roman,14" 

plot 'zgoubi.impdev.out' u 7:11 w lp ps .4 tit 'dB_X/dX'

pause .5

#     set title "Field and derivatives across magnet \n dBY/dX  "  # font "roman,16" 

set ylabel "dB_Y/dX /B{/Symbol r} [m^{-2}]" # font "roman,14" 

plot \
   'zgoubi.impdev.out' u ($410 ==1 ? $7 : 2/0):12 w lp ps .03 lc 1 tit 'dBY/dX Y=1' ,\
   'zgoubi.impdev.out' u ($410 ==2 ? $7 : 2/0):12 w lp ps .03 lc 3 tit '       Z=1'

       set terminal postscript eps blacktext color  enh #  size 8.3cm,4.4cm "Times-Roman" 14  
       set output "gnuplot_impdev_dBYdX.eps"  
       replot  
       set terminal X11  
       unset output  

pause .5

#     set title "Field and derivatives across magnet \n dBZ/dX  "  # font "roman,16" 

set ylabel "dB_Z/dX /B{/Symbol r} [m^{-2}]" # font "roman,14" 

plot \
   'zgoubi.impdev.out' u ($410 ==1 ? $7 : 2/0):13 w lp ps .03 lc 1 tit 'dBZ/dX Y=1' ,\
   'zgoubi.impdev.out' u ($410 ==2 ? $7 : 2/0):13 w lp ps .03 lc 3 tit '       Z=1' 

       set terminal postscript eps blacktext color  enh #  size 8.3cm,4.4cm "Times-Roman" 14  
       set output "gnuplot_impdev_dBZdX.eps"  
       replot  
       set terminal X11  
       unset output  

pause .5

#     set title "Field and derivatives across magnet \n dBX/dY  "  # font "roman,16" 

set ylabel "dB_X/dY /B{/Symbol r} [m^{-2}]" # font "roman,14" 

plot \
   'zgoubi.impdev.out' u ($410 ==1 ? $7 : 2/0):14 w lp ps .03 lc 1 tit 'dBX/dY Y=1' ,\
   'zgoubi.impdev.out' u ($410 ==2 ? $7 : 2/0):14 w lp ps .03 lc 3 tit '       Z=1' 

       set terminal postscript eps blacktext color  enh #  size 8.3cm,4.4cm "Times-Roman" 14  
       set output "gnuplot_impdev_dBXdY.eps"  
       replot  
       set terminal X11  
       unset output  


pause .5

#     set title "Field and derivatives across magnet   "  # font "roman,16" 

set xlabel "distance [cm]" # font "roman,14" 
set ylabel "dB_Y/dY /B{/Symbol r} [m^{-2}]" # font "roman,14" 
plot 'zgoubi.impdev.out' u 7:15 w lp ps .4 tit 'dBY/dY'

pause .5


set xlabel "distance [cm]" # font "roman,14" 
set ylabel "dB_Z/dY /B{/Symbol r} [m^{-2}]" # font "roman,14" 
plot 'zgoubi.impdev.out' u ($7):($16 /Brho) w lp ps .4 tit 'dBZ/dY'

set terminal postscript eps blacktext color  enh  "Times-Roman" 14  
       set output "gnuplot_impdev_dBZdY.eps"  
       replot  
       set terminal X11  
       unset output  

pause .5

set ylabel "dB_X/dZ /B{/Symbol r} [m^{-2}]" # font "roman,14" 
plot 'zgoubi.impdev.out' u 7:17 w lp ps .4 tit 'dBX/dZ'

pause .5

set xlabel "distance [cm]" # font "roman,14" 
set ylabel "dB_Y/dZ /B{/Symbol r} [m^{-2}]" # font "roman,14" 

plot \
'zgoubi.impdev.out' u ($7):($16 /Brho) w lp ps .4 tit 'dBY/dZ' ,\
'zgoubi.impdev.out' u ($7):($18 /Brho) w lp ps .4 tit 'dBZ/dY'


set terminal postscript eps blacktext color  enh  "Times-Roman" 14  
       set output "gnuplot_impdev_dBYdZ.eps"  
       replot  
       set terminal X11  
       unset output  


pause .5

set ylabel "dE_Y/dZ /B{/Symbol r} [m^{-2}]" # font "roman,14" 

plot \
'zgoubi.impdev.out' u ($7):($379 /Brho) w lp ps .4 tit 'dEY/dZ' ,\
'zgoubi.impdev.out' u ($7):($381 /Brho) w lp ps .4 tit 'dEZ/dY'


set terminal postscript eps blacktext color  enh  "Times-Roman" 14  
       set output "gnuplot_impdev_dEYdZ.eps"  
       replot  
       set terminal X11  
       unset output  


pause .5

set ylabel "dB_Z/dZ /B{/Symbol r} [m^{-2}]" # font "roman,14" 

plot 'zgoubi.impdev.out' u 7:19 w lp ps .4 tit 'dBZ/dZ'

pause .5


set ylabel "d^2B_X/dXdZ /B{/Symbol r} [m^{-3}]" # font "roman,14" 

plot 'zgoubi.impdev.out' u 7:20 w lp ps .4 tit 'd2BX/dXdZ'

pause .5

set ylabel "d^2B_X/dX^2 /B{/Symbol r} [m^{-3}]" # font "roman,14" 

plot 'zgoubi.impdev.out' u 7:21 w lp ps .4 tit 'd2BY/dX2'

pause .5

#     set title "Field and derivatives across magnet \n d2BZ/dX2  "  # font "roman,16" 

set ylabel "d^2B_Z/dX^2 /B{/Symbol r} [m^{-3}]" # font "roman,14" 

plot \
   'zgoubi.impdev.out' u ($410 ==1 ? $7 : 2/0):22 w lp ps .03 lc 1 tit 'd2BZ/dX2 Y=1' ,\
   'zgoubi.impdev.out' u ($410 ==2 ? $7 : 2/0):22 w lp ps .03 lc 3 tit '         Z=1

       set terminal postscript eps blacktext color  enh #  size 8.3cm,4.4cm "Times-Roman" 14  
       set output "gnuplot_impdev_d2BZdX2.eps"  
       replot  
       set terminal X11  
       unset output  

pause .5




exit
