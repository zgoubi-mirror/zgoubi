#plot for [i=1:1000] 'data'.i.'.txt' using 1:2 title 'Flow '.i

set title "Plotted from file zgoubi.plt"

set key maxcol 1
set key c l

#set logscale y 

set xtics mirror; set ytics mirror

set size ratio 1

set xlabel 'X  [m]'
set ylabel 'E  [MV/m]'

cm2m = 0.01 ; kG2T= 0.1 ; MeV2eV = 1e6 ; am = 938.27203 ; c = 2.99792458e8

set xrange []
set x2range []

plot \
   'zgoubi.plt' u ($22 *cm2m):($38 *10.) w lp pt 5 ps .4 

     set terminal postscript eps color  enh  
       set output "gnuplot_Zplt_XE.eps"  
       replot  
       set terminal X11  
       unset output  

 
pause 2

exit

