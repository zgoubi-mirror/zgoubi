set xtics mirror
set ytics mirror

set xlabel 'Y  [m]'
set ylabel 'Z  [m]'

set size ratio -1

set grid

cm2m = 0.01
kG2T= 0.1
MeV2eV = 1e6
am = 938.27203
c = 2.99792458e8

# sMin = 37e2 ; sMax = 49e2
sMin = -1e10 ; sMax = 1e10

#plot \
#for [IT=1:1]  'zgoubi.plt' u ($19==IT  ? $10 : 1/0):($12) w lp ps .4 lc rgb "black" 

plot \
for [IT=1:1]  'zgoubi.plt' u ($19==IT &&  $14>sMin && $14<sMax ? $10 : 1/0):($12) w lp ps .4 lc rgb "black" 

     set terminal postscript eps color  enh  
       set output "gnuplot_Zplt_YZ.eps"  
       replot  
       set terminal X11  
       unset output  

 
pause 1
exit

