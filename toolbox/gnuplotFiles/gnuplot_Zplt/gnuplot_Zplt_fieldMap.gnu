
set title "Field map of spiral DF doublet, from zgoubi.plt."

set xlabel 'X [cm]'
set ylabel 'Y [cm]'
set zlabel 'B [kG]'

set xtics  340, 40, 420

set hidden3d
set view 58, 72   #36, 70   #62, 292

 splot "zgoubi.plt"  u \
 ($10*cos($22)):($10*sin($22) < 70? $10*sin($22) :1/0):($25)  \
 with p pt 5 ps .8  lc palette notit

pause 1

 set terminal postscript eps blacktext color
 set output "gnuplot_zgoubi.plt_fieldMap.eps"
 replot

set term X

set title "Field map mesh, from zgoubi.plt."

set xlabel 'X [cm]'
set ylabel 'Y [cm]'

set size ratio -1

 plot "zgoubi.plt"  u ($10*cos($22)):($10*sin($22))  with p pt 4 ps .2  notit

pause 2

 set terminal postscript eps blacktext color
 set output "gnuplot_zgoubi.plt_mapMesh.eps"
 replot

exit
