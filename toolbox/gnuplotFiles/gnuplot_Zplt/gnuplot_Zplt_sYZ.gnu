set xtics mirror
set ytics mirror

set xlabel 's  [m]'
set ylabel 'Y,  Z  [m]'

cm2m = 0.01
kG2T= 0.1
MeV2eV = 1e6
am = 938.27203
c = 2.99792458e8

unset xrange 
# set xrange [2669:2681]    # snake 9'O range
# set xrange [753:764]      # snake 3'O range

plot \
for [i=1:1]  'zgoubi.plt' u ($19== i ? $14 *cm2m : 1/0):($10 *cm2m) w lp ps .4 lc rgb "red" ,\
for [i=1:1]  'zgoubi.plt' u ($19== i ? $14 *cm2m : 1/0):($12 *cm2m) w lp ps .4 lc rgb "blue"
  
     set terminal postscript eps color  enh  
       set output "gnuplot_Zplt_sYZ.eps"  
       replot  
       set terminal X11  
       unset output  

 
pause 3
exit

