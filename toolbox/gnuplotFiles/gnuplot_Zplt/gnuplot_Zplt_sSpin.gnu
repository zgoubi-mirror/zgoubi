
set title "Plotted from file zgoubi.plt "

set key maxrow 3
set key t c 

#set logscale y 

set xtics mirror
set ytics mirror

set xlabel 's  [m]'
set ylabel 'S_X, S_Y, S_Z  [m]'

cm2m = 0.01
MeV2eV = 1e6
am = 938.27203
c = 2.99792458e8

set xrange []
set x2range []

plot for [it=1:5] \
   'zgoubi.plt' u ($19== it ? $22 *cm2m : 1/0):($33) w lp tit 'S_X, prtcl '.i , \
   'zgoubi.plt' u ($19== it ? $22 *cm2m : 1/0):($34) w lp tit 'S_Y' , \
   'zgoubi.plt' u ($19== it ? $22 *cm2m : 1/0):($35) w lp tit 'S_Z' 

     set terminal postscript eps color  enh  
       set output "gnuplot_zgoubi.plt_sSpin.eps"  
       replot  
       set terminal X11  
       unset output  

 
pause 1
exit

