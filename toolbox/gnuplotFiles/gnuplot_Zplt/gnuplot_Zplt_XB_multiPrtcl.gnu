#plot for [i=1:1000] 'data'.i.'.txt' using 1:2 title 'Flow '.i

set title "Plotted from file zgoubi.plt"

set key maxcol 1
set key t r

#set logscale y 

set xtics mirror; set ytics mirror

set size ratio 1

set xlabel 's  [m]'
set ylabel 'B  [T]'

cm2m = 0.01 ; kG2T= 0.1

set xrange []
set x2range []

plot for [i=1:5] \
   'zgoubi.plt' u ($19== i ? $22 *cm2m : 1/0):($25 *kG2T) w lp pt 5 ps .4 tit 'prtcl '.i 

     set terminal postscript eps color  enh  
       set output "gnuplot_Zplt_XB.eps"  
       replot  
       set terminal X11  
       unset output  

 
pause 1

plot  \
   'zgoubi.plt' u ($22):($10) w p pt 6 lc rgb 'red' tit 'R vs. angle'

pause 8
exit

