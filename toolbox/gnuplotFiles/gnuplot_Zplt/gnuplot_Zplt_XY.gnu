#plot for [i=1:1000] 'data'.i.'.txt' using 1:2 title 'Flow '.i

set title "Plotted from file zgoubi.plt "

set key maxrow 3
set key t c 

#set logscale y 

set xtics mirror
set ytics mirror

set size ratio 1

set xlabel 'X  [m]'
set ylabel 'Y  [m]'

cm2m = 0.01
MeV2eV = 1e6
am = 938.27203
c = 2.99792458e8

set xrange []
set x2range []

plot for [i=1:5] \
   'zgoubi.plt' u ($19== i ? $22 *cm2m : 1/0):($10 *cm2m) w lp tit 'prtcl '.i 

     set terminal postscript eps color  enh  
       set output "gnuplot_zgoubi.plt_XY.eps"  
       replot  
       set terminal X11  
       unset output  

 
pause 1

plot  \
   'zgoubi.plt' u ($22):($10) w l lc rgb 'red' tit 'R vs. angle'

pause 1
exit

