
set tit "Tunes and spin parameters scan \n  9 o'clock coil current 320A"

set xlabel "G{/Symbol g}"
set ylabel "{/Symbol n}_x,  {/Symbol n}_y,  {/Symbol n}_{sp},  n_Z"
set y2label "spin rotation"

set xtics
set ytics nomirror
set y2tics

set grid

set key t l  ; set key maxrow 2

r2d = 180./(4.*atan(1.))

set yrange [-1.1:1.1]

plot \
"zgoubi.SPNPRT.Out_cat" u ($18):($51)                w lp pt 7 ps.4 tit "{/Symbol n}_{sp}" ,\
"zgoubi.SPNPRT.Out_cat" u ($18):($50)                w lp pt 8 ps.4 tit "n_Z" ,\
"zgoubi.SPNPRT.Out_cat" u ($18):($47 *r2d) axes x1y2 w lp pt 9 ps.4 tit "spin prec."

 set terminal postscript eps blacktext color enh size 8.3cm,5cm "Times-Roman" 12
 set output "gnuplot_SPNPRT_scan.vs.Brho.eps"
 replot
 set terminal X11
 unset output

pause 1

exit
