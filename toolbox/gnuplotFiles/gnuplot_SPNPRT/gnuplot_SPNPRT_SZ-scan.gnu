
set tit "n_z and spin tune, observed at IP6 \n From zgoubi.SPNPRT.Out"
set grid

set ylabel "n_Z"
set xlabel "Outer coil current / A"
set y2label "{/Symbol n}_s"

set ytics nomirror
set y2tics

set key b l 

set y2range [.45:.5]

plot [275:335] [.9:1.] \
  "zgoubi.SPNPRT.Out_cat"   u (($0-2)/3*5+280):($21==3 ?  $50 : 1/0)  w p pt 5 ps .6 ,\
  "zgoubi.SPNPRT.Out_cat"   u (($0-2)/3*5+280):($21==3 ?  $51 : 1/0) axes x1y2 w p pt 6 ps .6


pause 1

 set terminal postscript eps blacktext color enh 
 set output "gnuplot_scanNy.eps"
 replot
 set terminal X11
 unset output


set tit "n angle to X, to Z, observed at IP6 \n From zgoubi.SPNPRT.Out"

set ylabel "angle to X  / deg"
set xlabel "Outer coil current / A"
set y2label "angle to Z  / deg"

plot [275:335]  \
  "zgoubi.SPNPRT.Out_cat"   u (($0-2)/3*5+280):($21==3 ?  $53 : 1/0)  w p pt 5 ps .6 ,\
  "zgoubi.SPNPRT.Out_cat"   u (($0-2)/3*5+280):($21==3 ?  $52 : 1/0)  w p pt 5 ps .6 

 set terminal postscript eps blacktext color enh 
 set output "gnuplot_scanRotAxAngles.eps"
 replot
 set terminal X11
 unset output


exit
