#plot "my.dat" every A:B:C:D:E:F
#A: line increment
#B: data block increment
#C: The first line        (line count starts at 0 !!)
#D: The first data block
#E: The last line
#F: The last data block

 set title "Spin Tune vs G{/Symbol g}, from zgoubi.SPNPRT.Out"

 set xlabel "G{/Symbol g}"    
 set x2label "E [GeV]"     
 set ylabel "Spin tune"   

 set xtics   nomirror
 set x2tics  nomirror
 set ytics   mirror   

#set key above maxcol 2 1 
#set key tm c maxrows 1 width 4
set key t r maxrow 5   width -3
set key  samplen 1

set grid

pi = 4.*atan(1.)
deg = 180./pi
am = 938.27203
G = 1.79284735
q=1
am = 2808.391585
G = 4.1841538
q = 2.

Ggmi = 6.4 ; Ggma = 7.6

#set samples 10000
set xrange  [Ggmi:]
set x2range [Ggmi/G*am/1e3:]

#set xtics add (487 487)
#set arrow from 487,-180 to 487,-170 nohead

 plot \
  "zgoubi.SPNPRT.Out_cat"  u ($21==4 ? abs($18) : 1/0):($51)  w lp pt 4 ps .8 lt 2 lc 1 lw 1 tit "{/Symbol n}_{sp}"  ,\
  "zgoubi.SPNPRT.Out_cat"  u ($21==4 ? abs($18)/G*am/1e3 : 1/0):($51)  axes x2y1 w lp ps 0. lw 0. notit  

#  "zgoubi.SPNPRT.Out_cat"  u ($21==4 ? abs($18) : 1/0):(acos($15)/3.1416*180.)  w lp pt 4 ps .8 lt 2 lc 1 lw 1 tit "SPINR"  ,\
#  "zgoubi.SPNPRT.Out_cat"  u ($21==4 ? abs($18)/G*am/1e3 : 1/0):(acos($15)/3.1416*180.)  axes x2y1 w lp ps 0. lw 0. notit  

 set terminal postscript eps blacktext color enh 
 set output "gnuplot_SPNPRT_Qs.eps"
 replot 
 set terminal X11
 unset output

pause 1

 set title "Spin Closed Orbit Vector vs G{/Symbol g}"

 set ylabel "n_X,  n_Y,  n_Z"   

pi = 4.*atan(1.)
tta = 0. ; qsi=pi ;   Qs = 0.5 
sx(x) = -1./sin(pi*Qs) * sin( x* (pi -tta)) * sin(qsi/2.)

 plot \
  "zgoubi.SPNPRT.Out_cat"  u ($21==4 ? abs($18) : 1/0):($48)  w lp pt 4 ps .8 tit "n_X"  ,\
  "zgoubi.SPNPRT.Out_cat"  u ($21==4 ? abs($18) : 1/0):($49)  w lp pt 5 ps .8 tit "n_Y"  ,\
  "zgoubi.SPNPRT.Out_cat"  u ($21==4 ? abs($18) : 1/0):($50)  w lp pt 6 ps .8 tit "n_Z"  ,\
  sx(x)

 set terminal postscript eps blacktext color enh 
 set output "gnuplot_SPNPRT_n0.eps"
 replot 
 set terminal X11
 unset output

pause 2

 exit
