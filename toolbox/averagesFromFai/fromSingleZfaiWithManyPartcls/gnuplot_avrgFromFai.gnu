

set title "Assume zgoubi.fai contains turn-by-turn particle data, with many particles. \n Compute and plot the turn-by-turn average over all particles"

set xtics
set ytics

set xlabel "G{/Symbol g}"
set ylabel "<S_y>|_{prtcls}"

fName = 'zgoubi.fai'
plotCmd(col_num)=sprintf('< gawk -f analyze.awk -v col_num=%d %s', col_num, fName)

set format y '%0.2f'

M=938.27203
Ei = 50.
dE = 0.2  # MeV/turn
G = 1.79284735
Qy = 0.7715

set xr [G:G*15e3/M]
set yr [-1.:1.]

plot \
    plotCmd(22) u ( (Ei+($1-1.)*dE+M)/M*G ):2 w p pt 5 ps .4 lc rgb 'dark-red' t '<col.22> vs col.38' 

#plot \
#  "zgoubi.fai" u ($38):($22) w p

 set terminal postscript eps blacktext color enh size 9.3cm,6cm "Times-Roman" 12
 set output "gnuplot_avrgFromFai_awk_SZ.eps"
 replot
 set terminal X11
 unset output

pause 2
