
#set tit  "Fresnel integral model"  font "roman,25"

 set xtics font "roman,20"
 set ytics font "roman,20"

set xlab "y=(a/{/Symbol p})^{1/2} {/Symbol q}"                    font "roman,25" offset 0,-1
set ylab "{sin^2 {/Symbol j} / {/Symbol p}|J_n|^2/a} \n"               font "roman,25" offset 1,0
#set xlab "kin-E (MeV)"                    font "roman,25"
#set ylab "Sz"               font "roman,25"


pi = 4. * atan(1.)
Jn2 = 2.8635757000000000E-009
a =   8.5453261175241435E-007
ER = 1353.5756093687257    # resonant kin-E [MeV]

set arrow from 0,0 to 0,18  nohead

plot  \
        "fort.88"  us (sqrt(a/pi) * $3 *(2.*pi) ):(sqrt(1.- $2**2) / (pi * Jn2 /a)) w l lw 2 notit 

pause 2

 set term post eps enh  size 10cm, 20cm "helvetica,14" blacktext color
 set output "gnuplot_fresnelIntModel.eps"
 replot
 set terminal X11
 unset output


exit


