#set tit  "Fresnel integral model"  

 set xtics ;  set ytics 

set xlab "y=({/Symbol a}/{/Symbol p})^{1/2} {/Symbol q}"     
#set ylab "{sin^2 {/Symbol j} / {/Symbol p}|{/Symbol e}_n|^2/{/Symbol a}} \n"  
#set xlab "G{/Symbol g}}"  
set xlab "kinetic energy [MeV]"  
#set xlab "turn"  
set ylab "S_y"           

set key t r spacin 1.5

#set arrow from 0,0 to 0, 18  nohead
set arrow from 426.778, .986 to 426.778, 1  nohead
set label "resonance" at 425,.992 rotate by 90

plot [400:480] \
        "weakXingFresnel.out"  us ($1):($2) w l lw 2 tit "Fresnel int. model" ,\
	"zgoubi.fai" u ($26==1 ? $24 :1/0):($22) w p pt 6

#pi = 4. * atan(1.)
#Jn2 = 2.8635757000000000E-009
#a =   8.5453261175241435E-007
#ER = 1353.5756093687257    # resonant kin-E [MeV]

# plot  \
#        "weakXingFresnel.out"  us (sqrt(a/pi) * $3 *(2.*pi) ):(sqrt(1.- $2**2) / (pi * Jn2 /a)) w l lw 2 notit 

pause 2

 set term post eps enh  
 set output "gnuplot_fresnelIntModel.eps"
 replot
 set terminal X11
 unset output


exit


