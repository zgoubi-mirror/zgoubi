      implicit double precision (a-h,o-z)
      parameter (pi = 4.d0 * atan(1.d0), pi2 = pi/2.d0)
      parameter (lunR=7, lunW=8)
      parameter (c=2.99792458d8)

      data am, QC, G / 938.27203d6, 1.d0, 1.7928474 /
      
      open(unit=lunR,file='weakXingFresnel.in')
      open(unit=lunW,file='weakXingFresnel.out')

      read(lunR,*) V        ! volts
      read(lunR,*) phis     ! rad
      read(lunR,*) wn2      ! Jn^2, resonance strength (width), can be from prior Froissard-Stora tracking
      read(lunR,*) n        ! such that gammaG=n+/-qz
      read(lunR,*) qz       ! gammaG=n \pm qz 
      read(lunR,*) miTurn
      read(lunR,*) maTurn
      read(lunR,*,err=33) am
      read(lunR,*) QC
      read(lunR,*) G        ! |G|

!      G = abs(G)
      
 33   continue
      
c      V = 6.d3        
c      phis = 0.4873884165731143248
c      wn2 =2.8635757d-9      
c      n  = 8           !  8-qz
c      qz = 3.62074437
c      miTurn = -1200
c      maTurn = 1400

      dW = QC*V*sin(phis)
C FM - NOv 2020.
      a  = G * dW / am / (2.d0*pi) ! =xing speed = Gdg/dtta
C      a  = dW / am / (2.d0*pi)       ! =xing speed = Gdg/dtta
      gGr = dble(n) + qz
      Er = gGr /G *am
      Tr = Er-am

      write(lunW,*) '# ',  V     ,' peak V / volts'
      write(lunW,*) '# ',  phis  ,' synch phase / rad'
      write(lunW,*) '# ',  wn2   ,' Jn^2, resonance strength (width)'
      write(lunW,*) '# ',  n     ,' n, such that gammaG=n+/-qz'
      write(lunW,*) '# ',  qz    ,' Qy, algebraic, Gg=n+qz '
      write(lunW,*) '# ',  miTurn
      write(lunW,*) '# ',  maTurn
      write(lunW,*) '# ', am
      write(lunW,*) '# ',  QC    ,'  q/e '
      write(lunW,*) '# ',  G

      write(*,*)
     >'# dW (eV/turn), res. E & kin_E /MeV : '
     >,dW, Er/1d6, Tr/1d6
      write(lunW,*) '# Boost (eV/turn), resonant E, res. E-kin : '
     >,dW, Er, Tr
      write(*,*) '# Xing speed a = Gdg/dtta = ',a
      write(lunW,*) '# Xing speed a = Gdg/dtta = ',a
      write(*,*) '# |J_n|^2 = ',wn2,',    pi|J_n|^2/a = ',pi*wn2/a
      write(lunW,*) '# |J_n|^2 = ',wn2,',    pi|J_n|^2/a = ',pi*wn2/a

      w2sa = pi * (wn2 /a) 
      rap = sqrt(a/pi) * (2.d0 *pi)
CCCCCCCCCCC VERIFIER fints !!*************
      do i = miTurn, 0, 1
        y = rap * dble(i)
        if(-y.gt.5.d0)   stop ' y too large' 
        meth = 1
c        meth = 2
        s2p = w2sa *
     >  ((0.5d0-fintc(meth,-y))**2 + (0.5d0-fints(meth,-y))**2)
        T = Tr + dW * dble(i)
        write(lunW,*) T/1d6 , sqrt(1-s2p), i
c        write(88,*) T/1d6 , sqrt(1-s2p), i
c     >  ,y , abs(fintc(1,-y)-fintc(2,-y)), abs(fints(1,-y)-fints(2,-y))
c     >  , fintc(1,-y)/fintc(2,-y), fints(1,-y)/fints(2,-y)
c     >  , fintc(1,-y),fintc(2,-y), fints(1,-y),fints(2,-y)
      enddo
      do i = 1, maTurn, 1
        y = rap * dble(i)
        if(y.gt.5.d0)   stop ' y too large' 
        meth = 1
c        meth = 2
        s2p = w2sa *
     >  ((0.5d0+fintc(meth,y))**2 + (0.5d0+fints(meth,y))**2)
        T = Tr + dW * dble(i)
        write(lunW,*) T/1d6 , sqrt(1-s2p), i
c        write(88,*) T/1d6 , sqrt(1-s2p), i
c     >  ,y , abs(fintc(1,y)-fintc(2,y)), abs(fints(1,y)-fints(2,y))
c     >  , fintc(1,y)/fintc(2,y), fints(1,y)/fints(2,y)
c     >  , fintc(1,y),fintc(2,y), fints(1,y),fints(2,y)
      enddo               

      write(*,*) '# Fresnel int. method is  ', meth
      write(lunW,*) '# Fresnel int. method is  ', meth
      write(lunW,*) '# Resonant kin-E = ', Tr/1d6,' MeV'
      write(lunW,*) '# at theoretical turn# E_r/(dW/dTurn) = ', Tr/dW

      stop
      end
      function fintc(meth,x)
      implicit double precision (a-h,o-z)
      parameter (pi = 4.d0 * atan(1.d0), pi2 = pi/2.d0)
      integer*8 nfctrl

      goto (7,8) meth

C Method of series expansion. A-S 7.3.11
 7    continue
      fintc = 0.d0
      f1 = fintc
      n = 0
 1    continue
        fac = x**(4*n+1) * (-1.d0)**n * pi2**(2*n) / dble(4*n+1) 
        m = 2
 2      continue
          if(m.gt.2*n) goto 3
          fac = fac / dble(m)
          m = m+1
          goto 2      
 3      continue
        fintc = fintc + fac
        if(abs(fintc-f1).le.1d-6*f1) goto 10
        f1 = fintc
        n = n+1
        goto 1

        stop 'fint_C, method 1, should never end here !'

C Method of auxiliary functions f, g. A-S 7.3.9,10 & 7.3.32
 8    continue
C A-S coefficients
      fx = (1.d0 + .926d0*x)/(2.d0 + 1.792d0*x + 3.104*x*x)
      gx = 1.d0/(2.d0 + 4.142d0*x + 3.492*x*x + 6.670*x*x*x)
C Improved coefficients - Heald
      fx = (1.d0 + .882d0*x)/(2.d0 + 1.722d0*x + 3.017*x*x)
      gx = 1.d0/(2.d0 + 4.167d0*x + 3.274*x*x + 6.890*x*x*x)

      fintc = 0.5d0 + fx*sin(pi/2.d0*x*x) - gx*cos(pi/2.d0*x*x)
        
 10   return
      end
      function fints(meth,x)
      implicit double precision (a-h,o-z)
      parameter (pi = 4.d0 * atan(1.d0), pi2 = pi/2.d0)
      integer*8 nfctrl

      goto (7,8) meth

C Method of series expansion. A-S 7.3.11
 7    continue
      fints = 0.d0
      f1 = fints
      n = 0
 1    continue
        fac = x**(4*n+3) * (-1.d0)**n * pi2**(2*n+1) / dble(4*n+3)
        m = 2
 2      continue
          if(m.gt.2*n+1) goto 3
          fac = fac / dble(m)
          m = m+1
          goto 2      
 3      continue
        fints = fints + fac
        if(abs(fints-f1).le.1d-6*f1) goto 10
        f1 = fints
        n = n+1
        goto 1

        stop 'fint_C, method 1, should never end here !'

C Method of auxiliary functions f, g. A-S 7.3.9,10 & 7.3.32
 8    continue
C A-S coefficients
      fx = (1.d0 + .926d0*x)/(2.d0 + 1.792d0*x + 3.104*x*x)
      gx = 1.d0/(2.d0 + 4.142d0*x + 3.492*x*x + 6.670*x*x*x)
C Improved coefficients - Heald
      fx = (1.d0 + .882d0*x)/(2.d0 + 1.722d0*x + 3.017*x*x)
      gx = 1.d0/(2.d0 + 4.167d0*x + 3.274*x*x + 6.890*x*x*x)

      fints = 0.5d0 - fx*cos(pi/2.d0*x*x) - gx*sin(pi/2.d0*x*x)
        
 10   return
      end
