 
 set terminal X11

reset 
set title "Strength of imperfection resonances" 

 set lmargin 12.5
 set bmargin 3.5

  set xlabel  "|G{/Symbol g}|"             
  set ylabel "|N_n|"  textcolor rgb "blue"
  set y2label  "P_f/P_i"    textcolor rgb "red"

    set xtics 
    set ytics  nomirror 
    set y2tics nomirror 

    set key top left 
      set key maxrow 1

      set logscale y  10
 
set style line 1 lt 1 lw 1.8 lc rgb "red"
set style line 2 lt 1 lw 1.8 lc rgb "blue"

set y2range [:1.1]

plot  [4.5:12.5] [:] \
     "calcStrength.out" u ($1):($11)    w imp ls 1  tit '|N_n|' ,\
     "calcStrength.out" u ($1):($13)  axes x1y2  w p pt 4 ps 1. lc rgb "blue" tit '    P_f/P_i'


set label "Strong resonances: int*P{/Symbol \261}[Q_y] = 6+5, 12+/-5, 18+/-5, ... " at graph .1,.7 
set label "P=6, Q_y=4.82" at graph .1,.65 
set label "z_{max} = 4.69 mm" at graph .1,.6 

 set terminal postscript eps blacktext color enh 
 set output "gnuplot_resoStrengths.eps"
 replot
 set terminal X11
 unset output

pause 1

exit
