      implicit double precision (a-h,o-z)
      character txt800*800, name*20, txt14*14, txt132*132
      character txt400*400
      parameter (amref=938.27203d6, Qref = 1, Gref=1.7928474)
      parameter (c=2.99792458d8)
      parameter (pi=4.d0*atan(1.d0),dpi=2.d0*pi,qpi=4.d0*pi)
C      parameter (lunR= 7, lunOut = 8, lunDat=9)
      parameter (nX=9)
      dimension ndat(nX), sgndat(nX)
      character typ1*10, typ2*12
      character tsgn*1, tQz*7
      integer debstr, finstr
      character twFile*20
      character(20) KEYWORD,label1,label2
      logical strcon, idluni, ok

      character(10) rep
C Input data ---------------------------------------------------------
      ok = idluni(
     >            lunDat)
      open(unit=lunDat,file='calcStrength.in',action='read')
C Presumably twFile=zgoubi.TWISS.out
      call readat(lunDat,
     >    typ2,typ1,M,Ggmi,Ggma,amin,Qin,Gin,epszin,alpin,twFile,ierr)
      close(lunDat)
      if(ierr.eq.1) then
        am = amref
        Q = Qref
        G = Gref
C        epsz = 2.5d-6      ! normalized rms emittance
        epsz = 10-6      ! rhic case
        alpha = 4.41d-5    ! at  145keV/turn
      else
        am = amin
        Q = Qin
        G = Gin
        sgnG=1.d0
        if(G.lt.0.d0) then
           G = -G
           sgnG=-1.d0
        endif
        epsz = epszin
        alpha = alpin
      endif
      write(*,fmt='(3(1x,a),/,a,/,a,/,i4,/,1p,
     >  2e12.4,/,3e12.4,/,2e12.4)') 
     >  ' Data read from calcStrength.in,  ',
     >  ' typ2, typ1, M, Ggmi, Ggma, mass, Q, G, epsz, ',
     >  ' xing speed : ',
     >  typ2,typ1,M,Ggmi,Ggma, am, Q, G, epsz, alpha
C typ2 = 'imperfection' or 'intrinsic' 

      ok = idluni(
     >            lunOut)
      open(unit=lunOut,file='calcStrength.out',action='write')
      
      ok = idluni(
     >            lunR)
      open(unit=lunR,file=twFile,action='read')

      write(*,*)
      write(*,*) 'Vertical tune is read from '
     >     //twFile(debstr(twFile):finstr(twFile))//' '
      write(*,*)
      
 77   read(lunR,fmt='(a)') txt132
      if(.not.strcon(txt132,'Q2',2,
     >                             IS)) goto 77

      read(txt132(23:),*) Qz
 33   continue
      write(*,*) ' Warning: tune needs by total, not fractional!'
      write(*,*) ' Vertical tune, total, is  Qz = ', Qz,'. Ok ?'
      write(*,*) ' (type "enter" to carry on, "no" to change)'
      read(*,fmt='(a)') rep
      if(trim(rep) .eq. 'no') then
        write(*,*) ' give QZ : '
        read(*,*) Qz
        goto 33
      endif
C Position pointer at start of SELECT data list
 78   read(lunR,fmt='(a)') txt132
      if(.not.strcon(txt132,'@ TIME',6,
     >                                 IS)) goto 78

      write(*,*) ' Will now read SELECT data list from '//twFile
      write(*,*) '           type "enter" to carry on'
      read(*,*)

      Emi = Ggmi/G * am        ! (eV)
      Ema = Ggma/G * am        ! (eV)


      ii = -1000
      jj = 0
 2    continue
      ii = ii+1
      n = ii
      if(typ1 .eq. 'systematic') then
        if((n/M)*M .ne. n) goto 2
      endif

      if    (typ2 .eq. 'intrinsic')  then 
        sgn = -2.d0
      elseif(typ2 .eq. 'imperfection') then
        sgn = -1.d0
      endif
 3    continue
      sgn = sgn + 1.d0
      if(nint(sgn).gt.1)  goto 2
      if    (typ2 .eq. 'imperfection') then
        if(nint(sgn).ge.1)  goto 2
      elseif(typ2 .eq. 'intrinsic') then
        if    (nint(sgn).eq.0) goto 3
      endif
       
      Gg = n + sgn * Qz
      gma = Gg / G

      if(Gg .lt. Ggmi) goto 3

        if    (typ2 .eq. 'intrinsic') then
          if(nint(sgn) .eq. -1) then
            if(Gg .gt. Ggma) goto 88
          endif
        endif
        if    (typ2 .eq. 'imperfection') then
         if(Gg .gt. Ggma) goto 88
        endif
        if(nint(sgn) .eq. 0) then
          if(Gg .gt. Ggma) goto 3 
        endif
        if(nint(sgn) .eq. 1) then
          if(Gg .gt. Ggma) goto 2 
        endif

        if    (typ2 .eq. 'intrinsic')  then 
          write(tQz,fmt='(f7.4)') Qz
          if    (nint(sgn) .ge. 0) then
            tsgn = '+'
          else
            tsgn = '-'
          endif
        elseif(typ2 .eq. 'imperfection') then
          tQz = 'na'
          tsgn = '|'
        endif

      rewind(lunR)

      jj = jj+1
      Brho = am*sqrt((gma-1.d0)*(gma+1.d0))/(Q*c)
C      write(*,*) Gg, n+Qz,n-Qz, n+2.*Qz
      write(*,fmt='(a,f9.4,1x,i3,2a,2f12.6,1x,f14.6,2(1x,f12.6),2x,i4)')
     >   ' Gg, N+/-Qz,  E_tot, Brho, 4Qz^2-n^2, Emin, Emax, # : '
     >    ,Gg, n, tsgn,trim(tQz),am*gma/1.d9, Brho, 4.d0*Qz*Qz-n*n,
     >   Emi/1.d9,Ema/1.d9,jj
            
      scain = 0.d0
      ssain = 0.d0
      scaim = 0.d0
      ssaim = 0.d0
      ssqim = 0.d0
      zma = -1d10
      zmi = 1d10
      zco1 = 0.d0
      zco2 = 0.d0
      s = 0.d0
      tta = 0.d0
      ncou = 0
      nbnd = 0
      nqua = 0
      nmult = 0
      lmnt = 0
 1    continue
C read elements
      read(lunR,fmt='(a)',err=99,end=10) txt800
      lmnt = lmnt + 1
      if    (strcon(txt800,'SBEN',7,IS) 
     >.or.   strcon(txt800,'RBEN"',7,IS) 
     >.or.   strcon(txt800,'QUAD',12,IS)  ) then

c        read(txt800,fmt='(24e19.0,2e19.0,a16)')
c     >  xl, ang, ak1,ak2,ak3,ak4, tilt, e1,e2, h1,h2,sCntr,
c     >  alfx,betx, alfy,bety, xco,yco, Dx,Dxp, Dy,Dyp, xmu,ymu, 
c     >  hkic,vkic,
c     >  name

      txt400=txt800(1:208)
c      write(*,*) '***'//txt400(debstr(txt400):finstr(txt400))//'***'
c          read(*,*)
      read(txt400,*)
     >alfx,betx,alfy,bety,alfl,betl,Dx,Dxp,Dy,Dyp,xmu,ymu,sOut
      txt400=txt800(209:215)
c      write(*,*) '***'//txt400(debstr(txt400):finstr(txt400))//'***'
c          read(*,*)
      read(txt400,*) lmnt
      txt400=txt800(216:270)
c      write(*,*) '***'//txt400(debstr(txt400):finstr(txt400))//'***'
c          read(*,*)
      read(txt400,*) xco,xpco,yco,ypco
      txt400=txt800(271:322)
c      write(*,*) '***'//txt400(debstr(txt400):finstr(txt400))//'***'
c          read(*,*)
      read(txt400,*) KEYWORD,label1,label2
      txt400=txt800(323:707)
c      write(*,*) '***'//txt400(debstr(txt400):finstr(txt400))//'***'
c          read(*,*)
      read(txt400,*) FO61,aK0,aK1,aK2
C      txt400=txt800(708:800)
c      write(*,*) '***'//txt400(debstr(txt400):finstr(txt400))//'***'
c      read(*,*)

        sCntr = sOut !- xl/2.
        angl = ang
        akl = ak1
        name = label1
        
C          write(*,*) lmnt,name,xl,angl,akl
        if(strcon(txt800,'QUAD',12,IS)   ) then
          nqua = nqua + 1
        elseif(strcon(txt800,'SBEN',7,IS)
     >         .or.   strcon(txt800,'RBEN',7,IS) ) then
           xl = 0.d0; angl = 0.d0
          read(txt800(733:),*) xl, angl       
          nbnd = nbnd + 1
          tta = tta + angl
c          write(*,*) ' txt800(732:) : ',trim(txt800(733:))
c          write(*,*) ' angl, sum = ',angl, tta
c          read(*,*)
        endif
        alpz = alfy
        btaz = bety
        YMU = ymu
        zco = yco
        zco1 = zco1 + yco   * (sCntr - s)
        zco2 = zco2 + yco*yco  * (sCntr - s)
        s = sCntr
        ncou = ncou + 1
        if(zco.gt.zma) zma = zco
        if(zco.lt.zmi) zmi = zco
c        write(88,fmt='(4(i4,1x),1p,8e14.6,1x,a16,1x,a)')  
c     >    lmnt, nbnd,nqua,nmult,s,xl,angl,akl/xl,btaz,zco,tta,ymu,name,
c     >  ' lmnt#,nbnd,nqua,nmul, s,xl,angl,ak,btaz,zco,tta,ymu,name'

        if(akl.ne.0.d0) then
          Gga = Gg*tta
          if(Gga .lt.0 ) Gga = -Gga   ! Gga<0 because convention is normally x>0 toward center of ring
          facim = akl*zco
          scaim = scaim + cos(Gga) * facim
          ssaim = ssaim + sin(Gga) * facim
          ssqim = ssqim + facim**2 
          facin = akl*sqrt(btaz)
          ph = -sgn * dpi * YMU
          scain = scain + cos(Gga+ph) * facin
          ssain = ssain + sin(Gga+ph) * facin
c          write(89,fmt='((i4,1x),1p,8(e14.6,1x))') 
c     >    lmnt,Gga,akl,btaz,facin,scain,ssain,ymu,tta
          write(89,fmt='((i4,1x),1p,13(e14.6,1x),0p,2(f7.3,1x),
     >    1x,a,f7.3)') 
     >    lmnt,scain,cos(Gga+ph),cos(Gga+ph) * facin, 
     >    ssain,sin(Gga+ph),sin(Gga+ph) * facin, 
     >    akl,btaz,facin,sgn,dpi,YMU,ph,tta/pi*180,Gg,name,zco
        endif
      endif

      goto 1

 10   continue
      aJn2 = ((1.d0+ sgnG* Gg)/dpi)**2 * (scaim*scaim+ssaim*ssaim) ! contains z_co
      aJsq = ((1.d0+ sgnG* Gg)/dpi)**2 * ssqim
      aNn2 = ((1.d0+ sgnG* Gg)/qpi)**2 * (scain*scain+ssain*ssain) ! does not contain sqrt(eps_z)

      zco1 = zco1 / s
      zco2 = sqrt(zco2 / s  - zco1*zco1)

      write(txt14,fmt='(i4,1x,a)') n,
     >  tsgn(debstr(tsgn):finstr(tsgn))//tQz(debstr(tQz):finstr(tQz))

      if(zco2.ne.0.d0) then 
        pJn2 = aJn2/zco2**2
      else
        pJn2 = 0.d0
      endif

      bta = sqrt(1.d0-1.d0/(gma*gma))
      PfiJ = 2.d0*exp(-pi/2.d0 * aJn2/alpha) - 1.d0          ! aJn2 already contains z_co
      PfiN = 2.d0*exp(-pi/2.d0 * aNn2*(epsz/(bta*gma))/alpha) - 1.d0     ! aMn2 does not contain eps_z
      
      write(lunOut,fmt='(a,1p,2(e16.8,1x),0p,f10.5,1p,3(1x,e12.4)
     > ,0p,7(1x,f12.6),1p,3(1x,e12.4),5x,2a)') 
     > txt14, pJn2, aNn2, YMU, zma, zco2, tta-dpi, am*gma/1.d9, 
     > G*gma, sqrt(aJn2),  ! sqrt(pJn2)*1.d-3, 
     > sqrt(aNn2*epsz/(bta*gma)), 
     > PfiJ, PfiN, zco1, epsz, epsz/(bta*gma), (bta*gma), 
     >'n +/-Qz, Jn^2/zma^2, Nn^2/eps_z/pi, MUY, zmax, zrms, 2pi-2pi,',
     >' Etot, Gg, Jn|_z, Nn|_epsZ, Pf/Pi_imp, Pf/Pi_int, <zco>'

C      write(*,*) ' nint(sgn) = ',nint(sgn) 
      if(nint(sgn) .ge. 1) then
        goto 2 
      else
        goto 3
      endif

 88   continue

      write(*,*)
      write(*,*) ' Total bend angle found: ',tta
     >,'.   Over 2pi : ',tta/(8.d0*atan(1.d0))
      write(*,*)
      
      close(lunR)
      stop

 99   continue
      stop ' Read error in twiss file '
      end      
      subroutine readat(lunDat,
     >           typ2,typ1,M,Ggmi,Ggma,am,Q,G,epszN,alpha,twFile,ierr)
      implicit double precision (a-h,o-z)
      character typ1*(*), typ2*(*), twFile*(*)
      ierr = 0
      Gin = 0.d0
      read(lunDat,fmt='(a)',err=99,end=98) typ2
      read(lunDat,fmt='(a)',err=99,end=98) typ1
      read(lunDat,fmt='(a)',err=99,end=98) twFile
      read(lunDat,*,err=99,end=98) M
      read(lunDat,*,err=99,end=98) Ggmi, Ggma      ! absolute values
      read(lunDat,*,err=99,end=98) am, Q, G        ! facultative data.  G is algebraic (e.g.: -4.18 for 3He)
      read(lunDat,*,err=99,end=98) epszN, alpha   ! facultative data, epszN: normalized, xing speed
      return
 99   continue
      ierr = 1
      write(*,*) ' error during read in data file'
      return
 98   continue
      ierr = 1
      write(*,*) ' End of data file reached'
      return
      end
      FUNCTION DEBSTR(STRING)
      implicit double precision (a-h,o-z)
      INTEGER DEBSTR
      CHARACTER * (*) STRING

C     --------------------------------------
C     RENVOIE DANS DEBSTR LE RANG DU
C     1-ER CHARACTER NON BLANC DE STRING,
C     OU BIEN 0 SI STRING EST VIDE ou BLANC.
C     --------------------------------------

      DEBSTR=0
      LENGTH=LEN(STRING)
C      LENGTH=LEN(STRING)+1
1     CONTINUE
        DEBSTR=DEBSTR+1
C        IF(DEBSTR .EQ. LENGTH) RETURN
C        IF (STRING(DEBSTR:DEBSTR) .EQ. ' ') GOTO 1
        IF (STRING(DEBSTR:DEBSTR) .EQ. ' ') THEN
          IF(DEBSTR .EQ. LENGTH) THEN
            DEBSTR = 0
            RETURN
          ELSE
            GOTO 1
          ENDIF
        ENDIF

      RETURN
      END
      FUNCTION FINSTR(STRING)
      implicit double precision (a-h,o-z)
      INTEGER FINSTR
      CHARACTER * (*) STRING
C     --------------------------------------
C     RENVOIE DANS FINSTR LE RANG DU
C     DERNIER CHARACTER NON BLANC DE STRING,
C     OU BIEN 0 SI STRING EST VIDE ou BLANC.
C     --------------------------------------

      FINSTR=LEN(STRING)+1
1     CONTINUE
        FINSTR=FINSTR-1
        IF(FINSTR .EQ. 0) RETURN
        IF (STRING(FINSTR:FINSTR) .EQ. ' ') GOTO 1

      RETURN
      END

      subroutine gotoEnd(lunW,
     >                        line)
      character*132 txt132
      line = 1
 1    continue
        read(lunW,*,err=99,end=10) txt132
        line = line + 1
        goto 1

 10   continue
      return

 99   continue
      write(*,*) ' '
      write(*,*) '************* '
      write(*,*) 'Pgm geneTexEnd.  Error reading in lunW ' 
      write(*,*) 'line = ',line
      write(*,*) '************* '
      write(*,*) ' '
      stop 
      end

      FUNCTION STRCON(STR,STRIN,NCHAR,
     >                                IS)
      implicit double precision (a-h,o-z)
      LOGICAL STRCON
      CHARACTER STR*(*), STRIN*(*)
C     ------------------------------------------------------------------------
C     .TRUE. if the string STR contains the string STRIN with NCHAR characters
C     at least once.
C     IS = position of first occurence of STRIN in STR
C     ------------------------------------------------------------------------

      INTEGER DEBSTR,FINSTR

      II = 0
      DO 1 I = DEBSTR(STR), FINSTR(STR)
        II = II+1
        IF( STR(I:I+NCHAR-1) .EQ. STRIN ) THEN
          IS = II
          STRCON = .TRUE.
          RETURN
        ENDIF
 1    CONTINUE
      STRCON = .FALSE.
      RETURN
      END
      FUNCTION IDLUNI(
     >                LN)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL IDLUNI

      LOGICAL OPN

      I = 20
 1    CONTINUE
        INQUIRE(UNIT=I,ERR=99,IOSTAT=IOS,OPENED=OPN)
        I = I+1
        IF(I .EQ. 100) GOTO 99
        IF(OPN) GOTO 1
        IF(IOS .GT. 0) GOTO 1
      
      LN = I-1
      IDLUNI = .TRUE.
      RETURN

 99   CONTINUE
      LN = 0
      IDLUNI = .FALSE.
      RETURN
      END
      FUNCTION EMPTY(STR)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL EMPTY
      CHARACTER*(*) STR
      INTEGER FINSTR
      EMPTY = FINSTR(STR) .EQ. 0
      RETURN
      END
      SUBROUTINE RAZ(TAB,N)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION TAB(1)
      DO 1 I=1,N
 1      TAB(I) = 0.D0
      RETURN
      END
