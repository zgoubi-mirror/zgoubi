
clear 
 set terminal X11

reset 
#set title "Resonance strengths, intrinsic. From zgoubi.TWISS.out"

  set xlabel  "|G {/Symbol g}|"   
  set x2label  "E [GeV]"        
#  set ylabel "|{/Symbol e}_n|"  textcolor rgb "blue"
  set ylabel "|{/Symbol e}_n| / {/Symbol \326 e}_y/{/Symbol p}"  textcolor rgb "blue"

    set xtics nomirror;    set x2tics;    set ytics 

    set logscale y  10

M = 2808.3916
G = -4.18415
MeV2GeV = 1e-3
Ggmi = 4.19 ; Ggma = 16     # taken from calcStrength.in
Emi = Ggmi/abs(G)*M *MeV2GeV; Ema = Ggma/abs(G)*M *MeV2GeV
epspiN =2.5e-6

set xrange [Ggmi:Ggma]
set x2range [Emi:Ema]

#plot  \
#     "calcStrength.out" u ($1+$2):($12)    w imp lc rgb "red" lw 3  notit  ,\
#     "calcStrength.out" u (($1+$2)/abs(G)*M*MeV2GeV):($12) axes x2y1   w imp lc 1 lw 0 notit

plot  \
     "calcStrength.out" u ($1+$2):(sqrt($4))    w imp lc rgb "red" lw 3  notit  ,\
     "calcStrength.out" u (($1+$2)/abs(G)*M*MeV2GeV):(sqrt($4)) axes x2y1   w imp lc 1 lw 0 notit

 set terminal postscript eps blacktext color enh     # size 8.3cm,4cm "Times-Roman" 12
 set output "gnuplot_resoStrengths_intrinsic.eps"
 replot
 set terminal X11
 unset output

pause 2

exit
