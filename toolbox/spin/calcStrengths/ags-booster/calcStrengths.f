      parameter (pi = 4.*atan(1.), deg2rd = pi/180.)
      parameter (G = 4.18415, am =2808.39e6 )

      circ = 201.78     ! m
      Vp = 800.e3       ! V
      Q = 2.d0
      phis =   0.523598775598  ! 30. * deg2rd  ! rad
      Bdip =  7.2121043E-02  ! T

      DE = Vp * Q * sin(phis)      
      alpha = G * DE /(2. * pi * am)
      rho = 1./Bdip
      R = circ / (2.*pi)
      Bdot = DE/(2.*pi*Q*R*rho)

      write(*,*) ' Present conditions : '
      write(*,*) '    rho = ', rho,' m'
      write(*,*) '    xing speed alpha = dgamma/dtta =   ',alpha
      write(*,*) '    dB/dt                         =    ',Bdot,' T/s'

      write(*,*) 
      write(*,*) '-------------------'
      write(*,*) ' gamma G = nu_z '
      write(*,*) ' ' 
      pini = 1.
      pfin = -0.231978       ! from zgoubi
      pfi1 = pfin/pini + 1.
      A = 2.*alpha/pi
      strength=sqrt( -A*log(pfi1/2.))
      write(*,*) 'p_init = ',pini,'   p_final = ',
     >pfin,',  pf/pi = ',pfi1-1.,'  =>   |J_n|=',strength

      stop
      end





