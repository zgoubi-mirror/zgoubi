
 set xlabel "G.gamma"
 set ylabel "<S_y>  "
 set title "turn-by-turn average, from zgoubi.fai"
 
 set xtics
 set ytics

plot \
     "averageS.out" u ($12):($6) w p  tit "<S_Z>" 

 set terminal postscript eps blacktext color enh 
 set output "gnuplot_averageSZfromFai.eps"
 replot
 set terminal X11
 unset output

pause 1


exit


