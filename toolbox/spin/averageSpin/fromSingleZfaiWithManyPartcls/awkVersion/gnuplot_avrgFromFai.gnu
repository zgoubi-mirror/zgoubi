
nbtrj=23
evryNtrj = 2
evryNpass=5

set xlabel "turns"; set ylabel "Average S_y over particles"

fName = 'zgoubi.fai'
plotCmd(col_num)=sprintf('< gawk -f analyze.awk -v col_num=%d %s', col_num, fName)

set format y '%0.2f'
set xr [:20e3]
set yr [-1.01:1.01]

plot \
    for [it=1:nbtrj:evryNtrj] "zgoubi.fai" u ($26==it && evryNpass*int($38/evryNpass)==$38? $38 :1/0):22 w p pt 7 ps .1 lc rgb 'violet' notit ,\
    plotCmd(22) u 1:2 w p pt 5 ps .3 lc rgb 'dark-red' t '<S_y>'  


#plot \
#  "zgoubi.fai" u ($38):($22) w p

 set terminal postscript eps blacktext color enh # size 9.3cm,6cm "Times-Roman" 12
 set output "gnuplot_avrgFromFai_awk_SZ.eps"
replot
 set terminal X11
 unset output

pause 2
