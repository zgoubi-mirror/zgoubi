c      SUBROUTINE SPEANA(YM,BORNE,NC0,
c     >                               YNU,SPEC,PMAX)
      implicit double precision (a-h,o-z)
      DIMENSION YM(3), BORNE(2)
      parameter(mxpts=600000)
      dimension spin(3,mxpts)
      parameter(mxc=20000)
      dimension SPEC(mxc), q(mxc)
      dimension SPEC1(mxc)
      dimension SPEC2(mxc)
      dimension SPEC3(mxc)
      parameter(mxspec=mxpts/100)
      dimension Qs(mxspec),Qs1(mxspec),Qs2(mxspec),Qs3(mxspec)
      PARAMETER ( PI=3.1415926536 , DEUXPI=2.0*PI )
      logical idluni

      PARAMETER (mxj=7,MXS=4)
      DIMENSION FO(MXJ),F(MXJ),SO(MXS),SF(MXS)

      PARAMETER (LBLSIZ=8)
      CHARACTER*(LBLSIZ) LBL1, LBL2

      PARAMETER (KSIZ=10)
      CHARACTER KLEY*(KSIZ) 
      CHARACTER TX1*1
      CHARACTER*1 LET

      INCLUDE 'FILFAI.H'     ! Tells FILFAI= [b_]zgoubi.fai
      character*(20) FILFAIr, FILFAIn

      logical okfai, binary

      DATA KSX, KSY, KSZ / 1, 2, 3 /
      data FILFAIr, FILFAIn / ' ', 'zgoubi.fai' /
      data okfai / .false. /

      logical okend 
      character*700 txt700

      logical exs

      dimension omga(3), somga(3), b(3), c(3), u1(3), u2(3)
      dimension smi(3), sma(3), sav(3)

      logical txtsav
      CHARACTER(11) FRMT
      
      data nc0 / 2500 /
      data okend / .false. /
      data borne / 0.48d0, .52d0 / 

      data KPA, Ksmpl / 1000, 300 /
      data itraj /  1  / 

      FILFAIn = trim(FILFAI)
      
      write(*,*) ' '
      write(*,*) '----------------------------------------------------'
      write(*,*) 'NOW RUNNING PGM spinTuneFromFai... '
      write(*,*) '----------------------------------------------------'
      write(*,*) ' '

C Range of turns to be considered may be specified using spinTuneFromFai.In
      INQUIRE(FILE='spinTuneFromFai.In',exist=EXS)

      IF (IDLUNI(NLU)) THEN
        open(unit=nlu,file='spinTuneFromFai.In')
        write(*,*) ' opened  spinTuneFromFai.In'
      ELSE
        stop 'Pgm spinTuneFromFai :   No idle unit number ! '
      ENDIF

      if(.NOT.exs) then
        write(*,*)'WARNING : File spinTuneFromFai.In does not exist'
        write(*,*)'Pgm spinTuneFromFai creates one from default values'

        write(nlu,fmt='(2(i6,1x),t60,t60,a)')  kpa, ksmpl
     >  ,' ! kpa, ksmpl : Fourier transf. ksmpl turns starting from kpa'
        write(nlu,fmt='(i6,1x,t60,a)')  itraj
     >  ,' ! itraj : number of the particle to be Fourier''ed'
        write(nlu,fmt='(2(f8.4,1x),t60,a)')  (borne(i),i=1,2)
     >  ,' ! Q_1, Q_2 : x/y/l  spectrum range'
        write(nlu,fmt='(i6,1x,t60,a)')  nc0
     >  ,' ! nbin : x/y/l # of bins in spectrum range'
        write(nlu,*) ' Y        ! (Y/N) yes/no save spectra '
        write(nlu,*) FILFAIn,' !  coordinate storage file '
      endif

      rewind(nlu)

      read(nlu,*,err=11,end=11) kpa, ksmpl
      read(nlu,*,err=11,end=11) itraj
      read(nlu,*,err=11,end=11) borne(1), borne(2) 
      read(nlu,*,err=11,end=11) nc0
      read(nlu,*,err=11,end=11) tx1
      txtsav = tx1 .eq. 'y' .or. tx1 .eq. 'Y'
      read(nlu,*,err=11,end=11) FILFAIn

      close(nlu)

      KPB = KPA + ksmpl -1

      if(idluni(lunR)) then
        INQUIRE(FILE=FILFAIn,EXIST=EXS,IOSTAT=IOS)
        if(binary) then
            FRMT='UNFORMATTED'
          ELSE
            FRMT='FORMATTED'
        endif
        if(exs) then
          open(unit=lunR,file=FILFAIn,FORM=FRMT)
          write(*,*) 'Opened file ',FILFAIn
          CALL HEADER(lunR,4,binary,*98)
        else
          write(*,*) 'PGM SPINTUNEFROMFAI : no such file ',trim(FILFAIn)
          stop 'Exiting.'
        endif
        binary = FILFAIn(1:2).eq.'b_' .or. FILFAIn(1:2).eq.'B_'
      else
        stop ' PGM SPINTUNEFROMFAI : no idle unit'
      endif

      if(idluni(lunW)) 
     >   open(unit=lunW,file='spinTuneFromFai.Out')
      if(idluni(lunW2)) 
     >   open(unit=lunW2,file='spinTuneFromFai_spectrum.Out')


c      itraj = 0      
      itmx = 0
c 8    continue
c      itraj = itraj+1
c      itraj = 1

c      ii = 1
c 7    continue

      do jc=1, 3
        smi(jc) = 1d10
        sma(jc) = -1d10
        sav(jc) = 0.d0
      enddo

      i = 1
 1    continue
          IF(BINARY) THEN
 222        CONTINUE
            READ(lunR,ERR=3,END=3) 
     >      KEX,(FO(J),J=1,7),
     >      (F(J),J=1,7), 
     >      (SO(J),J=1,4),spin(1,i),spin(2,i),spin(3,i),dum,
     >      ENEKI, ENERG, 
     >      IT, IREP, SORT, AMQ1,AMQ2,AMQ3,AMQ4,AMQ5, RET, DPR, PS,
     >      BORO, IPASS, NOEL ,KLEY,LBL1,LBL2,LET

          ELSE
 21         CONTINUE
            READ(lunR,FMT='(A)',ERR=3,END=3) TXT700
            TXT700 = TRIM(TXT700)
            IF( TXT700(1:1).EQ.'#' .OR. TXT700(1:1).EQ.'!' 
     >      .OR. TXT700(1:1).EQ.'#' ) GOTO 21
            READ(TXT700,110,ERR=3,END=3) 
     >      KEX,(FO(J),J=1,7),
     >      (F(J),J=1,7), 
     >      (SO(J),J=1,4),spin(1,i),spin(2,i),spin(3,i),dum,
     >      ENEKI, ENERG, 
     >      IT, IREP, SORT, AMQ1,AMQ2,AMQ3,AMQ4,AMQ5, RET, DPR, PS,
     >      BORO, IPASS, NOEL ,KLEY,LBL1,LBL2,LET

            INCLUDE "FRMFAI.H"
          ENDIF

          
c         write(*,*) 
c         write(*,*) it,itmx,itraj,ipass,kpa
c         write(*,*) TRIM(TXT700)
c         write(*,*) IT, IREP, SORT, AMQ1,AMQ2
         
         if(it.gt.itmx) itmx = it
         IF(IT.NE.itraj) goto 1
         IF(ipass .lt. kpa) goto 1

c     write(*,*) ' spectrum,  i, it, itraj :',i,it,itraj
c        write(*,*) ' spin(3,i) :',spin(3,i),i,ksmpl
        if(i.ge.Ksmpl) goto 2
        if(i.ge.mxpts) goto 2
    
        do jc = 1, 3
          if(smi(jc) .gt. spin(jc,i)) smi(jc)=spin(jc,i)
          if(sma(jc) .lt. spin(jc,i)) sma(jc)=spin(jc,i)
          sav(jc) = sav(jc) + spin(jc,i)
        enddo

        i = i + 1
      goto 1

 3    i = i-1
      okend = .true.
 2    continue
      npt = i

       savm = 0.d0
        do jc = 1, 3
          sav(jc) = sav(jc) / dble(npt)
          savm = savm + sav(jc)**2
        enddo
        savm = sqrt(savm)
        do jc = 1, 3
          sav(jc) = sav(jc) / savm 
        enddo

C omga is ~ the local precession direction
      omga(1) = (sma(1) + smi(1)) / 2.d0
      omga(2) = (sma(2) + smi(2)) / 2.d0
      omga(3) = (sma(3) + smi(3)) / 2.d0
      romga = sqrt(omga(1)*omga(1)+omga(2)*omga(2)+omga(3)*omga(3)) 
      omga(1) = omga(1) / romga
      omga(2) = omga(2) / romga
      omga(3) = omga(3) / romga
C u1 and u2 define the plan normal to omga
        nt = 1
        b(1) = spin(1,nt)
        b(2) = spin(2,nt)
        b(3) = spin(3,nt)
        call pvect(omga,b,
     >                    c)
        u1(1) = c(1)
        u1(2) = c(2)
        u1(3) = c(3)
        call pvect(u1,omga,
     >                     c)
        u2(1) = c(1)
        u2(2) = c(2)
        u2(3) = c(3)

c      write(*,*) '0 = u1.u2 = ',u1(1)*u2(1)+u1(2)*u2(2)+u1(3)*u2(3)

      if(npt.le.2) goto 22
c      write(*,fmt='(a,3(1x,i6,a))') 'Fourier-transform of pass # '
c     >,kpa,' to pass # ', kpb,'  (',ksmpl,' points)'
      
        ANUI = BORNE(1)
        ANUF = BORNE(2)
        DELNU=(ANUF - ANUI) / NC0
        PAS=DEUXPI * DELNU
        VAL=DEUXPI *(ANUI - 0.5d0 * DELNU)
        PMAX=0.D0
        PMIN=1.D12
        PMAX1=0.D0
        PMIN1=1.D12
        PMAX2=0.D0
        PMIN2=1.D12
        PMAX3=0.D0
        PMIN3=1.D12
        DO NC=1,NC0           ! frequency bin
          VAL=VAL+PAS
          SR=0.D0
          SI=0.D0
          SR1=0.D0
          SI1=0.D0
          SR2=0.D0
          SI2=0.D0
          SR3=0.D0
          SI3=0.D0
          DO NT=1,npt
C Build vec (s-(s.omga)omga) contained in (vec u1, vec u2) plane
            b(1) = spin(1,nt)
            b(2) = spin(2,nt)
            b(3) = spin(3,nt)
            call pscal(b,omga,
     >                        som)
            somga(1) = spin(1,nt)-som*omga(1) 
            somga(2) = spin(2,nt)-som*omga(2) 
            somga(3) = spin(3,nt)-som*omga(3) 

c            write(*,*) ' 0 = (S - (S.O).O).O = ',
c     >      somga(1)*omga(1)+somga(2)*omga(2)+somga(3)*omga(3)

            call pscal(u1,somga,
     >                          su1)
            call pscal(u2,somga,
     >                          su2)
c            SR=SR + su1*cos(nt*val)  + su2*sin(nt*val)
c            SI=SI + su1*sin(nt*val)  - su2*cos(nt*val)
c            SR=SR + spin(1,nt)*cos(nt*val) + spin(2,nt)*sin(nt*val)
c            SI=SI + spin(1,nt)*sin(nt*val) - spin(2,nt)*cos(nt*val)
c            ff = sqrt((spin(1,nt)-sav(1))**2 + (spin(2,nt)-sav(2))**2 + 
c     >                 (spin(3,nt)-sav(3))**2 ) 

            ff = spin(2,nt)-sav(1)
            ff = spin(3,nt)-sav(1)
            ff = spin(1,nt)-sav(1)
            
            SR=SR + ff*cos(nt*val) 
            SI=SI + ff*sin(nt*val) 

            SR1=SR1 + spin(1,nt)*cos(nt*val) 
            SI1=SI1 + spin(1,nt)*sin(nt*val) 
            SR2=SR2 + spin(2,nt)*cos(nt*val) 
            SI2=SI2 + spin(2,nt)*sin(nt*val) 
            SR3=SR3 + spin(3,nt)*cos(nt*val) 
            SI3=SI3 + spin(3,nt)*sin(nt*val) 
          ENDDO
          PP=SR*SR+SI*SI
          PP1=SR1*SR1+SI1*SI1
          PP2=SR2*SR2+SI2*SI2
          PP3=SR3*SR3+SI3*SI3
          IF(PP.GT. PMAX) THEN
            PMAX=PP
            KMAX=NC
          ELSEIF(PP.LT. PMIN) THEN
            PMIN=PP
          ENDIF
          IF(PP1.GT. PMAX1) THEN
            PMAX1=PP1
            KMAX1=NC
          ELSEIF(PP1.LT. PMIN1) THEN
            PMIN1=PP1
          ENDIF
          IF(PP2.GT. PMAX2) THEN
            PMAX2=PP2
            KMAX2=NC
          ELSEIF(PP2.LT. PMIN2) THEN
            PMIN2=PP2
          ENDIF
          IF(PP3.GT. PMAX3) THEN
            PMAX3=PP3
            KMAX3=NC
          ELSEIF(PP3.LT. PMIN3) THEN
            PMIN3=PP3
          ENDIF
          SPEC(NC)=PP
          SPEC1(NC)=PP1
          SPEC2(NC)=PP2
          SPEC3(NC)=PP3
          Q(NC)=val/(2.d0*pi)
        ENDDO

        IF (PMAX .GT. PMIN) THEN
          IF (KMAX .LT. mxc) THEN
            DEC=0.5D0 * (SPEC(KMAX-1)-SPEC(KMAX+1))
     >      /(SPEC(KMAX-1) - 2.D0 *SPEC(KMAX)+SPEC(KMAX+1))
          ELSE
            DEC=0.5D0 
          ENDIF
          YNU= ANUI + (DBLE(KMAX)+DEC-0.5D0) * DELNU
        ELSE
           YNU = 0.D0
        ENDIF
        IF (PMAX1 .GT. PMIN1) THEN
          IF (KMAX1 .LT. MXC) THEN
            DEC=0.5D0 * (SPEC1(KMAX1-1)-SPEC1(KMAX1+1))
     >      /(SPEC1(KMAX1-1) - 2.D0 *SPEC1(KMAX1)+SPEC1(KMAX1+1))
          ELSE
            DEC=0.5D0 
          ENDIF
          YNU1= ANUI + (DBLE(KMAX1)+DEC-0.5D0) * DELNU
        ELSE
           YNU1 = 0.D0
        ENDIF
        IF (PMAX2 .GT. PMIN2) THEN
          IF (KMAX2 .LT. MXC) THEN
            DEC=0.5D0 * (SPEC2(KMAX2-1)-SPEC2(KMAX2+1))
     >      /(SPEC2(KMAX2-1) - 2.D0 *SPEC2(KMAX2)+SPEC2(KMAX2+1))
          ELSE
            DEC=0.5D0 
          ENDIF
          YNU2= ANUI + (DBLE(KMAX2)+DEC-0.5D0) * DELNU
        ELSE
           YNU2 = 0.D0
        ENDIF
        IF (PMAX3 .GT. PMIN3) THEN
          IF (KMAX3 .LT. MXC) THEN
            DEC=0.5D0 * (SPEC3(KMAX3-1)-SPEC3(KMAX3+1))
     >      /(SPEC3(KMAX3-1) - 2.D0 *SPEC3(KMAX3)+SPEC3(KMAX3+1))
          ELSE
            DEC=0.5D0 
          ENDIF
          YNU3= ANUI + (DBLE(KMAX3)+DEC-0.5D0) * DELNU
        ELSE
           YNU3 = 0.D0
        ENDIF

      write(*,*) '---------------------------------'
      write(*,*) '---------------------------------'
      write(*,*) ' Number of channels : ',nc0
      write(*,fmt='(3(a,i6))') 
     >  ' Number of turns analyzed : ',npt,', from ',kpa,' to ',kpb
      write(*,*) ' Qs / 1-Qs = ',ynu,' / ',1.d0-ynu
      write(*,*) '---------------------------------'
      write(*,*) '---------------------------------'
      
      Qs(1) = ynu  
      Qs1(1) = ynu1  
      Qs2(1) = ynu2 
      Qs3(1) = ynu3  
      write(lunW,fmt='(a,1p,7(1x,e14.5),3(1x,i6),3(1x,e14.5),5x,a)') 
     >'  ',Qs(1),omga(1), omga(2), omga(3)
     >,sav(1), sav(2), sav(3)
     >, kpa, kpb, itraj, Qs1(1), Qs2(1), Qs3(1)
     >,' ! Qs at max ampltd; omga_x,y,z; turn# range; Qs1, Qs2, Qs3'
      write(lunW2,fmt='(a)') '# Spectrum (Qs / amp. / bin# / traj#) : '
      write(lunW2,fmt='(1p,5(e16.8,1x),i6,1x,i6)') 
     >         (q(j),spec(j),spec1(j),spec2(j),spec3(j),j,itraj,j=1,nc0)


c      if(itraj.lt.itmx) goto 8


 22   continue
      write(*,*) ' Job completed !'
      close(lunW)
      close(lunW2)
      close(lunR)
      stop
 
 11   continue
      stop 'Error during read from spinTuneFromFai.In'
 98   continue
      stop 'Error during read header in zgoubi.fai'
 99   continue
      stop 'Error during read in zgoubi.fai'

      END
      FUNCTION IDLUNI(LN)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL IDLUNI

      LOGICAL OPN

      I = 20
 1    CONTINUE
        INQUIRE(UNIT=I,ERR=99,IOSTAT=IOS,OPENED=OPN)
        I = I+1
        IF(I .EQ. 100) GOTO 99
        IF(OPN) GOTO 1
        IF(IOS .GT. 0) GOTO 1
      
      LN = I-1
      IDLUNI = .TRUE.
      RETURN

 99   CONTINUE
      LN = 0
      IDLUNI = .FALSE.
      RETURN
      END
      SUBROUTINE HEADER(NL,N,BINARY,*)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL BINARY
      CHARACTER*800 TITL
      CHARACTER*800 TXT
      WRITE(6,FMT='(/,A,L)') ' Binary =  ',binary
      WRITE(6,FMT='(/,A,I0,A,/)') ' File header  (',N,' lines) :  '
      IF(.NOT.BINARY) THEN
        READ(NL,FMT='(A)',ERR=99,END=99) TXT
        WRITE(6,FMT='(A)') TRIM(TXT)
        READ(NL,FMT='(A)',ERR=99,END=99) TITL
        WRITE(6,FMT='(A)') trim(TITL)
      ELSE
        READ(NL,ERR=99,END=89) TXT
        WRITE(6,FMT='(A)') TRIM(TXT)
        READ(NL,ERR=99,END=89) TITL
        WRITE(6,FMT='(A)') trim(TITL)
      ENDIF
      IF(.NOT.BINARY) THEN
        DO 1 I=3, N
          READ(NL,FMT='(A)',ERR=99,END=99) TXT
          WRITE(6,FMT='(A)') TRIM(TXT)
 1      CONTINUE
      ELSE
        DO 2 I=3, N
           READ(NL,          ERR=99,END=89) TXT
           WRITE(6,FMT='(A)') TRIM(TXT)
 2      CONTINUE
      ENDIF
      RETURN
 89   CONTINUE
      WRITE(6,*) 'END of file reached while reading data file header'
      RETURN 1
 99   CONTINUE
      WRITE(6,*) '*** READ-error occured while reading data file header'
      WRITE(6,*) '        ... Empty file ?'
      RETURN 1
      END
      subroutine pvect(a,b,
     >                     c)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      dimension a(3),b(3),c(3)
      c(1) = a(2)*b(3) - a(3)*b(2)
      c(2) = a(3)*b(1) - a(1)*b(3)
      c(3) = a(1)*b(2) - a(2)*b(1)
      return
      end
      
      subroutine pscal(a,b,
     >                     c)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      dimension a(3),b(3)
      c = a(1)*b(1) + a(2)*b(2) + a(3)*b(3) 
      return
      end
      
