      #Tunes versus turn number !  
      set xlabel "TURN REGION" 
      set ylabel "Q_s" 

      set title "TUNE / CYCLE"  
      set xtics 
      set ytics nomirror 

#      set yrange [.48:.52]

      plot \
       "spinTuneFromFai_iterate.Out"   u ($5):($1) w lp ps .8 tit "Q_s" 

      set terminal postscript eps blacktext color  enh 
       set output "gnuplot_Qspin.vs.turn.eps"  
       replot  
       set terminal X11  
       unset output  

      pause 80


 exit
