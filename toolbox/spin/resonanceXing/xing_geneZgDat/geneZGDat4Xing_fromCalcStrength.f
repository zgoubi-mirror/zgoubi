      implicit double precision (a-h,o-z)
     
      parameter (lunR=11,lunW=12,lunDat=15,lunDa2=17,luntmp=14)
      character txt132*132, txt132c*132, let*1, txt4*4, txt4a*4, let1*1
      parameter (nCOmx=10001)
      dimension x(nCOmx),xp(nCOmx),z(nCOmx),
     >                   zp(nCOmx),s(nCOmx),d(nCOmx),let(nCOmx)
      character txtksy(10)*150, txt150*150
      character cmmnd*110
      character drctry*15, zgDatFile*50

      logical strcon
      INTEGER DEBSTR,FINSTR
      character typ2*12
      data typ2 / 'intrinsic' /
      parameter(pi=4.d0*atan(1.d0),c=2.99792458d8, deg2rd=pi/180.d0)
      parameter(zero=0.d0)
      logical exs, isnum
      character(132) txtmp,  txtmp2

      data am, q, G / 938.27203d6,1.602176487d-19,1.7928474d0 /
      data qe / 1.602176487d-19 /

      character(5) txtQz

C Input data ---------------------------------------------------------
      inquire(file='geneZGDat4Xing.data',exist=exs)
      if(.not. exs) then
C set default values         
        write(*,*) 'Pgm geneZGDat4Xing_fromCalcStrength : '
        write(*,*) 'geneZGDat4Xing.data needed. '
        write(*,*) 'Expected content, a template for RHIC:'
        write(*,*) ' '
        write(*,*) 'intrinsic'
        write(*,*) '0.5, 100         expected pf/pi; '
     >  ,'dist/|en| at start'
        write(*,*) '3833.85D0, 120.  orbit length, RF h'
        write(*,*) '23.5063091       gamma_tr'
        write(*,*) '40.d3,  30.d0    peak '
     >  //'V (V),  phi_s (deg.) in [0,180] (Vsin(phi)=20keV) yields '
     >  //'alpha=5.7e-6, about 5 times actual value==1.15e-6).'
        write(*,*) '938.27203d6, 1.602176487d-19, 1.7928474d0   '
     >  //'particle mass (eV), charge (C), G'
        write(*,*) ' '

      endif
      
      call system(
     >'cp -f geneZGDat4Xing.data geneZGDat4Xing.data_copy')
      open(unit=lunDat,file='geneZGDat4Xing.data')
      call readat(lunDat,
     >typ2,xpfpi,DltGgn,circ,ah,gtr,Vp,phisD,am,q,G,ierr)
      write(*,fmt='(3a,10(1x,e12.4),1x,I4)') 
     >     ' Data read from geneZGDat4Xing.data :  typ2, xpfpi, '
     > ,'DltGgn, circ, ah, gtr, Vp, phisD, am, q, G, ierr = ',
     >  typ2,xpfpi,DltGgn,circ,ah,gtr,Vp,phisD,am,q,G,ierr
C typ2 = 'imperfection' or 'intrinsic' 
      close(lunDat)
      
C  geneZGDat4Xing.In is a copy of calcStrength.out 
      open(unit=lunDa2,file='geneZGDat4Xing.In')
C---------------------------------------------------------------------

      if(ierr.eq.1) then
        write(*,*) '  Failed to read a data from geneZGDat4Xing.data'
      endif
      if(ierr.ge.1) then 
        write(*,*) '  geneZGDat4Xing.data should exist and contain,', 
     >                                            ' in that order :'
        write(*,*) ' = M, algebraic_Qz :  such that resonant',
     >                                   ' gG=M + sign(Qz)|Qz|'
        write(*,*) ' ring circumference, harmoniq # '
        write(*,*) ' particle mass (MeV), charge (C), G'
        write(*,*) ' numb. of tracking turns upstream and downstream',
     >                                                  ' of resonance'
      endif
C-----------------------------
     
 20   continue
        write(*,*) '//////////////////////////////',
     >  '//////////////////////////////'
        read(lunDa2,fmt='(a)') txt132
        read(txt132,*) M, Qz

 28     continue
        read(txt132,*,err=99,end=99)
     >  M, Qz,aJn2im,aNn2in,ymu,zma,vkick
        if(Qz.lt.0.d0) write(*,*) ' Case M +/-Qz = ',M,Qz
        if(Qz.ge.0.d0) write(*,*) ' Case M +/-Qz = ',M,'-',Qz
        write(*,*) 
        write(*,*) ' Read from geneZGDat4Xing.In, ',
     >  'M, Qz, Jn^2/z^2, Nn^2/eps, zma, vkick : ',
     >  M,Qz,aJn2im,aNn2in,zma,vkick
c        read(*,*)
        
        gmaR = (dble(M) + Qz) / G
        if(gmaR.lt.1.d0) stop ' Resonant gamma <1 !'
        if(gmaR.gt.gtr) phisD = 180.d0 - phisD
        phis = phisD * deg2rd        
        qqe = q/qe
        dEturn = qqe * Vp * sin(phis)
        alpha = G*dEturn/2.d0/pi/am
        rJn2 = 2.d0*alpha/pi * dlog(2.d0/(1+xpfpi))
c        write(*,*) ' rjn2 ',rjn2,G,dEturn,vp,phis,qqe
c        stop ' DONE ! '
        if    (typ2 .eq. 'imperfection') then
C aJn2im is Jn^2/zmax^2
          aJn2 = aJn2im
C          zma1 = 0.2d0 * rJn2 / aJn2
          zma1 = zma * sqrt(rJn2 / aJn2)
          if(zma1 .gt. 20.d-3) zma1=20.d-3
          vk = zma1/zma * vkick 
          epspi = zero
c          den = sqrt(aJn2*zma1**2)
c     dE = 300.d0 * den / G * am
          dE = DltGgn/G*am * sqrt(aJn2 * zma1**2)
       elseif(typ2 .eq. 'intrinsic') then
C aNn2in is Nn^2/epsilon/pi
          aJn2 = aNn2in

c          write(*,*) ' aJn2 = ', aJn2
c          read(*,*)
          
          epspi = rJn2 / aJn2
C          if(gmaR*epspi .gt. 20.d-6) epspi = 20.d-6/gmaR
          zma1 = 0.d0
c          den = sqrt(aJn2*epspi)
c          dE = 300.d0 * den / G * am
          dE = DltGgn/G*am * sqrt(aJn2 * epspi)
        else
          write(*,*) ' Pgm geneZGDat4Xing_fromCalcStrength : '
     >    //'typ2 = ',typ2,' :  No such choice available ! '
c          read(*,*)
        endif

C Note on value of nrbl : 
C Static,     Sz^2 = Delta^2/Jn^2 / (1 + Delta^2/Jn^2) 
C             Delta^2/Jn^2 = Sz^2 / (1 - Sz^2)
C  Distance to resoannce = gG-(n +/- Qz) = 7Jn corresponds to Sz=0.9900, i.e. 1% depolarization
C Sz=.9999 requires gG-(n +/- Qz) = 70.7Jn
C gG-(n +/- Qz) = 100Jn corresponds to Sz=0.9999500
C gG-(n +/- Qz) = 300Jn corresponds to Sz=0.99999444
C gG-(n +/- Qz) = 1000Jn corresponds to Sz=0.999999500

        E = gmaR * am
        T = E - am
        p = sqrt(T * (T + 2.d0*am))
        boro = p/c/qqe
        nrbl2 = nint(dE/dEturn)
        Ei = E - dble(nrbl2) * dEturn
        if(Ei .lt. am) Ei = am
        boroi = sqrt((Ei-am) * ((Ei-am) +2.d0*am))/c/qqe

        write(*,*) 
        write(*,*) ' Pgm geneZGDat4Xing_fromCalcStrength, ',
     >  'programmed parameters for this run: '
        write(*,*) 
        write(*,*) ' Working conditions : '
        write(*,*) ' Mass, charge, G : ',am,q,G
        write(*,*) ' Emittance, max z_c.o. : ',epspi, zma1
        write(*,*) ' On-resonance E, kin-E, Brho & G.gmaR          :'
     >  ,E, E-am,  boro, G*gmaR 
        write(*,*) ' At start tracking E_i, kin_Ei, Brho_i, G.gma_i :'
     >  ,Ei, Ei-am, boroi, G*Ei/am,', ',
     >    nrbl2,'  turns to resonance ' 
        if    (typ2 .eq. 'imperfection') then
C aJn2im is Jn^2/zmax^2
          epsn=sqrt(aJn2*zma1**2) ; dist = dE*G/am/epsn
          write(*,*) ' Distance to the resonance at start ',
     >    '= (Gg_i-Gg_n)/|epsilon_n|) = ', dist
          write(*,*) ' Imperfection resonance, ',
     >    ' M, +/-Qz, resonnant G.gma, Jn, y_rms, Jn/y_rms:'
          write(*,*)  M,Qz,G*gmaR,sqrt(aJn2*zma1**2),zma1,sqrt(aJn2)
        elseif(typ2 .eq. 'intrinsic') then
C aNn2in is Nn^2/epsilon/pi
          epsn = sqrt(aJn2*epspi) ; dist = dE*G/am/epsn
          write(*,*) ' Distance to the resonance at start ',
     >    '= (Gg_i-Gg_n)/|epsilon_n|) = ', dist
          write(*,*) ' Intrinsic resonance, ',
     >    ' M, +/-Qz, resonnant G.gma, Nn, eps/pi, Nn/sqrt(eps/pi):'
          write(*,*)  M, Qz, G*gmaR, sqrt(aJn2*epspi),epspi,sqrt(aJn2)
        else
          write(*,*) ' Pgm geneZGDat4Xing_fromCalcStrength : '
     >    //'typ2 = ',typ2,' :  No such choice available ! '
          stop
        endif
        write(*,*) ' Xing speed, GdE/2piM = ', alpha
        write(*,*) 
        write(*,*) ' Expected Pf/Pi =  ',
     >  2.d0*exp(-pi/2.d0*epsn**2/alpha)-1.d0
        write(*,*) 
        read(*,*)
        
        open(unit=lunR,file='zgoubi_geneZGDat4Xing-In.dat')
        
 2    continue

        rewind(lunR)
        open(unit=lunW,file='zgoubi_geneZGDat4Xing-Out.dat')
        rewind(lunW)

C Read in zgoubi_geneZGDat4Xing-In.dat
        read(lunR,fmt='(a)') txt132       ! title
        write(lunW,fmt='(a)') 
     >  'Data generated by  geneZGDat4Xing '
          read(lunR,fmt='(a)') txt132     ! OBJET
          write(lunW,*) txt132(debstr(txt132):finstr(txt132))
          read(lunR,fmt='(a)') txt132     ! BORO
          read(txt132,*) BORO
          write(lunW,fmt='(f14.8,a2)') boroi,'d3'
          read(lunR,*) kobj
          write(*,*) ' KOBJ = ',kobj
          if(kobj .ne. 8) stop '** OBJET should use KOBJ=8 '
          write(lunW,*) '   8 ' 
          read(lunR,fmt='(a)') txt132
          write(lunW,*) txt132(debstr(txt132):finstr(txt132))
          read(lunR,fmt='(a)') txt132
          write(lunW,*) txt132(debstr(txt132):finstr(txt132))
          read(lunR,fmt='(a)') txt132
          write(lunW,*) txt132(debstr(txt132):finstr(txt132))
          read(lunR,*) alpz, btaz, dum
          write(lunW,fmt='(1p,3(1x,g14.6))') alpz, btaz, epspi
          read(lunR,fmt='(a)') txt132
          write(lunW,*) txt132(debstr(txt132):finstr(txt132))

      txt132 = '''PARTICUL'''
      write(lunW,*) txt132(debstr(txt132):finstr(txt132))  
      write(lunW,fmt='(1p,3e17.9,a)') am/1d6, q , G, '  0. 0. '
      txt132 = '''SPNTRK'''
      write(lunW,*) txt132(debstr(txt132):finstr(txt132))  
      txt132 = '  3       '
      write(lunW,*) txt132(debstr(txt132):finstr(txt132))  
      txt132 = '''FAISCEAU'''
      write(lunW,*) txt132(debstr(txt132):finstr(txt132))  
      txt132 = '''PICKUPS'''
      write(lunW,*) txt132(debstr(txt132):finstr(txt132))  
      txt132 = '   1        '
      write(lunW,*) txt132(debstr(txt132):finstr(txt132))  
      txt132 = '   #Start'
      write(lunW,*) txt132(debstr(txt132):finstr(txt132))  
      txt132 = '''FAISTORE'''
      write(lunW,*) txt132(debstr(txt132):finstr(txt132))  
      txt132 = '   b_zgoubi.fai  #End'
      write(lunW,*) txt132(debstr(txt132):finstr(txt132))  
      txt132 = '     1'
      write(lunW,*) txt132(debstr(txt132):finstr(txt132))  
      txt132 = '''MARKER''  #Start'
      write(lunW,*) txt132(debstr(txt132):finstr(txt132))  

C Complete zgoubi_geneZGDat4Xing-Out.dat from content of zgoubi_geneZGDat4Xing-In.dat
 1      continue
          read(lunR,fmt='(a)',end=10) txt132
          txt132 = txt132(debstr(txt132):132)

          if(strcon(txt132,'''PARTICUL''',10,
     >                                       IS)) then 
            read(lunR,fmt='(a)') txt132

          elseif(strcon(txt132,'''FAISCEAU''',10,
     >                                           IS)) then 

          elseif(strcon(txt132,'''SPNTRK''',8,
     >                                       IS)) then 
            read(lunR,fmt='(a)') txt132

          elseif(strcon(txt132,'''PICKUPS''',9,
     >                                       IS)) then 
            read(lunR,fmt='(a)') txt132
            read(lunR,fmt='(a)') txt132

          elseif(strcon(txt132,'''SPNSTORE''',10,
     >                                       IS)) then 
            read(lunR,fmt='(a)') txt132
            read(lunR,fmt='(a)') txt132

          elseif(strcon(txt132,'''FAISTORE''',10,
     >                                       IS)) then 
            read(lunR,fmt='(a)') txt132
            read(lunR,fmt='(a)') txt132

          elseif(strcon(txt132,'''SCALING''',9,
     >                                        IS)) then 
            write(lunW,*) txt132(debstr(txt132):finstr(txt132))
            read(lunR,fmt='(a)') txt132
            write(lunW,*) txt132(debstr(txt132):finstr(txt132))
            read(txt132,*) ii1, ii2
            do ii = 1, ii2
              read(lunR,fmt='(a)',end=10) txt132
              write(lunW,*) txt132(debstr(txt132):finstr(txt132))
              read(lunR,fmt='(a)',end=10) txt132
              write(lunW,*) txt132(debstr(txt132):finstr(txt132))
              read(lunR,fmt='(a)',end=10) txt132

C     write(lunW,fmt='(f14.8)') boroi
              txtmp = trim(txt132)
              if(strcon(txt132,'!',1,
     >                               is)) then
                 txtmp = txt132(:is-1)
                 txtmp2 = txt132(is:)
              endif
              
              if(strcon(txtmp,'*',1,
     >                              iis)) then
                 read(txt132(:iis-1),*) aa
                 read(txt132(iis+1:),*) bb
                 if(aa.eq.boro*1d-3) then
                   write(lunW,*) boroi,'*', bb
                   write(*,*) ' boro=aa ',boro, aa
                 else
                   write(lunW,*) aa,'*', boroi
                   write(*,*) ' boro=bb ',boro, bb
                 endif
              else
                    write(lunW,fmt='(f14.8)') boroi
              endif

              
              read(lunR,fmt='(a)',end=10) txt132
              write(lunW,*) txt132(debstr(txt132):finstr(txt132))
            enddo

          elseif(strcon(txt132,'''MARKER''',8,
     >                                        IS)) then 
            txt132c = txt132(IS+8:132-8)
            txt132c = txt132c(debstr(txt132c):debstr(txt132c)+6)
            if(txt132c.eq.'#Start') then
            elseif(txt132c.eq.'#End') then
            else              
              write(lunW,*) txt132(debstr(txt132):finstr(txt132))
            endif

          elseif(strcon(txt132,'''TWISS''',7,
     >                                        IS)) then 
            read(lunR,fmt='(a)') txt132

          elseif(strcon(txt132,'DVCA02',8,                 ! AGS
     >                                        IS)) then 
            write(lunW,*) txt132(debstr(txt132):finstr(txt132))
            read(lunR,fmt='(a)',end=10) txt132
            write(lunW,*) txt132(debstr(txt132):finstr(txt132))
            read(lunR,*,end=10) xl,ro,b1
            b1 =  boroi * vk /xl        *1.d2 !*********
            write(lunW,fmt='(1p,3(1x,g14.6),a)')
     >               xl,ro,b1,' 0. 0. 0. 0. 0. 0. 0. 0. 0.'

          elseif(strcon(txt132,'''CAVITE''',8,
     >                                        IS)) then
            read(lunR,fmt='(a)',end=10) txt132
            read(lunR,fmt='(a)',end=10) txt132
            read(lunR,fmt='(a)',end=10) txt132

          elseif(strcon(txt132,'''REBELOTE''',10,
     >                                         IS)) then
            read(lunR,fmt='(a)',end=10) txt132

          elseif(strcon(txt132,'''END''',5,
     >                                       IS)) then

            goto 11
          else
            
             if(len(trim(txt132)) .le. 1) then
                   write(lunW,fmt='(a)') ' '
                   write(*,fmt='(a)') ' < 4 ',trim(txt132)
             else
                write(lunW,*) trim(txt132)
             endif
             
          endif

        goto 1

 11     continue

              txt132 = '''CAVITE'''
              write(lunW,*) txt132(debstr(txt132):finstr(txt132))
              txt132 = ' 3'
              write(lunW,*) txt132(debstr(txt132):finstr(txt132))
              write(lunW,fmt='(f18.11,3x,f7.2)') circ, ah
              write(lunW,fmt='(1p,e16.8,3x,e20.12)') Vp, phis
              txt132 = '''MARKER''   #End'
              write(lunW,*) txt132(debstr(txt132):finstr(txt132))

c              write(*,*) ' Pgm geneZGDat4Xing_fromCalcStrength : '
c              write(*,*) ' REBELOTE;  nrbl2 = ',nrbl2
C              read(*,*)
              
              txt132 = '''REBELOTE'''
              write(lunW,*) txt132(debstr(txt132):finstr(txt132))
c              write(lunW,fmt='(i8,a)') 2*nrbl2, '  0.2  99'
              write(lunW,fmt='(i8,a)') 3*nrbl2, '  0.2  99'

C When this zgoubi run finishes, Keyword 'SYSTEM' will cause launching of next zgoubi run
C Read file number from temporary storage by scanSpinResonances.f   
              open(unit=luntmp,file='scanSpinResonances.tmp')
              read(luntmp,*) ifile, drctry, zgDatFile
              close(luntmp)        
              txt132 = '''SYSTEM'''
              write(lunW,*) txt132(debstr(txt132):finstr(txt132))
              write(lunW,*) '  2'
              write(txt4,fmt='(i4)') ifile
              write(txt4a,fmt='(i4)') ifile+1
              cmmnd = 
     >        ' cd .. ; ' // 
     >        'sed -i ''s/'//txt4//' = xing simulation number/'
     >        //txt4a//' = xing simulation number/g'' '// 
     >        ' scanSpinResonances.count'
              write(*,*) cmmnd
              write(*,*) cmmnd
              write(*,*) cmmnd
              write(*,*) cmmnd
              write(lunW,*) cmmnd
              cmmnd = 'cd .. ; '//
     >        '~/zgoubi/toolbox/spin/resonanceXing/xing_scanResonances'
     >        //'/scanSpinResonances'
              write(lunW,*) cmmnd

              txt132 = '''END'''
              write(lunW,*) txt132(debstr(txt132):finstr(txt132))

 10   continue
      write(*,*) ' '
      write(*,*) ' Pgm geneZGDat4Xing_fromCalcStrength : '
     >//' End of zgoubi_geneZGDat4Xing-In.dat input file has'
     >//' been reached, file zgoubi_geneZGDat4Xing-Out.dat completed.'
      write(*,*) ' ------------'
      write(*,*) ' '
      close(lunR)
      close(lunW)

C Build zgoubi_searchCO-In.dat and run searchCO
      open(unit=lunR,file='zgoubi_geneZGDat4Xing-Out.dat')
      open(unit=lunW,file='zgoubi_searchCO-In.dat')
C Replaces objet 8 by objet 2
          read(lunR,fmt='(a)',end=13) txt132   ! titl
          write(lunW,*) txt132(debstr(txt132):finstr(txt132))
          
          write(*,*) 'titl :',  txt132(debstr(txt132):finstr(txt132))
c          read(*,*)
          
          read(lunR,fmt='(a)',end=13) txt132   ! objet
          write(lunW,*) txt132(debstr(txt132):finstr(txt132))
          read(lunR,fmt='(a)',end=13) txt132   ! boro
          write(lunW,*) txt132(debstr(txt132):finstr(txt132))
          read(lunR,fmt='(a)',end=13) txt132   ! kobj
          write(lunW,*) ' 8 '
          
          read(lunR,fmt='(a)',end=13) txt132   !  1 1 1
          write(*,*) ' 1 1 1  :', txt132(debstr(txt132):finstr(txt132))
c          read(*,*)
          write(lunW,*) ' 1 1 1 '
          
          read(lunR,*,end=13) yo, ypo, zo, zpo, xo, do
          write(lunW,fmt='(1p,4(e12.4,1x),e9.1,1x,e14.6,1x,3a1)') 
     >         yo*1d2, ypo*1d3, zo*1d2, zpo*1d3, xo, do
          write(*,*) ' coordinates : ',
     >         yo*1d2, ypo*1d3, zo*1d2, zpo*1d3, xo, do
c          read(*,*)
          
          read(lunR,*,end=13) alfy, bety, epsy
          write(lunW,*) alfy, bety, epsy
          write(*,*)    alfy, bety, epsy
c          read(*,*)
          
          read(lunR,*,end=13)  alfz, betz, epsz
          write(lunW,*) alfz, betz, epsz
          write(*,*)    alfz, betz, epsz
c          read(*,*)
          
          read(lunR,*,end=13)  alfl, betl, epsl
          write(lunW,*)  alfl, betl, epsl
          write(*,*)     alfl, betl, epsl
c          read(*,*)
C Complete zgoubi_searchCO-In.dat from content of zgoubi_geneZGDat4Xing-In.dat
 12   continue
          read(lunR,fmt='(a)',end=13) txt132
          if(strcon(txt132,'''CAVITE''',8,
     >                                        IS)) then
              write(lunW,*) txt132(debstr(txt132):finstr(txt132))
              read(lunR,*,end=13) icav
              write(lunW,*) 0 
              read(lunR,fmt='(a)',end=13) txt132
              write(lunW,*) txt132(debstr(txt132):finstr(txt132))
              read(lunR,fmt='(a)',end=13) txt132
              write(lunW,*) txt132(debstr(txt132):finstr(txt132))
          elseif(strcon(txt132,'''REBELOTE''',10,
     >                                         IS)) then
              write(lunW,*) txt132(debstr(txt132):finstr(txt132))
              read(lunR,fmt='(a)',end=13) txt132
              write(lunW,*) ' 9  0.2  99'
          elseif(strcon(txt132,'''SYSTEM''',8,
     >                                        IS)) then
              read(lunR,*,end=13) ksys
              do kk = 1, ksys
                read(lunR,fmt='(a)',end=13) txtksy(kk)
              enddo
           else

             if(len(trim(txt132)) .le. 1) then
                   write(lunW,fmt='(a)') ' '
                   write(*,fmt='(a)') ' < 4 ',trim(txt132)
             else
                write(lunW,*) trim(txt132)
             endif
          endif
          
        goto 12

 13     continue
      write(*,*) ' '
      write(*,*) ' Pgm geneZGDat4Xing_fromCalcStrength : '
     >//' End of zgoubi_geneZGDat4Xing-Out.dat input file has'
     >//' been reached, file zgoubi_searchCO-In.dat completed.'
      write(*,*) ' ------------'
      write(*,*) ' '
      close(lunR)
      close(lunW)

C Get closed orbit
      cmmnd = '~/zgoubi/current/toolbox/searchCO/searchCO_HV'
      call system(cmmnd)
      open(unit=lunR,file='zgoubi_searchCO-Out.dat')
      open(unit=lunW,file='zgoubi_geneZGDat4Xing-Out.dat')
C get co coordinates and rebuild objet in zgoubi_geneZGDat4Xing-Out.dat'
          read(lunR,fmt='(a)',end=16) txt132   ! titl
          write(lunW,*) txt132(debstr(txt132):finstr(txt132))
          read(lunR,fmt='(a)',end=16) txt132   ! objet
          write(lunW,*) txt132(debstr(txt132):finstr(txt132))
          read(lunR,fmt='(a)',end=16) txt132   ! boro
          write(lunW,*) txt132(debstr(txt132):finstr(txt132))
          read(lunR,fmt='(a)',end=16) txt132   ! kobj
          write(lunW,*) ' 8 '
          read(lunR,fmt='(a)',end=16) txt132   !  1 1 1
          write(lunW,*) ' 1 1 1 '
          read(lunR,*,end=16) yo, ypo, zo, zpo, xo, do, let1
          write(lunW,fmt='(1p,4(e12.4,1x),e9.1,1x,e14.6,1x,3a1)') 
     >      yo/1d2, ypo/1d3, zo/1d2, zpo/1d3, xo, do, '''',let1,''''
          read(lunR,fmt='(a)',end=16) txt132   !  '  1  '
          write(lunW,*)  alfy, bety, epsy
          write(lunW,*)  alfz, betz, epsz
          write(lunW,*)  alfl, betl, epsl

 15       continue
            read(lunR,fmt='(a)',end=16) txt132
            if(strcon(txt132,'''CAVITE''',8,
     >                                        IS)) then
              write(lunW,*) txt132(debstr(txt132):finstr(txt132))
              read(lunR,fmt='(a)',end=16) txt132
              write(lunW,*) icav
              read(lunR,fmt='(a)',end=16) txt132
              write(lunW,*) txt132(debstr(txt132):finstr(txt132))
              read(lunR,fmt='(a)',end=16) txt132
              write(lunW,*) txt132(debstr(txt132):finstr(txt132))

            elseif(strcon(txt132,'''REBELOTE''',10,
     >                                         IS)) then
              write(lunW,*) txt132(debstr(txt132):finstr(txt132))
              read(lunR,fmt='(a)',end=16) txt132
c              write(lunW,*) 2*nrbl2, '  0.2  99'
              write(lunW,*) 3*nrbl2, '  0.2  99'

              txt132 = '''SYSTEM'''
              write(lunW,*) txt132(debstr(txt132):finstr(txt132))
              write(lunW,*) ksys
              do kk = 1, ksys
                txt150 = txtksy(kk)
                write(lunW,fmt='(a)') 
     >             txt150(debstr(txt150):finstr(txt150))
              enddo

            else

             if(len(trim(txt132)) .le. 1) then
                   write(lunW,fmt='(a)') ' '
                   write(*,fmt='(a)') ' < 4 ',trim(txt132)
             else
                write(lunW,*) trim(txt132)
             endif

            endif
            goto 15

 16     continue
      write(*,*) ' '
      write(*,*) ' Pgm geneZGDat4Xing_fromCalcStrength : '
     >//' End of zgoubi_searchCO-Out.dat input file has'
     >//' been reached, file zgoubi_geneZGDat4Xing-Out.dat completed.'
      write(*,*) ' ------------'
      write(*,*) ' '
      close(lunR)
      close(lunW)

      stop

 99   continue
      write(*,*) ' '
      write(*,*) ' Pgm geneZGDat4Xing_fromCalcStrength : '
     >//' Error during read from geneZGDat4Xing.In. '
c      read(*,*)
      stop
      end
      FUNCTION STRCON(STR,STRIN,NCHAR,
     >                                IS)
      implicit double precision (a-h,o-z)
      LOGICAL STRCON
      CHARACTER STR*(*), STRIN*(*)
C     ------------------------------------------------------------------------
C     .TRUE. if the string STR contains the string STRIN with NCHAR characters
C     at least once.
C     IS = position of first occurence of STRIN in STR
C     ------------------------------------------------------------------------

      INTEGER DEBSTR,FINSTR

      II = 0
      DO 1 I = DEBSTR(STR), FINSTR(STR)
        II = II+1
        IF( STR(I:I+NCHAR-1) .EQ. STRIN ) THEN
          IS = II
          STRCON = .TRUE.
          RETURN
        ENDIF
 1    CONTINUE
      STRCON = .FALSE.
      RETURN
      END
      FUNCTION DEBSTR(STRING)
      implicit double precision (a-h,o-z)
      INTEGER DEBSTR
      CHARACTER * (*) STRING

C     --------------------------------------
C     RENVOIE DANS DEBSTR LE RANG DU
C     1-ER CHARACTER NON BLANC DE STRING,
C     OU BIEN 0 SI STRING EST VIDE ou BLANC.
C     --------------------------------------

      DEBSTR=0
      LENGTH=LEN(STRING)
C      LENGTH=LEN(STRING)+1
1     CONTINUE
        DEBSTR=DEBSTR+1
C        IF(DEBSTR .EQ. LENGTH) RETURN
C        IF (STRING(DEBSTR:DEBSTR) .EQ. ' ') GOTO 1
        IF (STRING(DEBSTR:DEBSTR) .EQ. ' ') THEN
          IF(DEBSTR .EQ. LENGTH) THEN
            DEBSTR = 0
            RETURN
          ELSE
            GOTO 1
          ENDIF
        ENDIF

      RETURN
      END
      FUNCTION FINSTR(STRING)
      implicit double precision (a-h,o-z)
      INTEGER FINSTR
      CHARACTER * (*) STRING
C     --------------------------------------
C     RENVOIE DANS FINSTR LE RANG DU
C     DERNIER CHARACTER NON BLANC DE STRING,
C     OU BIEN 0 SI STRING EST VIDE ou BLANC.
C     --------------------------------------

      FINSTR=LEN(STRING)+1
1     CONTINUE
        FINSTR=FINSTR-1
        IF(FINSTR .EQ. 0) RETURN
        IF (STRING(FINSTR:FINSTR) .EQ. ' ') GOTO 1

      RETURN
      END
      subroutine readat(lunDat,
     >   typ2,xpfpi,DltGgn,circ,ah,gtr,Vp,phisD,am,q,G,ierr)
      implicit double precision (a-h,o-z)
      character typ2*(*)
      ierr = 0
      read(lunDat,fmt='(a)',err=99,end=98) typ2
      read(lunDat,*,err=99,end=98) xpfpi, DltGgn
      read(lunDat,*,err=99,end=98) circ, ah
      read(lunDat,*,err=99,end=98) gtr
      read(lunDat,*,err=99,end=98) Vp, phisD
      read(lunDat,*,err=99,end=98) am, q, G
      return
 99   continue
      ierr = 1
      write(*,*) ' error during read in data file'
      return
 98   continue
      write(*,*) ' End of data file reached'
      return
      end

      FUNCTION EMPTY(STR)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL EMPTY
      CHARACTER*(*) STR
C     -----------------------------------------------------
C     .TRUE. if STR is either empty or contains only blanks
C     -----------------------------------------------------

      INTEGER FINSTR
      EMPTY = FINSTR(STR) .EQ. 0
      RETURN
      END
      FUNCTION ISNUM(string)
      IMPLICIT NONE
      CHARACTER(len=*), INTENT(IN) :: string
      LOGICAL :: ISNUM
      LOGICAL :: strcon
      REAL :: x
      INTEGER :: e
      INTEGER :: IS
      ISNUM = .FALSE.
      IF(strcon(string,'/',len(trim(string)),
     >                     IS)) THEN
        ISNUM = .FALSE.
      ELSE
        READ(string,*,IOSTAT=e) x
        ISNUM = e == 0
      ENDIF
      END FUNCTION ISNUM

