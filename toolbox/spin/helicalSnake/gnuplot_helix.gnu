


# set parametric   # replace x by t
# Parametric functions for a circle

r = 1.
 
set xrange [0:4.*pi]

hx(x) = -r*(1.-cos(x))
hy(x) = -r*sin(x) + r*x

plot hx(x), hy(x)

pause 4
exit

