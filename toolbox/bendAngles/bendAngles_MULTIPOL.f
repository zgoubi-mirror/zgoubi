      implicit double precision (a-h,o-z)
      character(200) txt
      logical strcon, ok
      character(1) rep
      
      parameter (pi = 4.d0 * atan(1.d0), dpi=2.d0*pi)

      lr = 1
      open(unit=lr,file='bendAngles.in') !  a link to zgoubi.dat for instance
      open(unit=2,file='bendAngles.out')  ! a new version of the above, with updated field and ale

      iter = 0

 4    continue
      nArcBnd = 0
      nArcBndKP3 = 0
      nKP3O = 0
      sale = 0.d0
      saleO = 0.d0
      ii = 0
 1    continue
        ii = ii +1
C        devTot =   6.2783841179999991
        if(iter.ne.1) devTot = dpi
        if(iter.eq.2) lr=2
        if(iter.eq.1) then
          dtta = (dpi - devTot) / dble(nDip)
        else
          dtta = 0.d0
        endif
        if(ii.eq.1) then
          write(*,*) ' '
          write(*,*) 'Now iter, devtot, dtta :',iter,devtot,dtta
          read(*,*)
        endif
       
        read(lr,fmt='(a)',err=10,end=10) txt
        if(lr.le.1) write(2,*) trim(txt)
        ok = strcon(txt,'RBEN',
     >                             IS)
     >   .or. strcon(txt,'SBEN',
     >                             IS)
            
        if ( .not. ok) goto 1

c        ok = strcon(txt,'_DH',
c     >                         IS)
c        ok = ok .and. (strcon(txt,'BO',
c     >                                IS1)
c     >       .or.  strcon(txt,'BI',
c     >                             IS1))
c        if(ok) read(txt(is+3:is+4),*) num
c        ok = ok .and. (num .ge. 10 .and. num .le. 21) 
c        if(ok) write(*,*) trim(txt)
        
        read(lr,*) txt       ! IL
        if(lr.le.1) write(2,*) trim(txt)
        read(lr,fmt='(a)',err=10,end=10) txt 
        read(txt,*) xl, rad, b
        tta = 2.d0*asin(b*xl *1.d-3/2.d0) 
        if(ok) tta = tta + dtta
        b = 2.d0 / xl *1.d3 *sin(tta/2.d0)
        if(lr.le.1) write(2,fmt='(1p,E16.9,1x,E12.4,1x,E16.9,a)')
     >  xl,rad,b,' 1. 0.' 
c        write(*,*) trim(txt)
        read(lr,fmt='(a)',err=10,end=10) txt
        if(lr.le.1) write(2,*) trim(txt)
        read(lr,fmt='(a)',err=10,end=10) txt
        if(lr.le.1) write(2,*) trim(txt)
        read(lr,fmt='(a)',err=10,end=10) txt
        if(lr.le.1) write(2,*) trim(txt)
        read(lr,fmt='(a)',err=10,end=10) txt
        if(lr.le.1) write(2,*) trim(txt)
        read(lr,fmt='(a)',err=10,end=10) txt
        if(lr.le.1) write(2,*) trim(txt)
        read(lr,fmt='(a)',err=10,end=10) txt
        if(lr.le.1) write(2,*) trim(txt)
        read(lr,fmt='(a)',err=10,end=10) txt
        read(txt,*) KPOS, xce, yce, ale
        if(ok.and.iter.eq.1) ale = -tta/2.d0
        if(lr.le.1) write(2,fmt='(1p,i2,3(1x,e16.9))')
     >  KPOS, xce, yce, ale

        if(ok) then
          nArcBnd = nArcBnd + 1
          if(nDip.lt.nArcBnd) nDip = nArcBnd
          ok = ok .and. KPOS .eq. 3
          if(ok) then
             nArcBndKP3 = nArcBndKP3 + 1
             sale = sale + ale
          endif
        else
          nKP3O = nKP3O + 1
          saleO = saleO + ale
        endif

c        if(ok) write(*,*) ' iter, dev ',iter,-2.*(sale+saleO)
        
      goto 1

 10   continue

      if(iter.eq.0) then
      
        write(*,*) ' '
        write(*,*) '-----------------------------------------------'
        write(*,*) '-----------------------------------------------'
        write(*,*) 'Found ',nDip,' arc bends, and ',
     >  nArcBndKP3,' bends with KP=3 positioning.'
        write(*,*) 'Total deviation in these arc BENDs, ALE_tot = '
     >  ,-2.d0*sale,' rad,    ',-2.d0*sale/pi*180.d0,' deg.'
        write(*,*) 'Found ',nKP3O,' oher BENDs with KPOS=3, ALE_tot = '
     >  ,-2.d0*saleO,' rad,    ',-2.d0*saleO/pi*180.d0,' deg'
        devTot = -2.d0*(sale+saleO)
        write(*,*) '   Total deviation =',
     >  devTot,' rad,  ',devTot/pi*180.d0,' deg'
        write(*,*) '-----------------------------------------------'
        write(*,*) '-----------------------------------------------'
        write(*,*) ' '
        write(*,*) ' '
 2      write(*,*) ' Apply correction ? (y/n)'
        read(*,*,err=2,end=2) rep
        if(rep.eq.'y') iter = 1

      elseif(iter.eq.1) then
         iter = 2
      else
         iter=-1
      endif

      if(iter.ge.1) then
        if(iter.eq.1) then
          rewind(1)
          rewind(2)
        elseif(iter.eq.2) then
          close(1)
          lr = 2        
          flush(2)
          rewind(2)
        endif
C        if(iter.le.1) goto 4
        if(iter.le.2) goto 4
      endif

      if(rep .eq. 'y') then
        write(*,*) ' '
        write(*,*) '-----------------------------------------------'
        write(*,*) 'CHECK TOTAL DEVIATION, AFTER CORRECTION: '
        write(*,*) 'Found ',nArcBnd,' arc bends, and ',
     >  nArcBndKP3,' bends with KP=3 positioning.'
        write(*,*) 'Total deviation in these arc BENDs, ALE_tot = '
     >  ,-2.d0*sale,' rad,    ',-2.d0*sale/pi*180.d0,' deg.'
        write(*,*) 'Found ',nKP3O,' oher BENDs with KPOS=3, ALE_tot = '
     >  ,-2.d0*saleO,' rad,    ',-2.d0*saleO/pi*180.d0,' deg'
        devTot = -2.d0*(sale+saleO)
        write(*,*) '   Total deviation =',
     >  devTot,' rad,  ',devTot/pi*180.d0,' deg'
        write(*,*) '-----------------------------------------------'
        write(*,*) '-----------------------------------------------'
        write(*,*) ' '
        write(*,*) ' '
        write(*,*) 'Correction has been applied. New file in'
     >     //'  bendAngles.out'
      endif
      
      write(*,*) 'Done !'
      

      close(1)
      close(2)
      
      stop
       end
      FUNCTION DEBSTR(STRING)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INTEGER DEBSTR
      CHARACTER * (*) STRING

C     --------------------------------------
C     RENVOIE DANS DEBSTR LE RANG DU
C     1-ER CHARACTER NON BLANC DE STRING,
C     OU BIEN 0 SI STRING EST VIDE ou BLANC.
C     --------------------------------------

      DEBSTR=0
      LENGTH=LEN(STRING)
1     CONTINUE
        DEBSTR=DEBSTR+1
        IF (STRING(DEBSTR:DEBSTR) .EQ. ' ') THEN
          IF(DEBSTR .EQ. LENGTH) THEN
            DEBSTR = 0
            RETURN
          ELSE
            GOTO 1
          ENDIF
        ENDIF

      RETURN
      END
      FUNCTION FINSTR(STRING)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INTEGER FINSTR
      CHARACTER * (*) STRING
C     --------------------------------------
C     RENVOIE DANS FINSTR LE RANG DU
C     DERNIER CHARACTER NON BLANC DE STRING,
C     OU BIEN 0 SI STRING EST VIDE ou BLANC.
C     --------------------------------------

      FINSTR=LEN(STRING)+1
1     CONTINUE
        FINSTR=FINSTR-1
        IF(FINSTR .EQ. 0) RETURN
        IF (STRING(FINSTR:FINSTR) .EQ. ' ') GOTO 1

      RETURN
      END
      FUNCTION STRCON(STR,STR2,
     >                         IS)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL STRCON
      CHARACTER STR*(*), STR2*(*)
C     ---------------------------------------------------------------
C     .TRUE. if the string STR contains the string STR2 at least once
C     IS = position of first occurence of STR2 in STR 
C     (i.e.,STR(IS:IS+LEN(STR2)-1)=STR2)
C     ---------------------------------------------------------------
      INTEGER DEBSTR,FINSTR
      LNG2 = LEN(STR2(DEBSTR(STR2):FINSTR(STR2)))
      IF(LEN(STR).LT.LNG2) GOTO 1
      DO I = DEBSTR(STR), FINSTR(STR)-LNG2+1
        IF( STR(I:I+LNG2-1) .EQ. STR2 ) THEN
          IS = I 
          STRCON = .TRUE.
          RETURN
        ENDIF
      ENDDO
 1    CONTINUE
      STRCON = .FALSE.
      RETURN
      END
