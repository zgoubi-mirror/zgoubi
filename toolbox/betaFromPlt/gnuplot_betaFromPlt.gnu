
system './betaFromPlt'

 set title "Beta and dispersion functions \n - from betaFromPlt -"  #  font "roman,16"   # offset 0,+.7    

 set xlabel "s (m)"       # font "roman,16"   # offset +4,-.5 rotate by +20  
 set ylabel "{/Symbol b}_x, {/Symbol b}_y (m)"           #  font "roman,13"   #offset -0,-1 rotate by -20 
 set y2label "D_x, D_y (m)"        #  font "roman,13"   #offset -0,-1 rotate by -20 

 set xtics  nomirror   # font "roman,12" 
 set ytics  nomirror   # font "roman,12"      #offset 0,-.6
 set y2tics  nomirror  # font "roman,12"   #offset 0,-.6

set key top left    # font "roman, 12"  samplen 1  
set key maxrow 2
unset grid

cm2m = 0.01

 plot  \
      "betaFromPlt.out" u ($13 * cm2m):($2)  axes x1y1 w lp ps .2 pt 4 lc rgb "red"  tit "{/Symbol b}_x"  ,\
      "betaFromPlt.out" u ($13 * cm2m):($4)  axes x1y1 w lp ps .2 pt 5 lc rgb "blue" tit "{/Symbol b}_y"  ,\
      "betaFromPlt.out" u ($13 * cm2m):($7)  axes x1y2 w lp ps .2 pt 6 lc rgb "black"  tit "   D_x"  ,\
      "betaFromPlt.out" u ($13 * cm2m):($9)  axes x1y2 w lp ps .2 pt 7 lc rgb "cyan" tit "   D_y"  

#set samples 100000
 set terminal postscript eps blacktext color enh  # size 8.3cm,6cm "Times-Roman" 12
 set output "gnuplot_betaFromPlt.eps"
 replot
 set terminal X11
 unset output

      pause 1

 exit
