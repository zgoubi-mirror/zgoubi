
set title "1000-turn admittance"

 set xlabel "x (mm)" font "roman,18"
 set ylabel "y (mm)" font "roman,18"

 set xtics font "roman,14"
 set ytics font "roman,14"

cm2mm=10.

set xrange [-30:30]

plot  for [i=1:5] \
   'searchDA.out'  using ($1*cm2mm):($4==i ? $2*cm2mm : 1/0) w lp pt i ps .4 lw 2 lt 1 lc i title 'dp/p='.(2*(i-3)).'e-3'    

 set terminal postscript eps blacktext color enh size 8.3cm,5cm "Times-Roman" 12
 set output "gnuplot_DAs.eps"
 replot
 set terminal X11
 unset output

pause 8




