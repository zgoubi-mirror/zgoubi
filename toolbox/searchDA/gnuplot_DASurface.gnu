
set title "Admittance vs. momentum. Data read from serachDA.out"

 set xlabel "dp/p" font "roman,18"
 set ylabel "Admittance (mm^2)" font "roman,18"

 set xtics font "roman,14"
 set ytics nomirror font "roman,14"
 set y2tics nomirror font "roman,0.1"

set logscale y

set key maxrow 1
set key c b

cmsq2mmsq=1.e2

plot   \
   'surface.out'  using ($1):($2*cmsq2mmsq) w lp pt 5 ps .4 lw 2 lt 1 lc 3  notit

 set terminal postscript eps blacktext color enh size 8.3cm,5cm "Times-Roman" 12
 set output "gnuplot_DASurface.eps"
 replot
 set terminal X11
 unset output

pause 8


